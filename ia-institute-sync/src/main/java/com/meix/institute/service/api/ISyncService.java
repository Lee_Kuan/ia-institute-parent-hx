package com.meix.institute.service.api;

import java.util.Map;

/**
 * Created by zenghao on 2019/8/27.
 */
public interface ISyncService {

	void syncSecumain(Map<String, Object> params);

	void syncHeatData(Map<String, Object> params);

	void syncConstant(Map<String, Object> params);

	void syncPersonaInfoList(Map<String, Object> params);
}
