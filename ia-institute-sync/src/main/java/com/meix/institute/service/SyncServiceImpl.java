package com.meix.institute.service;

import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.api.IStatDataService;
import com.meix.institute.api.IUserDao;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.config.IaserverConfig;
import com.meix.institute.constant.HttpApi;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.constant.SyncConstants;
import com.meix.institute.impl.IaServiceImpl;
import com.meix.institute.service.api.ISyncService;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.HttpUtil;
import com.meix.institute.vo.stat.HeatDataVo;
import com.meix.institute.vo.user.UserInfo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/8/27.
 */
@Service
public class SyncServiceImpl implements ISyncService {
	private Logger log = LoggerFactory.getLogger(SyncServiceImpl.class);

	/**
	 * ia-server 通信接口
	 * http|https
	 */
	@Autowired
	protected IaServiceImpl iaServiceImpl;
	@Autowired
	private IaserverConfig iaserverConfig;
	@Autowired
	private IStatDataService statDataService;
	@Autowired
	private IUserDao userDao;
	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IMeixSyncService meixSyncService;

	@SuppressWarnings("unchecked")
	@Override
	public void syncSecumain(Map<String, Object> params) {
		JSONArray dataArray = null;
		long startId = MapUtils.getLongValue(params, "startId", 0);
		do {
			JSONObject clientstr = new JSONObject();
			clientstr.putAll(params);
			JSONObject result = iaServiceImpl.httpPostByWehcat(clientstr, HttpApi.SYNC_SECUMAIN_LIST);
			if (result == null) {
				log.error("获取证券信息列表失败");
				break;
			}
			dataArray = result.getJSONArray("data");
			if (dataArray == null || dataArray.size() == 0) {
				log.info("获取证券信息列表为空");
				break;
			}
			String innerUrl = iaserverConfig.getInsInnerHome() + HttpApi.SAVE_SECUMAIN_LIST;
			Map<String, String> saveParam = new HashMap<>();
			saveParam.put("dataJson", dataArray.toString());
			HttpUtil.postHttpBasic(innerUrl, null, saveParam);
			//更新批次（startId）
			long endId = dataArray.getJSONObject(dataArray.size() - 1).getLong("id");
			log.info("同步证券信息列表{}条,startId:{},endId:{}", dataArray.size(), startId, endId);
			startId = endId;
			params.put("startId", startId);
		} while (dataArray != null && dataArray.size() > 0);
	}

	@Override
	public void syncHeatData(Map<String, Object> params) {
		JSONArray dataArray = null;
		long startId = MapUtils.getLongValue(params, "startId", 0);
		long endId = 0;
		do {
			JSONObject clientstr = new JSONObject();
			clientstr.putAll(params);
			@SuppressWarnings("unchecked")
			JSONObject result = iaServiceImpl.httpPostByWehcat(clientstr, HttpApi.SYNC_HEAT_DATA_LIST);
			if (result == null) {
				log.error("获取热度列表失败");
				break;
			}
			dataArray = result.getJSONArray("data");

			if (dataArray == null || dataArray.size() == 0) {
				log.info("获取热度列表为空");
				break;
			}
			List<HeatDataVo> list = GsonUtil.json2List(dataArray.toString(), HeatDataVo.class);
			//热度列表是删除旧的数据再批量插入，同步时保持一致
			startId = list.get(0).getId();
			endId = list.get(list.size() - 1).getId();

			log.info("同步热度列表{}条,startId:{},endId:{}", dataArray.size(), startId, endId);

			statDataService.removeHeatDataRange(startId, endId);
			statDataService.insertHeatDataList(list);

			startId = endId;
			params.put("startId", startId);
		} while (dataArray != null && dataArray.size() > 0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void syncConstant(Map<String, Object> params) {
		JSONArray dataArray = null;
		long startId = MapUtils.getLongValue(params, "startId", 0);
		do {
			JSONObject clientstr = new JSONObject();
			clientstr.putAll(params);
			JSONObject result = iaServiceImpl.httpPostByWehcat(clientstr, HttpApi.SYNC_CONSTANT_LIST);
			if (result == null) {
				log.error("获取常量列表失败");
				break;
			}
			dataArray = result.getJSONArray("data");
			if (dataArray == null || dataArray.size() == 0) {
				log.info("获取常量列表为空");
				break;
			}
			String innerUrl = iaserverConfig.getInsInnerHome() + HttpApi.SAVE_CONSTANT_LIST;
			Map<String, String> saveParam = new HashMap<>();
			saveParam.put("dataJson", dataArray.toString());
			HttpUtil.postHttpBasic(innerUrl, null, saveParam);
			//更新批次（startId）
			long endId = dataArray.getJSONObject(dataArray.size() - 1).getLong("id");
			log.info("同步常量列表{}条,startId:{},endId:{}", dataArray.size(), startId, endId);
			startId = endId;
			params.put("startId", startId);
		} while (dataArray != null && dataArray.size() > 0);
	}

	@Override
	public void syncPersonaInfoList(Map<String, Object> params) {
		params.put("insCompanyCode", commonServerProperties.getCompanyCode());
		long currentPage = MapUtils.getLongValue(params, "currentPage", 0);
		int showNum = MapUtils.getIntValue(params, "showNum", 100);
		int userCount = 0;
		do {
			List<UserInfo> userList = userDao.getUser(params);
			if (userList == null || userList.size() == 0) {
				break;
			}
			userCount = userList.size();
			List<Map<String, Object>> userInfoList = new ArrayList<>();
			List<Long> uidList = new ArrayList<>();
			for (UserInfo user : userList) {
				Map<String, Object> userVo = new HashMap<>();
				userVo.put("mobile", user.getMobile());
				userInfoList.add(userVo);
				uidList.add(user.getId());
			}

			if (userInfoList.size() == 0) {
				return;
			}

			Map<String, Object> reqParams = new HashMap<>();
			reqParams.putAll(params);
			reqParams.put("uidListJson", GsonUtil.obj2Json(uidList));
			reqParams.put("userInfoListJson", GsonUtil.obj2Json(userInfoList));
			reqParams.put("companyCode", commonServerProperties.getCompanyCode());
			meixSyncService.reqMeixSync(MeixConstants.USER_YH, SyncConstants.ACTION_GET_PERSONA_INFO_LIST, reqParams);
			currentPage += 1;
			log.info("同步第{}批用户画像", currentPage);
			params.put("currentPage", currentPage * showNum);
		} while (userCount > 0);
	}
}
