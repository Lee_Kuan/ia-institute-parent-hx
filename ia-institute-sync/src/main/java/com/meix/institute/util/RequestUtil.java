package com.meix.institute.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class RequestUtil {

	/**
	 * @param request
	 * @return
	 * @Title: getHeaderMap、
	 * @Description:获取请求头信息
	 * @return: Map<String,String>
	 */
	public static Map<String, String> getHeaderMap(HttpServletRequest request) {
		Map<String, String> headerMap = new HashMap<String, String>();
		Enumeration<String> heads = request.getHeaderNames();
		while (heads.hasMoreElements()) {
			String key = (String) heads.nextElement();
			headerMap.put(key, request.getHeader(key));
		}
		return headerMap;
	}

	public static Map<String, String> getHeaderMap(HttpServletRequest request, String... keys) {
		Map<String, String> keyMap = new HashMap<String, String>();
		for (String head : keys) {
			keyMap.put(head.toLowerCase(), head);
		}
		Map<String, String> headerMap = new HashMap<String, String>();
		Enumeration<String> heads = request.getHeaderNames();
		while (heads.hasMoreElements()) {
			String key = (String) heads.nextElement().toLowerCase();
			if (keyMap.containsKey(key)) {
				headerMap.put(keyMap.get(key), request.getHeader(key));
			}
		}
		return headerMap;
	}

	public final static String uploadUrl = "";
	public final static String downloadUrl = "";


	/**
	 * @param userAgent
	 * @return
	 * @Title: isMobile、
	 * @Description:判断请求来自手机还是PC
	 * @return: int 1 android 2 IOS 0 PC
	 */
	public static int isMobile(String userAgent) {
		if (userAgent.toUpperCase().indexOf("ANDROID") > -1) {
			return 1;
		} else if (userAgent.toUpperCase().indexOf("IPHONE") > -1) {
			return 2;
		} else if (userAgent.toUpperCase().indexOf("IPAD") > -1) {
			return 2;
		} else {
			return 0;
		}
	}

	/**
	 * 获取访问者IP
	 * <p>
	 * 在一般情况下使用Request.getRemoteAddr()即可，但是经过nginx等反向代理软件后，这个方法会失效。
	 * <p>
	 * 本方法先从Header中获取X-Real-IP，如果不存在再从X-Forwarded-For获得第一个IP(用,分割)，
	 * 如果还不存在则调用Request .getRemoteAddr()。
	 *
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-Real-IP");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return ip;
		}
		ip = request.getHeader("X-Forwarded-For");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return dealIP(ip);
		}
		ip = request.getHeader("Proxy-Client-IP");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return dealIP(ip);
		}
		ip = request.getHeader("WL-Proxy-Client-IP");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return dealIP(ip);
		}
		ip = request.getHeader("HTTP_CLIENT_IP");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return dealIP(ip);
		}
		ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return dealIP(ip);
		}

		return request.getRemoteAddr();
	}

	public static String dealIP(String ip) {
		// 多次反向代理后会有多个IP值，第一个为真实IP。
		int index = ip.indexOf(',');
		if (index != -1) {
			return ip.substring(0, index);
		} else {
			return ip;
		}
	}

	/**
	 * @param request
	 * @return
	 * @Title: getVersionValue、
	 * @Description:获取应用版本
	 * @return: long
	 */
	public static long getVersionValue(HttpServletRequest request) {
		//接口版本
		String interfaceVersion = request.getHeader("interfaceVersion");
		if (interfaceVersion == null) {
			interfaceVersion = request.getHeader("appVersion");
		}
		long version = VersionUtil.getVersionValue(interfaceVersion);
//		String clientType = request.getHeader("clientType");
//		if (clientType != null && clientType.equals("7")) {
//			version += VersionUtil.v1_7_0;
//		}
		return version;
	}
}
