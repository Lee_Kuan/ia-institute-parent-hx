package com.meix.institute.util;

import com.meix.institute.constant.MeixConstants;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @类说明 : 移动投资研究助手工具类
 */
public final class MobileassistantUtil {
	public static Logger log = LoggerFactory.getLogger(MobileassistantUtil.class);

	/**
	 * @创建人：yaojj 2014-4-16
	 * @功能介绍：生成指定位数随机数
	 */
	public static String getFixLenthString(int strLength) {
		Random rm = new Random();
		// 获得随机数
		double pross = (1 + rm.nextDouble()) * Math.pow(10, strLength);
		// 将获得的获得随机数转化为字符串
		String fixLenthString = String.valueOf(pross);
		// 返回固定的长度的随机数
		return fixLenthString.substring(1, strLength + 1);
	}

	/**
	 * @方法信息：2014-5-7 zhouwei
	 * @功能介绍：获取请求的json对象
	 */
	public static JSONObject parseRequest(Object clientstr) {
		JSONObject obj = JSONObject.fromObject(clientstr);
		return obj;
	}

	/**
	 * @方法信息：2014-5-17 zhouwei
	 * @功能介绍：获取TOken中的用户信息
	 */
	public static String getUserName(String token) {
		String str = Des.decrypt(token);
		if (StringUtils.isBlank(str)) {
			return null;
		}
		return str.split(MeixConstants.tokenSplite)[0];
	}

	/**
	 * @param token
	 * @return
	 * @Title: getUserId、
	 * @Description:获取Token中的用户ID
	 * @return: String
	 */
	public static long getUserId(String token) {
		if (StringUtils.isEmpty(token)) {
			return 0;
		}
		String str = Des.decrypt(token);
		if (StringUtils.isBlank(str)) {
			return 0;
		}
		return Long.parseLong(str.split(MeixConstants.tokenSplite)[6]);
	}

	/**
	 * @param token
	 * @return
	 * @Title: getClientType、
	 * @Description:获取客户端类型
	 * @return: int
	 */
	public static int getClientType(String token) {
		if (StringUtils.isEmpty(token)) {
			//外部页面
			return MeixConstants.CLIENT_TYPE_PC;
		}
		String str = Des.decrypt(token);
		if (StringUtils.isBlank(str)) {
			return MeixConstants.CLIENT_TYPE_PC;
		}
		return Integer.parseInt(str.split(MeixConstants.tokenSplite)[7]);
	}

	public static String[] getTokenArray(JSONObject obj) {
		String str = Des.decrypt(obj.getString("token"));
		if (StringUtils.isBlank(str)) {
			return null;
		}
		return str.split(MeixConstants.tokenSplite);
	}

	public static String[] getTokenArray(String token) {
		String str = Des.decrypt(token);
		if (StringUtils.isBlank(str)) {
			return null;
		}
		return str.split(MeixConstants.tokenSplite);
	}

	/**
	 * @param srcToken
	 * @param accountType
	 * @param deviceId
	 * @return
	 * @Title: generateNewToken、
	 * @Description:生成新的token
	 * @return: String
	 */
	public static String generateNewToken(String srcToken, int accountType, String deviceId) {
		srcToken = Des.decrypt(srcToken);
		if (StringUtils.isBlank(srcToken)) {
			return "";
		}
		String[] srcTokens = srcToken.split(MeixConstants.tokenSplite);
		String newToken = srcToken;
		if (srcTokens.length == 8) {
			newToken += MeixConstants.tokenSplite + accountType + MeixConstants.tokenSplite + deviceId;
		} else if (srcTokens.length == 9) {
			newToken += MeixConstants.tokenSplite + deviceId;
		}
		//token 构成 ： 手机号，验证码，系统，应用版本，时间，设备token，用户ID，登录类型，账号类型，设备ID
		return Des.encrypt(newToken);
	}

	/**
	 * @param user
	 * @param deviceId
	 * @return
	 * @Title: generateLoginToken、
	 * @Description:生成登录密钥
	 * @return: String
	 */
//	public static String generateLoginToken(UserInfo user, String deviceId) {
//		//token 构成 ： 手机号，验证码，系统，应用版本，时间，设备token，用户ID，登录类型，账号类型，设备ID
//		String tokenstr = user.getMobileOrEmail() + MeixConstants.tokenSplite +
//			user.getPwdOrUrl() + MeixConstants.tokenSplite +
//			user.getOsVersion() + MeixConstants.tokenSplite +
//			user.getAppVersion() + MeixConstants.tokenSplite +
//			user.getLocaltime() + MeixConstants.tokenSplite +
//			user.getDeviceToken() + MeixConstants.tokenSplite +
//			user.getId() + MeixConstants.tokenSplite +
//			user.getClientType() + MeixConstants.tokenSplite +
//			user.getAccountType() + MeixConstants.tokenSplite
//			+ deviceId;
//		return Des.encrypt(tokenstr);
//	}

	/**
	 * @param request
	 * @return
	 * @Title: getRequestParams、
	 * @Description:获取请求参数
	 * @return: JSONObject
	 */
	public static JSONObject getRequestParams(HttpServletRequest request) {
		JSONObject obj = (JSONObject) request.getAttribute("clientstr");
		if (obj == null) {
			String clientstr = request.getParameter("clientstr");
			if (!StringUtils.isEmpty(clientstr)) {
				try {
					obj = JSONObject.fromObject(clientstr);
				} catch (Exception e) {
					obj = null;
				}
			}
		}
		if (obj != null) {
			String token = MapUtils.getString(obj, "token", "");
			String openId = MapUtils.getString(obj, "openId");
			if (StringUtils.isEmpty(openId)) {
				openId = request.getHeader("openId");
			}
			long uid = 0;
			if (!StringUtils.isEmpty(token)) {
				try {
					String str = Des.decrypt(token);
					if (StringUtils.isBlank(str)) {

					} else {
						uid = Long.parseLong(str.split(MeixConstants.tokenSplite)[6]);
					}
					//用户ID属性
					request.setAttribute("uid", uid);
					obj.put("uid", uid);
					if (!obj.containsKey("clientType")) {
						obj.put("clientType", MobileassistantUtil.getClientType(token));
					}
//					ThreadParameter.setUid(uid);
				} catch (Exception e) {
					//非用户登录的token

				}
				//保存用户请求信息
				//MobileassistantUtil.addUrl(uri, str,request);
			}
			//设置最大查询数量showNum--50
			if (null != obj.get("showNum")) {
				int showNum = MapUtils.getIntValue(obj, "showNum");
				if (showNum > 50) {
					obj.put("showNum", 50);
				}
			}
		}
		return obj;
	}

	/**
	 * @param token
	 * @return
	 * @Title: getDeviceToken、
	 * @Description:获取token中的设备信息
	 * @return: String
	 */
	public static String getDeviceToken(String token) {
		String str = Des.decrypt(token);
		if (StringUtils.isBlank(str)) {
			return null;
		}
		return str.split(MeixConstants.tokenSplite)[5];
	}

	/**
	 * @param token
	 * @return
	 * @Title: getDeviceType、
	 * @Description:获取设备类型 1 IOS 2 android
	 * @return: int
	 */
	public static int getDeviceType(String token) {
		int deviceType = 0;
		String str = Des.decrypt(token);
		if (StringUtils.isBlank(str)) {
			return deviceType;
		}
		if (str.split(MeixConstants.tokenSplite).length < 3) {
			return deviceType;
		}
		str = str.split(MeixConstants.tokenSplite)[2].toUpperCase();
		if (str.indexOf("ANDROID") > -1) {
			deviceType = 2;
		} else if (str.indexOf("OS") > -1) {
			deviceType = 1;
		} else {
			deviceType = MeixConstants.APP_WEB;
		}
		return deviceType;
	}

	/**
	 * @param deviceType
	 * @return
	 * @Title: convertToIawebDeviceType、
	 * @Description:转换成iaweb所需要的设备类型
	 * @return: int
	 */
	public static int convertToIawebDeviceType(int deviceType) {
		if (deviceType == MeixConstants.APP_IOS) {
			deviceType = 3;
		} else if (deviceType == MeixConstants.APP_ANDROID) {
			deviceType = 1;
		}
		return deviceType;
	}

	public static int getDeviceType(JSONObject json) {
		String token = MapUtils.getString(json, "token");
		int deviceType = MeixConstants.APP_IOS;
		if (!StringUtils.isEmpty(token)) {
			deviceType = MobileassistantUtil.getDeviceType(token);
		}
		return deviceType;
	}

	public static int getDeviceTypeFromOs(String os) {
		os = os.toUpperCase();
		if (os.indexOf("ANDROID") > -1) {
			return MeixConstants.APP_ANDROID;
		} else if (os.indexOf("OS") > -1) {
			return MeixConstants.APP_IOS;
		}
		return 0;
	}

	/**
	 * @param request
	 * @return
	 * @Title: getToken、
	 * @Description:获取TOKEN
	 * @return: String
	 */
	public static String getToken(HttpServletRequest request) {
		String clientstr = request.getParameter("clientstr");
		JSONObject jsonobject = null;
		try {
			jsonobject = JSONObject.fromObject(clientstr);
		} catch (Exception e) {
			throw new RuntimeException("clientstr异常:" + clientstr);
		}
		if (jsonobject == null || jsonobject.isNullObject()) {
			return "";
		}
		return MapUtils.getString(jsonobject, "token", "");
	}

	/**
	 * @param request
	 * @return
	 * @Title: getTempToken、
	 * @Description:获取临时Token
	 * @return: String
	 */
	public static String getTempToken(HttpServletRequest request) {
		String clientstr = request.getParameter("clientstr");
		JSONObject jsonobject = null;
		try {
			jsonobject = JSONObject.fromObject(clientstr);
		} catch (Exception e) {
			throw new RuntimeException("clientstr异常:" + clientstr);
		}
		if (jsonobject == null || jsonobject.isNullObject()) {
			return "";
		}
		return MapUtils.getString(jsonobject, "tempToken", "");
	}

	/**
	 * @param request
	 * @return
	 * @Title: getUserId、
	 * @Description:无token时直接获取UID
	 * @return: long
	 */
	public static long getUserId(HttpServletRequest request) {
		String clientstr = request.getParameter("clientstr");
		JSONObject jsonobject = null;
		try {
			jsonobject = JSONObject.fromObject(clientstr);
		} catch (Exception e) {
			throw new RuntimeException("clientstr异常:" + clientstr);
		}
		if (jsonobject.isNullObject()) {
			return 0;
		}
		return MapUtils.getLongValue(jsonobject, "uid", 0);
	}

	/**
	 * @param request
	 * @return
	 * @Title: getDeviceId、
	 * @Description:获取设备标识
	 * @return: String
	 */
	public static String getDeviceId(HttpServletRequest request) {
		String token = getToken(request);
		if (StringUtils.isEmpty(token)) {
			String tempToken = getTempToken(request);
			if (StringUtils.isEmpty(tempToken)) {
				return null;
			}
			return tempToken;
		}
		String str = Des.decrypt(token);
		if (StringUtils.isBlank(str)) {
			return null;
		}
		String[] array = str.split(MeixConstants.tokenSplite);
		if (array.length <= 9) {
			return null;
		}
		return array[9];
	}

	public static String getDeviceId(String token) {
		if (StringUtils.isEmpty(token)) {
			return null;
		}
		String str = Des.decrypt(token);
		if (StringUtils.isBlank(str)) {
			return "";
		}
		String[] array = str.split(MeixConstants.tokenSplite);
		//String[] array=str.split(MeixConstants.tokenSplite);
		if (array == null || array.length <= 9) {
			return null;
		}
		return array[9];
	}

	/**
	 * 判断字符是否是中文
	 *
	 * @param c 字符
	 * @return 是否是中文
	 */
	public static boolean isChinese(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
			|| ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
			|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
			|| ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
			|| ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
			|| ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
			return true;
		}
		return false;
	}

	/**
	 * 判断字符串是否是乱码
	 *
	 * @param strName 字符串
	 * @return 是否是乱码
	 */
	public static boolean isMessyCode(String strName) {
		Pattern p = Pattern.compile("\\s*|\t*|\r*|\n*");
		Matcher m = p.matcher(strName);
		String after = m.replaceAll("");
		String temp = after.replaceAll("\\p{P}", "");
		char[] ch = temp.trim().toCharArray();
		float chLength = ch.length;
		float count = 0;
		for (int i = 0; i < ch.length; i++) {
			char c = ch[i];
			if (!Character.isLetterOrDigit(c)) {
				if (!isChinese(c)) {
					count = count + 1;
				}
			}
		}
		float result = count / chLength;
		if (result > 0.4) {
			return true;
		} else {
			return false;
		}

	}

	//生成uuid
	public static String generateUUID() {
		String s = "Research_" + UUID.randomUUID().toString();
		return s;
	}

	/**
	 * @param activityType
	 * @return
	 * @Title: isActivity、
	 * @Description:是否是活动
	 * @return: boolean
	 */
	public static boolean isActivity(int activityType) {
		return (activityType == MeixConstants.CHAT_MESSAGE_DY || activityType == MeixConstants.CHAT_MESSAGE_DHHY ||
			activityType == MeixConstants.CHAT_MESSAGE_LY ||
			activityType == MeixConstants.CHAT_MESSAGE_CL
			|| activityType == MeixConstants.CHAT_MESSAGE_HY);
	}

	/**
	 * @param messageType
	 * @return
	 * @Title: isSaveShareLog、
	 * @Description:是否保存分享日志
	 * @return: boolean
	 */
	public static boolean isSaveShareLog(int messageType) {
		return (messageType == MeixConstants.CHAT_MESSAGE_ZH || messageType == MeixConstants.CHAT_MESSAGE_GD || messageType == MeixConstants.CHAT_MESSAGE_TC
			|| messageType == MeixConstants.CHAT_MESSAGE_DY || messageType == MeixConstants.CHAT_MESSAGE_DHHY ||
			messageType == MeixConstants.CHAT_MESSAGE_LY
			|| messageType == MeixConstants.CHAT_MESSAGE_CL
			|| messageType == MeixConstants.CHAT_MESSAGE_HY);
	}
//	public static void main(String[] args) throws Exception {
//		File imageFile = new File("D:\\tmp\\test2\\tdx_jt.jpg");
//		FileInputStream in=new FileInputStream(imageFile);
//		ByteArrayOutputStream out=new ByteArrayOutputStream();
//		byte[] buff=new byte[1024];
//		int len=0;
//		while((len=in.read(buff))>0){
//			out.write(buff, 0, len);
//		}
//		in.close();
//		out.close();
//		System.out.println(RSAUtil.encryptBASE64(out.toByteArray()));
//		long startTime=System.currentTimeMillis();
//		BufferedImage image = ImageIO.read(new FileInputStream("D:\\tmp\\test2\\1_jt.jpg"));
//		// 这里对图片黑白处理,增强识别率.这里先通过截图,截取图片中需要识别的部分
//		BufferedImage textImage = ImageHelper.convertImageToGrayscale(ImageHelper.getSubImage(image, 0, 0, image.getWidth(), image.getHeight()));
////		// 图片锐化,自己使用中影响识别率的主要因素是针式打印机字迹不连贯,所以锐化反而降低识别率
////		 textImage = ImageHelper.convertImageToBinary(textImage);
////		// 图片放大5倍,增强识别率(很多图片本身无法识别,放大5倍时就可以轻易识,但是考滤到客户电脑配置低,针式打印机打印不连贯的问题,这里就放大5倍)
//		textImage = ImageHelper.getScaledInstance(textImage, image.getWidth() * 5, image.getHeight() * 5);
//		Tesseract instance=new Tesseract();  // JNA Interface Mapping
//		instance.setDatapath("E:\\Program Files (x86)\\Tesseract-OCR");
////		instance.setTessVariable("tessedit_char_whitelist", "0123456789");
//		instance.setLanguage("chi_sim");
//		String result = instance.doOCR(textImage);
////		Pattern pattern=Pattern.compile("^(.*)[0-9]{6}(.*)$");
////		Matcher match=pattern.matcher(result);
////		int size=match.groupCount();
////		int start=0;
////		for(int i=0;i<size;i++){
////			if(match.find(start)){
////				start=match.end()-1;
////				System.out.println(match.group());
////			}
////		}
//		String[] numArray=result.split("[^0-9]+");
//		for(int i=0;i<numArray.length;i++){
//			if(numArray[i].length()==6){
//				System.out.println(numArray[i]);
//			}
//		}
////		System.out.println(result);
//		System.out.println("耗时:"+(System.currentTimeMillis()-startTime));
//	}

	public static boolean isHttpStatusOK(String url) {
		HttpURLConnection connection = null;
		try {
			URL address_url = new URL(url);
			connection = (HttpURLConnection) address_url.openConnection();

			//after JDK 1.5
			connection.setConnectTimeout(10000);
			connection.setReadTimeout(10000);

			//得到访问页面的返回值
			int response_code = connection.getResponseCode();
			if (response_code == HttpURLConnection.HTTP_OK) {
				return true;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return false;
	}

	/**
	 * @param message
	 * @param e
	 * @Title: error、
	 * @Description:记录错误
	 * @return: void
	 */
	public static void error(String message, Exception e) {
		log.error(message, e);
	}

	/**
	 * @param initSqlx 设置默认类型
	 * @author zhangzhen(J)
	 * @date 2016年6月2日上午10:13:54
	 * @Description: 获取默认的打赏列表 -1代表其他
	 */
	public static List<Map<String, Object>> getRewardCompanyTypeList(boolean initSqlx) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		Map<String, Object> typeMap;

		for (int type : MeixConstants.companyTypeMap.keySet()) {
			//阳光私募、券商资管、券商研究所默认开通
			if (type == 16 || type == 3 || type == 8) {
				typeMap = new HashMap<String, Object>();
				typeMap.put("gslx", type);//公司类型
				typeMap.put("gslxms", MeixConstants.companyTypeMap.get(type));//公司类型描述
				if (initSqlx) {
					typeMap.put("sqlx", 2);//授权类型  1普通授权，2打赏授权
				} else {
					typeMap.put("sqlx", 1);//授权类型  1普通授权，2打赏授权
				}
				list.add(typeMap);
			} else if (type == 2 || type == 1 || type == 80) {//保险资管、公募基金默认关闭
				typeMap = new HashMap<String, Object>();
				typeMap.put("gslx", type);//公司类型
				typeMap.put("gslxms", MeixConstants.companyTypeMap.get(type));//公司类型描述
				typeMap.put("sqlx", 1);//授权类型  1普通授权，2打赏授权
				list.add(typeMap);
			}
		}
		//其余的选项归为其他，其他默认开通打赏授权
		typeMap = new HashMap<String, Object>();
		typeMap.put("gslx", -1);//公司类型
		typeMap.put("gslxms", "其他");//公司类型描述
		if (initSqlx) {
			typeMap.put("sqlx", 2);//授权类型  1普通授权，2打赏授权
		} else {
			typeMap.put("sqlx", 1);//授权类型  1普通授权，2打赏授权
		}
		list.add(typeMap);
		return list;
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年6月2日下午1:30:44
	 * @Description: 公司类型转换为打赏类型
	 */
	public static int getRewardType(int companyType) {
		if (companyType == 16 || companyType == 3 || companyType == 8 || companyType == 2 || companyType == 1) {
			return companyType;
		} else {
			return -1;//其他类型
		}
	}

	/**
	 * @param rankField
	 * @return
	 * @Title: getCombRankFieldDesc、
	 * @Description:获取组合排行字段描述
	 * @return: String
	 */
	public static String getCombRankFieldDesc(String rankField) {
		return MeixConstants.getCombRankMap().get(rankField);
	}

	public static void main(String[] args) {
		String cacheDeviceId = getDeviceId("F91AAD5084E787E8DB8C1E8BB988C3CC40350DF3B562F01DA09C07D2B425741497F6FEEF2D10DEEEE7362361EC353C799D72B3EA2FB74795ADCEBCD9221DBBABA6419D62A0E19BCB431C958FC945A8FA1970216E1C61985C81ABAAB0C074A6F3F7C91B26C900194D41061B2A52C6E1F29B881402B5F058D742471FF571080AED");
		System.out.println(cacheDeviceId);
		System.out.println("null".equalsIgnoreCase(cacheDeviceId));
		String deviceId = "123";
		if ((StringUtils.isEmpty(cacheDeviceId) || "null".equalsIgnoreCase(cacheDeviceId)) && !StringUtils.isEmpty(deviceId)) {
			System.out.println("--------------------");
		}
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年6月30日上午11:10:43
	 * @Description: 积分明细描述
	 */
	public static String getIntegralContent(int dataType) {
		String content = "";
		switch (dataType) {
			case MeixConstants.INTEGRAL_ZC:
				content = "成功注册";
				break;
			case MeixConstants.INTEGRAL_YQ:
				content = "邀请并注册";
				break;
			case MeixConstants.INTEGRAL_QD:
				content = "每日签到";
				break;
			case MeixConstants.INTEGRAL_QDJL:
				content = "连续7天签到";
				break;
			case MeixConstants.INTEGRAL_TKZH:
				content = "偷看";
				break;
			case MeixConstants.INTEGRAL_XTJL:
				content = "系统奖励";
				break;
			default:
				break;
		}
		return content;
	}

	public static String getMobile(String mobile) {
		try {
			Long.parseLong(mobile);
		} catch (Exception e) {
			mobile = "";
		}
		return mobile;
	}
}
