package com.meix.institute.jms;

import com.meix.institute.api.IInsSyncRecReqService;
import com.meix.institute.api.IInsSyncSndReqService;
import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.impl.IaServiceImpl;
import com.meix.institute.pojo.InsSyncRecReqInfo;
import com.meix.institute.pojo.InsSyncSndReqInfo;
import com.meix.institute.pojo.SyncAction;
import com.meix.institute.util.ExceptionUtil;
import com.meix.institute.util.GsonUtil;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Consumer {

	@Value("${ins.defaultUser}")
	private String defaultUser;
	@Value("${ins.defaultPassword}")
	private String defaultPassword;
	@Value("${ins.companyCode}")
	private String companyCode;

	@Autowired
	private IMeixSyncService meixSyncService;
	@Autowired
	private IaServiceImpl iaServiceImpl;
	@Autowired
	private IInsSyncSndReqService insSyncSndReqService;
	@Autowired
	private IInsSyncRecReqService insSyncRecReqService;

	private static final Logger log = LoggerFactory.getLogger(Consumer.class);

	@SuppressWarnings({"rawtypes", "unchecked"})
	@JmsListener(destination = "${syncQueue}", containerFactory = "queueListenerFactory")
	public void receiveSyncQueue(String text) {

		SyncAction action = GsonUtil.json2Obj(text, SyncAction.class);
		InsSyncSndReqInfo info = insSyncSndReqService.selectByPrimaryKey(action.getId());
		info.setRetryCnt(1);
		try {
			String url = info.getMethod();
			String reqHeader = info.getReqHeader();
			String reqBody = info.getReqBody();
			Map head = GsonUtil.json2Map(reqHeader);
			Map<String, Object> body = new HashMap<String, Object>();
			body.put("clientstr", reqBody);
			JSONObject result = iaServiceImpl.synchrodata(url, head, body); // 调用同步接口
			if (MeixConstantCode.M_1008 == MapUtils.getIntValue(result, "messageCode", 0)) {
				info.setRemarks("成功");
				info.setState(MeixConstants.SYNC_STATE_SUCCESS);
			} else {
				info.setState(MeixConstants.SYNC_STATE_FAIL);
				info.setRemarks(result.getString("message"));
			}
		} catch (Exception e) {
			info.setState(MeixConstants.SYNC_STATE_FAIL);
			info.setRemarks(StringUtils.substring(ExceptionUtil.getFormatedMessage(e), 0, 499));
		}

		insSyncSndReqService.save(info, defaultUser);
	}

	@JmsListener(destination = "${third.meixRecReqQueue}", containerFactory = "queueListenerFactory")
	public void receiveMeixRecReqQueue(String text) {
//		SyncAction action = GsonUtil.json2Obj(text, SyncAction.class);
//		if (action == null) {
//			log.info("Meix无效的同步数据：{}", text);
//			return;
//		}
//		if (action.getId() == 0) {
//			log.info("Meix无效的同步数据：{}", text);
//			return;
//		}
//		InsSyncRecReqInfo syncReqRecord = insSyncRecReqService.selectByPrimaryKey(action.getId());
		InsSyncRecReqInfo syncReqRecord = GsonUtil.json2Obj(text, InsSyncRecReqInfo.class);
		if (syncReqRecord == null) {
			log.info("Meix无效的同步数据：{}", text);
			return;
		}
		syncReqRecord.setRetryCnt(1);
		syncReqRecord = meixSyncService.handleSndSyncAction(syncReqRecord);
		insSyncRecReqService.save(syncReqRecord, defaultUser);
	}
}
