package com.meix.institute.jms;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

// 消息生产者
@Component
public class Producer {

	@Autowired
	private JmsMessagingTemplate jmsMessagingTemplate;

	public void sendQueueMsg(String destinationName, String message) throws Exception {
		jmsMessagingTemplate.convertAndSend(new ActiveMQQueue(destinationName), message);
	}
}

