package com.meix.institute.job;

import com.meix.institute.api.IArticlePushService;
import com.meix.institute.api.IInstituteService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.pojo.InsArticleGroupInfo;
import com.meix.institute.pojo.InsArticleGroupSearch;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.WechatUtil;
import com.meix.institute.vo.company.CompanyWechatConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 微信公众号服务器token刷新
 */
@Component
@EnableScheduling
public class WechatTimer {

	final static Logger log = LoggerFactory.getLogger(WechatTimer.class);

	/**
	 * 研究所微信公众号配置信息
	 *
	 * @author likuan 2019年7月8日19:39:49
	 * key - appId
	 * value - config
	 */
	public static Map<String, CompanyWechatConfig> companyWechatConfig;

	@Autowired
	private Environment env;
	@Autowired
	private IInstituteService instituteService;
	@Autowired
	private IArticlePushService articlePushService;

	@PostConstruct
	public void initWechatConfig() {
		if (companyWechatConfig == null) {
			companyWechatConfig = new HashMap<>();
		}
		List<CompanyWechatConfig> configList = instituteService.getWechatConfig();
		if (configList != null && configList.size() > 0) {
			for (CompanyWechatConfig item : configList) {
				companyWechatConfig.put(item.getCompanyCode() + "_" + item.getAppId() + "_" + item.getClientType(), item);
			}
		}
	}

	/**
	 * 每个110分钟，更新一次access_token、jsapi_ticket
	 */
	@Scheduled(fixedRate = 110 * 60 * 1000)
	public void updateAccessTokenAndJsapiTicket() {
		if (!env.getProperty("spring.profiles.active").contains("local")) {
			initWechatConfig();
			for (String key : companyWechatConfig.keySet()) {
				CompanyWechatConfig config = companyWechatConfig.get(key);
				String accessToken = this.updateAccessToken(false, config);
				String ticket = null;
				if (accessToken != null &&
					(config.getClientType() == MeixConstants.WX_CLIENT_TYPE_2)) {
					ticket = this.updateJsapiTicket(false, accessToken, companyWechatConfig.get(key));
				}
				log.info("【updateAccessTokenAndJsapiTicket】 -- access_token(" + accessToken + ")|jsapi_ticket(" + ticket + ")更新完成");
			}
		}
	}

	/**
	 * 检测access_token并返回
	 *
	 * @return access_token String
	 * @author likuan 2019年7月8日19:24:50
	 */
	public String updateAccessToken(boolean isCheckHour, CompanyWechatConfig config) {
		return WechatUtil.updateAccessToken(config.getCompanyCode(), config.getAppId(), config.getAppSecret(), isCheckHour);
	}

	public String updateAccessToken(boolean isCheckHour, long companyCode, int clientType) {
		String key = companyCode + "_" + clientType;
		CompanyWechatConfig config = companyWechatConfig.get(key);
		if (config == null) {
			return null;
		}
		return updateAccessToken(isCheckHour, config);
	}

	/**
	 * 检测ticket并返回
	 */
	public String updateJsapiTicket(boolean isCheckHour, String accessToken, CompanyWechatConfig config) {
		return WechatUtil.updateJsapiTicket(config.getCompanyCode(), config.getAppId(), accessToken, isCheckHour);
	}
	
	/**
     * 每隔1分钟扫描是否存在微信文章推送
     */
    @Scheduled(cron = "0 0/2 * * * ?")
    public void timingPushWechatArticle() {
        Date _now = new Date();
        String publishTimeFm = DateUtil.formatDate(_now, DateUtil.dateFormat);
        String publishTimeTo = DateUtil.getMinuteDate(publishTimeFm, 2);
        InsArticleGroupSearch search = new InsArticleGroupSearch();
        search.setState(MeixConstants.ARTICLE_STATE_PRE);
        search.setPublishTimeFm(DateUtil.stringToDate(publishTimeFm));
        search.setPublishTimeTo(DateUtil.stringToDate(publishTimeTo));
        List<InsArticleGroupInfo> list = articlePushService.groupFind(search);
        for (InsArticleGroupInfo info : list) {
            try {
                articlePushService.groupPublish(info.getId(), info.getMediaId(), null, ""); 
            } catch (Exception e) {
                log.warn("文章id-【{}】推送异常：", info.getId(), e);
            }
        }
    }
}
