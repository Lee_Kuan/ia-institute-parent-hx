package com.meix.institute.job;

import com.meix.institute.api.IStatDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 数据中心统计定时任务
 * Created by zenghao on 2019/10/11.
 */
@Component
@EnableScheduling
public class DataStatJob {
	@Autowired
	private IStatDataService statDataService;

	@PostConstruct
	public void init() {
//		executeActivityRecordStatJob();
//		executeUserRecordStatJob();
//		executeResearchReportStatJob();
//		executeInsSubscribeAnalystJob();
//		executeInsJoinActivityJob();
//		executeInsListenActivityJob();
//		executeInsReadResearchJob();
	}

	/**
	 * 研究所电话会议数据阅读、分享次数统计
	 */
	@Scheduled(cron = "0 1/27 * * * ?")
	public void executeActivityRecordStatJob() {
		statDataService.executeActivityRecordStatJob();
	}

	/**
	 * 用户阅读、分享研究所电话会议次数统计
	 */
	@Scheduled(cron = "0 2/27 * * * ?")
	public void executeUserRecordStatJob() {
		statDataService.executeUserRecordStatJob();
	}

	/**
	 * 研究所研报数据阅读、分享次数统计(耗时较长，半小时执行一次)
	 */
	@Scheduled(cron = "0 1/29 * * * ?")
	public void executeResearchReportStatJob() {
		statDataService.executeResearchReportStatJob();
	}

	/**
	 * 研究所关注分析师商机
	 */
	@Scheduled(cron = "0 3/27 * * * ?")
	public void executeInsSubscribeAnalystJob() {
		statDataService.executeInsSubscribeAnalystJob();
	}

	/**
	 * 研究所参会商机
	 */
	@Scheduled(cron = "0 4/27 * * * ?")
	public void executeInsJoinActivityJob() {
		statDataService.executeInsJoinActivityJob();
	}

	/**
	 * 研究所收听电话会议商机
	 */
	@Scheduled(cron = "0 5/27 * * * ?")
	public void executeInsListenActivityJob() {
		statDataService.executeInsListenActivityJob();
	}

	/**
	 * 研究所阅读研报商机
	 */
	@Scheduled(cron = "0 6/27 * * * ?")
	public void executeInsReadResearchJob() {
		statDataService.executeInsReadResearchJob();
	}
}
