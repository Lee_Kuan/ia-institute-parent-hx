package com.meix.institute.job;

import com.meix.institute.api.*;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsSyncTableJsid;
import com.meix.institute.service.api.ISyncService;
import com.meix.institute.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 定时job
 *
 * @author Xueyc
 */
@Component
@EnableScheduling
public class TaskJob {

	@Value("${ins.defaultUser}")
	private String defaultUser;

	@Autowired
	private ITaskService taskService;
	@Autowired
	private ISyncService syncService;
	@Autowired
	private IUserService userService;
	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IXnSyncService bgSyncService;
	@Autowired
	private IConfigService configServiceImpl;
	@Autowired
	private IMeixSyncJobService meixSyncJobServiceImpl;
	@Autowired
	private IActivityService activityServiceImpl;
	@Autowired
	private IInsActivityService insActivityService;

	private static final Logger log = LoggerFactory.getLogger(TaskJob.class);

	/**
	 * 研究所：数据中心——生成热点数据
	 */
	@Scheduled(cron = "30 30 01 * * ?")
	public void generateHeatData() {
		long start = System.currentTimeMillis();
		log.info("generateHeatData——start");
		try {
			// 1.清空所有
			taskService.clearHeadData();
			// 2.美市
			long startId = 0;
			Map<String, Object> params = new HashMap<>(4);
			params.put("companyCode", 0);
			params.put("showNum", 1000);
			params.put("startId", startId);
			params.put("tempToken", "nozuonodie");
			syncService.syncHeatData(params);
			// 3.研究所内部
			taskService.executeHeatData();
			// 4.copy
			taskService.copyInstituteHeatData();
		} catch (Exception e) {
			log.warn("generateHeatData 异常：", e);
		}
		log.info("generateHeatData——end, 总耗时：{} 秒", (System.currentTimeMillis() - start) / 1000);
	}

	/**
	 * 用户画像分值统计
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
	public void executeLabelScoreStatJob() {
		long start = System.currentTimeMillis();
		userService.callLabelScoreUpdate();
		log.info("用户画像分值统计耗时{}秒", (System.currentTimeMillis() - start) / 1000);
	}

	/**
	 * 白名单同步To每市
	 */
	@Scheduled(cron = "0 0 6,12,18,0 * * ?")
	public void syncWhiteUserToMeixJob() {
		meixSyncJobServiceImpl.syncWhiteUserToMeix();
	}

	/**
	 * 研报同步To每市
	 */
	@Scheduled(cron = "0 0 6,12,18,0 * * ?")
	public void syncReportToMeixJob() {
		meixSyncJobServiceImpl.syncReportToMeix(false);
	}

	/**
	 * 用户画像同步
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
	public void executePersonaInfoSyncJob() {
		if (commonServerProperties.getEnv().contains("dev") || commonServerProperties.getEnv().contains("prod")) {
			long currentPage = 0;

			Map<String, Object> params = new HashMap<>(4);
			params.put("showNum", 500);
			params.put("currentPage", currentPage);
			params.put("isDeleted", 0);
			params.put("tempToken", "nozuonodie");

			syncService.syncPersonaInfoList(params);
		}
	}

	/**
	 * 电话会议提醒
	 */
	@Scheduled(cron = "0 */1 * * * ? ")
	public void activityStartRemindJob() {
		insActivityService.activityStartRemind(commonServerProperties.getCompanyCode());
	}

//	/**
//	 * 用户画像卡片推送
//	 */
//	@Scheduled(cron = "0 0 12 * * ?")
//	public void userLabelPushJob() {
//		userService.userLabelPush(commonServerProperties.getCompanyCode());
//	}

	/**
	 * 股票信息同步
	 */
	@Scheduled(cron = "0 0 1 * * ?")
	public void syncSecumainJob() {
		try {
			long startTime = System.currentTimeMillis();
			Map<String, Object> params = new HashMap<>(3);
			params.put("showNum", 1000);
			params.put("startId", 0);
			params.put("tempToken", "nozuonodie");

			syncService.syncSecumain(params);

			long time = System.currentTimeMillis() - startTime;
			log.info("同步证券信息列表耗时{}ms", time);
		} catch (Exception e) {
			log.error("同步证券信息列表失败", e);
		}
	}

	/**
	 * 贝格研报信息同步
	 */
	@Scheduled(cron = "0 0/20 * * * ?")
	public void syncBgReportJob() {
		try {
			long startTime = System.currentTimeMillis();
			InsSyncTableJsid syncRecord = new InsSyncTableJsid();
			syncRecord.setTableName(MeixConstants.SYNC_JS_TABLE_NAME_REPORT);
			bgSyncService.action(syncRecord, MeixConstants.CREATED_VIA_1, false, defaultUser);
			long time = System.currentTimeMillis() - startTime;
			log.info("同步携宁研报信息耗时{}ms", time);
		} catch (Exception e) {
			log.error("同步携宁研报信息失败", e);
		}
	}

	/**
	 * 贝格内部用户同步
	 */
	@Scheduled(cron = "0 0/20 * * * ?")
	public void syncBgUserJob() {
		try {
			long startTime = System.currentTimeMillis();
			InsSyncTableJsid syncRecord = new InsSyncTableJsid();
			syncRecord.setTableName(MeixConstants.SYNC_JS_TABLE_NAME_USER);
			bgSyncService.action(syncRecord, MeixConstants.CREATED_VIA_1, false, defaultUser);
			long time = System.currentTimeMillis() - startTime;
			log.info("同步携宁用户信息耗时{}ms", time);
		} catch (Exception e) {
			log.error("同步携宁用户信息失败", e);
		}
	}

	/**
	 * 用户消息配置推送
	 */
	@Scheduled(cron = "0 0/1 * * * ?")
	public void userMessageConfigPushJob() {
		configServiceImpl.userMessageConfigPushJob();
	}

	/**
	 * 同步参会人员
	 * 每15分钟
	 */
	@Scheduled(cron = "0 0/15 * * * ?")
	public void syncGenseeRecord() {
		String startDate = DateUtil.getDayDate(-3);
		activityServiceImpl.syncGenseeRecord(startDate, DateUtil.getCurrentDateTime(), 0);
	}

	/**
	 * 同步电话接入参会人员
	 * 每15分钟
	 */
	@Scheduled(cron = "0 0/15 * * * ?")
	public void syncTeleJoinRecord() {
		String startDate = DateUtil.getDayDate(-3);
		activityServiceImpl.syncTeleJoinRecord(startDate, DateUtil.getCurrentDateTime(), 0);
	}
}
