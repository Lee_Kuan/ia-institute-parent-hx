package com.meix.institute.job;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.meix.institute.api.IInsMonthlyGoldStockService;
import com.meix.institute.entity.InsCompanyMonthlyGoldStock;
import com.meix.institute.search.StockYieldSearchVo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.vo.stock.MonthlyGoldStockVo;

/**
 * 月度金股价格同步任务
 */
@Component
@EnableScheduling
public class StockPriceUpdateJob {
	final static Logger log = LoggerFactory.getLogger(StockPriceUpdateJob.class);


	@Autowired
	private IInsMonthlyGoldStockService goldStockService;
//	@Autowired
//	private ServerProperties serverProperties;

	private DateFormat monthFormat = new SimpleDateFormat("yyyy-MM");
//	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * 每天跟新股票价格信息
	 */
	@Scheduled(cron = "30 20 9-18 * * ?")
	public void updataStockPriceJob() {
//		if (serverProperties.getEnv().contains("local")) {
//			return;
//		}
		long startTime = System.currentTimeMillis();
		log.info("update gold stock month price start {}", startTime);
		Calendar cal = Calendar.getInstance();
		String month = monthFormat.format(cal.getTime());
//		Map<String, Object> params = new HashMap<>();
//		params.put("exright", -1);
//		params.put("count", 1);

		List<MonthlyGoldStockVo> stockList =
			goldStockService.getGoldStockMonthList(-1, 0, 0, month, -1);
		List<StockYieldSearchVo> searchList = new ArrayList<>();
		for (MonthlyGoldStockVo stock : stockList) {
			StockYieldSearchVo searchVo = new StockYieldSearchVo();
			searchVo.setInnerCode(stock.getInnerCode());
			searchVo.setStartDate(month + "-01");
			searchVo.setEndDate(DateUtil.getCurrentDate());
			searchList.add(searchVo);
		}
		
		Map<Integer, Map<String, BigDecimal>> priceMap = goldStockService.getStockPriceMap(searchList);
		if (priceMap == null) {
			log.error("获取股票行情失败!");
			return;
		}
		for (MonthlyGoldStockVo stock : stockList) {
			try {

				InsCompanyMonthlyGoldStock goldStock = new InsCompanyMonthlyGoldStock();

				goldStock.setId(stock.getId());
//				goldStock.setUpdatedAt(new Date());
//				String date = null;
//				JSONObject res = null;
				if (stock.getAddPrice() == null || new BigDecimal(0).compareTo(stock.getAddPrice()) == 0) {
//					params.put("start", month + "-01");
//					String url = serverProperties.getQuoteUrl() + "/candlestick/" + stock.getSecuCode();
//					String begin = HttpUtil.getHttp(url, params, 2000);
//					if (StringUtils.isNotBlank(begin)) {
//						JSONArray jsonArray = JSONArray.fromObject(begin);
//						if (jsonArray != null && jsonArray.size() > 0) {
//							res = jsonArray.getJSONObject(0);
//							date = MapUtils.getString(res, "date");
//							if ((month).equals(date.substring(0, 7))) {
//								double beginOpen = MapUtils.getDoubleValue(res, "open");
//								goldStock.setAddPrice(new BigDecimal(beginOpen));
//							}
//						}
//					}
					if (priceMap.containsKey(stock.getInnerCode()) && priceMap.get(stock.getInnerCode()).containsKey("startPrice")) {
						goldStock.setAddPrice(priceMap.get(stock.getInnerCode()).get("startPrice"));
					}
				}
//				String url = serverProperties.getQuoteUrl() + "/symbols;s=" + stock.getSecuCode();
//				byte[] buffer = HttpUtil.getHttp(url);
//				if (buffer != null) {
//					JSONArray jsonArray = JSONArray.fromObject(new String(buffer, "UTF-8"));
//					if (jsonArray != null && jsonArray.size() > 0) {
//						res = jsonArray.getJSONObject(0);
//						date = MapUtils.getString(res, "date");
//						if (dateFormat.format(cal.getTime()).equals(date)) {
//							double price = MapUtils.getDoubleValue(res, "price");
//							goldStock.setAddMonthPrice(new BigDecimal(price));
//						}
//					}
//				}

				if (priceMap.containsKey(stock.getInnerCode()) && priceMap.get(stock.getInnerCode()).containsKey("endPrice")) {
					goldStock.setAddMonthPrice(priceMap.get(stock.getInnerCode()).get("endPrice"));
				}
				
				goldStockService.updateGoldStockPrice(goldStock);
			} catch (Exception e) {
				log.error("获取股票{}行情失败", GsonUtil.obj2Json(stock));
				log.error("", e);
			}
		}
		log.info("update gold stock month price stop (ms){}", System.currentTimeMillis() - startTime);
	}

}
