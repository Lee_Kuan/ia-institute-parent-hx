package com.meix.institute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Created by zenghao on 2019/7/24.
 */
@EnableAsync
@SpringBootApplication
public class SyncApplication {
	public static void main(String[] args) {
		SpringApplication.run(SyncApplication.class, args);
	}
}
