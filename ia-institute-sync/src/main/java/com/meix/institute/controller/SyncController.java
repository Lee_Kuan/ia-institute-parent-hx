package com.meix.institute.controller;

import com.meix.institute.api.IActivityService;
import com.meix.institute.api.IXnSyncService;
import com.meix.institute.api.IMeixSyncJobService;
import com.meix.institute.api.ITaskService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.BaseResp;
import com.meix.institute.entity.InsSyncTableJsid;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.service.api.ISyncService;
import com.meix.institute.util.MobileassistantUtil;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zenghao on 2019/8/26.
 */
@RequestMapping(value = "/sync/")
@RestController
public class SyncController {
	private Logger log = LoggerFactory.getLogger(SyncController.class);

	@Value("${ins.companyCode}")
	private long companyCode;
	@Value("${ins.defaultUser}")
	private String defaultUser;

	@Autowired
	private ISyncService syncService;
	@Autowired
	private IXnSyncService bgSyncService;
	@Autowired
	private ITaskService taskService;
	@Autowired
	private IMeixSyncJobService meixSyncService;
	@Autowired
	private IActivityService activityServiceImpl;

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:证券信息列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "secumainList", method = RequestMethod.POST)
	public Object secumainList(HttpServletRequest request, HttpServletResponse response) {
		long startTime = System.currentTimeMillis();
		MessageInfo info = new MessageInfo();
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long startId = MapUtils.getLongValue(object, "startId", 0);

			Map<String, Object> params = new HashMap<>(3);
			params.put("showNum", 1000);
			params.put("startId", startId);
			params.put("tempToken", "nozuonodie");

			syncService.syncSecumain(params);

			long time = System.currentTimeMillis() - startTime;
			log.info("同步证券信息列表耗时{}ms", time);
			info.setMessage("同步证券信息列表耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取证券信息列表失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:热度列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "heatDataList", method = RequestMethod.POST)
	public Object heatDataList(HttpServletRequest request, HttpServletResponse response) {
		long startTime = System.currentTimeMillis();
		MessageInfo info = new MessageInfo();
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long startId = MapUtils.getLongValue(object, "startId", 0);

			Map<String, Object> params = new HashMap<>(4);
			params.put("companyCode", 0);
			params.put("showNum", 1000);
			params.put("startId", startId);
			params.put("tempToken", "nozuonodie");

			syncService.syncHeatData(params);

			long time = System.currentTimeMillis() - startTime;
			log.info("同步热度列表耗时{}ms", time);
			info.setMessage("同步热度列表耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取热度列表列表失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:常量列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "constantList", method = RequestMethod.POST)
	public Object constantList(HttpServletRequest request, HttpServletResponse response) {
		long startTime = System.currentTimeMillis();
		MessageInfo info = new MessageInfo();
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long startId = MapUtils.getLongValue(object, "startId", 0);

			Map<String, Object> params = new HashMap<>(3);
			params.put("showNum", 1000);
			params.put("startId", startId);
			params.put("tempToken", "nozuonodie");

			syncService.syncConstant(params);

			long time = System.currentTimeMillis() - startTime;
			log.info("同步常量列表耗时{}ms", time);
			info.setMessage("同步常量列表耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取常量列表失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:用户画像列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "personaInfoList", method = RequestMethod.POST)
	public Object personaInfoList(HttpServletRequest request, HttpServletResponse response) {
		long startTime = System.currentTimeMillis();
		MessageInfo info = new MessageInfo();
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long currentPage = MapUtils.getLongValue(object, "currentPage", 0);

			Map<String, Object> params = new HashMap<>(4);
			params.put("showNum", 500);
			params.put("currentPage", currentPage);
			params.put("isDeleted", 0);
			params.put("tempToken", "nozuonodie");

			syncService.syncPersonaInfoList(params);

			long time = System.currentTimeMillis() - startTime;
			log.info("同步用户画像列表耗时{}ms", time);
			info.setMessage("同步用户画像列表耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取用户画像列表失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:触发生成热度数据（研究所）
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "triggerHeatData", method = RequestMethod.GET)
	public MessageInfo triggerHeatData(HttpServletRequest request, HttpServletResponse response) {
		long startTime = System.currentTimeMillis();
		MessageInfo info = new MessageInfo();
		try {

			taskService.executeHeatData();

			long time = System.currentTimeMillis() - startTime;

			log.info("同步生成热度数据耗时{}ms", time / 1000);
			info.setMessage("同步生成热度数据耗时" + time / 1000 + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.warn("同步生成热度数据", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @return
	 * @Description:触发生成热度数据（美市+研究所）
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "triggerAllHeatData", method = RequestMethod.GET)
	public MessageInfo triggerAllHeatData() {
		MessageInfo info = new MessageInfo();
		try {
			long startTime = System.currentTimeMillis();

			// 1.清空所有
			taskService.clearHeadData();
			// 2.美市
			long startId = 0;
			Map<String, Object> params = new HashMap<>(4);
			params.put("companyCode", 0);
			params.put("showNum", 1000);
			params.put("startId", startId);
			params.put("tempToken", "nozuonodie");
			syncService.syncHeatData(params);
			// 3.研究所内部
			taskService.executeHeatData();
			// 4.copy
			taskService.copyInstituteHeatData();

			long time = System.currentTimeMillis() - startTime;
			log.info("同步生成热度数据耗时{}s", time / 1000);
			info.setMessage("同步生成热度数据耗时" + time / 1000 + "s");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.warn("同步生成热度数据", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 贝格同步员工（手动触发）（内部使用）
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "syncBgUser")
	public Object syncBgUser(HttpServletRequest request) {
		long startTime = System.currentTimeMillis();
		String isSyncAllParams = request.getParameter("isSyncAll");
		Boolean isSyncAll = false;
		if (StringUtils.isNotBlank(isSyncAllParams)) {
			isSyncAll = Boolean.valueOf(isSyncAllParams);
		}
		MessageInfo info = new MessageInfo();
		try {
			InsSyncTableJsid syncRecord = new InsSyncTableJsid();
			syncRecord.setTableName(MeixConstants.SYNC_JS_TABLE_NAME_USER);
			BaseResp resp = bgSyncService.action(syncRecord, MeixConstants.CREATED_VIA_2, isSyncAll, defaultUser);
			if (!resp.isSuccess()) {
				return resp;
			}
			long time = System.currentTimeMillis() - startTime;
			log.info("同步贝格员工耗时{}ms", time);
			info.setMessage("同步贝格员工耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("同步贝格员工失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 贝格同步分析师方向
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "syncBgDirection")
	public Object syncBgDirection(HttpServletRequest request) {
		long startTime = System.currentTimeMillis();
		MessageInfo info = new MessageInfo();
		try {
			InsSyncTableJsid syncRecord = new InsSyncTableJsid();
			syncRecord.setTableName(MeixConstants.SYNC_JS_TABLE_NAME_DIRECTION);
			BaseResp resp = bgSyncService.action(syncRecord, MeixConstants.CREATED_VIA_2, true, defaultUser);
			if (!resp.isSuccess()) {
				return resp;
			}
			long time = System.currentTimeMillis() - startTime;
			log.info("同步贝格分析师方向耗时{}ms", time);
			info.setMessage("同步贝格分析师方向耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("同步贝格分析师方向失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 贝格同步职位信息
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "syncBgStationInfo")
	public Object syncBgStationInfo(HttpServletRequest request) {
		long startTime = System.currentTimeMillis();
		MessageInfo info = new MessageInfo();
		try {
			InsSyncTableJsid syncRecord = new InsSyncTableJsid();
			syncRecord.setTableName(MeixConstants.SYNC_JS_TABLE_NAME_STATION);
			BaseResp resp = bgSyncService.action(syncRecord, MeixConstants.CREATED_VIA_2, true, defaultUser);
			if (!resp.isSuccess()) {
				return resp;
			}
			long time = System.currentTimeMillis() - startTime;
			log.info("同步贝格职位信息耗时{}ms", time);
			info.setMessage("同步贝格职位信息耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("同步贝格职位信息失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 同步贝格研报
	 *
	 * @return
	 */
	@RequestMapping(value = "syncBgReport", method = RequestMethod.GET)
	public Object syncBgReport(HttpServletRequest request, HttpServletResponse response) {
		long startTime = System.currentTimeMillis();
		String isSyncAllParams = request.getParameter("isSyncAll");
		Boolean isSyncAll = false;
		if (StringUtils.isNotBlank(isSyncAllParams)) {
			isSyncAll = Boolean.valueOf(isSyncAllParams);
		}
		MessageInfo info = new MessageInfo();
		try {
			InsSyncTableJsid syncRecord = new InsSyncTableJsid();
			syncRecord.setTableName(MeixConstants.SYNC_JS_TABLE_NAME_REPORT);
			BaseResp resp = bgSyncService.action(syncRecord, MeixConstants.CREATED_VIA_2, isSyncAll, defaultUser);
			if (!resp.isSuccess()) {
				return resp;
			}
			long time = System.currentTimeMillis() - startTime;
			log.info("同步贝格研报耗时{}ms", time);
			info.setMessage("同步贝格研报耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("同步贝格研报失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 研报同步至每市
	 *
	 * @return
	 */
	@RequestMapping(value = "syncMeixReport", method = RequestMethod.GET)
	public Object syncMeixReport(HttpServletRequest request, HttpServletResponse response) {
		long startTime = System.currentTimeMillis();
		String isSyncAllParams = request.getParameter("isSyncAll");
		Boolean isSyncAll = false;
		if (StringUtils.isNotBlank(isSyncAllParams)) {
			isSyncAll = Boolean.valueOf(isSyncAllParams);
		}
		MessageInfo info = new MessageInfo();
		try {
			meixSyncService.syncReportToMeix(isSyncAll);
			long time = System.currentTimeMillis() - startTime;
			log.info("同步研报耗时{}ms", time);
			info.setMessage("同步研报耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("同步研报失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 白名单同步至每市
	 *
	 * @return
	 */
	@RequestMapping(value = "syncMeixWhiteUser", method = RequestMethod.GET)
	public Object syncMeixWL(HttpServletRequest request, HttpServletResponse response) {
		long startTime = System.currentTimeMillis();
		MessageInfo info = new MessageInfo();
		try {
			meixSyncService.syncWhiteUserToMeix();
			long time = System.currentTimeMillis() - startTime;
			log.info("同步白名单耗时{}ms", time);
			info.setMessage("同步白名单耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("同步白名单失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 手动同步直播收听记录
	 *
	 * @return
	 */
	@RequestMapping(value = "syncGenseeRecordList", method = RequestMethod.GET)
	public Object syncGenseeRecordList(HttpServletRequest request, HttpServletResponse response,
	                                   @RequestParam(required = false) String startTime,
	                                   @RequestParam(required = false) String endTime,
	                                   @RequestParam(required = false) Long activityId
	) {
		long start = System.currentTimeMillis();
		if (activityId == null) {
			activityId = 0L;
		}
		MessageInfo info = new MessageInfo();
		try {
			activityServiceImpl.syncGenseeRecord(startTime, endTime, activityId);
			long time = System.currentTimeMillis() - start;
			log.info("同步直播收听记录耗时{}ms", time);
			info.setMessage("同步直播收听记录耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("同步直播收听记录失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 手动同步电话会议收听记录
	 *
	 * @return
	 */
	@RequestMapping(value = "syncTeleRecordList", method = RequestMethod.GET)
	public Object syncTeleRecordList(HttpServletRequest request, HttpServletResponse response,
	                                 @RequestParam(required = false) String startTime,
	                                 @RequestParam(required = false) String endTime,
	                                 @RequestParam(required = false) Long activityId
	) {
		long start = System.currentTimeMillis();
		if (activityId == null) {
			activityId = 0L;
		}
		MessageInfo info = new MessageInfo();
		try {
			activityServiceImpl.syncTeleJoinRecord(startTime, endTime, activityId);
			long time = System.currentTimeMillis() - start;
			log.info("同步电话会议收听记录耗时{}ms", time);
			info.setMessage("同步电话会议收听记录耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("同步电话会议收听记录失败", e);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
}
