package com.meix.institute.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meix.institute.impl.IaServiceImpl;

import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2019/7/24.
 */
@Controller
@RequestMapping(value = "/test")
public class IndexController {
    
    @Autowired
    protected IaServiceImpl iaServiceImpl;
    
	@RequestMapping(value = "/forwardout", method = {RequestMethod.GET})
	@ResponseBody
	public JSONObject forwardout() {
	    Map<String, Object> params = new HashMap<String, Object>();
	    params.put("tempToken", "nozuonodie");
        JSONObject resp = iaServiceImpl.httpsGetByTempToken("test/test.do", params, false, false, 10000);
		return resp;
	}
}
