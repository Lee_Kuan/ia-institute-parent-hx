package com.meix.institute.controller;

import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.MobileassistantUtil;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static com.meix.institute.constant.SyncConstants.ACTION_SAVE_ACTIVITY;

/**
 * Created by yingw on 2020/4/9.
 */
@Controller
public class TestController {

	@Autowired
	private IMeixSyncService meixSyncService;

	@RequestMapping(value = "test", method = RequestMethod.GET)
	@ResponseBody
	public Object dataRecordStatList(HttpServletRequest request, HttpServletResponse response) {
		long startTime = System.currentTimeMillis();
		MessageInfo info = new MessageInfo();
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long startId = MapUtils.getLongValue(object, "startId", 0);

			Map<String, Object> params = new HashMap<>(4);
			params.put("companyCode", 76);
			params.put("showNum", 1000);
			params.put("startId", startId);
			params.put("tempToken", "nozuonodie");

			meixSyncService.reqMeixSync(MeixConstants.USER_HD, ACTION_SAVE_ACTIVITY, params);

			long time = System.currentTimeMillis() - startTime;
			info.setMessage("同步耗时" + time + "ms");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

}
