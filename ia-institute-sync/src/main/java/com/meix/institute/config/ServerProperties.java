package com.meix.institute.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class ServerProperties {
	@Value("${spring.profiles.active}")
	private String env;

	@Value("${ins.companyCode}")
	private long companyCode; // 公司代码

	@Value("${ins.file.dir}")
	private String fileDir;

	@Value("${ins.file.server}")
	private String fileServer;

}
