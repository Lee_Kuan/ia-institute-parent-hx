package com.meix.institute.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description:mvc配置文件
 */
@Component
@ConfigurationProperties(prefix = "mvc")
public class MVC {

	/**
	 * 上传文件大小
	 */
	private long maxUploadSize;

	public long getMaxUploadSize() {
		return maxUploadSize;
	}

	public void setMaxUploadSize(long maxUploadSize) {
		this.maxUploadSize = maxUploadSize;
	}

}
