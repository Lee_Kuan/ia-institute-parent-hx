package com.meix.institute.config;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.util.UrlPathHelper;

/**
 * @Description:配置mvc
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.meix"})
public class MVCConfig implements WebMvcConfigurer, SchedulingConfigurer {
	@Autowired
	private MVC mvc;
	@Resource
	private UrlPathHelper versionUrlPathHelper;

	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {

		return new MappingJackson2HttpMessageConverter() {
		};
	}

	/*******************添加拦截器***************/
	@Override
	public void addInterceptors(InterceptorRegistry registry) {

	}

	/**
	 * @return
	 * @throws IOException
	 * @Title: multipartResolver、
	 * @Description:文件上传限定
	 * @return: CommonsMultipartResolver
	 */
	@Bean
	public CommonsMultipartResolver multipartResolver() throws IOException {
		CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
		commonsMultipartResolver.setMaxUploadSize(mvc.getMaxUploadSize());
		return commonsMultipartResolver;
	}

	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setScheduler(setTaskExecutors());
	}

	@Bean(destroyMethod = "shutdown")
	public Executor setTaskExecutors() {
		return Executors.newScheduledThreadPool(3); // 3个线程来处理。
	}
}
