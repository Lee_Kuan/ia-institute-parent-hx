package com.meix.institute.req.ctrl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.meix.institute.api.DataHandler;
import com.meix.institute.api.IInsSyncRecReqService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.config.HandlerContext;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.constant.SyncConstants;
import com.meix.institute.core.Key;
import com.meix.institute.jms.Producer;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.pojo.InsSyncRecReqInfo;
import com.meix.institute.util.Des;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.HttpUtil;
import com.meix.institute.util.StringUtils;

import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2020/4/8.
 */

/**
 * 每市数据同步
 */
@RestController
public class MeixDataSyncController {
	private Logger log = LoggerFactory.getLogger(MeixDataSyncController.class);
	@Value("${third.meixRecReqQueue}")
	public String meixRecReqQueue;
	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IInsSyncRecReqService insSyncRecReqService;
	@Autowired
	private Producer producer;
	@Autowired
	private HandlerContext handlerContext;
	@Value("${ins.pub-server.url}")
	private String pubServer;
	@Value("${ins.pub-server.mutiUploadFile}")
	private String mutiUploadFile;

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:数据同步
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/dataSync/syncAction", method = RequestMethod.POST)
	public MessageInfo syncAction(HttpServletRequest request, HttpServletResponse response,
	                              @RequestParam(required = false) String appId,
	                              @RequestParam(required = false) String sign,
	                              @RequestParam(required = false) int dataType,
	                              @RequestParam(required = false) String action,
	                              @RequestParam(required = false) String data
	) {
		long startTime = System.currentTimeMillis();
		MessageInfo info = new MessageInfo();
		try {
			if (!commonServerProperties.getMeixAppId().equals(appId)) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("无效的appId");
				return info;
			}
			String appSecret = commonServerProperties.getMeixAppSecret();
			//sign（appId_appSecret_data的md5）验证
			String validSign = StringUtils.md5(appId + "_" + appSecret + "_" + data);
			if (!validSign.equals(sign)) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("签名认证失败");
				return info;
			}
			//解密key(appSecret的md5前16位)
			String key = StringUtils.md5(appSecret).substring(0, 16);
			String dataDecrypt = Des.decrypt(data, key);
			JSONObject params = null;
			try {
				params = JSONObject.fromObject(dataDecrypt);
			} catch (Exception e) {
				log.error("", e);
			}
			if (params == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("参数解析失败");
				return info;
			}
			DataHandler handler = handlerContext.getHandler(dataType);
			if (handler == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("无效的同步操作");
				return info;
			}

			saveRecReq(17, action, dataType, params);
			info.setMessageCode(MeixConstantCode.M_1008);
			info.setMessage("数据接收成功");
		} catch (Exception e) {
			log.error("数据接收失败", e);
			info.setMessage("数据接收失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		log.info("同步数据接收耗时{}ms,action:{}", (System.currentTimeMillis() - startTime), action);
		return info;
	}

	/**
	 * 保存同步请求
	 *
	 * @param data
	 */
	private void saveRecReq(long companyCode, String action, int dataType, JSONObject data) throws Exception {
		long startTime = System.currentTimeMillis();
		// 保存接受请求记录
		InsSyncRecReqInfo req = new InsSyncRecReqInfo();
		Date _now = new Date();
		req.setSndApp(String.valueOf(companyCode));
		req.setRecApp(String.valueOf(commonServerProperties.getCompanyCode()));
		req.setMethod(action);
		req.setReqHeader("");
		req.setReqBody(data.toString());
		req.setState(MeixConstants.SYNC_STATE_NEW);
		req.setRetryMax(3);
		req.setRetryCnt(0);
		req.setDataType(dataType);
		req.setCreatedAt(_now);
		req.setUpdatedAt(_now);
		Key save = insSyncRecReqService.save(req, "");
		req.setId(save.getId());

		log.info("保存同步请求耗时{}ms，action：{}", (System.currentTimeMillis() - startTime), action);
//		SyncAction syncAction = new SyncAction();
//		syncAction.setId(save.getId());
		// 发送到队列
		producer.sendQueueMsg(meixRecReqQueue, GsonUtil.obj2Json(req));
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:数据同步
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/dataSync/syncFile", method = RequestMethod.POST)
	public MessageInfo syncFile(HttpServletRequest request, HttpServletResponse response,
	                            @RequestParam(value = "file", required = false) MultipartFile file,
	                            @RequestParam(value = "clientstr", required = false) String clientstr) {
		MessageInfo info = new MessageInfo();
		File tempFile = null;
		try {
			JSONObject obj = JSONObject.fromObject(clientstr);
			String appId = MapUtils.getString(obj, "appId");
			String data = MapUtils.getString(obj, "data");
			String sign = MapUtils.getString(obj, "sign");
			if (commonServerProperties.getMeixAppSecret().equals(appId)) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("无效的appId");
				return info;
			}
			String appSecret = commonServerProperties.getMeixAppSecret();
			//sign（appId_appSecret_data的md5）验证
			String validSign = StringUtils.md5(appId + "_" + appSecret + "_" + data);
			if (!validSign.equals(sign)) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("签名认证失败");
				return info;
			}
			JSONObject params = new JSONObject();
			params.put("tempToken", SyncConstants.TEMP_TOKEN);
			tempFile = new File(file.getOriginalFilename());
			FileOutputStream os = new FileOutputStream(tempFile, false);
			IOUtils.copy(file.getInputStream(), os);
			os.flush();
			os.close();
			JSONObject res = HttpUtil.httpPostFile(tempFile, pubServer + mutiUploadFile, params);
			Map<String, Object> map = new HashMap<>();
			String url = null;
			if (res != null) {
				if (res.getInt("messageCode") == 1008) {
					url = res.getJSONObject("object").getString("url");
					map.put("url", url);
				}
			}
			info.setObject(map);
			info.setMessageCode(MeixConstantCode.M_1008);
			info.setMessage("数据接收成功");
		} catch (Exception e) {
			log.error("数据接收失败", e);
			info.setMessage("数据接收失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		} finally {
			if (tempFile != null) {
				tempFile.deleteOnExit();
			}
		}
		return info;
	}
}
