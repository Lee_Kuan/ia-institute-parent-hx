###启动  
#!/bin/bash
moduleName="ia-institute-mng"
pidPath="./ia-institute-mng.pid"
jarfile="../$moduleName.jar"
nohup java -Xms256m -Xmx256m -Xss256k -server -jar $jarfile --spring.profiles.active=$1 > ./run.log 2>&1 &
echo "-----ia-institute-mng-------"
echo $! > $pidPath
