package com.meix.institute.service;

import com.meix.institute.config.PubServerConfig;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.util.DesIAServer;
import com.meix.institute.util.HttpUtil;
import com.meix.institute.util.StringUtil;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.GZIPInputStream;

/**
 * 负责和insWechat通信
 */
@Service
public class PubServiceImpl {

	protected final static Logger log = LoggerFactory.getLogger(PubServiceImpl.class);
	/*
	 * tempToken:redis key
	 */
	public static final String ia_temp_token_name = "tempToken";
	/*
	 * post|get params's token name
	 */
	private static final String ia_token_name = "token";
	/*
	 * http post|get params's key
	 */
	private static final String httpParamsKey = "clientstr";
	/**
	 * des key
	 * jiawj add 2016-04-15
	 */
	private static final String secretKey = "iaweb";

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private Set<String> logExcludedMap = new HashSet<String>() {
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;

		{
			add("user/pageSelfActionLog.do");
		}
	};

	@Autowired
	private PubServerConfig pubServerConfig;
	@Value("${spring.profiles.active}")
	private String env;

	public String httpPostFile(File file, String url, String token, int serviceType) {
		return httpPostFile(file, url, token, serviceType, true);
	}

	/**
	 * 发送表单文件流到ia-server
	 *
	 * @author jiawj 2016年7月1日 上午11:10:54
	 * @since 1.6.0
	 */
	public String httpPostFile(File file, String url, String token, int serviceType, boolean needRename) {
		JSONObject param = new JSONObject();
		param.accumulate(ia_token_name, token);
		param.accumulate("serviceType", serviceType);
		param.accumulate("flag", 2);
		param.accumulate("needRename", needRename);
		try {
			CloseableHttpClient client = HttpClientBuilder.create().build();
			String uri = this.pubServerConfig.getUrl() + url;
			log.info("mng转发文件到{}", uri);
			HttpPost postMethod = new HttpPost(uri);
			postMethod.setHeader("clientType", String.valueOf(MeixConstants.CLIENT_TYPE_1));
			postMethod.setHeader("appVersion", this.pubServerConfig.getAppVersion());

			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setSocketTimeout(30000).build();
			postMethod.setConfig(requestConfig);

			MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create()
				.setMode(HttpMultipartMode.RFC6532);    //文件中文名乱码
			multipartEntityBuilder.addTextBody("clientstr", param.toString());
			multipartEntityBuilder.addBinaryBody("file", file);
			HttpEntity httpEntity = multipartEntityBuilder.build();
			postMethod.setEntity(httpEntity);

			CloseableHttpResponse httpResponse = client.execute(postMethod);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
				StringBuffer buffer = new StringBuffer();
				String str = "";
				while (StringUtil.isNotEmpty(str = reader.readLine())) {
					buffer.append(str);
				}
				byte[] response = buffer.toString().getBytes();
				String result = new String(response);
				return result;
			} else {
				return null;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String httpPostFile(InputStream is, String url, String token, int serviceType, boolean needRename) {
		JSONObject param = new JSONObject();
		param.accumulate(ia_token_name, token);
		param.accumulate("serviceType", serviceType);
		param.accumulate("flag", 2);
		param.accumulate("needRename", needRename);
		try {
			CloseableHttpClient client = HttpClientBuilder.create().build();
			String uri = this.pubServerConfig.getUrl() + url;
			log.info("mng转发文件到{}", uri);
			HttpPost postMethod = new HttpPost(uri);
			postMethod.setHeader("clientType", String.valueOf(MeixConstants.CLIENT_TYPE_1));
			postMethod.setHeader("appVersion", this.pubServerConfig.getAppVersion());

			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setSocketTimeout(30000).build();
			postMethod.setConfig(requestConfig);

			MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create()
				.setMode(HttpMultipartMode.RFC6532);    //文件中文名乱码
			multipartEntityBuilder.addTextBody("clientstr", param.toString());
			multipartEntityBuilder.addBinaryBody("file", is);
			HttpEntity httpEntity = multipartEntityBuilder.build();
			postMethod.setEntity(httpEntity);

			CloseableHttpResponse httpResponse = client.execute(postMethod);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
				StringBuffer buffer = new StringBuffer();
				String str = "";
				while (StringUtil.isNotEmpty(str = reader.readLine())) {
					buffer.append(str);
				}
				byte[] response = buffer.toString().getBytes();
				String result = new String(response);
				return result;
			} else {
				return null;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private byte[] httpsGet(String url, Map<String, Object> params, boolean hasClientstr, int readTimeout) {
		if (hasClientstr) {
			StringBuffer newUrl = new StringBuffer(pubServerConfig.getUrl());
			newUrl.append(url + "?");
			newUrl.append(httpParamsKey + "=");
			newUrl.append(JSONObject.fromObject(params).toString());

			log.debug("【httpsGet】newUrl:{}", newUrl);
			return HttpUtil.getHttps_bytes(newUrl.toString(), this.addHeadParams("", 0), readTimeout);
		} else {
			StringBuffer newUrl = new StringBuffer(pubServerConfig.getUrl());
			if (!params.isEmpty()) {
				newUrl.append(url + "?");
				for (String key : params.keySet()) {
					newUrl.append(key + "=").append(params.get(key)).append("&");
				}
				newUrl = newUrl.deleteCharAt(newUrl.length() - 1);
			}


			log.debug("【httpsGet】newUrl:{}", newUrl);
			return HttpUtil.getHttps_bytes(newUrl.toString(), this.addHeadParams("", 0), readTimeout);
		}
	}

	public JSONObject httpsGetByTempToken(String url, Map<String, Object> params) {
		return httpsGetByTempToken(url, params, true, true, 0);
	}

	public JSONObject httpsGetByTempToken(String url, Map<String, Object> params, boolean encrypt, boolean hasClientstr, int readTimeout) {
		String tempToken = null;
		if (params.containsKey(ia_temp_token_name)) {
			tempToken = params.get(ia_temp_token_name).toString();
		} else {
			throw new RuntimeException(ia_temp_token_name + " is null.");
		}
		byte[] response = this.httpsGet(url, params, hasClientstr, readTimeout);

		if (response != null) {
			String temp = null;
			try {
				if (encrypt) {
					response = DesIAServer.decryptNoBase64(response, tempToken.substring(0, 8), 2);
					temp = uncompress(response);
				} else {
					temp = new String(response, "UTF-8");
				}
				if (!StringUtils.isEmpty(temp)) {
					if (env.contains(MeixConstants.ACTIVE_LOCAL) || env.contains(MeixConstants.ACTIVE_DEV)) {
						log.info("[httpsGetByTempToken]调用接口：{}，请求返回参数：{}", url, temp);
					}
					return JSONObject.fromObject(temp);
				}
			} catch (Exception e) {
				log.error("【httpsGetByTempToken】解析失败", e);
			}
			return null;
		}
		return null;
	}

	/**
	 * 分享到wechat的url请求数据接口
	 * 走tempToken方式
	 *
	 * @author jiawj 2016年6月2日 上午10:32:00
	 * @since 1.5.1
	 */
	public JSONObject httpPostByWehcat(Map<String, Object> params, String url) {
		return this.httpPostByTempToken(url, params, MeixConstants.TEMP_TOKEN, ia_temp_token_name);
	}

	/**
	 * @param params
	 * @param url
	 * @param decrypt 是否解密
	 * @return
	 */
	public JSONObject httpPostByWehcat(Map<String, Object> params, String url, boolean decrypt) {
		return this.httpPostByTempToken(url, params, MeixConstants.TEMP_TOKEN, ia_temp_token_name, decrypt);
	}

	/**
	 * 参与ia-server临时token方式加密传输
	 *
	 * @author jiawj 2016年6月1日 下午5:57:43
	 * @since 1.5.1
	 */
	private JSONObject httpPostByTempToken(String url, Map<String, Object> params, String tempToken, String tempTokenName) {
		return httpPostByTempToken(url, params, tempToken, tempTokenName, true);
	}

	/**
	 * 参与ia-server临时token方式加密传输
	 *
	 * @param decrypt 是否解密
	 * @author jiawj 2016年6月1日 下午5:57:43
	 * @since 1.5.1
	 */
	private JSONObject httpPostByTempToken(String url, Map<String, Object> params, String tempToken, String tempTokenName, boolean decrypt) {
		params.put(tempTokenName, tempToken);
		byte[] response = this.HttpPostByClientStr(url, params);
		if (response == null) {
			log.error("【httpPostByTempToken-HttpPostByClientStr】,response:{}", response);
			return null;
		}
		byte[] response2 = null;
		String result = null;
		if (decrypt) {
			//对数据进行解密操作
			response2 = DesIAServer.decryptNoBase64(response, tempToken.substring(0, 8), 2);
			if (response2 == null) {
				log.error("【httpPostByTempToken-decrypt】 decrypt is not success,params:{},response:{}", params.toString(), response);
			}
			result = uncompress(response2);
		} else {
			result = uncompress(response);
		}
		// 去除json字符串中的null字符
		result = formatResponse(result);

		if (env.contains(MeixConstants.ACTIVE_LOCAL) || env.contains(MeixConstants.ACTIVE_DEV)) {
			log.info("[httpPostByTempToken]调用接口：{}，请求返回参数：{}", url, result);
		}
		return JSONObject.fromObject(result);
	}

	/**
	 * 排除非打印log
	 *
	 * @param url
	 * @return
	 */
	private boolean checkLogExcludedMap(String url) {

		return logExcludedMap.contains(url);
	}

	/**
	 * 生成加密key（不同用户不同时间生成的不同）
	 *
	 * @return
	 * @author jiawj
	 * 2016年4月15日 下午12:47:49
	 */
	private String createEncryptKey(JSONObject key) {
		if (key.containsKey("encryptKey")) {
			return key.getString("encryptKey");
		}
		//1 get current date
		String[] date2 = dateFormat.format(new Date()).split("-");
//		String year = date2[0];
		String month = date2[1];
		String day = date2[2];

		StringBuffer temp = new StringBuffer("&");
		temp.append(secretKey).append(day + "," + month);

		//log.debug("Encrypt-key:"+temp.toString());
		return temp.substring(0, 8);
	}

	public String httpPostRequest(String url, Map<String, Object> params, boolean isDncrypt, Map<String, String> headParam) {
		JSONObject temp = (JSONObject) params.get("clientstr");
		String key = this.createEncryptKey(temp);

		if (headParam == null) {
			headParam = new HashMap<>();
		}
//		headParam.put("clientType", "2");
		if (!StringUtils.isEmpty(pubServerConfig.getAppVersion())) {
			headParam.put("appVersion", pubServerConfig.getAppVersion());
		}
		//TODO 头部增加MD5
		// ia-server user/passwordLogin.do
		if (isDncrypt && (
			url.indexOf("login/thirdLoginByOpenId") > 0 ||
				url.indexOf("user/login") > 0 ||
				url.indexOf("user/passwordLogin") > 0 ||
				url.indexOf("user/validationToken") > 0
		)
			) {
			temp.accumulate("x", key);

			params.remove("clientstr");
			params.put("clientstr", temp);
		} else {//除登录功能外其他请求都要求有token且合法
			if (!temp.containsKey(ia_token_name) || StringUtils.isEmpty(temp.getString(ia_token_name))) {
				isDncrypt = false;
			} else {
//				//token的解密秘钥要求未过期,否则返回秘钥过期代码1010	by:likuan  2019年7月16日15:15:19
//				String token = Des.decrypt(temp.getString(ia_token_name));
//				if (token == null || !token.endsWith(key)) {
//					JSONObject res = new JSONObject();
//					res.put("messageCode", 1010);
//					res.put("message", "账户信息过期，请重新登录！");
//					if ((env.contains(MeixConstants.ACTIVE_LOCAL) || env.contains(MeixConstants.ACTIVE_DEV))
//						&& !checkLogExcludedMap(url)) {
//						log.info("=========>token：{}账户信息过期!返回[1010]", token);
//					}
//					return res.toString();
//				}
			}
		}

		byte[] response = null;
		try {
			log.info("forward请求参数日志打印：【请求体】{}", JSONObject.fromObject(params).toString());
			log.info("forward请求参数日志打印：【请求头】{}", JSONObject.fromObject(headParam).toString());
			response = HttpUtil.post(url, params, headParam);
			if (response == null) {
				log.error("[httpPostRequest] 和服务端http通信返回byte is null");
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("[httpPostRequest] 和服务端http通信报错");
		}
		//对数据进行解密操作
		log.info("加密标志isDncrypt=【{}】", isDncrypt);
		if (isDncrypt) {
			response = DesIAServer.decryptNoBase64(response, key, 2);
		}
		String responseStr = uncompress(response);
		// 去除json字符串中的null字符
		responseStr = formatResponse(responseStr);
		if ((env.contains(MeixConstants.ACTIVE_LOCAL) || env.contains(MeixConstants.ACTIVE_DEV)) && !checkLogExcludedMap(url)) {
			log.info("[httpPostRequest]调用接口：{}，请求参数{}，请求返回参数：{}", url, params.toString(), responseStr);
		}
		return responseStr;
	}

	/**
	 * 处理ia-server http请求数据
	 *
	 * @param url
	 * @param params
	 * @return
	 * @author jiawj
	 * 2016年4月15日 上午9:17:52
	 */
	public String httpPostRequest(String url, Map<String, Object> params, boolean isDncrypt) {
		return httpPostRequest(url, params, isDncrypt, null);
	}

	/**
	 * forward 请求ia-server
	 *
	 * @author jiawj
	 * 2016年4月14日 下午5:18:04
	 */
	public JSONObject forwardBackend(String url, JSONObject data, boolean encrypt) {
		try {
			Map<String, Object> clientstrMap = new HashMap<>();
			clientstrMap.put("clientstr", data);
			String response = this.httpPostRequest(url, clientstrMap, encrypt);
			return JSONObject.fromObject(formatResponse(response));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 处理接口返回非规范json格式数据，null -> ""
	 *
	 * @param response
	 * @return
	 * @author jiawj
	 */
	private String formatResponse(String response) {
		if (StringUtils.isEmpty(response)) {
			return response;
		}
		return StringUtils.replace(response.replaceAll(":null", ":\"\""), "\"null\"", "null");
	}

	/**
	 * 给http增加header参数
	 *
	 * @param clientType
	 * @author jiawj
	 * 2016年4月26日 上午10:06:08
	 */
	private Map<String, String> addHeadParams(String appVersion, int clientType) {
		Map<String, String> result = new HashMap<>();
		if (StringUtils.isEmpty(appVersion)) {
			result.put("appVersion", pubServerConfig.getAppVersion());
		} else {
			result.put("appVersion", appVersion);
		}
		if (clientType > 0) {
			result.put("clientType", String.valueOf(clientType));
		}
		return result;
	}

	/**
	 * 参数组合成clientstr={json}格式
	 *
	 * @author jiawj 2016年6月2日 下午1:21:07
	 * @since 1.5.1
	 */
	private byte[] HttpPostByClientStr(String url, Map<String, Object> bodyParams) {
		Map<String, Object> tmpMap = new HashMap<String, Object>();
		tmpMap.put("clientstr", JSONObject.fromObject(bodyParams));
		String appVersion = MapUtils.getString(bodyParams, "appVersion", "");

		int clientType = 0;
		if (bodyParams.containsKey("token") && !"".equals(bodyParams.get("token"))) {

		} else {
			//TODO 待优化  3微信 4 公众号
			clientType = 3;
		}
		if (bodyParams.containsKey("appVersion") && !StringUtils.isEmpty(appVersion) && !"0".equals(appVersion)) {
			tmpMap.remove("appVersion");
			return HttpUtil.post(pubServerConfig.getUrl() + url, tmpMap, this.addHeadParams(appVersion, clientType));
		} else {
			return HttpUtil.post(pubServerConfig.getUrl() + url, tmpMap, this.addHeadParams("", clientType));
		}
	}

	/**
	 * 返回错误消息
	 *
	 * @param msg
	 * @return
	 */
	public static JSONObject errorMsg(String msg) {
		JSONObject obj = new JSONObject();
		obj.put("messageCode", MeixConstantCode.M_1009);
		obj.put("message", msg);
		obj.put("enabled", 1);
		return obj;
	}

	/**
	 * @return String
	 * @throws IOException
	 * @Title: uncompress、
	 * @Description:解压缩数据
	 */
	public String uncompress(byte[] data) {
		if (data == null) {
			return null;
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayInputStream in = null;
		GZIPInputStream ungzip = null;
		try {
			in = new ByteArrayInputStream(data);
			ungzip = new GZIPInputStream(in);
			byte[] buffer = new byte[1024];
			int n;
			while ((n = ungzip.read(buffer)) >= 0) {
				out.write(buffer, 0, n);
			}
			return new String(out.toByteArray(), "UTF-8");
		} catch (IOException e) {
			//e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (ungzip != null) {
				try {
					ungzip.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			return new String(data, "UTF-8");
		} catch (UnsupportedEncodingException e) {

		}
		return null;

	}

	/**
	 * 同步数据
	 *
	 * @param url
	 * @param head
	 * @param body
	 * @return
	 */
	public JSONObject synchrodata(String url, Map<String, String> head, Map<String, Object> body) {
		JSONObject result = new JSONObject();
		try {
			byte[] response = HttpUtil.post(pubServerConfig.getUrl() + url, body, head);
			if (response == null) {
				log.warn("【synchrodata】,response:{}", response);
				result.put("message", "【synchrodata】请求每市云端返回值为空");
				return result;
			}
			response = DesIAServer.decryptNoBase64(response, MeixConstants.TEMP_TOKEN.substring(0, 8), 2);
			if (response == null) {
				log.warn("【synchrodata】解密失败");
				result.put("message", "【synchrodata】解密失败");
				return result;
			}
			String resultStr = uncompress(response);
			// 去除json字符串中的null字符
			resultStr = formatResponse(resultStr);
			result = JSONObject.fromObject(resultStr);
		} catch (Exception e) {
			log.warn("调用同步数据接口异常：【{}】", e);
			result.put("message", "【synchrodata】调用同步数据接口异常:【" + e.getMessage() + "】");
		}
		return result;
	}
}
