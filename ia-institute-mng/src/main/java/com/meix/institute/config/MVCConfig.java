package com.meix.institute.config;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.util.UrlPathHelper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meix.institute.config.vo.MVC;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.interceptor.ThreadLocalInterceptor;
import com.meix.institute.interceptor.VerifyHandlerInterceptor;
import com.meix.institute.util.Des;

/**
 * @Description:配置mvc
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.meix"})
public class MVCConfig implements WebMvcConfigurer, SchedulingConfigurer {
	@Autowired
	private VerifyHandlerInterceptor verifyHandlerInterceptor;
	@Autowired
	private ThreadLocalInterceptor threadLocalInterceptor;
	@Autowired
	private MVC mvc;
	@Resource
	private UrlPathHelper versionUrlPathHelper;

	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {

		return new MappingJackson2HttpMessageConverter() {
		};
	}

	/*******************添加拦截器***************/
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		String[] verifyInclude = new String[mvc.getVerifyInclude().size()];
		verifyInclude = mvc.getVerifyInclude().toArray(verifyInclude);
		String[] verifyExclude = new String[mvc.getVerifyExclude().size()];
		verifyExclude = mvc.getVerifyExclude().toArray(verifyExclude);
		registry.addInterceptor(verifyHandlerInterceptor).addPathPatterns(verifyInclude).excludePathPatterns(verifyExclude);
		registry.addInterceptor(threadLocalInterceptor).addPathPatterns(verifyInclude);

	}

	/**
	 * @return
	 * @throws IOException
	 * @Title: multipartResolver、
	 * @Description:文件上传限定
	 * @return: CommonsMultipartResolver
	 */
	@Bean
	public CommonsMultipartResolver multipartResolver() throws IOException {
		CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
		commonsMultipartResolver.setMaxUploadSize(mvc.getMaxUploadSize());
		return commonsMultipartResolver;
	}

	@Override
	public void configureMessageConverters(
		List<HttpMessageConverter<?>> converters) {
		List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();

		supportedMediaTypes.add(MediaType.TEXT_HTML);
		supportedMediaTypes.add(new MediaType("application", "json", Charset.forName("UTF-8")));
		supportedMediaTypes.add(new MediaType("application", "vnd.spring-boot.actuator.v2+json", Charset.forName("UTF-8")));
		ObjectMapper customObjectMapper = new ObjectMapper();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		customObjectMapper.setDateFormat(dateFormat);

		MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = mappingJackson2HttpMessageConverter();
		mappingJackson2HttpMessageConverter.setSupportedMediaTypes(supportedMediaTypes);
		mappingJackson2HttpMessageConverter.setObjectMapper(customObjectMapper);
		converters.add(mappingJackson2HttpMessageConverter);

		StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
		List<MediaType> supportedMediaTypes2 = new ArrayList<MediaType>();

		supportedMediaTypes2.add(MediaType.TEXT_HTML);
		supportedMediaTypes2.add(new MediaType("text", "plain", Charset.forName("UTF-8")));
		stringHttpMessageConverter.setSupportedMediaTypes(supportedMediaTypes2);
		converters.add(stringHttpMessageConverter);

	}

	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		configurer.setUrlPathHelper(versionUrlPathHelper);
	}

	public static String getDeviceId(String token) {
		String str = Des.decrypt(token);
		String[] array = str.split(MeixConstants.tokenSplite);
		if (array.length <= 9) {
			return null;
		}
		return array[9];
	}

	public static void main(String[] args) throws Exception {
		String token = "37E381828078EFF3959249D6B97275CD892C069521BD74B53F64DE3885D89903882FA4AA7DEC3EF7E666775200195B2E169E349DC0D35C8C0948236083424C88177B292D3FD07DCD9526EA38341F26D4";
		String key = getDeviceId(token);
		key = key.substring(0, 8);
		String encrypt = "Iau6GEgCcJ14p+3KCEB9IdLEVtqdJI7eJNc6GnlgOkRXVisjzUGcNfKNsgryMtVghtOx9PmDNjXLjfPowiYLRjjcSnteIrGRNIRuSApkThu2M2vh5GNSjYbX3bj3nxlNbazDEkmoXyJZCzttSF0wAgtRuZ36Fz/rJtYwsFRDM4BB3tRVXVih3N3L6zG0TKuFgCGdR2BxrpPfmb/tr9FL+tSICO/DeyE5fATXOQHF5kSCKXvBTXJVatN86KhRbnTLVI9+d6uJAF6CTSoN1eGf9ISZYDVU7G+c3AZiKboNEdurKkiASQIgfgtBvYmShDZ3FMq670/4ACOGFiDeSpI/Yz7lUw/KalxsKFvwhoOtYYBLFPOVB8MqTBqWy3eJq9IyPPtc8YkzowkGc2H90RcwjK9Q8YkY0TmqWV2Ue16F8fjp+d2oW4KZl4uTMJvraPiAtt3dg0TnxByxxEHmjdYwKN6EIMvnCDSDs6aed6R4jXeQPXOoP9B6XMTZfsShEV7UINQFq9gdH9AXJmuuEieIUj6to4vUKSYl6eMKrfnr5OedeY6scDVusvxI7uPP3q8Y+4kRsMpZ/xza5dc0Plck6gMhcanm25Z99/U1ulWuJGxSyBmMx0qFV+4hSHq6AzaNdsnM+nt6WbndtrXotOHzat0PXH+QrS6tUUsBmGb9AH7U8L+nArDXBemMKxC2NoTVajyHFDTmsSce/XWZ6lndWG8nirLrzpdotedEk2RzWcXve4XL9YZzXGdUoL+XDVUt6OL07RCuFwiFxTanXVfrJxo2NQ9nZ6ELsa5dC8lEWv+j8SkizpLGdsI31dBWi9OoiZ69jVVfEt975SndvKiE4d+gZ+t988patkVT5O3SEY51f54Uvplq1LMCS9KEpdxPQb+dYZ99NlMpiIhq4RxBc4JdEfbYQLVYkb60yjS+QQdiGSLvGtk68jTuPIhUxujXx6t93WcoC0uHOXEiSXp3WPCV++Y7nFqxNiIdgpUMazQSwfyoEmhonMWLhJTC2eWlFhR+2oO0taS16xmGc5xtU45Ux1oC8AqChlFIeZxN84UAocbhoBKdk4NfT8Bh8UALJ2Eq5Jyz37MrEZ6IXdktuOn11m5MZrTKO9RlTez3arW7NC7X/eiHikCpk8nOlIGCa9SXsVqamQq8vHAF70asmN23dHDJM89ptD2ixfW+Gy4xfNEAA8tYrk1MC1Wpk0AeXU2FQrgMHRyiRKkxF2FCmjAVhA2vB+7qwcGDTVr+Htt6Un+FqTe3IpD1ccTgOXMQ";
		String des = Des.decryptNoBase64(encrypt, key, 2);
		System.out.println(des);
	}

	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setScheduler(setTaskExecutors());
	}

	@Bean(destroyMethod = "shutdown")
	public Executor setTaskExecutors() {
		return Executors.newScheduledThreadPool(3); // 3个线程来处理。
	}
}
