package com.meix.institute.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 服务器地址接口配置
 * Created by zenghao on 2019/9/17.
 */
@Component
@ConfigurationProperties(prefix = "ins.pub-server")
public class PubServerConfig {

	private String appVersion;
	private String url;

	private String mutiUploadFile;

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMutiUploadFile() {
		return mutiUploadFile;
	}

	public void setMutiUploadFile(String mutiUploadFile) {
		this.mutiUploadFile = mutiUploadFile;
	}
}
