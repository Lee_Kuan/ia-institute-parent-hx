package com.meix.institute.config;

import java.io.File;

import javax.servlet.MultipartConfigElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class FileUploadConfig {

	@Autowired
	private CommonServerProperties serverProperties;
	
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("150MB");
		factory.setMaxRequestSize("150MB");
		File dir = new File(serverProperties.getFileTmpdir());
		if (!dir.exists()) {
			dir.mkdirs();
		}
		factory.setLocation(serverProperties.getFileTmpdir());
		return factory.createMultipartConfig();
	}

	@Bean
	public CorsFilter corsFilter() {
		final UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
		final CorsConfiguration corsConfiguration = new CorsConfiguration();
		/*是否允许请求带有验证信息*/
		corsConfiguration.setAllowCredentials(true);
	    /*允许访问的客户端域名*/
		corsConfiguration.addAllowedOrigin("*");
        /*允许服务端访问的客户端请求头*/
		corsConfiguration.addAllowedHeader("*");
        /*允许访问的方法名,GET POST等*/
		corsConfiguration.addAllowedMethod("*");
		urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
		return new CorsFilter(urlBasedCorsConfigurationSource);
	}
}
