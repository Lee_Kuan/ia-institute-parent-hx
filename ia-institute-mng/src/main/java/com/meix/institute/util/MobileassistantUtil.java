package com.meix.institute.util;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.meix.institute.constant.MeixConstants;

/**
 * @类说明 : 移动投资研究助手工具类
 */
public final class MobileassistantUtil {

	public static Logger log = LoggerFactory.getLogger(MobileassistantUtil.class);

	/**
	 * @param token
	 * @return
	 * @Title: getUserId、
	 * @Description:获取Token中的用户ID
	 * @return: String
	 */
	public static long getUserId(String token) {
		if (StringUtils.isEmpty(token)) {
			return 0;
		}
		String str = Des.decrypt(token);
		if (StringUtils.isBlank(str)) {
			return 0;
		}
		return Long.parseLong(str.split(MeixConstants.tokenSplite)[0]);
	}

	/**
	 * @param request
	 * @return
	 * @Title: getRequestParams、
	 * @Description:获取请求参数
	 * @return: JSONObject
	 */
	public static JSONObject getRequestParams(HttpServletRequest request) {
		Object clientstr = request.getAttribute("clientstr");
		if (clientstr == null) {
			clientstr = request.getParameter("clientstr");
		}
		JSONObject obj = null;
		try {
			obj = JSONObject.fromObject(clientstr);
			if (obj != null) {
				String token = MapUtils.getString(obj, "token");
				if (!StringUtils.isEmpty(token)) {
					try {
						String str = Des.decrypt(token);
						if (StringUtils.isNotBlank(str)) {
							long uid = Long.parseLong(str.split(MeixConstants.tokenSplite)[0]);
							String user = str.split(MeixConstants.tokenSplite)[1];
							obj.put("uid", uid);
							obj.put("uuid", user);
						}
					} catch (Exception e) {
						log.warn("token【{}】解密失败", token);
					}
				}
				//设置最大查询数量showNum--100
				int maxNum = 100;
				if (obj.containsKey("showNum")) {
					int showNum = MapUtils.getIntValue(obj, "showNum");
					obj.put("showNum", showNum < maxNum ? showNum : maxNum);
				}
				//设置clientType
				obj.put("clientType", request.getHeader(MeixConstants.CLIENT_TYPE_HEAD));
			}
			return obj;
		} catch (Exception e) {
			log.warn("获取请求参数异常", e);
		}
		return obj;
	}

}
