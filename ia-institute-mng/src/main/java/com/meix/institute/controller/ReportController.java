package com.meix.institute.controller;

import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meix.institute.BaseService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IDictTagService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.IReportService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsUser;
import com.meix.institute.entity.ReportInfo;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.search.ComplianceListSearchVo;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.vo.report.ReportDetailVo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import oracle.sql.BLOB;

@Controller
@RequestMapping(value = "/report")
public class ReportController extends BaseService{

	private static final Logger log = LoggerFactory.getLogger(ReportController.class);

	@Autowired
	private IReportService reportServiceImpl;
	@Autowired
	private IDictTagService dictTagService;
	@Autowired
	private IInsUserService insUserServiceImpl;
//	@Autowired
//	private PdfConfig pdfConfig;
	@Autowired
	private ICustomerGroupService groupService;

	@RequestMapping(value = "/saveReport", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo saveReport(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
//			long uid = MapUtils.getLongValue(obj, "uid");
			long id = MapUtils.getLongValue(obj, "id", 0);
			int operatorType = id == 0 ? MeixConstants.OPERATOR_ADD : MeixConstants.OPERATOR_UPDATE;
			String uuid = MapUtils.getString(obj, "uuid", null);
			String title = MapUtils.getString(obj, "title", "");
			long companyCode = MapUtils.getLongValue(obj, "companyCode");
			String summary = MapUtils.getString(obj, "summary");
			int share = MapUtils.getIntValue(obj, "share", 1);
			int type = MapUtils.getIntValue(obj, "type", 0);
			int powerSubType = MapUtils.getIntValue(obj, "powerSubType", 0);

			/**    发布渠道			*/
			JSONArray issueType = null;
			if (obj.containsKey("issueType")) {
				issueType = obj.getJSONArray("issueType");
			}
			JSONArray groupList = null;
			if (share == 1 && obj.containsKey("groupList")) {
				groupList = obj.getJSONArray("groupList");
			}
//			UserInfo userInfo = userService.getUserInfoByUuid(uuid);
			InsUser userInfo = insUserServiceImpl.getUserInfo(uuid);
			ReportInfo report = new ReportInfo();
			report.setResearchId(id);
			report.setShare(share);
			report.setType(type);
			if (type == 1) { //会议纪要
				report.setTitle(title);
				report.setOrgCode(companyCode);
				report.setSummary(summary);
				report.setAuthNames(userInfo == null ? "" : userInfo.getUserName());
				report.setStatus(1);
				report.setPowerSubType(0);
				report.setDeepStatus(0);
			}
			
			long resId = id == 0 ? com.meix.institute.util.StringUtils.getId(companyCode) : id;
			
			int isPub = groupService.saveDataGroupShare(resId, MeixConstants.USER_YB, powerSubType, groupList);
			report.setShare(isPub);
			id = reportServiceImpl.saveReport(resId, uuid, report, null, issueType);
			
			Map<String, Long> meetingId = new HashMap<String, Long>();
			meetingId.put("id", id);
			info.setObject(meetingId);

			if (operatorType == MeixConstants.OPERATOR_UPDATE) {
				saveUserOperatorLog(uuid, id, MeixConstants.USER_YB, operatorType, 0, obj.toString());
			}
			info.setMessageCode(MeixConstantCode.M_1008);

			try {
				String clientstr = request.getParameter("clientstr");
				JSONObject clientstrObj = JSONObject.fromObject(clientstr);
				if (clientstrObj != null) {
					clientstrObj.put("id", id);
					request.setAttribute("clientstr", clientstrObj.toString());
				}
			} catch (Exception e) {
				log.error("", e);
			}
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}

		return info;
	}

	/**
	 * @Description: 研报详情
	 */
	@RequestMapping(value = "/getReportDetail", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getReportDetail(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

//			long uid = MapUtils.getLongValue(obj, "uid", 0);
			long researchId = obj.getLong("id");
			ReportDetailVo vo = reportServiceImpl.getReportDetail(researchId, true);
			if (vo != null) {
				vo.setInfoType(dictTagService.getText(MeixConstants.REPORT_TYPE, vo.getInfoType()));
			}
			info.setObject(vo);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:内容管理列表（研报 ）
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getReportAllList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getReportAllList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		info.setClientStyle(MeixConstants.MS_DIALOG);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String condition = MapUtils.getString(object, "condition", null);
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
			int status = MapUtils.getIntValue(object, "status", 1);
			int type = MapUtils.getIntValue(object, "type", 0);

			int sortType = MapUtils.getIntValue(object, "sortType", 0);    //排序列0-默认列（开始时间）
			int sortRule = MapUtils.getIntValue(object, "sortRule", -1); //排序规则 1 asc, -1 desc
			sortRule = sortRule == 0 ? -1 : sortRule;

			String sortField = MapUtils.getString(object, "sortField", null); //排序字段
			if (StringUtils.isBlank(sortField) || "publishDate".equalsIgnoreCase(sortField)) {
				sortType = 0;
			}
			int share = MapUtils.getIntValue(object, "share", -1);
			ComplianceListSearchVo searchVo = new ComplianceListSearchVo();
			List<Integer> statusList = new ArrayList<>();
			switch (status) {
				case -1:
//				statusList.add(1);
					break;
				case 0:
					statusList.add(0);
					break;
				case 1:
					statusList.add(1);
					break;
				case 2:
					statusList.add(2);
					break;
				case 3:
					statusList.add(3);
					break;
				default:
					info.setMessage("不支持的状态");
					info.setMessageCode(MeixConstantCode.M_1009);
					return info;
			}
			searchVo.setStatusList(statusList);
			searchVo.setCompanyCode(companyCode);
			searchVo.setCondition(condition);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			searchVo.setType(type);
			searchVo.setShare(share);
			List<ReportDetailVo> list = reportServiceImpl.getReportList(searchVo);
			for (ReportDetailVo reportDetailVo : list) {
				reportDetailVo.setInfoType(dictTagService.getText(MeixConstants.REPORT_TYPE, reportDetailVo.getInfoType()));
			}
			int total = reportServiceImpl.getReportCount(searchVo);
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("total", total);
			objectMap.put("currentPage", currentPage);
			info.setObject(objectMap);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取研报列表失败", e);
			info.setMessage("获取研报列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @Description: 保存合辑
	 */
	@RequestMapping(value = "/saveReportAlbum", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo saveReportAlbum(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

//			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String uuid = MapUtils.getString(obj, "uuid");
			long id = MapUtils.getLongValue(obj, "id", 0);
			int operatorType = id == 0 ? MeixConstants.OPERATOR_ADD : MeixConstants.OPERATOR_UPDATE;
			long companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
			String title = obj.getString("title");
			String subtit = MapUtils.getString(obj, "subtit", "");
			String coverLgUrl = MapUtils.getString(obj, "coverLgUrl", "");
			String coverLgName = MapUtils.getString(obj, "coverLgName", "");
			String coverSmUrl = MapUtils.getString(obj, "coverSmUrl", "");
			String coverSmName = MapUtils.getString(obj, "coverSmName", "");
			int catalog = MapUtils.getIntValue(obj, "catalog", 0);
			int no = MapUtils.getIntValue(obj, "no", 10000);
			int visible = MapUtils.getIntValue(obj, "visible", 1);
			int isTop = MapUtils.getIntValue(obj, "isTop", 0);
			int share = MapUtils.getIntValue(obj, "share", 1);
			String albumDesc = MapUtils.getString(obj, "albumDesc", "");
			String url = MapUtils.getString(obj, "url", "");
			int type = MapUtils.getIntValue(obj, "type", 1); //0-链接，1-富文本
			JSONArray subObj = null;
			if (obj.containsKey("submenu")) {
				subObj = obj.getJSONArray("submenu");
			}

			JSONArray researchObj = null;
			if (obj.containsKey("research")) {
				researchObj = obj.getJSONArray("research");
			}
			JSONArray issueType = null;
			if (obj.containsKey("issueType")) {
				issueType = obj.getJSONArray("issueType");
			}
			JSONArray groupList = null;
			if (share == 1 && obj.containsKey("groupList")) {
				groupList = obj.getJSONArray("groupList");
			}
			long resId = id == 0 ? com.meix.institute.util.StringUtils.getId(companyCode) : id;
			int isPub = groupService.saveDataGroupShare(resId, MeixConstants.REPORT_ALBUM, 0, groupList);
			share = isPub;
			reportServiceImpl.saveReportAlbum(resId, uuid, id, companyCode, title, subtit, albumDesc, coverLgUrl, coverLgName, coverSmUrl, coverSmName,
				catalog, no, visible, isTop, share, subObj, researchObj, issueType, url, type);
			saveUserOperatorLog(uuid, resId, MeixConstants.REPORT_ALBUM, operatorType, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:内容管理列表（研报合辑 ）
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getReportAlbumList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getReportAlbumList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		info.setClientStyle(MeixConstants.MS_DIALOG);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String condition = MapUtils.getString(object, "condition", null);
//			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
			int status = MapUtils.getIntValue(object, "status", -1);
			int type = MapUtils.getIntValue(object, "type", 0);
			String sortField = MapUtils.getString(object, "sortField", null);
			int sort = 0;
			if (StringUtils.isNotBlank(sortField)) {
				if ("sortField".equals(sortField)) {
					sort = 0;
				}
			}
			int sortRule = MapUtils.getIntValue(object, "sortRule", 0);
			String startTime = MapUtils.getString(object, "startDate", null);
			String endTime = MapUtils.getString(object, "endDate", null);
			int share = MapUtils.getIntValue(object, "share", -1);
			List<Map<String, Object>> list = reportServiceImpl.getReportAlbumList(type, -1, condition, startTime, endTime, showNum, currentPage, status, -1, sort, sortRule, share);
			int total = reportServiceImpl.getReportAlbumCount(type, -1, condition, startTime, endTime, status, share);
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("total", total);
			objectMap.put("currentPage", currentPage);
			info.setObject(objectMap);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取研报列表失败", e);
			info.setMessage("获取研报列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:研报合辑详情
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getReportAlbumDetail", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getReportAlbumDetail(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		info.setClientStyle(MeixConstants.MS_DIALOG);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			int type = MapUtils.getIntValue(object, "type", 0);
			long bid = MapUtils.getLongValue(object, "id");
			Map<String, Object> res = reportServiceImpl.getReportAlbumDetail(type, bid);
			info.setObject(res);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取研报列表失败", e);
			info.setMessage("获取研报列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 获取研报原文
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "getPdfStream", method = RequestMethod.GET)
	public void getRemoteFileStream(HttpServletRequest request, HttpServletResponse response) {
		JSONObject object = null;
//        InputStream is = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String fileUrl = MapUtils.getString(object, "fileUrl");
			if (StringUtils.isBlank(fileUrl)) {
				return;
			}
			fileUrl = URLDecoder.decode(fileUrl, "utf-8");
//			String pdfServer = pdfConfig.getPdfServer();
//			fileUrl = pdfServer + fileUrl;
//			log.info("获取研报原文地址：【{}】", fileUrl);
			long startTime = System.currentTimeMillis();
//			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(
//				pdfConfig.getPdfHost(), pdfConfig.getPdfUserName(), pdfConfig.getPdfPassword());
//			byte[] bytes = HttpUtil.smbGet(fileUrl, auth);
			BLOB res = reportServiceImpl.getReportFile(fileUrl);
			log.info("getblob[ms]:{}", System.currentTimeMillis() - startTime);
			if (res == null) {
				return;
			}
			ServletOutputStream os = response.getOutputStream();
			InputStream input = res.getBinaryStream();
            int len = (int) res.length();
            byte buffer[] = new byte[len];
			while ((len = input.read(buffer)) != -1) {
				os.write(buffer, 0, len);
			}
			os.flush();
			os.close();
			input.close();
			log.info("total[ms]:{}", System.currentTimeMillis() - startTime);
//            URL url = new URL(fileUrl);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            // 设置连接超时时间
//            conn.setConnectTimeout(3000);
//            if (conn.getResponseCode() == 200) {
//                is = conn.getInputStream();
//                ServletOutputStream os = response.getOutputStream();
//                IOUtils.copy(is, os);
//                os.flush();
//                os.close();
//            }
		} catch (Exception e) {
			log.warn("获取获取研报原文失败", e);
		} finally {
//            if (is != null) {
//                try {
//                    is.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
		}
	}

}
