package com.meix.institute.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.meix.institute.BaseService;
import com.meix.institute.api.IInsActivityService;
import com.meix.institute.api.IInsWhiteListService;
import com.meix.institute.api.IUserService;
import com.meix.institute.config.PubServerConfig;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsActivity;
import com.meix.institute.entity.InsActivityPerson;
import com.meix.institute.excel.ImportInstituteWhiteUser;
import com.meix.institute.excel.ImportJoinPerson;
import com.meix.institute.impl.ExcelExportService;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.service.PubServiceImpl;
import com.meix.institute.util.FileUtil;
import com.meix.institute.util.MobileassistantUtil;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 研究所白名单管理
 *
 * @author likuan
 *         2019年7月1日16:58:13
 */
@Controller
public class WhiteListController extends BaseService {

	private static final Logger log = LoggerFactory.getLogger(WhiteListController.class);
	@Autowired
	private IInsWhiteListService instituteServiceImpl;
	@Autowired
	private IInsActivityService insActivityServiceImpl;
	@Autowired
	private ExcelExportService excelExportService;
	@Autowired
	private PubServiceImpl pubService;
	@Autowired
	private PubServerConfig pubServerConfig;
	@Autowired
	private IUserService userServiceImpl;



	/**
	 * 保存白名单用户导入
	 */
	@RequestMapping(value = "/institute/saveWhiteUserList", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo saveUserList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo();
		JSONObject dataobj = null;
//		boolean addIaUser;
		try {
			dataobj = MobileassistantUtil.getRequestParams(request);

//			long uid = MapUtils.getLongValue(dataobj, "uid", 0);
			String uuid = MapUtils.getString(dataobj, "uuid", null);
			long groupId = MapUtils.getLongValue(dataobj, "groupId", 0);
//			if (groupId == 0) {
//				result.setMessage("用户组必填！");
//				result.setMessageCode(MeixConstantCode.M_1009);
//				return result;
//			}
			JSONArray datas = dataobj.getJSONArray("data");
			List<String> errInfo = instituteServiceImpl.saveWhiteUserInfo(uuid, datas, groupId);
			result.setData(errInfo);
			if (datas.size() <= errInfo.size() - 1) {
				result.setMessageCode(MeixConstantCode.M_1009);
				result.setMessage(errInfo.size() > 0 ? errInfo.get(0) : "未知错误！");
				return result;
			} else {
				saveUserOperatorLog(uuid, groupId, MeixConstants.USER_YH_WL, MeixConstants.OPERATOR_ADD, 0, dataobj.toString());
				result.setMessageCode(MeixConstantCode.M_1008);
			}
			try {
				String clientstr = request.getParameter("clientstr");
				JSONObject clientstrObj = JSONObject.fromObject(clientstr);
				if (clientstrObj != null) {
					clientstrObj.put("data", datas);
					request.setAttribute("clientstr", clientstrObj.toString());
				}
			} catch (Exception e) {
				log.error("", e);
			}
		} catch (Exception e) {
			result.setMessageCode(MeixConstantCode.M_1009);
			result.setMessage("未知错误！");
			log.error("", e);
		}
		return result;
	}

	/**
	 * 删除白名单用户
	 */
	@RequestMapping(value = "/institute/deleteWhiteUser", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo deleteWhiteUser(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo();
		JSONObject dataobj = null;
//		boolean addIaUser;
		try {
			dataobj = MobileassistantUtil.getRequestParams(request);

//			long uid = MapUtils.getLongValue(dataobj, "uid", 0);
			String uuid = MapUtils.getString(dataobj, "uuid", null);
			//用户所属研究所ID
			long companyCode = dataobj.getLong("companyCode");
			long groupId = MapUtils.getLongValue(dataobj, "groupId", 0);
			JSONArray ids = null;
			if (dataobj.containsKey("ids")) {
				ids = dataobj.getJSONArray("ids");
			}
			instituteServiceImpl.deleteWhiteUser(uuid, companyCode, ids, groupId);
			
			saveUserOperatorLog(uuid, groupId, MeixConstants.USER_YH_WL, MeixConstants.OPERATOR_DELETE, 0, dataobj.toString());
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			result.setMessageCode(MeixConstantCode.M_1009);
			result.setMessage("未知错误！");
			log.error(e.getMessage());
		}
		return result;
	}
	/**
	 * 白名单导入
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/institute/importWhiteUser")
	public @ResponseBody
	MessageInfo importWhiteUser(@RequestParam(value = "file") MultipartFile importFile, HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo();
		JSONObject obj = null;
		File file = null;
		try {
			
			if (importFile == null) {
				info.setMessage("读取文件失败");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String token = obj.getString("token");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);
			long groupId = MapUtils.getLongValue(obj, "groupId", 0);
//			if (groupId == 0) {
//				info.setMessage("用户组必填！");
//				info.setMessageCode(MeixConstantCode.M_1009);
//				return info;
//			}
			ImportParams importParams = new ImportParams();
			importParams.setSheetNum(1);//查询的sheet数量
			List<ImportInstituteWhiteUser> combEliteRank = ExcelImportUtil.importExcel(importFile.getInputStream(), ImportInstituteWhiteUser.class, importParams);

			String uuid = MapUtils.getString(obj, "uuid", null);
			//用户所属研究所ID
			JSONArray datas = JSONArray.fromObject(combEliteRank);
			
			XSSFWorkbook wb = new XSSFWorkbook(importFile.getInputStream());
			XSSFSheet sheet = wb.getSheetAt(0);
//			sheet.getRow(0).createCell(6).setCellValue("备注");
			instituteServiceImpl.saveWhiteUserInfo(uuid, datas, groupId, sheet);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			wb.write(outputStream);
			wb.close();
			file = FileUtil.createTempFile(outputStream.toByteArray(), String.valueOf(uid), importFile.getOriginalFilename());
			if (file != null && file.exists()) {
				System.out.println(file.getAbsolutePath());
				log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
			} else {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("文件导入有问题，请更换文件重试哦~");
				return info;
			}
			String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
			log.debug("【fileUpload】temp:{}", temp);
			if (temp != null) {
			
				JSONObject result = JSONObject.fromObject(temp);
				if (result.getInt("messageCode") == 1008) {
					JSONObject res = result.getJSONObject("object");
					info.setMessageCode(MeixConstantCode.M_1008);
					info.setObject(res);
					saveUserOperatorLog(uuid, groupId, MeixConstants.USER_YH_WL, MeixConstants.OPERATOR_IMPORT, 0, obj.toString() + datas.toString());
					return info;
				}
			}
			info.setMessage("模板错误，导入失败！");
			info.setMessageCode(MeixConstantCode.M_1009);
		} catch (Exception e) {
			e.printStackTrace();
			info.setMessage("模板错误，导入失败！");
			info.setMessageCode(MeixConstantCode.M_1009);
			return info;
		}finally {
			if (file != null && file.exists()) {
				file.delete();
			}
		}
		return info;
	}


	/**
	 * 活动代为报名
	 */
	@RequestMapping(value = "/institute/saveJoinActivityByAdmin", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo saveJoinActivityByAdmin(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			String uuid = MapUtils.getString(obj, "uuid");
			long activityId = MapUtils.getLongValue(obj, "activityId", 0);
			JSONArray datas = obj.getJSONArray("data");
			List<String> errInfo = instituteServiceImpl.saveJoinPerson(uuid, activityId, datas);
			result.setData(errInfo);
			if (datas.size() <= errInfo.size()) {
				result.setMessageCode(MeixConstantCode.M_1009);
				result.setMessage("保存失败");
				return result;
			} else {
				result.setMessageCode(MeixConstantCode.M_1008);
			}
			try {
				String clientstr = request.getParameter("clientstr");
				JSONObject clientstrObj = JSONObject.fromObject(clientstr);
				if (clientstrObj != null) {
					clientstrObj.put("data", datas);
					request.setAttribute("clientstr", clientstrObj.toString());
				}
			} catch (Exception e) {
				log.error("", e);
			}
		} catch (Exception e) {
			log.error("", e);
			result.setMessage("未知错误！");
			result.setMessageCode(MeixConstantCode.M_1009);
		}
		return result;
	}

	/**
	 * 活动报名导入
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/institute/importJoinPerson")
	public @ResponseBody
	MessageInfo imporJoinPerson(@RequestParam(value = "file") MultipartFile importFile, HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo();
		JSONObject obj = null;
		File file = null;
		try {
			
			if (importFile == null) {
				info.setMessage("读取文件失败");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String token = obj.getString("token");
			long activityId = obj.getLong("activityId");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);
			ImportParams importParams = new ImportParams();
			importParams.setSheetNum(1);//查询的sheet数量
			List<ImportJoinPerson> combEliteRank = ExcelImportUtil.importExcel(importFile.getInputStream(), ImportJoinPerson.class, importParams);

			String uuid = MapUtils.getString(obj, "uuid", null);
			//用户所属研究所ID
			JSONArray datas = JSONArray.fromObject(combEliteRank);
			
			XSSFWorkbook wb = new XSSFWorkbook(importFile.getInputStream());
			XSSFSheet sheet = wb.getSheetAt(0);
			instituteServiceImpl.saveJoinPerson(uuid, activityId, datas, sheet);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			wb.write(outputStream);
			wb.close();
			file = FileUtil.createTempFile(outputStream.toByteArray(), String.valueOf(uid), importFile.getOriginalFilename());
			if (file != null && file.exists()) {
				System.out.println(file.getAbsolutePath());
				log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
			} else {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("文件导入有问题，请更换文件重试哦~");
				return info;
			}
			String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
			log.debug("【fileUpload】temp:{}", temp);
			if (temp != null) {
			
				JSONObject result = JSONObject.fromObject(temp);
				if (result.getInt("messageCode") == 1008) {
					JSONObject res = result.getJSONObject("object");
					info.setMessageCode(MeixConstantCode.M_1008);
					info.setObject(res);
					return info;
				}
			}
			info.setMessage("模板错误，导入失败！");
			info.setMessageCode(MeixConstantCode.M_1009);
		} catch (Exception e) {
			e.printStackTrace();
			info.setMessage("模板错误，导入失败！");
			info.setMessageCode(MeixConstantCode.M_1009);
			return info;
		}finally {
			if (file != null && file.exists()) {
				file.delete();
			}
		}
		return info;
	}

	/**
	 * 获取参加人员列表
	 */
	@RequestMapping(value = "/compliance/getJoinPersonList", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getJoinPersonList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long activityId = obj.getLong("activityId");
			int showNum = MapUtils.getIntValue(obj, "showNum", 20);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			String condition = MapUtils.getString(obj, "condition");
			List<InsActivityPerson> list = instituteServiceImpl.getJoinPersonList(activityId, showNum, currentPage, condition, -1);
			int count = 0;
			if (list != null && list.size() > 0) {
				count = instituteServiceImpl.getJoinPersonCount(activityId, condition);
			}
			String title = instituteServiceImpl.getActivityTitle(activityId);
			Map<String, Object> map = new HashMap<>();
			map.put("title", title);
			map.put("total", count);
			map.put("currentPage", currentPage);
			result.setObject(map);
			result.setData(list);
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			result.setMessage("未知错误！");
			result.setMessageCode(MeixConstantCode.M_1009);
		}
		return result;
	}

	/**
	 * 导出excel
	 * 参加人员导出Excel
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/institute/exportJoinPersonList", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public MessageInfo exportJoinPersonExcel(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		File file = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long uid = MapUtils.getLongValue(obj, "uid");
			long activityId = obj.getLong("activityId");
			String condition = MapUtils.getString(obj, "condition");
			String token = obj.getString("token");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);
			InsActivity activity = insActivityServiceImpl.getActivity(activityId, 0);
			List<InsActivityPerson> list = instituteServiceImpl.getJoinPersonList(activityId, -1, 0, condition, -1);
			
			String excelName = activity.getTitle() + "_报名名单";
			
			Workbook wb = excelExportService.getJoinPersonExcel(excelName, list);
			if (wb == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("报表导出失败");
				return info;
			}
			if (RequestMethod.GET.toString().equals(request.getMethod())) {
				// 设置response头信息
				response.reset();
				response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
				try {
					response.setHeader("Content-disposition", "attachment; filename=" + excelName + ".xlsx");
				} catch (Exception e1) {
					log.info(e1.getMessage());
				}

				try {
					//将文件输出
					OutputStream ouputStream = response.getOutputStream();
					wb.write(ouputStream);
					ouputStream.flush();
					ouputStream.close();
					return null;
				} catch (Exception e) {
					log.info("导出Excel失败！");
					log.error("", e);
				}
			} else {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				wb.write(outputStream);
				byte[] data = outputStream.toByteArray();
				outputStream.close();
				file = FileUtil.createTempFile(data, String.valueOf(uid), excelName + ".xlsx");
				if (file != null && file.exists()) {
					log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
				} else {
					info.setMessageCode(MeixConstantCode.M_1009);
					info.setMessage("导出文件异常");
					return info;
				}
				String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
				log.debug("【fileUpload】temp:{}", temp);
				if (temp != null) {
					JSONObject result = JSONObject.fromObject(temp);
					String url = null;
					if (result.getInt("messageCode") == 1008) {
						url = result.getJSONObject("object").getString("url");
					}
	
					info.setMessageCode(MeixConstantCode.M_1008);
					JSONObject object = new JSONObject();
					object.put("fileUrl", url);
					info.setObject(object);
				}
			}
		} catch (Exception e) {
			info.setMessage("未知错误");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		} finally {
			if (file != null) {
				file.deleteOnExit();
			}
		}
		return info;
	}
	
	/**
	 * 导出excel
	 * 白名单导出Excel
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/institute/exportWhiteUserList", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public MessageInfo exportWhiteUserList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		File file = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String token = obj.getString("token");
			long uid = MapUtils.getLongValue(obj, "uid");
			String uuid = MapUtils.getString(obj, "uuid", "");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);
			String condition = MapUtils.getString(obj, "condition");
			long groupId = MapUtils.getLongValue(obj, "groupId", 0);
			List<Map<String, Object>> list =  userServiceImpl.getCustomerList(2, 3, condition, 0, -1, null, -1, groupId, 0);
			String excelName = "白名单";
			
			Workbook wb = excelExportService.getWhiteUserExcel(excelName, list);
			if (wb == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("报表导出失败");
				return info;
			}
			if (RequestMethod.GET.toString().equals(request.getMethod())) {
				// 设置response头信息
				response.reset();
				response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
				try {
					response.setHeader("Content-disposition", "attachment; filename=" + excelName + ".xlsx");
				} catch (Exception e1) {
					log.info(e1.getMessage());
				}

				try {
					//将文件输出
					OutputStream ouputStream = response.getOutputStream();
					wb.write(ouputStream);
					ouputStream.flush();
					ouputStream.close();
					return null;
				} catch (Exception e) {
					log.info("导出Excel失败！");
					log.error("", e);
				}
			} else {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				wb.write(outputStream);
				byte[] data = outputStream.toByteArray();
				outputStream.close();
				file = FileUtil.createTempFile(data, String.valueOf(uid), excelName + ".xlsx");
				if (file != null && file.exists()) {
					log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
				} else {
					info.setMessageCode(MeixConstantCode.M_1009);
					info.setMessage("导出文件异常");
					return info;
				}
				String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
				log.debug("【fileUpload】temp:{}", temp);
				if (temp != null) {
					JSONObject result = JSONObject.fromObject(temp);
					String url = null;
					if (result.getInt("messageCode") == 1008) {
						url = result.getJSONObject("object").getString("url");
					}
	
					info.setMessageCode(MeixConstantCode.M_1008);
					JSONObject object = new JSONObject();
					object.put("fileUrl", url);
					info.setObject(object);
					saveUserOperatorLog(uuid, groupId, MeixConstants.USER_YH_WL, MeixConstants.OPERATOR_EXPORT, 0, obj.toString());
				} else {
					info.setMessage("内部返回null");
					info.setMessageCode(MeixConstantCode.M_1009);
				}
			}
		} catch (Exception e) {
			info.setMessage("未知错误");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		} finally {
			if (file != null) {
				file.deleteOnExit();
			}
		}
		return info;
	}
}
