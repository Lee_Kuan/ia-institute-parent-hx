package com.meix.institute.controller;

import com.meix.institute.api.ISecumainService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.impl.cache.ServiceCache;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.RequestUtil;
import com.meix.institute.util.VersionUtil;

import net.sf.json.JSONObject;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/8/28.
 */
@RestController
public class SecuMainController {
	private static Logger log = LoggerFactory.getLogger(SecuMainController.class);

	@Autowired
	private ISecumainService secumainService;
	@Autowired
	private ServiceCache serviceCache;
	
	/**
	 * 获取股票
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/secuMain/getSecuMain", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getSecuMain(HttpServletRequest request, HttpServletResponse response) throws Exception {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String condition = obj.getString("condition").toUpperCase();
			int industryCode = MapUtils.getIntValue(obj, "industryCode", 0);
			industryCode = serviceCache.convertIndustryCode(industryCode);
			int type = MapUtils.getIntValue(obj, "type", 0);// 0全部  1股票
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int supportHK = MapUtils.getIntValue(obj, "supportHK", 1);
			long version = RequestUtil.getVersionValue(request);
			int secuType = supportHK;
			if (supportHK == 1) {
				if (version < VersionUtil.v2_5_0) {
					secuType = 0;
				}
			}
			//condition 过滤掉'，防止输入中文时报错
			condition = condition.replace("'", "");
			List<SecuMain> list = secumainService.getSecuMain(secuType, type, showNum, condition, industryCode);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("查询证券信息失败", e);
			info.setMessage("查询证券信息失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
	
	/**
	 * 获取Comps行业
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/secuMain/getCompsIndustry", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo getCompsIndustry(HttpServletRequest request, HttpServletResponse response) throws Exception {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String uuid = MapUtils.getString(obj, "uuid");
            String condition = MapUtils.getString(obj, "labelName");
            if (StringUtils.isNotBlank(condition)) condition = condition.toUpperCase().replace("'", "");
            Integer currentPage = MapUtils.getInteger(obj, "currentPage", 0);
            Integer showNum = MapUtils.getInteger(obj, "showNum", 10);
            
            Map<String, Object> search = new HashMap<String, Object>();
            search.put("labelName", condition);
            search.put("currentPage", currentPage * showNum);
            search.put("showNum", showNum);

            Paged<Map<String, Object>> paged = secumainService.getCompsIndustry(search, uuid);
            info.setData(paged.getList());
            info.setDataCount(paged.getCount());
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            log.warn("getCompsIndustry异常：", e);
            info.setMessageCode(MeixConstantCode.M_1009);
        }
        return info;
    }

}
