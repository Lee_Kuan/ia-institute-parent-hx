package com.meix.institute.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.meix.institute.BaseService;
import com.meix.institute.api.IHomeService;
import com.meix.institute.api.IRecommendInfoLogService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsBanner;
import com.meix.institute.entity.RecommenedInfoLog;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.vo.banner.InsBannerVo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2019/7/24.
 */
@RestController
@RequestMapping(value = "/index/")
public class IndexController extends BaseService {
	private static final Logger log = LoggerFactory.getLogger(IndexController.class);
	@Autowired
	private IRecommendInfoLogService recommendInfoLogService;
	@Autowired
	private IHomeService homeServiceImpl;

	@RequestMapping(value = "addLog", method = {RequestMethod.GET})
	@ResponseBody
	public Object addLog(@RequestParam String configId, @RequestParam String reqId) {
		RecommenedInfoLog recommendInfoLog = new RecommenedInfoLog();
		recommendInfoLog.setConfigid(configId);
		recommendInfoLog.setReqid(reqId);
		recommendInfoLogService.saveRecommendInfoLog(recommendInfoLog);
		return recommendInfoLog.getId();
	}

	@RequestMapping(value = "getLog", method = {RequestMethod.GET})
	@ResponseBody
	public Object getLog(@RequestParam Long id) {
		RecommenedInfoLog recommendInfoLog = recommendInfoLogService.findRecommendInfoLog(id);
		return GsonUtil.obj2Json(recommendInfoLog);
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:保存banner
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "saveBanner", method = RequestMethod.POST)
	public MessageInfo saveBanner(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String uuid = MapUtils.getString(obj, "uuid");
			String title = MapUtils.getString(obj, "title", "");
			String resourceUrl = MapUtils.getString(obj, "resourceUrl", "");
			String resourceName = MapUtils.getString(obj, "resourceName", "");
			int no = 0;
			int type = MapUtils.getIntValue(obj, "type", 0);
			long resourceId = MapUtils.getLongValue(obj, "resourceId", 0);
			String comment = MapUtils.getString(obj, "comment", "");
			InsBanner banner = new InsBanner();
			banner.setTitle(title);
			banner.setResourceUrl(resourceUrl);
			banner.setResourceName(resourceName);
			banner.setNo(no);
			banner.setType(type);
			banner.setResourceId(resourceId);
			banner.setComment(comment);
			banner.setCreatedAt(new Date());
			banner.setUpdatedAt(new Date());
			banner.setCreatedBy(uuid);
			banner.setUpdatedBy(uuid);
			homeServiceImpl.saveBanner(banner);
			
			saveUserOperatorLog(uuid, banner.getId(), MeixConstants.BANNER, MeixConstants.OPERATOR_ADD, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("新增banner失败");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:保存banner排序
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "saveBannerSort", method = RequestMethod.POST)
	public MessageInfo saveBannerSort(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String uuid = MapUtils.getString(obj, "uuid");
			JSONArray ids = null;
			if (obj.containsKey("ids")) {
				ids = obj.getJSONArray("ids");
			}
			homeServiceImpl.saveBannerSort(uuid, ids);
			saveUserOperatorLog(uuid, 0, MeixConstants.BANNER, MeixConstants.OPERATOR_UPDATE, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("保存banner排序失败");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:获取banner列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getBannerList", method = RequestMethod.POST)
	public MessageInfo getBannerList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
//		JSONObject obj = null;
		try {
//			obj = MobileassistantUtil.getRequestParams(request);
//			String uuid = MapUtils.getString(obj, "uuid");
			List<InsBannerVo> list = homeServiceImpl.getBannerList();
			info.setData(list == null ? new ArrayList<>() : list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("获取banner失败");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:删除banner
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "deleteBanner", method = RequestMethod.POST)
	public MessageInfo deleteBanner(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String uuid = MapUtils.getString(obj, "uuid");
			long id = MapUtils.getLongValue(obj, "id");
			homeServiceImpl.deleteBanner(id);
			
			saveUserOperatorLog(uuid, id, MeixConstants.BANNER, MeixConstants.OPERATOR_DELETE, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("新增banner失败");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}
}
