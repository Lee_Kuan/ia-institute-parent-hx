package com.meix.institute.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meix.institute.api.IInsSyncSndReqService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.impl.IaServiceImpl;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.pojo.InsSyncSndReqInfo;
import com.meix.institute.util.ExceptionUtil;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.MobileassistantUtil;

/**
 * 接口同步控制器
 * @author Xueyc
 * @date 2019-9-2 14:02:31
 *
 */
@Controller
@RequestMapping("/sync")
public class InterfaceSyncController {
    
    @Autowired
    private IInsSyncSndReqService insSyncSndReqService;
    @Autowired
    private IaServiceImpl iaServiceImpl; 
    
    private static final Logger log = LoggerFactory.getLogger(InterfaceSyncController.class);
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @RequestMapping(value = "sndReq", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo sndReq(HttpServletRequest request, HttpServletResponse response) {
        
        MessageInfo resp = new MessageInfo(request);
        JSONObject obj = null;
        
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            long id = MapUtils.getLongValue(obj, "id", 0l);
            InsSyncSndReqInfo info = insSyncSndReqService.selectByPrimaryKey(id);
            info.setRetryCnt(info.getRetryCnt() + 1);
            info.setRetryLast(info.getUpdatedAt());
            info.setRetryMax(info.getRetryMax() + 1);
            try {
                String url = info.getMethod();
                String reqHeader = info.getReqHeader();
                String reqBody = info.getReqBody();
                Map head = GsonUtil.json2Map(reqHeader);
                Map body = GsonUtil.json2Map(reqBody);
                JSONObject result = iaServiceImpl.synchrodata(url, head, body); // 调用同步接口
                if (MeixConstantCode.M_1008 == MapUtils.getIntValue(result, "messageCode", 0)) {
                    info.setRemarks("成功");
                    info.setState(MeixConstants.SYNC_STATE_SUCCESS);
                } else {
                    info.setState(MeixConstants.SYNC_STATE_FAIL);
                    info.setRemarks(result.getString("message"));
                }
            } catch (Exception e) {
                info.setState(MeixConstants.SYNC_STATE_FAIL);
                info.setRemarks(StringUtils.substring(ExceptionUtil.getFormatedMessage(e), 0, 499));
            }
            
            insSyncSndReqService.save(info, "");
            resp.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            resp.setMessageCode(MeixConstantCode.M_1009);
            log.warn(e.getMessage());
        }
        return resp;
    }
    
}
