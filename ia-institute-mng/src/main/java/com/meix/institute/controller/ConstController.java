package com.meix.institute.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.meix.institute.api.IConfigService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.vo.commen.ConstantVo;

import net.sf.json.JSONObject;

/**
 * 系统常量相关接口
 * Created by zenghao on 2019/9/4.
 */
@RestController
public class ConstController {
	private static Logger log = LoggerFactory.getLogger(ConstController.class);

	@Autowired
	private IConfigService configService;
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:获取线索池线索类型列表
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/const/getClueTypeList", method = {RequestMethod.GET, RequestMethod.POST})
	public MessageInfo getClueTypeList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		try {
			List<ConstantVo> list = MeixConstants.getClueTypeList();
			info.setMessageCode(MeixConstantCode.M_1008);
			info.setData(list);
		} catch (Exception e) {
			log.error("", e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage("获取线索类型列表失败");
		}
		return info;
	}

	@RequestMapping(value = "/const/getSystemUrl", method = {RequestMethod.GET, RequestMethod.POST})
	public MessageInfo getSystemUrl(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		try {
			 JSONObject object = MobileassistantUtil.getRequestParams(request);
            String type = MapUtils.getString(object, "type");
			String res = configService.getSystemUrl(type);
			info.setObject(res);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("", e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage("获取线索类型列表失败");
		}
		return info;
	}
}
