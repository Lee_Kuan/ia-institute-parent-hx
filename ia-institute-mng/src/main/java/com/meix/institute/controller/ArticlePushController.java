package com.meix.institute.controller;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.meix.institute.api.FsService;
import com.meix.institute.api.IArticlePushService;
import com.meix.institute.api.IDictTagService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.WechatArticleAction;
import com.meix.institute.api.WechatArticleHandler;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.BaseResp;
import com.meix.institute.core.DataResp;
import com.meix.institute.core.Paged;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.pojo.InsArticleGroupInfo;
import com.meix.institute.pojo.InsArticleGroupPreviewInfo;
import com.meix.institute.pojo.InsArticleInfo;
import com.meix.institute.pojo.InsArticleOtherInfo;
import com.meix.institute.pojo.InsArticleSearch;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.FileUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.ModelUtil;
import com.meix.institute.util.UuidUtil;

import net.sf.json.JSONObject;

/**
 * 订阅号文章推送控制器
 * 
 * @author xueyc
 * @since 2020年4月15日
 *
 */
@RequestMapping("/article")
@RestController
public class ArticlePushController {
    
    @Value("${ins.companyCode}")
    private String appType;
    
    @Autowired
    private IArticlePushService articlePushService;
    @Autowired
    private IDictTagService dictTagService;
    @Autowired
    private IInsUserService userService;
    @Autowired
    private WechatArticleHandler wechatArticleHandler;
    @Autowired
    private FsService fsService;
    
    private static final Logger log = LoggerFactory.getLogger(ArticlePushController.class);
    
    /**
     * 文章列表
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/paged", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo paged(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            InsArticleSearch search = ModelUtil.obj2Search(obj, InsArticleSearch.class);
            search.setTitle(MapUtils.getString(obj, "condition"));
            Paged<InsArticleInfo> paded = articlePushService.paged(search, MapUtils.getString(obj, "uuid"));
            convertArticlePageList(paded.getList());
            info.setData(paded.getList());
            info.setDataCount(paded.getCount());
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error("{}", e);
        }
        return info;
    }

    /**
     * 文章详情
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/details", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo details(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            Long id = MapUtils.getLong(obj, "id", 0l);
            InsArticleInfo details = articlePushService.getArticleDetails(id, MapUtils.getString(obj, "uuid"));
            convertArticleData(details);
            info.setObject(details);
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error(e.getMessage());
        }

        return info;
    }
    
    /**
     * 文章保存
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/save", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo save(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            InsArticleInfo data = ModelUtil.obj2Info(obj, InsArticleInfo.class);
            articlePushService.save(data, MapUtils.getString(obj, "uuid"));
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error(e.getMessage());
        }
        return info;
    }
    
    /**
     * 文章删除
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo delete(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            Long id = MapUtils.getLong(obj, "id");
            articlePushService.delete(id, MapUtils.getString(obj, "uuid"));
            
            info.setMessageCode(MeixConstantCode.M_1008);

        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error(e.getMessage());
        }

        return info;
    }
    
    /**
     * 群发列表
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/groupPage", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo groupPage(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String condition = MapUtils.getString(obj, "condition", "");
            InsArticleSearch search = ModelUtil.obj2Search(obj, InsArticleSearch.class);
            search.setTitle(condition);
            Paged<InsArticleGroupInfo> paged = articlePushService.groupPage(search, MapUtils.getString(obj, "uuid"));
            convertGroupPageList(paged.getList());
            info.setData(paged.getList());
            info.setDataCount(paged.getCount());
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error(e.getMessage());
        }
        return info;
    }

    /**
     * 群发详情
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/groupDetails", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo groupDetails(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            InsArticleGroupInfo data = articlePushService.getArticleGroupDetails(MapUtils.getLong(obj, "id"), false, MapUtils.getString(obj, "uuid"));
            info.setObject(data);
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error("{}", e);
        }
        return info;
    }
    
    /**
     * 群发保存并发布
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/groupSave", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo groupSaveAndPublish(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            // 参数准备
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            String publishStr = MapUtils.getString(obj, "publishTime");
            Date publishTime = null;
            if (StringUtils.isNotBlank(publishStr)) {
            	publishTime = DateUtil.stringToDate(publishStr);
            } else {
            	publishTime = new Date();
            	obj.put("publishTime", DateUtil.formatDate(publishTime));
            }
            InsArticleGroupInfo data = ModelUtil.obj2Info(obj, InsArticleGroupInfo.class);
            data.setPublishTime(publishTime);
            DataResp dataResp = articlePushService.groupSaveAndPublish(data, user); // 群发保存并发布
            
            if (dataResp.isSuccess()) {
                info.setMessageCode(MeixConstantCode.M_1008);
            } else {
                info.setMessageCode(MeixConstantCode.M_1009);
                info.setMessage(dataResp.getData() == null ? "群发保存并发布失败： " + JSONObject.fromObject(dataResp).toString() : String.valueOf(dataResp.getData()));
            }
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error("群发保存并发布异常：", e);
        }
        return info;
    }
    
    /**
     * 群发删除
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/groupDelete", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo groupDelete(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            articlePushService.deleteGroup(MapUtils.getLong(obj, "id"), MapUtils.getString(obj, "uuid"));
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error(e.getMessage());
        }
        return info;
    }
    
    /**
     * 群发发布
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/groupPublish", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo push(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            DataResp resp = articlePushService.groupPublish(MapUtils.getLong(obj, "id"), null, null, MapUtils.getString(obj, "uuid"));
            if (resp.isSuccess()) {
                info.setMessageCode(MeixConstantCode.M_1008);
            } else {
                info.setMessageCode(MeixConstantCode.M_1009);
                info.setMessage(resp.getErr_desc());
            }
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error(e.getMessage());
        }

        return info;
    }
    
    /**
     * 群发预览
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/groupPreview", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo groupPreviewSaveAndPublish(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            InsArticleGroupPreviewInfo data = ModelUtil.obj2Info(obj, InsArticleGroupPreviewInfo.class);
            
            DataResp dataResp = articlePushService.groupPreview(data, user); // 群发预览
            
            if (dataResp.isSuccess()) {
                info.setMessageCode(MeixConstantCode.M_1008);
            } else {
                info.setMessageCode(MeixConstantCode.M_1009);
                info.setMessage(JSONObject.fromObject(dataResp).toString());
            }
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error(e.getMessage());
        }
        return info;
    }
    
    /**
     * 上传图文消息内的图片获取URL
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/mediaUploadimg", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo mediaUploadimg(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request) {
        MessageInfo info = new MessageInfo();
        JSONObject obj = null;
        File tempFile = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            String uuid = UuidUtil.generate32bitUUID();
            String fileName = file.getOriginalFilename();
            if (StringUtils.isNotBlank(fileName)) {
                int indexOf = fileName.lastIndexOf(".");
                if (indexOf > 0) {
                    uuid += fileName.substring(indexOf);
                }
            }
            tempFile = fsService.createTempFile(uuid, file.getInputStream());
            if (tempFile == null) {
                info.setMessageCode(MeixConstantCode.M_1009);
                info.setMessage("文件导入有问题，请更换文件重试哦~");
                return info;
            }
            WechatArticleAction action = new WechatArticleAction();
            action.setData(tempFile);
            action.setAction(MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_UPLOADIMG);
            DataResp dataResp = wechatArticleHandler.action(action, user);
            if (!dataResp.isSuccess()) {
                info.setMessage(JSONObject.fromObject(dataResp).toString());
                info.setMessageCode(MeixConstantCode.M_1009);
            }
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("url", String.valueOf(dataResp.getData()));
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setObject(jsonObj);
        } catch (Exception e) {
            log.warn("mediaUploadimg异常：", e);
            info.setMessageCode(MeixConstantCode.M_1009);
            info.setMessage("文件上传失败");
        } finally {
            if (tempFile != null && tempFile.exists()) {
                tempFile.delete();
            }
        }
        return info;
    }
    
    /**
     * 新增其他类型永久素材
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/addMaterial", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo addMaterial(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request) {
        MessageInfo info = new MessageInfo();
        JSONObject obj = null;
        File tempFile = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            long uid = MapUtils.getLongValue(obj, "uid", 0l);
            String user = MapUtils.getString(obj, "uuid");
            String type = MapUtils.getString(obj, "type");
            if (file != null && !file.isEmpty()) {
                String originFileName = file.getOriginalFilename();
                tempFile = FileUtil.createTempFile(file.getInputStream(), String.valueOf(uid), originFileName);
                if (tempFile != null && tempFile.exists()) {
                    log.debug("本地文件" + tempFile.getAbsolutePath() + "缓存成功。。。");
                } else {
                    info.setMessageCode(MeixConstantCode.M_1009);
                    info.setMessage("文件导入有问题，请更换文件重试哦~");
                    return info;
                }
                WechatArticleAction action = new WechatArticleAction();
                action.setData(tempFile);
                action.setAction(MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_ADD_MATERIAL);
                DataResp dataResp = wechatArticleHandler.action(action, user);
                if (dataResp.isSuccess()) {
                    InsArticleOtherInfo otherInfo = new InsArticleOtherInfo();
                    otherInfo.setType(type);
                    JSONObject resObj = JSONObject.fromObject(dataResp.getData());
                    otherInfo.setMediaId(MapUtils.getString(resObj, "media_id"));
                    otherInfo.setLocalUrl(MapUtils.getString(resObj, "url"));
                    otherInfo.setRemoteUrl(MapUtils.getString(resObj, "url"));
                    articlePushService.saveOther(otherInfo, user);
                    JSONObject result = new JSONObject();
                    result.put("mediaId", otherInfo.getMediaId());
                    result.put("url", otherInfo.getRemoteUrl());
                    info.setObject(result);
                } else {
                    info.setObject(new JSONObject());
                    info.setMessage(JSONObject.fromObject(dataResp).toString());
                    info.setMessageCode(MeixConstantCode.M_1009);
                    return info;
                }
                info.setMessageCode(MeixConstantCode.M_1008);
            }
        } catch (Exception e) {
            log.warn("mediaUploadimg异常：", e);
            info.setMessageCode(MeixConstantCode.M_1009);
            info.setMessage("文件上传失败");
        } finally {
            if (tempFile != null && tempFile.exists()) {
                tempFile.delete();
            }
        }
        return info;
    }
    
    /**
     * 群发下架(删除)
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "/groupUndo", method = {RequestMethod.POST})
    @ResponseBody
    public MessageInfo groupUndo(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            BaseResp resp = articlePushService.groupUndo(MapUtils.getLong(obj, "id"), MapUtils.getString(obj, "uuid"));
            if (resp.isSuccess()) {
                info.setMessageCode(MeixConstantCode.M_1008);
            } else {
                info.setMessageCode(MeixConstantCode.M_1009);
                info.setMessage(resp.getErr_desc());
            }
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.error(e.getMessage());
        }

        return info;
    }
    
    private void convertArticlePageList(List<InsArticleInfo> list) {
        for (InsArticleInfo insArticleInfo : list) {
            convertArticleData(insArticleInfo);
        }
    }

    private void convertArticleData(InsArticleInfo insArticleInfo) {
        if (insArticleInfo == null) {
            return;
        }
        if (insArticleInfo.getState() != null) {
            insArticleInfo.setStateText(dictTagService.getText(MeixConstants.ARTICLE_STATE, insArticleInfo.getState().toString()));
        }
        if (StringUtils.isNotBlank(insArticleInfo.getUpdatedBy())) {
            insArticleInfo.setUpdatedBy(userService.getUserInfo(insArticleInfo.getUpdatedBy()).getUserName());
        }
    }
    
    private void convertGroupPageList(List<InsArticleGroupInfo> list) {
        for (InsArticleGroupInfo insArticleGroupInfo : list) {
            convertGroupData(insArticleGroupInfo);
        }
    }

    private void convertGroupData(InsArticleGroupInfo insArticleGroupInfo) {
        if (insArticleGroupInfo == null) {
            return;
        }
        if (insArticleGroupInfo.getState() != null) {
            insArticleGroupInfo.setStateText(dictTagService.getText(MeixConstants.ARTICLE_STATE, insArticleGroupInfo.getState().toString()));
        }
        if (StringUtils.isNotBlank(insArticleGroupInfo.getUpdatedBy())) {
            insArticleGroupInfo.setUpdatedBy(userService.getUserInfo(insArticleGroupInfo.getUpdatedBy()).getUserName());
        }
    }
}
