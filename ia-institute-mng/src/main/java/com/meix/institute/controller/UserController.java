package com.meix.institute.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.meix.institute.BaseService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IInsLoginService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsUser;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.pojo.InsUserInfo;
import com.meix.institute.pojo.InsUserSearch;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.vo.user.InsUserLabelVo;
import com.meix.institute.vo.user.UserInfo;

import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2019/8/1.
 */
@RequestMapping(value = "/user/")
@RestController
public class UserController extends BaseService {
	private Logger log = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private IInsUserService insUserService;
	@Autowired
	private CommonServerProperties serverProperties;
	@Autowired
	private IInsLoginService insLoginService;
	@Autowired
	private ICustomerGroupService groupService;

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:用户离职（无用）
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "deleteUser", method = RequestMethod.POST)
	public MessageInfo deleteUser(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String user = MapUtils.getString(object, "uuid");
			long contactId = MapUtils.getLongValue(object, "contactId", 0);

			InsUser leaveUserInfo = insUserService.getUserInfo(contactId);
			if (leaveUserInfo == null) {
				info.setMessage("不存在的用户");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			insUserService.updateUserStatus(contactId, 1, user);

			info.setMessage("操作成功");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.warn("deleteUser异常：", e);
			info.setMessage("删除用户失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 内部添加用户（无用）
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "addUser", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo register(HttpServletRequest request) {

		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String user = MapUtils.getString(obj, "uuid");
			String userName = MapUtils.getString(obj, MeixConstants.USERNAME);
			if (StringUtils.isBlank(userName)) {
				info.setMessageCode(MeixConstantCode.M_1095);
				info.setMessage("用户名不能为空");
				return info;
			}
			String password = MapUtils.getString(obj, MeixConstants.PASSWORD);
			if (StringUtils.isBlank(password)) {
				info.setMessageCode(MeixConstantCode.M_1096);
				info.setMessage("密码不能为空");
				return info;
			}

			String mobile = MapUtils.getString(obj, "mobile");
			if (StringUtils.isBlank(mobile)) {
				info.setMessageCode(MeixConstantCode.M_1097);
				info.setMessage("手机号不能为空");
				return info;
			}

			// 其他参数
			String email = MapUtils.getString(obj, "email");
			String position = MapUtils.getString(obj, "position");
			String headImageUrl = MapUtils.getString(obj, "headImageUrl");
			String descr = MapUtils.getString(obj, "descr");
			Integer role = MapUtils.getInteger(obj, "role", 0);

			String direction = MapUtils.getString(obj, "direction");
			// 验证用户是否存在
			InsUserSearch search = new InsUserSearch();
			search.setMobileOrEmail(mobile);
			int count = insUserService.countByDuplicate(search);
			if (count > 0) {
				info.setMessageCode(MeixConstantCode.M_1026);
				info.setMessage("手机号已存在");
				return info;
			}

			// 保存
			InsUserInfo insUserInfo = new InsUserInfo();
			insUserInfo.setUserName(userName);
			insUserInfo.setMobile(mobile);
			insUserInfo.setEmail(email);
			insUserInfo.setPosition(position);
			insUserInfo.setHeadImageUrl(headImageUrl);
			insUserInfo.setDescr(descr);
			insUserInfo.setCompanyCode(serverProperties.getCompanyCode());
			insUserInfo.setChiSpelling(PinyinHelper.getShortPinyin(userName).toUpperCase());
			insUserInfo.setPassword(password);
			insUserInfo.setRole(role);
			insUserInfo.setDirection(direction);
			insUserService.save(insUserInfo, user);

			info.setMessageCode(MeixConstantCode.M_1008);

		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("addUser异常：", e);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:修改角色
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "saveUserRole", method = RequestMethod.POST)
	public MessageInfo saveUserRole(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String user = MapUtils.getString(object, "uuid");
			int role = MapUtils.getIntValue(object, "role", 0);
			long uid = MapUtils.getLongValue(object, "id", 0);
			if (uid == 0 || role == 0) {
				info.setMessage("参数错误！");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			insUserService.updateUserRole(uid, role, user);

			saveUserOperatorLog(user, uid, MeixConstants.USER_YH_INNER_ROLE, MeixConstants.OPERATOR_UPDATE, role, object.toString());
			info.setMessage("操作成功");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.warn("deleteUser异常：", e);
			info.setMessage("删除用户失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
	
	/**
	 * 修改密码
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "modifyPwd", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo modifyPwd(HttpServletRequest request) {

		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid");
			String user = MapUtils.getString(obj, "uuid");
			String password = MapUtils.getString(obj, MeixConstants.PASSWORD);
			if (StringUtils.isBlank(password)) {
				info.setMessageCode(MeixConstantCode.M_1096);
				info.setMessage("原始密码不能为空");
				return info;
			}
			String newPwd = MapUtils.getString(obj, "newPwd"); // 新密码
			if (StringUtils.isBlank(newPwd)) {
				info.setMessageCode(MeixConstantCode.M_1096);
				info.setMessage("新密码不能为空");
				return info;
			}

			// 验证用户信息
			InsUserSearch search = new InsUserSearch();
			search.setId(uid);
			search.setPassword(password);
			InsUserInfo userInfo = insLoginService.authByPassword(search);
			if (userInfo == null) {
				info.setMessageCode(MeixConstantCode.M_1028);
				info.setMessage("原始密码错误");
				return info;
			}

			//修改密码
			insLoginService.modifyPwd(uid, newPwd, user);

			saveUserOperatorLog(user, uid, MeixConstants.USER_YH, MeixConstants.OPERATOR_UPDATE, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
			info.setMessage("操作成功");
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("modifyPwd异常:", e);
		}
		return info;
	}

	/**
	 * 重置密码
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "resetPwd", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo resetPwd(HttpServletRequest request) {

		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long contactId = MapUtils.getLongValue(obj, "contactId", 0);
			String user = MapUtils.getString(obj, "uuid");
			Integer clientType = MapUtils.getInteger(obj, MeixConstants.CLIENT_TYPE_HEAD);
			
			// 密码验证
            String password = MapUtils.getString(obj, "password");
            String password2 = MapUtils.getString(obj, "password2");

            if (StringUtils.isBlank(password) || StringUtils.isBlank(password2)) {
                info.setMessageCode(MeixConstantCode.M_1009);
                info.setMessage("重置密码不能为空");
                return info;
            }

			// 验证用户是否存在
			InsUserInfo userInfo = insUserService.selectByPrimaryKey(contactId);
			if (userInfo == null || userInfo.getId() == null) {
				info.setMessageCode(MeixConstantCode.M_1095);
				info.setMessage("用户名不存在");
				return info;
			}
			if (!StringUtils.equals(password, password2)) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("两次密码输入不一致");
				return info;
			}

			// 重置密码
			insUserService.resetPwd(contactId, password, user);
			// 设置token 失效
			insLoginService.logout(userInfo.getUser(), clientType);

			saveUserOperatorLog(user, contactId, MeixConstants.USER_YH_INNER_PW, MeixConstants.OPERATOR_UPDATE, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
			info.setMessage("操作成功");
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("resetPwd异常：", e);
		}
		return info;
	}

	/**
	 * @Description: 内部用户查询
	 */
	@RequestMapping(value = "getUserList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getUserList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		Map<String, Object> params = new HashMap<>();

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);

			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			String condition = MapUtils.getString(obj, "condition", ""); // 查询条件
			String position = MapUtils.getString(obj, "position");
			int role = MapUtils.getIntValue(obj, "role", 0);
			int userType = MapUtils.getIntValue(obj, "userType", 0);
			long groupId = MapUtils.getLongValue(obj, "groupId", 0);
			int isExcept = MapUtils.getIntValue(obj, "isExcept", 0);

			params.put("showNum", showNum);
			params.put("currentPage", currentPage * showNum);
			params.put("condition", StringUtil.replaceMysqlWildcard(condition));
			params.put("uid", uid);
			params.put("position", position);
			params.put("role", role);
			params.put("userType", userType);
			params.put("groupId", groupId);
			params.put("isExcept", isExcept);
			Paged<Map<String, Object>> data = insUserService.getContactList(params);

			String groupName = "全部内部用户";
			if (groupId > 0) {
				Map<String, Object> groupInfo = groupService.getCustomerGroupInfo(groupId);
				if (groupInfo != null) {
					groupName = MapUtils.getString(groupInfo, "groupName", "");
				}
			} else if (groupId == -1) {
				groupName = "未分组";
			}
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("groupName", groupName);
			objectMap.put("total", data.getCount());
			info.setObject(objectMap);
			info.setMessageCode(MeixConstantCode.M_1008);
			info.setData(data.getList());
			info.setDataCount(data.getCount());

		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("getUserList异常：", e);
		}

		return info;
	}

	/**
	 * 获取个人信息
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "getPersonalInfo", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getPersonalInfo(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0l);
			InsUserInfo userInfo = insUserService.selectByPrimaryKey(uid);
			info.setObject(userInfo);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("getPersonalInfo异常：", e);
		}
		return info;
	}

	/**
	 * 编辑他人用户信息（无用）
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "editUser", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo editUser(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			// 参数准备
			obj = MobileassistantUtil.getRequestParams(request);
			String userName = MapUtils.getString(obj, MeixConstants.USERNAME);
			Integer clientType = MapUtils.getInteger(obj, MeixConstants.CLIENT_TYPE_HEAD);

			// 校验
			long contactId = MapUtils.getLongValue(obj, "contactId", 0l);
			if (contactId == 0l) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("未加载到用户信息");
				return info;
			}
			if (StringUtils.isBlank(userName)) {
				info.setMessageCode(MeixConstantCode.M_1095);
				info.setMessage("用户名不能为空");
				return info;
			}
			String mobile = MapUtils.getString(obj, "mobile");
			if (StringUtils.isBlank(mobile)) {
				info.setMessageCode(MeixConstantCode.M_1097);
				info.setMessage("手机号不能为空");
				return info;
			}

			// 验证用户是否存在
			InsUserInfo userInfo = insUserService.getUserByMobileOrEmail(mobile, -1);
			if (userInfo != null && userInfo.getId() != contactId) {
				info.setMessageCode(MeixConstantCode.M_1026);
				info.setMessage("手机号已存在");
				return info;
			}

			// 其他参数
			String user = MapUtils.getString(obj, "uuid");
			String email = MapUtils.getString(obj, "email");
			String position = MapUtils.getString(obj, "position");
			String headImageUrl = MapUtils.getString(obj, "headImageUrl");
			String descr = MapUtils.getString(obj, "descr");
			int role = MapUtils.getIntValue(obj, "role");
			String direction = MapUtils.getString(obj, "direction");

			// 保存
			InsUserInfo insUserInfo = new InsUserInfo();
			insUserInfo.setId(contactId);
			insUserInfo.setUserName(userName);
			insUserInfo.setMobile(mobile);
			insUserInfo.setEmail(email);
			insUserInfo.setPosition(position);
			insUserInfo.setHeadImageUrl(headImageUrl);
			insUserInfo.setDescr(descr);
			insUserInfo.setRole(role);
			insUserInfo.setChiSpelling(PinyinHelper.getShortPinyin(userName).toUpperCase());
			insUserInfo.setDirection(direction);
			insUserService.save(insUserInfo, user);

			// 如果修改手机号，执行注销操作
			insLoginService.logout(insUserService.selectByPrimaryKey(contactId).getUser(), clientType);

			info.setMessageCode(MeixConstantCode.M_1008);

		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("editUser异常：", e);
		}
		return info;
	}

	/**
	 * 获取用户画像列表
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "getUserLabelList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getUserLableList(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			// 参数准备
			obj = MobileassistantUtil.getRequestParams(request);
			String user = MapUtils.getString(obj, "uuid");
			String condition = MapUtils.getString(obj, "condition");
			Integer accountType = MapUtils.getInteger(obj, "accountType");
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);

			Map<String, Object> search = new HashMap<String, Object>();
			search.put("condition", condition);
			search.put("accountType", accountType);
			search.put("currentPage", currentPage * showNum);
			search.put("showNum", showNum);
			Paged<Map<String, Object>> paged = insUserService.getUserLableList(search, user);

			info.setData(paged.getList());
			info.setDataCount(paged.getCount());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("getUserLableList异常:", e);
		}
		return info;
	}

	/**
	 * 添加用户画像标签
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "addUserLable", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo addUserLable(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			// 参数准备
			obj = MobileassistantUtil.getRequestParams(request);
			String user = MapUtils.getString(obj, "uuid");
			long contactId = MapUtils.getLongValue(obj, "contactId");
			int labelType = MapUtils.getIntValue(obj, "labelType");
			String labelCode = MapUtils.getString(obj, "labelCode");

			// 校验参数
			if (StringUtils.isBlank(labelCode) || labelType == 0 || contactId == 0) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("请求参数缺失");
				return info;
			}

			// 添加
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("contactId", contactId);
			data.put("labelType", labelType);
			data.put("labelCode", labelCode);
			insUserService.addUserLable(data, user);

			saveUserOperatorLog(user, contactId, MeixConstants.USER_YH_LABEL, MeixConstants.OPERATOR_ADD, labelType, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("addUserLable异常:", e);
		}
		return info;
	}

	/**
	 * 删除用户画像标签
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteUserLable", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo deleteUserLable(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			// 参数准备
			obj = MobileassistantUtil.getRequestParams(request);
			String user = MapUtils.getString(obj, "uuid");
			Long labelId = MapUtils.getLong(obj, "labelId");

			// 校验参数
			if (labelId == 0) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("请求参数缺失");
				return info;
			}

			// 删除
			insUserService.deleteUserLable(labelId, user);
			saveUserOperatorLog(user, labelId, MeixConstants.USER_YH_LABEL, MeixConstants.OPERATOR_DELETE, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("deleteUserLable异常:", e);
		}
		return info;
	}

	/**
	 * 获取某个用户的一些基本信息
	 *
	 * @param request
	 * @param response
	 * @return
	 * @
	 */
	@RequestMapping(value = "/getUserById", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getUserById(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(object, "id", 0);
			if (uid > 0) {
				InsUser user = insUserService.getUserInfo(uid);
				if (user != null) {
					info.setObject(user);
					info.setMessageCode(MeixConstantCode.M_1008);
					return info;
				}
			}
			info.setMessage("未查到用户ID:" + uid);
		} catch (Exception e) {
			String devMessage = "获取用户的基本信息失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
		}
		info.setMessageCode(MeixConstantCode.M_1009);
		return info;
	}
	
	/**
     * 获取用户画像标签
     *
     * @param request
     * @param response
     * @return
     * @
     */
    @RequestMapping(value = "/getUserLabelByContactId", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo getUserLabelByContactId(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject object = null;
        try {
            object = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(object, "uuid");
            long contactId = MapUtils.getLongValue(object, "contactId", 0);
            
            List<InsUserLabelVo> list = insUserService.getUserLabelByContactId(contactId, user);
            UserInfo userInfo = insUserService.selectCustomerInfoById(contactId, user);
            if (userInfo == null) {
            	userInfo = new UserInfo();
            }
            info.setObject(userInfo);
            info.setData(list);
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            log.warn("getUserLabelByContactId 异常：", e);
            info.setMessageCode(MeixConstantCode.M_1009);
        }
        return info;
    }
    
    /**
     * 获取用户最关注的标签
     *
     * @param request
     * @param response
     * @return
     * @
     */
    @RequestMapping(value = "/getUserFocusLabel", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo getUserFocusLabel(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject object = null;
        try {
            object = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(object, "uuid");
            
            List<Map<String, Object>> list = insUserService.getUserFocusLabel(user);
            
            info.setData(list);
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            log.warn("getUserFocusLabel 异常：", e);
            info.setMessageCode(MeixConstantCode.M_1009);
        }
        return info;
    }
    
}
