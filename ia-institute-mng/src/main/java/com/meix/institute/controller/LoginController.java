package com.meix.institute.controller;

import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.landray.kmss.pro.IProSsoService;
import com.landray.kmss.pro.IProSsoServiceProxy;
import com.meix.institute.api.IDictTagService;
import com.meix.institute.api.IInsLoginService;
import com.meix.institute.api.IVerificationCodeService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.pojo.InsUserInfo;
import com.meix.institute.pojo.InsUserSearch;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.OssResourceUtil;
import com.meix.institute.util.VerifyCodeUtil;
import com.meix.institute.vo.SecToken;
import com.meix.institute.vo.commen.InsVerificationCodeVo;

import net.sf.json.JSONObject;

/**
 * 登录
 * 添加（管理员权限）
 * 修改密码
 * 重置密码
 * 注销
 * @author Xuyec
 * @date 2019-8-1 10:25:55
 *
 */
@Controller
@RequestMapping(value="/user")
public class LoginController {
    
    @Autowired
    private IInsLoginService insLoginService;
    @Autowired
    private IDictTagService dictTagService;
    @Autowired
    private IVerificationCodeService verificationCodeService;
//    @Autowired
//	private CommonServerProperties commonServerProperties;
    
    private static final Logger log = LoggerFactory.getLogger(LoginController.class);
    
    /**
     * 获取验证码
     * @param request
     * @param response
     */
    @RequestMapping(value = {"/valifyCode"}, method = RequestMethod.GET)
    public void valifyCode(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setDateHeader("Expires", 0);
            response.setContentType("image/jpeg");
            //生成随机字串
            String verifyCode = VerifyCodeUtil.generateVerifyCode(4);
            //存入redis
            HttpSession session = request.getSession(true);
            log.info("create sessionId:{},varifyCode:{}", session.getId(), verifyCode.toLowerCase());
            InsVerificationCodeVo vo = new InsVerificationCodeVo();
            vo.setCode(verifyCode);
            vo.setJid(session.getId());
            vo.setType(MeixConstants.VERCODE_KEY_MNG);
            vo.setDate(DateUtil.stringToDate(DateUtil.dateToStr(new Date(), DateUtil.dateShortFormat)));
            verificationCodeService.save(vo);
            Cookie cookie = new Cookie(MeixConstants.JSESSIONID, session.getId());
            cookie.setPath("/");
            response.addCookie(cookie);
            //生成图片
            int w = 200, h = 80;
            VerifyCodeUtil.outputImage(w, h, response.getOutputStream(), verifyCode);
        } catch (Exception e) {
            e.printStackTrace();
            log.warn("获取验证码异常：", e);
        }
    }
    
    /**
     * 登录
     * @param request
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo login(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String loginNo = MapUtils.getString(obj, MeixConstants.LOGIN_NO);
            if (StringUtils.isBlank(loginNo)) {
                info.setMessageCode(MeixConstantCode.M_1095);
                info.setMessage("用户名不能为空");
                return info;
            }
            String password = MapUtils.getString(obj, MeixConstants.PASSWORD);
            if (StringUtils.isBlank(password)) {
                info.setMessageCode(MeixConstantCode.M_1096);
                info.setMessage("密码不能为空");
                return info;
            }
            String verCode = MapUtils.getString(obj, MeixConstants.VER_CODE);
            if (StringUtils.isBlank(verCode)) {
                info.setMessageCode(MeixConstantCode.M_1098);
                info.setMessage("验证码不能为空");
                return info;
            }
            String sessionId = MapUtils.getString(obj, MeixConstants.JSESSIONID);
            if (StringUtils.isBlank(sessionId)) {
                info.setMessageCode(MeixConstantCode.M_1009);
                info.setMessage("请求参数格式不正确");
                return info;
            }
            
            // 验证验证码
            InsVerificationCodeVo vo = new InsVerificationCodeVo();
            vo.setJid(sessionId);
            vo.setType(MeixConstants.VERCODE_KEY_MNG);
            vo.setDate(DateUtil.stringToDate(DateUtil.dateToStr(new Date(), DateUtil.dateShortFormat)));
            vo = verificationCodeService.selectOne(vo);
            if (vo == null || !StringUtils.equalsIgnoreCase(verCode, vo.getCode())) {
                info.setMessageCode(MeixConstantCode.M_1031);
                info.setMessage("验证码不正确");
                return info;
            }
            
            // 验证用户信息
            InsUserSearch search = new InsUserSearch();
            search.setUserName(loginNo);
            search.setPassword(password);
//            search.setUserState(MeixConstants.BOOLEAN_INT_0);
            InsUserInfo userInfo = insLoginService.authByPassword(search);
            if (userInfo == null) {
                info.setMessageCode(MeixConstantCode.M_1028);
                info.setMessage("用户名或密码不正确");
                return info;
            }
            
            if (userInfo.getUserState() == MeixConstants.BOOLEAN_INT_1) { //离职
            	info.setMessageCode(MeixConstantCode.M_1039);
                info.setMessage("账户已被冻结");
                return info;
            }
            //获取未失效token
            Integer clientType = MapUtils.getInteger(obj, MeixConstants.CLIENT_TYPE_HEAD);
            SecToken secToken = insLoginService.getTokenByUser(userInfo.getUser(), clientType);
            String token = secToken == null ? null : secToken.getToken();
            if (StringUtils.isBlank(token)) {
                // 生成token
            	token = insLoginService.createToken(userInfo.getId(), userInfo.getUser(), userInfo.getCompanyCode(), userInfo.getUserState(), clientType, null);
            }
            String ltpaToken = secToken == null ? null : secToken.getLtpaToken();
            if (StringUtils.isBlank(ltpaToken) && StringUtils.isNotBlank(userInfo.getOaName())) {
            	try {
	            	IProSsoService proSsoService = new IProSsoServiceProxy();
	                String ticket = proSsoService.generatorTokenByUser(userInfo.getOaName());
	                JSONObject jsonData = JSONObject.fromObject(ticket);
	                String flag = jsonData.getString("flag");
	                if (flag != null && "1".equals(flag)) {
	                	ltpaToken = jsonData.getString("result");
	                } else {
//	                	info.setMessageCode(MeixConstantCode.M_1009);
//	                    info.setMessage("系统单点登录失败：" + ticket);
//	                    return info;
	                	log.info("单点登录失败：{}", jsonData.toString());
	                }
            	} catch (Exception e) {
            		log.error("单点登录服务异常：{}", e);
				}
            }
            if (StringUtils.isBlank(ltpaToken)) {
//            	if (commonServerProperties.getEnv().contains(MeixConstants.ACTIVE_DEV)) {
					ltpaToken = token.substring(0, 30);
//				} else {
//	            	info.setMessageCode(MeixConstantCode.M_1009);
//	                info.setMessage("系统单点登录异常");
//	                return info;
//				}
            }
            insLoginService.updateToken(token, ltpaToken);
            token = ltpaToken;
            // 组装用户信息返回
            InsUserInfo resp = new InsUserInfo();
            resp.setMobile(userInfo.getMobile());
            resp.setUserName(userInfo.getUserName());
            resp.setId(userInfo.getId());
            resp.setEmail(userInfo.getEmail());
            resp.setHeadImageUrl(OssResourceUtil.getHeadImage(userInfo.getHeadImageUrl()));
            resp.setUserState(userInfo.getUserState());
            resp.setCompanyCode(userInfo.getCompanyCode());
            resp.setDescr(userInfo.getDescr());
            resp.setToken(token);
            resp.setPosition(userInfo.getPosition());
            resp.setRole(userInfo.getRole());
            if (StringUtils.isNotBlank(resp.getPosition())) {
                resp.setPositionText(dictTagService.getText(MeixConstants.POSITION, resp.getPosition()));
            }
            if (resp.getRole() != null) {
                resp.setRoleText(dictTagService.getText(MeixConstants.ROLE, resp.getRole().toString()));
            }
            info.setObject(resp);
            info.setMessageCode(MeixConstantCode.M_1011);
            info.setMessage("登录成功");
            
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            info.setMessage("系统异常");
            log.warn(e.getMessage());
        }
        return info;
    }
    
    /**
     * 注销
     * @param request
     * @return
     */
    @RequestMapping(value = "logout", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo logout(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            Integer clientType = MapUtils.getInteger(obj, MeixConstants.CLIENT_TYPE_HEAD);
            
            // 注销
            insLoginService.logout(user, clientType);
            
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn(e.getMessage());
        }
        return info;
    }
}
