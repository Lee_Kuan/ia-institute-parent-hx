package com.meix.institute.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meix.institute.api.IDictTagService;
import com.meix.institute.api.IInsSyncRecReqService;
import com.meix.institute.api.IInsSyncSndReqService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.ISyncTableService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.Paged;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.pojo.InsSyncRecReqInfo;
import com.meix.institute.pojo.InsSyncRecReqSearch;
import com.meix.institute.pojo.InsSyncSndReqInfo;
import com.meix.institute.pojo.InsSyncSndReqSearch;
import com.meix.institute.pojo.InsSyncTableJsidInfo;
import com.meix.institute.pojo.InsSyncTableJsidSearch;
import com.meix.institute.util.MobileassistantUtil;

@Controller
@RequestMapping(value = "/nrd")
public class NrdController {
    
    private static final Logger log = LoggerFactory.getLogger(NrdController.class);
    
    @Autowired
    private IInsSyncRecReqService insSyncRecReqService;
    @Autowired
    private IInsSyncSndReqService insSyncSndReqService;
    @Autowired
    private IInsUserService insUserService;
    @Autowired
    private ISyncTableService syncTableService;
    @Autowired
    private IDictTagService dictTagService;
    
    /**
     * 接收请求列表
     * @param request
     * @return
     */
    @RequestMapping(value = "getRecReqList", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo getRecReqList(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            Long id = MapUtils.getLong(obj, "id");
            String bizKey = MapUtils.getString(obj, "bizKey");
            String method = MapUtils.getString(obj, "method");
            String state = MapUtils.getString(obj, "state");
            String createdAtFm = MapUtils.getString(obj, "createdAtFm");
            String createdAtTo = MapUtils.getString(obj, "createdAtTo");
            String updatedAtFm = MapUtils.getString(obj, "updatedAtFm");
            String updatedAtTo = MapUtils.getString(obj, "updatedAtTo");
            int currentPage = MapUtils.getIntValue(obj, "currentPage");
            int showNum = MapUtils.getIntValue(obj, "showNum");
            
            InsSyncRecReqSearch search = new InsSyncRecReqSearch();
            search.setId(id);
            search.setBizKey(bizKey);
            search.setMethod(method);
            search.setState(state);
            search.setCreatedAtFm(createdAtFm);
            search.setCreatedAtTo(createdAtTo);
            search.setUpdatedAtFm(updatedAtFm);
            search.setUpdatedAtTo(updatedAtTo);
            search.setCurrentPage(currentPage * showNum);
            search.setShowNum(showNum);
            
            Paged<InsSyncRecReqInfo> paged = insSyncRecReqService.paged(search, user);
            List<InsSyncRecReqInfo> list = paged.getList();
            for (InsSyncRecReqInfo insSyncRecReqInfo : list) {
                insSyncRecReqInfo.setMethod(dictTagService.getText(MeixConstants.REC_REQ_METHOD, insSyncRecReqInfo.getMethod()));
            }
            info.setData(list);
            info.setDataCount(paged.getCount());
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }
    
    /**
     * 接收请求详情
     * @param request
     * @return
     */
    @RequestMapping(value = "getRecReqDetail", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo getRecReqDetail(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            Long id = MapUtils.getLong(obj, "id");
            
            InsSyncRecReqInfo record = insSyncRecReqService.selectByPrimaryKey(id);
            if (record != null && StringUtils.isNotBlank(record.getUpdatedBy())) {
            	record.setUpdatedBy(insUserService.getUserInfo(record.getUpdatedBy()).getUserName());
            	record.setMethod(dictTagService.getText(MeixConstants.REC_REQ_METHOD, record.getMethod()));
            }
            
            info.setObject(record);
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }
    
    /**
     * 接收请求重试
     * @param request
     * @return
     */
    @RequestMapping(value = "retryRecReq", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo retryRecReq(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            Long id = MapUtils.getLong(obj, "id");
            
            insSyncRecReqService.retry(id, user);
            
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }
    
    /**
     * 接收请求关闭
     * @param request
     * @return
     */
    @RequestMapping(value = "closeRecReq", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo closeRecReq(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            Long id = MapUtils.getLong(obj, "id");
            String remarks = MapUtils.getString(obj, "remarks");
            
            insSyncRecReqService.close(id, remarks, user);
            
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }
    
    /**
     * 发送请求列表
     * @param request
     * @return
     */
    @RequestMapping(value = "getSndReqList", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo getSndReqList(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            Long id = MapUtils.getLong(obj, "id");
            String bizKey = MapUtils.getString(obj, "bizKey");
            String method = MapUtils.getString(obj, "method");
            String state = MapUtils.getString(obj, "state");
            String createdAtFm = MapUtils.getString(obj, "createdAtFm");
            String createdAtTo = MapUtils.getString(obj, "createdAtTo");
            String updatedAtFm = MapUtils.getString(obj, "updatedAtFm");
            String updatedAtTo = MapUtils.getString(obj, "updatedAtTo");
            int currentPage = MapUtils.getIntValue(obj, "currentPage");
            int showNum = MapUtils.getIntValue(obj, "showNum");
            
            InsSyncSndReqSearch search = new InsSyncSndReqSearch();
            search.setId(id);
            search.setBizKey(bizKey);
            search.setMethod(method);
            search.setState(state);
            search.setCreatedAtFm(createdAtFm);
            search.setCreatedAtTo(createdAtTo);
            search.setUpdatedAtFm(updatedAtFm);
            search.setUpdatedAtTo(updatedAtTo);
            search.setCurrentPage(currentPage * showNum);
            search.setShowNum(showNum);
            
            Paged<InsSyncSndReqInfo> paged = insSyncSndReqService.paged(search, user);
            List<InsSyncSndReqInfo> list = paged.getList();
            for (InsSyncSndReqInfo insSyncSndReqInfo : list) {
                insSyncSndReqInfo.setMethod(dictTagService.getText(MeixConstants.SND_REQ_METHOD, insSyncSndReqInfo.getMethod()));
            }
            
            info.setData(paged.getList());
            info.setDataCount(paged.getCount());
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }
    
    /**
     * 发送请求详情
     * @param request
     * @return
     */
    @RequestMapping(value = "getSndReqDetail", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo getSndReqDetail(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            Long id = MapUtils.getLong(obj, "id");
            
            InsSyncSndReqInfo record = insSyncSndReqService.selectByPrimaryKey(id);
            if (record != null && StringUtils.isNotBlank(record.getUpdatedBy())) {
                record.setUpdatedBy(insUserService.getUserInfo(record.getUpdatedBy()).getUserName());
                record.setMethod(dictTagService.getText(MeixConstants.SND_REQ_METHOD, record.getMethod()));
            }
            info.setObject(record);
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }
    
    /**
     * 发送请求重试
     * @param request
     * @return
     */
    @RequestMapping(value = "retrySndReq", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo retrySndReq(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            Long id = MapUtils.getLong(obj, "id");
            
            insSyncSndReqService.retry(id, user);
            
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }
    
    /**
     * 发送请求关闭
     * @param request
     * @return
     */
    @RequestMapping(value = "closeSndReq", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo closeSndReq(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            Long id = MapUtils.getLong(obj, "id");
            String remarks = MapUtils.getString(obj, "remarks");
            
            insSyncSndReqService.close(id, remarks, user);
            
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }

    /**
     * 贝格同步列表
     * @param request
     * @return
     */
    @RequestMapping(value = "getDataSyncList", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo getDataSyncList(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String method = MapUtils.getString(obj, "method");
            String state = MapUtils.getString(obj, "state");
            int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
            int showNum = MapUtils.getIntValue(obj, "showNum", 10);
            
            
            InsSyncTableJsidSearch search = new InsSyncTableJsidSearch();
            search.setTableName(StringUtils.trimToNull(method));
            search.setState(StringUtils.trimToNull(state));
            search.setCurrentPage(currentPage * showNum);
            search.setShowNum(showNum);
            Paged<InsSyncTableJsidInfo> paged = syncTableService.paged(search);
            
            convertData(paged.getList());
            
            info.setData(paged.getList());
            info.setDataCount(paged.getCount());
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }
    
    /**
     * 贝格同步重试
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "retrySyncTable", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo retrySyncManage(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            Long id = MapUtils.getLong(obj, "id");
            
            syncTableService.retry(id, user);
            
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }
    
    /**
     * 贝格同步关闭
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "closeSyncTable", method = RequestMethod.POST)
    @ResponseBody
    public MessageInfo closeSyncManage(HttpServletRequest request) {
        
        MessageInfo info = new MessageInfo(request);
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            String user = MapUtils.getString(obj, "uuid");
            Long id = MapUtils.getLong(obj, "id");
            String remarks = MapUtils.getString(obj, "remarks");
            
            syncTableService.close(id, remarks, user);
            
            info.setMessageCode(MeixConstantCode.M_1008);
            info.setMessage("操作成功");
        } catch (Exception e) {
            info.setMessageCode(MeixConstantCode.M_1009);
            log.warn("操作失败：", e);
        }
        return info;
    }
    
    private void convertData(List<InsSyncTableJsidInfo> list) {
        list.forEach((item)->{
            if (StringUtils.isNotBlank(item.getUpdatedBy())) {
                item.setUpdatedBy(insUserService.getUserInfo(item.getUpdatedBy()).getUserName());
                item.setTableName(dictTagService.getText(MeixConstants.SYNC_TABLE_NAME, item.getTableName()));
            }
        });
    }
}
