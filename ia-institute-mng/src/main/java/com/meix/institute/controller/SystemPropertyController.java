package com.meix.institute.controller;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * 系统属性控制器
 *
 */
@RestController
@RequestMapping("sys/property")
public class SystemPropertyController {
    
    @RequestMapping("name")
    public String getSysPropertyByName(HttpServletRequest request) {
        String name = request.getParameter("name");
        if (StringUtils.isBlank(name)) {
            return "请输入系统参数名称！！！";
        }
        return System.getProperty(request.getParameter("name"));
    }
    
    @RequestMapping("all")
    public String getSysPropertyAll(HttpServletRequest request) {
        Properties properties = System.getProperties();
        return properties.toString();
    }
}
