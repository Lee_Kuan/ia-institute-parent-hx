package com.meix.institute.controller;

import com.meix.institute.BaseService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IMeetingService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.MorningMeeting;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.search.ComplianceListSearchVo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.vo.meeting.MorningMeetingVo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
@RequestMapping(value = "/meeting")
public class MorningMeetingController extends BaseService {

	private static final Logger log = LoggerFactory.getLogger(MorningMeetingController.class);

	@Autowired
	private IMeetingService meetingServiceImpl;
	@Autowired
	private ICustomerGroupService groupService;

	@RequestMapping(value = "/saveMeeting", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo saveMeeting(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		MorningMeeting meeting = new MorningMeeting();
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long id = MapUtils.getLongValue(obj, "id", 0);
			int operatorType = id == 0 ? MeixConstants.OPERATOR_ADD : MeixConstants.OPERATOR_UPDATE;
			String uuid = MapUtils.getString(obj, "uuid", null);
			long companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
			int share = MapUtils.getIntValue(obj, "share", 0);
			String title = MapUtils.getString(obj, "title");
			String content = MapUtils.getString(obj, "content");
			String url = MapUtils.getString(obj, "url");
			int meetingType = MapUtils.getIntValue(obj, "meetingType", 0);
			String publishDate = MapUtils.getString(obj, "publishDate", DateUtil.getCurrentDateTime());
			int type = MapUtils.getIntValue(obj, "type");
			if (StringUtils.isBlank(title)) {
				info.setMessage("晨会名称必填");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			JSONArray groupList = null;
			if (share == 1 && obj.containsKey("groupList")) {
				groupList = obj.getJSONArray("groupList");
			}

			/**    活动发布渠道			*/
			JSONArray issueType = null;
			if (obj.containsKey("issueType")) {
				issueType = obj.getJSONArray("issueType");
			}
			meeting.setCompanyCode(companyCode);
			meeting.setContent(content);
			meeting.setId(id);
			meeting.setTitle(title);
			meeting.setUrl(url);
			meeting.setPublishDate(DateUtil.stringToDate(publishDate));
			meeting.setShare(share);
			meeting.setMeetingType(meetingType);
			meeting.setType(type);
			long resId = id == 0 ? com.meix.institute.util.StringUtils.getId(meeting.getCompanyCode()) : id;
			
			int isPub = groupService.saveDataGroupShare(resId, MeixConstants.USER_CH, meetingType, groupList);
			meeting.setShare(isPub);
			id = meetingServiceImpl.saveMeeting(resId, uuid, meeting, issueType);
			
			Map<String, Long> meetingId = new HashMap<String, Long>();
			meetingId.put("id", id);
			info.setObject(meetingId);

			saveUserOperatorLog(uuid, resId, MeixConstants.USER_CH, operatorType, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);

			try {
				String clientstr = request.getParameter("clientstr");
				JSONObject clientstrObj = JSONObject.fromObject(clientstr);
				if (clientstrObj != null) {
					clientstrObj.put("id", id);
					request.setAttribute("clientstr", clientstrObj.toString());
				}
			} catch (Exception e) {
				log.error("", e);
			}
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}

		return info;
	}


	/**
	 * @Description: 晨会详情
	 */
	@RequestMapping(value = "/getMeetingDetail", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getMeetingDetail(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

//			long uid = MapUtils.getLongValue(obj, "uid", 0);
			long meetingId = obj.getLong("id");
			MorningMeetingVo vo = meetingServiceImpl.getMeetingDetail(meetingId);

			info.setObject(vo);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:内容管理列表（晨会 ）
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getMeetingList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getMeetingList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		info.setClientStyle(MeixConstants.MS_DIALOG);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
//			String uid = MapUtils.getString(object, "uid");
			String condition = MapUtils.getString(object, "condition", null);
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
			String startTime = MapUtils.getString(object, "startDate", null);
			String endTime = MapUtils.getString(object, "endDate", null);
			int status = MapUtils.getIntValue(object, "status", -1);

			int sortType = MapUtils.getIntValue(object, "sortType", 0);	//排序列0-默认列（开始时间）
			int sortRule = MapUtils.getIntValue(object, "sortRule", -1); //排序规则 1 asc, -1 desc
			sortRule = sortRule == 0 ? -1 : sortRule;
			String sortField = MapUtils.getString(object, "sortField", null); //排序字段
			if (StringUtils.isBlank(sortField) || "publishDate".equalsIgnoreCase(sortField)) {
				sortType = 0;
			}
			int share = MapUtils.getIntValue(object, "share", -1);
			int meetingType = MapUtils.getIntValue(object, "meetingType", -1);
			ComplianceListSearchVo searchVo = new ComplianceListSearchVo();

			List<Integer> statusList = new ArrayList<>();
			switch (status) {
			case -1:
//				statusList.add(1);
				break;
			case 0:
				statusList.add(0);
				break;
			case 1:
				statusList.add(1);
				break;
			case 2:
				statusList.add(2);
				break;
			case 3:
				statusList.add(3);
				break;
			default:
				info.setMessage("不支持的状态");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
		}
			searchVo.setStatusList(statusList);
//			searchVo.setActivityType(activityType);
			searchVo.setCompanyCode(companyCode);
			searchVo.setCondition(condition);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			searchVo.setStartTime(startTime);
			searchVo.setEndTime(endTime);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			searchVo.setShare(share);
			searchVo.setType(meetingType);
			searchVo.setSubType(meetingType);
			List<MorningMeetingVo> list = meetingServiceImpl.getMeetingList(searchVo);
			int total = meetingServiceImpl.getMeetingCount(searchVo);
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("total", total);
			objectMap.put("currentPage", currentPage);
			info.setObject(objectMap);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取内容管理晨会列表失败", e);
			info.setMessage("获取内容管理晨会列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
}
