package com.meix.institute.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.meix.institute.BaseService;
import com.meix.institute.api.IActivityService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IInsWhiteListService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.constant.SyncConstants;
import com.meix.institute.entity.InsActivity;
import com.meix.institute.entity.InsCompanyInstituteUserActivity;
import com.meix.institute.entity.InsUser;
import com.meix.institute.impl.MeixSyncServiceImpl;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.MediaUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.vo.activity.ActivityDayNumberVo;
import com.meix.institute.vo.activity.InsActivityVo;
import com.meix.institute.vo.activity.OrgOrUserVo;
import com.meix.institute.vo.file.FileAttr;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/activity")
public class ActivityController extends BaseService {

	private static final Logger log = LoggerFactory.getLogger(ActivityController.class);

	@Autowired
	private IActivityService activityServiceImpl;
	@Autowired
	private IInsWhiteListService whiteListService;
	@Autowired
	private MeixSyncServiceImpl syncService;
	@Autowired
	private ICustomerGroupService groupService;
	
	@RequestMapping(value = "/saveActivity", method = RequestMethod.POST)
	@ResponseBody
	public Object saveActivity(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		InsActivity activity = new InsActivity();
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long id = MapUtils.getLongValue(obj, "id", 0);
			int operatorType = id == 0 ? MeixConstants.OPERATOR_ADD : MeixConstants.OPERATOR_UPDATE;
			String uuid = MapUtils.getString(obj, "uuid", null);
			long companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
			int activityType = MapUtils.getIntValue(obj, "activityType", 0);
			if (activityType == 0) {
				info.setMessage("活动类型必填");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			String title = MapUtils.getString(obj, "title", null);
			if (StringUtils.isBlank(title)) {
				info.setMessage("活动名称必填");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			//调研对象(电话会议\路演时为宏观、策略、事件)
			String researchObject = MapUtils.getString(obj, "researchObject", "");
			String startTime = MapUtils.getString(obj, "startTime", null);
			if (StringUtils.isBlank(startTime)) {
				info.setMessage("活动开始时间必填");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			if (startTime.length() == 16) {
				startTime += ":00";
			}
			String endTime = null;
			if (activityType == MeixConstants.CHAT_MESSAGE_CL) {
				endTime = MapUtils.getString(obj, "endTime", null);
			}
			if (endTime != null && endTime.length() == 16) {
				endTime += ":00";
			}
			String fileAttr = MapUtils.getString(obj, "fileAttr", "");
			if (fileAttr != null && fileAttr.length() > 0) {
				JSONObject fileAttrObj = JSONObject.fromObject(fileAttr);
				fileAttrObj.put("ct", System.currentTimeMillis());
				activity.setFileAttr(fileAttrObj.toString());
			}
			String resourceUrl = MapUtils.getString(obj, "resourceUrl");
			//新增字段
			String resourceUrlSmall = MapUtils.getString(obj, "resourceUrlSmall", "");
			String applyDeadlineTime = MapUtils.getString(obj, "applyDeadlineTime", "");
			
			if (StringUtils.isBlank(resourceUrl) || "null".equals(resourceUrl)) {
				resourceUrl = "";
				obj.put("resourceUrl", "");
			}
			if (StringUtils.isBlank(resourceUrlSmall) || "null".equals(resourceUrlSmall)) {
				resourceUrlSmall = "";
				obj.put("resourceUrlSmall", "");
			}
			int isHiden = MapUtils.getIntValue(obj, "isHiden", 1); //0-不隐藏
			activity.setIsHiden(isHiden);

			activity.setResourceUrl(resourceUrl);
			activity.setResourceUrlSmall(resourceUrlSmall);
			activity.setApplyDeadlineTime(DateUtil.stringToDate(applyDeadlineTime));

			//分析师、发起人
			JSONArray analyst = null;
			if (obj.containsKey("analyst")) {
				analyst = obj.getJSONArray("analyst");
			}
			if (analyst != null && !analyst.isEmpty()) {
				InsUser analystUser = insUserService.getUserInfo(Long.valueOf(String.valueOf(analyst.get(0))));
				if (analystUser != null) {
					obj.put("analystMobile", analystUser.getMobile());
				}
			}
			//行业代码(1.3.4变成内码)
			JSONArray industry = null;
			if (obj.containsKey("industry")) {
				industry = obj.getJSONArray("industry");
			}
			//股票内码
			JSONArray secu = null;
			if (obj.containsKey("secu")) {
				secu = obj.getJSONArray("secu");
			}

			int webcastType = MapUtils.getIntValue(obj, "webcastType", 0); //直播商 0-美市，1-263
			int share = MapUtils.getIntValue(obj, "share", 1);
			int replayType = MapUtils.getIntValue(obj, "replayType", 1); //回放 0-无回放，1-有回放
			activity.setShare(share);
			JSONArray sycnShare = new JSONArray();
			//分享列表
			JSONArray groupList = null;
			JSONArray shareList = null;
			JSONObject shareObj = new JSONObject();
			
			//联系人、可接入电话
			JSONArray contact = null;
			if (obj.containsKey("contact")) {
				contact = obj.getJSONArray("contact");
			}
			/**    活动发布渠道			*/
			JSONArray issueType = null;
			if (obj.containsKey("issueType")) {
				issueType = obj.getJSONArray("issueType");
			}

			String address = MapUtils.getString(obj, "address", "");
			String activityDesc = obj.getString("activityDesc");

			String city = MapUtils.getString(obj, "city", "");
			
			int isArtificial = MapUtils.getIntValue(obj, "isArtificial", 0); //会议类型，0-自主会议，1人工会议
			String joinNumber = MapUtils.getString(obj, "joinNumber", null);
			String joinPassword = MapUtils.getString(obj, "joinPassword", null);
			activity.setId(id);
			activity.setActivityDesc(activityDesc);
			activity.setActivityType(activityType);
			activity.setAddress(address);
			activity.setCompanyCode(companyCode);
			Date sTime = DateUtil.stringToDate(startTime);
			if(sTime == null) {
				info.setMessage("活动开始时间必填");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			long sTimet = new Date().getTime() + 3600000;
			if (activityType == MeixConstants.CHAT_MESSAGE_DHHY && sTime.before(new Date(sTimet))) {
				info.setMessage("电话会议需至少提前一小时创建，紧急会议可能无法安排，请知悉。");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			Date eTime = DateUtil.stringToDate(endTime);
			if (StringUtils.isEmpty(endTime)) {
				eTime = new Date(sTime.getTime() + 7200000);
				obj.put("endTime", DateUtil.formatDate(eTime, "yyyy-MM-dd HH:mm"));
			} else if (eTime.before(new Date(sTimet))) {
				info.setMessage("活动持续时间低于一小时，请注意活动开始、结束时间！");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			activity.setStartTime(sTime);
			activity.setEndTime(eTime);
			activity.setTitle(title);
			activity.setResearchObject(researchObject);
			activity.setCity(city);
			activity.setWebcastType(webcastType);
			activity.setReplayType(replayType);
			activity.setIsArtificial(isArtificial);
			activity.setJoinNumber(joinNumber);
			activity.setJoinPassword(joinPassword);

			if (id > 0) {
				whiteListService.deleteActivityUser(id);
			}
			long resId = activity.getId() == 0 ? com.meix.institute.util.StringUtils.getId(activity.getCompanyCode()) : activity.getId();
			if (share == 0) {
				shareObj.put("code", 0);
				shareObj.put("type", 0);
				sycnShare.add(shareObj);
			}
			if (share == 1 && obj.containsKey("shareList")) {
				shareObj.put("type", 13);
				shareObj.put("code", companyCode);
				sycnShare.add(shareObj);
				shareList = obj.getJSONArray("shareList");
				// 处理列表
				if (shareList != null && !shareList.isEmpty()) {
					Calendar cal = Calendar.getInstance();
					Date nowDate = cal.getTime();
					cal.setTime(DateUtil.stringToDate(startTime));
					cal.add(Calendar.DAY_OF_MONTH, 7);
					Date expireDate = cal.getTime();
					List<InsCompanyInstituteUserActivity> userList = new ArrayList<>();
					for (Object tempObj : shareList) {
						JSONObject temp = JSONObject.fromObject(tempObj);
						InsCompanyInstituteUserActivity insUser = new InsCompanyInstituteUserActivity();
						insUser.setActivityId(resId);
						insUser.setCompanyCode(companyCode);
						insUser.setCompanyName(MapUtils.getString(temp, "companyName", ""));
						insUser.setMobile(MapUtils.getString(temp, "mobile", ""));
						insUser.setPosition(MapUtils.getString(temp, "position", ""));
						insUser.setSalerName(MapUtils.getString(temp, "salerName"));
						insUser.setSalerUid(MapUtils.getLongValue(temp, "salerUid", 0));
						insUser.setUserName(MapUtils.getString(temp, "userName", ""));
						insUser.setCreatedAt(nowDate);
						insUser.setCreatedBy(uuid);
						insUser.setExpireTime(expireDate);
						userList.add(insUser);
					}
					whiteListService.addActivityUserInfo(userList);
				}
			}
			if (share == 1 && obj.containsKey("groupList")) {
				groupList = obj.getJSONArray("groupList");
			}
			int isPub = groupService.saveDataGroupShare(resId, MeixConstants.USER_HD, activityType, groupList);
			activity.setShare(isPub);
			id = activityServiceImpl.saveActivity(resId, uuid, activity, null, analyst, industry, secu, contact, issueType);

			Map<String, Long> activityID = new HashMap<>();
			activityID.put("id", id);
			info.setObject(activityID);
			obj.put("id", id);
			InsUser user = insUserService.getUserInfo(uuid);
			obj.put("authMobile", user == null ? null : user.getMobile());
			
			obj.put("share", 2); // 走美市白名单权限
			syncService.syncActivity(MeixConstants.USER_HD, SyncConstants.ACTION_SAVE_ACTIVITY, GsonUtil.json2Map(obj.toString()));
			saveUserOperatorLog(uuid, id, MeixConstants.USER_HD, operatorType, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}

		return info;
	}

	/**
	 * @Description: 活动详情
	 */
	@RequestMapping(value = "/getActivityDetail", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getActivityDetail(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

//			long uid = MapUtils.getLongValue(obj, "uid", 0);
			long activityId = obj.getLong("activityId");
			InsActivityVo vo = activityServiceImpl.getActivityDetail(0, activityId, 0, false);

			info.setObject(vo);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}


	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * @Title: uploadFile、
	 * @Description:上传文件（无用）
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo uploadFile(HttpServletRequest request, HttpServletResponse response) throws Exception {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		MultipartFile file = null;
		try {
			//判断是否上传了文件
			if (request instanceof MultipartHttpServletRequest) {
				file = ((MultipartHttpServletRequest) request).getFile("file");
			}
			obj = MobileassistantUtil.getRequestParams(request);
			long activityId = MapUtils.getLongValue(obj, "activityId", 0);
			String uuid = MapUtils.getString(obj, "uuid", null);
			activityServiceImpl.updateActivityStatus(activityId, 3, uuid);    //回放

			FileAttr media = new FileAttr();
			int mp3Time = 0;
			String fileName = file.getOriginalFilename();
//			fileName = fileName.replaceAll("\\.", System.currentTimeMillis() + ".");
			int index = fileName.lastIndexOf(".");
			String type = fileName.substring(index + 1);
			String filePath = MeixConstants.uploadDir + File.separator + fileName;

			OutputStream out = new FileOutputStream(new File(filePath));
			InputStream in = file.getInputStream();
			int length = 0;
			byte[] buf = new byte[1024];
			while ((length = in.read(buf)) != -1) {
				out.write(buf, 0, length);
			}
			in.close();
			out.close();

			// amr 转为mp3
			if (type.equalsIgnoreCase("amr")) {
				fileName = MediaUtil.convertToMp3(MeixConstants.uploadDir, fileName);
				filePath = MeixConstants.uploadDir + File.separator + fileName;
			}
//			media.setUrl(MeixConstants.liveAudioDir + filePath);	//阿里云的地址是这样生成的，但是此处调用不到
			media.setUrl(filePath);
			// 获取时长
			if (type.equalsIgnoreCase("mp3") || type.equalsIgnoreCase("amr")) {    //音频
				mp3Time = MediaUtil.getMp3Time(filePath);
				media.setMt(mp3Time);
				media.setSize(file.getSize());
				media.setCt(System.currentTimeMillis());
				media.setType(type);
			} else {    //图片
				ImageIcon imageIcon = new ImageIcon(filePath);
				media.setH(imageIcon.getIconHeight());
				media.setW(imageIcon.getIconWidth());
			}
			info.setObject(JSONObject.fromObject(media).toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error(e.getMessage());
		}
		return info;
	}

	@RequestMapping(value = "/getActivityNumOfDayByMonth", method = {RequestMethod.POST})
	public @ResponseBody
	MessageInfo getActivityNumOfDayByMonth(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = obj.getLong("uid");
			String uuid = MapUtils.getString(obj, "uuid", null);
			String date = MapUtils.getString(obj, "date", new SimpleDateFormat("yyyy-MM").format(Calendar.getInstance().getTime()));
			if (date.length() > 7) {
				date = date.substring(0, 7);
			}
			List<ActivityDayNumberVo> list = activityServiceImpl.getActivityNumOfDayByMonth(uid, uuid, date);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);

		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error(e.getMessage());
		}

		return info;
	}

	@RequestMapping(value = "/deleteActivity", method = {RequestMethod.POST})
	public @ResponseBody
	MessageInfo deleteActivity(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String uuid = MapUtils.getString(obj, "uuid", "");
			long activityId = MapUtils.getLongValue(obj, "id");
			activityServiceImpl.deleteActivity(activityId, uuid);
			
			saveUserOperatorLog(uuid, activityId, MeixConstants.USER_HD, 
					MeixConstants.OPERATOR_DELETE, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error(e.getMessage());
		}

		return info;
	}
	
	@RequestMapping(value = "/getOrgOrUserContactList", method = {RequestMethod.POST})
	public @ResponseBody
	MessageInfo getOrgOrUserContactList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String condition = MapUtils.getString(obj, "condition", "");
			int type = MapUtils.getIntValue(obj, "queryType");    //1-机构，2-系统用户，3-普通用户
			int currentPage = MapUtils.getIntValue(obj, "currentPage");
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			List<OrgOrUserVo> list = activityServiceImpl.getOrgOrUserContactList(currentPage, showNum, condition, type);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error(e.getMessage());
		}

		return info;
	}
	
	@RequestMapping(value = "/getDataPowerShare", method = {RequestMethod.POST})
	public @ResponseBody
	MessageInfo getDataPowerShare(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long dataId = MapUtils.getLongValue(obj, "dataId", 0);
			int dataType = MapUtils.getIntValue(obj, "dataType", 0);//3-研报类，8-活动类，3000-合辑类，13-晨会类
			int dataSubType = MapUtils.getIntValue(obj, "dataSubType", 0);//具体内容类型
			int share = MapUtils.getIntValue(obj, "share", -1);
			List<Map<String, Object>> list = groupService.getDataShareList(dataId, dataType, dataSubType, share);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error(e.getMessage());
		}

		return info;
	}
}
