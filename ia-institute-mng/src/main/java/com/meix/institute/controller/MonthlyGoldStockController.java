package com.meix.institute.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.meix.institute.BaseService;
import com.meix.institute.api.IInsMonthlyGoldStockService;
import com.meix.institute.config.PubServerConfig;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.excel.ImportGoldStock;
import com.meix.institute.impl.ExcelExportService;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.service.PubServiceImpl;
import com.meix.institute.util.FileUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.vo.stock.GoldStockSelfUserInfo;
import com.meix.institute.vo.stock.MonthlyGoldStockListVo;
import com.meix.institute.vo.stock.MonthlyGoldStockVo;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 月度金股管理
 *
 * @author likuan
 *         2019年7月1日16:58:13
 */
@Controller
public class MonthlyGoldStockController extends BaseService {

	private static final Logger log = LoggerFactory.getLogger(MonthlyGoldStockController.class);
	@Autowired
	private IInsMonthlyGoldStockService goldStockService;
	@Autowired
	private ExcelExportService excelExportService;
	@Autowired
	private PubServiceImpl pubService;
	@Autowired
	private PubServerConfig pubServerConfig;
	/**
	 * 月度金股保存
	 */
	@RequestMapping(value = "/institute/saveGoldStock", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo saveGoldStock(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo();
		JSONObject dataobj = null;
		try {
			dataobj = MobileassistantUtil.getRequestParams(request);

//			long uid = dataobj.getLong("uid");
			String uuid = MapUtils.getString(dataobj, "uuid", null);
			long companyCode = MapUtils.getLongValue(dataobj, "companyCode", 0);
			String month = dataobj.getString("month");
			JSONArray datas = dataobj.getJSONArray("data");
			List<String> errInfo = goldStockService.saveGoldStockMonth(uuid, companyCode, month, datas);
			if (datas.size() <= errInfo.size()) {
				result.setMessageCode(MeixConstantCode.M_1009);
				result.setMessage("保存失败");
				return result;
			} else {
				result.setMessageCode(MeixConstantCode.M_1008);
			}
			result.setData(errInfo);
			
		} catch (Exception e) {
			result.setMessage("未知错误！");
			result.setMessageCode(MeixConstantCode.M_1009);
			log.error(e.getMessage());
		}
		return result;
	}

	/**
	 * 删除月度金股中股票
	 */
	@RequestMapping(value = "/institute/deleteGoldStock", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo deleteGoldStock(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo();
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String user = MapUtils.getString(obj, "uuid", "");
			long companyCode = obj.getLong("companyCode");
			String month = obj.getString("month");
			long innerCode = obj.getLong("innerCode");
			goldStockService.deleteGoldStock(month, innerCode, companyCode);
			try {
				saveUserOperatorLog(user, innerCode, MeixConstants.USER_GP, MeixConstants.OPERATOR_DELETE, Integer.valueOf(month.replaceAll("-", "")), obj.toString());
			} catch (Exception e) {
			}
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			result.setMessage("未知错误！");
			result.setMessageCode(MeixConstantCode.M_1009);
			log.error(e.getMessage());
		}
		return result;
	}

	/**
	 * 月度金股月份统计列表
	 */
	@RequestMapping(value = "/institute/getGoldStockList", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getGoldStockList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo();
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long companyCode = obj.getLong("companyCode");
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			Map<String, Object> res = goldStockService.getGoldStockList(showNum, currentPage, companyCode);
//			List<MonthlyGoldStockListVo> list = goldStockService.getGoldStockList(showNum, currentPage, companyCode);
			@SuppressWarnings("unchecked")
			List<MonthlyGoldStockListVo> list = (List<MonthlyGoldStockListVo>) res.get("list");
			int count = 0;
			if (list != null && list.size() > 0) {
				count = goldStockService.getGoldStockCount(companyCode);
				count += MapUtils.getIntValue(res, "nowMonth", 0);
//				if (MapUtils.getBooleanValue(res, "newMonth", false)) {
//					count += 1;
//				}
			}
			Map<String, Object> map = new HashMap<>();
			map.put("total", count);
			map.put("currentPage", currentPage);
			result.setObject(map);
			result.setData(list);
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			result.setMessage("未知错误！");
			result.setMessageCode(MeixConstantCode.M_1009);
			log.error(e.getMessage());
		}
		return result;
	}

	/**
	 * 获取月度金股列表
	 */
	@RequestMapping(value = "/institute/getGoldStockMonthList", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getGoldStockMonthList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo();
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long companyCode = obj.getLong("companyCode");
			String month = obj.getString("month");
			int homeShowFlag = MapUtils.getIntValue(obj, "homeShowFlag", 2);
			int showNum = MapUtils.getIntValue(obj, "showNum", 20);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			List<MonthlyGoldStockVo> list = goldStockService.getGoldStockMonthList(showNum, currentPage, companyCode, month, homeShowFlag);
			int count = 0;
			if (list != null && list.size() > 0) {
				count = goldStockService.getGoldStockMonthCount(companyCode, month, homeShowFlag);
			}
			Map<String, Object> map = new HashMap<>();
			map.put("total", count);
			map.put("currentPage", currentPage);
			result.setObject(map);
			result.setData(list);
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			result.setMessage("未知错误！");
			result.setMessageCode(MeixConstantCode.M_1009);
			log.error(e.getMessage());
		}
		return result;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/institute/importGoldStock")
	public @ResponseBody
	MessageInfo importGoldStock(@RequestParam(value = "file") MultipartFile importFile, HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo();
		JSONObject obj = null;
		File file = null;
		try {
			
			if (importFile == null) {
				info.setMessage("读取文件失败");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String token = obj.getString("token");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);
			ImportParams importParams = new ImportParams();
			importParams.setSheetNum(1);//查询的sheet数量
			List<ImportGoldStock> combEliteRank = ExcelImportUtil.importExcel(importFile.getInputStream(), ImportGoldStock.class, importParams);

			String uuid = MapUtils.getString(obj, "uuid", null);
			//用户所属研究所ID
			JSONArray datas = JSONArray.fromObject(combEliteRank);
			
			XSSFWorkbook wb = new XSSFWorkbook(importFile.getInputStream());
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.getRow(0).createCell(4).setCellValue("备注");
			long companyCode = obj.getLong("companyCode");
			String month = obj.getString("month");
			goldStockService.saveGoldStockMonth(uuid, companyCode, month, datas, sheet);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			wb.write(outputStream);
			wb.close();
			file = FileUtil.createTempFile(outputStream.toByteArray(), String.valueOf(uid), importFile.getOriginalFilename());
			if (file != null && file.exists()) {
				System.out.println(file.getAbsolutePath());
				log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
			} else {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("文件导入有问题，请更换文件重试哦~");
				return info;
			}
			String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
			log.debug("【fileUpload】temp:{}", temp);
			if (temp != null) {
			
				JSONObject result = JSONObject.fromObject(temp);
				if (result.getInt("messageCode") == 1008) {
					JSONObject res = result.getJSONObject("object");
					info.setMessageCode(MeixConstantCode.M_1008);
					info.setObject(res);
					return info;
				}
			}
			info.setMessage("模板错误，导入失败！");
			info.setMessageCode(MeixConstantCode.M_1009);
		} catch (Exception e) {
			e.printStackTrace();
			info.setMessage("模板错误，导入失败！");
			info.setMessageCode(MeixConstantCode.M_1009);
			return info;
		}finally {
			if (file != null && file.exists()) {
				file.delete();
			}
		}
		return info;
	}
	/**
	 * 获取股票自选股用户列表
	 */
	@RequestMapping(value = "/institute/getSelfStockUserList", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getSelfStockUserList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo();
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long innerCode = obj.getLong("innerCode");
			int showNum = MapUtils.getIntValue(obj, "showNum", 20);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			List<GoldStockSelfUserInfo> list = goldStockService.getSelfStockUserList(showNum, currentPage, innerCode);
			int count = 0;
			if (list != null && list.size() > 0) {
				count = goldStockService.getSelfStockUserCount(innerCode);
			}
			Map<String, Object> map = new HashMap<>();
			map.put("total", count);
			map.put("currentPage", currentPage);
			result.setObject(map);
			result.setData(list);
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			result.setMessage("未知错误！");
			result.setMessageCode(MeixConstantCode.M_1009);
			log.error(e.getMessage());
		}
		return result;
	}

	/**
	 * 导出excel
	 * 自选股导出Excel
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/institute/exportGoldStockMonthList", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public MessageInfo exportSigninListExcel(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		File file = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long uid = MapUtils.getLongValue(obj, "uid");
			long companyCode = obj.getLong("companyCode");
			String month = obj.getString("month");
			int showNum = MapUtils.getIntValue(obj, "showNum", 1000);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			String token = obj.getString("token");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);
			List<MonthlyGoldStockVo> list = goldStockService.getGoldStockMonthList(showNum, currentPage, companyCode, month, 1);
			
			Workbook wb = excelExportService.getGoldStockMonthExcel(list);
			if (wb == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("报表导出失败");
				return info;
			}
			String excelName = "月度金股详情列表" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			if (RequestMethod.GET.toString().equals(request.getMethod())) {
				// 设置response头信息
				response.reset();
				response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
				try {
					response.setHeader("Content-disposition", "attachment; filename=" + excelName + ".xls");
				} catch (Exception e1) {
					log.info(e1.getMessage());
				}

				try {
					//将文件输出
					OutputStream ouputStream = response.getOutputStream();
					wb.write(ouputStream);
					ouputStream.flush();
					ouputStream.close();
					return null;
				} catch (Exception e) {
					log.info("导出Excel失败！");
					log.error("", e);
				}
			} else {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				wb.write(outputStream);
				byte[] data = outputStream.toByteArray();
				outputStream.close();
				file = FileUtil.createTempFile(data, String.valueOf(uid), excelName + ".xls");
				if (file != null && file.exists()) {
					log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
				} else {
					info.setMessageCode(MeixConstantCode.M_1009);
					info.setMessage("导出文件异常");
					return info;
				}
				String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
				log.debug("【fileUpload】temp:{}", temp);
				if (temp != null) {
					JSONObject result = JSONObject.fromObject(temp);
					String url = null;
					if (result.getInt("messageCode") == 1008) {
						url = result.getJSONObject("object").getString("url");
					}
	
					info.setMessageCode(MeixConstantCode.M_1008);
					JSONObject object = new JSONObject();
					object.put("fileUrl", url);
					info.setObject(object);
				}
			}
		} catch (Exception e) {
			info.setMessage("未知错误");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		} finally {
			if (file != null) {
				file.deleteOnExit();
			}
		}
		return info;
	}
	
	/**
     * 月度金股首页下架
     */
    @RequestMapping(value = "/institute/goldShelves", method = RequestMethod.POST)
    public @ResponseBody
    MessageInfo goldShelves(HttpServletRequest request) {
        MessageInfo result = new MessageInfo();
        JSONObject obj = null;
        try {
            obj = MobileassistantUtil.getRequestParams(request);
            Long id = MapUtils.getLong(obj, "id");
            String user = MapUtils.getString(obj, "user");
            int homeShowFlag = MapUtils.getInteger(obj, "flag", 0);
            String uuid = MapUtils.getString(obj, "uuid", "");
            goldStockService.goldPullOffShelves(id, homeShowFlag, user);
            
            saveUserOperatorLog(uuid, id, MeixConstants.USER_GP, MeixConstants.OPERATOR_UNDO, homeShowFlag, obj.toString());
            result.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            result.setMessage("未知错误！");
            result.setMessageCode(MeixConstantCode.M_1009);
            log.warn(e.getMessage());
        }
        return result;
    }
}
