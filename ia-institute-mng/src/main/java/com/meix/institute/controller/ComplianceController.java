package com.meix.institute.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.meix.institute.BaseService;
import com.meix.institute.api.IDictTagService;
import com.meix.institute.api.IInsActivityService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IMeetingService;
import com.meix.institute.api.IReportService;
import com.meix.institute.api.IUserService;
import com.meix.institute.config.PubServerConfig;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.constant.SyncConstants;
import com.meix.institute.entity.InsActivity;
import com.meix.institute.entity.InsReportAlbum;
import com.meix.institute.entity.InsUser;
import com.meix.institute.entity.MorningMeeting;
import com.meix.institute.entity.ReportInfo;
import com.meix.institute.impl.ExcelExportService;
import com.meix.institute.impl.MeixSyncServiceImpl;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.search.ComplianceCommentSearchVo;
import com.meix.institute.search.ComplianceListSearchVo;
import com.meix.institute.service.PubServiceImpl;
import com.meix.institute.util.FileUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.vo.CommentVo;
import com.meix.institute.vo.activity.InsActivityVo;
import com.meix.institute.vo.compliance.ComplianceVo;
import com.meix.institute.vo.compliance.ReportComplianceVo;
import com.meix.institute.vo.meeting.MorningMeetingVo;
import com.meix.institute.vo.report.ReportAlbumVo;
import com.meix.institute.vo.report.ReportDetailVo;

import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2019/7/31.
 */
@RequestMapping(value = "/compliance/")
@RestController
public class ComplianceController extends BaseService {
	private Logger log = LoggerFactory.getLogger(ComplianceController.class);

	@Autowired
	private IInsActivityService insActivityService;
	@Autowired
	private IInsUserService insUserService;
	@Autowired
	private IReportService reportService;
	@Autowired
	private IMeetingService meetingService;
	@Autowired
	private IInstituteMessagePushService messagePush;
	@Autowired
	private IUserService userServiceImpl;
	@Autowired
	private ExcelExportService excelExportService;
	@Autowired
	private PubServiceImpl pubService;
	@Autowired
	private PubServerConfig pubServerConfig;
	@Autowired
	private IDictTagService dictTagService;
	@Autowired
	private MeixSyncServiceImpl syncService;
	
	DateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-DD 00:00:00");

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:合规监控列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getComplianceList", method = RequestMethod.POST)
	public MessageInfo getComplianceList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		info.setClientStyle(MeixConstants.MS_DIALOG);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
//			String uid = MapUtils.getString(object, "uid");
			String condition = MapUtils.getString(object, "condition");
			int activityType = MapUtils.getIntValue(object, "activityType", 0);
			//-1活动监控 0待审核 3已撤销
			int activityStatus = MapUtils.getIntValue(object, "activityStatus", -1);
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
			
			int sortType = MapUtils.getIntValue(object, "sortType", 0);	//排序列0-默认列（开始时间）
			int sortRule = MapUtils.getIntValue(object, "sortRule", -1); //排序规则 1 asc, -1 desc
			sortRule = sortRule == 0 ? -1 : sortRule;
			
			String sortField = MapUtils.getString(object, "sortField", null); //排序字段
			if (StringUtils.isBlank(sortField) || "publishDate".equalsIgnoreCase(sortField)) {
				sortType = 0;
			} else if ("undoTime".equalsIgnoreCase(sortField)) {
				sortType = 1;
			}
			ComplianceListSearchVo searchVo = new ComplianceListSearchVo();
			List<Integer> activityStatusList = new ArrayList<>();
			switch (activityStatus) {
				case -1:
					activityStatusList.add(1);
//					activityStatusList.add(2);
					break;
				case 0:
					activityStatusList.add(0);
					break;
				case 3:
					activityStatusList.add(3);
					break;
				default:
					info.setMessage("不支持的活动状态");
					info.setMessageCode(MeixConstantCode.M_1009);
					return info;
			}
			searchVo.setStatusList(activityStatusList);
			searchVo.setType(activityType);
			searchVo.setCompanyCode(companyCode);
			searchVo.setCondition(condition);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			Map<String, Object> objectMap = new HashMap<>();
			List<Object> resList = new ArrayList<>();
			if (activityType == MeixConstants.USER_YB || activityType == 4) {    //研报 、会议纪要
				switch (activityType) {
					case MeixConstants.USER_YB:
						searchVo.setType(0);
						break;
					case 4:
						searchVo.setType(1);
						break;
					default:
						break;
				}
				List<ReportDetailVo> list = reportService.getReportList(searchVo);
				int total = reportService.getReportCount(searchVo);

				objectMap.put("total", total);
				objectMap.put("currentPage", currentPage);
				for (ReportDetailVo temp : list) {
					ReportComplianceVo vo = new ReportComplianceVo();
					vo.setId(temp.getId());
					vo.setActivityStatus(temp.getStatus());
					vo.setTitle(temp.getTitle());
					vo.setIssueTypeList(temp.getIssueTypeList());
					vo.setShareName(temp.getShareName());
					vo.setEndTime(temp.getPublishDate());
					vo.setEditUserName(temp.getEditUserName());
					vo.setActivityType(activityType);
					vo.setUpdatedBy(temp.getUpdatedBy());
					vo.setOperatorName(temp.getOperatorName());
					vo.setOperatorUid(temp.getOperatorUid());
					vo.setUndoReason(temp.getUndoReason());
					vo.setRefuseReason(temp.getRefuseReason());
					vo.setAuthorName(temp.getAuthorName());
					vo.setPublishDate(temp.getPublishDate());
					vo.setUndoTime(temp.getUndoTime());
					vo.setStartTime(temp.getPublishDate());
					vo.setInfoType(dictTagService.getText(MeixConstants.REPORT_TYPE, temp.getInfoType()));
					resList.add(vo);
				}
			} else if (activityType == MeixConstants.USER_CH) {    //晨会
				List<MorningMeetingVo> list = meetingService.getMeetingList(searchVo);
				int total = meetingService.getMeetingCount(searchVo);
				objectMap.put("total", total);
				objectMap.put("currentPage", currentPage);
				for (MorningMeetingVo temp : list) {
					ComplianceVo vo = new ComplianceVo();
					vo.setId(temp.getId());
					vo.setActivityStatus(temp.getStatus());
					vo.setTitle(temp.getTitle());
					vo.setIssueTypeList(temp.getIssueTypeList());
					vo.setShareName(temp.getShareName());
					vo.setEditUserName(temp.getEditUserName());
					vo.setActivityType(activityType);
					vo.setUpdatedBy(temp.getUpdatedBy());
					vo.setOperatorName(temp.getOperatorName());
					vo.setOperatorUid(temp.getOperatorUid());
					vo.setUndoReason(temp.getUndoReason());
					vo.setRefuseReason(temp.getRefuseReason());
					vo.setAuthorName(temp.getAuthorName());
					vo.setUndoTime(temp.getUndoTime());
					vo.setPublishDate(temp.getPublishDate());
					vo.setStartTime(temp.getPublishDate());
					resList.add(vo);
				}
			} else if (activityType == MeixConstants.REPORT_ALBUM || activityType == MeixConstants.MEETING_ALBUM) { //合辑
				if (activityType == MeixConstants.REPORT_ALBUM) {
					searchVo.setType(0);
				} else {
					searchVo.setType(1);
				}
				List<ReportAlbumVo> list = reportService.getReportAlbumList(searchVo);
				int total = reportService.getReportAlbumCount(searchVo);

				objectMap.put("total", total);
				objectMap.put("currentPage", currentPage);
				for (ReportAlbumVo temp : list) {
					ReportComplianceVo vo = new ReportComplianceVo();
					vo.setId(temp.getId());
					vo.setActivityStatus(temp.getStatus());
					vo.setTitle(temp.getTitle());
					vo.setShareName(temp.getShareName());
					vo.setIssueTypeList(temp.getIssueTypeList());
					vo.setEndTime(temp.getPublishDate());
					vo.setActivityType(activityType);
					vo.setOperatorUid(String.valueOf(temp.getOperatorUid()));
					vo.setUndoReason(temp.getUndoReason());
					vo.setPublishDate(temp.getPublishDate());
					vo.setUndoTime(temp.getUndoTime());
					vo.setStartTime(temp.getPublishDate());
					vo.setAuthorName(temp.getAuthorName());
					resList.add(vo);
				}
			} else {
				List<InsActivityVo> list = insActivityService.getComplianceList(searchVo);
				long count = insActivityService.getComplianceCount(searchVo);
				objectMap.put("total", count);
				objectMap.put("currentPage", currentPage);
				resList.addAll(list);
			}
			info.setData(resList);
			info.setObject(objectMap);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取合规监控列表失败", e);
			info.setMessage("获取合规监控列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:合规监控内容审核
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "examineActivity", method = RequestMethod.POST)
	public MessageInfo examineActivity(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		info.setClientStyle(MeixConstants.MS_DIALOG);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(object, "uid", 0);
			String uuid = MapUtils.getString(object, "uuid");
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			long activityId = MapUtils.getLongValue(object, "activityId", 0);
			String reason = MapUtils.getString(object, "reason");
			//1-已通过，2-已拒绝，3-已撤销
			int optType = MapUtils.getIntValue(object, "optType", 0);
			int activityType = MapUtils.getIntValue(object, "activityType", 0);

			InsUser userInfo = insUserService.getUserInfo(uid);
			if (userInfo == null) {
				info.setMessage("非系统用户无法进行该操作");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			if (optType <= 0) {
				info.setMessage("暂不支持该操作类型");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}

			int dataType = 0;
			if (activityType == MeixConstants.USER_YB || activityType == 4) {
				dataType = MeixConstants.USER_YB;
				ReportInfo report = new ReportInfo();
				report.setResearchId(activityId);
				report.setUpdatedAt(new Date());
				report.setUpdatedBy(uuid);
				report.setOperatorUid(uuid);
				report.setOperatorName(userInfo.getUserName());
				report.setStatus(optType);
				switch (optType) {
					case 2:
						report.setRefuseReason(reason);
						break;
					case 3:
						report.setUndoReason(reason);
						break;
					default:
						break;
				}
				reportService.examineReport(report);
			} else if (activityType == MeixConstants.USER_CH) {
				dataType = MeixConstants.USER_CH;
				MorningMeeting meeting = new MorningMeeting();
				meeting.setId(activityId);
				meeting.setUpdatedAt(new Date());
				meeting.setUpdatedBy(uuid);
				meeting.setOperatorUid(uuid);
				meeting.setOperatorName(userInfo.getUserName());
				meeting.setStatus(optType);
				switch (optType) {
					case 2:
						meeting.setRefuseReason(reason);
						break;
					case 3:
						meeting.setUndoReason(reason);
						break;
					default:
						break;
				}
				meetingService.examineMeeting(meeting);
			} else if (activityType == MeixConstants.REPORT_ALBUM || activityType == MeixConstants.MEETING_ALBUM) { //合辑
				dataType = MeixConstants.REPORT_ALBUM;
				InsReportAlbum reportAlbum = new InsReportAlbum();
				reportAlbum.setId(activityId);
				reportAlbum.setUpdatedAt(new Date());
				reportAlbum.setUpdatedBy(uuid);
				reportAlbum.setOperatorUid(uuid);
				reportAlbum.setOperatorName(userInfo.getUserName());
				reportAlbum.setStatus(optType);
				if (optType == 3) {
					reportAlbum.setUndoReason(reason);
					reportAlbum.setUndoTime(new Date());
				}
				reportService.examineReportAlbum(reportAlbum);
			}else {
				dataType = MeixConstants.USER_HD;
				InsActivity activity = insActivityService.getActivity(activityId, companyCode);
				if (activity == null) {
					info.setMessage("不存在的活动");
					info.setMessageCode(MeixConstantCode.M_1009);
					return info;
				}
				activity.setId(activityId);
				activity.setActivityStatus(optType);
				switch (optType) {
					case 2:
						activity.setRefuseReason(reason);
						break;
					case 3:
						activity.setUndoReason(reason);
						break;
					default:
						break;
				}
				activity.setOperatorUid(userInfo.getUser());
				activity.setOperatorName(userInfo.getUserName());
				activity.setUpdatedBy(userInfo.getUser());
				activity.setUpdatedAt(new Date());
				insActivityService.examineActivity(activity);
			}
			if (optType == 3) {
				saveUserOperatorLog(uuid, activityId, dataType, MeixConstants.OPERATOR_UNDO, 0, object.toString());
				messagePush.undoMessagePush(activityType, activityId, reason, companyCode);
				if (activityType == MeixConstants.USER_HD || activityType == MeixConstants.USER_YB || activityType == 4) {
					Map<String, Object> params = new HashMap<>();
					params.put("id", activityId);
					params.put("reason", reason);
					params.put("companyCode", companyCode);
					syncService.reqMeixSync(activityType == 4 ? MeixConstants.USER_YB : activityType, SyncConstants.ACTION_UNDO_RESOURCE, params);
				}
			}
			info.setMessage("操作成功");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("合规监控内容审核失败", e);
			info.setMessage("合规监控内容审核失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:内容管理列表（活动 ）
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getManagementList", method = RequestMethod.POST)
	public MessageInfo getManagementList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		info.setClientStyle(MeixConstants.MS_DIALOG);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
//			String uid = MapUtils.getString(object, "uid");
			String condition = MapUtils.getString(object, "condition", null);
			int activityType = MapUtils.getIntValue(object, "activityType", 0);
			int status = MapUtils.getIntValue(object, "status", -1);//-1全部  0-未审核，1-已通过，2-已拒绝，3-已撤销
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
			String startTime = MapUtils.getString(object, "startTime", null);
			int sortType = MapUtils.getIntValue(object, "sortType", 0);	//排序列0-默认列（开始时间）
			int sortRule = MapUtils.getIntValue(object, "sortRule", -1); //排序规则 1 asc, -1 desc
			sortRule = sortRule == 0 ? -1 : sortRule;
			String sortField = MapUtils.getString(object, "sortField", null); //排序字段
			if (StringUtils.isBlank(sortField) || "publishDate".equalsIgnoreCase(sortField)) {
				sortType = 0;
			}
			int share = MapUtils.getIntValue(object, "share", -1);
			
			if (StringUtils.isBlank(startTime)) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.YEAR, -1);
				startTime = startDateFormat.format(cal.getTime());
			}
//			int status = MapUtils.getIntValue(object, "status", -1);

			ComplianceListSearchVo searchVo = new ComplianceListSearchVo();
			List<Integer> statusList = new ArrayList<>();
			switch (status) {
				case -1:
//					statusList.add(1);
					break;
				case 0:
					statusList.add(0);
					break;
				case 1:
					statusList.add(1);
					break;
				case 2:
					statusList.add(2);
					break;
				case 3:
					statusList.add(3);
					break;
				case 4:
					statusList.add(-1);
					break;
				default:
					info.setData(new ArrayList<>());
					info.setMessage("不支持的状态");
					info.setMessageCode(MeixConstantCode.M_1009);
					return info;
			}
			searchVo.setStatusList(statusList);
			searchVo.setType(activityType);
			searchVo.setCompanyCode(companyCode);
			searchVo.setCondition(condition);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			searchVo.setShare(share);
			List<InsActivityVo> list = insActivityService.getManagementList(searchVo);
			int total = insActivityService.getManagementCount(searchVo);
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("total", total);
			objectMap.put("currentPage", currentPage);
			info.setObject(objectMap);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取内容管理列表失败", e);
			info.setMessage("获取内容管理列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 获取参加人员列表
	 */
	@RequestMapping(value = "/examineJoinPerson", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo examineJoinPerson(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long id = MapUtils.getLongValue(obj, "id");
			//0-未审核，1-通过，2-拒绝
			int optType = MapUtils.getIntValue(obj, "optType");
			String uuid = MapUtils.getString(obj, "uuid");
			insActivityService.examineJoinPersonList(id, optType, uuid);
			result.setMessage("操作成功！");
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			result.setMessage("未知错误！");
			result.setMessageCode(MeixConstantCode.M_1009);
		}
		return result;
	}

	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:评论合规
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getCommentList", method = RequestMethod.POST)
	public MessageInfo getCommentList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		info.setClientStyle(MeixConstants.MS_DIALOG);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
//			String uid = MapUtils.getString(object, "uid");
			String condition = MapUtils.getString(object, "condition", null);
			int type = MapUtils.getIntValue(object, "type", 0);
			int status = MapUtils.getIntValue(object, "status", -1);//-1全部  0-未审核，1-已通过，2-已拒绝，3-已撤销
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
//			String startTime = MapUtils.getString(object, "startTime", null);
			int sortType = MapUtils.getIntValue(object, "sortType", 0);	//排序列0-默认列（开始时间）
			int sortRule = MapUtils.getIntValue(object, "sortRule", -1); //排序规则 1 asc, -1 desc
			
			long dataId = MapUtils.getLongValue(object, "dataId", 0);
//			int status = MapUtils.getIntValue(object, "status", -1);
			String sortField = MapUtils.getString(object, "sortField", null); //排序字段
			if (StringUtils.isBlank(sortField) || "time".equalsIgnoreCase(sortField)) {
				sortType = 0;
			}
			ComplianceCommentSearchVo searchVo = new ComplianceCommentSearchVo();
			List<Integer> statusList = new ArrayList<>();
			switch (status) {
				case -1:
					statusList.add(0);
					statusList.add(1);
					statusList.add(2);
					statusList.add(3);
					break;
				case 0:
					statusList.add(0);
					break;
				case 1:
					statusList.add(1);
					break;
				case 2:
					statusList.add(2);
					break;
				case 3:
					statusList.add(3);
					break;
				default:
					info.setMessage("不支持的状态");
					info.setMessageCode(MeixConstantCode.M_1009);
					return info;
			}
			searchVo.setStatusList(statusList);
			searchVo.setType(type);
			searchVo.setCompanyCode(companyCode);
			searchVo.setCondition(condition);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			searchVo.setDataId(dataId);
			List<CommentVo> list = userServiceImpl.getCommentList(searchVo);
			int total = userServiceImpl.getCommentCount(searchVo);
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("total", total);
			objectMap.put("currentPage", currentPage);
			info.setObject(objectMap);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取评论管理列表失败", e);
			info.setMessage("获取评论管理列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:合规监控内容审核（评论）
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "examineComment", method = RequestMethod.POST)
	public MessageInfo examineComment(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		info.setClientStyle(MeixConstants.MS_DIALOG);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
//			long uid = MapUtils.getLongValue(object, "uid", 0);
			String uuid = MapUtils.getString(object, "uuid", null);
//			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			long commentId = MapUtils.getLongValue(object, "commentId", 0);
//			String reason = MapUtils.getString(object, "reason");
			//1-已通过，2-已拒绝，3-已撤销
			int optType = MapUtils.getIntValue(object, "optType", 0);

			InsUser userInfo = insUserService.getUserInfo(uuid);
			if (userInfo == null) {
				info.setMessage("非系统用户无法进行该操作");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			if (optType <= 0) {
				info.setMessage("暂不支持该操作类型");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			userServiceImpl.examineComment(commentId, optType, uuid);

			info.setMessage("操作成功");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("合规监控内容审核失败", e);
			info.setMessage("合规监控内容审核失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:评论合规
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "exportCommentList", method = RequestMethod.POST)
	public MessageInfo exportCommentList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		File file = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(object, "uid");
			String token = object.getString("token");
			int serviceType = MapUtils.getIntValue(object, "serviceType", 2);
			String condition = MapUtils.getString(object, "condition", null);
			int type = MapUtils.getIntValue(object, "type", 0);
			int status = MapUtils.getIntValue(object, "status", -1);//-1全部  0-未审核，1-已通过，2-已拒绝，3-已撤销
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", -1);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
//			String startTime = MapUtils.getString(object, "startTime", null);
			int sortType = MapUtils.getIntValue(object, "sortType", 0);	//排序列0-默认列（开始时间）
			int sortRule = MapUtils.getIntValue(object, "sortRule", -1); //排序规则 1 asc, -1 desc
			
			long dataId = MapUtils.getLongValue(object, "dataId", 0);
//			int status = MapUtils.getIntValue(object, "status", -1);
			String sortField = MapUtils.getString(object, "sortField", null); //排序字段
			if (StringUtils.isBlank(sortField) || "time".equalsIgnoreCase(sortField)) {
				sortType = 0;
			}
			ComplianceCommentSearchVo searchVo = new ComplianceCommentSearchVo();
			List<Integer> statusList = new ArrayList<>();
			switch (status) {
				case -1:
					statusList.add(0);
					statusList.add(1);
					statusList.add(2);
					statusList.add(3);
					break;
				case 0:
					statusList.add(0);
					break;
				case 1:
					statusList.add(1);
					break;
				case 2:
					statusList.add(2);
					break;
				case 3:
					statusList.add(3);
					break;
				default:
					info.setMessage("不支持的状态");
					info.setMessageCode(MeixConstantCode.M_1009);
					return info;
			}
			searchVo.setStatusList(statusList);
			searchVo.setType(type);
			searchVo.setCompanyCode(companyCode);
			searchVo.setCondition(condition);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			searchVo.setDataId(dataId);
			List<CommentVo> list = userServiceImpl.getCommentList(searchVo);
			if (dataId > 0 && list != null && list.size() > 0) {
				String excelName = "《" + list.get(0).getTime() + "》评论内容报表";
				Workbook wb = excelExportService.getCommentExcel(excelName, list);
				if (wb == null) {
					info.setMessageCode(MeixConstantCode.M_1009);
					info.setMessage("报表导出失败");
					return info;
				}
				if (RequestMethod.GET.toString().equals(request.getMethod())) {
					// 设置response头信息
					response.reset();
					response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
					try {
						response.setHeader("Content-disposition", "attachment; filename=" + excelName + ".xlsx");
					} catch (Exception e1) {
						log.info(e1.getMessage());
					}
	
					try {
						//将文件输出
						OutputStream ouputStream = response.getOutputStream();
						wb.write(ouputStream);
						ouputStream.flush();
						ouputStream.close();
						return null;
					} catch (Exception e) {
						log.info("导出Excel失败！");
						log.error("", e);
					}
				} else {
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					wb.write(outputStream);
					byte[] data = outputStream.toByteArray();
					outputStream.close();
					file = FileUtil.createTempFile(data, String.valueOf(uid), excelName + ".xlsx");
					if (file != null && file.exists()) {
						log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
					} else {
						info.setMessageCode(MeixConstantCode.M_1009);
						info.setMessage("导出文件异常");
						return info;
					}
					String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
					log.debug("【fileUpload】temp:{}", temp);
					if (temp != null) {
						JSONObject result = JSONObject.fromObject(temp);
						String url = null;
						if (result.getInt("messageCode") == 1008) {
							url = result.getJSONObject("object").getString("url");
						}
		
						info.setMessageCode(MeixConstantCode.M_1008);
						JSONObject obj = new JSONObject();
						obj.put("fileUrl", url);
						info.setObject(obj);
					}
				}
			} else {
				info.setMessage("导出评论管理列表失败");
				info.setMessageCode(MeixConstantCode.M_1009);
			}
		} catch (Exception e) {
			log.error("导出评论管理列表失败", e);
			info.setMessage("导出评论管理列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		} finally {
			if (file != null) {
				file.deleteOnExit();
			}
		}
		return info;
	}
}
