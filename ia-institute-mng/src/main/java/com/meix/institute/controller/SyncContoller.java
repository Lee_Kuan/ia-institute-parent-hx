package com.meix.institute.controller;

import com.meix.institute.api.IConstantService;
import com.meix.institute.api.ISecumainService;
import com.meix.institute.api.IStatDataService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.vo.IaConstantVo;
import com.meix.institute.vo.stat.HeatDataVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by zenghao on 2019/8/27.
 */
@RequestMapping("/sync/")
@RestController
public class SyncContoller {
	private Logger log = LoggerFactory.getLogger(SyncContoller.class);

	@Autowired
	private IStatDataService statDataService;
	@Autowired
	private ISecumainService secumainService;
	@Autowired
	private IConstantService constantService;

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:保存证券信息列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "saveSecumainList", method = RequestMethod.POST)
	public Object saveSecumainList(HttpServletRequest request, HttpServletResponse response, @RequestParam String dataJson) {
		MessageInfo info = new MessageInfo();
		try {
			List<SecuMain> list = GsonUtil.json2List(dataJson, SecuMain.class);
			if (list == null || list.size() == 0) {
				return info;
			}
			secumainService.saveSecumainList(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("保存证券信息列表失败", e);
			info.setMessage("保存证券信息列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:保存热度列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "saveHeatDataList", method = RequestMethod.POST)
	public Object saveHeatDataList(HttpServletRequest request, HttpServletResponse response, @RequestParam String dataJson) {
		MessageInfo info = new MessageInfo();
		try {
			List<HeatDataVo> list = GsonUtil.json2List(dataJson, HeatDataVo.class);
			if (list == null || list.size() == 0) {
				return info;
			}
			//热度列表是删除旧的数据再批量插入，同步时保持一致
			long startId = list.get(0).getId();
			long endId = list.get(list.size() - 1).getId();
			statDataService.removeHeatDataRange(startId, endId);
			statDataService.insertHeatDataList(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("保存线索池用户统计列表失败", e);
			info.setMessage("保存线索池用户统计列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:保存常量列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "saveConstantList", method = RequestMethod.POST)
	public Object saveConstantList(HttpServletRequest request, HttpServletResponse response, @RequestParam String dataJson) {
		MessageInfo info = new MessageInfo();
		try {
			List<IaConstantVo> list = GsonUtil.json2List(dataJson, IaConstantVo.class);
			if (list == null || list.size() == 0) {
				return info;
			}
			//常量是删除旧的数据再批量插入
			long startId = list.get(0).getId();
			long endId = list.get(list.size() - 1).getId();
			constantService.removeConstantRange(startId, endId);
			constantService.saveConstantList(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("保存常量列表失败", e);
			info.setMessage("保存常量列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
}
