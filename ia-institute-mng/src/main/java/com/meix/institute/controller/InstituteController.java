package com.meix.institute.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.meix.institute.BaseService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IInstituteService;
import com.meix.institute.api.IUserService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsUser;
import com.meix.institute.impl.InstituteMessagePushServiceImpl;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.vo.user.InsCustomer;
import com.meix.institute.vo.user.InsMeixUser;
import com.meix.institute.vo.user.UserInfo;

import net.sf.json.JSONObject;

/**
 * 研究所接口
 * Created by zenghao on 2019/9/4.
 */
@RestController
public class InstituteController extends BaseService {
	private static Logger log = LoggerFactory.getLogger(InstituteController.class);

	@Autowired
	private IInstituteService instituteServiceImpl;
	@Autowired
	private IInstituteMessagePushService messagePush;
	@Autowired
	private IUserService userServiceImpl;
	@Autowired
	private IInsUserService insUserService;
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:指派/修改销售
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/institute/assignSaler", method = RequestMethod.POST)
	public MessageInfo assignSaler(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(object, "uid", 0);
			String uuid = MapUtils.getString(object, "uuid", "");
			long clueUid = MapUtils.getLongValue(object, "clueUid", 0);
			long salerId = MapUtils.getLongValue(object, "salerId", 0);
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int clueType = MapUtils.getIntValue(object, "clueType", 0);
			String mobile = MapUtils.getString(object, "mobile", null);
			if (salerId <= 0 || companyCode <= 0 || StringUtil.isBlank(mobile)) {
				info.setMessage("参数有误");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			long dataId = 0;
			UserInfo userInfo = null;
//			List<CompanyClueInfo> clueList = null;
			if (clueUid > 0) {
//				ClueSearchVo searchVo = new ClueSearchVo();
//				searchVo.setCompanyCode(companyCode);
//				searchVo.setUid(clueUid);
//				searchVo.setShowNum(1);
//				clueList = statDataService.getClueList(searchVo);
				userInfo = userServiceImpl.getUserInfo(clueUid, null, null);
				dataId = clueUid;
			}
			InsUser saler = insUserService.getUserInfo(salerId);
			if (saler == null) {
				info.setMessage("不存在的销售");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			UserInfo whiteUser = userServiceImpl.getWhiteUserInfo(mobile);
			//白名单用户指派销售
			if (whiteUser != null) {
				dataId = whiteUser.getId();
				whiteUser.setSalerUid(salerId);
				userServiceImpl.updateUser(whiteUser);
				info.setMessage("操作成功");
			} else {
//				if (clueList == null || clueList.size() == 0) {
//					info.setMessage("不存在的商机");
//					info.setMessageCode(MeixConstantCode.M_1009);
//					return info;
//				}
				if (userInfo == null) {
					info.setMessage("不存在的商机");
					info.setMessageCode(MeixConstantCode.M_1009);
				}
//				InsMeixUser exist = instituteServiceImpl.getExistInsMeixUser(companyCode, clueList.get(0).getCustomerCompanyCode());
				InsMeixUser exist = instituteServiceImpl.getExistInsMeixUser(companyCode, userInfo.getCompanyCode());
				if (exist != null) {
					//已指派销售的再次指派时则修改
					exist.setSalerId(salerId);
					exist.setSalerName(saler.getUserName());
					exist.setOperatorId(uid);
					instituteServiceImpl.updateInsMeixUser(exist);
					info.setMessage("修改成功");
				} else {
					InsMeixUser insMeixUser = new InsMeixUser();
					insMeixUser.setCompanyCode(companyCode);
					insMeixUser.setUid(clueUid);
//					insMeixUser.setUserName(clueList.get(0).getUserName());
//					insMeixUser.setCustomerCompanyCode(clueList.get(0).getCustomerCompanyCode());
//					insMeixUser.setCustomerCompanyName(clueList.get(0).getCustomerCompanyAbbr());
//					insMeixUser.setPosition(clueList.get(0).getPosition());
//					insMeixUser.setMobile(clueList.get(0).getMobile());
					insMeixUser.setUserName(userInfo.getUserName());
					insMeixUser.setCustomerCompanyCode(userInfo.getCompanyCode());
					insMeixUser.setCustomerCompanyName(userInfo.getCompanyAbbr());
					insMeixUser.setPosition(userInfo.getPosition());
					insMeixUser.setMobile(userInfo.getMobile());
					insMeixUser.setSalerId(salerId);
					insMeixUser.setSalerName(saler.getUserName());
					insMeixUser.setOperatorId(uid);
					instituteServiceImpl.saveInsMeixUser(insMeixUser);
					info.setMessage("指派成功");
				}
			}
			//long companyCode, long uid, int messageType, String title, int dataType, int clueType, String mobile
			saveUserOperatorLog(uuid, dataId, MeixConstants.USER_YH_CLUE, MeixConstants.OPERATOR_ASSIGN_SALER, 0, object.toString());
			messagePush.messagePush(companyCode, clueUid, InstituteMessagePushServiceImpl.SEND_NOTICE_TOSALER, null, 0, clueType, saler.getMobile());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("操作失败", e);
			info.setMessage("操作失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:获取销售的客户清单
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/institute/getCustomersBySaler", method = RequestMethod.POST)
	public MessageInfo getCustomerListBySaler(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long salerUid = MapUtils.getLongValue(object, "salerUid", 0);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			
			List<InsCustomer> salerList = instituteServiceImpl.getCustomersBySaler(salerUid, currentPage, showNum);
			int total = 0;
			if (salerList != null && salerList.size() > 0) {
				total = instituteServiceImpl.getCustomerCountBySaler(salerUid);
			}
			Map<String, Object> map = new HashMap<>();
			map.put("total", total);
			map.put("currentPage", currentPage);
			info.setObject(map);
			info.setData(salerList);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("操作失败", e);
			info.setMessage("操作失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
}
