package com.meix.institute.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meix.institute.api.IMessageService;
import com.meix.institute.config.HxSmsConfig;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.service.HxSmsService;

@Controller
public class TestController {
	private static Logger log = LoggerFactory.getLogger(TestController.class);
	@Autowired
	private HxSmsConfig hxSmsConfig;
	@Autowired
	private IMessageService messageService;

	/**
	 * @param request
	 * @return
	 * @Title: getVerCode、
	 * @Description:获取验证码
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/test/getVerCode", method = RequestMethod.GET)
	@ResponseBody
	public MessageInfo getVerCode(HttpServletRequest request,
	                              @RequestParam String mobile) {
		MessageInfo info = new MessageInfo();
		try {

			String content = "后台管理短信测试";
			String result = HxSmsService.sendSMS(hxSmsConfig.getUrl(), hxSmsConfig.getUsername(), hxSmsConfig.getPassword(), mobile, "1", content);
			if (result == null) {
				info.setMessage("验证码发送成功！");
				info.setMessageCode(1008);
			} else {
				info.setMessage(result);
				info.setMessageCode(1009);
			}
		} catch (Exception e) {
			info.setMessage("验证码发送失败！");
			info.setMessageCode(1009);
			log.warn("getVerCode异常：", e);
		}
		return info;
	}

	/*private String getSmsContent(String templateContent, String templateParam) {
		if (templateContent == null) {
			return null;
		}

		JSONObject jsonParam = JSONObject.fromObject(templateParam);
		for (Object key : jsonParam.keySet()) {
			templateContent = templateContent.replace("${" + key + "}", jsonParam.getString((String) key));
		}

		return templateContent;
	}*/

	/**
	 * @param request
	 * @return
	 * @Title: sendEmail、
	 * @Description:发送邮件
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/test/sendEmail", method = RequestMethod.GET)
	@ResponseBody
	public MessageInfo sendEmail(HttpServletRequest request,
	                             @RequestParam String email) {
		MessageInfo info = new MessageInfo();
		try {
			String content = "测试后台管理项目邮件" + email;
			String result = messageService.sendEmailWithResult(email, "测试邮件", content);
			if (result == null) {
				info.setMessage("邮件发送成功！");
				info.setMessageCode(1008);
			} else {
				info.setMessage(result);
				info.setMessageCode(1009);
			}
		} catch (Exception e) {
			info.setMessage("邮件发送失败！");
			info.setMessageCode(1009);
			log.warn("sendEmail异常：", e);
		}
		return info;
	}
}
