package com.meix.institute.controller;

import com.meix.institute.api.IStatDataService;
import com.meix.institute.config.PubServerConfig;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.impl.ExcelExportService;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.search.ClueSearchVo;
import com.meix.institute.search.DataRecordStatSearchVo;
import com.meix.institute.search.UserRecordSearchVo;
import com.meix.institute.service.PubServiceImpl;
import com.meix.institute.util.FileUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.company.CompanyClueInfo;
import com.meix.institute.vo.stat.DataRecordStatVo;
import com.meix.institute.vo.user.UserClueStat;
import com.meix.institute.vo.user.UserRecordStatVo;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/8/2.
 */
@RequestMapping(value = "/data/")
@RestController
public class DataController {
	private Logger log = LoggerFactory.getLogger(DataController.class);

	@Autowired
	private IStatDataService statDataService;
	@Autowired
	private ExcelExportService excelExportService;
	@Autowired
	private PubServiceImpl pubService;
	@Autowired
	private PubServerConfig pubServerConfig;

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:行为数据统计总览
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getDataRecordStatList", method = RequestMethod.POST)
	public MessageInfo getDataRecordStatList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String condition = MapUtils.getString(object, "condition");
			int dataType = MapUtils.getIntValue(object, "dataType", 0);
			String startTime = MapUtils.getString(object, "startTime");
			String endTime = MapUtils.getString(object, "endTime");
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
			String sortField = MapUtils.getString(object, "sortField");
			int sortRule = MapUtils.getIntValue(object, "sortRule", -1);
			sortRule = sortRule == 0 ? -1 : sortRule;
			int sortType = 1;
			if (StringUtil.isNotEmpty(sortField)) {
				switch (sortField) {
					case "readNum":
						sortType = 1;
						break;
					case "duration":
						sortType = 2;
						break;
					case "shareNum":
						sortType = 3;
						break;
					case "startTime":
						sortType = 4;
						break;
				}
			}
			if (sortRule != 1 && sortRule != -1) {
				sortRule = -1;
			}

			DataRecordStatSearchVo searchVo = new DataRecordStatSearchVo();
			if (dataType == 0) {
				searchVo.setDataType(dataType);
			} else if (dataType == MeixConstants.USER_YB) {
				searchVo.setDataType(dataType);
			} else if (dataType == 4) {
				searchVo.setDataType(11); // 会议纪要查询条件是4, 数据存储是11
			} else {
				searchVo.setDataType(MeixConstants.USER_HD);
				searchVo.setActivityType(dataType);
			}
			searchVo.setCondition(condition);
			searchVo.setCompanyCode(companyCode);
			searchVo.setStartTime(startTime);
			searchVo.setEndTime(endTime);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			List<DataRecordStatVo> list = statDataService.getDataRecordStatList(searchVo);
			info.setData(list);
			long total = statDataService.getDataRecordStatCount(searchVo);
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("total", total);
			info.setObject(objectMap);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取合规监控列表失败", e);
			info.setMessage("获取合规监控列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	@RequestMapping(value = "exportDataRecordStatExcel", method = {RequestMethod.GET, RequestMethod.POST})
	public MessageInfo exportDataRecordStatExcel(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		File file = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			String condition = null;
			int dataType = 0;
			String startTime = null;
			String endTime = null;
			long companyCode = 0;
			long uid = MapUtils.getLongValue(obj, "uid");
			String token = obj.getString("token");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);
			int sortType = 1;
			String sortField = null;
			int sortRule = -1;

			if (RequestMethod.GET.toString().equals(request.getMethod())) {
				condition = request.getParameter("condition");
				startTime = request.getParameter("startTime");
				endTime = request.getParameter("endTime");
				String companyCodeStr = request.getParameter("companyCode");
				if (!StringUtil.isBlank(companyCodeStr)) {
					companyCode = Long.parseLong(companyCodeStr);
				}
				String dataTypeStr = request.getParameter("dataType");
				if (!StringUtil.isBlank(dataTypeStr)) {
					dataType = Integer.parseInt(dataTypeStr);
				}
				sortField = request.getParameter("sortField");
				String sortRuleStr = request.getParameter("sortRule");
				if (!StringUtil.isBlank(sortRuleStr)) {
					sortRule = Integer.parseInt(sortRuleStr);
				}
			} else {
				condition = MapUtils.getString(obj, "condition");
				dataType = MapUtils.getIntValue(obj, "dataType", 0);
				startTime = MapUtils.getString(obj, "startTime");
				endTime = MapUtils.getString(obj, "endTime");
				companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
				sortField = MapUtils.getString(obj, "sortField");
				sortRule = MapUtils.getIntValue(obj, "sortRule", -1);
			}

			if (companyCode == 0) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("参数错误");
				return info;
			}

			if (startTime != null && startTime.length() == 10) {
				startTime += " 00:00:00";
			}
			if (endTime != null && endTime.length() == 10) {
				endTime += " 23:59:59";
			}
			if (StringUtil.isNotEmpty(sortField)) {
				switch (sortField) {
					case "readNum":
						sortType = 1;
						break;
					case "duration":
						sortType = 2;
						break;
					case "shareNum":
						sortType = 3;
						break;
					case "startTime":
						sortType = 4;
						break;
				}
			}
			if (sortRule != 1 && sortRule != -1) {
				sortRule = -1;
			}
			DataRecordStatSearchVo searchVo = new DataRecordStatSearchVo();
			if (dataType == 0) {
				searchVo.setDataType(dataType);
			} else if (dataType == MeixConstants.USER_YB) {
				searchVo.setDataType(dataType);
			} else {
				searchVo.setDataType(MeixConstants.USER_HD);
				searchVo.setActivityType(dataType);
			}
			searchVo.setCondition(condition);
			searchVo.setCompanyCode(companyCode);
			searchVo.setStartTime(startTime);
			searchVo.setEndTime(endTime);
			searchVo.setCurrentPage(0);
			searchVo.setShowNum(1000);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			Workbook wb = null;
			boolean closeExcel = false;
			do {
				List<DataRecordStatVo> list = statDataService.getDataRecordStatList(searchVo);
				if (list.size() < searchVo.getShowNum()) {
					closeExcel = true;
				}
				wb = excelExportService.exportDataRecordStatExcel(list, closeExcel);
				list.clear();
				if (wb == null) {
					break;
				}
				int skipNum = (searchVo.getCurrentPage() + 1) * searchVo.getShowNum();
				searchVo.setCurrentPage(skipNum);
			} while (!closeExcel);
			if (wb == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("报表导出失败");
				return info;
			}
			String excelName = "数据总览" + StringUtils.getId24();
			if (RequestMethod.GET.toString().equals(request.getMethod())) {
				// 设置response头信息
				response.reset();
				response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
				try {
					response.setHeader("Content-disposition", "attachment; filename=" + excelName + ".xlsx");
				} catch (Exception e1) {
					log.info(e1.getMessage());
				}

				try {
					//将文件输出
					OutputStream ouputStream = response.getOutputStream();
					wb.write(ouputStream);
					ouputStream.flush();
					ouputStream.close();
					return null;
				} catch (Exception e) {
					log.info("导出Excel失败！");
					log.error(e.getMessage());
				}
			} else {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				wb.write(outputStream);
				byte[] data = outputStream.toByteArray();
				outputStream.close();
				file = FileUtil.createTempFile(data, String.valueOf(uid), excelName + ".xlsx");
				if (file != null && file.exists()) {
					log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
				} else {
					info.setMessageCode(MeixConstantCode.M_1009);
					info.setMessage("导出文件异常");
					return info;
				}
				String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
				log.debug("【fileUpload】temp:{}", temp);
				if (temp != null) {
					JSONObject result = JSONObject.fromObject(temp);
					String url = null;
					if (result.getInt("messageCode") == 1008) {
						url = result.getJSONObject("object").getString("url");
					}

					info.setMessageCode(MeixConstantCode.M_1008);
					JSONObject object = new JSONObject();
					object.put("fileUrl", url);
					info.setObject(object);
				}
			}
		} catch (Exception e) {
			log.error("导出文件失败", e);
			info.setMessage("导出文件失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:用户行为记录列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getUserRecordList", method = RequestMethod.POST)
	public MessageInfo getUserRecordList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			int dataType = MapUtils.getIntValue(object, "dataType", 0);
			long dataId = MapUtils.getLongValue(object, "dataId", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
			String sortField = MapUtils.getString(object, "sortField", null); //排序列名
			int sortType = MapUtils.getIntValue(object, "sortType", 0);    //排序列0-默认列（开始时间）
			int sortRule = MapUtils.getIntValue(object, "sortRule", -1); //排序规则 1 asc, -1 desc
			sortRule = sortRule == 0 ? -1 : sortRule;

			if (StringUtils.isBlank(sortField) || "createTime".equalsIgnoreCase(sortField)) {
				sortType = 0;
			} else if ("readNum".equalsIgnoreCase(sortField)) {
				sortType = 1;
			} else if ("duration".equalsIgnoreCase(sortField)) {
				sortType = 2;
			} else if ("shareNum".equalsIgnoreCase(sortField)) {
				sortType = 3;
			}
			dataType = dataType == 11 || dataType == 6 || dataType == 7 ? 8 : dataType;
			UserRecordSearchVo searchVo = new UserRecordSearchVo();
			searchVo.setDataId(dataId);
			searchVo.setDataType(dataType);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			List<UserRecordStatVo> list = statDataService.getUserRecordStatList(searchVo);
			info.setData(list);
			long total = statDataService.getUserRecordStatCount(searchVo);
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("total", total);
			info.setObject(objectMap);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取合规监控列表失败", e);
			info.setMessage("获取合规监控列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	@RequestMapping(value = "exportUserRecordExcel", method = {RequestMethod.GET, RequestMethod.POST})
	public MessageInfo exportUserRecordExcel(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		File file = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			String uid = MapUtils.getString(obj, "uid");
			String token = obj.getString("token");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);
			int dataType = 0;
			long dataId = 0;
			String sortField = null; //排序列名
			int sortType = 0;    //排序列0-默认列（开始时间）
			int sortRule = -1; //排序规则 1 asc, -1 desc
			if (RequestMethod.GET.toString().equals(request.getMethod())) {
				String dataTypeStr = request.getParameter("dataType");
				if (!StringUtil.isBlank(dataTypeStr)) {
					dataType = Integer.parseInt(dataTypeStr);
				}
				String dataIdStr = request.getParameter("dataId");
				if (!StringUtil.isBlank(dataIdStr)) {
					dataId = Long.parseLong(dataIdStr);
				}
				sortField = request.getParameter("sortField");
				String sortRuleStr = request.getParameter("sortRule");
				if (!StringUtil.isBlank(sortRuleStr)) {
					sortRule = Integer.parseInt(sortRuleStr);
				}
			} else {
				dataType = MapUtils.getIntValue(obj, "dataType", 0);
				dataId = MapUtils.getLongValue(obj, "dataId", 0);
				sortField = MapUtils.getString(obj, "sortType", null); //排序列名
				sortRule = MapUtils.getIntValue(obj, "sortRule", -1); //排序规则 1 asc, -1 desc
			}

			if (StringUtils.isBlank(sortField) || "createTime".equalsIgnoreCase(sortField)) {
				sortType = 0;
			} else if ("readNum".equalsIgnoreCase(sortField)) {
				sortType = 1;
			} else if ("duration".equalsIgnoreCase(sortField)) {
				sortType = 2;
			} else if ("shareNum".equalsIgnoreCase(sortField)) {
				sortType = 3;
			}

			UserRecordSearchVo searchVo = new UserRecordSearchVo();
			searchVo.setDataId(dataId);
			searchVo.setDataType(dataType);
			searchVo.setCurrentPage(0);
			searchVo.setShowNum(10000);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			List<UserRecordStatVo> list = statDataService.getUserRecordStatList(searchVo);
			Workbook wb = excelExportService.getUserRecordStatExcel(list);
			list.clear();
			if (wb == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("报表导出失败");
				return info;
			}
			String excelName = "效果分析报表" + StringUtils.getId24();
			if (RequestMethod.GET.toString().equals(request.getMethod())) {
				// 设置response头信息
				response.reset();
				response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
				try {
					response.setHeader("Content-disposition", "attachment; filename=" + excelName + ".xlsx");
				} catch (Exception e1) {
					log.info(e1.getMessage());
				}

				try {
					//将文件输出
					OutputStream ouputStream = response.getOutputStream();
					wb.write(ouputStream);
					ouputStream.flush();
					ouputStream.close();
					return null;
				} catch (Exception e) {
					log.info("导出Excel失败！");
					log.error(e.getMessage());
				}
			} else {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				wb.write(outputStream);
				byte[] data = outputStream.toByteArray();
				outputStream.close();
				file = FileUtil.createTempFile(data, String.valueOf(uid), excelName + ".xlsx");
				if (file != null && file.exists()) {
					log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
				} else {
					info.setMessageCode(MeixConstantCode.M_1009);
					info.setMessage("导出文件异常");
					return info;
				}
				String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
				log.debug("【fileUpload】temp:{}", temp);
				if (temp != null) {
					JSONObject result = JSONObject.fromObject(temp);
					String url = null;
					if (result.getInt("messageCode") == 1008) {
						url = result.getJSONObject("object").getString("url");
					}

					info.setMessageCode(MeixConstantCode.M_1008);
					JSONObject object = new JSONObject();
					object.put("fileUrl", url);
					info.setObject(object);
				}
			}
		} catch (Exception e) {
			log.error("导出文件失败", e);
			info.setMessage("导出文件失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:线索池列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getClueList", method = RequestMethod.POST)
	public MessageInfo getClueList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String condition = MapUtils.getString(object, "condition");//匹配客户机构或名字
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int issueType = MapUtils.getIntValue(object, "issueType", -1);
			int clueType = MapUtils.getIntValue(object, "clueType", -1);
			int customerType = MapUtils.getIntValue(object, "customerType", -1);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
			String sortField = MapUtils.getString(object, "sortField");
			int sortRule = MapUtils.getIntValue(object, "sortRule", -1);

			int sortType = 1;
			if (StringUtil.isNotEmpty(sortField)) {
				switch (sortField) {
					case "createTime":
						sortType = 1;
						break;
				}
			}
			if (sortRule != 1 && sortRule != -1) {
				sortRule = -1;
			}

			ClueSearchVo searchVo = new ClueSearchVo();
			searchVo.setCondition(condition);
			searchVo.setCompanyCode(companyCode);
			searchVo.setClueType(clueType);
			searchVo.setCustomerType(customerType);
			searchVo.setIssueType(issueType);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			List<CompanyClueInfo> list = statDataService.getClueList(searchVo);
			info.setData(list);
			long total = statDataService.getClueCount(searchVo);
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("total", total);
			info.setObject(objectMap);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取线索池列表失败", e);
			info.setMessage("获取线索池列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	@RequestMapping(value = "exportClueExcel", method = {RequestMethod.GET, RequestMethod.POST})
	public MessageInfo exportClueExcel(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		File file = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			String uid = MapUtils.getString(obj, "uid");
			String token = obj.getString("token");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);
			String condition = null;//匹配客户机构或名字
			int issueType = -1;
			int clueType = -1;
			int customerType = -1;
			long companyCode = 0;
			int sortType = 1;
			String sortField;
			int sortRule = -1;
			if (RequestMethod.GET.toString().equals(request.getMethod())) {
				condition = request.getParameter("condition");
				String issueTypeStr = request.getParameter("issueType");
				if (!StringUtil.isBlank(issueTypeStr)) {
					issueType = Integer.parseInt(issueTypeStr);
				}
				String clueTypeStr = request.getParameter("clueType");
				if (!StringUtil.isBlank(clueTypeStr)) {
					clueType = Integer.parseInt(clueTypeStr);
				}
				String customerTypeStr = request.getParameter("customerType");
				if (!StringUtil.isBlank(customerTypeStr)) {
					customerType = Integer.parseInt(customerTypeStr);
				}
				String companyCodeStr = request.getParameter("companyCode");
				if (!StringUtil.isBlank(companyCodeStr)) {
					companyCode = Integer.parseInt(companyCodeStr);
				}
				sortField = request.getParameter("sortField");
				String sortRuleStr = request.getParameter("sortRule");
				if (!StringUtil.isBlank(sortRuleStr)) {
					sortRule = Integer.parseInt(sortRuleStr);
				}
			} else {
				companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
				condition = MapUtils.getString(obj, "condition");//匹配客户机构或名字
				issueType = MapUtils.getIntValue(obj, "issueType", -1);
				clueType = MapUtils.getIntValue(obj, "clueType", -1);
				customerType = MapUtils.getIntValue(obj, "customerType", -1);
				sortField = MapUtils.getString(obj, "sortField");
				sortRule = MapUtils.getIntValue(obj, "sortRule", -1);
			}

			if (StringUtil.isNotEmpty(sortField)) {
				switch (sortField) {
					case "createTime":
						sortType = 1;
						break;
				}
			}
			if (sortRule != 1 && sortRule != -1) {
				sortRule = -1;
			}

			ClueSearchVo searchVo = new ClueSearchVo();
			searchVo.setCompanyCode(companyCode);
			searchVo.setCondition(condition);
			searchVo.setClueType(clueType);
			searchVo.setCustomerType(customerType);
			searchVo.setIssueType(issueType);
			searchVo.setCurrentPage(0);
			searchVo.setShowNum(1000);
			searchVo.setSortType(sortType);
			searchVo.setSortRule(sortRule);
			Workbook wb = null;
			boolean closeExcel = false;
			do {
				List<CompanyClueInfo> list = statDataService.getClueList(searchVo);
				if (list.size() < searchVo.getShowNum()) {
					closeExcel = true;
				}
				wb = excelExportService.exportClueExcel(list, closeExcel);
				list.clear();
				if (wb == null) {
					break;
				}
				int skipNum = (searchVo.getCurrentPage() + 1) * searchVo.getShowNum();
				searchVo.setCurrentPage(skipNum);
			} while (!closeExcel);
			if (wb == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("报表导出失败");
				return info;
			}
			String excelName = "线索池" + StringUtils.getId24();
			if (RequestMethod.GET.toString().equals(request.getMethod())) {
				// 设置response头信息
				response.reset();
				response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
				try {
					response.setHeader("Content-disposition", "attachment; filename=" + excelName + ".xlsx");
				} catch (Exception e1) {
					log.info(e1.getMessage());
				}

				try {
					//将文件输出
					OutputStream ouputStream = response.getOutputStream();
					wb.write(ouputStream);
					ouputStream.flush();
					ouputStream.close();
					return null;
				} catch (Exception e) {
					log.info("导出Excel失败！");
					log.error(e.getMessage());
				}
			} else {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				wb.write(outputStream);
				byte[] data = outputStream.toByteArray();
				outputStream.close();
				file = FileUtil.createTempFile(data, String.valueOf(uid), excelName + ".xlsx");
				if (file != null && file.exists()) {
					log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
				} else {
					info.setMessageCode(MeixConstantCode.M_1009);
					info.setMessage("导出文件异常");
					return info;
				}
				String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
				log.debug("【fileUpload】temp:{}", temp);
				if (temp != null) {
					JSONObject result = JSONObject.fromObject(temp);
					String url = null;
					if (result.getInt("messageCode") == 1008) {
						url = result.getJSONObject("object").getString("url");
					}

					info.setMessageCode(MeixConstantCode.M_1008);
					JSONObject object = new JSONObject();
					object.put("fileUrl", url);
					info.setObject(object);
				}
			}
		} catch (Exception e) {
			log.error("导出文件失败", e);
			info.setMessage("导出文件失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:用户线索统计列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getUserClueStatList", method = RequestMethod.POST)
	public MessageInfo getUserClueStatList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String condition = MapUtils.getString(object, "condition");//匹配客户机构
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int clueType = MapUtils.getIntValue(object, "clueType", -1);
			int customerType = MapUtils.getIntValue(object, "customerType", -1);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);

			ClueSearchVo searchVo = new ClueSearchVo();
			searchVo.setCondition(condition);
			searchVo.setCompanyCode(companyCode);
			searchVo.setClueType(clueType);
			searchVo.setCustomerType(customerType);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			List<UserClueStat> list = statDataService.getUserClueStatList(searchVo);
			info.setData(list);
			long total = statDataService.getUserClueStatCount(searchVo);
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("total", total);
			info.setObject(objectMap);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取合规监控列表失败", e);
			info.setMessage("获取合规监控列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:热度
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getHeatData", method = RequestMethod.POST)
	public MessageInfo getHeatData(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String uid = MapUtils.getString(object, "uid");
			int type = MapUtils.getIntValue(object, "type", 1); // 默认按一个月
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int dataType = MapUtils.getIntValue(object, "dataType", 1); // 默认个股
			String sortField = MapUtils.getString(object, "sortField");
			if (StringUtils.isBlank(sortField)) sortField = "MarketAttention";
			String sortRule = MapUtils.getString(object, "sortRule", "desc");
			if (StringUtils.isBlank(sortRule)) sortRule = "desc";
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);

			Map<String, Object> search = new HashMap<String, Object>();
			search.put("uid", uid);
			search.put("companyCode", companyCode);
			search.put("dataType", dataType);
			search.put("type", type);
			search.put("showNum", showNum);
			search.put("currentPage", currentPage * showNum);
			search.put("sortField", sortField);
			search.put("sortRule", sortRule);
			List<Map<String, Object>> list = statDataService.getHeatData(search);
			Map<String, Object> countMap = statDataService.getHeatDataCount(search);
			info.setData(list);
			//info.setDataCount(MapUtils.getIntValue(countMap, "count"));
			info.setObject(countMap);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取热度列表失败", e);
			info.setMessage("获取热度列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:获取分析师数据总览列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getUserReadData", method = RequestMethod.POST)
	public MessageInfo getUserReadData(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String condition = MapUtils.getString(object, "condition", "");
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);

			Map<String, Object> search = new HashMap<String, Object>();
			search.put("condition", condition);
			search.put("showNum", showNum);
			search.put("currentPage", currentPage * showNum);
			List<Map<String, Object>> list = statDataService.getUserReadData(search);
			Map<String, Object> countMap = statDataService.getUserReadDataCount(search);
			info.setData(list);
			info.setObject(countMap);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取分析师数据总览列表失败", e);
			info.setMessage("获取分析师数据总览列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:获取分析师阅读明细列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getUserReadDataByUser", method = RequestMethod.POST)
	public MessageInfo getUserReadDataByUser(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			long id = MapUtils.getLongValue(object, "id", 0);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);

			Map<String, Object> search = new HashMap<String, Object>();
			search.put("uid", id);
			search.put("showNum", showNum);
			search.put("currentPage", currentPage * showNum);
			List<Map<String, Object>> list = statDataService.getUserReadDataByUser(search);
			Map<String, Object> countMap = statDataService.getUserReadDataByUserCount(search);
			info.setData(list);
			info.setObject(countMap);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取分析师数据总览列表失败", e);
			info.setMessage("获取分析师数据总览列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
}
