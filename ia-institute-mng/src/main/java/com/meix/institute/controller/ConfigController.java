package com.meix.institute.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.meix.institute.BaseService;
import com.meix.institute.api.IConfigService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.config.PubServerConfig;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsMessagePushConfig;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.service.PubServiceImpl;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.FileUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.XssShiedUtil;
import com.meix.institute.vo.InsMessagePushConfigVo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2019/9/17.
 */
@Controller
public class ConfigController extends BaseService {
	private static final Logger log = LoggerFactory.getLogger(ConfigController.class);

	@Autowired
	private PubServiceImpl pubService;
	@Autowired
	private PubServerConfig pubServerConfig;
	@Autowired
	private IConfigService configServiceImpl;
	@Autowired
	private IInstituteMessagePushService messagePushService;

	/**
	 * 上传语音文件
	 */
	@RequestMapping(value = {"config/fileUpload"}, method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public Object fileUpload(@RequestParam(value = "file") MultipartFile multipartFile, HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo();
		JSONObject obj = null;
		File file = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = obj.getLong("uid");
			String token = obj.getString("token");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);

			if (multipartFile != null && uid > 0) {
				if (!multipartFile.isEmpty()) {
					String originFileName = multipartFile.getOriginalFilename();
					// 检测文件类型支持
					String fileType = request.getParameter("fileType");
					String suffix = originFileName.split("[.]")[1];
					if (fileType != null && fileType.contains(suffix)) {
						info.setMessageCode(MeixConstantCode.M_1009);
						info.setMessage("文件格式不正确，我们不支持*." + suffix + "类型文件.");
						return info;
					}
					file = FileUtil.createTempFile(multipartFile.getInputStream(), String.valueOf(uid), originFileName);
					if (file != null && file.exists()) {
						log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
					} else {
						info.setMessageCode(MeixConstantCode.M_1009);
						info.setMessage("文件导入有问题，请更换文件重试哦~");
						return info;
					}
					String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType);
//					log.debug("【fileUpload】temp:{}", temp);
					if (temp != null) {
					
					JSONObject result = JSONObject.fromObject(temp);
					String fileNewName = "";
					String url = "";
					if (result.getInt("messageCode") == 1008) {
						fileNewName = result.getJSONObject("object").getString("fileName");
						url = result.getJSONObject("object").getString("url");
					} else {
						log.info("【fileUpload】temp:{}", temp);
						info.setMessageCode(MeixConstantCode.M_1009);
						info.setMessage("文件导入有问题，请更换文件重试哦~");
						return info;
					}
					JSONObject back = new JSONObject();
					back.accumulate("fileOldName", originFileName);
					back.accumulate("fileNewName", fileNewName);
					back.accumulate("fileName", fileNewName);
					back.accumulate("url", url);

					info.setMessageCode(MeixConstantCode.M_1008);
					info.setObject(back);
					
					} else {
						
						info.setMessageCode(MeixConstantCode.M_1009);
						info.setMessage("文件导入有问题，请更换文件重试哦~");
					}
				}
			}
		} catch (Exception e) {
			log.error("", e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage("文件导入有问题，请更换文件重试哦~");
		} finally {
			if (file != null && file.exists()) {
				file.delete();
			}
		}
		return info;
	}
	
	@GetMapping("show/{date}/{uid}/{filename:.+}")
    public void show(HttpServletResponse response, @PathVariable("date") String date, @PathVariable("uid") String uid, @PathVariable("filename") String filename) {
        try {
            date = XssShiedUtil.stripXss(date);
            uid = XssShiedUtil.stripXss(uid);
            filename = XssShiedUtil.stripXss(filename);

            String uploadFileDir = commonServerProperties.getFileDir();
            if (uploadFileDir != null) {
            	uploadFileDir = uploadFileDir.replaceAll("upload", "");
            }
            uploadFileDir = uploadFileDir + MeixConstants.fileSeparate + date
                + MeixConstants.fileSeparate + uid;
            String path = uploadFileDir + MeixConstants.fileSeparate + filename;
            File file = new File(path);
            if (!file.exists()) {
                log.warn("不存在的文件：{}", path);
                return;
            }
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(Files.readAllBytes(Paths.get(uploadFileDir).resolve(filename)));
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            log.error("", e);
        }
    }

	/**
	 * 保存推送配置
	 */
	@RequestMapping(value = {"config/savePushConfig"}, method = {RequestMethod.POST})
	@ResponseBody
	public MessageInfo savePushConfig(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo();
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String uuid = MapUtils.getString(obj, "uuid");
			long id = MapUtils.getLongValue(obj, "id", 0);
			int operatorType = id == 0 ? MeixConstants.OPERATOR_ADD : MeixConstants.OPERATOR_UPDATE;
			int messageType = MapUtils.getIntValue(obj, "messageType");//0-短信，1-邮件
			String title = MapUtils.getString(obj, "title");
			String content = MapUtils.getString(obj, "content");
			int pushType = MapUtils.getIntValue(obj, "pushType");//0-立即推送，1-预约推送
			String pushTime = MapUtils.getString(obj, "pushTime");
			String comment = MapUtils.getString(obj, "comment"); //手动配置接收人信息
			JSONArray typeList = null;
			int receiverType = 0;
			if (obj.containsKey("receiverType")) {
				typeList = obj.getJSONArray("receiverType"); //1-白名单，2-潜在用户，4-游客
				for (Object temp : typeList) {
					receiverType += Integer.valueOf(String.valueOf(temp));
				}
			}
			comment = comment == null ? "" : comment.replaceAll("，", ",");
			InsMessagePushConfig config = new InsMessagePushConfig();
			config.setComment(comment);
			config.setContent(content);
			config.setId(id);
			config.setMessageType(messageType);
			config.setPushStatus(0);
			Date pushDate = DateUtil.stringToDate(pushTime);
			config.setPushTime(pushDate);
			config.setPushType(pushType);
			config.setReceiverType(receiverType);
			config.setTitle(title);
			config.setUpdatedAt(new Date());
			config.setUpdatedBy(uuid);
			id = configServiceImpl.saveMessagePushConfig(config);
			
			if (pushType == 0) {
				messagePushService.pushUserConfigMessage(id, messageType, receiverType, comment, title, content);
			}
			saveUserOperatorLog(uuid, id, MeixConstants.MESSAGE_PUSH, operatorType, messageType, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("", e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage("保存推送配置失败");
		}
		return info;
	}
	/**
	 * 获取推送配置列表
	 */
	@RequestMapping(value = {"config/getPushConfigList"}, method = {RequestMethod.POST})
	@ResponseBody
	public MessageInfo getPushConfigList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo();
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			int messageType = MapUtils.getIntValue(obj, "messageType", -1);//0-短信，1-邮件
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			List<InsMessagePushConfig> list = configServiceImpl.getPushConfigList(messageType, currentPage, showNum);
			info.setData(list);
			int total = configServiceImpl.getPushConfigCount(messageType);
			Map<String, Object> map = new HashMap<>();
			map.put("total", total);
			info.setObject(map);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("", e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage("保存推送配置失败");
		}
		return info;
	}
	/**
	 * 获取推送配置详情
	 */
	@RequestMapping(value = {"config/getPushConfigDetail"}, method = {RequestMethod.POST})
	@ResponseBody
	public MessageInfo getPushConfigDetail(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo();
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			int id = MapUtils.getIntValue(obj, "id");
			InsMessagePushConfigVo res = configServiceImpl.getPushConfigDetail(id);
			info.setObject(res);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("", e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage("保存推送配置失败");
		}
		return info;
	}
	
	/**
	 * 查看接收人
	 */
	@RequestMapping(value = {"config/getReceiverList"}, method = {RequestMethod.POST})
	@ResponseBody
	public MessageInfo getReceiverList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo();
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			int id = MapUtils.getIntValue(obj, "id");
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			if (id <= 0) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("获取接收人列表失败，id无效");
				return info;
			}
			InsMessagePushConfigVo vo = configServiceImpl.getPushConfigDetail(id);
			if (vo == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("获取接收人列表失败，id无效");
				return info;
			}
			List<Map<String, Object>> res = null;
			int total = configServiceImpl.getReceiverCount(id, vo.getReceiverType(), vo.getPushStatus());
			if (total > 0) {
				res = configServiceImpl.getReceiverList(id, vo.getReceiverType(), vo.getPushStatus(), vo.getMessageType(), currentPage, showNum);
			}
			res = res == null ? new ArrayList<>() : res;
			if (StringUtils.isNotBlank(vo.getComment()) && vo.getPushStatus() == 0) {
				String[] comms = vo.getComment().split(",");
				if (res.size() < showNum) {
					if (res.size() > 0 ) {
						int needNum = showNum - res.size();
						for (int i = 0; i < needNum && i < comms.length; i++) {
							Map<String, Object> resMap = new HashMap<>();
							resMap.put("contact", comms[i]);
							resMap.put("accountType", "无");
							res.add(resMap);
						}
					} else {
						int stemp = showNum * currentPage - total;
						for (int i = stemp; i<stemp + showNum && i < comms.length; i++) {
							Map<String, Object> resMap = new HashMap<>();
							resMap.put("contact", comms[i]);
							resMap.put("accountType", "无");
							res.add(resMap);
						}
					}
				}
				total += comms.length;
			}
			Map<String, Object> map = new HashMap<>();
			map.put("total", total);
			info.setData(res);
			info.setObject(map);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("", e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage("获取接收人列表失败");
		}
		return info;
	}
}
