package com.meix.institute.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.meix.institute.BaseService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IInsLoginService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IUserService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.config.PubServerConfig;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsUser;
import com.meix.institute.impl.ExcelExportService;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.search.ClueSearchVo;
import com.meix.institute.service.PubServiceImpl;
import com.meix.institute.util.FileUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.user.UserClueStat;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2019/9/25.
 */
@RequestMapping(value = "/customer/")
@RestController
public class CustomerController extends BaseService {
	private Logger log = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	private IUserService userService;
	@Autowired
	private IInsUserService insUserService;
	@Autowired
	private ExcelExportService excelExportService;
	@Autowired
	private PubServiceImpl pubService;
	@Autowired
	private PubServerConfig pubServerConfig;
	@Autowired
	private IInstituteMessagePushService messagePushService;
	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IInsLoginService insLoginService;
	@Autowired
	private ICustomerGroupService customerGroupService;

	/**
	 * @Description: 待审核用户
	 */
	@RequestMapping(value = "getUncheckedUserList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getUncheckedUserList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		Map<String, Object> params = new HashMap<>();

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);

			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			String condition = MapUtils.getString(obj, "condition", ""); // 查询条件

			params.put("showNum", showNum);
			params.put("currentPage", currentPage * showNum);
			params.put("condition", condition);
			params.put("uid", uid);
			List<Map<String, Object>> list = userService.getUncheckedUserList(params);

			info.setMessageCode(MeixConstantCode.M_1008);
			info.setData(list);
			if (list == null || list.size() == 0) {
				info.setDataCount(0);
			} else {
				int count = userService.getUncheckedUserCount(params);
				info.setDataCount(count);
			}
		} catch (Exception e) {
			info.setMessage("获取待审核用户列表异常");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("获取待审核用户列表异常", e);
		}

		return info;
	}

	/**
	 * @Description: 用户审核
	 */
	@RequestMapping(value = "auditCustomer", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo auditCustomer(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String uuid = MapUtils.getString(obj, "uuid", "");
			long customerId = MapUtils.getIntValue(obj, "customerId", 0);
			long cardId = MapUtils.getLongValue(obj, "cardId", 0);
			String userName = MapUtils.getString(obj, "userName");
			String companyName = MapUtils.getString(obj, "companyName");
			String position = MapUtils.getString(obj, "position");
			int auditResult = MapUtils.getIntValue(obj, "auditResult", 0);//审核结果 0 不通过 1通过
			String reason = MapUtils.getString(obj, "reason"); //理由
			String email = MapUtils.getString(obj, "email", "");
			long companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
			long groupId = MapUtils.getLongValue(obj, "groupId", 0);
			if (customerId == 0 || (auditResult > 0 && groupId == 0)) {
				info.setMessage("审核信息不全，请检查后重试");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			if (auditResult != 0 && StringUtils.isNullorWhiteSpace(userName, companyName, position)) {
				info.setMessage("审核信息不全，请检查后重试");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			InsUser insUser = insUserService.getUserInfo(uid);
			if (insUser == null) {
				info.setMessage("无权限操作");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			Map<String, Object> params = new HashMap<>();
			params.put("customerId", customerId);
			params.put("cardId", cardId);
			params.put("userName", userName);
			params.put("companyName", companyName);
			params.put("position", position);
			params.put("auditResult", auditResult);
			params.put("operatorId", insUser.getUser());
			params.put("reason", reason);
			params.put("companyCode", companyCode);
			params.put("uuid", uuid);
			params.put("email", email);
			params.put("groupId", groupId);
			userService.updateUserAuditStatus(params);
			// 注销微信端token
			insLoginService.logout(insUser.getMobile(), MeixConstants.CLIENT_TYPE_2);
			insLoginService.logout(insUser.getMobile(), MeixConstants.CLIENT_TYPE_3);
			try {
				messagePushService.sendCardAuthNotice(commonServerProperties.getCompanyCode(), customerId, auditResult);
			} catch (Exception e) {
				log.error("", e);
			}
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("获取待审核用户列表异常");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("获取待审核用户列表异常", e);
		}

		return info;
	}

	/**
	 * @Description: 获取用户信息列表
	 */
	@RequestMapping(value = "getCustomerList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getCustomerList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			int accountType = MapUtils.getIntValue(obj, "accountType", 0);
			String condition = MapUtils.getString(obj, "condition");
			int authStatus = MapUtils.getIntValue(obj, "authStatus", -1);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 12);
			String sortField = MapUtils.getString(obj, "sortField", null);
			int sortRule = MapUtils.getIntValue(obj, "sortRule", -1);
			long groupId = MapUtils.getLongValue(obj, "groupId", 0);
			int isExcept = MapUtils.getIntValue(obj, "isExcept", 0); // 是不是搜索组以外的，0-不是，1-是
			sortRule = sortRule == 0 ? -1 : sortRule;
			List<Map<String, Object>> list = userService.getCustomerList(accountType, authStatus, condition, currentPage, showNum, sortField, sortRule, groupId, isExcept);
			info.setData(list);
			int total = userService.getCustomerCount(accountType, authStatus, condition, groupId, isExcept);
			String groupName = "全部白名单";
			if (groupId > 0) {
				Map<String, Object> groupInfo = customerGroupService.getCustomerGroupInfo(groupId);
				if (groupInfo != null) {
					groupName = MapUtils.getString(groupInfo, "groupName", "");
				}
			} else if (groupId == -1) {
				groupName = "未分组";
			}
			Map<String, Object> map = new HashMap<>();
			map.put("groupName", groupName);
			map.put("total", total);
			map.put("currentPage", currentPage);
			info.setObject(map);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("获取待审核用户列表异常");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("获取待审核用户列表异常", e);
		}

		return info;
	}

	/**
	 * @Description: 导出用户信息列表
	 */
	@RequestMapping(value = "exportCustomerList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo exportCustomerList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		File file = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			int uid = MapUtils.getIntValue(obj, "uid");
			int accountType = MapUtils.getIntValue(obj, "accountType", 0);
			String condition = MapUtils.getString(obj, "condition");
			int authStatus = MapUtils.getIntValue(obj, "authStatus", -1);
			String token = obj.getString("token");
			int serviceType = MapUtils.getIntValue(obj, "serviceType", 2);
			long groupId = MapUtils.getLongValue(obj, "groupId", 0);
			int isExcept = MapUtils.getIntValue(obj, "isExcept", 0);
			List<Map<String, Object>> list = userService.getCustomerList(accountType, authStatus, condition, 0, -1, null, -1, groupId, isExcept);
			String excelName = "游客使用清单";

			Workbook wb = excelExportService.getCustomerExcel(excelName, list);
			if (wb == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("报表导出失败");
				return info;
			}
			if (RequestMethod.GET.toString().equals(request.getMethod())) {
				// 设置response头信息
				response.reset();
				response.setContentType("application/vnd.ms-excel"); // 改成输出excel文件
				try {
					response.setHeader("Content-disposition", "attachment; filename=" + excelName + ".xlsx");
				} catch (Exception e1) {
					log.info(e1.getMessage());
				}

				try {
					//将文件输出
					OutputStream ouputStream = response.getOutputStream();
					wb.write(ouputStream);
					ouputStream.flush();
					ouputStream.close();
					return null;
				} catch (Exception e) {
					log.info("导出Excel失败！");
					log.error("", e);
				}
			} else {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				wb.write(outputStream);
				byte[] data = outputStream.toByteArray();
				outputStream.close();
				file = FileUtil.createTempFile(data, String.valueOf(uid), excelName + ".xlsx");
				if (file != null && file.exists()) {
					log.debug("本地文件" + file.getAbsolutePath() + "缓存成功。。。");
				} else {
					info.setMessageCode(MeixConstantCode.M_1009);
					info.setMessage("导出文件异常");
					return info;
				}
				String temp = pubService.httpPostFile(file, pubServerConfig.getMutiUploadFile(), token, serviceType, false);
				log.debug("【fileUpload】temp:{}", temp);
				if (temp != null) {
					JSONObject result = JSONObject.fromObject(temp);
					String url = null;
					if (result.getInt("messageCode") == 1008) {
						url = result.getJSONObject("object").getString("url");
					}

					info.setMessageCode(MeixConstantCode.M_1008);
					JSONObject object = new JSONObject();
					object.put("fileUrl", url);
					info.setObject(object);
				}
			}
		} catch (Exception e) {
			info.setMessage("获取待审核用户列表异常");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("获取待审核用户列表异常", e);
		}

		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Description:获取已认证客户列表
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "getAuthedUserClueList", method = RequestMethod.POST)
	public MessageInfo getAuthedUserClueList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String condition = MapUtils.getString(object, "condition");//匹配客户机构
			long companyCode = MapUtils.getLongValue(object, "companyCode", 0);
			int clueType = MapUtils.getIntValue(object, "clueType", -1);
			int showNum = MapUtils.getIntValue(object, "showNum", 10);
			int currentPage = MapUtils.getIntValue(object, "currentPage", 0);
			long groupId = MapUtils.getLongValue(object, "groupId", 0);
			int isExcept = MapUtils.getIntValue(object, "isExcept", 0);

			ClueSearchVo searchVo = new ClueSearchVo();
			searchVo.setCondition(condition);
			searchVo.setCompanyCode(companyCode);
			searchVo.setClueType(clueType);
			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShowNum(showNum);
			searchVo.setGroupId(groupId);
			searchVo.setIsExcept(isExcept);
			List<UserClueStat> list = userService.getAuthedUserClueList(searchVo);
			info.setData(list);
			int total = userService.getAuthedUserClueCount(searchVo);
			String groupName = "全部已认证用户";
			if (groupId > 0) {
				Map<String, Object> groupInfo = customerGroupService.getCustomerGroupInfo(groupId);
				if (groupInfo != null) {
					groupName = MapUtils.getString(groupInfo, "groupName", "");
				}
			} else if (groupId == -1) {
				groupName = "未分组";
			}
			Map<String, Object> objectMap = new HashMap<>();
			objectMap.put("groupName", groupName);
			objectMap.put("total", total);
			info.setObject(objectMap);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("获取合规监控列表失败", e);
			info.setMessage("获取合规监控列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
	
	/**
	 * @Description: 获取用户组列表
	 */
	@RequestMapping(value = "getCustomerGroupList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getCustomerGroupList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			int groupType = MapUtils.getIntValue(obj, "groupType", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", -1);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int isSelect = MapUtils.getIntValue(obj, "isSelect", 1); //是否是用于选择，1-是，0-不是
			List<Map<String, Object>> list = customerGroupService.getCustomerGroupList(groupType, isSelect, showNum, currentPage);
			info.setData(list);
			if (isSelect == 0) {
				int total = customerGroupService.getCustomerGroupCount(groupType);
				Map<String, Object> map = new HashMap<>();
				map.put("total", total);
				map.put("currentPage", currentPage);
				info.setObject(map);
			}
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("获取用户组列表异常");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("获取用户组列表异常:{}", e);
		}

		return info;
	}
	
	/**
	 * @Description: 删除用户组
	 */
	@RequestMapping(value = "deleteCustomerGroup", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo deleteCustomerGroup(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long groupId = MapUtils.getLongValue(obj, "groupId", 0);
			String uuid = MapUtils.getString(obj, "uuid", "");
			int count = customerGroupService.deleteCustomerGroup(groupId, uuid);
			if (count == 0) {
				info.setMessage("删除用户组失败");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			saveUserOperatorLog(uuid, groupId, MeixConstants.USER_YH_GROUP, MeixConstants.OPERATOR_DELETE, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("删除用户组异常");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("删除用户组异常:{}", e);
		}

		return info;
	}
	
	/**
	 * @Description: 添加用户组，同时加入权限
	 */
	@RequestMapping(value = "saveCustomerGroup", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo saveCustomerGroup(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long groupId = MapUtils.getLongValue(obj, "groupId", 0);
			int operatorType = groupId == 0 ? MeixConstants.OPERATOR_ADD : MeixConstants.OPERATOR_UPDATE;
			String uuid = MapUtils.getString(obj, "uuid", "");
			String groupName = MapUtils.getString(obj, "groupName");
			int groupType = MapUtils.getIntValue(obj, "groupType", 0);
			if (groupId == 0 && groupType == 0) {
				info.setMessage("信息错误！");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			JSONArray powers = null;
			if (obj.containsKey("powers")) {
				powers = obj.getJSONArray("powers");
			}
			groupId = customerGroupService.saveCustomerGroup(groupId, groupName, groupType, uuid, powers);
			if (groupId < 0) {
				info.setMessage("用户组名称重复！");
				info.setMessageCode(MeixConstantCode.M_1009);
			} else {
				info.setMessageCode(MeixConstantCode.M_1008);
				Map<String, Object> map = new HashMap<>();
				map.put("groupId", groupId);
				info.setObject(map);
				saveUserOperatorLog(uuid, groupId, MeixConstants.USER_YH_GROUP, operatorType, groupType, obj.toString());
			}
		} catch (Exception e) {
			info.setMessage("添加用户组异常！");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("添加用户组异常:{}", e);
		}

		return info;
	}
	
	/**
	 * @Description: 获取用户组权限
	 */
	@RequestMapping(value = "getCustomerGroupPower", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getCustomerGroupPower(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long groupId = MapUtils.getLongValue(obj, "groupId", 0);
			int groupType = MapUtils.getIntValue(obj, "groupType", 0);
			if (groupId == 0 && groupType == 0) {
				info.setMessage("参数错误！");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			if (groupId > 0) {
				Map<String, Object> groupInfo = customerGroupService.getCustomerGroupInfo(groupId);
				info.setObject(groupInfo);
				if (groupInfo == null || (groupType > 0 && MapUtils.getIntValue(groupInfo, "groupType", 0) != groupType)) {
					info.setMessage("参数错误！");
					info.setMessageCode(MeixConstantCode.M_1009);
					return info;
				}
				if (groupType == 0) {
					groupType = MapUtils.getIntValue(groupInfo, "groupType", 0);
				}
			}
			List<Map<String, Object>> list = customerGroupService.getCustomerGroupPower(groupId, groupType);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("获取用户组权限异常！");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("获取用户组权限异常:{}", e);
		}

		return info;
	}
	
	/**
	 * @Description: 保存用户组用户
	 */
	@RequestMapping(value = "saveCustomerGroupUser", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo saveCustomerGroupUser(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long groupId = MapUtils.getLongValue(obj, "groupId", 0);
			if (groupId <= 0) {
				info.setMessage("无效的用户组！");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			String uuid = MapUtils.getString(obj, "uuid", "");

			long fromGroup = MapUtils.getLongValue(obj, "fromGroup", 0);//如果存在fromGroup，则为转移用户操作，否则为新增用户操作
			JSONArray powers= null;
			if (obj.containsKey("ids")) {
				powers = obj.getJSONArray("ids");
			}
			customerGroupService.saveCustomerGroupUser(groupId, powers, uuid, fromGroup);
			saveUserOperatorLog(uuid, groupId, MeixConstants.USER_YH, MeixConstants.OPERATOR_CHANGE_GROUP, 0, obj.toString());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("保存用户组用户异常！");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("保存用户组用户异常:{}", e);
		}

		return info;
	}
}
