package com.meix.institute.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.meix.institute.api.IDictTagService;

/**
 * 枚举拦截器
 * @author Xueyc
 *
 */
@Component
public class ThreadLocalInterceptor implements HandlerInterceptor{
    
    @Autowired
    private IDictTagService dictTagService;
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (dictTagService != null) {
            dictTagService.reset();
        }
        return true;
    }
    
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        if (dictTagService != null) {
            dictTagService.reset();
        }
    }

}
