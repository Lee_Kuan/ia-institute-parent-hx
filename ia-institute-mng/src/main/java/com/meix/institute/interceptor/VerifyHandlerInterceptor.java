package com.meix.institute.interceptor;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.landray.kmss.pro.IProSsoService;
import com.landray.kmss.pro.IProSsoServiceProxy;
import com.meix.institute.api.IInsLoginService;
import com.meix.institute.api.IUserService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsUser;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.SecurityUtil;
import com.meix.institute.vo.SecToken;

import net.sf.json.JSONObject;

/**
 * @Description:权限拦截器
 */
@Component
public class VerifyHandlerInterceptor implements HandlerInterceptor {

	@Autowired
	private IInsLoginService insLoginService;
	@Autowired
	private IUserService userService;

	private Logger log = LoggerFactory.getLogger(VerifyHandlerInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request,
	                         HttpServletResponse response, Object handler) throws Exception {
	    
		Object clientstr = request.getAttribute("clientstr");
		if (clientstr == null) {
			clientstr = request.getParameter("clientstr");
		}
		log.info("uri【{}】,请求参数：【{}】", request.getServletPath(), clientstr);
		String clientType = request.getHeader(MeixConstants.CLIENT_TYPE_HEAD);
		if (clientstr == null) {
			return false;
		}
		JSONObject jsonobject = null;
		try {
			jsonobject = JSONObject.fromObject(clientstr);
			if (clientType == null) {
				clientType = MapUtils.getString(jsonobject, "clientType", null);
				if (clientType == null) {
					return false;
				}
			}
		} catch (Exception e) {
			log.warn("clientstr： json格式化异常【{}】", e);
			return false;
		}
		//验证token是否有效
		String token = MapUtils.getString(jsonobject, "token");
		if (StringUtils.isBlank(token)) {
			sendResponse(response, MeixConstantCode.M_1012, "token令牌为空");
			return false;
		}
		try {
			String user = SecurityUtil.getUserByToken(token);
			if (user == null) {
				String meixToken = insLoginService.getTokenByLtpaToken(token, clientType);
				if (meixToken == null) {
					String loginName = null;
					try {
						IProSsoService proSsoService = new IProSsoServiceProxy();
						String ticket = proSsoService.decodeToken(token);
						JSONObject jsonData = JSONObject.fromObject(ticket);
		                String flag = jsonData.getString("flag");
		                if (flag != null && "1".equals(flag)) {
		                	loginName = jsonData.getString("result");
		                } else {
		                	log.info("单点登录校验失败:{}", jsonData);
		                	sendResponse(response, MeixConstantCode.M_1010, "单点登录token认证失败：" + jsonData.getString("result"));
		                	return false;
		                }
					} catch (Exception e) {
						log.error("单点登录校验异常：{}", e);
						sendResponse(response, MeixConstantCode.M_1010, "token认证失败");
						return false;
					}
					if (StringUtils.isBlank(loginName)) {
						sendResponse(response, MeixConstantCode.M_1010, "token认证失败");
						return false;
					}
					InsUser userInfo = userService.getUserInfoByOaName(loginName);
					if (userInfo == null) {
						sendResponse(response, MeixConstantCode.M_1010, "token认证失败");
						return false;
					}
					user = userInfo.getUser();
					meixToken = insLoginService.createToken(userInfo.getId(), user, userInfo.getCompanyCode(), userInfo.getUserState(), Integer.valueOf(clientType), token);
				}
				jsonobject.put("token", meixToken);
			} else {
				SecToken tokenDb = insLoginService.getTokenByUser(user, Integer.valueOf(clientType));
				if (tokenDb == null || !StringUtils.equals(token, tokenDb.getToken())) {
					sendResponse(response, MeixConstantCode.M_1010, "token认证失败");
					return false;
				}
			}
			//同步时需要使用此参数
			request.setAttribute("user", user);
			request.setAttribute("clientstr", jsonobject);
			return true;
		} catch (Exception e) {
			log.error("", e);
			return false;
		}
	}

	private void sendResponse(HttpServletResponse response, int messageCode, String message) {
		response.setContentType("application/json; charset=utf-8");
		MessageInfo info = new MessageInfo();
		info.setMessage(message);
		info.setMessageCode(messageCode);
		try {
			PrintWriter writer = response.getWriter();
			writer.write(GsonUtil.obj2Json(info));
			writer.flush();
			writer.close();
		} catch (IOException e) {
			log.warn("认证返回异常：{}", e);
		}
	}

}
