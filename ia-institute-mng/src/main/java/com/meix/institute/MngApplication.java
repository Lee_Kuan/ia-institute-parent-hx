package com.meix.institute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by zenghao on 2019/7/24.
 */
@SpringBootApplication
public class MngApplication {
	public static void main(String[] args) {
		SpringApplication.run(MngApplication.class, args);
	}
}
