-- // create table ia sync snd req
-- Migration SQL that makes the change goes here.
CREATE TABLE IA_SYNC_SND_REQ(
        ID                    BIGINT(20) NOT NULL AUTO_INCREMENT,
                
        SND_APP               VARCHAR(50) NOT NULL,
        REC_APP               VARCHAR(50) NOT NULL,
        METHOD                VARCHAR(50) NOT NULL,
        
        REQ_HEADER            TEXT,
        REQ_BODY              TEXT,
        
        STATE                 VARCHAR(50),
        RETRY_MAX             INT(10),
        RETRY_CNT             INT(10),
        RETRY_LAST            DATETIME,

        REMARKS               VARCHAR(500),
        CREATED_AT            DATETIME,
        CREATED_BY            VARCHAR(50),
        UPDATED_AT            DATETIME,
        UPDATED_BY            VARCHAR(50),
        
        PRIMARY KEY(ID)
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE IA_SYNC_SND_REQ;

