-- // create table IA_SYS_CODE
-- Migration SQL that makes the change goes here.
CREATE TABLE IA_SYS_CODE(
    ID               BIGINT(20) NOT NULL AUTO_INCREMENT,
    
    NAME             VARCHAR(50) NOT NULL,
    CODE             VARCHAR(50) NOT NULL,
    
    TEXT             VARCHAR(50) NOT NULL,
    SEQ              INT(10),
    UDF1             VARCHAR(50),
    UDF2             VARCHAR(50),
    UDF3             VARCHAR(50),

    REMARKS          VARCHAR(200),
    CREATED_BY       VARCHAR(50),
    CREATED_AT       DATETIME,
    UPDATED_BY       VARCHAR(50),
    UPDATED_AT       DATETIME,
    
    PRIMARY KEY (ID)
)ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COMMENT '系统枚举';
CREATE UNIQUE INDEX IA_SYS_CODE_1 ON IA_SYS_CODE(NAME,CODE);

-- //@UNDO
-- SQL to undo the change goes here.
DROP INDEX IA_SYS_CODE_1 ON IA_SYS_CODE;
DROP TABLE IA_SYS_CODE;
