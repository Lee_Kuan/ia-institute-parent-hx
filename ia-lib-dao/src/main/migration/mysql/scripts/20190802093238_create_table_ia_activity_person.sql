-- // create table ia company institute level
-- Migration SQL that makes the change goes here.
CREATE TABLE `ia_activity_person` (
	`ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
	`ACTIVITY_ID` bigint(20) NOT NULL COMMENT '活动ID',
	`USER_NAME` varchar(50) DEFAULT NULL COMMENT '用户姓名',
	`MOBILE` varchar(20) NOT NULL COMMENT '手机号',
	`COMPANY_NAME` varchar(50) DEFAULT NULL COMMENT '机构名称',
	`POSITION` varchar(50) DEFAULT NULL COMMENT '职位',
	`TYPE` int(2) NOT NULL COMMENT '报名类型 0-自主报名，1-代为报名',
	`OPERATOR_ID` varchar(50) DEFAULT NULL COMMENT '操作人ID（代为报名人）',
	`OPERATOR_NAME` varchar(50) DEFAULT NULL COMMENT '操作人姓名',
	`SALER_NAME` varchar(50) DEFAULT NULL COMMENT '对口销售',
	`STATUS` int(2) NOT NULL COMMENT '报名状态 0-未审核，1-已通过，2-已拒绝',
	`AUDIT_AT` timestamp NULL DEFAULT NULL COMMENT '审核时间',
	`CREATED_AT` datetime NOT NULL COMMENT '创建时间',
	`CREATED_BY` varchar(50) NOT NULL COMMENT '创建人',
	`UPDATED_AT` datetime NOT NULL COMMENT '更新时间',
	`UPDATED_BY` varchar(50) NOT NULL COMMENT '更新人',
	PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COMMENT='活动报名人员';



-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE IA_ACTIVITY_PERSON;

