-- // create table ia_morning_meeting
-- Migration SQL that makes the change goes here.

CREATE TABLE `ia_morning_meeting` (
  `ID` bigint (20) NOT NULL AUTO_INCREMENT,
  `COMPANY_CODE` BIGINT (20) DEFAULT NULL COMMENT '机构代码',
  `TITLE` VARCHAR (200) COMMENT '标题',
  `CONTENT` LONGTEXT COMMENT '内容',
  `CREATED_BY` VARCHAR (50),
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` VARCHAR (50),
  `UPDATED_AT` datetime DEFAULT NULL,
  
	OPERATOR_UID varchar(50) comment '操作人id',
	OPERATOR_NAME varchar(50) comment '操作人姓名',
	UNDO_REASON VARCHAR(200) COMMENT '撤销原因',
	REFUSE_REASON VARCHAR(200) COMMENT '拒绝原因',
	STATUS INT(2) COMMENT '状态：0-未审核，1-通过，2-拒绝，3-撤销',
	URL VARCHAR(200) COMMENT 'url',
  PRIMARY KEY (`ID`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 COMMENT = '晨会';

-- //@UNDO
-- SQL to undo the change goes here.

drop table ia_morning_meeting;
