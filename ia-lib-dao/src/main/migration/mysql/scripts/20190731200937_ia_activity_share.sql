--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // ia_activity_share
-- Migration SQL that makes the change goes here.
CREATE TABLE `ia_activity_share` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVITY_ID` bigint(20) NOT NULL COMMENT '活动ID',
  `SHARE_TYPE` int(2) NOT NULL COMMENT '分享类型 0全市场 1 机构  2研究员 3群组  4买方 5卖方 7 公司类别 11 睿行研合同用户 12 具有新财富投票权用户 13 机构白名单客户 18-研究所白名单客户（研究所新增）',
  `DM` bigint(20) NOT NULL COMMENT '分享代码',
  `CREATED_AT` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `CREATED_BY` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '创建人',
  `UPDATED_AT` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `UPDATED_BY` VARCHAR(50) NOT NULL COMMENT '更新人',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `activity_share_index` (`ACTIVITY_ID`,`SHARE_TYPE`,`DM`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9145 DEFAULT CHARSET=utf8mb4 COMMENT='活动权限表';


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE ia_activity_share;

