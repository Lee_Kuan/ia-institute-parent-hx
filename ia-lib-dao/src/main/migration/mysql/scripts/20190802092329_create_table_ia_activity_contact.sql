-- // create table ia company institute level
-- Migration SQL that makes the change goes here.

CREATE TABLE `ia_activity_contact` (
	`ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
	`ACTIVITY_ID` bigint(20) NOT NULL COMMENT '活动ID',
	`UID` bigint(20) NOT NULL DEFAULT '0' COMMENT '联系人ID',
	`CONTACT` varchar(50) DEFAULT NULL COMMENT '联系人、可接入电话姓名、描述',
	`MOBILE` varchar(50) DEFAULT NULL COMMENT '联系人、可接入电话',
	`CREATED_AT` datetime NOT NULL COMMENT '创建时间',
	`CREATED_BY` varchar(50) NOT NULL COMMENT '创建人',
	`UPDATED_AT` datetime NOT NULL COMMENT '更新时间',
	`UPDATED_BY` varchar(50) NOT NULL COMMENT '更新人',
	PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='联系人、可接入电话';



-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE IA_ACTIVITY_CONTACT;

