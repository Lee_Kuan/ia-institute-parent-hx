-- // create table ia company institute level
-- Migration SQL that makes the change goes here.
CREATE TABLE `ia_secumain` (
	`ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
	`INNER_CODE` int(11) NOT NULL COMMENT '股票内码',
	`SECU_CODE` varchar(10) NOT NULL COMMENT '股票代码',
	`SECU_ABBR` varchar(20) NOT NULL COMMENT '股票名称',
	`SECU_MARKET` int(11) NOT NULL,
	`SECU_CATEGORY` int(11) NOT NULL COMMENT '股票分类',
	`INDUSTRY_CODE` int(11) NOT NULL COMMENT '行业代码',
	`INDUSTRY_NAME` varchar(20) NOT NULL COMMENT '行业名称',
	`SUFFIX` varchar(10) NOT NULL COMMENT '交易所代码',
	PRIMARY KEY (`ID`),
	UNIQUE KEY `inner_code` (`INNER_CODE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=951 DEFAULT CHARSET=utf8mb4 COMMENT='证券信息表';



-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE IA_SECUMAIN;

