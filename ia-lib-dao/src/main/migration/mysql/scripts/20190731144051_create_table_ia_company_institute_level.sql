-- // create table ia company institute level
-- Migration SQL that makes the change goes here.
CREATE TABLE IA_COMPANY_INSTITUTE_LEVEL(
    ID 							BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
    
	LEVEL						VARCHAR(50) NOT NULL COMMENT '唯一标识UUID',
    COMPANY_CODE        		BIGINT(20) NOT NULL DEFAULT 0 COMMENT '公司',
    LEVEL_NAME                	VARCHAR(50) COMMENT '等级名称',
	
    CREATED_BY 					VARCHAR(20) NOT NULL,
    CREATED_AT 					DATETIME NOT NULL,
    UPDATED_BY 					VARCHAR(20) NOT NULL,
    UPDATED_AT 					DATETIME NOT NULL,
    
    PRIMARY KEY (ID)
)ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT '白名单服务等级';

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE IA_COMPANY_INSTITUTE_LEVEL;

