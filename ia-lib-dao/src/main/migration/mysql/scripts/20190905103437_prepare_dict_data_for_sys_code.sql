-- // prepare dict data for sys code
-- Migration SQL that makes the change goes here.
DELETE FROM IA_SYS_CODE WHERE NAME IN ('POSITION');

INSERT INTO IA_SYS_CODE (NAME, CODE, TEXT, SEQ, UDF1, UDF2, UDF3) VALUES 
('CATEGORY', 'POSITION', '职位', NULL, '', '', ''),
('POSITION', 'FUND_MANAGER', '基金经理', NULL, '', '', ''),
('POSITION', 'SENIOR_RESEARCHER', '高级研究员', NULL, '', '', ''),
('POSITION', 'DIRECTOR', '董事', NULL, '', '', ''),
('POSITION', 'SENIOR_ANALYST', '高级分析师', NULL, '', '', '');
-- //@UNDO
-- SQL to undo the change goes here.


