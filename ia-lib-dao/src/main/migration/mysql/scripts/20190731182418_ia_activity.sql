--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // ia_activity
-- Migration SQL that makes the change goes here.
CREATE TABLE `IA_ACTIVITY` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVITY_STATUS` INT(2) NOT NULL COMMENT '状态：0-未审核，1-已通过，2-已拒绝，3-已撤销',
  `COMPANY_CODE` bigint(20) NOT NULL COMMENT '公司代码',
  `COMPANY_ABBR` varchar(50) NOT NULL COMMENT '发起人机构名称(创建人机构)',
  `ACTIVITY_TYPE` INT(2) NOT NULL COMMENT '活动类型：6 调研 7电话会议 8路演 11策略 12会议',
  `TITLE` varchar(200) NOT NULL COMMENT '标题',
  `RESEARCH_OBJECT` varchar(50) DEFAULT NULL COMMENT '调研对象或宏观策略',
  `START_TIME` DATETIME NOT NULL COMMENT '开始时间',
  `END_TIME` DATETIME NOT NULL COMMENT '结束时间',
  `APPLY_DEADLINE_TIME` datetime DEFAULT NULL COMMENT '报名截止时间',
  `ADDRESS` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `MAP_POSITION` varchar(200) DEFAULT NULL COMMENT '地图方位',
  `ACTIVITY_DESC` VARCHAR(500) NOT NULL COMMENT '活动描述',
  `CITY` VARCHAR(50) DEFAULT NULL COMMENT '城市',
  `CLOSE_TIME` datetime DEFAULT NULL COMMENT '关闭时间',
  `FLAG` smallint(6) DEFAULT '1' COMMENT '操作标志：1激活 -1 取消',
  `LATITUDE` int(11) DEFAULT '0' COMMENT '纬度: 做整数处理(10000)',
  `LONGITUDE` int(11) DEFAULT '0' COMMENT '经度：做整数处理',
  `OPEN_FLAG` smallint(6) DEFAULT '0' COMMENT '开发标志： 0不开放 1 开放',
  `URL` varchar(500) DEFAULT NULL COMMENT '外部链接地址',
  `FILE_ATTR` varchar(800) DEFAULT NULL COMMENT '文件属性:json方式存放',
  `SUB_TYPE` int(2) NOT NULL DEFAULT '0' COMMENT '子类型 0普通类型 1金组合',
  `ROOM_NO` int(6) NOT NULL DEFAULT '0' COMMENT '直播间编号',
  `IS_END` smallint(2) DEFAULT NULL COMMENT '会议状态 NULL：根据会议时间判断会议状态 3 回放 4 进行中',
  `RESOURCE_URL` varchar(500) DEFAULT NULL COMMENT '活动海报链接地址',
  `RESOURCE_URL_SMALL` varchar(500) DEFAULT NULL COMMENT '活动海报链接地址(小图)',
  `PLAY_TYPE` smallint(2) DEFAULT '0' COMMENT '播放类型 0 未填写(默认) 1 直播 2 转播 3 录音 ',
  `AUDIO_HIDE_DAY` int(6) NOT NULL DEFAULT '0' COMMENT '录音隐藏天数',
  `SUMMARY_URL` varchar(400) DEFAULT NULL COMMENT '纪要文字链接',
  `PPT_IMG` VARCHAR(500) COMMENT '电话会议ppt',
  `IS_HIDEN` INT(2) NOT NULL DEFAULT '1' COMMENT '是否隐藏 0-否，1-是 （研究所新增）',
  `MEETING_TYPE` INT(2) NOT NULL DEFAULT '0' COMMENT '会议类型 0-非直播会议，1-直播263, 3-直播录音 （研究所新增）',
  `MEETING_LABEL` INT(2) NOT NULL DEFAULT '0' COMMENT '会议标签 0-普通会议， 1-专场会议， 2-付费会议， 3-易私募精品课',
  `THIRD_ID` varchar(50) DEFAULT NULL COMMENT '第三方唯一标识',

  `OPERATOR_UID` VARCHAR(50) COMMENT '操作人UID',
  `OPERATOR_NAME` VARCHAR(50) COMMENT '操作人姓名',
  `UNDO_REASON` VARCHAR(200) COMMENT '撤销原因',
  `REFUSE_REASON` VARCHAR(200) COMMENT '拒绝原因',

  `CREATED_AT` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `CREATED_BY` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '创建人',
  `UPDATED_AT` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `UPDATED_BY` VARCHAR(50) NOT NULL COMMENT '更新人',
  PRIMARY KEY (`ID`),
  KEY `ia_activity_startTime` (`START_TIME`,`LATITUDE`,`LONGITUDE`) USING BTREE,
  KEY `index_activityType` (`ACTIVITY_TYPE`) USING BTREE,
  KEY `index_id_type` (`ID`,`ACTIVITY_TYPE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28431 DEFAULT CHARSET=utf8mb4 COMMENT='活动';


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE IA_ACTIVITY;

