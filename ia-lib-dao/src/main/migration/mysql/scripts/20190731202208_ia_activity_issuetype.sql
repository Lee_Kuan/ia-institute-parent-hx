--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // ia_activity_issuetype
-- Migration SQL that makes the change goes here.
CREATE TABLE `IA_ACTIVITY_ISSUETYPE` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVITY_ID` bigint(20) NOT NULL COMMENT '活动ID',
  `ISSUE_TYPE` int(2) NOT NULL COMMENT '发布渠道 0-美市机构主页 1-公众号 2-短信 3-邮件',
  `CREATED_AT` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `CREATED_BY` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '创建人',
  `UPDATED_AT` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `UPDATED_BY` VARCHAR(50) NOT NULL COMMENT '更新人',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `activity_issue_index` (`ACTIVITY_ID`,`ISSUE_TYPE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9145 DEFAULT CHARSET=utf8mb4 COMMENT='活动发布渠道表';


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE IA_ACTIVITY_ISSUETYPE;

