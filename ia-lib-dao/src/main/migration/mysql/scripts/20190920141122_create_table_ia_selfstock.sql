-- // create table ia_selfstock
-- Migration SQL that makes the change goes here.

CREATE TABLE `ia_selfstock` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UID` bigint(20) NOT NULL COMMENT '用户ID',
  `INNER_CODE` int(11) DEFAULT NULL,
  `IS_DELETED` int(2) DEFAULT '0' COMMENT '1 删除  0 未删除',
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDEX_1` (`INNER_CODE`),
  KEY `INDEX_2` (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='自选股';



-- //@UNDO
-- SQL to undo the change goes here.
drop table ia_selfstock;

