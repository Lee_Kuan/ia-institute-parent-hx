-- // create table ia company institute user
-- Migration SQL that makes the change goes here.
CREATE TABLE IA_COMPANY_INSTITUTE_USER(
    ID 							BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
    
	USER_NAME					VARCHAR(50)	NOT NULL COMMENT '用户名',
	COMPANY_CODE				BIGINT(20)	NOT NULL COMMENT '所属白名单机构ID',
	COMPANY_NAME				VARCHAR(50)	COMMENT '用户公司名称',
	POSITION					VARCHAR(50)	COMMENT '职位',
	MOBILE						VARCHAR(20)	NOT NULL COMMENT '手机号',
	SALER_UID					BIGINT(20)	NOT NULL DEFAULT 0 COMMENT '对口销售ID',
	SALER_NAME					VARCHAR(50)	COMMENT '对口销售名字',
	EXPIRE_TIME					TIMESTAMP	NULL DEFAULT NULL COMMENT '到期时间',
	CREATED_VIA					VARCHAR(50)	NOT NULL COMMENT '创建方式',

    CREATED_BY 					VARCHAR(20) NOT NULL,
    CREATED_AT 					DATETIME	NOT NULL,
    UPDATED_BY 					VARCHAR(20) NOT NULL,
    UPDATED_AT 					DATETIME NOT NULL,
    
    PRIMARY KEY (ID)
)ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT '白名单人员';


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE IA_COMPANY_INSTITUTE_USER;

