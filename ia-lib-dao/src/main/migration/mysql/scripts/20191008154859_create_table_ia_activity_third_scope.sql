-- // create table ia activity third scope
-- Migration SQL that makes the change goes here.
CREATE TABLE IA_ACTIVITY_THIRD_SCOPE(
        ID                    BIGINT(20) NOT NULL AUTO_INCREMENT,
                
        ACTIVITY_ID           BIGINT(20) NOT NULL DEFAULT 0 COMMENT '活动ID',
        SCOPE                 VARCHAR(20) NOT NULL COMMENT '活动权限',

        CREATED_AT            DATETIME,
        CREATED_BY            VARCHAR(50),
        UPDATED_AT            DATETIME,
        UPDATED_BY            VARCHAR(50),
        
        PRIMARY KEY(ID)
)ENGINE=INNODB DEFAULT CHARSET=UTF8;
CREATE INDEX IA_ACTIVITY_THIRD_SCOPE_IDX_1 ON IA_ACTIVITY_THIRD_SCOPE(ACTIVITY_ID, SCOPE);

-- //@UNDO
-- SQL to undo the change goes here.
DROP INDEX IA_ACTIVITY_THIRD_SCOPE_IDX_1 ON IA_ACTIVITY_THIRD_SCOPE;
DROP TABLE IA_ACTIVITY_THIRD_SCOPE;

