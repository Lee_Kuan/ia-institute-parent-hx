-- // create table ia company institute user
-- Migration SQL that makes the change goes here.
CREATE TABLE IA_COMPANY_INSTITUTE_USER_LEVEL(
    ID 							BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
    
	WID							BIGINT(20)	NOT NULL COMMENT '白名单用户的ID',
	LEVEL_ID					BIGINT(20)	NOT NULL COMMENT '服务等级ID',
	LEVEL_NAME					VARCHAR(50)	COMMENT '服务等级名称',

    CREATED_BY 					VARCHAR(50) NOT NULL,
    CREATED_AT 					DATETIME NOT NULL,
    UPDATED_BY 					VARCHAR(50) NOT NULL,
    UPDATED_AT 					DATETIME NOT NULL,
    
    PRIMARY KEY (ID)
)ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT '白名单人员服务等级信息';


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE IA_COMPANY_INSTITUTE_USER_LEVEL;

