-- // create table ia_report_relation
-- Migration SQL that makes the change goes here.

CREATE TABLE `ia_report_relation` (
  `RESEARCH_ID` bigint(20) NOT NULL COMMENT '研报id，唯一',
  `CONTENT` varchar(50) COMMENT '内容描述',
  `CODE` varchar(20) comment '代码',
  `NO` int(2) NOT NULL DEFAULT 0 comment '优先级，0最大',
  `TYPE` int(2) NOT NULL COMMENT '内容类别 0-rate，1-author，2-innerCode，3-industryCode',
  `CREATED_AT` datetime NOT NULL,
  `CREATED_BY` VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='研报附加信息表';

-- //@UNDO
-- SQL to undo the change goes here.

drop table ia_report_relation;
