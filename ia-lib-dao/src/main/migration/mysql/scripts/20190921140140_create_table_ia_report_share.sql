-- // create table ia_report_share
-- Migration SQL that makes the change goes here.

CREATE TABLE `ia_report_share` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `RESEARCH_ID` BIGINT(20) NOT NULL COMMENT '研报ID',
  `SHARE_TYPE` INT(2) NOT NULL COMMENT '分享类型 0全市场 1 机构  2研究员 3群组  4买方 5卖方 7 公司类别 13 机构白名单客户 18-研究所白名单客户（研究所新增）',
  `DM` BIGINT(20) NOT NULL COMMENT '分享代码',
  `CREATED_AT` DATETIME NOT NULL,
  `CREATED_BY` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `meeting_share_index` (`research_id`,`share_type`,`DM`) USING BTREE
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='研报权限表';


-- //@UNDO
-- SQL to undo the change goes here.

drop table ia_report_share;
