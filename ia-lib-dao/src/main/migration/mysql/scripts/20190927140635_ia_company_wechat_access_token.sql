--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // ia_company_wechat_access_token
-- Migration SQL that makes the change goes here.
CREATE TABLE `ia_company_wechat_access_token` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `COMPANY_CODE` bigint(20) NOT NULL COMMENT '公众号所属机构',
  `TYPE` int(2) NOT NULL COMMENT '类型 1-AccessToken 2-JsApiTicket',
  `ACCESS_TOKEN` varchar(300) DEFAULT NULL COMMENT '微信token',
  `EXPIRES_IN` bigint(11) DEFAULT '0' COMMENT '过期时间(秒)',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'TOKEN更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='微信公众号配置表';


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE `ia_company_wechat_access_token`;

