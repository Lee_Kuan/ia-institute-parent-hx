-- // create table ia_user_label_score
-- Migration SQL that makes the change goes here.

CREATE TABLE `ia_user_label_score` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `Uid` bigint(20) DEFAULT NULL COMMENT 'Customer_ID',
  `DataId` bigint(20) DEFAULT NULL COMMENT '数据ID',
  `DataType` int(2) DEFAULT NULL COMMENT '数据类型',
  `DataStatus` int(2) NOT NULL DEFAULT '0' COMMENT '数据有效状态0-有效，1-失效',
  `Score` int(11) DEFAULT NULL COMMENT '得分',
  `UpdateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CreateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户画像得分统计表';


-- //@UNDO
-- SQL to undo the change goes here.

drop table ia_user_label_score;
