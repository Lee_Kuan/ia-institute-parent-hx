-- // create table ia user third role
-- Migration SQL that makes the change goes here.
CREATE TABLE IA_USER_THIRD_ROLE(
        ID                    BIGINT(20) NOT NULL AUTO_INCREMENT,
                
        UID              	  BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户Id',
        ROLE               	  VARCHAR(20) NOT NULL COMMENT '用户角色',

        CREATED_AT            DATETIME,
        CREATED_BY            VARCHAR(50),
        UPDATED_AT            DATETIME,
        UPDATED_BY            VARCHAR(50),
        
        PRIMARY KEY(ID)
)ENGINE=INNODB DEFAULT CHARSET=UTF8;
CREATE INDEX IA_USER_THIRD_ROLE_IDX_1 ON IA_USER_THIRD_ROLE(UID, ROLE);

-- //@UNDO
-- SQL to undo the change goes here.
DROP INDEX IA_USER_THIRD_ROLE_IDX_1 ON IA_USER_THIRD_ROLE;
DROP TABLE IA_USER_THIRD_ROLE;

