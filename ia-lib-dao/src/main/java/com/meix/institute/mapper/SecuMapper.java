package com.meix.institute.mapper;

import com.meix.institute.entity.InsCompsIndustry;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.vo.secu.SecuMainVo;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 股票查询
 * by：likuan	2019年8月2日13:32:56
 */
@Repository
public class SecuMapper {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public List<SecuMain> getSecuMain(int type, int showNum, String condition, String nameCondition, int industryCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condition", condition);
		params.put("industryCode", industryCode);
		params.put("type", type);
		params.put("showNum", showNum);

		params.put("nameCondition", nameCondition);
		List<SecuMain> list = sqlSessionTemplate.selectList("secuMainMapper.getSecuMain", params);
		return list;
	}

	public List<SecuMain> getSecuMainForHK(int type, int showNum, String condition, String nameCondition, int industryCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condition", condition);
		params.put("industryCode", industryCode);
		params.put("type", type);
		params.put("showNum", showNum);
		params.put("nameCondition", nameCondition);
		List<SecuMain> list = sqlSessionTemplate.selectList("secuMainMapper.getSecuMainForHK", params);
		return list;
	}

	public List<SecuMainVo> getActivityTargetList(long activityId) {
		return sqlSessionTemplate.selectList("secuMainMapper.getActivityTarget", activityId);
	}
	
	public InsCompsIndustry getCompsIndustryByLabelId(String labelId) {
	    return sqlSessionTemplate.selectOne("secuMainMapper.getCompsIndustryByLabelId", labelId);
	}

    public List<Map<String, Object>> findCompsIndustry(Map<String, Object> search) {
        return sqlSessionTemplate.selectList("secuMainMapper.findCompsIndustry", search);
    }

    public int countCompsIndustry(Map<String, Object> search) {
        return sqlSessionTemplate.selectOne("secuMainMapper.countCompsIndustry", search);
    }
    
    /**
     * 根据股票获取comps行业
     * @param dataId
     * @return
     */
    public String getCompsIndustryByInnerCode(int innerCode) {
        return sqlSessionTemplate.selectOne("secuMainMapper.getCompsIndustryByInnerCode", innerCode);
    }
}
