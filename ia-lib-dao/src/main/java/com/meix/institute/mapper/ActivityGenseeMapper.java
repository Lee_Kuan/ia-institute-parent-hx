package com.meix.institute.mapper;

import com.meix.institute.vo.activity.gensee.ActivityGenseeRecord;
import com.meix.institute.vo.activity.gensee.ActivityThirdJoinInfo;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 活动收听记录dao
 * Created by zenghao on 2020/4/15.
 */
@Repository
public class ActivityGenseeMapper {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public List<Long> getUnsyncActivityId(String startDate, String endDate) {
		Map<String, Object> params = new HashMap<>();
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		return sqlSessionTemplate.selectList("activityGenseeMapper.getUnsyncActivityId", params);
	}

	public int deleteGenseeRecord(long activityId) {
		Map<String, Object> params = new HashMap<>();
		params.put("activityId", activityId);
		return sqlSessionTemplate.delete("activityGenseeMapper.deleteGenseeRecord", params);
	}

	public int saveGenseeRecordList(List<ActivityGenseeRecord> list) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", list);
		return sqlSessionTemplate.insert("activityGenseeMapper.saveGenseeRecordList", params);
	}

	public List<Long> getUnsyncTeleActivityId(String startDate, String endDate) {
		Map<String, Object> params = new HashMap<>();
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		return sqlSessionTemplate.selectList("activityGenseeMapper.getUnsyncTeleActivityId", params);
	}

	public int deleteTeleJoinRecord(long activityId) {
		Map<String, Object> params = new HashMap<>();
		params.put("activityId", activityId);
		return sqlSessionTemplate.delete("activityGenseeMapper.deleteTeleJoinRecord", params);
	}

	public int saveTeleJoinRecordList(List<ActivityThirdJoinInfo> list) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", list);
		return sqlSessionTemplate.insert("activityGenseeMapper.saveTeleJoinRecordList", params);
	}
}
