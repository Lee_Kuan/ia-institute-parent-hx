package com.meix.institute.mapper;

import java.util.List;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsSyncRecReq;
import com.meix.institute.pojo.InsSyncRecReqS;

public interface InsSyncRecReqMapper extends MeixBaseMapper<InsSyncRecReq> {
    
    /**
     * 查找异常同步数据
     * @return
     */
    List<InsSyncRecReq> findExceptionReq();
    
    /**
     * 列表查询
     * @param search
     * @return
     */
    List<InsSyncRecReq> find(InsSyncRecReqS daoSearch);
    
    /**
     * 统计
     * @param daoSearch
     * @return
     */
    int count(InsSyncRecReqS daoSearch);
}