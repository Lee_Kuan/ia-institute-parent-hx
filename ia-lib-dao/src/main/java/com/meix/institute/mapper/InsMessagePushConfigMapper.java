package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsMessagePushConfig;

public interface InsMessagePushConfigMapper extends MeixBaseMapper<InsMessagePushConfig> {
    
}