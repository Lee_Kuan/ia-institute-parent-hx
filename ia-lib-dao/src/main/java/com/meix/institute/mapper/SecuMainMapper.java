package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.SecuMain;

public interface SecuMainMapper extends MeixBaseMapper<SecuMain> {
}