package com.meix.institute.mapper;

import com.meix.institute.api.IUserDao;
import com.meix.institute.search.ClueSearchVo;
import com.meix.institute.search.ComplianceCommentSearchVo;
import com.meix.institute.vo.Comment;
import com.meix.institute.vo.CommentVo;
import com.meix.institute.vo.IA_PWDMapping;
import com.meix.institute.vo.user.*;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @类 说  明 : 研究所用户持久层
 */
@Repository
public class UserDaoImpl implements IUserDao {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public UserDaoImpl() {
	}

	@Override
	public List<UserInfo> getUser(Map<String, Object> params) {
		List<UserInfo> u = sqlSessionTemplate.selectList("userMapper.getUserList", params);
		return u;
	}

	@Override
	public List<Map<String, Object>> getUncheckedUserList(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("userMapper.getUncheckedUserList", params);
	}

	@Override
	public int getUncheckedUserCount(Map<String, Object> params) {
		return sqlSessionTemplate.selectOne("userMapper.getUncheckedUserCount", params);
	}

	@Override
	public void addUser(UserInfo user) {
		sqlSessionTemplate.insert("userMapper.addUser", user);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int deleteUser(Map params) {
		return sqlSessionTemplate.update("userMapper.deleteUser", params);
	}

	@Override
	public void saveVerCode(IA_PWDMapping bean) {
		sqlSessionTemplate.insert("userMapper.saveVerCode", bean);
	}

	@Override
	public UserInfo login(UserInfo bean) {
		return null;
	}

	@Override
	public IA_PWDMapping getUserVerCode(String mobileOrEmail, int loginType, String tday) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobileOrEmail", mobileOrEmail);
		params.put("tday", tday);
		params.put("loginType", loginType);
		IA_PWDMapping obj = sqlSessionTemplate.selectOne("userMapper.getUserVerCode", params);
		if (obj == null) return null;
		return obj;
	}

	@Override
	public boolean updateVerCode(String mobileOrEmail, int loginType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobileOrEmail", mobileOrEmail);
		params.put("loginType", loginType);
		sqlSessionTemplate.update("userMapper.updateVerCode", params);
		return true;
	}

	@Override
	public int loginOut(String token) {
		int updateState = sqlSessionTemplate.update("userMapper.loginOut", token);
		return updateState;
	}

	@Override
	public UserInfo getUserInfo(long uid, String areaCode, String mobileOrEmail) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("mobileOrEmail", mobileOrEmail);
		params.put("areaCode", areaCode);
		UserInfo bean = sqlSessionTemplate.selectOne("userMapper.getUserInfo", params);
		return bean;
	}

	@Override
	public UserInfo getUserInfo(long uid, String areaCode, String mobileOrEmail, int isWhite) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("mobileOrEmail", mobileOrEmail);
		params.put("areaCode", areaCode);
		params.put("isWhite", isWhite);
		UserInfo bean = sqlSessionTemplate.selectOne("userMapper.getUserInfo", params);
		return bean;
	}

	@Override
	public UserInfo getUserInfoByUuid(String user) {
		Map<String, Object> params = new HashMap<>();
		params.put("uuid", user);
		return sqlSessionTemplate.selectOne("userMapper.getUserInfoByUuid", params);
	}

	@SuppressWarnings("rawtypes")
	public List<Map> getUserByMobile(String mobile) {
		Map<String, String> params = new HashMap<String, String>();
		if (!StringUtils.isEmpty(mobile)) {
			params.put("mobile", mobile);
		}
		return sqlSessionTemplate.selectList("userMapper.getUserByMobile", params);
	}

	public UserInfo getUserByAccount(String account, String password) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("account", account);
		params.put("password", password);
		return sqlSessionTemplate.selectOne("userMapper.getUserByAccount", params);
	}

	@Override
	public int updateUser(UserInfo user) {
		return sqlSessionTemplate.update("userMapper.updateUser", user);
	}

	@Override
	public int userAuditNotPass(long uid, String operatorId, String reason) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("operatorId", operatorId);
		params.put("reason", reason);
		return sqlSessionTemplate.update("userMapper.userAuditNotPass", params);
	}

	@Override
	public int userAuditPass(long uid, String userName, String operatorId, long companyCode, String position, int auditResult, long cardId, String email) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("userName", userName);
		params.put("operatorId", operatorId);
		params.put("companyCode", companyCode);
		params.put("position", position);
		params.put("auditResult", auditResult);
		params.put("cardId", cardId);
		params.put("email", email);
		return sqlSessionTemplate.update("userMapper.userAuditPass", params);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int getUserCount(Map params) {
		return sqlSessionTemplate.selectOne("userMapper.getUserCount", params);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int updateUserPassword(Map params) {
		return sqlSessionTemplate.update("userMapper.updateUserPassword", params);
	}

	@Override
	public UserInfo getPersonalInfo(long uid, long authorId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", authorId);
		params.put("uid", uid);
		return sqlSessionTemplate.selectOne("userMapper.getPersonalInfo", params);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int updateUserState(Map params) {
		return sqlSessionTemplate.update("userMapper.updateUserState", params);
	}

	@Override
	public List<Map<String, Object>> selectMyConcrenedPersons(
		Map<String, Object> params) {
		return sqlSessionTemplate.selectList("userMapper.selectMyConcernedPersons", params);
	}

	@Override
	public int updateActivateAccount(UserInfo user) {

		return sqlSessionTemplate.update("userMapper.updateActivateAccount", user);
	}

	@Override
	public List<Map<String, Object>> getSystemLabel(long uid, int searchType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("searchType", searchType);
		params.put("uid", uid);
		return sqlSessionTemplate.selectList("userMapper.getSystemLabel", params);
	}

	@Override
	public void saveSystemLabel(Map<String, Object> params) {

		sqlSessionTemplate.insert("userMapper.saveSystemLabel", params);
	}

	@Override
	public void updateUserIdentityType(long uid, int identityType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("identityType", identityType);
		params.put("uid", uid);
		sqlSessionTemplate.update("userMapper.updateUserIdentityType", params);

	}

	@Override
	public void deleteSystemLabel(Map<String, Object> params) {
		sqlSessionTemplate.delete("userMapper.deleteSystemLabel", params);

	}

	@Override
	public Map<String, Object> getUserHeadImage(long uid) {
		return sqlSessionTemplate.selectOne("userMapper.getUserHeadImage", uid);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getUidByOpenId(String openId, int clientType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("openId", openId);
		params.put("clientType", clientType);
		return sqlSessionTemplate.selectOne("userMapper.getUidByOpenId", params);
	}

	@Override
	public int updateUserCard(long uid, long cardId, int authStatus, String companyAbbr, String email, String position, String username) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("cardId", cardId);
		params.put("authStatus", authStatus);
		params.put("companyAbbr", companyAbbr);
		params.put("email", email);
		params.put("position", position);
		params.put("username", username);
		return sqlSessionTemplate.update("userMapper.updateUserCard", params);
	}

	@Override
	public void saveComment(Comment comment) {
		sqlSessionTemplate.insert("userMapper.saveComment", comment);
	}

	/**
	 * 查询评价列表
	 *
	 * @param type
	 * @return
	 */
	public List<CommentVo> getCommentList(long id, int type, int currentPage, int showNum, Set<Long> ids, long companyCode, String condition) {
		Map<String, Object> params = new HashMap<>();
		params.put("type", type);
		params.put("currentPage", currentPage);
		params.put("showNum", showNum);
		params.put("id", id);
		params.put("ids", ids);
		params.put("companyCode", companyCode);
		params.put("condition", condition);

		List<CommentVo> list = sqlSessionTemplate.selectList("userMapper.getCommentList", params);
		return list;
	}

	@Override
	public Comment getCommentById(@Param(value = "id") long dataId) {

		return sqlSessionTemplate.selectOne("userMapper.getCommentById", dataId);
	}

	@Override
	public void updateCommentPraise(@Param(value = "id") long dataId, int num) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", dataId);
		params.put("num", num);
		sqlSessionTemplate.update("userMapper.updateCommentPraise", params);
	}

	@Override
	public List<UserIdVo> getMyFocusedPersons(long uid, int showNum, int currentPage) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("showNum", showNum);
		params.put("currentPage", currentPage);
		return sqlSessionTemplate.selectList("userMapper.getMyFocusedPersons", params);
	}

	@Override
	public List<UserIdVo> getHotFocusedPersons(long uid, int showNum, int currentPage) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("showNum", showNum);
		params.put("currentPage", currentPage);
		return sqlSessionTemplate.selectList("userMapper.getHotFocusedPersons", params);
	}

	@Override
	public List<UserIdVo> getFocusedPersonsByMyFocusedPersons(long uid, int showNum, int currentPage) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("showNum", showNum);
		params.put("currentPage", currentPage);
		return sqlSessionTemplate.selectList("userMapper.getFocusedPersonsByMyFocusedPersons", params);
	}

	@Override
	public List<UserIdVo> getPersonsFocusMe(long uid, int showNum, int currentPage) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("showNum", showNum);
		params.put("currentPage", currentPage);
		return sqlSessionTemplate.selectList("userMapper.getPersonsFocusMe", params);
	}

	@Override
	public List<UserIdVo> getMyFocusedPersonsFocusMe(long uid, int showNum, int currentPage) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("showNum", showNum);
		params.put("currentPage", currentPage);
		return sqlSessionTemplate.selectList("userMapper.getMyFocusedPersonsFocusMe", params);
	}

	@Override
	public int saveCommentAuthorId(long id, List<Long> authorIds) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("commentId", id);
		params.put("list", authorIds);
		return sqlSessionTemplate.insert("userMapper.saveCommentAuthorId", params);

	}

	@Override
	public int updateUserStatus(long uid, int status) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("status", status);
		return sqlSessionTemplate.update("userMapper.updateUserStatus", params);
	}

	@Override
	public int saveCommentRelation(long uid, long dataId, int dataType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("dataId", dataId);
		params.put("dataType", dataType);

		return sqlSessionTemplate.insert("userMapper.saveCommentRelation", params);
	}

	@Override
	public List<Long> getUIDList(long startId, int currentPage, int showNum) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("startId", startId);
		params.put("currentPage", currentPage);
		params.put("showNum", showNum);

		return sqlSessionTemplate.selectList("userMapper.getUIDList", params);
	}

	@Override
	public List<UserRecordStateVo> getUserRecordList(String startId, int currentPage, int showNum) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("currentPage", currentPage);
		params.put("showNum", showNum);
		params.put("startId", startId);

		return sqlSessionTemplate.selectList("userMapper.getUserRecordList", params);
	}

	/*
	 * 新增用户点击记录
	 * @see com.meix.dao.IUserDao#saveLastUserRecord(com.meix.model.user.UserRecordStateVo)
	 */
	@Override
	public void saveLastUserRecord(UserRecordStateVo vo) {
		sqlSessionTemplate.insert("userMapper.saveLastUserRecord", vo);
	}

	@Override
	public boolean isFocusedPerson(long uid, long authorCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("authorCode", authorCode);

		int count = sqlSessionTemplate.selectOne("userMapper.isFocusedPerson", params);

		return count > 0;
	}

	@Override
	public List<Long> getFocusedPersons(long uid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);

		return sqlSessionTemplate.selectList("userMapper.getFocusedPersons", params);
	}

	@Override
	public long getFocusedPersonCount(long uid) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		return sqlSessionTemplate.selectOne("userMapper.getFocusedPersonCount", params);
	}

	@Override
	public String getOpenIdByUid(long uid, String appId, int clientType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("appId", appId);
		params.put("clientType", clientType);
		return sqlSessionTemplate.selectOne("userMapper.getOpenIdByUid", params);
	}

	@Override
	public void unbindweixin(String mobile, int clientType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobile", mobile);
		params.put("clientType", clientType);
		sqlSessionTemplate.delete("userMapper.unbindweixin", params);
	}

	@Override
	public int updateUserRecordState(UserRecordStateVo vo) {
		int returnValue = sqlSessionTemplate.update("userMapper.updateUserRecordState", vo);
		return returnValue;
	}

	@Override
	public int saveUserRecordState(UserRecordStateVo vo) {
		int returnval = sqlSessionTemplate.insert("userMapper.insertUserRecordState", vo);
		return returnval;
	}

	@Override
	public int deleteUserRecordState(UserRecordStateVo vo) {
		int returnVal = 0;
		if (vo.getUid() > 0) {
			returnVal = sqlSessionTemplate.delete("userMapper.deleteUserRecordState", vo);
		}
		return returnVal;
	}

	@Override
	public int saveUserRecordStateLog(UserRecordStateVo vo) {
		return sqlSessionTemplate.insert("userMapper.insertUserRecordStateLog", vo);
	}

	@Override
	public int batchInsertUserRecordStateLog(List<UserRecordStateVo> list) {
		return sqlSessionTemplate.insert("userMapper.batchInsertUserRecordStateLog", list);
	}

	@Override
	public UserRecordStateVo getUserRecordState(int dataType, long dataId, long uid, int dataState) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataType", dataType);
		params.put("dataId", dataId);
		params.put("uid", uid);
		params.put("dataState", dataState);
		return sqlSessionTemplate.selectOne("userMapper.getUserRecordState", params);
	}

	@Override
	public void updateUserCompanyCode(long uid, long companyCode, String userName) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("companyCode", companyCode);
		params.put("userName", userName);
		sqlSessionTemplate.insert("userMapper.updateUserCompanyCode", params);
	}

	@Override
	public Map<String, Object> selectCardByUid(long uid, String mobile, int type) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		if (uid == 0 && !StringUtils.isEmpty(mobile)) {
			params.put("mobile", mobile);
		}
		params.put("type", type);
		return sqlSessionTemplate.selectOne("userMapper.selectCardByUid", params);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int addCustomerCard(Map params) {
		return sqlSessionTemplate.insert("userMapper.addCustomerCard", params);
	}

	@Override
	public int updateCustomerCardStatus(long cardId, int isUsed) {
		Map<String, Object> params = new HashMap<>();
		params.put("cardId", cardId);
		params.put("isUsed", isUsed);
		return sqlSessionTemplate.update("userMapper.updateCustomerCardStatus", params);
	}

	public int getUserFollowNum(Long uid) {
		return sqlSessionTemplate.selectOne("userMapper.getUesrFollowNum", uid);
	}

	public int getReportReadNum(Long researchId) {
		return sqlSessionTemplate.selectOne("userMapper.getReportReadNum", researchId);
	}

	@Override
	public List<UserRecommendationInfo> getRelatedRecommendations(long uid, long dataId, int dataType,
	                                                              int currentPage, int showNum,
	                                                              int clientType, int accountType) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		params.put("clientType", clientType);
		params.put("accountType", accountType);
		return sqlSessionTemplate.selectList("userMapper.getRelatedRecommendations", params);
	}

	@Override
	public List<UserInfo> getActivityAnalystInfo(long activityId) {
		return sqlSessionTemplate.selectList("userMapper.getActivityAnalystInfo", activityId);
	}

	@Override
	public List<UserInfo> getReportAuthorInfo(long researchId) {
		return sqlSessionTemplate.selectList("userMapper.getReportAuthorInfo", researchId);
	}

	@Override
	public int getUesrFollowNum(long uid) {
		return sqlSessionTemplate.selectOne("userMapper.getUesrFollowNum", uid);
	}

	@Override
	public int getReadNum(long uid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		Object num = sqlSessionTemplate.selectOne("userMapper.getReadNum", params);
		return num == null ? 0 : Integer.valueOf(num.toString());
	}

	@Override
	public List<UserRecommendationInfo> getRelatedRecommendationsByAuthor(long uid, long authorId, long dataId, int dataType,
	                                                                      int currentPage, int showNum, int clientType, int accountType) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("authorId", authorId);
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		params.put("clientType", clientType);
		params.put("accountType", accountType);
		return sqlSessionTemplate.selectList("userMapper.getRelatedRecommendationsByAuthor", params);
	}

	@Override
	public List<UserRecommendationInfo> getRelatedRecommendationsByTime(long uid, long dataId, int dataType,
	                                                                    int currentPage, int showNum, int clientType, int accountType) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		params.put("clientType", clientType);
		params.put("accountType", accountType);
		return sqlSessionTemplate.selectList("userMapper.getRelatedRecommendationsByTime", params);
	}

	@Override
	public List<String> getRelatedRecommendationsSecuInfo(long dataId, int dataType) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		return sqlSessionTemplate.selectList("userMapper.getRelatedRecommendationsSecuInfo", params);
	}

	@Override
	public Long getUidByThirdId(String thirdId) {
		return sqlSessionTemplate.selectOne("userMapper.getUidByThirdId", thirdId);
	}

	@Override
	public String getMobileById(long uid) {
		return sqlSessionTemplate.selectOne("userMapper.getMobileById", uid);
	}

	/**
	 * @Title: 获取用户画像得分、
	 * @Description:获取用户画像打分
	 * @return: void
	 */
	@Override
	public List<UserLabelScoreVo> getUserLabelScoreList() {
		return sqlSessionTemplate.selectList("userMapper.getUserLabelScoreList");
	}

	public void deleteUserPersonas(List<Long> uidList) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", uidList);
		sqlSessionTemplate.update("userMapper.deleteUserLabel", params);
	}

	public String getUserPersonas(String labelId) {
		return sqlSessionTemplate.selectOne("userMapper.getUserLabel", labelId);
	}

	public void updateUserPersonas(UserPersonasVo label) {
		sqlSessionTemplate.update("userMapper.updateUserLabel", label);
	}

	public void addUserPersonas(UserPersonasVo label) {
		sqlSessionTemplate.update("userMapper.addUserLabel", label);
	}

	@Override
	public int updateLoginInfo(long uid) {
		Map<String, Object> map = sqlSessionTemplate.selectOne("userMapper.getLastLoginDats", uid);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		if (map != null) {
			int days = MapUtils.getIntValue(map, "loginDays", 1);
			if (!df.format(new Date()).equals(MapUtils.getString(map, "loginDate"))) {
				days = days + 1;
			}
			Map<String, Object> params = new HashMap<>();
			params.put("uid", uid);
			params.put("loginDays", days);
			return sqlSessionTemplate.update("userMapper.updateLoginInfo", params);
		}
		return 0;
	}

	@Override
	public void insertLoginInfo(long uid) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		sqlSessionTemplate.insert("userMapper.insertLoginInfo", params);
	}

	@Override
	public List<Map<String, Object>> getCustomerList(int accountType, int authStatus, String condition, int currentPage, int showNum, 
			String updatedAt, String sortField, int sortRule, long groupId, int isExcept) {
		Map<String, Object> params = new HashMap<>();
		params.put("accountType", accountType);
		params.put("authStatus", authStatus);
		params.put("condition", condition);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		params.put("updatedAt", updatedAt);
		params.put("sortField", sortField);
		params.put("sortRule", sortRule);
		params.put("groupId", groupId);
		params.put("isExcept", isExcept);
		return sqlSessionTemplate.selectList("userMapper.getCustomerList", params);
	}

	@Override
	public int getCustomerCount(int accountType, int authStatus, String condition, long groupId, int isExcept) {
		Map<String, Object> params = new HashMap<>();
		params.put("accountType", accountType);
		params.put("authStatus", authStatus);
		params.put("condition", condition);
		params.put("groupId", groupId);
		params.put("isExcept", isExcept);
		Map<String, Object> countMap = sqlSessionTemplate.selectOne("userMapper.getCustomerCount", params);
		if (countMap != null) {
			return MapUtils.getIntValue(countMap, "count");
		}
		return 0;
	}

	@Override
	public List<UserClueStat> getAuthedUserClueList(ClueSearchVo searchVo) {
		return sqlSessionTemplate.selectList("userMapper.getAuthedUserClueList", searchVo);
	}

	@Override
	public int getAuthedUserClueCount(ClueSearchVo searchVo) {
		Integer count = sqlSessionTemplate.selectOne("userMapper.getAuthedUserClueCount", searchVo);
		return count == null ? 0 : count.intValue();
	}

	@Override
	public List<CommentVo> getCommentList(ComplianceCommentSearchVo searchVo) {
		return sqlSessionTemplate.selectList("userMapper.getCommentMngList", searchVo);
	}

	@Override
	public int getCommentCount(ComplianceCommentSearchVo searchVo) {
		Integer count = sqlSessionTemplate.selectOne("userMapper.getCommentMngCount", searchVo);
		return count == null ? 0 : count.intValue();
	}

	@Override
	public void examineComment(long commentId, int optType, String uuid) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", commentId);
		params.put("optType", optType);
		params.put("uuid", uuid);
		sqlSessionTemplate.update("userMapper.updateComment", params);
	}

	@Override
	public void updateWechatInfo(UserInfo userInfo) {
		sqlSessionTemplate.update("userMapper.updateWechatInfo", userInfo);
	}

	@Override
	public List<UserInfo> getPushWechatMsgUserList(long companyCode) {
		return sqlSessionTemplate.selectList("userMapper.getPushWechatMsgUserList", companyCode);
	}

	@Override
	public List<Map<String, Object>> getUserLabelPushInfo() {
		return sqlSessionTemplate.selectList("userMapper.getUserLabelPushInfo");
	}

	@Override
	public void saveUserLabelPushRecord(long uid, long dataId, int type) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("dataId", dataId);
		params.put("dataType", type);
		sqlSessionTemplate.insert("userMapper.saveUserLabelPushRecord", params);
	}

	@Override
	public boolean isFromMyFocusedPerson(long uid, int dataType, long dataId) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("dataType", dataType);
		params.put("dataId", dataId);
		int count = sqlSessionTemplate.selectOne("userMapper.isFromMyFocusedPerson", params);
		return count > 0;
	}

	@Override
	public boolean isFromMyFocusedStock(long uid, int dataType, long dataId) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("dataType", dataType);
		params.put("dataId", dataId);
		int count = sqlSessionTemplate.selectOne("userMapper.isFromMyFocusedStock", params);
		return count > 0;
	}

	@Override
	public void updateThirdIdByMobile(Map<String, Object> params) {
		sqlSessionTemplate.update("userMapper.updateThirdIdByMobile", params);
	}

	@Override
	public void updateUserPrivacyPolicyStatus(Map<String, Object> params) {
		sqlSessionTemplate.update("userMapper.updateUserPrivacyPolicyStatus", params);
	}

	@Override
	public long getLoginRecordCount(long companyCode, String startTime, String endTime) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		Object count = sqlSessionTemplate.selectOne("userMapper.getLoginRecordCount", params);
		if (count == null) {
			return 0;
		}
		return (long) count;
	}

	@Override
	public List<UserRecordStateResp> getUserRecordStateList(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("userMapper.getUserRecordStateList", params);
	}

	@Override
	public List<UserContentReadStat> getUserContentReadStat(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("userMapper.getUserContentReadStat", params);
	}

	@Override
	public long getUserContentCount(Map<String, Object> params) {
		Object result = sqlSessionTemplate.selectOne("userMapper.getUserContentCount", params);
		if (result == null) {
			return 0;
		}
		return (long) result;
	}

	@Override
	public long getDataReaderNum(int dataType, long dataId, String startTime, String endTime) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataType", dataType);
		params.put("dataId", dataId);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		Object result = sqlSessionTemplate.selectOne("userMapper.getDataReaderNum", params);
		if (result == null) {
			return 0;
		}
		return (long) result;
	}

	@Override
	public long getDataReadNum(long uid, String startTime, String endTime) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		Object result = sqlSessionTemplate.selectOne("userMapper.getDataReadNum", params);
		if (result == null) {
			return 0;
		}
		return (long) result;
	}

	@Override
	public UserInfo getUserInfoByUserName(String salerName) {
		Map<String, Object> params = new HashMap<>();
		params.put("userName", salerName);
		return sqlSessionTemplate.selectOne("userMapper.getUserInfoByUserName", params);
	}

	@Override
	public void deleteWhiteUser(String uuid, long uid, int accountType) {
		Map<String, Object> params = new HashMap<>();
		params.put("operatorID", uuid);
		params.put("accountType", accountType);
		params.put("uid", uid);
		sqlSessionTemplate.update("userMapper.deleteWhiteUser", params);
	}

	@Override
	public void updateUserIdentify(Map<String, Object> params) {
		sqlSessionTemplate.update("userMapper.updateUserIdentify", params);
	}
	
	@Override
	public UserLabelScoreVo getUserLabelScore(UserLabelScoreVo vo) {
		return sqlSessionTemplate.selectOne("userMapper.getUserLabelScore", vo);
	}

	@Override
	public void updateUserLabelScore(UserLabelScoreVo vo) {
		sqlSessionTemplate.update("userMapper.updateUserLabelScore", vo);
	}

	@Override
	public void addUserLabelScore(UserLabelScoreVo vo) {
		sqlSessionTemplate.insert("userMapper.addUserLabelScore", vo);
	}

	@Override
	public void deleteUserLabelScore(List<Long> existId) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", existId);
		sqlSessionTemplate.delete("userMapper.deleteUserLabelScore", params);
	}

	@Override
	public void saveUserOperatorLog(Map<String, Object> params) {
		sqlSessionTemplate.insert("userMapper.saveUserOperatorLog", params);
	}

}
