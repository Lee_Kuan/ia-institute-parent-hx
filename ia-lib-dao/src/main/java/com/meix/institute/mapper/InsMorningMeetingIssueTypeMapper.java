package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsMorningMeetingIssueType;

public interface InsMorningMeetingIssueTypeMapper extends MeixBaseMapper<InsMorningMeetingIssueType> {
}