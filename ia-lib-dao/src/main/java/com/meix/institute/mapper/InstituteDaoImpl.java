package com.meix.institute.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.meix.institute.api.IInstituteDao;
import com.meix.institute.vo.company.CompanyWechatConfig;
import com.meix.institute.vo.company.CompanyWechatTemplete;
import com.meix.institute.vo.user.InsCustomer;
import com.meix.institute.vo.user.InsMeixUser;

/**
 * Created by zenghao on 2019/9/4.
 */
@Repository
public class InstituteDaoImpl implements IInstituteDao {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	@Override
	public int saveInsMeixUser(InsMeixUser insMeixUser) {
		return sqlSessionTemplate.insert("instituteMapper.saveInsMeixUser", insMeixUser);
	}

	@Override
	public int updateInsMeixUser(InsMeixUser insMeixUser) {
		return sqlSessionTemplate.update("instituteMapper.updateInsMeixUser", insMeixUser);
	}

	@Override
	public InsMeixUser getExistInsMeixUser(long companyCode, long customerCompanyCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("customerCompanyCode", customerCompanyCode);
		return sqlSessionTemplate.selectOne("instituteMapper.getExistInsMeixUser", params);
	}

	/**
	 * 获取微信公众号配置信息
	 *
	 * @return
	 */
	@Override
	public List<CompanyWechatConfig> getWechatConfig() {
		return sqlSessionTemplate.selectList("instituteMapper.getWechatConfig");
	}

	@Override
	public CompanyWechatConfig getWechatConfigByAppType(String appType) {
		Map<String, Object> params = new HashMap<>();
		params.put("appType", appType);
		return sqlSessionTemplate.selectOne("instituteMapper.getWechatConfigByAppType", params);
	}

	@Override
	public CompanyWechatConfig getWechatConfigByAppId(String appId) {
		Map<String, Object> params = new HashMap<>();
		params.put("appId", appId);
		return sqlSessionTemplate.selectOne("instituteMapper.getWechatConfigByAppId", params);
	}

	@Override
	public CompanyWechatConfig getWechatConfigByCompanyCode(long companyCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		return sqlSessionTemplate.selectOne("instituteMapper.getWechatConfigByCompanyCode", params);
	}

	@Override
	public CompanyWechatConfig getSPConfigByCompanyCode(long companyCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		return sqlSessionTemplate.selectOne("instituteMapper.getSPConfigByCompanyCode", params);
	}

	@Override
	public CompanyWechatTemplete getCompanyWechatTemplete(long companyCode, int msgType) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("msgType", msgType);
		return sqlSessionTemplate.selectOne("instituteMapper.getCompanyWechatTemplete", params);
	}

	@Override
	public List<InsCustomer> getCustomersBySaler(long salerUid, int currentPage, int showNum) {
		Map<String, Object> params = new HashMap<>();
		params.put("salerUid", salerUid);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		return sqlSessionTemplate.selectList("instituteMapper.getCustomersBySaler", params);
	}

	@Override
	public int getCustomerCountBySaler(long salerUid) {
		return sqlSessionTemplate.selectOne("instituteMapper.getCustomerCountBySaler", salerUid);
	}

    @Override
    public CompanyWechatConfig getWechatConfigByAppTypeAndClientType(String appType, Integer clientType) {
        Map<String, Object> params = new HashMap<>();
        params.put("appType", appType);
        params.put("clientType", clientType);
        return sqlSessionTemplate.selectOne("instituteMapper.getWechatConfigByAppTypeAndClientType", params);
    }

}
