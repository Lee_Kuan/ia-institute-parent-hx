package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.CustomerRelation;

public interface CustomerRelationMapper extends MeixBaseMapper<CustomerRelation> {
}