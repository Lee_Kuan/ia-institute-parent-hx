package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsSyncTableJsid;

public interface InsSyncTableJsidMapper extends MeixBaseMapper<InsSyncTableJsid> {

}