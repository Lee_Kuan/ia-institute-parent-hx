package com.meix.institute.mapper;

import com.meix.institute.vo.company.CompanyInfoVo;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:公司设置数据层
 */
@Repository
public class CompanyDaoImpl {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public CompanyInfoVo getCompanyInfoById(long id) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		return sqlSessionTemplate.selectOne("companyMapper.getCompanyInfoById", params);
	}

	public CompanyInfoVo getCompanyInfoByName(String companyName) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyName", companyName);
		return sqlSessionTemplate.selectOne("companyMapper.getCompanyInfoByName", params);
	}

	/**
	 * @param vo
	 * @return
	 * @Title: addCompanyInfo、
	 * @Description:保存公司信息
	 * @return: int
	 */
	public int addCompanyInfo(CompanyInfoVo vo) {
		return sqlSessionTemplate.insert("companyMapper.addCompanyInfo", vo);
	}

	public List<CompanyInfoVo> getCompanyList(String condition, int currentPage, int showNum) {
		Map<String, Object> params = new HashMap<>();
		params.put("condition", condition);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		return sqlSessionTemplate.selectList("companyMapper.getCompanyList", params);
	}

}
