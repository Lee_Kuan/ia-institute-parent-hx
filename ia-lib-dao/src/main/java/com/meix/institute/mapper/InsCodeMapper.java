package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsCode;

public interface InsCodeMapper extends MeixBaseMapper<InsCode> {
}