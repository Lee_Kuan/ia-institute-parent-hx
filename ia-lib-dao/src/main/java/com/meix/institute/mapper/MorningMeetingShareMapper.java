package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.MorningMeetingShare;

public interface MorningMeetingShareMapper extends MeixBaseMapper<MorningMeetingShare> {
}