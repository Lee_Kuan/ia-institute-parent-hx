package com.meix.institute.mapper;

import com.meix.institute.vo.third.ThirdAccountVo;
import com.meix.institute.vo.third.ThirdLoginVo;

import net.sf.json.JSONArray;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhangzhen(J)
 * @date 2016年1月4日下午2:13:30
 * @describe 活动
 */
@Repository
public class ThridPartyDaoImpl {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	/**
	 * 获取预约信息
	 *
	 * @param uid
	 * @param mobile
	 * @return
	 */
	public List<Map<String, Object>> getThinkerAppointment(long uid, String mobile) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("mobile", mobile);
		return sqlSessionTemplate.selectList("thridPartyMapper.getThinkerAppointment", params);
	}

	/**
	 * 保存预约
	 *
	 * @param uid
	 * @param mobile
	 * @return
	 */
	public int saveThinkerAppointment(long uid, String mobile) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("mobile", mobile);
		return sqlSessionTemplate.insert("thridPartyMapper.saveThinkerAppointment", params);
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年6月1日下午1:58:56
	 * @Description: 保存微信流水记录
	 */
	public int saveWeiXinShare(String openId, String uri, String browserInfo,
	                           String host) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("openId", openId);
		params.put("uri", uri);
		params.put("browserInfo", browserInfo);
		params.put("host", host);
		return sqlSessionTemplate.insert("thridPartyMapper.saveWeiXinShare", params);
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年6月14日下午1:30:21
	 * @Description: 更新分享链接的阅读数
	 */
	public int updateShareViewReadNum(String uri) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uri", uri);

		return sqlSessionTemplate.update("thridPartyMapper.updateShareViewReadNum", params);
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年8月31日下午4:37:21
	 * @Description: 新增朋友圈信息
	 */
	public void insertPYQ(JSONArray add) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("list", add);
		sqlSessionTemplate.insert("thridPartyMapper.insertPYQ", params);
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年8月31日下午4:40:21
	 * @Description: 更新朋友圈信息
	 */
	@Deprecated
	public void updatePYQ(JSONArray update) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("list", update);
		sqlSessionTemplate.update("thridPartyMapper.updatePYQ", params);
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年8月31日下午5:17:14
	 * @Description: 查询微信朋友圈列表
	 */
	public List<Map<String, Object>> getPYQList(int showNum, int currentPage) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("showNum", showNum);
		params.put("currentPage", currentPage * showNum);
		return sqlSessionTemplate.selectList("thridPartyMapper.getPYQList", params);
	}

	/**
	 * @param vo
	 * @return
	 * @Title: saveThirdAccount、
	 * @Description:保存第三方账户信息
	 * @return: int
	 */
	public int saveThirdAccount(ThirdAccountVo vo) {
		return sqlSessionTemplate.insert("thridPartyMapper.saveThirdAccount", vo);
	}

	/**
	 * @param openId
	 * @param type
	 * @return
	 * @Title: getThirdAccount、
	 * @Description:获取第三方账户信息
	 * @return: ThirdAccountVo
	 */
	public ThirdAccountVo getThirdAccount(String openId, int type) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("openId", openId);
		params.put("type", type);
		return sqlSessionTemplate.selectOne("thridPartyMapper.getThirdAccount", params);
	}

	/**
	 * @param vo
	 * @return
	 * @Title: saveThirdLogin、
	 * @Description:保存第三方登录信息
	 * @return: int
	 */
	public int saveThirdLogin(ThirdLoginVo vo) {
		return sqlSessionTemplate.insert("thridPartyMapper.saveThirdLogin", vo);
	}

	public int updateThirdLogin(String appVersion, String osVersion) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("appVersion", appVersion);
		params.put("osVersion", osVersion);
		return sqlSessionTemplate.update("thridPartyMapper.updateThirdLogin", params);
	}

	/**
	 * @param openId
	 * @param clientType
	 * @return
	 * @Title: updateThirdLoginState、
	 * @Description:注销账户
	 * @return: int
	 */
	public int updateThirdLoginState(String openId, int clientType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("openId", openId);
		params.put("clientType", clientType);
		return sqlSessionTemplate.update("thridPartyMapper.updateThirdLoginState", params);
	}

	/**
	 * @author zhangzhen(J)
	 * @param clientType 
	 * @date 2016年3月27日下午8:42:04
	 * @Description: 查询用户的openId
	 */
	public Map<String, Object> getOpenIdByUidAndOpenId(long uid, String openId, int clientType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("openId", openId);
		params.put("clientType", clientType);
		return sqlSessionTemplate.selectOne("thridPartyMapper.getOpenIdByUidAndOpenId", params);
	}

	/**
	 * @author likuan
	 * @date 2019年7月17日19:33:34
	 * @Description: 查询用户的openId
	 */
	public List<Map<String, Object>> getOpenIdByUidAndAppId(long uid, String appId, int clientType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("appId", appId);
		params.put("clientType", clientType);
		return sqlSessionTemplate.selectList("thridPartyMapper.getOpenIdByUidAndAppId", params);
	}

	/**
	 * @author likuan
	 * @date 2019年7月17日19:37:00
	 * @Description: 增加用户的openId
	 */
	public int updateUserOpenId(long uid, String openId, String appId, int clientType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("openId", openId);
		params.put("appId", appId);
		params.put("clientType", clientType);
		return sqlSessionTemplate.update("thridPartyMapper.updateUserOpenId", params);
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年3月28日上午9:31:23
	 * @Description: 增加用户的openId
	 */
	public int insertUserOpenId(long uid, String openId, String appId, int clientType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("openId", openId);
		params.put("appId", appId);
		params.put("clientType", clientType);
		return sqlSessionTemplate.insert("thridPartyMapper.insertUserOpenId", params);
	}

	/**
	 * @param uid
	 * @param openId
	 * @return
	 * @Title: deleteUserOpenId、
	 * @Description:删除用户微信关系
	 * @return: int
	 */
	public int deleteUserOpenId(long uid, String openId, int clientType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("openId", openId);
		params.put("clientType", clientType);
		return sqlSessionTemplate.delete("thridPartyMapper.deleteUserOpenId", params);
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年9月27日上午11:17:28
	 * @Description: 删除
	 */
	public void deletePYQ(JSONArray delete) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("list", delete);
		sqlSessionTemplate.delete("thridPartyMapper.deletePYQ", params);
	}
	
	/**
	 * 删除appId 和 openId 下所有绑定用户
	 * @param openId 
	 * @param appId 
	 * @param clientType 
	 */
    public void deleteUserByOpenId(String openId, String appId, int clientType) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("openId", openId);
        params.put("appId", appId);
        params.put("clientType", clientType);
        sqlSessionTemplate.delete("thridPartyMapper.deleteUserByOpenId", params);
    }
}
