package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsReportIssueType;

public interface InsReportIssueTypeMapper extends MeixBaseMapper<InsReportIssueType> {
}