package com.meix.institute.mapper;

import com.meix.institute.api.IConstDao;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.vo.IaConstantVo;
import com.meix.institute.vo.cache.ConstantVo;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public class ConstDaoImpl implements IConstDao {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	@Override
	public int insertConstantList(List<IaConstantVo> list) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", list);
		return sqlSessionTemplate.insert("constMapper.insertConstantList", params);
	}

	@Override
	public int removeConstantRange(long startId, long endId) {
		Map<String, Object> params = new HashMap<>();
		params.put("startId", startId);
		params.put("endId", endId);
		return sqlSessionTemplate.delete("constMapper.removeConstantRange", params);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<SecuMain> getStockMap(Map params) {

		return sqlSessionTemplate.selectList("constMapper.getStockMap", params);
	}

	@Override
	public Long defaultInnerCode() {
		return sqlSessionTemplate.selectOne("constMapper.selectDefaultInnerCode");
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Map> getCompanyTypeList() {

		return sqlSessionTemplate.selectList("constMapper.getCompanyTypeList");
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getIndustryLabel(@Param(value = "innerCode") int innerCode) {
		return sqlSessionTemplate.selectOne("constMapper.getIndustryLabel", innerCode);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Map> getIndustryList() {
		return sqlSessionTemplate.selectList("constMapper.getIndustryList");
	}

	@Override
	public Map<String,Object> getIndustryInfo(String industryName) {
		Map<String, Object> params = new HashMap<>();
		params.put("industryName", industryName);

		return sqlSessionTemplate.selectOne("constMapper.getIndustryInfo", params);
	}

	@Override
	public Map<String, Object> getIndustryInfoByCode(String industryCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("industryCode", industryCode);

		return sqlSessionTemplate.selectOne("constMapper.getIndustryInfoByCode", params);
	}
	@Override
	public List<ConstantVo> getIndustryLabelList() {
		return sqlSessionTemplate.selectList("constMapper.getIndustryLabelList");
	}

	@Override
	public String getCustomQueryCondition(String key) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("key", key);

		return sqlSessionTemplate.selectOne("constMapper.getCustomQueryCondition", params);
	}

	@Override
	public float getExchange(String currencyType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("currencyType", currencyType);

		return sqlSessionTemplate.selectOne("constMapper.getExchange", params);
	}

	@Override
	public List<Map<String, Object>> selectSecuByName(Set<String> keySet) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("list", keySet);

		return sqlSessionTemplate.selectList("constMapper.selectSecuByName", params);
	}

	@Override
	public List<Map<String, Object>> find(Map<String, Object> map) {
		return sqlSessionTemplate.selectList("constMapper.find", map);
	}

}
