package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsActivity;

public interface InsActivityMapper extends MeixBaseMapper<InsActivity> {
    
}