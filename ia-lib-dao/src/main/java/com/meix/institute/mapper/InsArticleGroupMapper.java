package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsArticleGroup;

public interface InsArticleGroupMapper extends MeixBaseMapper<InsArticleGroup> {
}