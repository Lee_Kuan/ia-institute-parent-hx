package com.meix.institute.mapper;

import com.meix.institute.api.IMessageDao;
import com.meix.institute.vo.message.MeetingMessageVo;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/9/26.
 */
@Repository
public class MessageDaoImpl implements IMessageDao {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	@Override
	public List<MeetingMessageVo> getMeetingMessages(long uid, long activityId,
	                                                 long speakerId, long messageId, int messageFlag, int showNum) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("activityId", activityId);
		params.put("speakerId", speakerId);
		params.put("messageId", messageId);
		params.put("messageFlag", messageFlag);
		if (showNum != -1) {
			params.put("showNum", showNum);
		}
		return sqlSessionTemplate.selectList("messageMapper.getMeetingMessages", params);
	}

	@Override
	public int saveMeetingMessageDetail(MeetingMessageVo mmv) {
		return sqlSessionTemplate.insert("messageMapper.saveMeetingMessageDetail", mmv);
	}

	@Override
	public void saveMeetingMessage(MeetingMessageVo mmv) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("activityId", mmv.getActivityId());
		params.put("messageId", mmv.getId());

		sqlSessionTemplate.insert("messageMapper.saveMeetingMessage", params);
	}
}
