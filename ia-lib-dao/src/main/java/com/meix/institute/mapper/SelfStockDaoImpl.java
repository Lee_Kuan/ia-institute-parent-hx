package com.meix.institute.mapper;

import com.meix.institute.api.ISelfStockDao;
import com.meix.institute.vo.stock.*;
import org.apache.commons.collections.MapUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @版权信息: 上海睿涉取有限公司
 * @部门 :开发部
 * @工程名 : 投资终端
 * @作者 :zhouwei
 * @E-mail : zhouwei@51research.com
 * @创建日期: 2014-4-22下午1:45:31
 */
@SuppressWarnings("rawtypes")
@Repository
public class SelfStockDaoImpl implements ISelfStockDao {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	@Override
	public List<SelfStockCategoryVo> querySelfCategory(Map<String, Object> params) {
		List<SelfStockCategoryVo> list = null;
		list = sqlSessionTemplate.selectList("stockMapper.getStockCategory", params);
		return list;
	}

	@Override
	public List<SelfStockVo> querySelfStock(Map<String, Object> params) {
		List<SelfStockVo> list = null;
		list = sqlSessionTemplate.selectList("stockMapper.getStockList", params);
		return list;
	}

	@Override
	public int insertSelfCategory(SelfStockCategoryVo data) {
		return sqlSessionTemplate.insert("stockMapper.saveStockCategory", data);
	}

	@Override
	public int updateSelfCategory(SelfStockCategoryVo data) {
		int count = 0;
		count = sqlSessionTemplate.update("stockMapper.updateStockCategory", data);
		return count;
	}

	@Override
	public int deleteSelfCategory(SelfStockCategoryVo data) {
		int count = 0;
		count = sqlSessionTemplate.delete("stockMapper.deleteStockCategory", data);
		return count;
	}

	@Override
	public int insertSelfStock(List<SelfStockVo> list) {
		return sqlSessionTemplate.insert("stockMapper.saveStockInfo", list);
	}

	@Override
	public int updateSelfStock(SelfStockVo data) {
		return sqlSessionTemplate.update("stockMapper.updateStockInfo", data);
	}

	@Override
	public int deleteSelfStock(Map params) {
		int count = 0;
		count = sqlSessionTemplate.delete("stockMapper.deleteStockInfo", params);
		return count;
	}

	@Override
	public List<Map> getAllStock(Map<String, Object> params) {
		List<Map> list = null;
		list = sqlSessionTemplate.selectList("stockMapper.getAllStock", params);
		return list;
	}

	@Override
	public List<SelfStockVo> queryAllSelfStock(Map<String, Object> params) {
		List<SelfStockVo> list = null;
		list = sqlSessionTemplate.selectList("stockMapper.getAllSelfStock", params);
		return list;
	}

	@Override
	public List<StockVo> getUserSelfStockList(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("stockMapper.getUserSelfStockList", params);
	}

	@Override
	public SelfStockVo getSelfStock(long uid, int innerCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("innerCode", innerCode);
		return sqlSessionTemplate.selectOne("stockMapper.getSelfStock", params);
	}

	@Override
	public SelfStockVo getStockOfUser(Map<String, Object> params) {
		SelfStockVo vo = null;
		vo = sqlSessionTemplate.selectOne("stockMapper.getStockOfUser", params);
		return vo;
	}

	@Override
	public int getSortNoOfCategory(long categoryId, int flag) {
		int sortNo = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("categoryId", categoryId);
		params.put("flag", flag);
		sortNo = sqlSessionTemplate.selectOne("stockMapper.getSortNoOfCategory", params);
		return sortNo;
	}

	@Override
	public SelfStockVo getStockByInnerCode(Map<String, Object> params) {
		SelfStockVo vo = null;
		vo = sqlSessionTemplate.selectOne("stockMapper.getStockByInnerCode", params);
		return vo;
	}

	@Override
	public SelfStockVo getStockBySecuCode(Map<String, Object> params) {
		SelfStockVo vo = null;
		vo = sqlSessionTemplate.selectOne("stockMapper.getStockBySecuCode", params);
		return vo;
	}

	@Override
	public int checkStockIsExist(long categoryId, int innerCode) {
		int count = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("categoryId", categoryId);
		params.put("innerCode", innerCode);
		count = sqlSessionTemplate.selectOne("stockMapper.checkStockIsExist", params);
		return count;
	}

	@Override
	public List<SelfStockVo> getStockListByCategory(Map params) {

		return sqlSessionTemplate.selectList("stockMapper.getStockListByCategory", params);
	}

	@Override
	public int addSelfPoolShare(List<SelfStockCategoryShareVo> list) {

		return sqlSessionTemplate.insert("stockMapper.addSelfPoolShare", list);
	}

	@Override
	public int deleteSelfPoolShare(long groupId, List<SelfStockCategoryShareVo> list) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("groupId", groupId);
		params.put("list", list);
		return sqlSessionTemplate.delete("stockMapper.deleteSelfPoolShare", params);
	}

	@Override
	public List<SelfStockCategoryShareVo> getStockCategoryAuthorizedList(
		int showNum, int currentPage, long uid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("showNum", showNum);
		params.put("currentPage", currentPage);
		return sqlSessionTemplate.selectList("stockMapper.getStockCategoryAuthorizedList", params);
	}

	@Override
	public int deleteSelectedPerson(Map<String, Object> params) {
		return sqlSessionTemplate.delete("stockMapper.deleteSelectedPerson", params);
	}

	@Override
	public int insertSelectedPerson(Map<String, Object> params) {
		return sqlSessionTemplate.insert("stockMapper.insertSelectedPerson", params);
	}

	@Override
	public List<Map<String, Object>> selectPersonalSelected(
		Map<String, Object> params) {
		return sqlSessionTemplate.selectList("stockMapper.selectPersonalSelected", params);
	}

	@Override
	public List<Map<String, Object>> selectPersonalSelected2(
		Map<String, Object> params) {
		return sqlSessionTemplate.selectList("stockMapper.selectPersonalSelected2", params);
	}

	@Override
	public List<Map<String, Object>> selectSelectedPersonList(
		Map<String, Object> params) {
		int searchType = (int) params.get("selectedType");
		if (searchType == 4) {
			return sqlSessionTemplate.selectList("stockMapper.selectedPersonList2", params);
		}
		return sqlSessionTemplate.selectList("stockMapper.selectedPersonList1", params);
	}

	@Override
	public int updateSelectedPerson(Map params) {

		return sqlSessionTemplate.update("stockMapper.updateSelectedPerson", params);
	}

	@Override
	public int getStockNumOfCategory(Map params) {
		return sqlSessionTemplate.selectOne("stockMapper.getStockNumOfCategory", params);
	}

	@Override
	public List<Map> getCandle(Map params) {
		return sqlSessionTemplate.selectOne("stockMapper.getCandle", params);
	}

	@Override
	public List<Map<String, Object>> getPersonalPointView(
		Map<String, Object> params) {

		return sqlSessionTemplate.selectList("stockMapper.getPersonalPointView", params);
	}

	@Override
	public List<SelfStockVo> getAllSelfPoolSecu(long uid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		return sqlSessionTemplate.selectList("stockMapper.getAllSelfPoolSecu", params);
	}

	@Override
	public List<Map> getStockPoolChangedList(long uid, String maxTime) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("maxTime", maxTime);
		return sqlSessionTemplate.selectList("stockMapper.getStockPoolChangedList", params);
	}

	@Override
	public List<SelfStockCategoryShareVo> getSelfPoolShare(long groupId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("groupId", groupId);
		return sqlSessionTemplate.selectList("stockMapper.getSelfPoolShare", params);
	}

	@Override
	public int saveSelfPoolGroup(long uid, long categoryId, long groupId,
	                             List<Integer> list) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("categoryId", categoryId);
		params.put("groupId", groupId);
		params.put("list", list);
		return sqlSessionTemplate.insert("stockMapper.saveSelfPoolGroup", params);
	}

	@Override
	public int getSelfPoolType(long poolId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("poolId", poolId);
		return sqlSessionTemplate.selectOne("stockMapper.getSelfPoolType", params);
	}

	@Override
	public int deleteSelfPoolGroup(long groupId, long uid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("groupId", groupId);
		params.put("uid", uid);
		return sqlSessionTemplate.delete("stockMapper.deleteSelfPoolGroup", params);
	}

	@Override
	public List<SelfStockCategoryShareVo> getStockCategoryAuthorizedDetail(
		long groupId, long uid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("groupId", groupId);
		params.put("uid", uid);
		return sqlSessionTemplate.selectList("stockMapper.getStockCategoryAuthorizedDetail", params);
	}

	@Override
	public List<Map<String, Object>> getSharedStockCategory(long uid,
	                                                        int showNum, int currentPage, int stockExpire, int type) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("showNum", showNum);
		params.put("currentPage", currentPage * showNum);
		params.put("stockExpire", stockExpire);
		params.put("type", type);

		return sqlSessionTemplate.selectList("stockMapper.getSharedStockCategory", params);
	}

	@Override
	public List<SharedStockListVo> getSharedStockByCategoryId(long uid,
	                                                          long categoryId, int showNum, int currentPage, String sort,
	                                                          int sortRule, int stockExpire) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("showNum", showNum);
		params.put("currentPage", currentPage * showNum);
		params.put("stockExpire", stockExpire);
		params.put("categoryId", categoryId);
		params.put("sort", sort);
		params.put("sortRule", sortRule > 0 ? "ASC" : "DESC");

		return sqlSessionTemplate.selectList("stockMapper.getSharedStockByCategoryId", params);
	}

	@Override
	public long getOldCategoryId(long groupId) {
		Map result = sqlSessionTemplate.selectOne("stockMapper.getOldCategoryId", groupId);

		return MapUtils.getLong(result, "PoolID", -1l);
	}

	@Override
	public List<Map> getStockOfCustomer(long uid, int currentPage, int showNum, int innerCode, String sort,
	                                    int sortRule) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		params.put("sort", sort);
		params.put("sortRule", sortRule > 0 ? "ASC" : "DESC");
		if (innerCode != -1) {
			params.put("innerCode", innerCode);
		}
		return sqlSessionTemplate.selectList("stockMapper.getStockOfCustomer", params);
	}

	@Override
	public List<Map> getIndustryOfCustomer(long uid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		return sqlSessionTemplate.selectList("stockMapper.getIndustryOfCustomer", params);
	}

	@Override
	public List<SharedStockListVo> getServiceStockList(long uid,
	                                                   long researcherId, int showNum, int currentPage, String sort,
	                                                   int sortRule, int stockExpire) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("showNum", showNum);
		params.put("currentPage", currentPage * showNum);
		params.put("stockExpire", stockExpire);
		params.put("researcherId", researcherId);
		params.put("sort", sort);
		params.put("sortRule", sortRule > 0 ? "ASC" : "DESC");
		return sqlSessionTemplate.selectList("stockMapper.getServiceStockList", params);
	}

	@Override
	public List<Map> getCustomerOfStock(long uid, List<Integer> list) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("list", list);
		return sqlSessionTemplate.selectList("stockMapper.getCustomerOfStock", params);
	}

	@Override
	public List<Map> getCustomerOfIndustry(long uid, List<Integer> list) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("list", list);
		return sqlSessionTemplate.selectList("stockMapper.getCustomerOfIndustry", params);
	}

	@Override
	public List<Map> getServiceStockNumList(long uid, int currentPage, int showNum) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		return sqlSessionTemplate.selectList("stockMapper.getServiceStockNumList", params);
	}

	@Override
	public List<Map> getManagedStockNumList(long uid, int currentPage, int showNum) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		return sqlSessionTemplate.selectList("stockMapper.getManagedStockNumList", params);
	}

	@Override
	public List<Map> getPositions(List<Long> list) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("list", list);
		return sqlSessionTemplate.selectList("stockMapper.getPositions", params);
	}

	@Override
	public List<Map> getOverServiceStockNumList(long authorId, int innerCode,
	                                            int stockLimitNum, List<Long> list) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("authorId", authorId);
		params.put("innerCode", innerCode);
		params.put("stockLimitNum", stockLimitNum);
		params.put("list", list);
		return sqlSessionTemplate.selectList("stockMapper.getOverServiceStockNumList", params);
	}

	@Override
	public List<Long> getOverServiceResearcherList(int industryCode,
	                                               List<Long> poolList) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("industryCode", industryCode);
		params.put("list", poolList);
		return sqlSessionTemplate.selectList("stockMapper.getOverServiceResearcherList", params);
	}

	@Override
	public List<Long> getCustomersWithNewMsg(long uid, List<Long> customers) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("list", customers);
		return sqlSessionTemplate.selectList("stockMapper.getMessageStates", params);
	}

	@Override
	public List<SelfStockCategoryVo> getSelfStockCategory(long uid, int dataType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("dataType", dataType);
		return sqlSessionTemplate.selectList("stockMapper.getSelfStockCategory", params);
	}

	@Override
	public List<Map<String, Object>> getSelfPoolShareListByType(long uid,
	                                                            int industryCode, int dataType, int shareType, long dm) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("industryCode", industryCode);
		params.put("shareType", shareType);
		params.put("dm", dm);
		params.put("dataType", dataType);
		return sqlSessionTemplate.selectList("stockMapper.getSelfPoolShareListByType", params);
	}

	@Override
	public Map<String, Object> querySelfStockMaxSort(long poolId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("poolId", poolId);
		return sqlSessionTemplate.selectOne("stockMapper.querySelfStockMaxSort", params);
	}

	@Override
	public Map<String, Object> querySelfStockMinSort(long poolId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("poolId", poolId);
		return sqlSessionTemplate.selectOne("stockMapper.querySelfStockMinSort", params);
	}

	@Override
	public List<Map<String, Object>> getStockStarList(int accountType, int innerCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("accountType", accountType);
		params.put("innerCode", innerCode);
		return sqlSessionTemplate.selectList("stockMapper.getStockStarList", params);
	}

	@Override
	public void saveAdStocks(long uid, String innerCodes) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("innerCodes", innerCodes);
		sqlSessionTemplate.update("stockMapper.saveAdStocks", params);
	}

	@Override
	public List<Integer> getContestTopStockList() {
		return sqlSessionTemplate.selectList("stockMapper.getContestTopStockList");
	}

	@Override
	public List<SelfStockVo> getStockListByOneCategory(long categoryId, long companyCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("categoryId", categoryId);
		params.put("companyCode", companyCode);
		return sqlSessionTemplate.selectList("stockMapper.getStockListByOneCategory", params);
	}

	@Override
	public String getSelfStockLabel(long uid, long dataId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dataId", dataId);
		params.put("uid", uid);

		return sqlSessionTemplate.selectOne("stockMapper.getSelfStockLabel", params);
	}

	@Override
	public boolean isInSelfstock(long uid, long dataId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dataId", dataId);
		params.put("uid", uid);

		int count = sqlSessionTemplate.selectOne("stockMapper.isInSelfstock", params);

		return count > 0;
	}

	@Override
	public List<SelfStockItem> getSelfStockList(String updateTime) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("updateTime", updateTime);

		return sqlSessionTemplate.selectList("stockMapper.getSelfStockList", params);
	}

	@Override
	public SecuExtInfo getStockOfHeatDesc(int innerCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("innerCode", innerCode);
		return sqlSessionTemplate.selectOne("stockMapper.getStockOfHeatDescByJob", params);
	}

	@Override
	public List<Integer> getSelfStockList(long uid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);

		return sqlSessionTemplate.selectList("stockMapper.getSelfStockListByUid", params);
	}

	@Override
	public SelfStockCategoryVo getSelfStockCategoryBySort(long uid, int dataType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("dataType", dataType);
		return sqlSessionTemplate.selectOne("stockMapper.getSelfStockCategoryBySort", params);
	}

	@Override
	public int checkStockIsExistByCondition(Map<String, Object> checkMap) {
		return sqlSessionTemplate.selectOne("stockMapper.checkStockIsExistByCondition", checkMap);
	}

	@Override
	public List<SelfStockVo> getSelfStockShare(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("stockMapper.getSelfStockShare", params);
	}

	@Override
	public int getSelfStockSharePeopleCount(Map<String, Object> paramsCount) {
		return sqlSessionTemplate.selectOne("stockMapper.getSelfStockSharePeopleCount", paramsCount);
	}

	@Override
	public List<SelfStockVo> getSelfPoolShareRecord(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("stockMapper.getSelfPoolShareRecord", params);
	}

	@Override
	public List<Map<String, Object>> getSelfPoolShareSecuRecord(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("stockMapper.getSelfPoolShareSecuRecord", params);
	}

	@Override
	public List<Long> getGroupIds(SelfStockVo vo) {
		return sqlSessionTemplate.selectList("stockMapper.getGroupIds", vo);
	}

	@Override
	public List<Long> getShareUids(Map<String, Object> map) {
		return sqlSessionTemplate.selectList("stockMapper.getShareUids", map);
	}

	@Override
	public SelfStockVo selectPoolByPrimaryKey(long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		return sqlSessionTemplate.selectOne("stockMapper.selectPoolByPrimaryKey", params);
	}

	@Override
	public void insertStockShareStatus(List<SelfStockVo> list) {
		sqlSessionTemplate.insert("stockMapper.insertStockShareStatus", list);
	}

	@Override
	public List<SelfStockVo> findselfStock(SelfStockVo data) {
		return sqlSessionTemplate.selectList("stockMapper.findselfStock", data);
	}

	@Override
	public List<Integer> getSelfShareInnerCodesByIndustries(Map<String, Object> map) {
		return sqlSessionTemplate.selectList("stockMapper.getSelfShareInnerCodesByIndustries", map);
	}

	@Override
	public List<Map<String, Object>> getSelfSharePeople(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("stockMapper.getSelfSharePeople", params);
	}

	@Override
	public void deleteSelfShareAction(Set<Long> viewsIds, Set<Long> groupIds, Set<Long> shareIds) {
		Map<String, Set> params = new HashMap<String, Set>();
		params.put("viewsIds", viewsIds);
		params.put("groupIds", groupIds);
		params.put("shareIds", shareIds);
		sqlSessionTemplate.delete("stockMapper.deleteSelfShareAction", params);
	}

	@Override
	public List<Map<String, Long>> getSelfShareMsg(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("stockMapper.getSelfShareMsg", params);
	}

	@Override
	public void deleteSelfShareActionByOrg(Map<String, Object> params) {
		sqlSessionTemplate.delete("stockMapper.deleteSelfShareActionByOrg", params);
	}

	@Override
	public List<Map<String, Object>> getSelfPoolShareSecuRecordFromStock(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("stockMapper.getSelfPoolShareSecuRecordFromStock", params);
	}

	@Override
	public Map<String, Object> getStockOrderStatistical(int innerCode, String startDate, String endDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("innerCode", innerCode);
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		return sqlSessionTemplate.selectOne("stockMapper.getStockOrderStatistical", params);
	}

	@Override
	public List<Map<String, Object>> getSecuOrderTrend(int innerCode, String startDate, String endDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("innerCode", innerCode);
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		return sqlSessionTemplate.selectList("stockMapper.getSecuOrderTrend", params);
	}

	@Override
	public List<Map<String, Object>> getSecuOrderNetTrend(int innerCode, String startDate, String endDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("innerCode", innerCode);
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		return sqlSessionTemplate.selectList("stockMapper.getSecuOrderNetTrend", params);
	}

	@Override
	public Map<String, Integer> getSecuOrderContrast(int innerCode, String startDate, String endDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("innerCode", innerCode);
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		return sqlSessionTemplate.selectOne("stockMapper.getSecuOrderContrast", params);
	}
}
