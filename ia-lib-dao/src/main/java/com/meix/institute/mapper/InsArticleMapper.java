package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsArticle;

public interface InsArticleMapper extends MeixBaseMapper<InsArticle> {
}