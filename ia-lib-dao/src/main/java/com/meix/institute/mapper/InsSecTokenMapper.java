package com.meix.institute.mapper;

import org.apache.ibatis.annotations.Param;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsSecToken;
import com.meix.institute.vo.SecToken;

public interface InsSecTokenMapper extends MeixBaseMapper<InsSecToken> {
    
    /**
     * 注销
     * @param record
     */
    void logout(InsSecToken record);
    
    /**
     * 获取token
     * @param user
     * @param clientType
     * @return
     */
    SecToken getTokenByUser(@Param("user") String user, @Param("clientType") Integer clientType);

	String getTokenByLtpaToken(@Param("ltpaToken") String ltpaToken, @Param("clientType") String clientType);
}