package com.meix.institute.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.meix.institute.vo.CustomerGroupPowerVo;
import com.meix.institute.vo.CustomerGroupVo;
import com.meix.institute.vo.DataShareVo;
import com.meix.institute.vo.GroupPowerVo;

@Repository
public class CustomerGroupMapper {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public int updateUserGroup(Map<String, Object> params) {
		return sqlSessionTemplate.update("customerGroupMapper.updateCustomerGroup", params);
	}

	public void addUserGroup(Map<String, Object> params) {
		sqlSessionTemplate.insert("customerGroupMapper.addCustomerGroup", params);
	}

	public List<Map<String, Object>> getCustomerGroupList(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("customerGroupMapper.getCustomerGroupList", params);
	}

	public int getCustomerGroupCount(Map<String, Object> params) {
		return sqlSessionTemplate.selectOne("customerGroupMapper.getCustomerGroupCount", params);
	}

	public int deleteCustomerGroup(long groupId) {
		Map<String, Object> params = new HashMap<>();
		params.put("groupId", groupId);
		return sqlSessionTemplate.delete("customerGroupMapper.deleteCustomerGroup", params);
	}

	public void deleteGroupCustomerRela(long groupId, List<Long> idList) {
		Map<String, Object> params = new HashMap<>();
		params.put("groupId", groupId);
		params.put("list", idList);
		sqlSessionTemplate.delete("customerGroupMapper.deleteGroupCustomerRela", params);
	}

	public void addGroupInfo(Map<String, Object> params) {
		sqlSessionTemplate.insert("customerGroupMapper.addGroupInfo", params);
	}

	public void updateGroupInfo(Map<String, Object> params) {
		sqlSessionTemplate.update("customerGroupMapper.updateGroupInfo", params);
	}

	public List<CustomerGroupPowerVo> getCustomerGroupPower(long groupId, int groupType) {
		Map<String, Object> params = new HashMap<>();
		params.put("groupId", groupId);
		params.put("groupType", groupType);
		return sqlSessionTemplate.selectList("customerGroupMapper.getCustomerGroupPower", params);
	}

	public int updateGroupPower(Map<String, Object> params) {
		return sqlSessionTemplate.update("customerGroupMapper.updateGroupPower", params);
	}

	public void addGroupPower(Map<String, Object> params) {
		sqlSessionTemplate.insert("customerGroupMapper.addGroupPower", params);
	}

	public void deleteDataGroupShare(long dataId, int dataType) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		sqlSessionTemplate.delete("customerGroupMapper.deleteDataShare", params);
	}

	public void addDataGroupShare(long dataId, int dataType, int dataSubType, List<Map<String, Object>> list) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		params.put("dataSubType", dataSubType);
		params.put("list", list);
		sqlSessionTemplate.insert("customerGroupMapper.addDataShare", params);
	}

	public List<GroupPowerVo> getResourceGroupPower(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("customerGroupMapper.getResourceGroupPower", params);
	}

	public void deleteCustomerFromAllGroup(long uid) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		sqlSessionTemplate.delete("customerGroupMapper.deleteCustomerFromAllGroup", params);
	}

	public List<CustomerGroupVo> getDefaultPowerGroup(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("customerGroupMapper.getDefaultPowerGroup", params);
	}

	public List<Long> getGroupUserList(long groupId) {
		Map<String, Object> params = new HashMap<>();
		params.put("groupId", groupId);
		return sqlSessionTemplate.selectList("customerGroupMapper.getGroupUserList", params);
	}

	public Map<String, Object> getCustomerGroupName(long groupId) {
		Map<String, Object> params = new HashMap<>();
		params.put("groupId", groupId);
		return sqlSessionTemplate.selectOne("customerGroupMapper.getCustomerGroupName", params);
	}

	public int checkGroupName(String groupName, long groupId) {
		Map<String, Object> params = new HashMap<>();
		params.put("groupId", groupId);
		params.put("groupName", groupName);
		return sqlSessionTemplate.selectOne("customerGroupMapper.checkGroupName", params);
	}

	/**
	 * 检查用户是否已经存在于目标分组中
	 * @param uid
	 * @param groupId
	 * @return
	 */
	public boolean checkUserIsInGroup(long uid, long groupId) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("groupId", groupId);
		int res = sqlSessionTemplate.selectOne("customerGroupMapper.checkUserIsInGroup", params);
		return res > 0 ? true : false;
	}

	/**
	 * 将用户从组中删除
	 * @param uid
	 * @param groupId
	 */
	public void deleteCustomerFromGroup(long uid, long groupId) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("groupId", groupId);
		sqlSessionTemplate.delete("customerGroupMapper.deleteCustomerFromGroup", params);
	}

	public List<DataShareVo> getDataShareList(long dataId, int dataType, int dataSubType, int location) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		params.put("dataSubType", dataSubType);
		params.put("location", location);
		return sqlSessionTemplate.selectList("customerGroupMapper.getDataShareList", params);
	}

	/**
	 * 删除组的权限
	 * @param groupId
	 */
	public void deleteCustomerGroupPower(long groupId) {
		Map<String, Object> params = new HashMap<>();
		params.put("groupId", groupId);
		sqlSessionTemplate.delete("customerGroupMapper.deleteCustomerGroupPower", params);
	}

	public boolean checkUserCanInGroup(long uid, long groupId) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("groupId", groupId);
		int res = sqlSessionTemplate.selectOne("customerGroupMapper.checkUserCanInGroup", params);
		return res > 0 ? true : false;
	}

	public String getGroupNameListByUser(long uid) {
		return sqlSessionTemplate.selectOne("customerGroupMapper.getGroupNameListByUser", uid);
	}

	/**
	 * 获取数据类型下的所有叶子节点权限
	 * @param dataType
	 * @param dataSubType
	 * @return
	 */
	public List<GroupPowerVo> getDataPowerLeaf(int dataType, int dataSubType) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataType", dataType);
		params.put("dataSubType", dataSubType);
		return sqlSessionTemplate.selectList("customerGroupMapper.getDataPowerLeaf", params);
	}
}
