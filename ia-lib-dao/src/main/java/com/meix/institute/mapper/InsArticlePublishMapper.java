package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsArticlePublish;

public interface InsArticlePublishMapper extends MeixBaseMapper<InsArticlePublish> {
}