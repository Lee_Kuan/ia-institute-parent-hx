package com.meix.institute.mapper;

import com.meix.institute.vo.report.ReportDetailVo;
import com.meix.institute.vo.user.InfluenceAnalyseVo;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @describe 晨会
 */
@Repository
public class ReportMapper {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public List<ReportDetailVo> getReportList(long uid, int type, long companyCode, int showNum, int currentPage, int clientType, long code, List<String> infoTypeLists, String publishDateFm, String publishDateTo, String condition, String lastSyncTime) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("type", type);
		params.put("companyCode", companyCode);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		params.put("clientType", clientType);
		params.put("code", code);
		params.put("infoTypes", infoTypeLists);
		params.put("publishDateFm", publishDateFm);
		params.put("publishDateTo", publishDateTo);
		params.put("condition", condition);
		params.put("updatedAt", lastSyncTime);
		return sqlSessionTemplate.selectList("reportMapper.getReportList", params);
	}

	public ReportDetailVo getReportDetail(long reportId, long uid) {
		Map<String, Object> params = new HashMap<>();
		params.put("reportId", reportId);
		params.put("uid", uid);
		return sqlSessionTemplate.selectOne("reportMapper.getReportDetail", params);
	}

	public Map<String, Object> getReportInnerCodeAndIndustry(long researchId) {
		return sqlSessionTemplate.selectOne("reportMapper.getReportInnerCodeAndIndustry", researchId);
	}

	public List<Map<String, Object>> getReportAlbumList(int type, int visible, String condition, String startTime, String endTime, int showNum, int currentPage, int status, int clientType, int sortField, int sortRule, int share) {
		Map<String, Object> params = new HashMap<>();
		params.put("type", type);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		params.put("visible", visible);
		params.put("condition", condition);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("status", status);
		params.put("clientType", clientType);
		params.put("sortField", sortField);
		params.put("sortRule", sortRule);
		params.put("share", share);
		return sqlSessionTemplate.selectList("reportMapper.getReportAlbumList", params);
	}

	public List<ReportDetailVo> getReportListByAlbum(long bid, long submenuId, int currentPage, int showNum, int type, int clientType) {
		Map<String, Object> params = new HashMap<>();
		params.put("bid", bid);
		params.put("submenuId", submenuId);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		params.put("type", type);
		params.put("clientType", clientType);
		return sqlSessionTemplate.selectList("reportMapper.getReportListByAlbum", params);
	}

	public Map<String, Object> getReportAlbumDetail(long bid) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", bid);
		return sqlSessionTemplate.selectOne("reportMapper.getReportAlbumDetail", params);
	}

	public List<Map<String, Object>> getReportAlbumSubmenuList(long bid) {
		Map<String, Object> params = new HashMap<>();
		params.put("bid", bid);
		return sqlSessionTemplate.selectList("reportMapper.getReportAlbumSubmenuList", params);
	}

	public int getReportAlbumCount(int type, int visible, String condition, String startTime, String endTime, int status, int share) {
		Map<String, Object> params = new HashMap<>();
		params.put("type", type);
		params.put("visible", visible);
		params.put("condition", condition);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("status", status);
		params.put("share", share);
		return sqlSessionTemplate.selectOne("reportMapper.getReportAlbumCount", params);
	}

	public List<InfluenceAnalyseVo> getResearchReportReadRank(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("reportMapper.getResearchReportReadRank", params);
	}

	public String getReportFileUrl(Long researchId) {
		return sqlSessionTemplate.selectOne("reportMapper.getReportAttachById", researchId);
	}

	public int getReportAlbumPermission(long albumId, long uid) {
		Map<String, Object> params = new HashMap<>();
		params.put("albumId", albumId);
		params.put("uid", uid);
		return sqlSessionTemplate.selectOne("reportMapper.getReportAlbumPermission", params);
	}
}
