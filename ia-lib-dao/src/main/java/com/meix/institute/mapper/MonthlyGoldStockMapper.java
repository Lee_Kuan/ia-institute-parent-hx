package com.meix.institute.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.meix.institute.vo.stock.GoldStockSelfUserInfo;
import com.meix.institute.vo.stock.MonthlyGoldStockListVo;

/**
 * 月度金股管理列表
 * by：likuan	2019年8月2日13:32:56
 */
@Repository
public class MonthlyGoldStockMapper {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public List<MonthlyGoldStockListVo> getMonthlyGoldStockList(int showNum, int currentPage, long companyCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("currentPage", showNum * currentPage);
		params.put("showNum", showNum);
		return sqlSessionTemplate.selectList("monthlyGoldStockMapper.getMonthlyGoldStockList", params);
	}

	public int getMonthlyGoldStockCount(long companyCode) {
		return sqlSessionTemplate.selectOne("monthlyGoldStockMapper.getMonthlyGoldStockCount", companyCode);
	}

	public List<GoldStockSelfUserInfo> getSelfStockUserList(int showNum, int currentPage, long innerCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("innerCode", innerCode);
		params.put("currentPage", showNum * currentPage);
		params.put("showNum", showNum);
		return sqlSessionTemplate.selectList("monthlyGoldStockMapper.getSelfStockUserList", params);
	}
	
}
