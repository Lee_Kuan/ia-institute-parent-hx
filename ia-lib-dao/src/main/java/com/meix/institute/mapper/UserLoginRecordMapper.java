package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.UserLoginRecord;

public interface UserLoginRecordMapper extends MeixBaseMapper<UserLoginRecord> {
}