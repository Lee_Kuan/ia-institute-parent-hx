package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsReportAlbumIssueType;

public interface InsReportAlbumIssueTypeMapper extends MeixBaseMapper<InsReportAlbumIssueType> {
}