package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.Attachment;

public interface AttachmentMapper extends MeixBaseMapper<Attachment> {
}