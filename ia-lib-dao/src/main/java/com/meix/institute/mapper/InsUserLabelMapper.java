package com.meix.institute.mapper;

import java.util.List;
import java.util.Map;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsUserLabel;

public interface InsUserLabelMapper extends MeixBaseMapper<InsUserLabel> {
    
    /**
     * 获取用户最关注的标签
     * @return
     */
    List<Map<String, Object>> getUserFocusLabel();
}