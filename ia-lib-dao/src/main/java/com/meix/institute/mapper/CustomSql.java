package com.meix.institute.mapper;

import java.util.HashMap;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.meix.institute.entity.RecommenedInfoLog;

/**
 * 测试自定义连表查询，实际应按业务分文件
 * Created by zenghao on 2019/7/29.
 */
@Repository
public class CustomSql {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public RecommenedInfoLog getLog(long id) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		RecommenedInfoLog recommenedInfoLog = sqlSessionTemplate.selectOne("customSqlMapper.getLog", params);
		return recommenedInfoLog;
	}
}
