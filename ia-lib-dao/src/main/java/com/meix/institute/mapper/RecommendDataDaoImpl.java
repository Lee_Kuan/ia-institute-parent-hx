package com.meix.institute.mapper;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2018/9/15.
 */
@Repository
public class RecommendDataDaoImpl {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public List<Map<String, Object>> getRecommendDataListByIds(String statement, List<Long> list) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", list);

		return sqlSessionTemplate.selectList(statement, params);
	}
}
