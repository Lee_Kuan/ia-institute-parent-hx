package com.meix.institute.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.meix.institute.vo.banner.InsBannerVo;

@Repository
public class BannerMapper {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public List<InsBannerVo> getBannerList(int applyModule, int bannerType, int currentPage, int showNum) {
		Map<String, Object> params = new HashMap<>();
		params.put("applyModule", applyModule);
		params.put("bannerType", bannerType);
		params.put("currentPage", showNum * currentPage);
		params.put("showNum", showNum);
		return sqlSessionTemplate.selectList("bannerMapper.getBannerList", params);
	}
}
