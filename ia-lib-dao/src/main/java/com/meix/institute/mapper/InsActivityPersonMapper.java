package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsActivityPerson;

public interface InsActivityPersonMapper extends MeixBaseMapper<InsActivityPerson> {
}