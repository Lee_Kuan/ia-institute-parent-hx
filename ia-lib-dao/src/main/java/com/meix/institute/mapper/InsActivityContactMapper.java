package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsActivityContact;

public interface InsActivityContactMapper extends MeixBaseMapper<InsActivityContact> {
}