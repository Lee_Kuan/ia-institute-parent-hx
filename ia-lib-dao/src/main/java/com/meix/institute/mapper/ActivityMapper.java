package com.meix.institute.mapper;

import com.meix.institute.vo.activity.Activity;
import com.meix.institute.vo.activity.ActivityDayNumberVo;
import com.meix.institute.vo.activity.ActivityListSearchVo;
import com.meix.institute.vo.user.InfluenceAnalyseVo;
import net.sf.json.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 活动管理
 * by：likuan	2019年8月2日13:32:56
 */
@Repository
public class ActivityMapper {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public List<ActivityDayNumberVo> getActivityNumOfDayByMonth(String date) {
		return sqlSessionTemplate.selectList("activitySqlMapper.getActivityNumOfDayByMonth", date);
	}

	public int updateActivityStatus(long activityId, int isEnd, String uuid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("activityId", activityId);
		params.put("isEnd", isEnd);
		params.put("uuid", uuid);
		return sqlSessionTemplate.update("activitySqlMapper.updateActivityStatus", params);
	}

	/**
	 * @Description: 获取活动列表
	 */
	public List<Activity> getActivityList(ActivityListSearchVo searchVo) {

		return sqlSessionTemplate.selectList("activitySqlMapper.getActivityList", searchVo);
	}

	/**
	 * 保存活动相关的直播信息
	 *
	 * @param params
	 * @return
	 */
	public int saveActivityGenseeWebcast(Map<String, Object> params) {
		return sqlSessionTemplate.insert("activitySqlMapper.saveActivityGenseeWebcast", params);
	}

	public Map<String, Object> getIsJoinActivity(long uid, long activityId) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("activityId", activityId);
		return sqlSessionTemplate.selectOne("activitySqlMapper.getIsJoinActivity", params);
	}

	public Map<String, Object> getWebcastInfo(long activityId) {
		Map<String, Object> params = new HashMap<>();
		params.put("activityId", activityId);
		return sqlSessionTemplate.selectOne("activitySqlMapper.getWebcastInfo", params);
	}

	public Map<String, Object> getVodInfo(long activityId) {
		Map<String, Object> params = new HashMap<>();
		params.put("activityId", activityId);
		return sqlSessionTemplate.selectOne("activitySqlMapper.getVodInfo", params);
	}

	public Map<String, Object> getActivityInnerCodeAndIndustry(long activityId) {
		return sqlSessionTemplate.selectOne("activitySqlMapper.getActivityInnerCodeAndIndustry", activityId);
	}

	public List<InfluenceAnalyseVo> getActivityAttendRank(Map<String, Object> params) {
		return sqlSessionTemplate.selectList("activitySqlMapper.getActivityAttendRank", params);
	}

	public long getActAttenderCount(long companyCode, String startTime, String endTime) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		Object result = sqlSessionTemplate.selectOne("activitySqlMapper.getActAttenderCount", params);
		if (result == null) {
			return 0;
		}
		return (long) result;
	}

	public List<Map<String, Object>> getActivityWhiteList(long activityId) {
		Map<String, Object> params = new HashMap<>();
		params.put("activityId", activityId);
		return sqlSessionTemplate.selectList("activitySqlMapper.getActivityWhiteList", params);
	}

	public void deleteActivityWhiteList(long activityId) {
		Map<String, Object> params = new HashMap<>();
		params.put("activityId", activityId);
		sqlSessionTemplate.delete("activitySqlMapper.deleteActivityWhiteList", params);
	}

	public void deleteActivityGenseeVideo(String webcastId) {
		Map<String, Object> params = new HashMap<>();
		params.put("webcastId", webcastId);
		sqlSessionTemplate.delete("activitySqlMapper.deleteActivityGenseeVideo", params);
	}

	public void deleteGenseeVod(long activityId) {
		Map<String, Object> params = new HashMap<>();
		params.put("activityId", activityId);
		sqlSessionTemplate.delete("activitySqlMapper.deleteGenseeVod", params);
	}

	public void saveGenseeVodList(List<JSONObject> list) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", list);
		sqlSessionTemplate.delete("activitySqlMapper.saveGenseeVodList", params);
	}
}
