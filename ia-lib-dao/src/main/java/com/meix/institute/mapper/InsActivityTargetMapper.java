package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsActivityTarget;

public interface InsActivityTargetMapper extends MeixBaseMapper<InsActivityTarget> {
}