package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.ReportRelation;

public interface ReportRelationMapper extends MeixBaseMapper<ReportRelation> {
}