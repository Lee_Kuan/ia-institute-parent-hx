package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsUserDirection;

public interface InsUserDirectionMapper extends MeixBaseMapper<InsUserDirection> {
}