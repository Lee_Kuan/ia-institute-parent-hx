package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsActivityAnalyst;

public interface InsActivityAnalystMapper extends MeixBaseMapper<InsActivityAnalyst> {
}