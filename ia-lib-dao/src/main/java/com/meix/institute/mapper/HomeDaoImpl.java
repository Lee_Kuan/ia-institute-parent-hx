package com.meix.institute.mapper;

import com.meix.institute.vo.HoneyBeeBaseVo;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/10/8.
 */
@Repository
public class HomeDaoImpl {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public List<HoneyBeeBaseVo> getSysWaterflowList(long uid, int currentPage, int showNum, int issueType) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("currentPage", currentPage);
		params.put("showNum", showNum);
		params.put("issueType", issueType);
		return sqlSessionTemplate.selectList("homeMapper.getSysWaterflowList", params);
	}

	/**
	 * 近一周内热门推荐
	 *
	 * @param currentPage
	 * @param showNum
	 * @return
	 */
	public List<HoneyBeeBaseVo> getLastWeekHotRecommend(int currentPage, int showNum, int issueType) {
		Map<String, Object> params = new HashMap<>();
		params.put("currentPage", currentPage);
		params.put("showNum", showNum);
		params.put("issueType", issueType);
		return sqlSessionTemplate.selectList("homeMapper.getLastWeekHotRecommend", params);
	}

	/**
	 * 一周以前热门推荐
	 *
	 * @param currentPage
	 * @param showNum
	 * @return
	 */
	public List<HoneyBeeBaseVo> getHistoryHotRecommend(int currentPage, int showNum, int issueType) {
		Map<String, Object> params = new HashMap<>();
		params.put("currentPage", currentPage);
		params.put("showNum", showNum);
		params.put("issueType", issueType);
		return sqlSessionTemplate.selectList("homeMapper.getHistoryHotRecommend", params);
	}
}
