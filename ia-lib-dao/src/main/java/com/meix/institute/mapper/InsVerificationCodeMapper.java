package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsVerificationCode;

public interface InsVerificationCodeMapper extends MeixBaseMapper<InsVerificationCode> {
}