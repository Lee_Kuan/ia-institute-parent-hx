package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsReportAlbumResearch;

public interface InsReportAlbumResearchMapper extends MeixBaseMapper<InsReportAlbumResearch> {
    
}