package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.WechatAccessToken;

public interface WechatAccessTokenMapper extends MeixBaseMapper<WechatAccessToken> {
}