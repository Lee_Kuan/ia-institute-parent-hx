package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.MorningMeeting;

public interface MorningMeetingMapper extends MeixBaseMapper<MorningMeeting> {
}