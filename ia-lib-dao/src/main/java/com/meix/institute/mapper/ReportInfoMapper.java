package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.ReportInfo;

public interface ReportInfoMapper extends MeixBaseMapper<ReportInfo> {
    
}