package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsCompanyMonthlyGoldStock;

public interface InsCompanyMonthlyGoldStockMapper extends MeixBaseMapper<InsCompanyMonthlyGoldStock> {
	
}