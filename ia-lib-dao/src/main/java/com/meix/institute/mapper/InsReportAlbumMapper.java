package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsReportAlbum;

public interface InsReportAlbumMapper extends MeixBaseMapper<InsReportAlbum> {
    
}