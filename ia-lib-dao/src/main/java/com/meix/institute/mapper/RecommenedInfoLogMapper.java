package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.RecommenedInfoLog;

public interface RecommenedInfoLogMapper extends MeixBaseMapper<RecommenedInfoLog> {

    RecommenedInfoLog getById(long id);
}