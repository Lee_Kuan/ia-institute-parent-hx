package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsUser;
import com.meix.institute.pojo.InsUserS;
import com.meix.institute.vo.user.UserInfo;

import java.util.List;
import java.util.Map;

public interface InsUserMapper extends MeixBaseMapper<InsUser> {

	/**
	 * 根据手机号获取用户信息
	 *
	 * @param search
	 * @return
	 */
	InsUser getUserByMobileOrEmail(InsUserS search);

	/**
	 * 获取待审核用户列表
	 *
	 * @param InsUserS daoS
	 * @return
	 */
	List<Map<String, Object>> getNotAuditCustomerList(InsUserS daoS);

	/**
	 * 统计待审核用户列表
	 *
	 * @param InsUserS daoS
	 * @return
	 */
	int countNotAuditCustomerList(InsUserS daoS);

	/**
	 * 更新名片使用状态
	 *
	 * @param isUsed 0 待处理  1 使用 -1作废
	 * @param id
	 */
	void updateCustomerCard(Integer isUsed, Long id);

	/**
	 * 查询标签列表
	 *
	 * @param search
	 * @return
	 */
	List<Map<String, Object>> getUserLableList(Map<String, Object> search);

	/**
	 * 统计标签列表
	 *
	 * @param search
	 * @return
	 */
	int countUserLable(Map<String, Object> search);

	/**
	 * 获取研究所用户信息
	 *
	 * @param daoS
	 * @return
	 */
	InsUser selectCustomerInfo(InsUserS daoS);

	/**
	 * 根据id获取研究所用户信息
	 *
	 * @param id
	 * @return
	 */
	UserInfo selectCustomerInfoById(long id);

	/**
	 * 根据第三方唯一标识获取用户信息
	 *
	 * @return
	 */
	InsUser selectByThirdId(String thirdId);

	/**
	 * 根据uuid获取用户信息
	 *
	 * @return
	 */
	InsUser selectByUuid(String uuid);

	/**
	 * 获取分析师列表
	 *
	 * @param params
	 * @return
	 */
	List<InsUser> getAnalystList(Map<String, Object> params);

	/**
	 * 统计分析师列表大小
	 *
	 * @param params
	 * @return
	 */
	int countAnalystList(Map<String, Object> params);

	/**
	 * 获取数据库中已存在的贝格id
	 *
	 * @param syncBgIds
	 * @return
	 */
	List<String> getBgExistsBgIds(List<String> syncBgIds);

}