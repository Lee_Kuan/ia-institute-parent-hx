package com.meix.institute.mapper;

import com.meix.institute.api.IDataStatDao;
import com.meix.institute.search.ClueSearchVo;
import com.meix.institute.search.DataRecordStatSearchVo;
import com.meix.institute.search.UserRecordSearchVo;
import com.meix.institute.vo.company.CompanyClueInfo;
import com.meix.institute.vo.company.CompanyClueUserStat;
import com.meix.institute.vo.stat.DataRecordStatVo;
import com.meix.institute.vo.stat.HeatDataVo;
import com.meix.institute.vo.user.UserClueStat;
import com.meix.institute.vo.user.UserRecordStatVo;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by zenghao on 2019/8/6.
 */
@Repository
public class DataStatDaoImpl implements IDataStatDao {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	@Override
	public List<DataRecordStatVo> getAllInsActivityList() {
		return sqlSessionTemplate.selectList("dataStatMapper.getAllInsActivityList");
	}

	@Override
	public List<Long> getInsReportIdList(String startDate, String endDate) {
		Map<String, Object> params = new HashMap<>();
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		return sqlSessionTemplate.selectList("dataStatMapper.getInsReportIdList", params);
	}

	@Override
	public DataRecordStatVo getInsReportByRid(long rid) {
		Map<String, Object> params = new HashMap<>();
		params.put("rid", rid);
		return sqlSessionTemplate.selectOne("dataStatMapper.getInsReportByRid", params);
	}

	@Override
	public long getDataReadStat(long dataId, int dataType) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		return sqlSessionTemplate.selectOne("dataStatMapper.getDataReadStat", params);
	}

	@Override
	public long getReadDuration(long dataId, int dataType) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		Object obj = sqlSessionTemplate.selectOne("dataStatMapper.getReadDuration", params);
		if (obj == null) {
			return 0;
		}
		return (long) obj;
	}

	@Override
	public long getDataShareStat(long dataId, int dataType, int activityType) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataId", dataId);
//		if (dataType == MeixConstants.USER_HD) {
//			params.put("dataType", activityType);
//		} else {
		params.put("dataType", dataType);
//		}
		return sqlSessionTemplate.selectOne("dataStatMapper.getDataShareStat", params);
	}

	@Override
	public List<UserRecordStatVo> getUserRecordList(long dataId, long dataType, int activityType) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		params.put("activityType", activityType);
		return sqlSessionTemplate.selectList("dataStatMapper.getUserRecordList", params);
	}

	@Override
	public List<CompanyClueInfo> getSubscribeAnalystRecordList(long companyCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		return sqlSessionTemplate.selectList("dataStatMapper.getSubscribeAnalystRecordList", params);
	}

	@Override
	public List<CompanyClueInfo> getJoinActivityRecordList(long companyCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		return sqlSessionTemplate.selectList("dataStatMapper.getJoinActivityRecordList", params);
	}

	@Override
	public List<CompanyClueUserStat> getJoinActivityRecordDetailList(long companyCode, long customerCompanyCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("customerCompanyCode", customerCompanyCode);
		return sqlSessionTemplate.selectList("dataStatMapper.getJoinActivityRecordDetailList", params);
	}

	@Override
	public List<CompanyClueInfo> getListenTeleActivityList() {
		return sqlSessionTemplate.selectList("dataStatMapper.getListenTeleActivityList");
	}

	@Override
	public List<CompanyClueUserStat> getListenTeleActivityDetailList(long companyCode, long customerCompanyCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("customerCompanyCode", customerCompanyCode);
		return sqlSessionTemplate.selectList("dataStatMapper.getListenTeleActivityDetailList", params);
	}

	@Override
	public List<CompanyClueInfo> getReadResearchReportList() {
		return sqlSessionTemplate.selectList("dataStatMapper.getReadResearchReportList");
	}

	@Override
	public List<CompanyClueUserStat> getReadReportDetailList(long companyCode, long customerCompanyCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("customerCompanyCode", customerCompanyCode);
		return sqlSessionTemplate.selectList("dataStatMapper.getReadReportDetailList", params);
	}

	@Override
	public int saveClue(CompanyClueInfo companyClueInfo) {
		return sqlSessionTemplate.insert("dataStatMapper.saveClue", companyClueInfo);
	}

	@Override
	public int updateClue(CompanyClueInfo companyClueInfo) {
		return sqlSessionTemplate.update("dataStatMapper.updateClue", companyClueInfo);
	}

	@Override
	public CompanyClueInfo getExistClueByUuid(String uuid) {
		Map<String, Object> params = new HashMap<>();
		params.put("uuid", uuid);
		return sqlSessionTemplate.selectOne("dataStatMapper.getExistClueByUuid", params);
	}

	@Override
	public CompanyClueInfo getExistClue(long companyCode, long customerCompanyCode, int clueType, String latestRecordTime) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("customerCompanyCode", customerCompanyCode);
		params.put("clueType", clueType);
		params.put("latestRecordTime", latestRecordTime);
		return sqlSessionTemplate.selectOne("dataStatMapper.getExistClue", params);
	}

	@Override
	public int removeClueUserStat(String clueUuid) {
		Map<String, Object> params = new HashMap<>();
		params.put("clueUuid", clueUuid);
		return sqlSessionTemplate.delete("dataStatMapper.removeClueUserStat", params);
	}

	@Override
	public List<CompanyClueUserStat> getClueUserStatList(String clueUuid) {
		Map<String, Object> params = new HashMap<>();
		params.put("clueUuid", clueUuid);
		return sqlSessionTemplate.selectList("dataStatMapper.getClueUserStatList", params);
	}

	@Override
	public int insertClueUserStat(CompanyClueUserStat companyClueUserStat) {
		return sqlSessionTemplate.insert("dataStatMapper.insertClueUserStat", companyClueUserStat);
	}

	@Override
	public int insertClueUserStatList(List<CompanyClueUserStat> list) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", list);
		return sqlSessionTemplate.insert("dataStatMapper.insertClueUserStatList", params);
	}

	@Override
	public int removeClueUserStatRange(long startId, long endId) {
		Map<String, Object> params = new HashMap<>();
		params.put("startId", startId);
		params.put("endId", endId);
		return sqlSessionTemplate.delete("dataStatMapper.removeClueUserStatRange", params);
	}

	@Override
	public int saveDataRecordStat(DataRecordStatVo dataRecordStatVo) {
		return sqlSessionTemplate.insert("dataStatMapper.saveDataRecordStat", dataRecordStatVo);
	}

	@Override
	public int updateDataRecordStat(DataRecordStatVo dataRecordStatVo) {
		return sqlSessionTemplate.update("dataStatMapper.updateDataRecordStat", dataRecordStatVo);
	}

	@Override
	public DataRecordStatVo getExistDataRecordStat(long companyCode, long dataId, int dataType) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		return sqlSessionTemplate.selectOne("dataStatMapper.getExistDataRecordStat", params);
	}

	@Override
	public DataRecordStatVo getExistDataRecordStatById(long id) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		return sqlSessionTemplate.selectOne("dataStatMapper.getExistDataRecordStatById", params);
	}

	@Override
	public List<DataRecordStatVo> getDataRecordStatList(DataRecordStatSearchVo statSearchVo) {
		return sqlSessionTemplate.selectList("dataStatMapper.getDataRecordStatList", statSearchVo);
	}

	@Override
	public long getDataRecordStatCount(DataRecordStatSearchVo statSearchVo) {
		return sqlSessionTemplate.selectOne("dataStatMapper.getDataRecordStatCount", statSearchVo);
	}

	@Override
	public int saveUserRecordStat(UserRecordStatVo userRecordStatVo) {
		return sqlSessionTemplate.insert("dataStatMapper.saveUserRecordStat", userRecordStatVo);
	}

	@Override
	public int updateUserRecordStat(UserRecordStatVo userRecordStatVo) {
		return sqlSessionTemplate.update("dataStatMapper.updateUserRecordStat", userRecordStatVo);
	}

	@Override
	public int removeUserRecordStat(int dataType, long dataId, Set<Long> uidSet) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataType", dataType);
		params.put("dataId", dataId);
		params.put("list", uidSet);
		return sqlSessionTemplate.delete("dataStatMapper.removeUserRecordStat", params);
	}

	@Override
	public UserRecordStatVo getExistUserRecordStat(long userId, long dataId, int dataType) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", userId);
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		return sqlSessionTemplate.selectOne("dataStatMapper.getExistUserRecordStat", params);
	}

	@Override
	public UserRecordStatVo getExistUserRecordStatById(long id) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		return sqlSessionTemplate.selectOne("dataStatMapper.getExistUserRecordStatById", params);
	}

	@Override
	public List<UserRecordStatVo> getUserRecordStatList(UserRecordSearchVo searchVo) {
		return sqlSessionTemplate.selectList("dataStatMapper.getUserRecordStatList", searchVo);
	}

	@Override
	public long getUserRecordStatCount(UserRecordSearchVo searchVo) {
		return sqlSessionTemplate.selectOne("dataStatMapper.getUserRecordStatCount", searchVo);
	}

	@Override
	public List<CompanyClueInfo> getClueList(ClueSearchVo searchVo) {
		return sqlSessionTemplate.selectList("dataStatMapper.getClueList", searchVo);
	}

	@Override
	public long getClueCount(ClueSearchVo searchVo) {
		return sqlSessionTemplate.selectOne("dataStatMapper.getClueCount", searchVo);
	}

	@Override
	public List<UserClueStat> getUserClueStatList(ClueSearchVo searchVo) {
		return sqlSessionTemplate.selectList("dataStatMapper.getUserClueStatList", searchVo);
	}

	@Override
	public long getUserClueStatCount(ClueSearchVo searchVo) {
		return sqlSessionTemplate.selectOne("dataStatMapper.getUserClueStatCount", searchVo);
	}

	@Override
	public int removeHeatDataRange(long startId, long endId) {
		Map<String, Object> params = new HashMap<>();
		params.put("startId", startId);
		params.put("endId", endId);
		return sqlSessionTemplate.delete("dataStatMapper.removeHeatDataRange", params);
	}

	@Override
	public void insertHeatDataList(List<HeatDataVo> list) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", list);
		sqlSessionTemplate.insert("dataStatMapper.insertHeatDataList", params);
	}

	@Override
	public List<Map<String, Object>> getHeatData(Map<String, Object> search) {
		return sqlSessionTemplate.selectList("dataStatMapper.getHeatData", search);
	}

	@Override
	public Map<String, Object> getHeatDataCount(Map<String, Object> search) {
		return sqlSessionTemplate.selectOne("dataStatMapper.getHeatDataCount", search);
	}

	@Override
	public List<Map<String, Object>> statisticalInnerCodeByCompsIndustry(String dataCode, int type) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataCode", dataCode);
		params.put("type", type);
		return sqlSessionTemplate.selectList("dataStatMapper.statisticalInnerCodeByCompsIndustry", params);
	}

	@Override
	public List<Map<String, Object>> getUserReadData(Map<String, Object> search) {
		return sqlSessionTemplate.selectList("dataStatMapper.getUserReadData", search);
	}

	@Override
	public Map<String, Object> getUserReadDataCount(Map<String, Object> search) {
		return sqlSessionTemplate.selectOne("dataStatMapper.getUserReadDataCount", search);
	}

	@Override
	public List<Map<String, Object>> getUserReadDataByUser(Map<String, Object> search) {
		return sqlSessionTemplate.selectList("dataStatMapper.getUserReadDataByUser", search);
	}

	@Override
	public Map<String, Object> getUserReadDataByUserCount(Map<String, Object> search) {
		return sqlSessionTemplate.selectOne("dataStatMapper.getUserReadDataByUserCount", search);
	}
}
