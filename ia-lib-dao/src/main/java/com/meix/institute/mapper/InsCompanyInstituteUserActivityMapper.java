package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsCompanyInstituteUserActivity;

public interface InsCompanyInstituteUserActivityMapper extends MeixBaseMapper<InsCompanyInstituteUserActivity> {
}