package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.ReportShare;

public interface ReportShareMapper extends MeixBaseMapper<ReportShare> {
}