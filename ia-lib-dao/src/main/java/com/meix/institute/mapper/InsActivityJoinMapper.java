package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsActivityJoin;

public interface InsActivityJoinMapper extends MeixBaseMapper<InsActivityJoin> {
}