package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsReportAlbumSubmenu;

public interface InsReportAlbumSubmenuMapper extends MeixBaseMapper<InsReportAlbumSubmenu> {
    
}