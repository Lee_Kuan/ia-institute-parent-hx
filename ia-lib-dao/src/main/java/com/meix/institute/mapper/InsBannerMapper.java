package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsBanner;

public interface InsBannerMapper extends MeixBaseMapper<InsBanner> {
    
}