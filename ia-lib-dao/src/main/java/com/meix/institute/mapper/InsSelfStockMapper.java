package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsSelfStock;

public interface InsSelfStockMapper extends MeixBaseMapper<InsSelfStock> {
}