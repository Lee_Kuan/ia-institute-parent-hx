package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsActivityIssueType;

public interface InsActivityIssueTypeMapper extends MeixBaseMapper<InsActivityIssueType> {
}