package com.meix.institute.mapper;

import com.meix.institute.vo.meeting.MorningMeetingVo;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @describe 晨会
 */
@Repository
public class MeetingMapper {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public MorningMeetingVo getMeetingDetail(long meetingId, long uid) {
		Map<String, Object> params = new HashMap<>();
		params.put("meetingId", meetingId);
		params.put("uid", uid);
		return sqlSessionTemplate.selectOne("myMeetingMapper.getMeetingDetail", params);
	}

	public List<MorningMeetingVo> getMeetingDetailList(List<Long> ids) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", ids);
		return sqlSessionTemplate.selectList("myMeetingMapper.getMeetingDetailList", params);
	}

	public List<MorningMeetingVo> getMeetingList(long uid, long companyCode, int showNum, int currentPage, int clientType, int meetingType) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("companyCode", companyCode);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		params.put("clientType", clientType);
		params.put("meetingType", meetingType);
		return sqlSessionTemplate.selectList("myMeetingMapper.getMeetingList", params);
	}

	/**
	 * 获取当天的晨会
	 *
	 * @param companyCode
	 * @param issueType   1研究所公众号
	 * @return
	 */
	public MorningMeetingVo getCurDateMeeting(long companyCode, int issueType) {
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", companyCode);
		params.put("issueType", issueType);
		return sqlSessionTemplate.selectOne("myMeetingMapper.getCurDateMeeting", params);
	}
}
