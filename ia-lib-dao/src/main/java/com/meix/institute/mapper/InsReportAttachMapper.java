package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsReportAttach;

public interface InsReportAttachMapper extends MeixBaseMapper<InsReportAttach> {
}