package com.meix.institute.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.meix.institute.api.InstituteDataCenterDao;


@Repository
public class InstituteDataCenterDaoImpl implements InstituteDataCenterDao {
    
    @Resource   
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    public List<Map<String, Object>> statisticsReportReadAndShareByIndustry(String startDate, Integer type) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);
        params.put("type", type);
        return sqlSessionTemplate.selectList("instituteDataCenter.statisticsReportReadAndShareByIndustry", params);
    }

    @Override
    public List<Map<String, Object>> statisticsActivityReadAndShareByIndustry(String startDate, Integer type) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);
        params.put("type", type);
        return sqlSessionTemplate.selectList("instituteDataCenter.statisticsActivityReadAndShareByIndustry", params);
    }

    @Override
    public List<Map<String, Object>> statisticsActivityJoinNumByIndustry(String startDate, Integer type) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);
        params.put("type", type);
        return sqlSessionTemplate.selectList("instituteDataCenter.statisticsActivityJoinNumByIndustry", params);
    }

    @Override
    public List<Map<String, Object>> statisticsReportReadAndShareByInnerCode(String startDate, Integer type) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);
        params.put("type", type);
        return sqlSessionTemplate.selectList("instituteDataCenter.statisticsReportReadAndShareByInnerCode", params);
    }

    @Override
    public List<Map<String, Object>> statisticsActivityReadAndShareByInnerCode(String startDate, Integer type) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);
        params.put("type", type);
        return sqlSessionTemplate.selectList("instituteDataCenter.statisticsActivityReadAndShareByInnerCode", params);
    }

    @Override
    public List<Map<String, Object>> statisticsActivityJoinNumByInnerCode(String startDate, Integer type) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);
        params.put("type", type);
        return sqlSessionTemplate.selectList("instituteDataCenter.statisticsActivityJoinNumByInnerCode", params);
    }

    @Override
    public void saveDuplicate(List<Map<String, Object>> list, Integer dateType, Integer companyCode, String field, Integer dataType) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("list", list);
        params.put("type", dateType);
        params.put("companyCode", companyCode);
        params.put("field", field);
        params.put("dataType", dataType);
        sqlSessionTemplate.insert("instituteDataCenter.saveDuplicate", params);
    }

    @Override
    public List<Map<String, Object>> statisticActivityNumByIndustry(String startDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);
        return sqlSessionTemplate.selectList("instituteDataCenter.statisticActivityNumByIndustry", params);
    }

    @Override
    public List<Map<String, Object>> statisticReportNumByIndustry(String startDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);
        return sqlSessionTemplate.selectList("instituteDataCenter.statisticReportNumByIndustry", params);
    }

    @Override
    public List<Map<String, Object>> statisticActivityNumByInnerCode(String startDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);
        return sqlSessionTemplate.selectList("instituteDataCenter.statisticActivityNumByInnerCode", params);
    }

    @Override
    public List<Map<String, Object>> statisticReportNumByInnerCode(String startDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);
        return sqlSessionTemplate.selectList("instituteDataCenter.statisticReportNumByInnerCode", params);
    }

    @Override
    public void clearHeadData() {
        sqlSessionTemplate.delete("instituteDataCenter.clearHeadData");
    }

    @Override
    public void copyInstituteHeatData() {
        sqlSessionTemplate.selectList("instituteDataCenter.copyInstituteHeatData");
    }

    @Override
    public void clearOrgHeatData() {
        sqlSessionTemplate.update("instituteDataCenter.clearOrgHeatData");
    }
}
