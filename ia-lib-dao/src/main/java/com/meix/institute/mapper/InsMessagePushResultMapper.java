package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsMessagePushResult;

public interface InsMessagePushResultMapper extends MeixBaseMapper<InsMessagePushResult> {
    
}