package com.meix.institute.mapper;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsArticleOther;

public interface InsArticleOtherMapper extends MeixBaseMapper<InsArticleOther> {
}