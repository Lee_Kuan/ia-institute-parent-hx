package com.meix.institute.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ConfigMapper {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public List<Map<String, Object>> getCustomerList(List<Integer> accountTypeList, int messageType, int currentPage, int showNum) {
		Map<String, Object> params = new HashMap<>();
		params.put("accountTypeList", accountTypeList);
		params.put("messageType", messageType);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		return sqlSessionTemplate.selectList("configMapper.getCustomerList", params);
	}

	public int getCustomerCount(List<Integer> accountTypeList) {
		Map<String, Object> params = new HashMap<>();
		params.put("accountTypeList", accountTypeList);
		return sqlSessionTemplate.selectOne("configMapper.getCustomerCount", params);
	}

	public List<Map<String, Object>> getReceiverList(int id, int currentPage, int showNum) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		return sqlSessionTemplate.selectList("configMapper.getReceiverList", params);
	}

	public int getReceiverCount(int id) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		return sqlSessionTemplate.selectOne("configMapper.getReceiverCount", params);
	}

	public String getSystemUrl(String type) {
		return sqlSessionTemplate.selectOne("configMapper.getSystemUrl", type);
	}
}
