package com.meix.institute.mapper;

import java.util.List;

import com.meix.institute.MeixBaseMapper;
import com.meix.institute.entity.InsSyncSndReq;
import com.meix.institute.pojo.InsSyncSndReqS;

public interface InsSyncSndReqMapper extends MeixBaseMapper<InsSyncSndReq> {
    
    /**
     * 查找异常同步数据
     * @return
     */
    List<InsSyncSndReq> findExceptionReq();
    
    /**
     * 列表查询
     * @param daoSearch
     * @return
     */
	List<InsSyncSndReq> find(InsSyncSndReqS daoSearch);
	
	 /**
     * 统计
     * @param daoSearch
     * @return
     */
	int count(InsSyncSndReqS daoSearch);
}