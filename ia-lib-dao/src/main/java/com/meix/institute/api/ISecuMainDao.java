//package com.meix.institute.api;
//
//import com.meix.institute.entity.SecuMain;
//
//import java.util.List;
//
///**
// * Created by zenghao on 2019/8/28.
// */
//public interface ISecuMainDao {
//
//	/**
//	 * @param nameCondition TODO
//	 * @Dscription 根据条件获取股票
//	 * @Createtime 2014-4-17下午01:47:47
//	 * @Param
//	 * @Return
//	 */
//	List<SecuMain> getSecuMain(int type, int showNum, String condition, String nameCondition, int industryCode);
//
//	/**
//	 * @param nameCondition TODO
//	 * @Dscription 根据条件获取港股股票
//	 * @Param
//	 * @Return
//	 */
//	List<SecuMain> getSecuMainForHK(int type, int showNum, String condition, String nameCondition, int industryCode);
//}
