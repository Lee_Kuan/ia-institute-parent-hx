package com.meix.institute.api;

import com.meix.institute.search.ClueSearchVo;
import com.meix.institute.search.ComplianceCommentSearchVo;
import com.meix.institute.vo.Comment;
import com.meix.institute.vo.CommentVo;
import com.meix.institute.vo.IA_PWDMapping;
import com.meix.institute.vo.user.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @类 说  明 : 用例测试
 */
@SuppressWarnings("rawtypes")
public interface IUserDao {

	/**
	 * 查询我关注的人员列表
	 *
	 * @author jiawj
	 * @since 2015年4月13日 上午10:09:22
	 */
	public List<Map<String, Object>> selectMyConcrenedPersons(Map<String, Object> params);

	/**
	 * @Dscription 获取所有用户
	 * @Createtime 2014-4-17下午01:47:47
	 * @Param
	 * @Return
	 */
	public List<UserInfo> getUser(Map<String, Object> params);

	public int getUserCount(Map params);

	/**
	 * 待审核用户列表
	 *
	 * @param params
	 * @return
	 */
	List<Map<String, Object>> getUncheckedUserList(Map<String, Object> params);

	int getUncheckedUserCount(Map<String, Object> params);

	/**
	 * @Dscription 保存用户
	 * @Createtime 2014-4-17下午01:48:06
	 * @Param
	 * @Return
	 */
	public void addUser(UserInfo user);

	/**
	 * @Dscription 删除用户
	 * @Createtime 2014-4-17下午01:48:18
	 * @Param
	 * @Return
	 */
	public int deleteUser(Map params);

	/**
	 * @param user
	 * @return
	 * @Title: updateUser、
	 * @Description:更改用户
	 * @return: int
	 */
	public int updateUser(UserInfo user);

	/**
	 * 用户审核不通过
	 *
	 * @param uid
	 * @return
	 */
	int userAuditNotPass(long uid, String operatorId, String reason);

	int userAuditPass(long uid, String userName, String operatorId, long companyCode, String position, int auditResult, long cardId, String email);

	/**
	 * @Dscription 保存验证码
	 * @Createtime 2014-4-17下午01:48:30
	 * @Param
	 * @Return
	 */
	public void saveVerCode(IA_PWDMapping bean);

	/**
	 * @Dscription 登录验证
	 * @Createtime 2014-4-17下午01:48:44
	 * @Param
	 * @Return
	 */
	public UserInfo login(UserInfo bean);

	/**
	 * @param loginType
	 * @Dscription 根据手机号码获取验证码
	 * @Createtime 2014-4-17下午01:48:53
	 * @Param
	 * @Return
	 */
	public IA_PWDMapping getUserVerCode(String mobileOrEmail, int loginType, String tday);

	/**
	 * @Dscription 登录成功后设置验证为作废状态
	 * @Param mobile
	 * @Param loginType 1 手机登录
	 */
	public boolean updateVerCode(String mobile, int loginType);

	/**
	 * @Dscription退出登录
	 * @Createtime 2014-4-24下午03:41:08
	 * @Param
	 * @Return
	 */
	public int loginOut(String token);

	/**
	 * @param areaCode
	 * @Dscription获取用户
	 * @Createtime 2014-5-13下午02:33:32
	 * @Param
	 * @Return
	 */
	public UserInfo getUserInfo(long uid, String areaCode, String mobileOrEmail);

	UserInfo getUserInfoByUuid(String user);

	/**
	 * @param mobile
	 * @return
	 * @Title: getUserByMobileOrEmail、
	 * @Description:根据手机号获取用户
	 * @return: List<Map>
	 */
	public List<Map> getUserByMobile(String mobile);

	/**
	 * @param account
	 * @param password
	 * @return
	 * @Title: getUserByAccount、
	 * @Description:根据账号获取用户信息
	 * @return: UserInfo
	 */
	public UserInfo getUserByAccount(String account, String password);

	/**
	 * @param params
	 * @return
	 * @Title: updateUserPassword、
	 * @Description:更改用户密码
	 * @return: int
	 */
	public int updateUserPassword(Map params);

	/**
	 * @return
	 * @Title: getPersonalInfo、
	 * @Description:获取个人信息
	 * @return: UserInfo
	 */
	public UserInfo getPersonalInfo(long uid, long authorId);

	/**
	 * @param params
	 * @return
	 * @Title: updateUserState、
	 * @Description:更新用户状态
	 * @return: int
	 */
	public int updateUserState(Map params);

	/**
	 * @author zhangzhen(J)
	 * @date 2015年11月10日上午10:55:03
	 * @Description: 邮件激活账户
	 */
	public int updateActivateAccount(UserInfo user);

	/**
	 * @param uid
	 * @param searchType 1 返回所有系统标签，2只返回选择的系统标签
	 * @author zhangzhen(J)
	 * @date 2015年11月12日上午10:31:05
	 * @Description: 获取行业系统标签
	 */
	public List<Map<String, Object>> getSystemLabel(long uid, int searchType);

	/**
	 * @author zhangzhen(J)
	 * @date 2015年11月12日上午10:42:27
	 * @Description: 保存行业系统标签
	 */
	public void saveSystemLabel(Map<String, Object> params);

	/**
	 * @author zhangzhen(J)
	 * @date 2015年11月12日上午11:05:20
	 * @Description: 更新用户身份类别
	 */
	public void updateUserIdentityType(long uid, int identityType);

	public void deleteSystemLabel(Map<String, Object> params);

	/**
	 * @param uid
	 * @return
	 * @Title: getUserHeadImage、
	 * @Description:获取用户头像
	 * @return: Map<String,Object>
	 */
	public Map<String, Object> getUserHeadImage(@Param(value = "uid") long uid);

	/**
	 * @author zhangzhen(J)
	 * @date 2016年3月28日下午2:05:53
	 * @Description: 通过微信的openId获取用户id
	 */
	public Map getUidByOpenId(String openId, int clientType);

	/**
	 * @param uid
	 * @param cardId
	 * @return
	 * @Title: updateUserCard、
	 * @Description:更新用户名片
	 * @return: int
	 */
	public int updateUserCard(long uid, long cardId, int authStatus, String companyAbbr, String email, String position, String username);

	/**
	 * @author zhangzhen(J)
	 * @date 2016年10月13日下午3:11:33
	 * @Description: 保存留言
	 */
	public void saveComment(Comment comment);

	List<CommentVo> getCommentList(long id, int type, int currentPage, int showNum, Set<Long> ids, long companyCode, String condition);

	/**
	 * @author zhangzhen(J)
	 * @date 2016年10月26日下午1:53:18
	 * @Description: 通过id查询留言
	 */
	public Comment getCommentById(long dataId);

	/**
	 * @author zhangzhen(J)
	 * @date 2016年10月26日下午2:16:58
	 * @Description: 更新留言点赞数
	 */
	public void updateCommentPraise(long dataId, int num);

	/**
	 * @param uid
	 * @param showNum
	 * @param currentPage
	 * @Title: getMyFocusedPersons、
	 * @Description:我关注的人
	 * @return: List<UserIdVo>
	 */
	public List<UserIdVo> getMyFocusedPersons(long uid, int showNum, int currentPage);

	/**
	 * @param uid
	 * @param showNum
	 * @param currentPage
	 * @Title: getFocusedPersonsByMyFocusedPersons、
	 * @Description:我关注的人关注的人
	 * @return: List<UserIdVo>
	 */
	public List<UserIdVo> getFocusedPersonsByMyFocusedPersons(long uid, int showNum, int currentPage);

	/**
	 * @param uid
	 * @param showNum
	 * @param currentPage
	 * @Title: getHotFocusedPersons、
	 * @Description:热门关注人员列表
	 * @return: List<UserIdVo>
	 */
	public List<UserIdVo> getHotFocusedPersons(long uid, int showNum, int currentPage);

	/**
	 * @param uid
	 * @param showNum
	 * @param currentPage
	 * @Title: getPersonsFocusMe、
	 * @Description:关注我的人
	 * @return: List<UserIdVo>
	 */
	public List<UserIdVo> getPersonsFocusMe(long uid, int showNum, int currentPage);

	/**
	 * @param uid
	 * @param showNum
	 * @param currentPage
	 * @Title: getPersonsFocusMe、
	 * @Description:关注我的人
	 * @return: List<UserIdVo>
	 */
	public List<UserIdVo> getMyFocusedPersonsFocusMe(long uid, int showNum, int currentPage);

	/**
	 * 2017年7月21日下午5:39:01<br>
	 * 保存留言作者
	 *
	 * @author zhangzhen(J)
	 */
	public int saveCommentAuthorId(long id, List<Long> authorIds);

	/**
	 * 更新用户状态
	 *
	 * @param uid
	 * @param status
	 * @return
	 */
	public int updateUserStatus(long uid, int status);

	/**
	 * 保存活动关系表
	 *
	 * @param uid
	 * @param dataId
	 * @param dataType
	 * @return
	 */
	public int saveCommentRelation(long uid, long dataId, int dataType);

	/**
	 * 获取用户ID列表
	 *
	 * @param startId
	 * @return
	 */
	public List<Long> getUIDList(long startId, int currentPage, int showNum);

	/**
	 * 获取用户行为记录
	 *
	 * @param startId     TODO
	 * @param currentPage
	 * @param showNum
	 * @return
	 */
	public List<UserRecordStateVo> getUserRecordList(String startId, int currentPage, int showNum);

	/**
	 * 新增用户点击记录
	 *
	 * @param vo
	 */
	public void saveLastUserRecord(UserRecordStateVo vo);

	/**
	 * 判断是否是关注的作者
	 *
	 * @param uid
	 * @param authorCode
	 * @return
	 */
	public boolean isFocusedPerson(long uid, long authorCode);

	/**
	 * 获取关注的作者
	 *
	 * @param uid
	 * @return
	 */
	public List<Long> getFocusedPersons(long uid);

	/**
	 * 关注数
	 *
	 * @param uid
	 * @return
	 */
	public long getFocusedPersonCount(long uid);

	/**
	 * likuan  2019年6月28日14:57:58
	 *
	 * @param uid
	 * @param appId
	 * @return
	 */
	public String getOpenIdByUid(long uid, String appId, int clientType);

	/**
	 * 解绑微信绑定
	 *
	 * @param mobile
	 */
	public void unbindweixin(String mobile, int clientType);

	/**
	 * @param vo
	 * @return
	 * @Title: updateUserRecordState、
	 * @Description:更新用户记录状态
	 * @return: int
	 */
	public int updateUserRecordState(UserRecordStateVo vo);

	/**
	 * @param vo
	 * @return
	 * @Title: deleteUserRecordState、
	 * @Description:删除状态
	 * @return: int
	 */
	public int deleteUserRecordState(UserRecordStateVo vo);

	/**
	 * @param vo
	 * @return
	 * @Title: saveUserRecordState、
	 * @Description:保存用户记录状态
	 * @return: int
	 */
	public int saveUserRecordState(UserRecordStateVo vo);

	/**
	 * @param vo
	 * @return
	 * @Title: saveUserRecordStateLog、
	 * @Description:保存流水记录
	 * @return: int
	 */
	public int saveUserRecordStateLog(UserRecordStateVo vo);

	/**
	 * @param list
	 * @return
	 * @Title: batchInsertUserRecordStateLog、
	 * @Description:批量保存用户记录
	 * @return: int
	 */
	public int batchInsertUserRecordStateLog(List<UserRecordStateVo> list);

	UserRecordStateVo getUserRecordState(int dataType, long dataId, long uid, int dataState);

	public void updateUserCompanyCode(long uid, long companyCode, String userName);

	/**
	 * @Description: 获取用户最新名片
	 */
	public Map<String, Object> selectCardByUid(long uid, String mobile, int type);

	/**
	 * @param params
	 * @return
	 * @Title: addCustomerCard、
	 * @Description:添加客户名片
	 * @return: int
	 */
	public int addCustomerCard(Map params);

	int updateCustomerCardStatus(long cardId, int isUsed);

	public List<UserRecommendationInfo> getRelatedRecommendations(long uid, long dataId, int dataType,
	                                                              int currentPage, int showNum,
	                                                              int clientType, int accountType);

	public List<UserInfo> getActivityAnalystInfo(long activityId);

	public List<UserInfo> getReportAuthorInfo(long dataId);

	/**
	 * 粉丝数（关注我的人数）
	 *
	 * @param uid
	 * @return
	 */
	public int getUesrFollowNum(long uid);

	/**
	 * 阅读数
	 *
	 * @param uid
	 * @return
	 */
	int getReadNum(long uid);

	public List<UserRecommendationInfo> getRelatedRecommendationsByAuthor(long uid, long authorId, long dataId, int dataType,
	                                                                      int currentPage, int showNum,
	                                                                      int clientType, int accountType);

	public List<UserRecommendationInfo> getRelatedRecommendationsByTime(long uid, long dataId, int dataType,
	                                                                    int currentPage, int showNum,
	                                                                    int clientType, int accountType);

	public List<String> getRelatedRecommendationsSecuInfo(long dataId, int dataType);

	/**
	 * 根据第三方获取用户id
	 *
	 * @param thirdId
	 * @return
	 */
	public Long getUidByThirdId(String thirdId);

	/**
	 * 根据Id获取手机号
	 *
	 * @param id
	 * @return
	 */
	public String getMobileById(long id);

	public List<UserLabelScoreVo> getUserLabelScoreList();

	public void deleteUserPersonas(List<Long> uidList);

	public String getUserPersonas(String labelId);

	public void updateUserPersonas(UserPersonasVo label);

	public void addUserPersonas(UserPersonasVo label);

	public int updateLoginInfo(long uid);

	public void insertLoginInfo(long uid);

	public List<Map<String, Object>> getCustomerList(int accountType, int authStatus, String condition, int currentPage, int showNum, String updatedAt, String sortField, int sortRule, long groupId, int isExcept);

	public int getCustomerCount(int accountType, int authStatus, String condition, long groupId, int isExcept);

	public List<UserClueStat> getAuthedUserClueList(ClueSearchVo searchVo);

	public int getAuthedUserClueCount(ClueSearchVo searchVo);

	public List<CommentVo> getCommentList(ComplianceCommentSearchVo searchVo);

	public int getCommentCount(ComplianceCommentSearchVo searchVo);

	public void examineComment(long commentId, int optType, String uuid);

	public void updateWechatInfo(UserInfo userInfo);

	public List<UserInfo> getPushWechatMsgUserList(long companyCode);

	public List<Map<String, Object>> getUserLabelPushInfo();

	public void saveUserLabelPushRecord(long uid, long dataId, int type);

	public boolean isFromMyFocusedPerson(long uid, int dataType, long dataId);

	public boolean isFromMyFocusedStock(long uid, int dataType, long dataId);

	public void updateThirdIdByMobile(Map<String, Object> params);

	public void updateUserPrivacyPolicyStatus(Map<String, Object> params);

	long getLoginRecordCount(long companyCode, String startTime, String endTime);

	List<UserRecordStateResp> getUserRecordStateList(Map<String, Object> params);

	List<UserContentReadStat> getUserContentReadStat(Map<String, Object> params);

	/**
	 * 获取用户发布内容数量
	 *
	 * @param params
	 * @return
	 */
	long getUserContentCount(Map<String, Object> params);

	/**
	 * 获取阅读人数
	 *
	 * @param dataType
	 * @param dataId
	 * @return
	 */
	long getDataReaderNum(int dataType, long dataId, String startTime, String endTime);

	/**
	 * 获取阅读数
	 *
	 * @return
	 */
	long getDataReadNum(long uid, String startTime, String endTime);

	public UserInfo getUserInfoByUserName(String salerName);

	public UserInfo getUserInfo(long uid, String area, String mobile, int isWhite);

	public void deleteWhiteUser(String uuid, long uid, int accountType);

	public void updateUserIdentify(Map<String, Object> params);
	
	public UserLabelScoreVo getUserLabelScore(UserLabelScoreVo vo);

	public void updateUserLabelScore(UserLabelScoreVo vo);

	public void addUserLabelScore(UserLabelScoreVo vo);

	public void deleteUserLabelScore(List<Long> existId);

	public void saveUserOperatorLog(Map<String, Object> params);

}
