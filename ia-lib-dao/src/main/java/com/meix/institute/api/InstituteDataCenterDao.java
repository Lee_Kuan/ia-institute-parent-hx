package com.meix.institute.api;

import java.util.List;
import java.util.Map;


public interface InstituteDataCenterDao {
    
    /**
     * 统计研报阅读分享数——行业
     * @param startDate 开始时间
     * @param type 用户类型 1 全部 2 白名单
     * @return
     */
    List<Map<String, Object>> statisticsReportReadAndShareByIndustry(String startDate, Integer type);
    
    /**
     * 活动阅读分享 (包括电话会议)——行业
     * @param startDate 开始时间
     * @param type 用户类型 1 全部 2 白名单
     * @return
     */
    List<Map<String, Object>> statisticsActivityReadAndShareByIndustry(String startDate, Integer type);
    
    /**
     * 活动参加人数——行业
     * @param startDate 开始时间
     * @param type 用户类型 1 全部 2 白名单
     * @return
     */
    List<Map<String, Object>> statisticsActivityJoinNumByIndustry(String startDate, Integer type);
    
    /**
     * 统计研报阅读分享数——股票
     * @param startDate 开始时间
     * @param type 用户类型 1 全部 2 白名单
     * @return
     */
    List<Map<String, Object>> statisticsReportReadAndShareByInnerCode(String startDate, Integer type);
    
    /**
     * 活动阅读 分享数(包括电话会议)——股票
     * @param startDate 开始时间
     * @param type 用户类型 1 全部 2 白名单
     * @return
     */
    List<Map<String, Object>> statisticsActivityReadAndShareByInnerCode(String startDate, Integer type);
    
    /**
     * 活动参加人数——股票
     * @param startDate 开始时间
     * @param type 用户类型 1 全部 2 白名单
     * @return
     */
    List<Map<String, Object>> statisticsActivityJoinNumByInnerCode(String startDate, Integer type);
    
    /**
     * 保存数据中心
     * @param report 数据
     * @param dateType 日期类型 ： 1 近一月 2 近三月 3 近半年 4 近一年
     * @param companyCode 公司代码 ： 0 全部   非0 机构代码
     * @param field 表字段
     * @param 1 股票 2 行业
     */
    void saveDuplicate(List<Map<String, Object>> report, Integer dateType, Integer companyCode, String field, Integer type);
    
    /**
     * 统计活动数——行业
     * @param startDate 开始时间
     * @return
     */
    List<Map<String, Object>> statisticActivityNumByIndustry(String startDate);
    
    /**
     * 统计研报数——行业
     * @param startDate
     * @return
     */
    List<Map<String, Object>> statisticReportNumByIndustry(String startDate);
    
    /**
     * 统计活动数——股票
     * @param startDate 开始时间
     * @return
     */
    List<Map<String, Object>> statisticActivityNumByInnerCode(String startDate);
    
    /**
     * 统计活动数——股票
     * @param startDate 开始时间
     * @return
     */
    List<Map<String, Object>> statisticReportNumByInnerCode(String startDate);
    
    /**
     * 清空热度数据
     */
    void clearHeadData();
    
    /**
     * copy演技所热度数据
     */
    void copyInstituteHeatData();
    
    /**
     * 清空公司热度
     */
    void clearOrgHeatData();
    
  
}
