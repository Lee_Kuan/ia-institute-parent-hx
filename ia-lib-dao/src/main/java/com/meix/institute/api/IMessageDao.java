package com.meix.institute.api;

import com.meix.institute.vo.message.MeetingMessageVo;

import java.util.List;

/**
 * Created by zenghao on 2019/9/26.
 */
public interface IMessageDao {
	/**
	 * @param uid
	 * @param activityId
	 * @param speakerId
	 * @param messageId
	 * @param messageFlag
	 * @param showNum
	 * @return
	 * @Title: getMeetingMessages、
	 * @Description:获取会议留言
	 */
	public List<MeetingMessageVo> getMeetingMessages(long uid, long activityId,
	                                                 long speakerId, long messageId, int messageFlag, int showNum);

	/**
	 * @param mmv
	 * @return
	 * @Title: saveMeetingMessageDetail、
	 * @Description:保存会议留言详情
	 */
	public int saveMeetingMessageDetail(MeetingMessageVo mmv);

	/**
	 * @param mmv
	 * @Title: saveMeetingMessage、
	 * @Description:保存会议留言
	 */
	public void saveMeetingMessage(MeetingMessageVo mmv);
}
