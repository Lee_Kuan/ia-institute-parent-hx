package com.meix.institute.api;

import com.meix.institute.vo.stock.*;

import java.util.List;
import java.util.Map;
import java.util.Set;
@SuppressWarnings("rawtypes")
public interface ISelfStockDao {

	/**
	 * 查询人员选中状态列表（研究报告）
	 *
	 * @author jiawj
	 * @since 2015年4月3日 下午2:33:48
	 */
	public List<Map<String, Object>> selectPersonalSelected2(Map<String, Object> params);

	/**
	 * 查询选中人员列表
	 *
	 * @author jiawj
	 * @since 2015年4月3日 下午1:35:44
	 */
	public List<Map<String, Object>> selectSelectedPersonList(Map<String, Object> params);

	/**
	 * 查询人员选中状态列表
	 *
	 * @author jiawj
	 * @since 2015年4月1日 上午9:43:55
	 */
	public List<Map<String, Object>> selectPersonalSelected(Map<String, Object> params);

	/**
	 * 删除人员选中状态
	 *
	 * @author jiawj
	 * @since 2015年3月31日 下午5:25:48
	 */
	public int deleteSelectedPerson(Map<String, Object> params);

	/**
	 * 保存人员选中状态
	 *
	 * @author jiawj
	 * @since 2015年3月31日 下午5:24:20
	 */
	public int insertSelectedPerson(Map<String, Object> params);

	/**
	 * @方法信息：2014-4-22 zhouwei
	 * @功能介绍：获取自选股的目录
	 */
	public List<SelfStockCategoryVo> querySelfCategory(Map<String, Object> params);

	/**
	 * @方法信息：2014-4-22 zhouwei
	 * @功能介绍：获取自选股的股票列表
	 */
	public List<SelfStockVo> querySelfStock(Map<String, Object> params);

	/**
	 * @方法信息：2014-4-22 zhouwei
	 * @功能介绍：保存自选股目录
	 */
	public int insertSelfCategory(SelfStockCategoryVo data);

	/**
	 * @方法信息：2014-4-22 zhouwei
	 * @功能介绍：更新自选股目录
	 */
	public int updateSelfCategory(SelfStockCategoryVo data);

	/**
	 * @方法信息：2014-4-22 zhouwei
	 * @功能介绍：删除自选股目录
	 */
	public int deleteSelfCategory(SelfStockCategoryVo data);

	/**
	 * @方法信息：2014-4-22 zhouwei
	 * @功能介绍：保存自选股
	 */
	public int insertSelfStock(List<SelfStockVo> list);

	/**
	 * @方法信息：2014-4-22 zhouwei
	 * @功能介绍：更新自选股
	 */
	public int updateSelfStock(SelfStockVo data);

	/**
	 * @方法信息：2014-4-22 zhouwei
	 * @功能介绍：删除自选股
	 */
	public int deleteSelfStock(Map params);

	/**
	 * @方法信息：2014-4-29 zhouwei
	 * @功能介绍：查询用户所有的自选股信息
	 */
	public List<SelfStockVo> queryAllSelfStock(Map<String, Object> params);

	/**
	 * 查询用户自选股信息
	 *
	 * @param params
	 * @return
	 */
	public List<StockVo> getUserSelfStockList(Map<String, Object> params);

	SelfStockVo getSelfStock(long uid, int innerCode);

	/**
	 * @方法信息：2014-4-22 zhouwei
	 * @功能介绍：获取所有的股票
	 */
	public List<Map> getAllStock(Map<String, Object> params);

	/**
	 * @方法信息：2014-5-20 zhouwei
	 * @功能介绍：获取用户的某只股票的信息
	 */
	public SelfStockVo getStockOfUser(Map<String, Object> params);

	/**
	 * @param flag -1 去最小 1 取最大
	 * @方法信息：2014-5-20 zhouwei
	 * @功能介绍：获取股票目录下的最大序号
	 */
	public int getSortNoOfCategory(long categoryId, int flag);

	/**
	 * @方法信息：2014-5-21 zhouwei
	 * @功能介绍：根据内码获取股票信息
	 */
	public SelfStockVo getStockByInnerCode(Map<String, Object> params);

	/**
	 * @方法信息：2014-5-21 zhouwei
	 * @功能介绍：根据代码获取股票信息
	 */
	public SelfStockVo getStockBySecuCode(Map<String, Object> params);

	/**
	 * @方法信息：2014-6-12 zhouwei
	 * @功能介绍：验证自选股中是否添加此股票代码
	 */
	public int checkStockIsExist(long categoryId, int innerCode);

	/**
	 * @param params
	 * @return
	 * @Title: getStockListByCategory、
	 * @Description:根据多目录ID获取自选股列表
	 * @return: List<SelfStockVo>
	 */
	public List<SelfStockVo> getStockListByCategory(Map params);

	/**
	 * @param list
	 * @return
	 * @Title: addSelfPoolShare、
	 * @Description:添加自选股分享
	 * @return: int
	 */
	public int addSelfPoolShare(List<SelfStockCategoryShareVo> list);

	/**
	 * @return
	 * @Title: deleteSelfPoolShare、
	 * @Description:删除自选股分享
	 * @return: int
	 */
	public int deleteSelfPoolShare(long groupId, List<SelfStockCategoryShareVo> list);

	/**
	 * @param uid
	 * @return
	 * @Title: getStockCategoryAuthorizedList、
	 * @Description:获取自选股授权
	 * @return: List<SelfStockCategoryShareVo>
	 */
	public List<SelfStockCategoryShareVo> getStockCategoryAuthorizedList(int showNum, int currentPage, long uid);

	/**
	 * @param params
	 * @return
	 * @Title: updateSelectedPerson、
	 * @Description:更新选中人员
	 * @return: int
	 */
	public int updateSelectedPerson(Map params);

	/**
	 * @param params
	 * @return
	 * @Title: getStockNumOfCategory、
	 * @Description:获取自选股目录下的股票数量
	 * @return: int
	 */
	public int getStockNumOfCategory(Map params);

	/**
	 * @param params
	 * @return
	 * @Title: getCandle
	 * @Description:获得日K线
	 * @return: int
	 */
	public List<Map> getCandle(Map params);

	/**
	 * @author zhangzhen(J)
	 * @date 2015年11月3日下午4:21:07
	 * @Description: 个人中心-股票 v1.3.0
	 */
	public List<Map<String, Object>> getPersonalPointView(
		Map<String, Object> params);

	/**
	 * @author zhangzhen(J)
	 * @date 2016年3月2日上午10:27:59
	 * @Description: 获取自选股的所有股票
	 */
	public List<SelfStockVo> getAllSelfPoolSecu(long uid);

	/**
	 * @param uid
	 * @param maxTime
	 * @return
	 * @Title: getStockPoolChangedList、
	 * @Description:获取被分享的自选股池的变更列表
	 * @return: List<Map>
	 */
	public List<Map> getStockPoolChangedList(long uid, String maxTime);

	/**
	 * @param groupId
	 * @return
	 * @Title: getSelfPoolShare、
	 * @Description:获取自选股池的授权
	 * @return: List<SelfStockCategoryShareVo>
	 */
	public List<SelfStockCategoryShareVo> getSelfPoolShare(long groupId);

	/**
	 * @param uid
	 * @param categoryId
	 * @param groupId
	 * @param list
	 * @return
	 * @Title: saveSelfPoolGroup、
	 * @Description:保存自选股池行业分组
	 * @return: int
	 */
	public int saveSelfPoolGroup(long uid, long categoryId, long groupId, List<Integer> list);

	/**
	 * @param poolId
	 * @return
	 * @Title: getSelfPoolType、
	 * @Description:获取股票池类型 0 用户目录 1、系统创建目录 2、睿行研
	 */
	public int getSelfPoolType(long poolId);

	/**
	 * @param groupId
	 * @return
	 * @Title: deleteSelfPoolGroup、
	 * @Description:删除自选股池的行业分组
	 * @return: int
	 */
	public int deleteSelfPoolGroup(long groupId, long uid);

	/**
	 * @param groupId
	 * @param uid
	 * @return
	 * @Title: getStockCategoryAuthorizedDetail、
	 * @Description:获取自选股池的授权明细
	 * @return: List<SelfStockCategoryShareVo>
	 */
	public List<SelfStockCategoryShareVo> getStockCategoryAuthorizedDetail(long groupId, long uid);

	/**
	 * @param stockExpire
	 * @author zhangzhen(J)
	 * @date 2016年4月6日上午10:28:12
	 * @Description: 获取分享的股票池
	 */
	public List<Map<String, Object>> getSharedStockCategory(long uid,
	                                                        int showNum, int currentPage, int stockExpire, int type);

	/**
	 * @author zhangzhen(J)
	 * @date 2016年4月6日下午3:33:50
	 * @Description: 获取分享的股票池中的股票
	 */
	public List<SharedStockListVo> getSharedStockByCategoryId(long uid,
	                                                          long categoryId, int showNum, int currentPage, String sort,
	                                                          int sortRule, int stockExpire);

	/**
	 * @author zhangzhen(J)
	 * @date 2016年4月25日上午10:23:14
	 * @Description: 查询分享的股票池中旧的自选股id
	 */
	public long getOldCategoryId(long groupId);

	/**
	 * @Title: getStockOfCustomer、
	 * @Description:获取客户的股票分组
	 * @return: List<Map>
	 */
	public List<Map> getStockOfCustomer(long uid, int currentPage, int showNum, int innerCode, String sort,
	                                    int sortRule);

	/**
	 * @param uid
	 * @return
	 * @Title: getIndustryOfCustomer、
	 * @Description:获取客户的行业分组
	 * @return: List<Map>
	 */
	public List<Map> getIndustryOfCustomer(long uid);

	/**
	 * @param uid  研究员ID
	 * @param list 股票内码数组
	 * @return
	 * @Title: getCustomerOfStock、
	 * @Description:
	 * @return: List<Map>
	 */
	public List<Map> getCustomerOfStock(long uid, List<Integer> list);

	/**
	 * @param uid  研究员ID
	 * @param list 行业内码数组
	 * @return
	 * @Title: getCustomerOfIndustry、
	 * @Description:
	 * @return: List<Map>
	 */
	public List<Map> getCustomerOfIndustry(long uid, List<Integer> list);

	/**
	 * @author zhangzhen(J)
	 * @date 2017年1月13日下午5:58:27
	 * @Description: 获取被服务的股票详情列表
	 */
	public List<SharedStockListVo> getServiceStockList(long uid,
	                                                   long researcherId, int showNum, int currentPage, String sort,
	                                                   int sortRule, int stockExpire);

	/**
	 * @param currentPage 当前页
	 * @param showNum     每页结果数
	 * @return
	 * @Title: getServiceStockNumList
	 * @Description:获取被服务的股票数目列表
	 * @return: List<Map>
	 */
	public List<Map> getServiceStockNumList(long uid, int currentPage, int showNum);

	/**
	 * @param list 用户ID列表
	 * @return List<Map> 与用户ID列表对应的职位列表
	 * @Title: getPositions
	 * @Description:获取用户职位信息
	 */
	public List<Map> getPositions(List<Long> list);

	/**
	 * @param currentPage 当前页
	 * @param showNum     每页结果数
	 * @return
	 * @Title: getManagedStockNumList
	 * @Description:获取服务的股票数目列表
	 * @return: List<Map>
	 */
	public List<Map> getManagedStockNumList(long uid, int currentPage, int showNum);

	/**
	 * @param uid       卖方ID
	 * @param customers 买方ID列表
	 * @return
	 * @Title: getCustomersWithNewMsg
	 * @Description:获取与卖方有新消息的买方列表
	 * @return: List<Long>
	 */
	public List<Long> getCustomersWithNewMsg(long uid, List<Long> customers);

	/**
	 * @param authorId
	 * @param innerCode
	 * @param stockLimitNum
	 * @return
	 * @Title: getOverServiceStockNumList、
	 * @Description: 获取超限的被服务的股票数目
	 * @return: List<Map>
	 */
	public List<Map> getOverServiceStockNumList(long authorId, int innerCode, int stockLimitNum, List<Long> list);

	/**
	 * @param industryCode 行业代码
	 * @param poolList     股票池ID
	 * @return
	 * @Title: getOverServiceResearcherList、
	 * @Description:获取超限的合同卖方
	 * @return: List<Long>
	 */
	public List<Long> getOverServiceResearcherList(int industryCode, List<Long> poolList);

	/**
	 * 2017年2月10日上午9:21:48<br>
	 * 获取某一类型的自选股目录
	 *
	 * @param uid
	 * @param dataType 股票目录类型<br>为 -1时查全部类型
	 * @author zhangzhen(J)
	 */
	public List<SelfStockCategoryVo> getSelfStockCategory(long uid,
	                                                      int dataType);

	/**
	 * 2017年2月10日上午10:32:09<br>
	 * 按人员，行业查询 自选股分享目录
	 *
	 * @param shareType 1 机构  2 用户
	 * @author zhangzhen(J)
	 */
	public List<Map<String, Object>> getSelfPoolShareListByType(long uid, int industryCode, int dataType, int shareType, long dm);

	/**
	 * 2017年2月10日上午11:00:10<br>
	 * 获取自选股股票的最大排序值
	 *
	 * @author zhangzhen(J)
	 */
	public Map<String, Object> querySelfStockMaxSort(long poolId);

	/**
	 * 2017年2月14日上午8:59:27<br>
	 * 获取自选股股票的最小排序值
	 *
	 * @author zhangzhen(J)
	 */
	public Map<String, Object> querySelfStockMinSort(long poolId);

	/**
	 * 获取个股明星列表
	 *
	 * @param accountType
	 */
	public List<Map<String, Object>> getStockStarList(int accountType, int innerCode);

	/**
	 * @param uid        用户ID
	 * @param innerCodes 股票内码 以，分割
	 * @return void
	 * @Title: saveAdStocks、
	 * @Description:添加推广股票
	 */
	public void saveAdStocks(long uid, String innerCodes);

	/**
	 * @return List<Integer>
	 * @Title: getContestTopStockList、
	 * @Description:获取港股大赛热门股票内码
	 */
	public List<Integer> getContestTopStockList();

	/**
	 * 根据自选股目录ID获取自选股列表
	 *
	 * @param categoryId
	 * @return
	 */
	public List<SelfStockVo> getStockListByOneCategory(long categoryId, long companyCode);

	/**
	 * 获取自选股标签
	 *
	 * @param uid
	 * @param dataId
	 * @return
	 */
	public String getSelfStockLabel(long uid, long dataId);

	/**
	 * 判断是否在自选股
	 *
	 * @param uid
	 * @param dataId
	 * @return
	 */
	public boolean isInSelfstock(long uid, long dataId);

	/**
	 * 获取自选股列表(用于缓存)
	 *
	 * @param updateTime
	 * @return
	 */
	public List<SelfStockItem> getSelfStockList(String updateTime);

	/**
	 * 获取热度数据描述
	 *
	 * @param innerCode
	 * @return SecuExtInfo
	 */
	public SecuExtInfo getStockOfHeatDesc(int innerCode);

	/**
	 * 获取自选股列表
	 *
	 * @param uid
	 * @return
	 */
	public List<Integer> getSelfStockList(long uid);

	/**
	 * 获取排名最靠前的自选股池Id
	 *
	 * @param uid
	 * @return
	 */
	public SelfStockCategoryVo getSelfStockCategoryBySort(long uid, int dataType);

	/**
	 * 检验股票是否存在自选股中
	 *
	 * @param checkMap
	 * @return 0 不存在, 1 存在
	 */
	public int checkStockIsExistByCondition(Map<String, Object> checkMap);

	/**
	 * 获取共享 股票池股票及行情
	 *
	 * @param params
	 * @return
	 */
	public List<SelfStockVo> getSelfStockShare(Map<String, Object> params);

	/**
	 * 获取自选股单个股票共享人数
	 *
	 * @param paramsCount
	 */
	public int getSelfStockSharePeopleCount(Map<String, Object> paramsCount);

	/**
	 * 获取共享 股票池变化及行情
	 *
	 * @param params
	 * @return
	 */
	public List<SelfStockVo> getSelfPoolShareRecord(Map<String, Object> params);

	/**
	 * 获取共享股票池股票调入情况
	 *
	 * @param params
	 * @return
	 */
	public List<Map<String, Object>> getSelfPoolShareSecuRecord(Map<String, Object> params);

	/**
	 * 获取分享组集合
	 *
	 * @param vo
	 * @return
	 */
	public List<Long> getGroupIds(SelfStockVo vo);

	/**
	 * 获取分享人Ids
	 *
	 * @param params
	 */
	public List<Long> getShareUids(Map<String, Object> params);

	/**
	 * 根据主键获取股票池信息
	 *
	 * @param categoryId
	 * @return
	 */
	public SelfStockVo selectPoolByPrimaryKey(long categoryId);

	/**
	 * 批量添加自选股调入调出日志
	 */
	public void insertStockShareStatus(List<SelfStockVo> list);

	/**
	 * 记录自选股调入调出日志 （@version :pc3.0  @2019年2月28日11:18:06  @author Xueyc ）
	 *
	 * @param data
	 * @return
	 */
	public List<SelfStockVo> findselfStock(SelfStockVo data);

	/**
	 * 获取共享池股票内码集合
	 *
	 * @param map
	 * @return
	 */
	public List<Integer> getSelfShareInnerCodesByIndustries(Map<String, Object> map);

	/**
	 * 获取自选股分享人
	 *
	 * @param params
	 * @return
	 */
	public List<Map<String, Object>> getSelfSharePeople(Map<String, Object> params);

	/**
	 * 删除自选股分享
	 *
	 * @param viewsIds
	 * @param groupIds
	 * @param shareIds
	 */
	public void deleteSelfShareAction(Set<Long> viewsIds, Set<Long> groupIds, Set<Long> shareIds);

	/**
	 * 获取自选股分享关联表信息
	 *
	 * @param params
	 * @return
	 */
	public List<Map<String, Long>> getSelfShareMsg(Map<String, Object> params);

	/**
	 * 删除自选股分享(分享对象为机构，再由机构分配到个人)
	 *
	 * @param params
	 */
	public void deleteSelfShareActionByOrg(Map<String, Object> params);

	/**
	 * 从自选股中获取共享股票池股票调入情况
	 *
	 * @param params
	 * @return
	 */
	public List<Map<String, Object>> getSelfPoolShareSecuRecordFromStock(Map<String, Object> params);

	/**
	 * 个股详情——调仓统计
	 *
	 * @param innerCode
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Map<String, Object> getStockOrderStatistical(int innerCode, String startDate, String endDate);

	/**
	 * 调仓趋势
	 *
	 * @param innerCode
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<Map<String, Object>> getSecuOrderTrend(int innerCode, String startDate, String endDate);

	/**
	 * 调仓净买入趋势
	 *
	 * @param innerCode
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<Map<String, Object>> getSecuOrderNetTrend(int innerCode, String startDate, String endDate);

	/**
	 * 调仓对比
	 *
	 * @param innerCode
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Map<String, Integer> getSecuOrderContrast(int innerCode, String startDate, String endDate);
}
