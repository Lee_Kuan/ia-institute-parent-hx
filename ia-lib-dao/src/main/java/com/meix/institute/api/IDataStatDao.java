package com.meix.institute.api;


import com.meix.institute.search.ClueSearchVo;
import com.meix.institute.search.DataRecordStatSearchVo;
import com.meix.institute.search.UserRecordSearchVo;
import com.meix.institute.vo.company.CompanyClueInfo;
import com.meix.institute.vo.company.CompanyClueUserStat;
import com.meix.institute.vo.stat.DataRecordStatVo;
import com.meix.institute.vo.stat.HeatDataVo;
import com.meix.institute.vo.user.UserClueStat;
import com.meix.institute.vo.user.UserRecordStatVo;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by zenghao on 2019/8/6.
 */
public interface IDataStatDao {

	/**
	 * 获取所有研究所活动列表，用于统计阅读、分享
	 *
	 * @return
	 */
	List<DataRecordStatVo> getAllInsActivityList();

	/**
	 * 获取研究所研报id列表
	 *
	 * @return
	 */
	List<Long> getInsReportIdList(String startDate, String endDate);

	DataRecordStatVo getInsReportByRid(long rid);

	long getDataReadStat(long dataId, int dataType);

	long getReadDuration(long dataId, int dataType);

	long getDataShareStat(long dataId, int dataType, int activityType);

	List<UserRecordStatVo> getUserRecordList(long dataId, long dataType, int activityType);

	/**
	 * 获取15天内用户订阅分析师的行为列表
	 */
	List<CompanyClueInfo> getSubscribeAnalystRecordList(long companyCode);

	/**
	 * 30天内参加活动超过5次的机构列表
	 */
	List<CompanyClueInfo> getJoinActivityRecordList(long companyCode);

	/**
	 * 30天内参加活动超过5次的机构的用户记录列表
	 */
	List<CompanyClueUserStat> getJoinActivityRecordDetailList(long companyCode, long customerCompanyCode);

	/**
	 * 15天内收听电话会议超过3次的机构列表
	 */
	List<CompanyClueInfo> getListenTeleActivityList();

	/**
	 * 15天内收听电话会议超过3次的机构的用户记录列表
	 */
	List<CompanyClueUserStat> getListenTeleActivityDetailList(long companyCode, long customerCompanyCode);

	/**
	 * 15天内查看研报超过5次的机构列表
	 */
	List<CompanyClueInfo> getReadResearchReportList();

	/**
	 * 15天内查看研报超过5次的机构的用户记录列表
	 */
	List<CompanyClueUserStat> getReadReportDetailList(long companyCode, long customerCompanyCode);

	int saveClue(CompanyClueInfo companyClueInfo);

	int updateClue(CompanyClueInfo companyClueInfo);

	CompanyClueInfo getExistClueByUuid(String uuid);

	CompanyClueInfo getExistClue(long companyCode, long customerCompanyCode, int clueType, String latestRecordTime);

	int removeClueUserStat(String clueUuid);

	List<CompanyClueUserStat> getClueUserStatList(String clueUuid);

	int insertClueUserStat(CompanyClueUserStat companyClueUserStat);

	int insertClueUserStatList(List<CompanyClueUserStat> list);

	int removeClueUserStatRange(long startId, long endId);

	int saveDataRecordStat(DataRecordStatVo dataRecordStatVo);

	int updateDataRecordStat(DataRecordStatVo dataRecordStatVo);

	DataRecordStatVo getExistDataRecordStat(long companyCode, long dataId, int dataType);

	DataRecordStatVo getExistDataRecordStatById(long id);

	List<DataRecordStatVo> getDataRecordStatList(DataRecordStatSearchVo statSearchVo);

	long getDataRecordStatCount(DataRecordStatSearchVo statSearchVo);

	int saveUserRecordStat(UserRecordStatVo userRecordStatVo);

	int updateUserRecordStat(UserRecordStatVo userRecordStatVo);

	int removeUserRecordStat(int dataType, long dataId, Set<Long> uidSet);

	UserRecordStatVo getExistUserRecordStat(long userId, long dataId, int dataType);

	UserRecordStatVo getExistUserRecordStatById(long id);

	List<UserRecordStatVo> getUserRecordStatList(UserRecordSearchVo searchVo);

	long getUserRecordStatCount(UserRecordSearchVo searchVo);

	List<CompanyClueInfo> getClueList(ClueSearchVo searchVo);

	long getClueCount(ClueSearchVo searchVo);

	List<UserClueStat> getUserClueStatList(ClueSearchVo searchVo);

	long getUserClueStatCount(ClueSearchVo searchVo);

	int removeHeatDataRange(long startId, long endId);

	void insertHeatDataList(List<HeatDataVo> list);

	/**
	 * 热度
	 *
	 * @param search
	 * @return
	 */
	List<Map<String, Object>> getHeatData(Map<String, Object> search);

	/**
	 * 热度数量
	 *
	 * @param search
	 * @return
	 */
	Map<String, Object> getHeatDataCount(Map<String, Object> search);

	/**
	 * 统计Comps行业下的股票
	 *
	 * @param dataCode comps行业
	 * @param type     时间类型
	 * @return
	 */
	List<Map<String, Object>> statisticalInnerCodeByCompsIndustry(String dataCode, int type);

	List<Map<String, Object>> getUserReadData(Map<String, Object> search);

	Map<String, Object> getUserReadDataCount(Map<String, Object> search);

	List<Map<String, Object>> getUserReadDataByUser(Map<String, Object> search);

	Map<String, Object> getUserReadDataByUserCount(Map<String, Object> search);
}
