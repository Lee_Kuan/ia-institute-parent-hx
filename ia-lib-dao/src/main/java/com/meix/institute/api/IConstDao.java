package com.meix.institute.api;

import com.meix.institute.entity.SecuMain;
import com.meix.institute.vo.IaConstantVo;
import com.meix.institute.vo.cache.ConstantVo;

import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("rawtypes")
public interface IConstDao {

	int insertConstantList(List<IaConstantVo> list);

	int removeConstantRange(long startId, long endId);

	/**
	 * @param params
	 * @return
	 * @Title: getStockMap、
	 * @Description:获取股票字典
	 * @return: List<SecuMain>
	 */
	public List<SecuMain> getStockMap(Map params);

	/**
	 * @return
	 * @Title: defaultInnerCode、
	 * @Description:获取全库唯一ID
	 * @return: List<SecuMain>
	 */
	public Long defaultInnerCode();

	/**
	 * @return
	 * @Title: getCompanyTypeList、
	 * @Description:获取公司类型数组
	 * @return: List<Map>
	 */
	public List<Map> getCompanyTypeList();

	/**
	 * @param innerCode
	 * @return
	 * @Title: getIndustryLabel、
	 * @Description:获取行业标签
	 * @return: Map
	 */
	public Map getIndustryLabel(int innerCode);

	/**
	 * @return
	 * @Title: getIndustryList、
	 * @Description:获取行业列表
	 * @return: List<Map>
	 */
	public List<Map> getIndustryList();

	Map<String,Object> getIndustryInfo(String industryName);

	/**
	 * @return
	 * @Title: getIndustryLabelList、
	 * @Description:获取行业标签列表
	 * @return: List<ConstantVo>
	 */
	public List<ConstantVo> getIndustryLabelList();

	/**
	 * 根据键值找到自定义查询语句的条件
	 *
	 * @param key
	 * @return
	 */
	public String getCustomQueryCondition(String key);

	/**
	 * 获取汇率
	 *
	 * @param currencyType
	 * @return
	 */
	public float getExchange(String currencyType);

	/**
	 * 通过股票名称获取内码
	 *
	 * @param keySet
	 * @return
	 */
	public List<Map<String, Object>> selectSecuByName(Set<String> keySet);

	/**
	 * ia_constant find 通用方法
	 *
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> find(Map<String, Object> map);

	public Map<String, Object> getIndustryInfoByCode(String industryCode);

}
