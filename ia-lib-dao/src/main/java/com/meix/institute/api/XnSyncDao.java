package com.meix.institute.api;

import java.util.List;
import java.util.Map;

import com.meix.institute.pojo.xn.XnDirection;
import com.meix.institute.pojo.xn.XnReportVo;
import com.meix.institute.pojo.xn.XnStationInfo;
import com.meix.institute.pojo.xn.XnUserVo;

public interface XnSyncDao {
    
    /**
     * 统计内部用户数量
     * 
     * @param updateTimeFm 更新时间-起始
     * @param updateTimeTo 更新时间-结束
     * @param isSyncAll 是否同步全量
     * @return count
     */
    int countInnerUser(String updateTimeFm, String updateTimeTo, Boolean isSyncAll);
    
    /**
     * 获取内部用户列表
     * 
     * @param updateTimeFm 更新时间-起始
     * @param updateTimeTo 更新时间-结束
     * @param page 起始页
     * @param pageSize 分页大小
     * @return List<XnUserVo>
     */
    List<XnUserVo> getInnerUser(String updateTimeFm, String updateTimeTo, Boolean isSyncAll, int page, int pageSize);
    
    /**
     * 获取职位列表
     * 
     * @param page 起始页
     * @param pageSize 分页大小
     * @return List<BgStation>
     */
    List<XnStationInfo> getStation(int page, int pageSize);
    
    /**
     * 获取分析师方向
     * 
     * @return List<Direction>
     */
    List<XnDirection> getDirection();
    
    /**
     * 获取研报列表
     * 
     * @param updateTimeFm 更新时间-起始
     * @param updateTimeTo 更新时间-结束
     * @param page 起始页
     * @param pageSize 分页大小
     * @return List<XnReportVo>
     */
	List<XnReportVo> getBgReportList(String updateTimeFm, String updateTimeTo, Boolean isSyncAll, int page, int pageSize);
	
	/**
	 * 统计职位数量
	 * 
	 * @return
	 */
    int countStation();
    
    /**
     * 统计研报数量
     * @param updateTimeFm
     * @param updateTimeTo
     * @return
     */
    int countBgReport(String updateTimeFm, String updateTimeTo, Boolean isSyncAll);
    
	public Map<String, Object> getReportFile(String fileId);

}
