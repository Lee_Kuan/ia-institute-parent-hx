package com.meix.institute.api;

import java.util.List;

import com.meix.institute.vo.company.CompanyWechatConfig;
import com.meix.institute.vo.company.CompanyWechatTemplete;
import com.meix.institute.vo.user.InsCustomer;
import com.meix.institute.vo.user.InsMeixUser;

/**
 * Created by zenghao on 2019/9/4.
 */
public interface IInstituteDao {

	int saveInsMeixUser(InsMeixUser insMeixUser);

	int updateInsMeixUser(InsMeixUser insMeixUser);

	InsMeixUser getExistInsMeixUser(long companyCode, long customerCompanyCode);

	/**
	 * 获取微信公众号配置信息
	 *
	 * @return
	 */
	public List<CompanyWechatConfig> getWechatConfig();

	CompanyWechatConfig getWechatConfigByAppType(String appType);

	CompanyWechatConfig getWechatConfigByAppId(String appId);

	CompanyWechatConfig getWechatConfigByCompanyCode(long companyCode);

	/**
	 * 获取小程序配置
	 *
	 * @param companyCode
	 * @return
	 */
	CompanyWechatConfig getSPConfigByCompanyCode(long companyCode);

	/**
	 * 获取微信模板信息
	 *
	 * @param companyCode
	 * @param msgType
	 * @return
	 */
	CompanyWechatTemplete getCompanyWechatTemplete(long companyCode, int msgType);

	public List<InsCustomer> getCustomersBySaler(long salerUid, int currentPage, int showNum);

	public int getCustomerCountBySaler(long salerUid);

    CompanyWechatConfig getWechatConfigByAppTypeAndClientType(String appType, Integer clientType);

}
