package com.meix.institute.pojo.xn;

public class XnDirection {
    
    private String oid;
    private String objname;
    private String col1;
    
    public String getOid() {
        return oid;
    }
    public void setOid(String oid) {
        this.oid = oid;
    }
    public String getObjname() {
        return objname;
    }
    public void setObjname(String objname) {
        this.objname = objname;
    }
    public String getCol1() {
        return col1;
    }
    public void setCol1(String col1) {
        this.col1 = col1;
    }
}
