package com.meix.institute.pojo.xn;

import lombok.Data;

@Data
public class XnUserVo {
    
    /**
     * 标识
     */
    private String orgid;
    /**
     * 姓名
     */
    private String orgname;
    
    /**
     * oa账号
     */
    private String username;
    
    /**
     * 职位名称
     */
    private String job;
    /**
     * 手机号
     */
    private String mobilephone;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 部门id
     */
    private String unitid;
    /**
     * 部门名称（分析师方向）
     */
    private String unname;
    /**
     * 状态（在职、离职）
     */
    private String inservice;
    /**
     * 简介
     */
    private String introduction;
    /**
     * 更新时间
     */
    private String updatetime;
    
    /**
     * 研究方向标识
     */
    private String industrycode;
    
    /**
     * 研究方向
     */
    private String industryname;
    
    /**
     * 分析师编号
     */
    private String qualifyno;
    
    /**
     * 岗位名称
     */
    private String positionname;
}
