package com.meix.institute.pojo;

public class InsSyncSndReqS {

	private Long id;
	private String bizKey;
	private String method;
	private String state;
	private String createdAtFm;
	private String createdAtTo;
	private String updatedAtFm;
	private String updatedAtTo;
	private int showNum;
	private int currentPage;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBizKey() {
		return bizKey;
	}
	public void setBizKey(String bizKey) {
		this.bizKey = bizKey;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCreatedAtFm() {
		return createdAtFm;
	}
	public void setCreatedAtFm(String createdAtFm) {
		this.createdAtFm = createdAtFm;
	}
	public String getCreatedAtTo() {
		return createdAtTo;
	}
	public void setCreatedAtTo(String createdAtTo) {
		this.createdAtTo = createdAtTo;
	}
	public String getUpdatedAtFm() {
		return updatedAtFm;
	}
	public void setUpdatedAtFm(String updatedAtFm) {
		this.updatedAtFm = updatedAtFm;
	}
	public String getUpdatedAtTo() {
		return updatedAtTo;
	}
	public void setUpdatedAtTo(String updatedAtTo) {
		this.updatedAtTo = updatedAtTo;
	}
	public int getShowNum() {
		return showNum;
	}
	public void setShowNum(int showNum) {
		this.showNum = showNum;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
    
}
