package com.meix.institute.pojo.xn;

public class XnStationInfo {
    
    private String id;
    private String objname;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getObjname() {
        return objname;
    }
    public void setObjname(String objname) {
        this.objname = objname;
    }
}
