package com.meix.institute.pojo;

import java.util.Date;

import com.meix.institute.entity.InsSecToken;

public class InsSecTokenS extends InsSecToken{
    
    private Date expiresAtFm; // 过期时间——起始

    public Date getExpiresAtFm() {
        return expiresAtFm;
    }
    public void setExpiresAtFm(Date expiresAtFm) {
        this.expiresAtFm = expiresAtFm;
    }
    
}
