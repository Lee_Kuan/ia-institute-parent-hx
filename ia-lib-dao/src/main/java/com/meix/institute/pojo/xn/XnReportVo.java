package com.meix.institute.pojo.xn;

import lombok.Data;

import java.io.Serializable;

@Data
public class XnReportVo implements Serializable {

	private static final long serialVersionUID = 7456835368518400742L;
	
	private String objid;
	private String title;
	/**
	 * 作者，员工唯一标识，多个以“,”分割
	 */
	private String authorid;
	private String columnid;
	private String columnname;//一级分类
	private String name; //二级分类
	private String stkrank;//评级
	private String scode; //个股（secucode）
	private String scodename;
	private String indcode;//行业（携宁代码）
	private String indname;//行业名
	private String archivetime;//发布时间
	private String summary;//摘要
	private String updateTime;
	
}
