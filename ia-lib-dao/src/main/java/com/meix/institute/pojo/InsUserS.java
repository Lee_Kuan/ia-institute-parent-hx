package com.meix.institute.pojo;

import com.meix.institute.search.BaseSearchVo;

public class InsUserS extends BaseSearchVo{
    
    private static final long serialVersionUID = 1L;
    
    private long id;
    private String mobileOrEmail;
    private String position;
    private Integer userState;
    private Integer accountType;
    
    public Integer getAccountType() {
        return accountType;
    }
    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }
    public Integer getUserState() {
        return userState;
    }
    public void setUserState(Integer userState) {
        this.userState = userState;
    }
    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getMobileOrEmail() {
        return mobileOrEmail;
    }
    public void setMobileOrEmail(String mobileOrEmail) {
        this.mobileOrEmail = mobileOrEmail;
    }
}
