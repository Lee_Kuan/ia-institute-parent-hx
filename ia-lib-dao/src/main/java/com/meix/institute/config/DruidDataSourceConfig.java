package com.meix.institute.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.meix.institute.config.bo.SecretProperty;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import tk.mybatis.spring.annotation.MapperScan;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.sql.SQLException;

/**
 * Created by zenghao on 2018/11/22.
 */
@MapperScan("com.meix.institute.mapper")
@Configuration
public class DruidDataSourceConfig {

	//private Logger logger = LoggerFactory.getLogger(DruidDataSourceConfig.class);

	@Autowired
	private SecretProperty secretProperty;

	/**
	 * mysql
	 *
	 * @return
	 */
	@Bean    //声明其为Bean实例
	@Primary  //在同样的DataSource中，首先使用被标注的DataSource
	public DataSource dataSource() {
		DruidDataSource datasource = new DruidDataSource();

		datasource.setUrl(secretProperty.getStringValue("jdbchx", "url"));
		datasource.setUsername(secretProperty.getStringValue("jdbchx", "username"));
		datasource.setPassword(secretProperty.getStringValue("jdbchx", "password"));
		datasource.setDriverClassName(secretProperty.getStringValue("jdbchx", "driverClassName"));
		datasource.setMaxActive(secretProperty.getIntValue("jdbchx", "maxPoolSize"));

		datasource.setTimeBetweenEvictionRunsMillis(60000);//每60秒检查所有连接池中的空闲连接
		datasource.setRemoveAbandoned(true);
		datasource.setLogAbandoned(true);

		//测试连接有效性
		datasource.setTestWhileIdle(true);

		/*datasource.setInitialSize(secretProperty.getIntValue("spring.datasource.initialSize"));
		datasource.setMinIdle(secretProperty.getIntValue("spring.datasource.minIdle"));
		// 配置获取连接等待超时的时间
		datasource.setMaxWait(secretProperty.getIntValue("spring.datasource.maxWait"));
		// 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
		datasource.setTimeBetweenEvictionRunsMillis(secretProperty.getIntValue("spring.datasource.timeBetweenEvictionRunsMillis"));
		// 配置一个连接在池中最小生存的时间，单位是毫秒
		datasource.setMinEvictableIdleTimeMillis(secretProperty.getIntValue("spring.datasource.minEvictableIdleTimeMillis"));
		datasource.setValidationQuery(secretProperty.getStringValue("spring.datasource.validationQuery"));
		datasource.setTestWhileIdle(secretProperty.getBooleanValue("spring.datasource.testWhileIdle"));
		datasource.setTestOnBorrow(secretProperty.getBooleanValue("spring.datasource.testOnBorrow"));
		datasource.setTestOnReturn(secretProperty.getBooleanValue("spring.datasource.testOnReturn"));
		datasource.setPoolPreparedStatements(secretProperty.getBooleanValue("spring.datasource.poolPreparedStatements"));
		datasource.setMaxPoolPreparedStatementPerConnectionSize(secretProperty.getIntValue("spring.datasource.maxPoolPreparedStatementPerConnectionSize"));
		datasource.setUseGlobalDataSourceStat(secretProperty.getBooleanValue("spring.datasource.filters"));
		try {
			datasource.setFilters(secretProperty.getStringValue("spring.datasource.connectionProperties"));
		} catch (SQLException e) {
			logger.error("druid configuration initialization filter", e);
		}
		datasource.setConnectionProperties(secretProperty.getStringValue("spring.datasource.useGlobalDataSourceStat"));*/

		return datasource;
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	@Primary
	public SqlSessionTemplate sqlSessionTemplate() throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory());
	}

	/**
	 * @return
	 * @throws Exception
	 * @Title: sqlSessionFactory、
	 * @Description:数据库连接工厂
	 * @return: SqlSessionFactory
	 */
	@Bean
	@Primary
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setMapperLocations(resolver.getResources("classpath*:mapper/*/*.xml"));
		return sessionFactory.getObject();
	}

	/**
	 * @return
	 * @throws PropertyVetoException
	 * @Title: transactionManager、
	 * @Description:数据库事务管理
	 * @return: DataSourceTransactionManager
	 */
	@Bean
	public DataSourceTransactionManager transactionManager() throws PropertyVetoException, SQLException {
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
		transactionManager.setDataSource(dataSource());
		return transactionManager;
	}

}
