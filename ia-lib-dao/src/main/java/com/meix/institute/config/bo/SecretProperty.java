package com.meix.institute.config.bo;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.yaml.snakeyaml.Yaml;

@SuppressWarnings("rawtypes")
@Component
public class SecretProperty {
	@Value("${accountFilePath}")
	private String accountFilePath;
	private Map accountFile;

	/**
	 * @return
	 * @Title: getAccountFile、
	 * @Description:获取账号配置文件
	 * @return: Map
	 */
	private Map getAccountFile() {
		if (accountFile != null) {
			return accountFile;
		}
		//加载账户配置文件
		File file;
		try {
			file = ResourceUtils.getFile(accountFilePath);
//			String jsonStr=FileUtils.readFileToString(file,"UTF-8");
			Yaml yaml = new Yaml();
			accountFile = yaml.loadAs(new FileInputStream(file), Map.class);
			return accountFile;
		} catch (Exception e) {
			throw new RuntimeException("加载账号配置文件失败！", e);
		}
	}

	/**
	 * @param attr
	 * @return
	 * @Title: getStringValue、
	 * @Description:获取字符串
	 * @return: String
	 */
	public String getStringValue(String attr) {
		String[] properties = attr.split("[.]");
		Map map = getAccountFile();
		for (int index = 0; index < properties.length - 1; index++) {
			Map curMap = (Map) map.get(properties[index]);
			if (curMap == null) {
				return null;
			}
			map = curMap;
		}
		return MapUtils.getString(map, properties[properties.length - 1]);
	}

	public int getIntValue(String attr) {
		String[] properties = attr.split("[.]");
		Map map = getAccountFile();
		for (int index = 0; index < properties.length - 1; index++) {
			Map curMap = (Map) map.get(properties[index]);
			if (curMap == null) {
				return 0;
			}
			map = curMap;
		}
		return MapUtils.getIntValue(map, properties[properties.length - 1]);
	}

	public boolean getBooleanValue(String attr) {
		String[] properties = attr.split("[.]");
		Map map = getAccountFile();
		for (int index = 0; index < properties.length - 1; index++) {
			Map curMap = (Map) map.get(properties[index]);
			if (curMap == null) {
				return false;
			}
			map = curMap;
		}
		return MapUtils.getBooleanValue(map, properties[properties.length - 1]);
	}

	/**
	 * @param prefix
	 * @param attr
	 * @return
	 * @Title: getStringValue、
	 * @Description:根据前缀获取属性值
	 * @return: String
	 */
	public String getStringValue(String prefix, String attr) {
		return MapUtils.getString((Map) getAccountFile().get(prefix), attr);
	}

	public int getIntValue(String prefix, String attr) {
		return MapUtils.getIntValue((Map) getAccountFile().get(prefix), attr);
	}

}
