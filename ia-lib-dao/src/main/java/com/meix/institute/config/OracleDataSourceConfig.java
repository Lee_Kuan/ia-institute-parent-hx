package com.meix.institute.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.meix.institute.config.bo.SecretProperty;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import tk.mybatis.spring.annotation.MapperScan;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

@MapperScan("com.meix.institute.oraclemapper")
@Configuration
public class OracleDataSourceConfig {
	@Autowired
	private SecretProperty secretProperty;

	@Bean(name = "oracleDataSource")
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	public DataSource oracleDataSource() throws PropertyVetoException {
		DruidDataSource ds = new DruidDataSource();
		ds.setDriverClassName(secretProperty.getStringValue("ojdbc", "driverClassName"));
		ds.setUrl(secretProperty.getStringValue("ojdbc", "url"));
		ds.setUsername(secretProperty.getStringValue("ojdbc", "username"));
		ds.setPassword(secretProperty.getStringValue("ojdbc", "password"));
		ds.setMaxActive(secretProperty.getIntValue("ojdbc", "maxPoolSize"));
		ds.setTimeBetweenEvictionRunsMillis(60000);//每60秒检查所有连接池中的空闲连接
		ds.setRemoveAbandoned(true);
		ds.setLogAbandoned(true);
		ds.setTestWhileIdle(true);

		return ds;
	}

	@Bean(name = "oracleSqlSessionTemplate")
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	public SqlSessionTemplate oracleSqlSessionTemplate() throws Exception {
		return new SqlSessionTemplate(oracleSqlSessionFactory());
	}

	/**
	 * @return
	 * @throws Exception
	 * @Title: sqlSessionFactory、
	 * @Description:数据库连接工厂
	 * @return: SqlSessionFactory
	 */
	@Bean(name = "oracleSqlSessionFactory")
	public SqlSessionFactory oracleSqlSessionFactory() throws Exception {
		final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sessionFactory.setDataSource(oracleDataSource());
		sessionFactory.setMapperLocations(resolver.getResources("classpath*:mapper/xml/xn.xml"));
		return sessionFactory.getObject();
	}
}
