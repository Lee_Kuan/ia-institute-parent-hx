package com.meix.institute.config.bo;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 判断是否为服务器环境
 * Created by zenghao on 2018/11/22.
 */
public class ServerEnvCondition implements Condition {
	@Override
	public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
		Environment environment = conditionContext.getEnvironment();
		String profiles = environment.getProperty("spring.profiles.active");
		if (profiles.contains("local")) {
			return false;
		} else {
			return true;
		}
	}
}
