package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_company_wechat_access_token")
public class WechatAccessToken {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 公众号所属机构
     */
    @Column(name = "COMPANY_CODE")
    private Long companyCode;
    
    /**
     * 公众号appId
     */
    @Column(name = "APP_ID")
    private String appId;

    /**
     * 类型 1-AccessToken 2-JsApiTicket
     */
    @Column(name = "TYPE")
    private Integer type;

    /**
     * 微信token
     */
    @Column(name = "ACCESS_TOKEN")
    private String accessToken;

    /**
     * 过期时间(秒)
     */
    @Column(name = "EXPIRES_IN")
    private Long expiresIn;

    @Column(name = "CREATE_TIME")
    private Date createTime;

    /**
     * TOKEN更新时间
     */
    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取公众号所属机构
     *
     * @return COMPANY_CODE - 公众号所属机构
     */
    public Long getCompanyCode() {
        return companyCode;
    }

    /**
     * 设置公众号所属机构
     *
     * @param companyCode 公众号所属机构
     */
    public void setCompanyCode(Long companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 获取类型 1-AccessToken 2-JsApiTicket
     *
     * @return TYPE - 类型 1-AccessToken 2-JsApiTicket
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置类型 1-AccessToken 2-JsApiTicket
     *
     * @param type 类型 1-AccessToken 2-JsApiTicket
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取微信token
     *
     * @return ACCESS_TOKEN - 微信token
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * 设置微信token
     *
     * @param accessToken 微信token
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken == null ? null : accessToken.trim();
    }

    /**
     * 获取过期时间(秒)
     *
     * @return EXPIRES_IN - 过期时间(秒)
     */
    public Long getExpiresIn() {
        return expiresIn;
    }

    /**
     * 设置过期时间(秒)
     *
     * @param expiresIn 过期时间(秒)
     */
    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     * @return CREATE_TIME
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取TOKEN更新时间
     *
     * @return UPDATE_TIME - TOKEN更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置TOKEN更新时间
     *
     * @param updateTime TOKEN更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
    
}