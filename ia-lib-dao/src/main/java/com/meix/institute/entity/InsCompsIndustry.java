package com.meix.institute.entity;

import java.util.Date;

public class InsCompsIndustry {
    
    private Long id;
    private String LabelId;
    private String labelName;
    private Date createTime;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getLabelId() {
        return LabelId;
    }
    public void setLabelId(String labelId) {
        LabelId = labelId;
    }
    public String getLabelName() {
        return labelName;
    }
    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
