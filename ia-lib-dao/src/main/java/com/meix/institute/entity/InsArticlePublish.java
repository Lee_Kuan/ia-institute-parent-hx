package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_article_publish")
public class InsArticlePublish {
    /**
     * 流水号
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，视频为video，卡券为wxcard
     */
    @Column(name = "TYPE")
    private String type;

    /**
     * 素材标志： type=text 时为空，type=mynews 时关联 ia_article_group 中的media_id， 其它的关联ia_article_other 中的media_id
     */
    @Column(name = "MEDIA_ID")
    private String mediaId;

    /**
     * 微信号
     */
    @Column(name = "TO_WX_NAME")
    private String toWxName;

    /**
     * 接收消息用户对应该公众号的openid
     */
    @Column(name = "TOUSER")
    private String touser;

    /**
     * 状态： -1 删除 0 未发布 1 预约发布 2 已发布
     */
    @Column(name = "STATE")
    private Integer state;

    /**
     * 发布时间
     */
    @Column(name = "PUBLISH_TIME")
    private Date publishTime;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /**
     * 群发文本： type = text 时存在
     */
    @Column(name = "CONTENT")
    private String content;

    /**
     * 获取流水号
     *
     * @return ID - 流水号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置流水号
     *
     * @param id 流水号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，视频为video，卡券为wxcard
     *
     * @return TYPE - 群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，视频为video，卡券为wxcard
     */
    public String getType() {
        return type;
    }

    /**
     * 设置群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，视频为video，卡券为wxcard
     *
     * @param type 群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，视频为video，卡券为wxcard
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取素材标志： type=text 时为空，type=mynews 时关联 ia_article_group 中的media_id， 其它的关联ia_article_other 中的media_id
     *
     * @return MEDIA_ID - 素材标志： type=text 时为空，type=mynews 时关联 ia_article_group 中的media_id， 其它的关联ia_article_other 中的media_id
     */
    public String getMediaId() {
        return mediaId;
    }

    /**
     * 设置素材标志： type=text 时为空，type=mynews 时关联 ia_article_group 中的media_id， 其它的关联ia_article_other 中的media_id
     *
     * @param mediaId 素材标志： type=text 时为空，type=mynews 时关联 ia_article_group 中的media_id， 其它的关联ia_article_other 中的media_id
     */
    public void setMediaId(String mediaId) {
        this.mediaId = mediaId == null ? null : mediaId.trim();
    }

    /**
     * 获取微信号
     *
     * @return TO_WX_NAME - 微信号
     */
    public String getToWxName() {
        return toWxName;
    }

    /**
     * 设置微信号
     *
     * @param toWxName 微信号
     */
    public void setToWxName(String toWxName) {
        this.toWxName = toWxName == null ? null : toWxName.trim();
    }

    /**
     * 获取接收消息用户对应该公众号的openid
     *
     * @return TOUSER - 接收消息用户对应该公众号的openid
     */
    public String getTouser() {
        return touser;
    }

    /**
     * 设置接收消息用户对应该公众号的openid
     *
     * @param touser 接收消息用户对应该公众号的openid
     */
    public void setTouser(String touser) {
        this.touser = touser == null ? null : touser.trim();
    }

    /**
     * 获取状态： -1 删除 0 未发布 1 预约发布 2 已发布
     *
     * @return STATE - 状态： -1 删除 0 未发布 1 预约发布 2 已发布
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态： -1 删除 0 未发布 1 预约发布 2 已发布
     *
     * @param state 状态： -1 删除 0 未发布 1 预约发布 2 已发布
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取发布时间
     *
     * @return PUBLISH_TIME - 发布时间
     */
    public Date getPublishTime() {
        return publishTime;
    }

    /**
     * 设置发布时间
     *
     * @param publishTime 发布时间
     */
    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }

    /**
     * 获取群发文本： type = text 时存在
     *
     * @return CONTENT - 群发文本： type = text 时存在
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置群发文本： type = text 时存在
     *
     * @param content 群发文本： type = text 时存在
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}