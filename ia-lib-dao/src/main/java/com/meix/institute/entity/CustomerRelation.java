package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_customer_relation")
public class CustomerRelation {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 研究所公司代码
     */
    @Column(name = "COMPANY_CODE")
    private Long companyCode;

    /**
     * uuid
     */
    @Column(name = "UUID")
    private String uuid;

    /**
     * 用户手机号
     */
    @Column(name = "MOBILE")
    private String mobile;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取研究所公司代码
     *
     * @return COMPANY_CODE - 研究所公司代码
     */
    public Long getCompanyCode() {
        return companyCode;
    }

    /**
     * 设置研究所公司代码
     *
     * @param companyCode 研究所公司代码
     */
    public void setCompanyCode(Long companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 获取uuid
     *
     * @return UUID - uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * 设置uuid
     *
     * @param uuid uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取用户手机号
     *
     * @return MOBILE - 用户手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置用户手机号
     *
     * @param mobile 用户手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}