package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_morning_meeting")
public class MorningMeeting {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 机构代码
     */
    @Column(name = "COMPANY_CODE")
    private Long companyCode;

    /**
     * 标题
     */
    @Column(name = "TITLE")
    private String title;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "UPDATED_BY")
    private String updatedBy;

    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 操作人id
     */
    @Column(name = "OPERATOR_UID")
    private String operatorUid;

    /**
     * 操作人姓名
     */
    @Column(name = "OPERATOR_NAME")
    private String operatorName;

    /**
     * 撤销原因
     */
    @Column(name = "UNDO_REASON")
    private String undoReason;

    /**
     * 拒绝原因
     */
    @Column(name = "REFUSE_REASON")
    private String refuseReason;

    /**
     * 状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
     */
    @Column(name = "STATUS")
    private Integer status;

    /**
     * url
     */
    @Column(name = "URL")
    private String url;

    /**
     * 发布日期
     */
    @Column(name = "PUBLISH_DATE")
    private Date publishDate;

    /**
     * 内容
     */
    @Column(name = "CONTENT")
    private String content;

    @Column(name = "TYPE")
    private Integer type;
    @Column(name = "SHARE")
    private Integer share;
    @Column(name = "MEETING_TYPE")
    private Integer meetingType;
    
    public Integer getMeetingType() {
		return meetingType;
	}

	public void setMeetingType(Integer meetingType) {
		this.meetingType = meetingType;
	}

	/**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取机构代码
     *
     * @return COMPANY_CODE - 机构代码
     */
    public Long getCompanyCode() {
        return companyCode;
    }

    /**
     * 设置机构代码
     *
     * @param companyCode 机构代码
     */
    public void setCompanyCode(Long companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 获取标题
     *
     * @return TITLE - 标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * @return CREATED_BY
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * @return CREATED_AT
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return UPDATED_BY
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }

    /**
     * @return UPDATED_AT
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取操作人id
     *
     * @return OPERATOR_UID - 操作人id
     */
    public String getOperatorUid() {
        return operatorUid;
    }

    /**
     * 设置操作人id
     *
     * @param operatorUid 操作人id
     */
    public void setOperatorUid(String operatorUid) {
        this.operatorUid = operatorUid == null ? null : operatorUid.trim();
    }

    /**
     * 获取操作人姓名
     *
     * @return OPERATOR_NAME - 操作人姓名
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * 设置操作人姓名
     *
     * @param operatorName 操作人姓名
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName == null ? null : operatorName.trim();
    }

    /**
     * 获取撤销原因
     *
     * @return UNDO_REASON - 撤销原因
     */
    public String getUndoReason() {
        return undoReason;
    }

    /**
     * 设置撤销原因
     *
     * @param undoReason 撤销原因
     */
    public void setUndoReason(String undoReason) {
        this.undoReason = undoReason == null ? null : undoReason.trim();
    }

    /**
     * 获取拒绝原因
     *
     * @return REFUSE_REASON - 拒绝原因
     */
    public String getRefuseReason() {
        return refuseReason;
    }

    /**
     * 设置拒绝原因
     *
     * @param refuseReason 拒绝原因
     */
    public void setRefuseReason(String refuseReason) {
        this.refuseReason = refuseReason == null ? null : refuseReason.trim();
    }

    /**
     * 获取状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
     *
     * @return STATUS - 状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
     *
     * @param status 状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取url
     *
     * @return URL - url
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置url
     *
     * @param url url
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * 获取发布日期
     *
     * @return PUBLISH_DATE - 发布日期
     */
    public Date getPublishDate() {
        return publishDate;
    }

    /**
     * 设置发布日期
     *
     * @param publishDate 发布日期
     */
    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }


	/**
     * 获取内容
     *
     * @return CONTENT - 内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置内容
     *
     * @param content 内容
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getShare() {
		return share;
	}

	public void setShare(Integer share) {
		this.share = share;
	}

}