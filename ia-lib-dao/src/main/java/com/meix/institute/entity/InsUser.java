package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_user")
public class InsUser {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 唯一标识UUID
     */
    @Column(name = "USER")
    private String user;
    
    /**
     * 唯一标识UUID
     */
    @Column(name = "USER_TYPE")
    private Integer userType;

    /**
     * 用户名
     */
    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "OA_NAME")
    private String oaName;
    
    /**
     * 密码
     */
    @Column(name = "PASSWORD")
    private String password;

    /**
     * 公司ID
     */
    @Column(name = "COMPANY_CODE")
    private Long companyCode;

    /**
     * 手机号
     */
    @Column(name = "MOBILE")
    private String mobile;

    /**
     * 邮件
     */
    @Column(name = "EMAIL")
    private String email;

    /**
     * 头像URL
     */
    @Column(name = "HEAD_IMAGE_URL")
    private String headImageUrl;
    
    /**
     * 职位
     */
    @Column(name = "POSITION")
    private String position;
    
    /**
     * 部门
     */
    @Column(name = "DEPARTMENT")
    private String department;

    /**
     * 中文简拼
     */
    @Column(name = "CHI_SPELLING")
    private String chiSpelling;

    /**
     * 用户在职状态 0-在职，1-离职
     */
    @Column(name = "USER_STATE")
    private Integer userState;

    /**
     * 加密信息
     */
    @Column(name = "SALT")
    private String salt;

    /**
     * 用户描述
     */
    @Column(name = "DESCR")
    private String descr;
    
    /**
     * 研究方向
     */
    @Column(name = "DIRECTION")
    private String direction;
    
    @Column(name = "QUALIFYNO")
    private String qualifyno;
    
    /**
     * 角色
     */
    @Column(name = "ROLE")
    private Integer role;
    
    /**
     * 第三方唯一标识
     */
    @Column(name = "THIRD_ID")
    private String thirdId;
    
    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取唯一标识UUID
     *
     * @return USER - 唯一标识UUID
     */
    public String getUser() {
        return user;
    }

    /**
     * 设置唯一标识UUID
     *
     * @param user 唯一标识UUID
     */
    public void setUser(String user) {
        this.user = user == null ? null : user.trim();
    }

    /**
     * 获取用户名
     *
     * @return USER_NAME - 用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户名
     *
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 获取密码
     *
     * @return PASSWORD - 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 获取公司ID
     *
     * @return COMPANY_CODE - 公司ID
     */
    public Long getCompanyCode() {
        return companyCode;
    }

    /**
     * 设置公司ID
     *
     * @param companyCode 公司ID
     */
    public void setCompanyCode(Long companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 获取手机号
     *
     * @return MOBILE - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取邮件
     *
     * @return EMAIL - 邮件
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置邮件
     *
     * @param email 邮件
     */
    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }
    
    /**
     * 设置职位
     * @param position
     */
    public void setPosition(String position) {
        this.position = position;
    }
    
    /**
     * 获取职位
     *
     * @return POSITION - 职位
     */
    public String getPosition() {
        return position;
    }

    /**
     * 获取头像URL
     *
     * @return HEAD_IMAGE_URL - 头像URL
     */
    public String getHeadImageUrl() {
        return headImageUrl;
    }

    /**
     * 设置头像URL
     *
     * @param headImageUrl 头像URL
     */
    public void setHeadImageUrl(String headImageUrl) {
        this.headImageUrl = headImageUrl == null ? null : headImageUrl.trim();
    }

    /**
     * 获取中文简拼
     *
     * @return CHI_SPELLING - 中文简拼
     */
    public String getChiSpelling() {
        return chiSpelling;
    }

    /**
     * 设置中文简拼
     *
     * @param chiSpelling 中文简拼
     */
    public void setChiSpelling(String chiSpelling) {
        this.chiSpelling = chiSpelling == null ? null : chiSpelling.trim();
    }

    /**
     * 获取用户在职状态 0-在职，1-离职
     *
     * @return USER_STATE - 用户在职状态 0-在职，1-离职
     */
    public Integer getUserState() {
        return userState;
    }

    /**
     * 设置用户在职状态 0-在职，1-离职
     *
     * @param userState 用户在职状态 0-在职，1-离职
     */
    public void setUserState(Integer userState) {
        this.userState = userState;
    }

    /**
     * 获取加密信息
     *
     * @return SALT - 加密信息
     */
    public String getSalt() {
        return salt;
    }

    /**
     * 设置加密信息
     *
     * @param salt 加密信息
     */
    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }

    /**
     * 获取用户描述
     *
     * @return DESCR - 用户描述
     */
    public String getDescr() {
        return descr;
    }

    /**
     * 设置用户描述
     *
     * @param descr 用户描述
     */
    public void setDescr(String descr) {
        this.descr = descr == null ? null : descr.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getThirdId() {
        return thirdId;
    }

    public void setThirdId(String thirdId) {
        this.thirdId = thirdId;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

	public String getOaName() {
		return oaName;
	}

	public void setOaName(String oaName) {
		this.oaName = oaName;
	}

	public String getQualifyno() {
		return qualifyno;
	}

	public void setQualifyno(String qualifyno) {
		this.qualifyno = qualifyno;
	}
    
}