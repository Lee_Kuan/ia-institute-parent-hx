package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_morning_meeting_share")
public class MorningMeetingShare {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 晨会ID
     */
    @Column(name = "MEETING_ID")
    private Long meetingId;

    /**
     * 分享类型 0全市场 1 机构  2研究员 3群组  4买方 5卖方 7 公司类别 13 机构白名单客户 18-研究所白名单客户（研究所新增）
     */
    @Column(name = "SHARE_TYPE")
    private Integer shareType;

    /**
     * 分享代码
     */
    @Column(name = "DM")
    private Long dm;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取晨会ID
     *
     * @return MEETING_ID - 晨会ID
     */
    public Long getMeetingId() {
        return meetingId;
    }

    /**
     * 设置晨会ID
     *
     * @param meetingId 晨会ID
     */
    public void setMeetingId(Long meetingId) {
        this.meetingId = meetingId;
    }

    /**
     * 获取分享类型 0全市场 1 机构  2研究员 3群组  4买方 5卖方 7 公司类别 13 机构白名单客户 18-研究所白名单客户（研究所新增）
     *
     * @return SHARE_TYPE - 分享类型 0全市场 1 机构  2研究员 3群组  4买方 5卖方 7 公司类别 13 机构白名单客户 18-研究所白名单客户（研究所新增）
     */
    public Integer getShareType() {
        return shareType;
    }

    /**
     * 设置分享类型 0全市场 1 机构  2研究员 3群组  4买方 5卖方 7 公司类别 13 机构白名单客户 18-研究所白名单客户（研究所新增）
     *
     * @param shareType 分享类型 0全市场 1 机构  2研究员 3群组  4买方 5卖方 7 公司类别 13 机构白名单客户 18-研究所白名单客户（研究所新增）
     */
    public void setShareType(Integer shareType) {
        this.shareType = shareType;
    }

    /**
     * 获取分享代码
     *
     * @return DM - 分享代码
     */
    public Long getDm() {
        return dm;
    }

    /**
     * 设置分享代码
     *
     * @param dm 分享代码
     */
    public void setDm(Long dm) {
        this.dm = dm;
    }

    /**
     * @return CREATED_AT
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return CREATED_BY
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }
}