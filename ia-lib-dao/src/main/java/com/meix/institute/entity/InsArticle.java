package com.meix.institute.entity;

import java.util.Date;

import javax.persistence.*;

@Table(name = "ia_article")
public class InsArticle {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 标题
     */
    @Column(name = "TITLE")
    private String title;

    /**
     * 作者
     */
    @Column(name = "AUTHOR")
    private String author;

    /**
     * 状态： -1 删除 0 未删除
     */
    @Column(name = "STATE")
    private Integer state;

    /**
     * 发布时间
     */
    @Column(name = "PUBLISH_TIME")
    private Date publishTime;
    
    /**
     * 封面图片
     */
    @Column(name = "SHARE_PICTURE_MEDIA_ID")
    private String sharePictureMediaId;

    /**
     * 封面描述
     */
    @Column(name = "SHARE_CONTENT")
    private String shareContent;

    /**
     * 是否显示封面图片： 1 显示  0 不显示
     */
    @Column(name = "IS_SHARE_PICTURE")
    private Integer isSharePicture;

    /**
     * 图文消息的原文地址，即点击“阅读原文”后的URL
     */
    @Column(name = "CONTENT_SOURCE_URL")
    private String contentSourceUrl;

    /**
     * Uint32 是否打开评论，0不打开，1打开
     */
    @Column(name = "OPEN_COMMENT")
    private Integer openComment;

    /**
     * Uint32 是否粉丝才可评论，0所有人可评论，1粉丝才可评论
     */
    @Column(name = "ONLY_FANS_CAN_COMMENT")
    private Integer onlyFansCanComment;

    /**
     * 微信素材Id
     */
    @Column(name = "MEDIA_ID")
    private String mediaId;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /**
     * 正文
     */
    @Column(name = "CONTENT")
    private String content;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取标题
     *
     * @return TITLE - 标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 获取作者
     *
     * @return AUTHOR - 作者
     */
    public String getAuthor() {
        return author;
    }

    /**
     * 设置作者
     *
     * @param author 作者
     */
    public void setAuthor(String author) {
        this.author = author == null ? null : author.trim();
    }

    /**
     * 获取状态： -1 删除 0 未发布 1 预约发布 2 已发布
     *
     * @return STATE - 状态：-1 删除 0 未发布 1 预约发布 2 已发布
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态： -1 删除 0 未发布 1 预约发布 2 已发布
     *
     * @param state 状态： -1 删除 0 未发布 1 预约发布 2 已发布
     */
    public void setState(Integer state) {
        this.state = state;
    }
    
    /**
     * 获取发布时间
     *
     * @return PUBLISH_TIME - 发布时间
     */
    public Date getPublishTime() {
        return publishTime;
    }

    /**
     * 设置发布时间
     *
     * @param publishTime 发布时间
     */
    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }
    
    public String getSharePictureMediaId() {
        return sharePictureMediaId;
    }

    public void setSharePictureMediaId(String sharePictureMediaId) {
        this.sharePictureMediaId = sharePictureMediaId;
    }

    /**
     * 获取封面描述
     *
     * @return SHARE_CONTENT - 封面描述
     */
    public String getShareContent() {
        return shareContent;
    }

    /**
     * 设置封面描述
     *
     * @param shareContent 封面描述
     */
    public void setShareContent(String shareContent) {
        this.shareContent = shareContent == null ? null : shareContent.trim();
    }

    /**
     * 获取是否显示封面图片： 1 显示  0 不显示
     *
     * @return IS_SHARE_PICTURE - 是否显示封面图片： 1 显示  0 不显示
     */
    public Integer getIsSharePicture() {
        return isSharePicture;
    }

    /**
     * 设置是否显示封面图片： 1 显示  0 不显示
     *
     * @param isSharePicture 是否显示封面图片： 1 显示  0 不显示
     */
    public void setIsSharePicture(Integer isSharePicture) {
        this.isSharePicture = isSharePicture;
    }

    /**
     * 获取图文消息的原文地址，即点击“阅读原文”后的URL
     *
     * @return CONTENT_SOURCE_URL - 图文消息的原文地址，即点击“阅读原文”后的URL
     */
    public String getContentSourceUrl() {
        return contentSourceUrl;
    }

    /**
     * 设置图文消息的原文地址，即点击“阅读原文”后的URL
     *
     * @param contentSourceUrl 图文消息的原文地址，即点击“阅读原文”后的URL
     */
    public void setContentSourceUrl(String contentSourceUrl) {
        this.contentSourceUrl = contentSourceUrl == null ? null : contentSourceUrl.trim();
    }

    /**
     * 获取Uint32 是否打开评论，0不打开，1打开
     *
     * @return OPEN_COMMENT - Uint32 是否打开评论，0不打开，1打开
     */
    public Integer getOpenComment() {
        return openComment;
    }

    /**
     * 设置Uint32 是否打开评论，0不打开，1打开
     *
     * @param openComment Uint32 是否打开评论，0不打开，1打开
     */
    public void setOpenComment(Integer openComment) {
        this.openComment = openComment;
    }

    /**
     * 获取Uint32 是否粉丝才可评论，0所有人可评论，1粉丝才可评论
     *
     * @return ONLY_FANS_CAN_COMMENT - Uint32 是否粉丝才可评论，0所有人可评论，1粉丝才可评论
     */
    public Integer getOnlyFansCanComment() {
        return onlyFansCanComment;
    }

    /**
     * 设置Uint32 是否粉丝才可评论，0所有人可评论，1粉丝才可评论
     *
     * @param onlyFansCanComment Uint32 是否粉丝才可评论，0所有人可评论，1粉丝才可评论
     */
    public void setOnlyFansCanComment(Integer onlyFansCanComment) {
        this.onlyFansCanComment = onlyFansCanComment;
    }

    /**
     * 获取微信素材Id
     *
     * @return MEDIA_ID - 微信素材Id
     */
    public String getMediaId() {
        return mediaId;
    }

    /**
     * 设置微信素材Id
     *
     * @param mediaId 微信素材Id
     */
    public void setMediaId(String mediaId) {
        this.mediaId = mediaId == null ? null : mediaId.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }

    /**
     * 获取正文
     *
     * @return CONTENT - 正文
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置正文
     *
     * @param content 正文
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}