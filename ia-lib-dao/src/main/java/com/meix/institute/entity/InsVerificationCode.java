package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_verification_code")
public class InsVerificationCode {
    /**
     * 自增长ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 类型: (VERCODE_KEY_MNG 后台管理   VERCODE_KEY_WX  微信)
     */
    @Column(name = "TYPE")
    private String type;

    /**
     * 日期
     */
    @Column(name = "DATE")
    private Date date;

    /**
     * JESSIONID
     */
    @Column(name = "JID")
    private String jid;

    /**
     * 验证码
     */
    @Column(name = "CODE")
    private String code;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 获取自增长ID
     *
     * @return ID - 自增长ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置自增长ID
     *
     * @param id 自增长ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取类型: (VERCODE_KEY_MNG 后台管理   VERCODE_KEY_WX  微信)
     *
     * @return TYPE - 类型: (VERCODE_KEY_MNG 后台管理   VERCODE_KEY_WX  微信)
     */
    public String getType() {
        return type;
    }

    /**
     * 设置类型: (VERCODE_KEY_MNG 后台管理   VERCODE_KEY_WX  微信)
     *
     * @param type 类型: (VERCODE_KEY_MNG 后台管理   VERCODE_KEY_WX  微信)
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取日期
     *
     * @return DATE - 日期
     */
    public Date getDate() {
        return date;
    }

    /**
     * 设置日期
     *
     * @param date 日期
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * 获取JESSIONID
     *
     * @return JID - JESSIONID
     */
    public String getJid() {
        return jid;
    }

    /**
     * 设置JESSIONID
     *
     * @param jid JESSIONID
     */
    public void setJid(String jid) {
        this.jid = jid == null ? null : jid.trim();
    }

    /**
     * 获取验证码
     *
     * @return CODE - 验证码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置验证码
     *
     * @param code 验证码
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}