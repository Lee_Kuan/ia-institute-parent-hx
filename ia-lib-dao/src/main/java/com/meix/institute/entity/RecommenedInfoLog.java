package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "recommened_info_log")
public class RecommenedInfoLog {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    @Column(name = "configId")
    private String configid;

    @Column(name = "reqId")
    private String reqid;

    private Date timestamp;

    @Column(name = "respTime")
    private Date resptime;

    @Column(name = "CreateTime")
    private Date createtime;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return configId
     */
    public String getConfigid() {
        return configid;
    }

    /**
     * @param configid
     */
    public void setConfigid(String configid) {
        this.configid = configid == null ? null : configid.trim();
    }

    /**
     * @return reqId
     */
    public String getReqid() {
        return reqid;
    }

    /**
     * @param reqid
     */
    public void setReqid(String reqid) {
        this.reqid = reqid == null ? null : reqid.trim();
    }

    /**
     * @return timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return respTime
     */
    public Date getResptime() {
        return resptime;
    }

    /**
     * @param resptime
     */
    public void setResptime(Date resptime) {
        this.resptime = resptime;
    }

    /**
     * @return CreateTime
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * @param createtime
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}