package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_report_info")
public class ReportInfo {
    /**
     * 研报ID，唯一
     */
    @Id
    @Column(name = "RESEARCH_ID")
    private Long researchId;

    /**
     * 数据类型，0-研报，1-会议纪要
     */
    @Column(name = "TYPE")
    private Integer type;

    /**
     * 机构代码
     */
    @Column(name = "ORG_CODE")
    private Long orgCode;

    /**
     * 标题
     */
    @Column(name = "TITLE")
    private String title;

    /**
     * 发布日期
     */
    @Column(name = "PUBLISH_DATE")
    private Date publishDate;

    /**
     * 研报列表图
     */
    @Column(name = "PICTURE")
    private String picture;

    /**
     * 研报分类
     */
    @Column(name = "INFO_TYPE")
    private String infoType;
    
    /**
     * 研报小类
     */
    @Column(name = "INFO_SUB_TYPE")
    private String infoSubType;
    
    /**
     * 研报搜索分类
     */
    @Column(name = "INFO_SEARCH_TYPE")
    private String infoSearchType;
    
    /**
     * 研报评级
     */
    @Column(name = "INFO_GRADE")
    private String infoGrade;
    
    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "UPDATED_BY")
    private String updatedBy;

    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 摘要
     */
    @Column(name = "SUMMARY")
    private String summary;

    @Column(name = "AUTH_NAMES")
    private String authNames;

    @Column(name = "THIRD_ID")
    private String thirdId;
    
    @Column(name = "OPERATOR_UID")
    private String operatorUid;
    
    @Column(name = "OPERATOR_NAME")
    private String operatorName;
    
    @Column(name = "UNDO_REASON")
    private String undoReason;
    
    @Column(name = "REFUSE_REASON")
    private String refuseReason;
    
    @Column(name = "STATUS")
    private Integer status;
    
    @Column(name = "SHARE")
    private Integer share;

    @Column(name = "POWER_SUB_TYPE")
    private Integer powerSubType;
    
    @Column(name = "DEEP_STATUS")
    private Integer deepStatus;
    
    public String getInfoSearchType() {
		return infoSearchType;
	}

	public void setInfoSearchType(String infoSearchType) {
		this.infoSearchType = infoSearchType;
	}

	public Integer getPowerSubType() {
		return powerSubType;
	}

	public void setPowerSubType(Integer powerSubType) {
		this.powerSubType = powerSubType;
	}

	public Integer getDeepStatus() {
		return deepStatus;
	}

	public void setDeepStatus(Integer deepStatus) {
		this.deepStatus = deepStatus;
	}

	public String getOperatorUid() {
		return operatorUid;
	}

	public void setOperatorUid(String operatorUid) {
		this.operatorUid = operatorUid;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getUndoReason() {
		return undoReason;
	}

	public void setUndoReason(String undoReason) {
		this.undoReason = undoReason;
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
     * 获取研报ID，唯一
     *
     * @return RESEARCH_ID - 研报ID，唯一
     */
    public Long getResearchId() {
        return researchId;
    }

    /**
     * 设置研报ID，唯一
     *
     * @param researchId 研报ID，唯一
     */
    public void setResearchId(Long researchId) {
        this.researchId = researchId;
    }

    /**
     * 数据类型，0-研报，1-会议纪要
     *
     * @return TYPE - 数据类型，0-研报，1-会议纪要
     */
    public Integer getType() {
        return type;
    }

    /**
     * 数据类型，0-研报，1-会议纪要
     *
     * @param type 数据类型，0-研报，1-会议纪要
     */
    public void setType(Integer type) {
        this.type = type == null ? 0 : type;
    }

    /**
     * 获取机构代码
     *
     * @return ORG_CODE - 机构代码
     */
    public Long getOrgCode() {
        return orgCode;
    }

    /**
     * 设置机构代码
     *
     * @param orgCode 机构代码
     */
    public void setOrgCode(Long orgCode) {
        this.orgCode = orgCode;
    }

    /**
     * 获取标题
     *
     * @return TITLE - 标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 获取发布日期
     *
     * @return PUBLISH_DATE - 发布日期
     */
    public Date getPublishDate() {
        return publishDate;
    }

    /**
     * 设置发布日期
     *
     * @param publishDate 发布日期
     */
    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    /**
     * 获取文件类型如pdf
     *
     * @return FILE_TYPE - 文件类型如pdf
     */
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture == null ? null : picture.trim();
    }

    /**
     * 获取研报分类
     *
     * @return INFO_TYPE - 研报分类
     */
    public String getInfoType() {
        return infoType;
    }

    /**
     * 设置研报评级
     *
     * @param infoType 研报分类
     */
    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }
    
    /**
     * 获取研报评级
     *
     * @return INFO_TYPE - 研报分类
     */
    public String getInfoGrade() {
        return infoGrade;
    }

    /**
     * 设置研报分类
     *
     * @param infoGrade 研报评级分类
     */
    public void setInfoGrade(String infoGrade) {
        this.infoGrade = infoGrade;
    }

    /**
     * @return CREATED_BY
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * @return CREATED_AT
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return UPDATED_BY
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }

    /**
     * @return UPDATED_AT
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取摘要
     *
     * @return SUMMARY - 摘要
     */
    public String getSummary() {
        return summary;
    }

    /**
     * 设置摘要
     *
     * @param summary 摘要
     */
    public void setSummary(String summary) {
        this.summary = summary == null ? null : summary.trim();
    }

	public String getAuthNames() {
		return authNames;
	}

	public void setAuthNames(String authNames) {
		this.authNames = authNames;
	}

    public String getThirdId() {
        return thirdId;
    }

    public void setThirdId(String thirdId) {
        this.thirdId = thirdId;
    }

    public String getInfoSubType() {
        return infoSubType;
    }

    public void setInfoSubType(String infoSubType) {
        this.infoSubType = infoSubType;
    }

	public Integer getShare() {
		return share;
	}

	public void setShare(Integer share) {
		this.share = share;
	}

}