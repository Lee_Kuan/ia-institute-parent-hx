package com.meix.institute.entity;

import javax.persistence.*;

@Table(name = "ia_user_direction")
public class InsUserDirection {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 分析师id
     */
    @Column(name = "UID")
    private Long uid;

    /**
     * 分析师方向
     */
    @Column(name = "DIRECTION")
    private String direction;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取分析师id
     *
     * @return UID - 分析师id
     */
    public Long getUid() {
        return uid;
    }

    /**
     * 设置分析师id
     *
     * @param uid 分析师id
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * 获取分析师方向
     *
     * @return DIRECTION - 分析师方向
     */
    public String getDirection() {
        return direction;
    }

    /**
     * 设置分析师方向
     *
     * @param direction 分析师方向
     */
    public void setDirection(String direction) {
        this.direction = direction == null ? null : direction.trim();
    }
}