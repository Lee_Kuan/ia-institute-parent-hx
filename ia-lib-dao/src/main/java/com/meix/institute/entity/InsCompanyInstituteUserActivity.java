package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_company_institute_user_activity")
public class InsCompanyInstituteUserActivity {
    /**
     * 自增长ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    @Column(name = "ACTIVITY_ID")
    private Long activityId;
    /**
     * 用户名
     */
    @Column(name = "USER_NAME")
    private String userName;

    /**
     * 所属白名单机构ID
     */
    @Column(name = "COMPANY_CODE")
    private Long companyCode;

    /**
     * 用户公司名称
     */
    @Column(name = "COMPANY_NAME")
    private String companyName;

    /**
     * 职位
     */
    @Column(name = "POSITION")
    private String position;

    /**
     * 手机号
     */
    @Column(name = "MOBILE")
    private String mobile;

    /**
     * 对口销售ID
     */
    @Column(name = "SALER_UID")
    private Long salerUid;

    /**
     * 对口销售名字
     */
    @Column(name = "SALER_NAME")
    private String salerName;

    /**
     * 到期时间
     */
    @Column(name = "EXPIRE_TIME")
    private Date expireTime;


    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 获取自增长ID
     *
     * @return ID - 自增长ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置自增长ID
     *
     * @param id 自增长ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取用户名
     *
     * @return USER_NAME - 用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户名
     *
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 获取所属白名单机构ID
     *
     * @return COMPANY_CODE - 所属白名单机构ID
     */
    public Long getCompanyCode() {
        return companyCode;
    }

    /**
     * 设置所属白名单机构ID
     *
     * @param companyCode 所属白名单机构ID
     */
    public void setCompanyCode(Long companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 获取用户公司名称
     *
     * @return COMPANY_NAME - 用户公司名称
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * 设置用户公司名称
     *
     * @param companyName 用户公司名称
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    /**
     * 获取职位
     *
     * @return POSITION - 职位
     */
    public String getPosition() {
        return position;
    }

    /**
     * 设置职位
     *
     * @param position 职位
     */
    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    /**
     * 获取手机号
     *
     * @return MOBILE - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取对口销售ID
     *
     * @return SALER_UID - 对口销售ID
     */
    public Long getSalerUid() {
        return salerUid;
    }

    /**
     * 设置对口销售ID
     *
     * @param salerUid 对口销售ID
     */
    public void setSalerUid(Long salerUid) {
        this.salerUid = salerUid;
    }

    /**
     * 获取对口销售名字
     *
     * @return SALER_NAME - 对口销售名字
     */
    public String getSalerName() {
        return salerName;
    }

    /**
     * 设置对口销售名字
     *
     * @param salerName 对口销售名字
     */
    public void setSalerName(String salerName) {
        this.salerName = salerName == null ? null : salerName.trim();
    }

    /**
     * 获取到期时间
     *
     * @return EXPIRE_TIME - 到期时间
     */
    public Date getExpireTime() {
        return expireTime;
    }

    /**
     * 设置到期时间
     *
     * @param expireTime 到期时间
     */
    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    /**
     * @return CREATED_BY
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * @return CREATED_AT
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

	public Long getActivityId() {
		return activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
    

}