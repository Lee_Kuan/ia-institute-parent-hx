package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_activity_person")
public class InsActivityPerson {
    /**
     * 自增长ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 活动ID
     */
    @Column(name = "ACTIVITY_ID")
    private Long activityId;

    /**
     * 用户姓名
     */
    @Column(name = "USER_NAME")
    private String userName;

    /**
     * 手机号
     */
    @Column(name = "MOBILE")
    private String mobile;

    /**
     * 机构名称
     */
    @Column(name = "COMPANY_NAME")
    private String companyName;

    /**
     * 职位
     */
    @Column(name = "POSITION")
    private String position;

    /**
     * 报名类型 0-自主报名，1-代为报名
     */
    @Column(name = "TYPE")
    private Integer type;

    /**
     * 操作人ID（代为报名人）
     */
    @Column(name = "OPERATOR_ID")
    private String operatorId;

    /**
     * 操作人姓名
     */
    @Column(name = "OPERATOR_NAME")
    private String operatorName;

    @Column(name = "UID")
    private Long uid;
    
    @Column(name = "SALER_ID")
    private Long salerId;
    /**
     * 对口销售
     */
    @Column(name = "SALER_NAME")
    private String salerName;

    /**
     * 报名状态 0-未审核，1-已通过，2-已拒绝
     */
    @Column(name = "STATUS")
    private Integer status;

    /**
     * 审核时间
     */
    @Column(name = "AUDIT_AT")
    private Date auditAt;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Long getSalerId() {
		return salerId;
	}

	public void setSalerId(Long salerId) {
		this.salerId = salerId;
	}

	/**
     * 获取自增长ID
     *
     * @return ID - 自增长ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置自增长ID
     *
     * @param id 自增长ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取活动ID
     *
     * @return ACTIVITY_ID - 活动ID
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * 设置活动ID
     *
     * @param activityId 活动ID
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * 获取用户姓名
     *
     * @return USER_NAME - 用户姓名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户姓名
     *
     * @param userName 用户姓名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 获取手机号
     *
     * @return MOBILE - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取机构名称
     *
     * @return COMPANY_NAME - 机构名称
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * 设置机构名称
     *
     * @param companyName 机构名称
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    /**
     * 获取职位
     *
     * @return POSITION - 职位
     */
    public String getPosition() {
        return position;
    }

    /**
     * 设置职位
     *
     * @param position 职位
     */
    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    /**
     * 获取报名类型 0-自主报名，1-代为报名
     *
     * @return TYPE - 报名类型 0-自主报名，1-代为报名
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置报名类型 0-自主报名，1-代为报名
     *
     * @param type 报名类型 0-自主报名，1-代为报名
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取操作人ID（代为报名人）
     *
     * @return OPERATOR_ID - 操作人ID（代为报名人）
     */
    public String getOperatorId() {
        return operatorId;
    }

    /**
     * 设置操作人ID（代为报名人）
     *
     * @param operatorId 操作人ID（代为报名人）
     */
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId == null ? null : operatorId.trim();
    }

    /**
     * 获取操作人姓名
     *
     * @return OPERATOR_NAME - 操作人姓名
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * 设置操作人姓名
     *
     * @param operatorName 操作人姓名
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName == null ? null : operatorName.trim();
    }

    /**
     * 获取对口销售
     *
     * @return SALER_NAME - 对口销售
     */
    public String getSalerName() {
        return salerName;
    }

    /**
     * 设置对口销售
     *
     * @param salerName 对口销售
     */
    public void setSalerName(String salerName) {
        this.salerName = salerName == null ? null : salerName.trim();
    }

    /**
     * 获取报名状态 0-未审核，1-已通过，2-已拒绝
     *
     * @return STATUS - 报名状态 0-未审核，1-已通过，2-已拒绝
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置报名状态 0-未审核，1-已通过，2-已拒绝
     *
     * @param status 报名状态 0-未审核，1-已通过，2-已拒绝
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取审核时间
     *
     * @return AUDIT_AT - 审核时间
     */
    public Date getAuditAt() {
        return auditAt;
    }

    /**
     * 设置审核时间
     *
     * @param auditAt 审核时间
     */
    public void setAuditAt(Date auditAt) {
        this.auditAt = auditAt;
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }
}