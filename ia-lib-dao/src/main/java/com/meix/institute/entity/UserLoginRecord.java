package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_user_login_record")
public class UserLoginRecord {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    @Column(name = "UID")
    private Long uid;

    /**
     * 2 公众号 3小程序
     */
    @Column(name = "CLIENT_TYPE")
    private Integer clientType;

    @Column(name = "CREATE_TIME")
    private Date createTime;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return UID
     */
    public Long getUid() {
        return uid;
    }

    /**
     * @param uid
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * 获取2 公众号 3小程序
     *
     * @return CLIENT_TYPE - 2 公众号 3小程序
     */
    public Integer getClientType() {
        return clientType;
    }

    /**
     * 设置2 公众号 3小程序
     *
     * @param clientType 2 公众号 3小程序
     */
    public void setClientType(Integer clientType) {
        this.clientType = clientType;
    }

    /**
     * @return CREATE_TIME
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}