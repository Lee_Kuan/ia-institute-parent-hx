package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_sync_snd_req")
public class InsSyncSndReq {
    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "JDBC")
    private Long id;

    @Column(name = "SND_APP")
    private String sndApp;

    @Column(name = "REC_APP")
    private String recApp;

    @Column(name = "METHOD")
    private String method;

    @Column(name = "STATE")
    private String state;

    @Column(name = "RETRY_MAX")
    private Integer retryMax;

    @Column(name = "RETRY_CNT")
    private Integer retryCnt;

    @Column(name = "RETRY_LAST")
    private Date retryLast;

    @Column(name = "REMARKS")
    private String remarks;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /**
     *  3研报 4用户 5 股票 8活动
     */
    @Column(name = "DATA_TYPE")
    private Integer dataType;

    /**
     * 关键字方便查询
     */
    @Column(name = "KEYWORD")
    private String keyword;

    @Column(name = "REQ_HEADER")
    private String reqHeader;

    @Column(name = "REQ_BODY")
    private String reqBody;

    @Column(name = "RESULT")
    private String result;
    
	/**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return SND_APP
     */
    public String getSndApp() {
        return sndApp;
    }

    /**
     * @param sndApp
     */
    public void setSndApp(String sndApp) {
        this.sndApp = sndApp == null ? null : sndApp.trim();
    }

    /**
     * @return REC_APP
     */
    public String getRecApp() {
        return recApp;
    }

    /**
     * @param recApp
     */
    public void setRecApp(String recApp) {
        this.recApp = recApp == null ? null : recApp.trim();
    }

    /**
     * @return METHOD
     */
    public String getMethod() {
        return method;
    }

    /**
     * @param method
     */
    public void setMethod(String method) {
        this.method = method == null ? null : method.trim();
    }

    /**
     * @return STATE
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * @return RETRY_MAX
     */
    public Integer getRetryMax() {
        return retryMax;
    }

    /**
     * @param retryMax
     */
    public void setRetryMax(Integer retryMax) {
        this.retryMax = retryMax;
    }

    /**
     * @return RETRY_CNT
     */
    public Integer getRetryCnt() {
        return retryCnt;
    }

    /**
     * @param retryCnt
     */
    public void setRetryCnt(Integer retryCnt) {
        this.retryCnt = retryCnt;
    }

    /**
     * @return RETRY_LAST
     */
    public Date getRetryLast() {
        return retryLast;
    }

    /**
     * @param retryLast
     */
    public void setRetryLast(Date retryLast) {
        this.retryLast = retryLast;
    }

    /**
     * @return REMARKS
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    /**
     * @return CREATED_AT
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return CREATED_BY
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * @return UPDATED_AT
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return UPDATED_BY
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }

    /**
     * 获取 3研报 4用户 5 股票 8活动
     *
     * @return DATA_TYPE -  3研报 4用户 5 股票 8活动
     */
    public Integer getDataType() {
        return dataType;
    }

    /**
     * 设置 3研报 4用户 5 股票 8活动
     *
     * @param dataType  3研报 4用户 5 股票 8活动
     */
    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    /**
     * 获取关键字方便查询
     *
     * @return KEYWORD - 关键字方便查询
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * 设置关键字方便查询
     *
     * @param keyword 关键字方便查询
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword == null ? null : keyword.trim();
    }

    /**
     * @return REQ_HEADER
     */
    public String getReqHeader() {
        return reqHeader;
    }

    /**
     * @param reqHeader
     */
    public void setReqHeader(String reqHeader) {
        this.reqHeader = reqHeader == null ? null : reqHeader.trim();
    }

    /**
     * @return REQ_BODY
     */
    public String getReqBody() {
        return reqBody;
    }

    /**
     * @param reqBody
     */
    public void setReqBody(String reqBody) {
        this.reqBody = reqBody == null ? null : reqBody.trim();
    }

    /**
     * @return RESULT
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result
     */
    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }
}