package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_activity")
public class InsActivity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
     */
    @Column(name = "ACTIVITY_STATUS")
    private Integer activityStatus;

    /**
     * 公司代码
     */
    @Column(name = "COMPANY_CODE")
    private Long companyCode;

    /**
     * 活动类型：6 调研 7电话会议 8路演 11策略 12会议
     */
    @Column(name = "ACTIVITY_TYPE")
    private Integer activityType;

    /**
     * 标题
     */
    @Column(name = "TITLE")
    private String title;

    /**
     * 调研对象或宏观策略
     */
    @Column(name = "RESEARCH_OBJECT")
    private String researchObject;

    /**
     * 开始时间
     */
    @Column(name = "START_TIME")
    private Date startTime;

    /**
     * 结束时间
     */
    @Column(name = "END_TIME")
    private Date endTime;

    /**
     * 报名截止时间
     */
    @Column(name = "APPLY_DEADLINE_TIME")
    private Date applyDeadlineTime;

    /**
     * 详细地址
     */
    @Column(name = "ADDRESS")
    private String address;

    /**
     * 活动描述
     */
    @Column(name = "ACTIVITY_DESC")
    private String activityDesc;

    /**
     * 城市
     */
    @Column(name = "CITY")
    private String city;

    /**
     * 文件属性:json方式存放
     */
    @Column(name = "FILE_ATTR")
    private String fileAttr;

    /**
     * 会议状态 NULL：根据会议时间判断会议状态 3 回放 4 进行中
     */
    @Column(name = "IS_END")
    private Short isEnd;

    /**
     * 活动海报链接地址
     */
    @Column(name = "RESOURCE_URL")
    private String resourceUrl;

    /**
     * 活动海报链接地址(小图)
     */
    @Column(name = "RESOURCE_URL_SMALL")
    private String resourceUrlSmall;

    /**
     * 是否隐藏 0-否，1-是 （研究所新增）
     */
    @Column(name = "IS_HIDEN")
    private Integer isHiden;

    /**
     * 操作人UID
     */
    @Column(name = "OPERATOR_UID")
    private String operatorUid;

    /**
     * 操作人姓名
     */
    @Column(name = "OPERATOR_NAME")
    private String operatorName;

    /**
     * 撤销原因
     */
    @Column(name = "UNDO_REASON")
    private String undoReason;

    /**
     * 拒绝原因
     */
    @Column(name = "REFUSE_REASON")
    private String refuseReason;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    @Column(name = "SHARE")
    private Integer share;
    
    @Column(name = "WEBCAST_TYPE")
    private Integer webcastType;
    
    @Column(name = "REPLAY_TYPE")
    private Integer replayType;
    
    @Column(name = "IS_ARTIFICIAL")
    private Integer isArtificial;
    
    @Column(name = "JOIN_NUMBER")
    private String joinNumber;
    
    @Column(name = "JOIN_PASSWORD")
    private String joinPassword;
    
    public Integer getIsArtificial() {
		return isArtificial;
	}

	public void setIsArtificial(Integer isArtificial) {
		this.isArtificial = isArtificial;
	}

	/**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
     *
     * @return ACTIVITY_STATUS - 状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
     */
    public Integer getActivityStatus() {
        return activityStatus;
    }

    /**
     * 设置状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
     *
     * @param activityStatus 状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
     */
    public void setActivityStatus(Integer activityStatus) {
        this.activityStatus = activityStatus;
    }

    /**
     * 获取公司代码
     *
     * @return COMPANY_CODE - 公司代码
     */
    public Long getCompanyCode() {
        return companyCode;
    }

    /**
     * 设置公司代码
     *
     * @param companyCode 公司代码
     */
    public void setCompanyCode(Long companyCode) {
        this.companyCode = companyCode;
    }


    /**
     * 获取活动类型：6 调研 7电话会议 8路演 11策略 12会议
     *
     * @return ACTIVITY_TYPE - 活动类型：6 调研 7电话会议 8路演 11策略 12会议
     */
    public Integer getActivityType() {
        return activityType;
    }

    /**
     * 设置活动类型：6 调研 7电话会议 8路演 11策略 12会议
     *
     * @param activityType 活动类型：6 调研 7电话会议 8路演 11策略 12会议
     */
    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    /**
     * 获取标题
     *
     * @return TITLE - 标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 获取调研对象或宏观策略
     *
     * @return RESEARCH_OBJECT - 调研对象或宏观策略
     */
    public String getResearchObject() {
        return researchObject;
    }

    /**
     * 设置调研对象或宏观策略
     *
     * @param researchObject 调研对象或宏观策略
     */
    public void setResearchObject(String researchObject) {
        this.researchObject = researchObject == null ? null : researchObject.trim();
    }

    /**
     * 获取开始时间
     *
     * @return START_TIME - 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置开始时间
     *
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取结束时间
     *
     * @return END_TIME - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取报名截止时间
     *
     * @return APPLY_DEADLINE_TIME - 报名截止时间
     */
    public Date getApplyDeadlineTime() {
        return applyDeadlineTime;
    }

    /**
     * 设置报名截止时间
     *
     * @param applyDeadlineTime 报名截止时间
     */
    public void setApplyDeadlineTime(Date applyDeadlineTime) {
        this.applyDeadlineTime = applyDeadlineTime;
    }

    /**
     * 获取详细地址
     *
     * @return ADDRESS - 详细地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置详细地址
     *
     * @param address 详细地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取活动描述
     *
     * @return ACTIVITY_DESC - 活动描述
     */
    public String getActivityDesc() {
        return activityDesc;
    }

    /**
     * 设置活动描述
     *
     * @param activityDesc 活动描述
     */
    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc == null ? null : activityDesc.trim();
    }

    /**
     * 获取城市
     *
     * @return CITY - 城市
     */
    public String getCity() {
        return city;
    }

    /**
     * 设置城市
     *
     * @param city 城市
     */
    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    /**
     * 获取文件属性:json方式存放
     *
     * @return FILE_ATTR - 文件属性:json方式存放
     */
    public String getFileAttr() {
        return fileAttr;
    }

    /**
     * 设置文件属性:json方式存放
     *
     * @param fileAttr 文件属性:json方式存放
     */
    public void setFileAttr(String fileAttr) {
        this.fileAttr = fileAttr == null ? null : fileAttr.trim();
    }

    /**
     * 获取会议状态 NULL：根据会议时间判断会议状态 3 回放 4 进行中
     *
     * @return IS_END - 会议状态 NULL：根据会议时间判断会议状态 3 回放 4 进行中
     */
    public Short getIsEnd() {
        return isEnd;
    }

    /**
     * 设置会议状态 NULL：根据会议时间判断会议状态 3 回放 4 进行中
     *
     * @param isEnd 会议状态 NULL：根据会议时间判断会议状态 3 回放 4 进行中
     */
    public void setIsEnd(Short isEnd) {
        this.isEnd = isEnd;
    }

    /**
     * 获取活动海报链接地址
     *
     * @return RESOURCE_URL - 活动海报链接地址
     */
    public String getResourceUrl() {
        return resourceUrl;
    }

    /**
     * 设置活动海报链接地址
     *
     * @param resourceUrl 活动海报链接地址
     */
    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl == null ? null : resourceUrl.trim();
    }

    /**
     * 获取活动海报链接地址(小图)
     *
     * @return RESOURCE_URL_SMALL - 活动海报链接地址(小图)
     */
    public String getResourceUrlSmall() {
        return resourceUrlSmall;
    }

    /**
     * 设置活动海报链接地址(小图)
     *
     * @param resourceUrlSmall 活动海报链接地址(小图)
     */
    public void setResourceUrlSmall(String resourceUrlSmall) {
        this.resourceUrlSmall = resourceUrlSmall == null ? null : resourceUrlSmall.trim();
    }

    /**
     * 获取是否隐藏 0-否，1-是 （研究所新增）
     *
     * @return IS_HIDEN - 是否隐藏 0-否，1-是 （研究所新增）
     */
    public Integer getIsHiden() {
        return isHiden;
    }

    /**
     * 设置是否隐藏 0-否，1-是 （研究所新增）
     *
     * @param isHiden 是否隐藏 0-否，1-是 （研究所新增）
     */
    public void setIsHiden(Integer isHiden) {
        this.isHiden = isHiden;
    }

    /**
     * 获取操作人UID
     *
     * @return OPERATOR_UID - 操作人UID
     */
    public String getOperatorUid() {
        return operatorUid;
    }

    /**
     * 设置操作人UID
     *
     * @param operatorUid 操作人UID
     */
    public void setOperatorUid(String operatorUid) {
        this.operatorUid = operatorUid == null ? null : operatorUid.trim();
    }

    /**
     * 获取操作人姓名
     *
     * @return OPERATOR_NAME - 操作人姓名
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * 设置操作人姓名
     *
     * @param operatorName 操作人姓名
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName == null ? null : operatorName.trim();
    }

    /**
     * 获取撤销原因
     *
     * @return UNDO_REASON - 撤销原因
     */
    public String getUndoReason() {
        return undoReason;
    }

    /**
     * 设置撤销原因
     *
     * @param undoReason 撤销原因
     */
    public void setUndoReason(String undoReason) {
        this.undoReason = undoReason == null ? null : undoReason.trim();
    }

    /**
     * 获取拒绝原因
     *
     * @return REFUSE_REASON - 拒绝原因
     */
    public String getRefuseReason() {
        return refuseReason;
    }

    /**
     * 设置拒绝原因
     *
     * @param refuseReason 拒绝原因
     */
    public void setRefuseReason(String refuseReason) {
        this.refuseReason = refuseReason == null ? null : refuseReason.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }

	public Integer getShare() {
		return share;
	}

	public void setShare(Integer share) {
		this.share = share;
	}

	public Integer getWebcastType() {
		return webcastType;
	}

	public void setWebcastType(Integer webcastType) {
		this.webcastType = webcastType;
	}

	public Integer getReplayType() {
		return replayType;
	}

	public void setReplayType(Integer replayType) {
		this.replayType = replayType;
	}

	public String getJoinNumber() {
		return joinNumber;
	}

	public void setJoinNumber(String joinNumber) {
		this.joinNumber = joinNumber;
	}

	public String getJoinPassword() {
		return joinPassword;
	}

	public void setJoinPassword(String joinPassword) {
		this.joinPassword = joinPassword;
	}
}