package com.meix.institute.entity;

import java.util.Date;

import javax.persistence.*;

@Table(name = "ia_article_group")
public class InsArticleGroup {
    /**
     * 流水号
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 标题（第一个文章标题）
     */
    @Column(name = "TITLE")
    private String title;

    /**
     * 状态：  -1 删除 0 未删除
     */
    @Column(name = "STATE")
    private Integer state;
    
    /**
     * 发布时间
     */
    @Column(name = "PUBLISH_TIME")
    private Date publishTime;

    /**
     * 关联文章01
     */
    @Column(name = "ARTICLE01")
    private Long article01;

    /**
     * 关联文章02
     */
    @Column(name = "ARTICLE02")
    private Long article02;

    /**
     * 关联文章03
     */
    @Column(name = "ARTICLE03")
    private Long article03;

    /**
     * 关联文章04
     */
    @Column(name = "ARTICLE04")
    private Long article04;

    /**
     * 关联文章05
     */
    @Column(name = "ARTICLE05")
    private Long article05;

    /**
     * 关联文章06
     */
    @Column(name = "ARTICLE06")
    private Long article06;

    /**
     * 关联文章07
     */
    @Column(name = "ARTICLE07")
    private Long article07;

    /**
     * 关联文章08
     */
    @Column(name = "ARTICLE08")
    private Long article08;

    /**
     * 微信素材id
     */
    @Column(name = "MEDIA_ID")
    private String mediaId;
    
    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /**
     * 获取流水号
     *
     * @return ID - 流水号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置流水号
     *
     * @param id 流水号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取标题（第一个文章标题）
     *
     * @return TITLE - 标题（第一个文章标题）
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置标题（第一个文章标题）
     *
     * @param title 标题（第一个文章标题）
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 获取状态：  -1 删除 0 未发布 1 预约发布 2 已发布
     *
     * @return STATE - 状态： -1 删除 0 未发布 1 预约发布 2 已发布
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态：  -1 删除 0 未发布 1 预约发布 2 已发布
     *
     * @param state 状态：  -1 删除 0 未发布 1 预约发布 2 已发布
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取发布时间
     *
     * @return PUBLISH_TIME - 发布时间
     */
    public Date getPublishTime() {
        return publishTime;
    }

    /**
     * 设置发布时间
     *
     * @param publishTime 发布时间
     */
    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }
    
    /**
     * 获取关联文章01
     *
     * @return ARTICLE01 - 关联文章01
     */
    public Long getArticle01() {
        return article01;
    }

    /**
     * 设置关联文章01
     *
     * @param article01 关联文章01
     */
    public void setArticle01(Long article01) {
        this.article01 = article01;
    }

    /**
     * 获取关联文章02
     *
     * @return ARTICLE02 - 关联文章02
     */
    public Long getArticle02() {
        return article02;
    }

    /**
     * 设置关联文章02
     *
     * @param article02 关联文章02
     */
    public void setArticle02(Long article02) {
        this.article02 = article02;
    }

    /**
     * 获取关联文章03
     *
     * @return ARTICLE03 - 关联文章03
     */
    public Long getArticle03() {
        return article03;
    }

    /**
     * 设置关联文章03
     *
     * @param article03 关联文章03
     */
    public void setArticle03(Long article03) {
        this.article03 = article03;
    }

    /**
     * 获取关联文章04
     *
     * @return ARTICLE04 - 关联文章04
     */
    public Long getArticle04() {
        return article04;
    }

    /**
     * 设置关联文章04
     *
     * @param article04 关联文章04
     */
    public void setArticle04(Long article04) {
        this.article04 = article04;
    }

    /**
     * 获取关联文章05
     *
     * @return ARTICLE05 - 关联文章05
     */
    public Long getArticle05() {
        return article05;
    }

    /**
     * 设置关联文章05
     *
     * @param article05 关联文章05
     */
    public void setArticle05(Long article05) {
        this.article05 = article05;
    }

    /**
     * 获取关联文章06
     *
     * @return ARTICLE06 - 关联文章06
     */
    public Long getArticle06() {
        return article06;
    }

    /**
     * 设置关联文章06
     *
     * @param article06 关联文章06
     */
    public void setArticle06(Long article06) {
        this.article06 = article06;
    }

    /**
     * 获取关联文章07
     *
     * @return ARTICLE07 - 关联文章07
     */
    public Long getArticle07() {
        return article07;
    }

    /**
     * 设置关联文章07
     *
     * @param article07 关联文章07
     */
    public void setArticle07(Long article07) {
        this.article07 = article07;
    }

    /**
     * 获取关联文章08
     *
     * @return ARTICLE08 - 关联文章08
     */
    public Long getArticle08() {
        return article08;
    }

    /**
     * 设置关联文章08
     *
     * @param article08 关联文章08
     */
    public void setArticle08(Long article08) {
        this.article08 = article08;
    }

    /**
     * 获取微信素材id
     *
     * @return MEDIA_ID - 微信素材id
     */
    public String getMediaId() {
        return mediaId;
    }

    /**
     * 设置微信素材id
     *
     * @param mediaId 微信素材id
     */
    public void setMediaId(String mediaId) {
        this.mediaId = mediaId == null ? null : mediaId.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }
}