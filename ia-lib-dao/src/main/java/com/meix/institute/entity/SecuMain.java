package com.meix.institute.entity;

import javax.persistence.*;

@Table(name = "ia_secumain")
public class SecuMain {
    /**
     * 自增长ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 股票内码
     */
    @Column(name = "INNER_CODE")
    private Integer innerCode;

    /**
     * 股票代码
     */
    @Column(name = "SECU_CODE")
    private String secuCode;

    /**
     * 股票名称
     */
    @Column(name = "SECU_ABBR")
    private String secuAbbr;

    @Column(name = "SECU_MARKET")
    private Integer secuMarket;

    /**
     * 股票分类
     */
    @Column(name = "SECU_CATEGORY")
    private Integer secuCategory;

    /**
     * 行业代码
     */
    @Column(name = "INDUSTRY_CODE")
    private Integer industryCode;

    /**
     * 行业名称
     */
    @Column(name = "INDUSTRY_NAME")
    private String industryName;

    /**
     * 交易所代码
     */
    @Column(name = "SUFFIX")
    private String suffix;

    /**
     * 获取自增长ID
     *
     * @return ID - 自增长ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置自增长ID
     *
     * @param id 自增长ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取股票内码
     *
     * @return INNER_CODE - 股票内码
     */
    public Integer getInnerCode() {
        return innerCode;
    }

    /**
     * 设置股票内码
     *
     * @param innerCode 股票内码
     */
    public void setInnerCode(Integer innerCode) {
        this.innerCode = innerCode;
    }

    /**
     * 获取股票代码
     *
     * @return SECU_CODE - 股票代码
     */
    public String getSecuCode() {
        return secuCode;
    }

    /**
     * 设置股票代码
     *
     * @param secuCode 股票代码
     */
    public void setSecuCode(String secuCode) {
        this.secuCode = secuCode == null ? null : secuCode.trim();
    }

    /**
     * 获取股票名称
     *
     * @return SECU_ABBR - 股票名称
     */
    public String getSecuAbbr() {
        return secuAbbr;
    }

    /**
     * 设置股票名称
     *
     * @param secuAbbr 股票名称
     */
    public void setSecuAbbr(String secuAbbr) {
        this.secuAbbr = secuAbbr == null ? null : secuAbbr.trim();
    }

    /**
     * @return SECU_MARKET
     */
    public Integer getSecuMarket() {
        return secuMarket;
    }

    /**
     * @param secuMarket
     */
    public void setSecuMarket(Integer secuMarket) {
        this.secuMarket = secuMarket;
    }

    /**
     * 获取股票分类
     *
     * @return SECU_CATEGORY - 股票分类
     */
    public Integer getSecuCategory() {
        return secuCategory;
    }

    /**
     * 设置股票分类
     *
     * @param secuCategory 股票分类
     */
    public void setSecuCategory(Integer secuCategory) {
        this.secuCategory = secuCategory;
    }

    /**
     * 获取行业代码
     *
     * @return INDUSTRY_CODE - 行业代码
     */
    public Integer getIndustryCode() {
        return industryCode;
    }

    /**
     * 设置行业代码
     *
     * @param industryCode 行业代码
     */
    public void setIndustryCode(Integer industryCode) {
        this.industryCode = industryCode;
    }

    /**
     * 获取行业名称
     *
     * @return INDUSTRY_NAME - 行业名称
     */
    public String getIndustryName() {
        return industryName;
    }

    /**
     * 设置行业名称
     *
     * @param industryName 行业名称
     */
    public void setIndustryName(String industryName) {
        this.industryName = industryName == null ? null : industryName.trim();
    }

    /**
     * 获取交易所代码
     *
     * @return SUFFIX - 交易所代码
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * 设置交易所代码
     *
     * @param suffix 交易所代码
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix == null ? null : suffix.trim();
    }
}