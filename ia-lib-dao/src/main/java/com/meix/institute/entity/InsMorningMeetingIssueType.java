package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_morning_meeting_issuetype")
public class InsMorningMeetingIssueType {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 晨会ID
     */
    @Column(name = "MEETING_ID")
    private Long meetingId;

    /**
     * 发布渠道 0-美市机构主页 1-公众号 2-短信 3-邮件
     */
    @Column(name = "ISSUE_TYPE")
    private Integer issueType;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取活动ID
     *
     * @return MEETINGID - 晨会ID
     */
    public Long getMeetingId() {
        return meetingId;
    }

    /**
     * 设置活动ID
     *
     * @param meetingId 晨会ID
     */
    public void setMeetingId(Long meetingId) {
        this.meetingId = meetingId;
    }

    /**
     * 获取发布渠道 0-美市机构主页 1-公众号 2-短信 3-邮件
     *
     * @return ISSUE_TYPE - 发布渠道 0-美市机构主页 1-公众号 2-短信 3-邮件
     */
    public Integer getIssueType() {
        return issueType;
    }

    /**
     * 设置发布渠道 0-美市机构主页 1-公众号 2-短信 3-邮件
     *
     * @param issueType 发布渠道 0-美市机构主页 1-公众号 2-短信 3-邮件
     */
    public void setIssueType(Integer issueType) {
        this.issueType = issueType;
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }
}