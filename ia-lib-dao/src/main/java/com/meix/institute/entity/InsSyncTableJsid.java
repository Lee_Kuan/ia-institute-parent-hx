package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_sync_table_jsid")
public class InsSyncTableJsid {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 同步表名称
     */
    @Column(name = "TABLE_NAME")
    private String tableName;

    /**
     * 同步时间（起始）
     */
    @Column(name = "JS_UPDATE_TIME_FM")
    private Date jsUpdateTimeFm;
    
    /**
     * 同步时间（截止）
     */
    @Column(name = "JS_UPDATE_TIME_TO")
    private Date jsUpdateTimeTo;

    /**
     * 同步状态:  0 失败  1 成功 2 关闭
     */
    @Column(name = "STATE")
    private String state;

    /**
     * 重试次数
     */
    @Column(name = "RETRY_CNT")
    private Integer retryCnt;

    /**
     * 最大重试次数
     */
    @Column(name = "RETRY_MAX")
    private Integer retryMax;
    
    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /**
     * 备注
     */
    @Column(name = "REMARKS")
    private String remarks;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取同步表名称
     *
     * @return TABLE_NAME - 同步表名称
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * 设置同步表名称
     *
     * @param tableName 同步表名称
     */
    public void setTableName(String tableName) {
        this.tableName = tableName == null ? null : tableName.trim();
    }

    /**
     * 获取同步时间（起始）
     *
     * @return JS_UPDATE_TIME_FM - 同步时间（起始）
     */
    public Date getJsUpdateTimeFm() {
        return jsUpdateTimeFm;
    }

    /**
     * 设置上次更新时间戳
     *
     * @param jsUpdateTimeFm 同步时间（起始）
     */
    public void setJsUpdateTimeFm(Date jsUpdateTimeFm) {
        this.jsUpdateTimeFm = jsUpdateTimeFm;
    }
    
    /**
     * 获取同步时间（截止）
     *
     * @return JS_UPDATE_TIME_TO - 同步时间（起始）
     */
    public Date getJsUpdateTimeTo() {
        return jsUpdateTimeTo;
    }

    /**
     * 设置上次更新时间戳
     *
     * @param jsUpdateTimeTo 同步时间（起始）
     */
    public void setJsUpdateTimeTo(Date jsUpdateTimeTo) {
        this.jsUpdateTimeTo = jsUpdateTimeTo;
    }

    /**
     * 获取同步状态:  0 失败  1 成功 2 关闭
     *
     * @return STATE - 同步状态:  0 失败  1 成功 2 关闭
     */
    public String getState() {
        return state;
    }

    /**
     * 设置同步状态:  0 失败  1 成功 2 关闭
     *
     * @param state 同步状态:  0 失败  1 成功 2 关闭
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * 获取重试次数
     *
     * @return RETRY_CNT - 重试次数
     */
    public Integer getRetryCnt() {
        return retryCnt;
    }

    /**
     * 设置重试次数
     *
     * @param retryCnt 重试次数
     */
    public void setRetryCnt(Integer retryCnt) {
        this.retryCnt = retryCnt;
    }

    /**
     * 获取最大重试次数
     *
     * @return RETRY_MAX - 最大重试次数
     */
    public Integer getRetryMax() {
        return retryMax;
    }

    /**
     * 设置最大重试次数
     *
     * @param retryMax 最大重试次数
     */
    public void setRetryMax(Integer retryMax) {
        this.retryMax = retryMax;
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }

    /**
     * 获取备注
     *
     * @return REMARKS - 备注
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * 设置备注
     *
     * @param remarks 备注
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }
}