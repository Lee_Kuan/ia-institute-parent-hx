package com.meix.institute.entity;

import java.util.Date;

import javax.persistence.*;

@Table(name = "ia_report_relation")
public class ReportRelation {
    
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;
    
    @Column(name = "RESEARCH_ID")
    private Long researchId;

    /**
     * 内容描述
     */
    @Column(name = "CONTENT")
    private String content;

    @Column(name = "CODE")
    private Long code;
    
    @Column(name = "NO")
    private Integer no;
    /**
     * 内容类别 0-rate，1-author，2-innerCode，3-industryCode
     */
    @Column(name = "TYPE")
    private Integer type;

    @Column(name = "XNIND_CODE")
    private String xnindCode;
    
    @Column(name = "XNIND_NAME")
    private String xnindName;
    
    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 获取研报id，唯一
     *
     * @return RESEARCH_ID - 研报id，唯一
     */
    public Long getResearchId() {
        return researchId;
    }

    /**
     * 设置研报id，唯一
     *
     * @param researchId 研报id，唯一
     */
    public void setResearchId(Long researchId) {
        this.researchId = researchId;
    }

    /**
     * 获取内容描述
     *
     * @return CONTENT - 内容描述
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置内容描述
     *
     * @param content 内容描述
     */
    public void setContent(String content) {
        this.content = content;
    }

    public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	/**
     * 获取内容类别 0-rate，1-author，2-innerCode，3-industryCode
     *
     * @return TYPE - 内容类别 0-rate，1-author，2-innerCode，3-industryCode
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置内容类别 0-rate，1-author，2-innerCode，3-industryCode
     *
     * @param type 内容类别 0-rate，1-author，2-innerCode，3-industryCode
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return CREATED_AT
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return CREATED_BY
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getXnindCode() {
		return xnindCode;
	}

	public void setXnindCode(String xnindCode) {
		this.xnindCode = xnindCode;
	}

	public String getXnindName() {
		return xnindName;
	}

	public void setXnindName(String xnindName) {
		this.xnindName = xnindName;
	}
}