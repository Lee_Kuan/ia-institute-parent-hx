package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_report_album")
public class InsReportAlbum {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    @Column(name = "THIRD_ID")
    private String thirdId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "SUBTIT")
    private String subtit;

    @Column(name = "ALBUM_DESC")
    private String albumDesc;
    
    @Column(name = "COVER_LG_URL")
    private String coverLgUrl;

    @Column(name = "COVER_LG_NAME")
    private String coverLgName;

    @Column(name = "COVER_SM_URL")
    private String coverSmUrl;

    @Column(name = "COVER_SM_NAME")
    private String coverSmName;

    @Column(name = "CATALOG")
    private Integer catalog;

    @Column(name = "NO")
    private Integer no;

    @Column(name = "VISIBLE")
    private Integer visible; //0-不显示，1-显示

    @Column(name = "IS_TOP")
    private Integer isTop; //0-不置顶，1-置顶
    
    @Column(name = "SHARE")
    private Integer share;

    @Column(name = "CREATED_BY")
    private String createdBy;
    
    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "UPDATED_BY")
    private String updatedBy;
    
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    @Column(name = "STATUS")
    private Integer status;
    
    @Column(name = "UNDO_REASON")
    private String undoReason;
    
    @Column(name = "OPERATOR_UID")
    private String operatorUid;
    
    @Column(name = "OPERATOR_NAME")
    private String operatorName;
    
    @Column(name = "UNDO_TIME")
    private Date undoTime;
    
    @Column(name = "URL")
    private String url;
    
    @Column(name = "TYPE")
    private Integer type;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getThirdId() {
		return thirdId;
	}

	public void setThirdId(String thirdId) {
		this.thirdId = thirdId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtit() {
		return subtit;
	}

	public void setSubtit(String subtit) {
		this.subtit = subtit;
	}

	public String getAlbumDesc() {
		return albumDesc;
	}

	public void setAlbumDesc(String albumDesc) {
		this.albumDesc = albumDesc;
	}

	public String getCoverLgUrl() {
		return coverLgUrl;
	}

	public void setCoverLgUrl(String coverLgUrl) {
		this.coverLgUrl = coverLgUrl;
	}

	public String getCoverLgName() {
		return coverLgName;
	}

	public void setCoverLgName(String coverLgName) {
		this.coverLgName = coverLgName;
	}

	public String getCoverSmUrl() {
		return coverSmUrl;
	}

	public void setCoverSmUrl(String coverSmUrl) {
		this.coverSmUrl = coverSmUrl;
	}

	public String getCoverSmName() {
		return coverSmName;
	}

	public void setCoverSmName(String coverSmName) {
		this.coverSmName = coverSmName;
	}

	public Integer getCatalog() {
		return catalog;
	}

	public void setCatalog(Integer catalog) {
		this.catalog = catalog;
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public Integer getVisible() {
		return visible;
	}

	public void setVisible(Integer visible) {
		this.visible = visible;
	}

	public Integer getIsTop() {
		return isTop;
	}

	public void setIsTop(Integer isTop) {
		this.isTop = isTop;
	}

	
	public Integer getShare() {
		return share;
	}

	public void setShare(Integer share) {
		this.share = share;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUndoReason() {
		return undoReason;
	}

	public void setUndoReason(String undoReason) {
		this.undoReason = undoReason;
	}

	public String getOperatorUid() {
		return operatorUid;
	}

	public void setOperatorUid(String operatorUid) {
		this.operatorUid = operatorUid;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public Date getUndoTime() {
		return undoTime;
	}

	public void setUndoTime(Date undoTime) {
		this.undoTime = undoTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	
}