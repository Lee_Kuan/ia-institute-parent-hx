package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_user_label")
public class InsUserLabel {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 用户ID
     */
    @Column(name = "UID")
    private Long uid;

    /**
     * 标签类型: 0-作者 1-公司 2-股票  3-行业
     */
    @Column(name = "LABEL_TYPE")
    private Integer labelType;

    /**
     * 标签代码
     */
    @Column(name = "LABEL_CODE")
    private String labelCode;

    /**
     * 分数
     */
    @Column(name = "SCORE")
    private Float score;

    /**
     * 来源： 1 系统生成  2 手工输入
     */
    @Column(name = "CREATE_VIA")
    private Integer createVia;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取用户ID
     *
     * @return UID - 用户ID
     */
    public Long getUid() {
        return uid;
    }

    /**
     * 设置用户ID
     *
     * @param uid 用户ID
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * 获取标签类型: 0-作者 1-公司 2-股票  3-行业
     *
     * @return LABEL_TYPE - 标签类型: 0-作者 1-公司 2-股票  3-行业
     */
    public Integer getLabelType() {
        return labelType;
    }

    /**
     * 设置标签类型: 0-作者 1-公司 2-股票  3-行业
     *
     * @param labelType 标签类型: 0-作者 1-公司 2-股票  3-行业
     */
    public void setLabelType(Integer labelType) {
        this.labelType = labelType;
    }

    /**
     * 获取标签代码
     *
     * @return LABEL_CODE - 标签代码
     */
    public String getLabelCode() {
        return labelCode;
    }

    /**
     * 设置标签代码
     *
     * @param labelCode 标签代码
     */
    public void setLabelCode(String labelCode) {
        this.labelCode = labelCode == null ? null : labelCode.trim();
    }

    /**
     * 获取分数
     *
     * @return SCORE - 分数
     */
    public Float getScore() {
        return score;
    }

    /**
     * 设置分数
     *
     * @param score 分数
     */
    public void setScore(Float score) {
        this.score = score;
    }

    /**
     * 获取来源： 1 系统生成  2 手工输入
     *
     * @return CREATE_VIA - 来源： 1 系统生成  2 手工输入
     */
    public Integer getCreateVia() {
        return createVia;
    }

    /**
     * 设置来源： 1 系统生成  2 手工输入
     *
     * @param createVia 来源： 1 系统生成  2 手工输入
     */
    public void setCreateVia(Integer createVia) {
        this.createVia = createVia;
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }
}