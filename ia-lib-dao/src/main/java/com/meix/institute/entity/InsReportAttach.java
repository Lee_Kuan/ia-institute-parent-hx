package com.meix.institute.entity;

import javax.persistence.*;

@Table(name = "ia_report_attach")
public class InsReportAttach {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 研报ID
     */
    @Column(name = "RESEARCH_ID")
    private Long researchId;

    /**
     * 附件地址
     */
    @Column(name = "FILE_URL")
    private String fileUrl;

    @Column(name = "SOURCE_URL")
    private String sourceUrl;
    
    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取研报ID
     *
     * @return RESEARCH_ID - 研报ID
     */
    public Long getResearchId() {
        return researchId;
    }

    /**
     * 设置研报ID
     *
     * @param researchId 研报ID
     */
    public void setResearchId(Long researchId) {
        this.researchId = researchId;
    }

    /**
     * 获取附件地址
     *
     * @return FILE_URL - 附件地址
     */
    public String getFileUrl() {
        return fileUrl;
    }

    /**
     * 设置附件地址
     *
     * @param fileUrl 附件地址
     */
    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl == null ? null : fileUrl.trim();
    }

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
    
}