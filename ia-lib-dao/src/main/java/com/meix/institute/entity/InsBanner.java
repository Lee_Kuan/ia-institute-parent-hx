package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_banner")
public class InsBanner {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "RESOURCE_URL")
    private String resourceUrl;

    @Column(name = "RESOURCE_NAME")
    private String resourceName;

    @Column(name = "NO")
    private Integer no;

    @Column(name = "TYPE")
    private Integer type;

    @Column(name = "RESOURCE_ID")
    private Long resourceId;
    
    @Column(name = "COMMENT")
    private String comment;

    @Column(name = "CREATED_BY")
    private String createdBy;
    
    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "UPDATED_BY")
    private String updatedBy;
    
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}