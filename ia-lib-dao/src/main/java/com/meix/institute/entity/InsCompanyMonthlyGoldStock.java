package com.meix.institute.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_company_monthly_gold_stock")
public class InsCompanyMonthlyGoldStock {
    /**
     * 自增长ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 公司代码
     */
    @Column(name = "COMPANY_CODE")
    private Long companyCode;

    /**
     * 推荐人
     */
    @Column(name = "REC_PERSON")
    private String recPerson;

    /**
     * 推荐理由
     */
    @Column(name = "REC_DESC")
    private String recDesc;

    /**
     * 股票内码
     */
    @Column(name = "INNER_CODE")
    private Integer innerCode;

    /**
     * 月份 格式YYYY-MM
     */
    @Column(name = "MONTH")
    private String month;

    /**
     * 加入时价格
     */
    @Column(name = "ADD_PRICE")
    private BigDecimal addPrice;

    /**
     * 加入月底价格
     */
    @Column(name = "ADD_MONTH_PRICE")
    private BigDecimal addMonthPrice;

    /**
     * 现在价格
     */
    @Column(name = "NOW_PRICE")
    private BigDecimal nowPrice;
    
    /**
     * 首页下架标志: 1 上架  0 下架
     */
    @Column(name = "HOME_SHOW_FLAG")
    private Integer homeShowFlag;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /**
     * 获取自增长ID
     *
     * @return ID - 自增长ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置自增长ID
     *
     * @param id 自增长ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取公司代码
     *
     * @return COMPANY_CODE - 公司代码
     */
    public Long getCompanyCode() {
        return companyCode;
    }

    /**
     * 设置公司代码
     *
     * @param companyCode 公司代码
     */
    public void setCompanyCode(Long companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 获取推荐人
     *
     * @return REC_PERSON - 推荐人
     */
    public String getRecPerson() {
        return recPerson;
    }

    /**
     * 设置推荐人
     *
     * @param recPerson 推荐人
     */
    public void setRecPerson(String recPerson) {
        this.recPerson = recPerson == null ? null : recPerson.trim();
    }

    /**
     * 获取推荐理由
     *
     * @return REC_DESC - 推荐理由
     */
    public String getRecDesc() {
        return recDesc;
    }

    /**
     * 设置推荐理由
     *
     * @param recDesc 推荐理由
     */
    public void setRecDesc(String recDesc) {
        this.recDesc = recDesc == null ? null : recDesc.trim();
    }

    /**
     * 获取股票内码
     *
     * @return INNER_CODE - 股票内码
     */
    public Integer getInnerCode() {
        return innerCode;
    }

    /**
     * 设置股票内码
     *
     * @param innerCode 股票内码
     */
    public void setInnerCode(Integer innerCode) {
        this.innerCode = innerCode;
    }

    /**
     * 获取月份 格式YYYY-MM
     *
     * @return MONTH - 月份 格式YYYY-MM
     */
    public String getMonth() {
        return month;
    }

    /**
     * 设置月份 格式YYYY-MM
     *
     * @param month 月份 格式YYYY-MM
     */
    public void setMonth(String month) {
        this.month = month == null ? null : month.trim();
    }

    /**
     * 获取加入时价格
     *
     * @return ADD_PRICE - 加入时价格
     */
    public BigDecimal getAddPrice() {
        return addPrice;
    }

    /**
     * 设置加入时价格
     *
     * @param addPrice 加入时价格
     */
    public void setAddPrice(BigDecimal addPrice) {
        this.addPrice = addPrice;
    }

    /**
     * 获取加入月底价格
     *
     * @return ADD_MONTH_PRICE - 加入月底价格
     */
    public BigDecimal getAddMonthPrice() {
        return addMonthPrice;
    }

    /**
     * 设置加入月底价格
     *
     * @param addMonthPrice 加入月底价格
     */
    public void setAddMonthPrice(BigDecimal addMonthPrice) {
        this.addMonthPrice = addMonthPrice;
    }

    /**
     * 获取现在价格
     *
     * @return NOW_PRICE - 现在价格
     */
    public BigDecimal getNowPrice() {
        return nowPrice;
    }

    /**
     * 设置现在价格
     *
     * @param nowPrice 现在价格
     */
    public void setNowPrice(BigDecimal nowPrice) {
        this.nowPrice = nowPrice;
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }

    public Integer getHomeShowFlag() {
        return homeShowFlag;
    }

    public void setHomeShowFlag(Integer homeShowFlag) {
        this.homeShowFlag = homeShowFlag;
    }
}