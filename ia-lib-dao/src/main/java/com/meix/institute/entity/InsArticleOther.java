package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_article_other")
public class InsArticleOther {
    /**
     * 流水号
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
     */
    @Column(name = "TYPE")
    private String type;

    /**
     * 状态：  -1 删除 0 未删除
     */
    @Column(name = "STATE")
    private Integer state;

    /**
     * 标题
     */
    @Column(name = "TITLE")
    private String title;

    /**
     * 介绍
     */
    @Column(name = "INTRODUCTION")
    private String introduction;

    /**
     * 服务器本地地址
     */
    @Column(name = "LOCAL_URL")
    private String localUrl;
    
    /**
     * 腾讯远程地址
     */
    @Column(name = "REMOTE_URL")
    private String remoteUrl;

    /**
     * 微信素材Id
     */
    @Column(name = "MEDIA_ID")
    private String mediaId;

    /**
     * 创建时间
     */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /**
     * 创建人
     */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /**
     * 更新时间
     */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    /**
     * 更新人
     */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /**
     * 获取流水号
     *
     * @return ID - 流水号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置流水号
     *
     * @param id 流水号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
     *
     * @return TYPE - 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
     */
    public String getType() {
        return type;
    }

    /**
     * 设置媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
     *
     * @param type 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取状态：  -1 删除 0 未删除
     *
     * @return STATE - 状态：  -1 删除 0 未删除
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态：  -1 删除 0 未删除
     *
     * @param state 状态：  -1 删除 0 未删除
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取标题
     *
     * @return TITLE - 标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 获取介绍
     *
     * @return INTRODUCTION - 介绍
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 设置介绍
     *
     * @param introduction 介绍
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction == null ? null : introduction.trim();
    }

    /**
     * 获取服务器本地地址
     *
     * @return LOCAL_URL - 服务器本地地址
     */
    public String getLocalUrl() {
        return localUrl;
    }

    /**
     * 设置服务器本地地址
     *
     * @param localUrl 服务器本地地址
     */
    public void setLocalUrl(String localUrl) {
        this.localUrl = localUrl == null ? null : localUrl.trim();
    }
    
    public String getRemoteUrl() {
        return remoteUrl;
    }

    public void setRemoteUrl(String remoteUrl) {
        this.remoteUrl = remoteUrl;
    }

    /**
     * 获取微信素材Id
     *
     * @return MEDIA_ID - 微信素材Id
     */
    public String getMediaId() {
        return mediaId;
    }

    /**
     * 设置微信素材Id
     *
     * @param mediaId 微信素材Id
     */
    public void setMediaId(String mediaId) {
        this.mediaId = mediaId == null ? null : mediaId.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }
}