package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_activity_join")
public class InsActivityJoin {
    /**
     * 自增长ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 活动ID
     */
    @Column(name = "ACTIVITY_ID")
    private Long activityId;

    /**
     * 用户ID
     */
    @Column(name = "UID")
    private Long uid;

    /**
     * 参加时间
     */
    @Column(name = "JOIN_AT")
    private Date joinAt;

    /**
     * 获取自增长ID
     *
     * @return ID - 自增长ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置自增长ID
     *
     * @param id 自增长ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取活动ID
     *
     * @return ACTIVITY_ID - 活动ID
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * 设置活动ID
     *
     * @param activityId 活动ID
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * 获取用户ID
     *
     * @return UID - 用户ID
     */
    public Long getUid() {
        return uid;
    }

    /**
     * 设置用户ID
     *
     * @param uid 用户ID
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * 获取参加时间
     *
     * @return JOIN_AT - 参加时间
     */
    public Date getJoinAt() {
        return joinAt;
    }

    /**
     * 设置参加时间
     *
     * @param joinAt 参加时间
     */
    public void setJoinAt(Date joinAt) {
        this.joinAt = joinAt;
    }
}