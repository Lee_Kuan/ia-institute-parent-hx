package com.meix.institute.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "ia_sec_token")
public class InsSecToken {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    @Column(name = "ACCESS_TOKEN")
    private String accessToken;

    @Column(name = "EXPIRES_AT")
    private Date expiresAt;

    @Column(name = "USER")
    private String user;

    /**
     * 删除标志： 0  未删除  1 删除
     */
    @Column(name = "IS_DELETED")
    private Integer isDeleted;
    
    /**
     * APP类型： 1-管理后台  2-公众号 3-小程序
     */
    @Column(name = "CLIENT_TYPE")
    private Integer clientType;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    @Column(name = "UPDATED_BY")
    private String updatedBy;
    
    @Column(name = "LTPA_TOKEN")
    private String ltpaToken;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return ACCESS_TOKEN
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * @param accessToken
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken == null ? null : accessToken.trim();
    }

    /**
     * @return EXPIRES_AT
     */
    public Date getExpiresAt() {
        return expiresAt;
    }

    /**
     * @param expiresAt
     */
    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    /**
     * @return USER
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user
     */
    public void setUser(String user) {
        this.user = user == null ? null : user.trim();
    }

    /**
     * 获取删除标志： 0  未删除  1 删除
     *
     * @return IS_DELETE - 删除标志： 0  未删除  1 删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置删除标志： 0  未删除  1 删除
     *
     * @param isDelete 删除标志： 0  未删除  1 删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * @return CREATED_AT
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return CREATED_BY
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * @return UPDATED_AT
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return UPDATED_BY
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }

    public Integer getClientType() {
        return clientType;
    }

    public void setClientType(Integer clientType) {
        this.clientType = clientType;
    }

	public String getLtpaToken() {
		return ltpaToken;
	}

	public void setLtpaToken(String ltpaToken) {
		this.ltpaToken = ltpaToken;
	}
    
}