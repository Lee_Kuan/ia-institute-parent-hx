package com.meix.institute;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * Mapper基类，不能被当作mapper扫描（单独放一个目录）
 * Created by zenghao on 2019/7/24.
 */
public interface MeixBaseMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
