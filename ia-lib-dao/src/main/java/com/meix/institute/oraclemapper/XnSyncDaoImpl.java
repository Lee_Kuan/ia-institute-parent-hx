package com.meix.institute.oraclemapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.meix.institute.api.XnSyncDao;
import com.meix.institute.pojo.xn.XnDirection;
import com.meix.institute.pojo.xn.XnReportVo;
import com.meix.institute.pojo.xn.XnStationInfo;
import com.meix.institute.pojo.xn.XnUserVo;

@Repository
public class XnSyncDaoImpl implements XnSyncDao {

	private static final String XN_NAMESPACE = "xn.";

	@Autowired
	@Qualifier("oracleSqlSessionTemplate")
	private SqlSessionTemplate oracleSqlSessionTemplate;

	@Override
	public List<XnUserVo> getInnerUser(String updateTimeFm, String updateTimeTo, Boolean isSyncAll, int page, int pageSize) {
		Map<String, Object> params = new HashMap<>();
		params.put("updateTimeFm", updateTimeFm);
		params.put("updateTimeTo", updateTimeTo);
		params.put("isSyncAll", isSyncAll);
		params.put("pageFm",  page * pageSize);
		params.put("pageTo", (page + 1 ) * pageSize);
		return oracleSqlSessionTemplate.selectList(XN_NAMESPACE + "getInnerUser", params);
	}

	@Override
	public List<XnStationInfo> getStation(int page, int pageSize) {
	    Map<String, Object> params = new HashMap<>();
        params.put("pageFm",  page * pageSize);
        params.put("pageTo", (page +1 ) * pageSize);
		return oracleSqlSessionTemplate.selectList(XN_NAMESPACE + "getStation",  params);
	}

	@Override
	public int countInnerUser(String updateTimeFm, String updateTimeTo, Boolean isSyncAll) {
		Map<String, Object> params = new HashMap<>();
		params.put("updateTimeFm", updateTimeFm);
		params.put("updateTimeTo", updateTimeTo);
		params.put("isSyncAll", isSyncAll);
		return oracleSqlSessionTemplate.selectOne(XN_NAMESPACE + "countInnerUser", params);
	}

	@Override
	public List<XnReportVo> getBgReportList(String updateTimeFm, String updateTimeTo, Boolean isSyncAll, int page, int pageSize) {
        Map<String, Object> params = new HashMap<>();
        params.put("updateTime", updateTimeFm);
        params.put("updateTime", updateTimeTo);
        params.put("isSyncAll", isSyncAll);
        params.put("pageFm", page * pageSize);
        params.put("pageTo", (page +1 ) * pageSize);
		return oracleSqlSessionTemplate.selectList(XN_NAMESPACE + "getXnReportList", params);
	}

    @Override
    public List<XnDirection> getDirection() {
        return oracleSqlSessionTemplate.selectList(XN_NAMESPACE + "getDirection");
    }

    @Override
    public int countStation() {
        return oracleSqlSessionTemplate.selectOne(XN_NAMESPACE + "countStation");
    }

    @Override
    public int countBgReport(String updateTimeFm, String updateTimeTo, Boolean isSyncAll) {
        Map<String, Object> params = new HashMap<>();
        params.put("updateTimeFm", updateTimeFm);
        params.put("updateTimeTo", updateTimeTo);
        params.put("isSyncAll", isSyncAll);
        return oracleSqlSessionTemplate.selectOne(XN_NAMESPACE + "countXnReport", params);
    }

	@Override
	public Map<String, Object> getReportFile(String fileId) {
		Map<String, Object> params = new HashMap<>();
        params.put("objid", Long.valueOf(fileId));
        return oracleSqlSessionTemplate.selectOne(XN_NAMESPACE + "getReportFile", params);
	}

}
