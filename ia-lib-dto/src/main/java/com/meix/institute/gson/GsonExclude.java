package com.meix.institute.gson;

/**
 * Created by zenghao on 2019/8/27.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 序列化时 用于排除 属性
 * @GsonExclude 在序列化时 要比  @Expose 这个注解 好用
 * 无需每个属性都加 ， 只需在排除的属性加即可
 *
 * @author sunRainAmazing
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface GsonExclude {
}
