package com.meix.institute.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * 排除策略
 * 基于策略是利用Gson提供的ExclusionStrategy接口，同样需要使用GsonBuilder,
 * 相关API 2个，分别是
 * addSerializationExclusionStrategy()
 * addDeserializationExclusionStrategy() 分别针对序列化和反序化时。
 * ————————————————
 * 版权声明：本文为CSDN博主「sunRainAmazing」的原创文章，遵循CC 4.0 by-sa版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/sunrainamazing/article/details/80975245
 * Created by zenghao on 2019/8/27.
 */
public class GsonExclusionStrategy implements ExclusionStrategy {
	/**
	 * 是否跳过属性 不序列化
	 * 返回 false 代表 属性要进行序列化
	 *
	 * @param f
	 * @return
	 */
	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		/**
		 * 判断当前属性是否带有GsonExclude排除的注解
		 * 若有 则 不进行序列化
		 * 若无 为 null 则进行序列化
		 */
		return f.getAnnotation(GsonExclude.class) != null;
	}

	/**
	 * 是否排除对应的类
	 * 同时这里也可以 排除 int 类型的属性 不进行序列化
	 * <p>
	 * 若不排除任何 类  直接 返回false 即可
	 *
	 * @param clazz
	 * @return
	 */
	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
//		return clazz == int.class;
		return false;
	}
}
