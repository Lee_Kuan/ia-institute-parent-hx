package com.meix.institute.search;

public class ComplianceCommentSearchVo extends ComplianceListSearchVo{

	private static final long serialVersionUID = 8799390399559344893L;
	private long dataId;
	public long getDataId() {
		return dataId;
	}
	public void setDataId(long dataId) {
		this.dataId = dataId;
	}
}
