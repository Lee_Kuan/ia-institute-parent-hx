package com.meix.institute.search;

/**
 * Created by zenghao on 2019/8/2.
 */
public class ClueSearchVo extends BaseSearchVo {
	private static final long serialVersionUID = -4778647664170591736L;

	private long companyCode;
	private long uid;
	private String condition;
	private int issueType = -1;
	private int clueType = -1;
	private int customerType = -1;
	private int sortType;//排序类型 1访问次数 2浏览时长 3分享次数 4发布时间
	private int sortRule;//排序规则 1 ASC -1 DESC
	private long groupId;
	private int isExcept;
	
	public int getIsExcept() {
		return isExcept;
	}

	public void setIsExcept(int isExcept) {
		this.isExcept = isExcept;
	}

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public int getIssueType() {
		return issueType;
	}

	public void setIssueType(int issueType) {
		this.issueType = issueType;
	}

	public int getClueType() {
		return clueType;
	}

	public void setClueType(int clueType) {
		this.clueType = clueType;
	}

	public int getCustomerType() {
		return customerType;
	}

	public void setCustomerType(int customerType) {
		this.customerType = customerType;
	}

	public int getSortType() {
		return sortType;
	}

	public void setSortType(int sortType) {
		this.sortType = sortType;
	}

	public int getSortRule() {
		return sortRule;
	}

	public void setSortRule(int sortRule) {
		this.sortRule = sortRule;
	}
}
