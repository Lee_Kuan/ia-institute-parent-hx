package com.meix.institute.search;

import java.io.Serializable;

/**
 * Created by zenghao on 2019/7/31.
 */
public class BaseSearchVo implements Serializable {
	private static final long serialVersionUID = 3535022026120026620L;

	private int showNum;
	private int currentPage;

	public int getShowNum() {
		return showNum;
	}

	public void setShowNum(int showNum) {
		this.showNum = showNum;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
}
