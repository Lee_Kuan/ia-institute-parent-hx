package com.meix.institute.search;

/**
 * Created by zenghao on 2019/8/2.
 */
public class UserRecordSearchVo extends BaseSearchVo {
	private static final long serialVersionUID = -4511277046403083052L;

	private int dataType;
	private long dataId;

	private int sortType;
	private int sortRule;
	
	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public int getSortType() {
		return sortType;
	}

	public void setSortType(int sortType) {
		this.sortType = sortType;
	}

	public int getSortRule() {
		return sortRule;
	}

	public void setSortRule(int sortRule) {
		this.sortRule = sortRule;
	}
	
}
