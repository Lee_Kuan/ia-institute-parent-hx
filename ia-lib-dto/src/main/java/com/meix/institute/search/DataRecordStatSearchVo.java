package com.meix.institute.search;

import java.io.Serializable;

/**
 * Created by zenghao on 2019/8/2.
 */
public class DataRecordStatSearchVo extends BaseSearchVo implements Serializable {
	private static final long serialVersionUID = -8973172855405489227L;

	private String condition;
	private int dataType;
	private int activityType;
	private String startTime;
	private String endTime;
	private long companyCode;

	private int sortType;//排序类型 1访问次数 2浏览时长 3分享次数 4发布时间
	private int sortRule;//排序规则 1 ASC -1 DESC

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public int getActivityType() {
		return activityType;
	}

	public void setActivityType(int activityType) {
		this.activityType = activityType;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public int getSortType() {
		return sortType;
	}

	public void setSortType(int sortType) {
		this.sortType = sortType;
	}

	public int getSortRule() {
		return sortRule;
	}

	public void setSortRule(int sortRule) {
		this.sortRule = sortRule;
	}
}
