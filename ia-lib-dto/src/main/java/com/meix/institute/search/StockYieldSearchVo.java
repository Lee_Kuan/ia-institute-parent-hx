package com.meix.institute.search;

import java.io.Serializable;

/**
 * 查询股票收益
 * Created by zenghao on 2019/10/8.
 */
public class StockYieldSearchVo implements Serializable {
	private static final long serialVersionUID = 6243670845336126425L;

	private int innerCode;
	private String startDate;
	private String endDate;

	public int getInnerCode() {
		return innerCode;
	}

	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
