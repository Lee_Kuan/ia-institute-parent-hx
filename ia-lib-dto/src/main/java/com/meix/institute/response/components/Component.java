package com.meix.institute.response.components;

import java.io.Serializable;

/**
 * @Auther: Administrator
 * @Date: 2018/7/17 0017 16:07
 * @Description:
 */
public abstract class Component implements Serializable {

	private static final long serialVersionUID = 3184665625813376153L;

	private long id;
	private int type;
	private long dataId;
	private int dataType;//-99 用来标记这是存放lastId的组件，用完之后需要从列表删除

	private String title;//标题
	private String content; //内容

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

}
