package com.meix.institute.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户消息
 * 聊天信息
 */
public class MessagePo implements Serializable{

	private static final long serialVersionUID = 3566446504802110405L;
	private Long id;//消息ID
	private Long sender;//发送人
	private String senderName;//发送人姓名
	private String senderImageUrl;//发送人头像
	private String senderCompany;//发送者公司
	private Long themeId; //主题ID
	private Integer themeType; //主题类型
	private String themeTime;
	private String message;//消息内容
	private Integer messageStatus;//消息状态
	private int isDeleted;//删除标志 int 1删除 0未删除
	private String createTime;
	private String resourceUrl="";
	private int messageType;//消息类型
	private int mediaTime;//多媒体时长
	private long pointGroupId;//观点组ID
	private int evaluateState;//买入  卖出
	private String evaluateDesc;//评价描述
	private String themeContent;//主题内容
	private BigDecimal startPosition;//指令起始仓位 
	private BigDecimal endPosition;
	private BigDecimal settleStartPosition;//成交起始仓位
	private BigDecimal settleEndPosition;
	private String title;//标题
	private String subTitle;//子标题
	@SuppressWarnings("rawtypes")
	private List<Map> secuList=new ArrayList<>();
	private Map<String,Object> comb=new HashMap<String, Object>();//模拟组合信息
	private Map<String,Object> share=new HashMap<String, Object>();//分享信息
	private String groupName;//群组名（使用）
	private Map<String,Object> activity=new HashMap<String, Object>();//活动信息
	private Map<String,Object> system=new HashMap<String, Object>();//系统信息
	private Map<String,Object> p=new HashMap<String, Object>();//观点信息
	private Map<String,Object> file=new HashMap<String, Object>();//文件信息
	private Map<String,Object> image=new HashMap<String, Object>();//图片信息
	private Map<String,Object> service = new HashMap<String, Object>();//研究员服务信息
	private String iconUrl = "";
	private String linkUrl = "";
	private String weixinShare = "";//web端推送消息时使用
	
	public Map<String, Object> getService() {
		return service;
	}
	public void setService(Map<String, Object> service) {
		this.service = service;
	}
	public Map<String, Object> getImage() {
		return image;
	}
	public void setImage(Map<String, Object> image) {
		this.image = image;
	}
	public Map<String, Object> getFile() {
		return file;
	}
	public void setFile(Map<String, Object> file) {
		this.file = file;
	}
	
	public Map<String, Object> getP() {
		return p;
	}
	public void setP(Map<String, Object> p) {
		this.p = p;
	}
	public Map<String, Object> getActivity() {
		return activity;
	}
	public void setActivity(Map<String, Object> activity) {
		this.activity = activity;
	}
	public Map<String, Object> getSystem() {
		return system;
	}
	public void setSystem(Map<String, Object> system) {
		this.system = system;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getSubTitle() {
		return subTitle;
	}
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	public String getSenderCompany() {
		return senderCompany;
	}
	public void setSenderCompany(String senderCompany) {
		this.senderCompany = senderCompany;
	}
	public Map<String, Object> getShare() {
		return share;
	}
	public void setShare(Map<String, Object> share) {
		this.share = share;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSender() {
		return sender;
	}
	public void setSender(Long sender) {
		this.sender = sender;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getSenderImageUrl() {
		return senderImageUrl;
	}
	public void setSenderImageUrl(String senderImageUrl) {
		this.senderImageUrl = senderImageUrl;
	}
	public Long getThemeId() {
		return themeId;
	}
	public void setThemeId(Long themeId) {
		this.themeId = themeId;
	}
	public Integer getThemeType() {
		return themeType;
	}
	public void setThemeType(Integer themeType) {
		this.themeType = themeType;
	}
	public String getThemeTime() {
		return themeTime;
	}
	public void setThemeTime(String themeTime) {
		this.themeTime = themeTime;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getMessageStatus() {
		return messageStatus;
	}
	public void setMessageStatus(Integer messageStatus) {
		this.messageStatus = messageStatus;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getResourceUrl() {
		return resourceUrl;
	}
	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}
	public int getMessageType() {
		return messageType;
	}
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}
	public int getMediaTime() {
		return mediaTime;
	}
	public void setMediaTime(int mediaTime) {
		this.mediaTime = mediaTime;
	}
	public long getPointGroupId() {
		return pointGroupId;
	}
	public void setPointGroupId(long pointGroupId) {
		this.pointGroupId = pointGroupId;
	}
	public int getEvaluateState() {
		return evaluateState;
	}
	public void setEvaluateState(int evaluateState) {
		this.evaluateState = evaluateState;
	}
	public String getEvaluateDesc() {
		return evaluateDesc;
	}
	public void setEvaluateDesc(String evaluateDesc) {
		this.evaluateDesc = evaluateDesc;
	}
	public String getThemeContent() {
		return themeContent;
	}
	public void setThemeContent(String themeContent) {
		this.themeContent = themeContent;
	}
	public BigDecimal getStartPosition() {
		return startPosition;
	}
	public void setStartPosition(BigDecimal startPosition) {
		this.startPosition = startPosition;
	}
	public BigDecimal getEndPosition() {
		return endPosition;
	}
	public void setEndPosition(BigDecimal endPosition) {
		this.endPosition = endPosition;
	}
	public BigDecimal getSettleStartPosition() {
		return settleStartPosition;
	}
	public void setSettleStartPosition(BigDecimal settleStartPosition) {
		this.settleStartPosition = settleStartPosition;
	}
	public BigDecimal getSettleEndPosition() {
		return settleEndPosition;
	}
	public void setSettleEndPosition(BigDecimal settleEndPosition) {
		this.settleEndPosition = settleEndPosition;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@SuppressWarnings("rawtypes")
	public List<Map> getSecuList() {
		return secuList;
	}
	@SuppressWarnings("rawtypes")
	public void setSecuList(List<Map> secuList) {
		this.secuList = secuList;
	}
	public Map<String, Object> getComb() {
		return comb;
	}
	public void setComb(Map<String, Object> comb) {
		this.comb = comb;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getLinkUrl() {
		return linkUrl;
	}
	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}
	public String getWeixinShare() {
		return weixinShare;
	}
	public void setWeixinShare(String weixinShare) {
		this.weixinShare = weixinShare;
	}
}
