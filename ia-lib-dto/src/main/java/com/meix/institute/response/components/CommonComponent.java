package com.meix.institute.response.components;

import com.meix.institute.vo.BaseVo;

/**
 * 瀑布流组件
 *
 * @author zz
 */
public class CommonComponent extends Component {
	private static final long serialVersionUID = 8259984611724511851L;
	public String from;
	public String publishTime;
	public String resourceUrl;
	BaseVo item;

	long queueId = 0;

	private double recScore;

	private int waterflowRecType; //推荐类型

	public int getWaterflowRecType() {
		return waterflowRecType;
	}

	public void setWaterflowRecType(int waterflowRecType) {
		this.waterflowRecType = waterflowRecType;
	}

	public BaseVo getItem() {
		return item;
	}

	public void setItem(BaseVo item) {
		this.item = item;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	public long getQueueId() {
		return queueId;
	}

	public void setQueueId(long queueId) {
		this.queueId = queueId;
	}

	public double getRecScore() {
		return recScore;
	}

	public void setRecScore(double recScore) {
		this.recScore = recScore;
	}

}
