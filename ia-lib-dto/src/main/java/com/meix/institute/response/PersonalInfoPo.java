package com.meix.institute.response;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @Description:个人信息
 */
public class PersonalInfoPo implements Serializable {
	private static final long serialVersionUID = 1L;

	public PersonalInfoPo() {
		super();
	}
	
	public PersonalInfoPo(int permission) {
		super();
		this.permission = permission;
	}
	private long authorCode;
	private String authorName;
	private String telephone;
	private String comment;
	private String authorHeadImgUrl;
	private String createTime;
	private int followNum;//关注我的人数(粉丝数)
	private long subscribeNum;//关注人数
	private int readNum;
	private int followFlag;
	private long orgCode;
	private String orgName;
	private String authorCardUrl;//名片链接
	private int accountType;//账户类型 int 1 机构账户  2 非机构账户 3第三方账户
	private int authStatus; // 认证状态
	private String reason; // 失败原因

	private List<Map<String, Object>> label; //标签列表

	private String position;// 职位
	private String email; // 邮件
	private String companyAbbr; // 公司描述
	private String direction;//分析师研究方向
	private int identify;
	private int permission;
	
	public int getPermission() {
		return permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}

	public int getIdentify() {
		return identify;
	}

	public void setIdentify(int identify) {
		this.identify = identify;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCompanyAbbr() {
		return companyAbbr;
	}

	public void setCompanyAbbr(String companyAbbr) {
		this.companyAbbr = companyAbbr;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAuthStatus() {
		return authStatus;
	}

	public void setAuthStatus(int authStatus) {
		this.authStatus = authStatus;
	}

	public List<Map<String, Object>> getLabel() {
		return label;
	}

	public void setLabel(List<Map<String, Object>> label) {
		this.label = label;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public String getAuthorCardUrl() {
		return authorCardUrl;
	}

	public void setAuthorCardUrl(String authorCardUrl) {
		this.authorCardUrl = authorCardUrl;
	}

	public long getAuthorCode() {
		return authorCode;
	}

	public void setAuthorCode(long authorCode) {
		this.authorCode = authorCode;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getAuthorHeadImgUrl() {
		return authorHeadImgUrl;
	}

	public void setAuthorHeadImgUrl(String authorHeadImgUrl) {
		this.authorHeadImgUrl = authorHeadImgUrl;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public int getFollowNum() {
		return followNum;
	}

	public void setFollowNum(int followNum) {
		this.followNum = followNum;
	}

	public long getSubscribeNum() {
		return subscribeNum;
	}

	public void setSubscribeNum(long subscribeNum) {
		this.subscribeNum = subscribeNum;
	}

	public int getReadNum() {
		return readNum;
	}

	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}

	public int getFollowFlag() {
		return followFlag;
	}

	public void setFollowFlag(int followFlag) {
		this.followFlag = followFlag;
	}

	public long getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(long orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}
}
