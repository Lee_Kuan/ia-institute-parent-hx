package com.meix.institute.response;


import com.meix.institute.vo.BaseVo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @作者 :zhouwei
 * @E-mail : zhouwei@51research.com
 * @创建日期 : 2015年3月9日
 * @Description:股票相关数据对象
 */
public class StockRelationDataPo extends BaseVo implements Serializable {
	private static final long serialVersionUID = 7778841656088115494L;
	private long id;//
	private String reportId;//
	private int innerCode;
	private String secuCode;
	private String secuAbbr;
	private String message;
	private String evaluateDesc;
	private String infoDate;
	private int readNum;
	private long authorCode;
	private String authorName;
	private String authorHeadImgUrl;
	private long orgCode;
	private String orgName;
	private int followFlag; // 关注（订阅） 0 未关注  1 关注
	private String content;//观点与调仓的理由描述，研报的摘要前200字
	private String title = "";//观点标题
	private Map<String, Object> report;//研报展示信息
	private long companyCode;//每市机构代码
	private String ybfl;//研报分类
	private List<Integer> ybsx;//研报属性
	private List<Integer> yjdx;//研究对象
	private List<String> yjfl;//一级分类
	private String infoSubType;//二级分类
	private String resourceId;
	private BigDecimal accumulatedYieldRate;//累计收益率
	private String picture;

	public List<String> getYjfl() {
		return yjfl;
	}

	public void setYjfl(List<String> yjfl) {
		this.yjfl = yjfl;
	}

	public String getInfoSubType() {
		return infoSubType;
	}

	public void setInfoSubType(String infoSubType) {
		this.infoSubType = infoSubType;
	}

	public BigDecimal getAccumulatedYieldRate() {
		return accumulatedYieldRate;
	}

	public void setAccumulatedYieldRate(BigDecimal accumulatedYieldRate) {
		this.accumulatedYieldRate = accumulatedYieldRate;
	}

	public int getFollowFlag() {
		return followFlag;
	}

	public void setFollowFlag(int followFlag) {
		this.followFlag = followFlag;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public Map<String, Object> getReport() {
		return report;
	}

	public void setReport(Map<String, Object> report) {
		this.report = report;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
		this.reportId = String.valueOf(id);
	}

	public int getInnerCode() {
		return innerCode;
	}

	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}

	public String getSecuCode() {
		return secuCode;
	}

	public void setSecuCode(String secuCode) {
		this.secuCode = secuCode;
	}

	public String getSecuAbbr() {
		return secuAbbr;
	}

	public void setSecuAbbr(String secuAbbr) {
		this.secuAbbr = secuAbbr;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEvaluateDesc() {
		return evaluateDesc;
	}

	public void setEvaluateDesc(String evaluateDesc) {
		this.evaluateDesc = evaluateDesc;
	}

	public String getInfoDate() {
		return infoDate;
	}

	public void setInfoDate(String infoDate) {
		this.infoDate = infoDate;
	}

	public int getReadNum() {
		return readNum;
	}

	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}

	public long getAuthorCode() {
		return authorCode;
	}

	public void setAuthorCode(long authorCode) {
		this.authorCode = authorCode;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getAuthorHeadImgUrl() {
		return authorHeadImgUrl;
	}

	public void setAuthorHeadImgUrl(String authorHeadImgUrl) {
		this.authorHeadImgUrl = authorHeadImgUrl;
	}

	public long getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(long orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getYbfl() {
		return ybfl;
	}

	public void setYbfl(String ybfl) {
		this.ybfl = ybfl;
	}

	public List<Integer> getYbsx() {
		return ybsx;
	}

	public void setYbsx(List<Integer> ybsx) {
		this.ybsx = ybsx;
	}

	public List<Integer> getYjdx() {
		return yjdx;
	}

	public void setYjdx(List<Integer> yjdx) {
		this.yjdx = yjdx;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
}
