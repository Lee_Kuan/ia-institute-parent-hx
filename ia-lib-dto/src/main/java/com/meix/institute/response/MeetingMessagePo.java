package com.meix.institute.response;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MeetingMessagePo implements Serializable {
	private static final long serialVersionUID = 6165368912747432543L;

	private Long id;//消息ID
	private Long sender;//发送人
	private int senderType;//0 普通参与者 1 主持人 2 嘉宾
	private String senderName;//发送人姓名
	private String senderImageUrl;//发送人头像
	private String senderCompany;//发送者公司
	private String message;//消息内容
	private int isDeleted;//删除标志 int 1删除 0未删除
	private String createTime;//创建时间string yyyy-MM-dd HH:mm:ss
	private int messageType;//消息类型  1 文本 2语音 9 图片
	private String resourceUrl="";
	private Map<String,Object> image=new HashMap<String, Object>();//图片信息
	private int mediaTime;//多媒体时长 单位：秒
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSender() {
		return sender;
	}
	public void setSender(Long sender) {
		this.sender = sender;
	}
	public int getSenderType() {
		return senderType;
	}
	public void setSenderType(int senderType) {
		this.senderType = senderType;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getSenderImageUrl() {
		return senderImageUrl;
	}
	public void setSenderImageUrl(String senderImageUrl) {
		this.senderImageUrl = senderImageUrl;
	}
	public String getSenderCompany() {
		return senderCompany;
	}
	public void setSenderCompany(String senderCompany) {
		this.senderCompany = senderCompany;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public int getMessageType() {
		return messageType;
	}
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}
	public String getResourceUrl() {
		return resourceUrl;
	}
	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}
	public Map<String, Object> getImage() {
		return image;
	}
	public void setImage(Map<String, Object> image) {
		this.image = image;
	}
	public int getMediaTime() {
		return mediaTime;
	}
	public void setMediaTime(int mediaTime) {
		this.mediaTime = mediaTime;
	}
}
