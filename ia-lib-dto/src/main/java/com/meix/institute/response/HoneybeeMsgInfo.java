package com.meix.institute.response;

import com.meix.institute.response.components.CommonComponent;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zenghao on 2018/10/12.
 */
public class HoneybeeMsgInfo implements Serializable {
	private static final long serialVersionUID = -793877685538666317L;


	private List<CommonComponent> data;// 服务端返回数据
	private Object object;// 存储单个对象

	public List<CommonComponent> getData() {
		return data;
	}

	public void setData(List<CommonComponent> data) {
		this.data = data;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
