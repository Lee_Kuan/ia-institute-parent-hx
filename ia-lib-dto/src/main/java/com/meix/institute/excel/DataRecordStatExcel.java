package com.meix.institute.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * Created by zenghao on 2019/8/2.
 */
public class DataRecordStatExcel implements Serializable {
	private static final long serialVersionUID = 1886040673434522466L;

	@Excel(name = "类型", orderNum = "1", width = 20, height = 8)
	private String dataType;
	@Excel(name = "标题", orderNum = "2", width = 60, height = 8)
	private String title;
	@Excel(name = "作者", orderNum = "3", width = 20, height = 8)
	private String authorName;
	@Excel(name = "访问次数", orderNum = "4", width = 20, height = 8)
	private String readNum;
	@Excel(name = "浏览时长", orderNum = "5", width = 20, height = 8)
	private String duration;//浏览时长，单位秒
	@Excel(name = "分享次数", orderNum = "6", width = 20, height = 8)
	private String shareNum;
	@Excel(name = "发布时间", orderNum = "7", width = 30, height = 8)
	private String startTime;

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getReadNum() {
		return readNum;
	}

	public void setReadNum(String readNum) {
		this.readNum = readNum;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getShareNum() {
		return shareNum;
	}

	public void setShareNum(String shareNum) {
		this.shareNum = shareNum;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
}
