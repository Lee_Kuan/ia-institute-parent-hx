package com.meix.institute.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * likuan	2019年9月21日10:46:20
 */
public class ExportComment implements Serializable {
	private static final long serialVersionUID = 3340335132858746154L;
	
	@Excel(name = "评论内容", orderNum = "1", width = 20, height = 8)
	private String comment;
	@Excel(name = "评论人", orderNum = "2", width = 20, height = 8)
	private String userName;
	@Excel(name = "评论时间", orderNum = "3", width = 20, height = 8)
	private String time;
	@Excel(name = "审核状态", orderNum = "4", width = 30, height = 8)
	private String status;
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
