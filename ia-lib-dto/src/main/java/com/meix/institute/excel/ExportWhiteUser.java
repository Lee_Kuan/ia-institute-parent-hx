package com.meix.institute.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * likuan	2019年9月21日10:46:20
 */
public class ExportWhiteUser implements Serializable {
	private static final long serialVersionUID = 3340335132858746154L;
	
	@Excel(name = "姓名", orderNum = "1", width = 20, height = 8)
	private String userName;
	@Excel(name = "手机号", orderNum = "2", width = 20, height = 8)
	private String mobile;
	@Excel(name = "机构", orderNum = "3", width = 20, height = 8)
	private String orgName;
	@Excel(name = "职位", orderNum = "4", width = 30, height = 8)
	private String position;
	@Excel(name = "对口销售", orderNum = "5", width = 20, height = 8)
	private String salerName;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getSalerName() {
		return salerName;
	}
	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}
	
}
