package com.meix.institute.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * Created by zenghao on 2019/8/2.
 */
public class UserRecordStatExcel implements Serializable {
	private static final long serialVersionUID = -4438924499234362562L;

	@Excel(name = "姓名", orderNum = "1", width = 20, height = 8)
	private String userName;
	@Excel(name = "手机号", orderNum = "2", width = 30, height = 8)
	private String mobile;
	@Excel(name = "机构", orderNum = "3", width = 20, height = 8)
	private String companyAbbr;
	@Excel(name = "职位", orderNum = "4", width = 20, height = 8)
	private String position;
	@Excel(name = "访问次数", orderNum = "5", width = 20, height = 8)
	private String readNum;
	@Excel(name = "浏览时长", orderNum = "6", width = 20, height = 8)
	private String duration;//浏览时长，单位秒
	@Excel(name = "分享次数", orderNum = "7", width = 20, height = 8)
	private String shareNum;
	@Excel(name = "时间", orderNum = "8", width = 40, height = 8)
	private String createTime;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCompanyAbbr() {
		return companyAbbr;
	}

	public void setCompanyAbbr(String companyAbbr) {
		this.companyAbbr = companyAbbr;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getReadNum() {
		return readNum;
	}

	public void setReadNum(String readNum) {
		this.readNum = readNum;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getShareNum() {
		return shareNum;
	}

	public void setShareNum(String shareNum) {
		this.shareNum = shareNum;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
