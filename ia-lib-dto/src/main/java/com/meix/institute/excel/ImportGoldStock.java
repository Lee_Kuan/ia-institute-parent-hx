package com.meix.institute.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;

/**
 * 研究所白名单用户导入
 * @author likuan
 * @date 2019年7月2日10:34:27
 */
@ExcelTarget(value = "importGoldStock")
public class ImportGoldStock {

	@Excel(name="股票",orderNum="1",isImportField="secuAbbr")
	private String secuAbbr;
	
	@Excel(name="股票代码",orderNum="2",isImportField="stockNo")
	private String stockNo;
	
	@Excel(name="推荐人",orderNum="3",isImportField="recPerson")
	private String recPerson;
	
	@Excel(name="推荐理由",orderNum="4",isImportField="recDesc")
	private String recDesc;

	public String getSecuAbbr() {
		return secuAbbr;
	}

	public void setSecuAbbr(String secuAbbr) {
		this.secuAbbr = secuAbbr;
	}
	
	public String getStockNo() {
		return stockNo;
	}
	
	public void setStockNo(String stockNo) {
		this.stockNo = stockNo;
	}

	public String getRecDesc() {
		return recDesc;
	}

	public void setRecDesc(String recDesc) {
		this.recDesc = recDesc;
	}

	public String getRecPerson() {
		return recPerson;
	}

	public void setRecPerson(String recPerson) {
		this.recPerson = recPerson;
	}

	//	@Override
//	public String toString() {
//		return " {\"userName\":\"" + userName + "\", \"position\":\"" + position + "\", \"companyName\":\"" + companyName
//				+ "\", \"mobile\":\"" + mobile + "\", \"levelName\":\"" + levelName + "\", \"salerName\":\"" + salerName + "\", \"expireTime\":\""
//				+ expireTime + "\", \"password\":\"" + password + "\"}";
//	}
	@Override
	public String toString() {
		return " {\"stockNo\":\"" + stockNo + "\", \"recDesc\":\"" + recDesc + "\"}";
	}

}
