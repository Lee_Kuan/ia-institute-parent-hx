package com.meix.institute.excel;


import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;

/**
 * 研究所对口销售导入
 *
 * @author likuan
 * @date 2019年8月7日16:43:43
 */
@ExcelTarget(value = "importSaler")
public class ImportSaler {

	@Excel(name = "姓名", orderNum = "1", isImportField = "salerName")
	private String salerName;

	@Excel(name = "手机号码", orderNum = "2", isImportField = "mobile")
	private String mobile;

	@Excel(name = "所在区域", orderNum = "3", isImportField = "area")
	private String area;

	public String getSalerName() {
		return salerName;
	}

	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@Override
	public String toString() {
		return " {\"salerName\":\"" + salerName + "\", \"mobile\":\"" + mobile + "\", \"area\":\"" + area + "\"}";
	}

}
