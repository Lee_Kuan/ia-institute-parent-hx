package com.meix.institute.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * likuan	2019年9月21日10:46:20
 */
public class ExportGoldStock implements Serializable {
	private static final long serialVersionUID = -6504771018560040789L;

	@Excel(name = "股票", orderNum = "1", width = 30, height = 8)
	private String secuMain;
	@Excel(name = "推荐人", orderNum = "2", width = 20, height = 8)
	private String recPerson;
	@Excel(name = "推荐理由", orderNum = "3", width = 60, height = 10)
	private String recDesc;
	@Excel(name = "编辑时间", orderNum = "4", width = 20, height = 8)
	private String editTime;
	@Excel(name = "当月涨幅", orderNum = "5", width = 20, height = 8)
	private String monthRate;
	@Excel(name = "添加自选用户", orderNum = "6", width = 15, height = 8)
	private Integer selfUserNum;
	
	public String getSecuMain() {
		return secuMain;
	}
	public void setSecuMain(String secuMain) {
		this.secuMain = secuMain;
	}
	public String getRecPerson() {
		return recPerson;
	}
	public void setRecPerson(String recPerson) {
		this.recPerson = recPerson;
	}
	public String getRecDesc() {
		return recDesc;
	}
	public void setRecDesc(String recDesc) {
		this.recDesc = recDesc;
	}
	public String getEditTime() {
		return editTime;
	}
	public void setEditTime(String editTime) {
		this.editTime = editTime;
	}
	public String getMonthRate() {
		return monthRate;
	}
	public void setMonthRate(String monthRate) {
		this.monthRate = monthRate;
	}
	public Integer getSelfUserNum() {
		return selfUserNum;
	}
	public void setSelfUserNum(Integer selfUserNum) {
		this.selfUserNum = selfUserNum;
	}

}
