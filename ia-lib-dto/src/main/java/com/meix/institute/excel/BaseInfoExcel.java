package com.meix.institute.excel;


import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * Created by zenghao on 2019/4/23.
 */
public class BaseInfoExcel implements Serializable {
	private static final long serialVersionUID = 2304644333063045312L;

	@Excel(name = "属性", orderNum = "1", width = 30, height = 12)
	private String attr;
	@Excel(name = "数据", orderNum = "2", width = 30, height = 12)
	private String value;

	public String getAttr() {
		return attr;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
