package com.meix.institute.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;

/**
 * 研究所白名单用户导入
 *
 * @author likuan
 * @date 2019年7月2日10:34:27
 */
@ExcelTarget(value = "importInsUser")
public class ImportInsUser {

	@Excel(name = "姓名", orderNum = "1", isImportField = "userName")
	private String userName;

	@Excel(name = "研究方向（申万一级行业）", orderNum = "2", isImportField = "direction")
	private String direction;

	@Excel(name = "职位", orderNum = "3", isImportField = "position")
	private String position;

	@Excel(name = "手机号", orderNum = "4", isImportField = "mobile")
	private String mobile;

	@Excel(name = "邮箱", orderNum = "5", isImportField = "email")
	private String email;

	@Excel(name = "角色", orderNum = "6", isImportField = "role")
	private String role;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
