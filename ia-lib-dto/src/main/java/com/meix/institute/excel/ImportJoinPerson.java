package com.meix.institute.excel;


import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;

/**
 * 研究所代为报名导入
 *
 * @author likuan
 * @date 2019年9月5日10:32:02
 */
@ExcelTarget(value = "importJoinPerson")
public class ImportJoinPerson {

	@Excel(name = "姓名", orderNum = "1", isImportField = "userName")
	private String userName;

	@Excel(name = "机构", orderNum = "2", isImportField = "companyName")
	private String companyName;
	
	@Excel(name = "职位", orderNum = "3", isImportField = "position")
	private String position;
	
	@Excel(name = "手机号", orderNum = "4", isImportField = "mobile")
	private String mobile;

	@Excel(name = "对口销售", orderNum = "5", isImportField = "salerName")
	private String salerName;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getSalerName() {
		return salerName;
	}

	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}

}
