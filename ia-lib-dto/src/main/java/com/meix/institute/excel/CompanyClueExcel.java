package com.meix.institute.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * Created by zenghao on 2019/8/2.
 */
public class CompanyClueExcel implements Serializable {
	private static final long serialVersionUID = -6504771018560040789L;

	@Excel(name = "客户基本信息", orderNum = "1", width = 30, height = 8)
	private String customerCompanyAbbr;
	@Excel(name = "线索类型", orderNum = "2", width = 50, height = 8)
	private String clueName;
	@Excel(name = "客户类型", orderNum = "3", width = 20, height = 8)
	private String customerType;
	@Excel(name = "对口销售", orderNum = "4", width = 20, height = 8)
	private String salerName;
	@Excel(name = "创建时间", orderNum = "5", width = 40, height = 8)
	private String createTime;

	public String getCustomerCompanyAbbr() {
		return customerCompanyAbbr;
	}

	public void setCustomerCompanyAbbr(String customerCompanyAbbr) {
		this.customerCompanyAbbr = customerCompanyAbbr;
	}

	public String getClueName() {
		return clueName;
	}

	public void setClueName(String clueName) {
		this.clueName = clueName;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getSalerName() {
		return salerName;
	}

	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
