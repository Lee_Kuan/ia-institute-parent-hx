package com.meix.institute.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;

/**
 * 研究所白名单用户导入
 *
 * @author likuan
 * @date 2019年7月2日10:34:27
 */
@ExcelTarget(value = "importInstituteWhiteUser")
public class ImportInstituteWhiteUser {

	@Excel(name = "姓名", orderNum = "1", isImportField = "userName")
	private String userName;

	@Excel(name = "机构", orderNum = "2", isImportField = "companyName")
	private String companyName;

	@Excel(name = "职位", orderNum = "3", isImportField = "position")
	private String position;

	@Excel(name = "手机号", orderNum = "4", isImportField = "mobile")
	private String mobile;

	@Excel(name = "邮箱", orderNum = "5", isImportField = "email")
	private String email;
	
	@Excel(name = "固定电话", orderNum = "6", isImportField = "phoneNumber")
	private String phoneNumber;

	@Excel(name = "对口销售", orderNum = "7", isImportField = "salerName")
	private String salerName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSalerName() {
		return salerName;
	}

	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}

}
