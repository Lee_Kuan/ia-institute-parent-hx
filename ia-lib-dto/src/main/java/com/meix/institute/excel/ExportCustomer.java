package com.meix.institute.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * likuan	2019年9月21日10:46:20
 */
public class ExportCustomer implements Serializable {
	private static final long serialVersionUID = 3340335132858746154L;
	
	@Excel(name = "姓名/昵称", orderNum = "1", width = 20, height = 8)
	private String userName;
	@Excel(name = "手机号", orderNum = "2", width = 30, height = 8)
	private String mobile;
	@Excel(name = "登录天数", orderNum = "3", width = 20, height = 8)
	private int loginDays;
	@Excel(name = "首次使用时间", orderNum = "4", width = 40, height = 8)
	private String firstLoginTime;
	@Excel(name = "最后使用时间", orderNum = "5", width = 40, height = 8)
	private String lastLoginTime;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getLoginDays() {
		return loginDays;
	}
	public void setLoginDays(int loginDays) {
		this.loginDays = loginDays;
	}
	public String getFirstLoginTime() {
		return firstLoginTime;
	}
	public void setFirstLoginTime(String firstLoginTime) {
		this.firstLoginTime = firstLoginTime;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
}
