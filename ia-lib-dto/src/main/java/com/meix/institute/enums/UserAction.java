package com.meix.institute.enums;

public enum UserAction {

	/**
	 * 用户单次行为/用户的属性
	 **/
	READ(1, "阅读"),
	//	PRAISE(2, "点赞"),
	SUBSCRIBE(3, "订阅"),
	UNSUBSCRIBE(4, "取消订阅"),
	BOOK(5, "报名/预约"),
	SHARE(6, "分享"),
	SEARCH(7, "搜索"),
//	NOT_INTEREST(8, "不感兴趣"),
	READ_DURATION(9, "阅读时长"),
	PLAY(10, "播放"),
	PLAY_DURATION(11, "播放时长"),

	ADD_SELF(12, "加自选"),
	CANCEL_SELF(13, "取消自选"),
	//	POSITION_STOCK(14, "持仓"),
//	CELL_POSITION_STOCK(15, "卖出持仓"),
	FOCUS(16, "关注"),
	UNFOCUS(17, "取消关注");

	private int code;
	private String msg;

	UserAction(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	UserAction() {

	}

	public int getCode() {
		return this.code;
	}

	public String getMsg() {
		return this.msg;
	}


}
