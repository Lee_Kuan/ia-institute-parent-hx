package com.meix.institute.vo;

import java.io.Serializable;
import java.util.Map;

public class CommentVo implements Serializable {
	private static final long serialVersionUID = 1162373650328472234L;

	private String title;//标题
	private String authorName;//作者名称
	private long authorCode;//作者代码
	private String authorHeadImgUrl;//用户头像链接 
	private String orgName;//机构名称
	private long orgCode;//机构代码 
	private String position;//职位
	private String time;//时间 
	private String commentObject;//评价对象 
	private String commentContent;//评价内容
	private Float score;//客户评分 int 0-5
	private int type;
	private long id;
	private long dataId;
	private String orgTypeName;//公司类型名称
	private Map<String, Object> activity;
	private int hideName;//匿名标识
	
	private String resourceId;//唯一资源id

	private int status;
	private String dataAuthor;
	
	private String operatorName;
	
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getDataAuthor() {
		return dataAuthor;
	}
	public void setDataAuthor(String dataAuthor) {
		this.dataAuthor = dataAuthor;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Map<String, Object> getActivity() {
        return activity;
    }
    public void setActivity(Map<String, Object> activity) {
        this.activity = activity;
    }
    public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public long getAuthorCode() {
		return authorCode;
	}
	public void setAuthorCode(long authorCode) {
		this.authorCode = authorCode;
	}
	public String getAuthorHeadImgUrl() {
		return authorHeadImgUrl;
	}
	public void setAuthorHeadImgUrl(String authorHeadImgUrl) {
		this.authorHeadImgUrl = authorHeadImgUrl;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public long getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(long orgCode) {
		this.orgCode = orgCode;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getCommentObject() {
		return commentObject;
	}
	public void setCommentObject(String commentObject) {
		this.commentObject = commentObject;
	}
	public String getCommentContent() {
		return commentContent;
	}
	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}
	public Float getScore() {
        return score;
    }
    public void setScore(Float score) {
        this.score = score;
    }
    public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getDataId() {
		return dataId;
	}
	public void setDataId(long dataId) {
		this.dataId = dataId;
	}
	public String getOrgTypeName() {
		return orgTypeName;
	}
	public void setOrgTypeName(String orgTypeName) {
		this.orgTypeName = orgTypeName;
	}
	public int getHideName() {
		return hideName;
	}
	public void setHideName(int hideName) {
		this.hideName = hideName;
	}
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	
}
