package com.meix.institute.vo;

import java.io.Serializable;

public class GroupPowerVo implements Serializable {
	private static final long serialVersionUID = 9093417843657020650L;
	
	private long id;
	private String powerName; //权限名称
	private int share; //权限位置
	private String shareName; //权限位置描述
	private int power; //是否有权限
	private long level;//权限父节点id
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPowerName() {
		return powerName;
	}
	public void setPowerName(String powerName) {
		this.powerName = powerName;
	}
	public int getShare() {
		return share;
	}
	public void setShare(int share) {
		this.share = share;
	}
	public String getShareName() {
		return shareName;
	}
	public void setShareName(String shareName) {
		this.shareName = shareName;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public long getLevel() {
		return level;
	}
	public void setLevel(long level) {
		this.level = level;
	}
	
}
