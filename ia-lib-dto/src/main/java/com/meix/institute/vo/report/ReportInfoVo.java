package com.meix.institute.vo.report;

import com.meix.institute.vo.secu.SecuMainVo;

import java.util.List;

public class ReportInfoVo {

	private List<ReportAuthorVo> author;
	private List<SecuMainVo> secu;
	private List<SecuMainVo> industry;
	private List<String> yjfl;
	private int permission;
	private int isCompanyWhiteList;
	private int applyStatus;
	private String pdfUrl;

	public List<ReportAuthorVo> getAuthor() {
		return author;
	}

	public void setAuthor(List<ReportAuthorVo> author) {
		this.author = author;
	}

	public List<SecuMainVo> getSecu() {
		return secu;
	}

	public void setSecu(List<SecuMainVo> secu) {
		this.secu = secu;
	}

	public List<SecuMainVo> getIndustry() {
		return industry;
	}

	public void setIndustry(List<SecuMainVo> industry) {
		this.industry = industry;
	}

	public List<String> getYjfl() {
		return yjfl;
	}

	public void setYjfl(List<String> yjfl) {
		this.yjfl = yjfl;
	}

	public int getPermission() {
		return permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}

	public int getIsCompanyWhiteList() {
		return isCompanyWhiteList;
	}

	public void setIsCompanyWhiteList(int isCompanyWhiteList) {
		this.isCompanyWhiteList = isCompanyWhiteList;
	}

	public int getApplyStatus() {
		return applyStatus;
	}

	public void setApplyStatus(int applyStatus) {
		this.applyStatus = applyStatus;
	}

	public String getPdfUrl() {
		return pdfUrl;
	}

	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}
}
