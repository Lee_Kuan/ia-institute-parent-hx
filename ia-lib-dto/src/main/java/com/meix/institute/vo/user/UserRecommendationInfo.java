package com.meix.institute.vo.user;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class UserRecommendationInfo implements Serializable {

	private static final long serialVersionUID = -57432079625967028L;
	/**
	 * 相关推荐
	 */
	private long id;
	private String title;
	private String secuAbbr;
	private String orgName;
	private Date infoDate;
	private List<UserInfo> authorList;
	private String resourceUrl;
	private String resourceUrlSmall;
	private int dataType;
	private long dataId;
	private int activitySubType;
	private int isEnd;
	private Date activityEndTime;
	private Date activityStartTime;
	private List<String> reportRate;
	private String reportType;
	private List<String> secuList;
	private String reportGrade;
	private String evaluateDesc;
	private String infoType;
	private List<String> yjfl;
	private List<String> author;
	private int activityLabel;
	private String picture;
	private String infoSubType; // 研报二级分类
	private int activityType;

}
