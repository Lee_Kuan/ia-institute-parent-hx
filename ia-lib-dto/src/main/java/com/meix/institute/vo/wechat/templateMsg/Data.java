package com.meix.institute.vo.wechat.templateMsg;

public abstract class Data {
	/**
	 * 服务通知
	 */
	static public class ServiceNoticeData extends Data {
		Model first;//模板开始前描述
		Model keyword1;
		Model keyword2;
		Model remark;//模板结束描述

		public ServiceNoticeData(Model first, Model content, Model occurTime, Model remark) {
			this.first = first;
			this.keyword1 = content;
			this.keyword2 = occurTime;
			this.remark = remark;
		}
	}

	/**
	 * 审核结果通知
	 */
	static public class ExamineNoticeData extends Data {
		Model first;//模板开始前描述
		Model keyword1;//主题
		Model keyword2;//时间
		Model keyword3;//结果
		Model keyword4;//意见
		Model remark;//模板结束描述

		public ExamineNoticeData(Model first, Model theme, Model time, Model result, Model reason, Model remark) {
			this.first = first;
			this.keyword1 = theme;
			this.keyword2 = time;
			this.keyword3 = result;
			this.keyword4 = reason;
			this.remark = remark;
		}
	}
}
