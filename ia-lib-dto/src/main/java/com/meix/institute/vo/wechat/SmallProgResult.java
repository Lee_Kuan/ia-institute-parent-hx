package com.meix.institute.vo.wechat;

import lombok.Data;

import java.io.Serializable;

/**
 * 获取小程序用户openid返回值
 * Created by zenghao on 2020/4/2.
 */
@Data
public class SmallProgResult implements Serializable {
	private static final long serialVersionUID = -7770998668753990085L;

	private String openid;
	private String session_key;
	private String unionid;
	private int errcode;//-1系统繁忙 0成功 40029 code无效 45011接口频率限制（每分钟100次）
	private String errmsg;
}
