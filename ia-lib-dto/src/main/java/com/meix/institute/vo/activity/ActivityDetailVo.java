package com.meix.institute.vo.activity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ActivityDetailVo implements Serializable {
	private static final long serialVersionUID = 4708707244695659171L;

	private long id;

	private int activityType;

	private String authorName;

	private long authorCode;

	private String authorHeadImgUrl;

	private long orgCode;

	private String orgName;

	private String title;

	private String researchObject;

	private int subType;//子类型  int 0 普通 1 金组合 

	private int showPassword;//是否显示电话会议密码 0 不显示 1 显示

	/**
	 * Map{
	 * innerCode,
	 * secuCode,
	 * relationType,
	 * secuAbbr
	 * }
	 */
	private List<Map<String, Object>> secuList;

	/**
	 * Map{
	 * contact,
	 * mobile,
	 * uid
	 * }
	 */
	private List<Map<String, Object>> contact;

	private String address;

	private String mapPosition;

	private String activityDesc;

	private String startTime;

	private String endTime;

	private String form;

	private String createTime;

	private int readNum;

	/**
	 * Map{
	 * authorName,
	 * authorCode,
	 * authorHeadImgUrl,
	 * orgCode,
	 * orgName
	 * }
	 */
	private Map<String, Object> buyer;

	/**
	 * Map{
	 * authorName,
	 * authorCode,
	 * authorHeadImgUrl,
	 * orgCode,
	 * orgName,
	 * identity
	 * }
	 */
	private List<Map<String, Object>> attendPersion;


	/**
	 * Map{
	 * name
	 * code
	 * }
	 */
	private List<Map<String, Object>> analyst;

	private int isJoinIn;//是否加入会议  0未参加 1 参加

	private int isAnalyst;//是否是发起人 0 不是 1是

	private int isEnd;// 活动是否结束  0 未结束  1结束
	private int contentType;//内容类型：1 文本 2 富文本
	private int isRemind;//是否添加提醒 int 0未添加 1已添加
	private String city;//城市
	private int payFlag;//付费开关int 0关闭  1开启
	private double payMoney;//付费金额 
	private int remnantTimes;//试听剩余次数
	private int canSee;//付费标志 int 0不需要付费  1需要付费
	private int sameTimeFlag;//同时段标志
	private int sameIndustryFlag;//同行业标志
	private String url;//外部资源链接
	private String audioUrl;//资源链接地址
	private String fileAttr;//文件属性
	private int roomNo;//直播房间编号
	private String resourceUrl;//海报地址
	private String resourceUrlSmall;//海报地址(小图)

	private String webcastId;//直播ID
	private String vodId;//点播ID
	private String webcastNumber;//直播编号
	private String organizerToken;//组织者口令
	private String panelistToken;//嘉宾口令
	private String attendeeToken;//普通参加者口令
	private String organizerJoinUrl;//组织者加入URL
	private String panelistJoinUrl;//嘉宾加入URL
	private String attendeeAShortJoinUrl;//普通参加者加入URL(不带token)
	private String attendeeJoinUrl;//普通参加者加入URL(带token)
	private String vodNumber;//点播编号
	private String managerTips;//管理员记录信息
	private int playType;//播放类型 0 未填写(默认) 1 直播 2 转播 3 录音
	private int payIntegral;//看活动需要花费的积分
	private String payIntegralUserType;//积分人员类型
	private String orgMeetingUserType;//机构专属会议参会人员类型
	private String orgMeetingCode;//参会密码
	private int evaluateFlag;//报名状态 0 未报名 1 已报名
	private String summaryUrl;//摘要链接
	private int audioHideDay;//录音隐藏天数
	private String applyDeadlineTime;//报名截止时间
	private int playTimes;//播放次数
	private int audioDuration;//播放时长
	private int permission;// 是否有权限 0 没有 1 有
	private int internalType;// 1、每市运营电话会议
	private String pptImg;//ppt图片

	/**
	 * likuan	2019-06-21
	 * 路演活动新增来访机构，接待人员，路演人员
	 */
	/**
	 * Map{
	 * code,
	 * contact
	 * }
	 */
	private List<Map<String, Object>> visitOrg;

	/**
	 * Map{
	 * code,
	 * contact
	 * }
	 */
	private List<Map<String, Object>> jdPeople;

	/**
	 * Map{
	 * code,
	 * contact
	 * }
	 */
	private List<Map<String, Object>> lyPeople;

	/**
	 * 研究所项目新增：发布渠道，隐藏标志，会议类型，会议标签
	 * by:likuan
	 * 2019年7月5日10:46:46
	 */
	private List<Long> issueType;
	private int isHiden;
	private int meetingType;
	private int meetingLabel;
	private int isCompanyWhiteList;//是否是机构的白名单 0不是 1是
	private int applyStatus;//申请公司的白名单权限状态 -1 未申请 0申请中 1成功 2失败

	public List<Long> getIssueType() {
		return issueType;
	}

	public void setIssueType(List<Long> issueType) {
		this.issueType = issueType;
	}

	public int getIsHiden() {
		return isHiden;
	}

	public void setIsHiden(int isHiden) {
		this.isHiden = isHiden;
	}

	public int getMeetingType() {
		return meetingType;
	}

	public void setMeetingType(int meetingType) {
		this.meetingType = meetingType;
	}

	public int getMeetingLabel() {
		return meetingLabel;
	}

	public void setMeetingLabel(int meetingLabel) {
		this.meetingLabel = meetingLabel;
	}

	public List<Map<String, Object>> getVisitOrg() {
		return visitOrg;
	}

	public void setVisitOrg(List<Map<String, Object>> visitOrg) {
		this.visitOrg = visitOrg;
	}

	public List<Map<String, Object>> getJdPeople() {
		return jdPeople;
	}

	public void setJdPeople(List<Map<String, Object>> jdPeople) {
		this.jdPeople = jdPeople;
	}

	public List<Map<String, Object>> getLyPeople() {
		return lyPeople;
	}

	public void setLyPeople(List<Map<String, Object>> lyPeople) {
		this.lyPeople = lyPeople;
	}

	public String getPptImg() {
		return pptImg;
	}

	public void setPptImg(String pptImg) {
		this.pptImg = pptImg;
	}

	public int getInternalType() {
		return internalType;
	}

	public void setInternalType(int internalType) {
		this.internalType = internalType;
	}

	public int getPermission() {
		return permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}

	public String getResourceUrlSmall() {
		return resourceUrlSmall;
	}

	public void setResourceUrlSmall(String resourceUrlSmall) {
		this.resourceUrlSmall = resourceUrlSmall;
	}

	public int getAudioDuration() {
		return audioDuration;
	}

	public void setAudioDuration(int audioDuration) {
		this.audioDuration = audioDuration;
	}

	public String getApplyDeadlineTime() {
		return applyDeadlineTime;
	}

	public void setApplyDeadlineTime(String applyDeadlineTime) {
		this.applyDeadlineTime = applyDeadlineTime;
	}

	public String getVodId() {
		return vodId;
	}

	public void setVodId(String vodId) {
		this.vodId = vodId;
	}

	public String getFileAttr() {
		return fileAttr;
	}

	public void setFileAttr(String fileAttr) {
		this.fileAttr = fileAttr;
	}

	public String getAudioUrl() {
		return audioUrl;
	}

	public void setAudioUrl(String audioUrl) {
		this.audioUrl = audioUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getSameTimeFlag() {
		return sameTimeFlag;
	}

	public void setSameTimeFlag(int sameTimeFlag) {
		this.sameTimeFlag = sameTimeFlag;
	}

	public int getSameIndustryFlag() {
		return sameIndustryFlag;
	}

	public void setSameIndustryFlag(int sameIndustryFlag) {
		this.sameIndustryFlag = sameIndustryFlag;
	}

	public int getCanSee() {
		return canSee;
	}

	public void setCanSee(int canSee) {
		this.canSee = canSee;
	}

	public int getPayFlag() {
		return payFlag;
	}

	public void setPayFlag(int payFlag) {
		this.payFlag = payFlag;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getIsRemind() {
		return isRemind;
	}

	public void setIsRemind(int isRemind) {
		this.isRemind = isRemind;
	}

	public int getContentType() {
		return contentType;
	}

	public void setContentType(int contentType) {
		this.contentType = contentType;
	}

	private String shareName;

	
	public String getShareName() {
		return shareName;
	}

	public void setShareName(String shareName) {
		this.shareName = shareName;
	}

	public int getIsEnd() {
		return isEnd;
	}

	public void setIsEnd(int isEnd) {
		this.isEnd = isEnd;
	}

	public int getIsAnalyst() {
		return isAnalyst;
	}

	public void setIsAnalyst(int isAnalyst) {
		this.isAnalyst = isAnalyst;
	}

	public int getIsJoinIn() {
		return isJoinIn;
	}

	public void setIsJoinIn(int isJoinIn) {
		this.isJoinIn = isJoinIn;
	}

	public List<Map<String, Object>> getAnalyst() {
		return analyst;
	}

	public void setAnalyst(List<Map<String, Object>> analyst) {
		this.analyst = analyst;
	}

	public int getReadNum() {
		return readNum;
	}

	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getActivityType() {
		return activityType;
	}

	public void setActivityType(int activityType) {
		this.activityType = activityType;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public long getAuthorCode() {
		return authorCode;
	}

	public void setAuthorCode(long authorCode) {
		this.authorCode = authorCode;
	}

	public String getAuthorHeadImgUrl() {
		return authorHeadImgUrl;
	}

	public void setAuthorHeadImgUrl(String authorHeadImgUrl) {
		this.authorHeadImgUrl = authorHeadImgUrl;
	}

	public long getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(long orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResearchObject() {
		return researchObject;
	}

	public void setResearchObject(String researchObject) {
		this.researchObject = researchObject;
	}

	public List<Map<String, Object>> getSecuList() {
		return secuList;
	}

	public void setSecuList(List<Map<String, Object>> secuList) {
		this.secuList = secuList;
	}

	public List<Map<String, Object>> getContact() {
		return contact;
	}

	public void setContact(List<Map<String, Object>> contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMapPosition() {
		return mapPosition;
	}

	public void setMapPosition(String mapPosition) {
		this.mapPosition = mapPosition;
	}

	public String getActivityDesc() {
		return activityDesc;
	}

	public void setActivityDesc(String activityDesc) {
		this.activityDesc = activityDesc;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public Map<String, Object> getBuyer() {
		return buyer;
	}

	public void setBuyer(Map<String, Object> buyer) {
		this.buyer = buyer;
	}

	public List<Map<String, Object>> getAttendPersion() {
		return attendPersion;
	}

	public void setAttendPersion(List<Map<String, Object>> attendPersion) {
		this.attendPersion = attendPersion;
	}

	public int getSubType() {
		return subType;
	}

	public void setSubType(int subType) {
		this.subType = subType;
	}

	public int getShowPassword() {
		return showPassword;
	}

	public void setShowPassword(int showPassword) {
		this.showPassword = showPassword;
	}

	public int getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}

	public String getWebcastId() {
		return webcastId;
	}

	public void setWebcastId(String webcastId) {
		this.webcastId = webcastId;
	}

	public String getWebcastNumber() {
		return webcastNumber;
	}

	public void setWebcastNumber(String webcastNumber) {
		this.webcastNumber = webcastNumber;
	}

	public String getOrganizerToken() {
		return organizerToken;
	}

	public void setOrganizerToken(String organizerToken) {
		this.organizerToken = organizerToken;
	}

	public String getPanelistToken() {
		return panelistToken;
	}

	public void setPanelistToken(String panelistToken) {
		this.panelistToken = panelistToken;
	}

	public String getAttendeeToken() {
		return attendeeToken;
	}

	public void setAttendeeToken(String attendeeToken) {
		this.attendeeToken = attendeeToken;
	}

	public String getOrganizerJoinUrl() {
		return organizerJoinUrl;
	}

	public void setOrganizerJoinUrl(String organizerJoinUrl) {
		this.organizerJoinUrl = organizerJoinUrl;
	}

	public String getPanelistJoinUrl() {
		return panelistJoinUrl;
	}

	public void setPanelistJoinUrl(String panelistJoinUrl) {
		this.panelistJoinUrl = panelistJoinUrl;
	}

	public String getAttendeeAShortJoinUrl() {
		return attendeeAShortJoinUrl;
	}

	public void setAttendeeAShortJoinUrl(String attendeeAShortJoinUrl) {
		this.attendeeAShortJoinUrl = attendeeAShortJoinUrl;
	}

	public String getAttendeeJoinUrl() {
		return attendeeJoinUrl;
	}

	public void setAttendeeJoinUrl(String attendeeJoinUrl) {
		this.attendeeJoinUrl = attendeeJoinUrl;
	}

	public String getVodNumber() {
		return vodNumber;
	}

	public void setVodNumber(String vodNumber) {
		this.vodNumber = vodNumber;
	}

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	public double getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(double payMoney) {
		this.payMoney = payMoney;
	}

	public int getRemnantTimes() {
		return remnantTimes;
	}

	public void setRemnantTimes(int remnantTimes) {
		this.remnantTimes = remnantTimes;
	}

	public String getManagerTips() {
		return managerTips;
	}

	public void setManagerTips(String managerTips) {
		this.managerTips = managerTips;
	}

	public int getPlayType() {
		return playType;
	}

	public void setPlayType(int playType) {
		this.playType = playType;
	}

	public int getPayIntegral() {
		return payIntegral;
	}

	public void setPayIntegral(int payIntegral) {
		this.payIntegral = payIntegral;
	}

	public String getPayIntegralUserType() {
		return payIntegralUserType;
	}

	public void setPayIntegralUserType(String payIntegralUserType) {
		this.payIntegralUserType = payIntegralUserType;
	}

	public String getOrgMeetingUserType() {
		return orgMeetingUserType;
	}

	public void setOrgMeetingUserType(String orgMeetingUserType) {
		this.orgMeetingUserType = orgMeetingUserType;
	}

	public String getOrgMeetingCode() {
		return orgMeetingCode;
	}

	public void setOrgMeetingCode(String orgMeetingCode) {
		this.orgMeetingCode = orgMeetingCode;
	}

	public int getEvaluateFlag() {
		return evaluateFlag;
	}

	public void setEvaluateFlag(int evaluateFlag) {
		this.evaluateFlag = evaluateFlag;
	}

	public String getSummaryUrl() {
		return summaryUrl;
	}

	public void setSummaryUrl(String summaryUrl) {
		this.summaryUrl = summaryUrl;
	}

	public int getAudioHideDay() {
		return audioHideDay;
	}

	public void setAudioHideDay(int audioHideDay) {
		this.audioHideDay = audioHideDay;
	}

	public int getPlayTimes() {
		return playTimes;
	}

	public void setPlayTimes(int playTimes) {
		this.playTimes = playTimes;
	}

	public int getIsCompanyWhiteList() {
		return isCompanyWhiteList;
	}

	public void setIsCompanyWhiteList(int isCompanyWhiteList) {
		this.isCompanyWhiteList = isCompanyWhiteList;
	}

	public int getApplyStatus() {
		return applyStatus;
	}

	public void setApplyStatus(int applyStatus) {
		this.applyStatus = applyStatus;
	}
}
