package com.meix.institute.vo.activity;

import java.io.Serializable;

/**
 * 活动标的
 * @author likuan
 *
 */
public class ActivityTargetVo implements Serializable {
	
	private static final long serialVersionUID = -7414425072381995800L;
	private int innerCode;
	private int industryCode;
	private int relartionType;
	private String secuAbbr;
	private String secuCode;
	public int getInnerCode() {
		return innerCode;
	}
	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}
	public int getIndustryCode() {
		return industryCode;
	}
	public void setIndustryCode(int industryCode) {
		this.industryCode = industryCode;
	}
	public int getRelartionType() {
		return relartionType;
	}
	public void setRelartionType(int relartionType) {
		this.relartionType = relartionType;
	}
	public String getSecuAbbr() {
		return secuAbbr;
	}
	public void setSecuAbbr(String secuAbbr) {
		this.secuAbbr = secuAbbr;
	}
	public String getSecuCode() {
		return secuCode;
	}
	public void setSecuCode(String secuCode) {
		this.secuCode = secuCode;
	}
}
