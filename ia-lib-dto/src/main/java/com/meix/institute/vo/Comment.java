package com.meix.institute.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author zhangzhen(J)
 * @describe 留言
 */
public class Comment implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -204041119843476392L;

	private long id;
	
	private long dataId;
	
	private int dataType;

	private String openId;
	
	private long uid;
	
	private String comment; //留言
	
	private int orgFlag;
	
	private Date createTime;
	
	private Float score;//评分
	
	private int hideNameFlag = 1;//匿名 0 不匿名 1 匿名
	
	private int status;
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public int getOrgFlag() {
		return orgFlag;
	}

	public void setOrgFlag(int orgFlag) {
		this.orgFlag = orgFlag;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getHideNameFlag() {
		return hideNameFlag;
	}

	public void setHideNameFlag(int hideNameFlag) {
		this.hideNameFlag = hideNameFlag;
	}
}
