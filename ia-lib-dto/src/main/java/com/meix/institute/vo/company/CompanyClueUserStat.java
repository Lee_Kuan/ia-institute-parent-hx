package com.meix.institute.vo.company;

import java.io.Serializable;

/**
 * 研究所商机用户记录
 * Created by zenghao on 2019/8/8.
 */
public class CompanyClueUserStat implements Serializable {
	private static final long serialVersionUID = 4117740941068481639L;

	private long id;
	private String clueUuid;
	private long companyCode;
	private long uid;
	private String userName;
	private String mobile;
	private long customerCompanyCode;
	private String customerCompanyAbbr;
	private String position;
	private String latestRecordTime;
	private long times;
	private long duration;
	private int clueType;
	private String updateTime;
	private String createTime;
	private String latestLoginTime;
	private int dataType;
	private long dataId;

	private String clueAction;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getClueUuid() {
		return clueUuid;
	}

	public void setClueUuid(String clueUuid) {
		this.clueUuid = clueUuid;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public long getCustomerCompanyCode() {
		return customerCompanyCode;
	}

	public void setCustomerCompanyCode(long customerCompanyCode) {
		this.customerCompanyCode = customerCompanyCode;
	}

	public String getCustomerCompanyAbbr() {
		return customerCompanyAbbr;
	}

	public void setCustomerCompanyAbbr(String customerCompanyAbbr) {
		this.customerCompanyAbbr = customerCompanyAbbr;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getLatestRecordTime() {
		return latestRecordTime;
	}

	public void setLatestRecordTime(String latestRecordTime) {
		this.latestRecordTime = latestRecordTime;
	}

	public long getTimes() {
		return times;
	}

	public void setTimes(long times) {
		this.times = times;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public int getClueType() {
		return clueType;
	}

	public void setClueType(int clueType) {
		this.clueType = clueType;
	}

	public String getClueAction() {
		return clueAction;
	}

	public void setClueAction(String clueAction) {
		this.clueAction = clueAction;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLatestLoginTime() {
		return latestLoginTime;
	}

	public void setLatestLoginTime(String latestLoginTime) {
		this.latestLoginTime = latestLoginTime;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}
}
