package com.meix.institute.vo.secu;

public class SecuActivityVo {

	private int innerCode;
	private int industryCode;
	private int secuCategory;
	private String secuAbbr;
	private String secuCode;
	private String suffix;
	public int getInnerCode() {
		return innerCode;
	}
	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}
	public int getIndustryCode() {
		return industryCode;
	}
	public void setIndustryCode(int industryCode) {
		this.industryCode = industryCode;
	}
	public int getSecuCategory() {
		return secuCategory;
	}
	public void setSecuCategory(int secuCategory) {
		this.secuCategory = secuCategory;
	}
	public String getSecuAbbr() {
		return secuAbbr;
	}
	public void setSecuAbbr(String secuAbbr) {
		this.secuAbbr = secuAbbr;
	}
	public String getSecuCode() {
		return secuCode;
	}
	public void setSecuCode(String secuCode) {
		this.secuCode = secuCode;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
}
