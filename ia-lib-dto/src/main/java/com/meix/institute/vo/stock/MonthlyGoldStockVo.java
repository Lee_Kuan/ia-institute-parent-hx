package com.meix.institute.vo.stock;

import java.math.BigDecimal;
import java.util.Date;

public class MonthlyGoldStockVo {

	private Long id;
    private Long companyCode;
    private String recPerson;
    private String recDesc;
    private Integer innerCode;
    private String secuAbbr;
    private String secuCode;
    private String month;
    private BigDecimal addPrice;
    private BigDecimal addMonthPrice;
    private BigDecimal nowPrice;
    private BigDecimal monthRate;
    private BigDecimal nowRate;
    private Integer selfUserNum;
    private Date createdAt;
    private String createdBy;
    private Date updatedAt;
    private String updatedBy;
    private String recPersonName;
    private Integer homeShowFlag;

    public MonthlyGoldStockVo() {
    	
	}
    public MonthlyGoldStockVo(long id, long companyCode, String recPerson, String recPersonName, String recDesc, int innerCode, String secuAbbr, String secuCode, String month,
    		BigDecimal monthRate, BigDecimal nowRate, int selfUserNum, Date createdAt, String createdBy, Date updatedAt, String updatedBy) {
    	this.id = id;
    	this.companyCode = companyCode;
    	this.recPerson = recPerson;
    	this.recPersonName = recPersonName;
    	this.recDesc = recDesc;
    	this.secuAbbr = secuAbbr;
    	this.secuCode = secuCode;
    	this.innerCode =innerCode;
    	this.month = month;
    	this.monthRate = monthRate;
    	this.nowRate = nowRate;
    	this.selfUserNum = selfUserNum;
    	this.createdAt = createdAt;
    	this.createdBy = createdBy;
    	this.updatedAt = updatedAt;
    	this.updatedBy = updatedBy;
	}
    
	public Integer getHomeShowFlag() {
        return homeShowFlag;
    }
    public void setHomeShowFlag(Integer homeShowFlag) {
        this.homeShowFlag = homeShowFlag;
    }
    public String getSecuAbbr() {
		return secuAbbr;
	}
	public void setSecuAbbr(String secuAbbr) {
		this.secuAbbr = secuAbbr;
	}
	public String getSecuCode() {
		return secuCode;
	}
	public void setSecuCode(String secuCode) {
		this.secuCode = secuCode;
	}
	public BigDecimal getAddPrice() {
		return addPrice;
	}
	public void setAddPrice(BigDecimal addPrice) {
		this.addPrice = addPrice;
	}
	public BigDecimal getAddMonthPrice() {
		return addMonthPrice;
	}
	public void setAddMonthPrice(BigDecimal addMonthPrice) {
		this.addMonthPrice = addMonthPrice;
	}
	public BigDecimal getNowPrice() {
		return nowPrice;
	}
	public void setNowPrice(BigDecimal nowPrice) {
		this.nowPrice = nowPrice;
	}
	public BigDecimal getMonthRate() {
		return monthRate;
	}

	public void setMonthRate(BigDecimal monthRate) {
		this.monthRate = monthRate;
	}

	public BigDecimal getNowRate() {
		return nowRate;
	}

	public void setNowRate(BigDecimal nowRate) {
		this.nowRate = nowRate;
	}

	public Integer getSelfUserNum() {
		return selfUserNum;
	}
	public void setSelfUserNum(Integer selfUserNum) {
		this.selfUserNum = selfUserNum;
	}
	/**
     * 获取自增长ID
     *
     * @return ID - 自增长ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置自增长ID
     *
     * @param id 自增长ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取公司代码
     *
     * @return COMPANY_CODE - 公司代码
     */
    public Long getCompanyCode() {
        return companyCode;
    }

    /**
     * 设置公司代码
     *
     * @param companyCode 公司代码
     */
    public void setCompanyCode(Long companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 获取推荐人
     *
     * @return REC_PERSON - 推荐人
     */
    public String getRecPerson() {
        return recPerson;
    }

    /**
     * 设置推荐人
     *
     * @param recPerson 推荐人
     */
    public void setRecPerson(String recPerson) {
        this.recPerson = recPerson == null ? null : recPerson.trim();
    }

    /**
     * 获取推荐理由
     *
     * @return REC_DESC - 推荐理由
     */
    public String getRecDesc() {
        return recDesc;
    }

    /**
     * 设置推荐理由
     *
     * @param recDesc 推荐理由
     */
    public void setRecDesc(String recDesc) {
        this.recDesc = recDesc == null ? null : recDesc.trim();
    }

    /**
     * 获取股票内码
     *
     * @return INNER_CODE - 股票内码
     */
    public Integer getInnerCode() {
        return innerCode;
    }

    /**
     * 设置股票内码
     *
     * @param innerCode 股票内码
     */
    public void setInnerCode(Integer innerCode) {
        this.innerCode = innerCode;
    }

    /**
     * 获取月份 格式YYYY-MM
     *
     * @return MONTH - 月份 格式YYYY-MM
     */
    public String getMonth() {
        return month;
    }

    /**
     * 设置月份 格式YYYY-MM
     *
     * @param month 月份 格式YYYY-MM
     */
    public void setMonth(String month) {
        this.month = month == null ? null : month.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATED_AT - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取创建人
     *
     * @return CREATED_BY - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATED_AT - 更新时间
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 设置更新时间
     *
     * @param updatedAt 更新时间
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 获取更新人
     *
     * @return UPDATED_BY - 更新人
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 设置更新人
     *
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }
	public String getRecPersonName() {
		return recPersonName;
	}
	public void setRecPersonName(String recPersonName) {
		this.recPersonName = recPersonName;
	}
    
}