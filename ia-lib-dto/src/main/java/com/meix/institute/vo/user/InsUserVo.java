package com.meix.institute.vo.user;

import java.util.Date;
import java.util.List;

import com.meix.institute.vo.level.InsLevelVo;

public class InsUserVo {

	private long id;
	private String userName;
	private String companyName;
	private String position;
	private String mobile;
	private long salerUid;
	private String salerName;
	private Date expireTime;
	private String createVia;	//创建方式
	private List<InsLevelVo> level;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public long getSalerUid() {
		return salerUid;
	}
	public void setSalerUid(long salerUid) {
		this.salerUid = salerUid;
	}
	public String getSalerName() {
		return salerName;
	}
	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}
	public Date getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}
	public String getCreateVia() {
		return createVia;
	}
	public void setCreateVia(String createVia) {
		this.createVia = createVia;
	}
	public List<InsLevelVo> getLevel() {
		return level;
	}
	public void setLevel(List<InsLevelVo> level) {
		this.level = level;
	}
	
}
