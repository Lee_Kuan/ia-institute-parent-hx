package com.meix.institute.vo.meeting;

import java.io.Serializable;

/**
 * @版权信息    :上海睿涉取有限公司
 * @部门        :开发部
 * @作者        :zhouwei
 * @E-mail : zhouwei@51research.com
 * @创建日期    : 2016年1月6日
 * @Description:活动权限
 */
public class MorningMeetingShareVo implements Serializable {
	private static final long serialVersionUID = -4011767678572325195L;
	private long dm;
	private int shareType;//权限类型   1 机构  2研究员 3群组  4买方 5卖方
	private String name;//展示的名字

	public long getDm() {
		return dm;
	}

	public void setDm(long dm) {
		this.dm = dm;
	}

	public int getShareType() {
		return shareType;
	}

	public void setShareType(int shareType) {
		this.shareType = shareType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
