package com.meix.institute.vo.meeting;


import com.meix.institute.vo.BaseVo;

import java.io.Serializable;
import java.util.Date;

/**
 * @describe 晨会
 */
public class MorningMeetingListVo extends BaseVo implements Serializable {
	private static final long serialVersionUID = -9063462376758852284L;
	private Long id;

	private Long uid;

	private Long companyCode;

	/**
	 * 标题
	 */
	private String title;

	/**
	 * 内容
	 */
	private String content;

	/**
	 * 文件属性
	 */
	private String fileAttr;

	/**
	 * 标志  -1 撤销  1 可用
	 */
	private Integer flag;

	private String createTime;

	private Long orgCode;

	private String orgName;

	private String audioUrl;

	private Integer share;
	private String shareName;

	private Integer status;

	private int permission; //权限：0-无权限，1-有权限

	private Date publishDate;

	private String publishDateText; // 发布时间文本
	private int type;
	private String url;
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPublishDateText() {
		return publishDateText;
	}

	public void setPublishDateText(String publishDateText) {
		this.publishDateText = publishDateText;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	/**
	 * @return 0-无权限，1-有权限
	 */
	public int getPermission() {
		return permission;
	}

	/**
	 * @param permission 0-无权限，1-有权限
	 */
	public void setPermission(int permission) {
		this.permission = permission;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public MorningMeetingListVo() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(Long companyCode) {
		this.companyCode = companyCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFileAttr() {
		return fileAttr;
	}

	public void setFileAttr(String fileAttr) {
		this.fileAttr = fileAttr;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Long getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(Long orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getAudioUrl() {
		return audioUrl;
	}

	public void setAudioUrl(String audioUrl) {
		this.audioUrl = audioUrl;
	}

	public String getShareName() {
		return shareName;
	}

	public void setShareName(String shareName) {
		this.shareName = shareName;
	}

	public Integer getShare() {
		return share;
	}

	public void setShare(Integer share) {
		this.share = share;
	}

}
