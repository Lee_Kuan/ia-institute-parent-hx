package com.meix.institute.vo.meeting;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @describe 晨会
 */
public class MorningMeetingVo implements Serializable {

	private static final long serialVersionUID = -6886589100844144734L;

	private Long id;

	private Long uid;

	private Long companyCode;

	/**
	 * 标题
	 */
	private String title;

	/**
	 * 内容
	 */
	private String content;

	/**
	 * 文件属性
	 */
	private String fileAttr;

	/**
	 * 标志  -1 撤销  1 可用
	 */
	private Integer flag;

	private String createTime;

	private Long authorCode;

	private String authorName;

	private String authorHeadImgUrl;

	private Long orgCode;

	private String orgName;

	private String audioUrl;

	private Integer mt;

	private Integer readNum;

	private List<Integer> issueTypeList;
	private Integer share;
	private String shareName;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;

	/**
	 * 作者在职状态
	 * 0-在职，1-离职
	 */
	private Integer userState;
	/**
	 * 录音时长
	 */
	private Integer audioDuration;

	private String editUserName;

	private String operatorUid;
	private String operatorName;
	private String undoReason;
	private String refuseReason;
	private Integer status;

	private int permission; //权限：0-无权限，1-有权限
	private int isCompanyWhiteList;    //白名单用户标志：0-非白名单，1-白名单
	private int applyStatus;    //申请白名单权限状态：0-申请中，1-成功，2-失败

	private Integer duration;
	private String url;
	private Date publishDate;

	private Date undoTime;

	private String publishDateText; // 发布时间文本
	private int meetingType;
	
	private int type;
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getMeetingType() {
		return meetingType;
	}

	public void setMeetingType(int meetingType) {
		this.meetingType = meetingType;
	}

	public String getPublishDateText() {
		return publishDateText;
	}

	public void setPublishDateText(String publishDateText) {
		this.publishDateText = publishDateText;
	}

	public Date getUndoTime() {
		return undoTime;
	}

	public void setUndoTime(Date undoTime) {
		this.undoTime = undoTime;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	/**
	 * @return 0-无权限，1-有权限
	 */
	public int getPermission() {
		return permission;
	}

	/**
	 * @param permission 0-无权限，1-有权限
	 */
	public void setPermission(int permission) {
		this.permission = permission;
	}

	/**
	 * @return 0-非白名单用户，1-白名单用户
	 */
	public int getIsCompanyWhiteList() {
		return isCompanyWhiteList;
	}

	/**
	 * @param permission 0-非白名单用户，1-白名单用户
	 */
	public void setIsCompanyWhiteList(int isCompanyWhiteList) {
		this.isCompanyWhiteList = isCompanyWhiteList;
	}

	/**
	 * @return 0-申请中，1-成功，2-失败
	 */
	public int getApplyStatus() {
		return applyStatus;
	}

	/**
	 * @param applyStatus 0-申请中，1-成功，2-失败
	 */
	public void setApplyStatus(int applyStatus) {
		this.applyStatus = applyStatus;
	}

	public String getOperatorUid() {
		return operatorUid;
	}

	public void setOperatorUid(String operatorUid) {
		this.operatorUid = operatorUid;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getUndoReason() {
		return undoReason;
	}

	public void setUndoReason(String undoReason) {
		this.undoReason = undoReason;
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public MorningMeetingVo() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(Long companyCode) {
		this.companyCode = companyCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFileAttr() {
		return fileAttr;
	}

	public void setFileAttr(String fileAttr) {
		this.fileAttr = fileAttr;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Long getAuthorCode() {
		return authorCode;
	}

	public void setAuthorCode(Long authorCode) {
		this.authorCode = authorCode;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getAuthorHeadImgUrl() {
		return authorHeadImgUrl;
	}

	public void setAuthorHeadImgUrl(String authorHeadImgUrl) {
		this.authorHeadImgUrl = authorHeadImgUrl;
	}

	public Long getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(Long orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getAudioUrl() {
		return audioUrl;
	}

	public void setAudioUrl(String audioUrl) {
		this.audioUrl = audioUrl;
	}

	public Integer getMt() {
		return mt;
	}

	public void setMt(Integer mt) {
		this.mt = mt;
	}

	public Integer getReadNum() {
		return readNum;
	}

	public void setReadNum(Integer readNum) {
		this.readNum = readNum;
	}

	public List<Integer> getIssueTypeList() {
		return issueTypeList;
	}

	public void setIssueTypeList(List<Integer> issueTypeList) {
		this.issueTypeList = issueTypeList;
	}

	public String getShareName() {
		return shareName;
	}

	public void setShareName(String shareName) {
		this.shareName = shareName;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getAudioDuration() {
		return audioDuration;
	}

	public void setAudioDuration(Integer audioDuration) {
		this.audioDuration = audioDuration;
	}

	public Integer getUserState() {
		return userState;
	}

	public void setUserState(Integer userState) {
		this.userState = userState;
	}

	public String getEditUserName() {
		return editUserName;
	}

	public void setEditUserName(String editUserName) {
		this.editUserName = editUserName;
	}

	public Integer getShare() {
		return share;
	}

	public void setShare(Integer share) {
		this.share = share;
	}

}
