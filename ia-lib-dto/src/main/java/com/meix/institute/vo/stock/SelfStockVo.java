package com.meix.institute.vo.stock;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @版权信息: 上海睿涉取有限公司
 * @部门    ：开发部
 * @工程名  : 投资终端
 * @作者    ：zhouwei
 * @E-mail  : zhouwei@51research.com
 * @创建日期: 2014-4-22下午1:33:54
 */
public class SelfStockVo implements Serializable{
	private static final long serialVersionUID = 4140975763631716473L;
	private long categoryId;//自选股目录id
	private long uid;//用户ID
	private String categoryName;//目录名称
	private String categoryDate;//目录日期
	private long id;//自选股数据库ID
	private int sortNo;//股票序号
	private int innerCode;//股票内码
	private short flag;//1 插入 2 更新 3删除
	private int isDeleted;//删除标志 0 未删除 1删除
	private String createDate;
	private String orderTime;
	private String pointTime;
	private String readTime;
	private String secuCode;//股票代码
	private String secuAbbr;//简称
	private String chiSpelling;
	private int industryCode;
	private int secuCategory;//类别 4指数 
	private String suffix;//后缀
	private String codeByAgent;
	private int listedState;//状态
	private int originFlag;//来源标识：0 手动添加 1 睿行研推广
	private int secuMarket;//证券市场类型
	private int count;//统计数
	private String authorName; //创建人
	private BigDecimal colsePrice; //关闭价格
	private BigDecimal adjust; // 复权因子
	private long authorId; //创建人id
	
    public long getAuthorId() {
        return authorId;
    }
    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }
    public BigDecimal getColsePrice() {
        return colsePrice;
    }
    public void setColsePrice(BigDecimal colsePrice) {
        this.colsePrice = colsePrice;
    }
    public BigDecimal getAdjust() {
        return adjust;
    }
    public void setAdjust(BigDecimal adjust) {
        this.adjust = adjust;
    }
    public String getCreateDate() {
        return createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
    public int getListedState() {
		return listedState;
	}
	public void setListedState(int listedState) {
		this.listedState = listedState;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getCodeByAgent() {
		return codeByAgent;
	}
	public void setCodeByAgent(String codeByAgent) {
		this.codeByAgent = codeByAgent;
	}
	public int getSecuCategory() {
		return secuCategory;
	}
	public void setSecuCategory(int secuCategory) {
		this.secuCategory = secuCategory;
	}
	public int getOriginFlag() {
		return originFlag;
	}
	public void setOriginFlag(int originFlag) {
		this.originFlag = originFlag;
	}
	public int getIndustryCode() {
		return industryCode;
	}
	public void setIndustryCode(int industryCode) {
		this.industryCode = industryCode;
	}
	public String getChiSpelling() {
		return chiSpelling;
	}
	public void setChiSpelling(String chiSpelling) {
		this.chiSpelling = chiSpelling;
	}
	public String getSecuCode() {
		return secuCode;
	}
	public void setSecuCode(String secuCode) {
		this.secuCode = secuCode;
	}
	public String getSecuAbbr() {
		return secuAbbr;
	}
	public void setSecuAbbr(String secuAbbr) {
		this.secuAbbr = secuAbbr;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryDate() {
		return categoryDate;
	}
	public void setCategoryDate(String categoryDate) {
		this.categoryDate = categoryDate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getSortNo() {
		return sortNo;
	}
	public void setSortNo(int sortNo) {
		this.sortNo = sortNo;
	}
	public int getInnerCode() {
		return innerCode;
	}
	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}
	public short getFlag() {
		return flag;
	}
	public void setFlag(short flag) {
		this.flag = flag;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getPointTime() {
		return pointTime;
	}
	public void setPointTime(String pointTime) {
		this.pointTime = pointTime;
	}
	public String getReadTime() {
		return readTime;
	}
	public void setReadTime(String readTime) {
		this.readTime = readTime;
	}
	public int getSecuMarket() {
		return secuMarket;
	}
	public void setSecuMarket(int secuMarket) {
		this.secuMarket = secuMarket;
	}
	
}
