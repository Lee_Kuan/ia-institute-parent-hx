package com.meix.institute.vo.stock;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @创建日期: 2018-11-06下午05:44:39
 * @类说明  :个股衍生数据
 */
public class SecuExtInfo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private long id;
	private int innerCode;//股票内码
	private int combOwnerCount;//持有该股票的组合数
	private BigDecimal combGainDegree;//盈利组合占比
	private BigDecimal combLossDegree;//亏损组合占比
	private BigDecimal heatDegree;//热度
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getInnerCode() {
		return innerCode;
	}
	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}
	public int getCombOwnerCount() {
		return combOwnerCount;
	}
	public void setCombOwnerCount(int combOwnerCount) {
		this.combOwnerCount = combOwnerCount;
	}
	public BigDecimal getCombGainDegree() {
		return combGainDegree;
	}
	public void setCombGainDegree(BigDecimal combGainDegree) {
		this.combGainDegree = combGainDegree;
	}
	public BigDecimal getCombLossDegree() {
		return combLossDegree;
	}
	public void setCombLossDegree(BigDecimal combLossDegree) {
		this.combLossDegree = combLossDegree;
	}
	public BigDecimal getHeatDegree() {
		return heatDegree;
	}
	public void setHeatDegree(BigDecimal heatDegree) {
		this.heatDegree = heatDegree;
	}
   
}
