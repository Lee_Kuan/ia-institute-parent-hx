package com.meix.institute.vo.commen;

import java.util.HashMap;

/**
 * @author luki
 */
public class LowerHashMap extends HashMap<String, Object> {
	/**
	 *
	 */
	private static final long serialVersionUID = -1527774714181410277L;

	@Override
	public Object put(String key, Object value) {
		String newKey = key;
		if (key.endsWith("ID")) {
			key = key.substring(0, key.length() - 2) + "Id";
		}
		if ("ID".equalsIgnoreCase(key) || "UID".equalsIgnoreCase(key)) {
			newKey = key.toLowerCase();
		} else {
			char[] cs = key.toCharArray();
			if (cs[0] > 64 && cs[0] < 91) {
				cs[0] += 32;
				newKey = new String(cs);
			}
		}
		return super.put(newKey, value);
	}

	public static void main(String[] args) {
		System.err.println('A' + 0);
		System.err.println('Z' + 0);

		System.err.println('a' + 0);
		System.err.println('z' + 0);
	}
}
