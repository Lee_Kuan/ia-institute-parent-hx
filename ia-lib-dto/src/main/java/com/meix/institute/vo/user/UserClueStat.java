package com.meix.institute.vo.user;

import java.io.Serializable;

/**
 * 用户商机统计
 * Created by zenghao on 2019/9/12.
 */
public class UserClueStat implements Serializable {
	private static final long serialVersionUID = -1644629881036561118L;

	private long companyCode;
	private long uid;
	private String userName;
	private String mobile;
	private long customerCompanyCode;
	private String customerCompanyAbbr;
	private int customerCompanyType;
	private String fundSize;//资金规模
	private String address;//公司地址
	private String position;
	private String latestRecordTime;
	private String latestLoginTime;
	private long clueCount;
	private long salerId;
	private String salerName;
	private int clueType;
	private String email;
	private String groupName;
	private int identify;
	private String identifyName;
	
	public int getIdentify() {
		return identify;
	}

	public void setIdentify(int identify) {
		this.identify = identify;
	}

	public String getIdentifyName() {
		return identifyName;
	}

	public void setIdentifyName(String identifyName) {
		this.identifyName = identifyName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public long getCustomerCompanyCode() {
		return customerCompanyCode;
	}

	public void setCustomerCompanyCode(long customerCompanyCode) {
		this.customerCompanyCode = customerCompanyCode;
	}

	public String getCustomerCompanyAbbr() {
		return customerCompanyAbbr;
	}

	public void setCustomerCompanyAbbr(String customerCompanyAbbr) {
		this.customerCompanyAbbr = customerCompanyAbbr;
	}

	public int getCustomerCompanyType() {
		return customerCompanyType;
	}

	public void setCustomerCompanyType(int customerCompanyType) {
		this.customerCompanyType = customerCompanyType;
	}

	public String getFundSize() {
		return fundSize;
	}

	public void setFundSize(String fundSize) {
		this.fundSize = fundSize;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getLatestRecordTime() {
		return latestRecordTime;
	}

	public void setLatestRecordTime(String latestRecordTime) {
		this.latestRecordTime = latestRecordTime;
	}

	public String getLatestLoginTime() {
		return latestLoginTime;
	}

	public void setLatestLoginTime(String latestLoginTime) {
		this.latestLoginTime = latestLoginTime;
	}

	public long getClueCount() {
		return clueCount;
	}

	public void setClueCount(long clueCount) {
		this.clueCount = clueCount;
	}

	public long getSalerId() {
		return salerId;
	}

	public void setSalerId(long salerId) {
		this.salerId = salerId;
	}

	public String getSalerName() {
		return salerName;
	}

	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}

	public int getClueType() {
		return clueType;
	}

	public void setClueType(int clueType) {
		this.clueType = clueType;
	}
	
}
