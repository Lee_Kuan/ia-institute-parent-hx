package com.meix.institute.vo.cache;

import java.io.Serializable;

/**
 * @Description:常量值对象
 */
public class ConstantVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2144054646625926507L;
	private int code;
	private String name;
	private int innerCode;
	private int value;
	private String nameAbbr;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getInnerCode() {
		return innerCode;
	}
	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getNameAbbr() {
		return nameAbbr;
	}
	public void setNameAbbr(String nameAbbr) {
		this.nameAbbr = nameAbbr;
	}
	
}
