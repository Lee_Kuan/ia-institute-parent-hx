package com.meix.institute.vo.user;

import java.io.Serializable;

/**
 * @Description:用户行为统计
 */
public class UserRecordStatVo implements Serializable {
	private static final long serialVersionUID = -6194446423839750920L;

	private long id;
	private long insCompanyCode;
	private long dataId;
	private int dataType;
	private long uid;
	private String userName;
	private String mobile;
	private String position;
	private String companyAbbr;
	private long companyCode;
	private String updateTime;
	private String createTime;
	private long readNum;
	private long duration;//浏览时长，单位秒
	private long shareNum;
	private String latestRecordTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getInsCompanyCode() {
		return insCompanyCode;
	}

	public void setInsCompanyCode(long insCompanyCode) {
		this.insCompanyCode = insCompanyCode;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getCompanyAbbr() {
		return companyAbbr;
	}

	public void setCompanyAbbr(String companyAbbr) {
		this.companyAbbr = companyAbbr;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public long getReadNum() {
		return readNum;
	}

	public void setReadNum(long readNum) {
		this.readNum = readNum;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getShareNum() {
		return shareNum;
	}

	public void setShareNum(long shareNum) {
		this.shareNum = shareNum;
	}

	public String getLatestRecordTime() {
		return latestRecordTime;
	}

	public void setLatestRecordTime(String latestRecordTime) {
		this.latestRecordTime = latestRecordTime;
	}
}
