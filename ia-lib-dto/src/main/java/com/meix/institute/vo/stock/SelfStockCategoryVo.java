package com.meix.institute.vo.stock;

import java.io.Serializable;

/**
 * @版权信息: 上海睿涉取有限公司
 * @部门    ：开发部
 * @工程名  : 投资终端
 * @作者    ：zhouwei
 * @E-mail  : zhouwei@51research.com
 * @创建日期: 2014-4-22下午1:33:54
 */
public class SelfStockCategoryVo implements Serializable,Cloneable{
	private static final long serialVersionUID = 4140975763631716473L;
	private long id;//自选股目录id
	private long uid;//用户ID
	private String categoryName;//目录名称
	private String createTime;//目录日期
	private String updateTime;//更新日期
	private int flag;
	private int sortNo;//序号
	private int privilege;//权限  0 私密 1半公开  2公开
	private int stockNum;//股票数目
	private int dataType;//0 用户目录 1、系统创建目录 2、睿行研
	private String clientType;//客户端类型 1 每市APP 2 pc网页 3 微信(分享页面) 4 iamanage 5 微信(公众号) 6 财联社 7 私募阿尔法平台
	
	public String getClientType() {
		return clientType;
	}
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	public int getDataType() {
		return dataType;
	}
	public void setDataType(int dataType) {
		this.dataType = dataType;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public int getStockNum() {
		return stockNum;
	}
	public void setStockNum(int stockNum) {
		this.stockNum = stockNum;
	}
	public int getSortNo() {
		return sortNo;
	}
	public void setSortNo(int sortNo) {
		this.sortNo = sortNo;
	}
	public int getPrivilege() {
		return privilege;
	}
	public void setPrivilege(int privilege) {
		this.privilege = privilege;
	}
	public long getId() {
		return id;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	@Override
	public Object clone() {
		SelfStockCategoryVo vo=null;
		try {
			vo=(SelfStockCategoryVo) super.clone();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vo;
	}		
}
