package com.meix.institute.vo.company;

import com.meix.institute.gson.GsonExclude;

import java.io.Serializable;
import java.util.List;

/**
 * 公司线索（商机）
 * Created by zenghao on 2019/8/2.
 */
public class CompanyClueInfo implements Serializable {
	private static final long serialVersionUID = 521505577382746153L;

	private long id;
	private String uuid;//唯一id
	private long companyCode;//研究所公司代码
	private long uid;
	private String userName;
	private String mobile;
	private String position;
	private String address;
	private String customerCompanyAbbr;
	private long customerCompanyCode;
	private int customerCompanyType;
	private String fundSize;//资金规模
	private int clueType;
	private String clueName;
	private int customerType;//0白名单用户 1潜在用户
	private int issueType;//0-美市机构主页 1-公众号 2-短信 3-邮件
	private String latestRecordTime;
	private long salerId;
	private String salerName;
	private String createTime;
	private String updateTime;
	private String latestLoginTime;
	@GsonExclude
	private List<CompanyClueUserStat> userStatList;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCustomerCompanyAbbr() {
		return customerCompanyAbbr;
	}

	public void setCustomerCompanyAbbr(String customerCompanyAbbr) {
		this.customerCompanyAbbr = customerCompanyAbbr;
	}

	public long getCustomerCompanyCode() {
		return customerCompanyCode;
	}

	public void setCustomerCompanyCode(long customerCompanyCode) {
		this.customerCompanyCode = customerCompanyCode;
	}

	public int getCustomerCompanyType() {
		return customerCompanyType;
	}

	public void setCustomerCompanyType(int customerCompanyType) {
		this.customerCompanyType = customerCompanyType;
	}

	public String getFundSize() {
		return fundSize;
	}

	public void setFundSize(String fundSize) {
		this.fundSize = fundSize;
	}

	public int getClueType() {
		return clueType;
	}

	public void setClueType(int clueType) {
		this.clueType = clueType;
	}

	public String getClueName() {
		return clueName;
	}

	public void setClueName(String clueName) {
		this.clueName = clueName;
	}

	public int getCustomerType() {
		return customerType;
	}

	public void setCustomerType(int customerType) {
		this.customerType = customerType;
	}

	public int getIssueType() {
		return issueType;
	}

	public void setIssueType(int issueType) {
		this.issueType = issueType;
	}

	public String getLatestRecordTime() {
		return latestRecordTime;
	}

	public void setLatestRecordTime(String latestRecordTime) {
		this.latestRecordTime = latestRecordTime;
	}

	public long getSalerId() {
		return salerId;
	}

	public void setSalerId(long salerId) {
		this.salerId = salerId;
	}

	public String getSalerName() {
		return salerName;
	}

	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getLatestLoginTime() {
		return latestLoginTime;
	}

	public void setLatestLoginTime(String latestLoginTime) {
		this.latestLoginTime = latestLoginTime;
	}

	public List<CompanyClueUserStat> getUserStatList() {
		return userStatList;
	}

	public void setUserStatList(List<CompanyClueUserStat> userStatList) {
		this.userStatList = userStatList;
	}
}
