package com.meix.institute.vo;

import java.io.Serializable;

public class DataShareVo implements Serializable {
	private static final long serialVersionUID = -4011767678572325195L;
	private long dm;
	private int share;//权限类型   -1 默认，0 公开 1 组 4 accountType 
	private String powerName;//权限名字
	private String name;//展示的名字
	private int type; //类别 （share=1时，为组类型，1-内部用户组，2-白名单组，3-已认证用户组）
	private int location;//授权位置，0-全部，1-列表，2-详情，3、4
	private int isSelect;
	
	public String getPowerName() {
		return powerName;
	}

	public void setPowerName(String powerName) {
		this.powerName = powerName;
	}

	public long getDm() {
		return dm;
	}

	public void setDm(long dm) {
		this.dm = dm;
	}

	public int getShare() {
		return share;
	}

	public void setShare(int share) {
		this.share = share;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public int getIsSelect() {
		return isSelect;
	}

	public void setIsSelect(int isSelect) {
		this.isSelect = isSelect;
	}
	
}
