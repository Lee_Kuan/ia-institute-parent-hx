package com.meix.institute.vo.stat;

import java.io.Serializable;

/**
 * 热度数据
 * Created by zenghao on 2019/9/4.
 */
public class HeatDataVo implements Serializable {
	private static final long serialVersionUID = 6871792309780332296L;

	private long id;
	private int type;
	private String dataId;
	private int dataType;
	private String dataDesc;
	private String dataCode;
	private long companyCode;
	private long marketAttention;
	private long orgAttention;
	private long orgWhiteAttention;
	private long marketContentNum;
	private long orgContentNum;
	private String createTime;
	
	public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public long getOrgWhiteAttention() {
        return orgWhiteAttention;
    }

    public void setOrgWhiteAttention(long orgWhiteAttention) {
        this.orgWhiteAttention = orgWhiteAttention;
    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public String getDataDesc() {
		return dataDesc;
	}

	public void setDataDesc(String dataDesc) {
		this.dataDesc = dataDesc;
	}

	public String getDataCode() {
		return dataCode;
	}

	public void setDataCode(String dataCode) {
		this.dataCode = dataCode;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public long getMarketAttention() {
		return marketAttention;
	}

	public void setMarketAttention(long marketAttention) {
		this.marketAttention = marketAttention;
	}

	public long getOrgAttention() {
		return orgAttention;
	}

	public void setOrgAttention(long orgAttention) {
		this.orgAttention = orgAttention;
	}


	public long getMarketContentNum() {
		return marketContentNum;
	}

	public void setMarketContentNum(long marketContentNum) {
		this.marketContentNum = marketContentNum;
	}

	public long getOrgContentNum() {
		return orgContentNum;
	}

	public void setOrgContentNum(long orgContentNum) {
		this.orgContentNum = orgContentNum;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
