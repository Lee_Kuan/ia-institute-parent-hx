package com.meix.institute.vo.file;

import java.io.Serializable;

/**
 * @版权信息	:上海睿涉取有限公司
 * @部门		:开发部
 * @作者		:zhouwei
 * @E-mail  : zhouwei@51research.com
 * @创建日期	: 2014年11月4日
 * @Description:上传文件属性
 */
public class FileAttr implements Serializable{
	private static final long serialVersionUID = 7081031963376701773L;
	private String origin;//原始名
	private String url;//链接地址
	private long size;//大小
	private String type;//类型 (后缀名：例如 mp3, png, jpeg 等等)
	private long ct;//创建时间 unix时间戳
	private int mt;//时长 音频时长(秒)
	private int w;//图片宽度 （暂时废弃）
	private int h;//图片高度 （暂时废弃）
   
	public int getW() {
		return w;
	}
	public void setW(int w) {
		this.w = w;
	}
	public int getH() {
		return h;
	}
	public void setH(int h) {
		this.h = h;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getCt() {
		return ct;
	}
	public void setCt(long ct) {
		this.ct = ct;
	}
	public int getMt() {
		return mt;
	}
	public void setMt(int mt) {
		this.mt = mt;
	}
	@Override
	public String toString() {
		return "FileAttr [origin=" + origin + ", url=" + url + ", size=" + size
				+ ", type=" + type + ", ct=" + ct + ", mt=" + mt + ", w=" + w
				+ ", h=" + h + "]";
	}
	
}
