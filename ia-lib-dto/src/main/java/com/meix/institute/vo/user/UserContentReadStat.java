package com.meix.institute.vo.user;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户内容阅读统计
 * Created by zenghao on 2020/3/31.
 */
@Data
public class UserContentReadStat implements Serializable {
	private static final long serialVersionUID = 7112856893164347668L;

	private long dataId;
	private int dataType;
	private String title;
	private String createTime;
	private long readNum;//阅读数
	private long readerNum;//阅读人数
}
