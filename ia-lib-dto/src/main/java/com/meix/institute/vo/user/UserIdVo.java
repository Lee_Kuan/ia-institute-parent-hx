package com.meix.institute.vo.user;

import java.io.Serializable;

public class UserIdVo implements Serializable {
	private static final long serialVersionUID = -6381425244292890770L;
	private long id;
	private int type;//4、每市用户 19、专家
	private int focusStatus = 1;//关注状态  0 未关注 1 已关注 2 互相关注

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getFocusStatus() {
		return focusStatus;
	}

	public void setFocusStatus(int focusStatus) {
		this.focusStatus = focusStatus;
	}
}
