package com.meix.institute.vo.activity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.meix.institute.vo.GroupPowerVo;

/**
 * 研究所合规监控
 * Created by zenghao on 2019/7/31.
 */
public class InsActivityVo implements Serializable {

	private static final long serialVersionUID = 3153943904299493055L;
	private Long id;

	/**
	 * 状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
	 */
	private Integer activityStatus;

	/**
	 * 公司代码
	 */
	private Long companyCode;

	/**
	 * 发起人机构名称(创建人机构)
	 */
	private String companyAbbr = "";

	/**
	 * 活动类型：6 调研 7电话会议 8路演 11策略 12会议
	 */
	private Integer activityType;

	/**
	 * 标题
	 */
	private String title = "";

	/**
	 * 调研对象或宏观策略
	 */
	private String researchObject = "";

	/**
	 * 开始时间
	 */
	private Date startTime;

	/**
	 * 结束时间
	 */
	private Date endTime;

	/**
	 * 报名截止时间
	 */
	private Date applyDeadlineTime;

	/**
	 * 详细地址
	 */
	private String address = "";

	/**
	 * 地图方位
	 */
	private String mapPosition = "";

	/**
	 * 活动描述
	 */
	private String activityDesc = "";

	/**
	 * 城市
	 */
	private String city = "";

	/**
	 * 关闭时间
	 */
	private Date closeTime;

	/**
	 * 操作标志：1激活 -1 取消
	 */
	private Short flag;

	/**
	 * 纬度: 做整数处理(10000)
	 */
	private Integer latitude;

	/**
	 * 经度：做整数处理
	 */
	private Integer longitude;

	/**
	 * 开发标志： 0不开放 1 开放
	 */
	private Short openFlag;

	/**
	 * 外部链接地址
	 */
	private String url = "";

	/**
	 * 文件属性:json方式存放
	 */
	private String fileAttr;
	//音频文件地址
	private String audioUrl = "";

	/**
	 * 子类型 0普通类型 1金组合
	 */
	private Integer subType;

	/**
	 * 直播间编号
	 */
	private Integer roomNo;

	/**
	 * 会议状态 NULL：根据会议时间判断会议状态 3 回放 4 进行中
	 */
	private Short isEnd;

	/**
	 * 活动海报链接地址
	 */
	private String resourceUrl = "";

	/**
	 * 活动海报链接地址(小图)
	 */
	private String resourceUrlSmall = "";

	/**
	 * 播放类型 0 未填写(默认) 1 直播 2 转播 3 录音
	 */
	private Short playType;

	/**
	 * 录音隐藏天数
	 */
	private Integer audioHideDay;

	/**
	 * 纪要文字链接
	 */
	private String summaryUrl = "";

	/**
	 * 电话会议ppt
	 */
	private String pptImg;

	/**
	 * 是否隐藏 0-否，1-是 （研究所新增）
	 */
	private Integer isHiden;

	/**
	 * 会议类型 0-非直播会议，1-直播263, 3-直播录音 （研究所新增）
	 */
	private Integer meetingType;

	/**
	 * 会议标签 0-普通会议， 1-专场会议， 2-付费会议， 3-易私募精品课
	 */
	private Integer meetingLabel;

	private String rtmp = "";

	private String hls = "";

	private Boolean thirdAuth;

	private Boolean isWhitelist;

	private Boolean canReplay;

	/**
	 * 操作人UID
	 */
	private String operatorUid = "";

	/**
	 * 操作人姓名
	 */
	private String operatorName = "";

	/**
	 * 撤销原因
	 */
	private String undoReason = "";

	/**
	 * 拒绝原因
	 */
	private String refuseReason = "";

	/**
	 * 创建时间
	 */
	private Date createdAt;

	/**
	 * 创建人
	 */
	private String createdBy = "";

	/**
	 * 更新时间
	 */
	private Date updatedAt;

	/**
	 * 更新人
	 */
	private String updatedBy = "";

	private String authorName = "";//作者名字
	private Integer userState;//用户在职状态 0-在职，1-离职
	private Date editTime;
	private String editUserName = "";//编辑人名
	private List<Integer> issueTypeList;//发布渠道
	private String shareName;
	private Integer share;
	private List<Map<String, Object>> shareList;
	private Integer webcastType;
	private Integer replayType;

	private List<ActivityAnalystVo> analyst;
	private List<ActivityContactVo> contactList;
	private List<ActivityContactVo> contact;
	private List<ActivityRelevantVo> visitOrg;
	private List<ActivityRelevantVo> jdPeople;
	private List<ActivityRelevantVo> lyPeople;
	private String summary = "";
	private List<ActivityTargetVo> targetList;

	private List<ActivityTargetVo> secuList;
	private List<ActivityTargetVo> industryList;
	/**
	 * 录音时长
	 */
	private Integer audioDuration;
	private int isJoinIn;//是否加入会议  0未参加 1 参加
	private int joinStatus;

	private String webcastId;//直播ID
	private String webcastNumber;//直播编号
	private String organizerToken;//组织者口令
	private String panelistToken;//嘉宾口令
	private String attendeeToken;//普通参加者口令
	private String organizerJoinUrl;//组织者加入URL
	private String panelistJoinUrl;//嘉宾加入URL
	private String attendeeAShortJoinUrl;//普通参加者加入URL(不带token)
	private String attendeeJoinUrl;//普通参加者加入URL(带token)

	private String vodId;//点播ID
	private String vodNumber;//点播编号

	private int permission;// 是否有权限 0 没有 1 有
	private int isCompanyWhiteList;//是否是机构的白名单 0不是 1是
	private int applyStatus;//申请公司的白名单权限状态 -1 未申请 0申请中 1成功 2失败

	private Date publishDate;

	private Date undoTime;

	private long attendPersonCount;//参会人数

	//未审核的报名人数
	private int unExamineJoinPersonCount;

	private int activityLabel; // 活动标签：  1：报名开始；2：报名结束；3：活动开始；4：活动结束；5：直播预告；6：直播中；7：直播结束；8：回放

	private int liveStyle; // 0 音频  1 视频 

	private int isArtificial;
	private String joinNumber;
	private String joinPassword;

	private long createUid;
	
	private List<GroupPowerVo> groupPower;
	
	public int getJoinStatus() {
		return joinStatus;
	}

	public void setJoinStatus(int joinStatus) {
		this.joinStatus = joinStatus;
	}

	public List<GroupPowerVo> getGroupPower() {
		return groupPower;
	}

	public void setGroupPower(List<GroupPowerVo> groupPower) {
		this.groupPower = groupPower;
	}

	public long getCreateUid() {
		return createUid;
	}

	public void setCreateUid(long createUid) {
		this.createUid = createUid;
	}

	public int getIsArtificial() {
		return isArtificial;
	}

	public void setIsArtificial(int isArtificial) {
		this.isArtificial = isArtificial;
	}

	public String getJoinNumber() {
		return joinNumber;
	}

	public void setJoinNumber(String joinNumber) {
		this.joinNumber = joinNumber;
	}

	public String getJoinPassword() {
		return joinPassword;
	}

	public void setJoinPassword(String joinPassword) {
		this.joinPassword = joinPassword;
	}

	public Integer getShare() {
		return share;
	}

	public void setShare(Integer share) {
		this.share = share;
	}

	public List<Map<String, Object>> getShareList() {
		return shareList;
	}

	public void setShareList(List<Map<String, Object>> shareList) {
		this.shareList = shareList;
	}

	public Integer getWebcastType() {
		return webcastType;
	}

	public void setWebcastType(Integer webcastType) {
		this.webcastType = webcastType;
	}

	public Integer getReplayType() {
		return replayType;
	}

	public void setReplayType(Integer replayType) {
		this.replayType = replayType;
	}

	public int getLiveStyle() {
		return liveStyle;
	}

	public void setLiveStyle(int liveStyle) {
		this.liveStyle = liveStyle;
	}

	public Boolean getThirdAuth() {
		return thirdAuth;
	}

	public void setThirdAuth(Boolean thirdAuth) {
		this.thirdAuth = thirdAuth;
	}

	public Boolean getIsWhitelist() {
		return isWhitelist;
	}

	public void setIsWhitelist(Boolean isWhitelist) {
		this.isWhitelist = isWhitelist;
	}

	public Boolean getCanReplay() {
		return canReplay;
	}

	public void setCanReplay(Boolean canReplay) {
		this.canReplay = canReplay;
	}

	public int getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(int activityLabel) {
		this.activityLabel = activityLabel;
	}

	public int getUnExamineJoinPersonCount() {
		return unExamineJoinPersonCount;
	}

	public void setUnExamineJoinPersonCount(int unExamineJoinPersonCount) {
		this.unExamineJoinPersonCount = unExamineJoinPersonCount;
	}

	public List<ActivityContactVo> getContact() {
		return contact;
	}

	public void setContact(List<ActivityContactVo> contact) {
		this.contact = contact;
	}

	public Date getUndoTime() {
		return undoTime;
	}

	public void setUndoTime(Date undoTime) {
		this.undoTime = undoTime;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getActivityStatus() {
		return activityStatus;
	}

	public void setActivityStatus(Integer activityStatus) {
		this.activityStatus = activityStatus;
	}

	public Long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(Long companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyAbbr() {
		return companyAbbr;
	}

	public void setCompanyAbbr(String companyAbbr) {
		this.companyAbbr = companyAbbr;
	}

	public Integer getActivityType() {
		return activityType;
	}

	public void setActivityType(Integer activityType) {
		this.activityType = activityType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResearchObject() {
		return researchObject;
	}

	public void setResearchObject(String researchObject) {
		this.researchObject = researchObject;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getApplyDeadlineTime() {
		return applyDeadlineTime;
	}

	public void setApplyDeadlineTime(Date applyDeadlineTime) {
		this.applyDeadlineTime = applyDeadlineTime;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMapPosition() {
		return mapPosition;
	}

	public void setMapPosition(String mapPosition) {
		this.mapPosition = mapPosition;
	}

	public String getActivityDesc() {
		return activityDesc;
	}

	public void setActivityDesc(String activityDesc) {
		this.activityDesc = activityDesc;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	public Short getFlag() {
		return flag;
	}

	public void setFlag(Short flag) {
		this.flag = flag;
	}

	public Integer getLatitude() {
		return latitude;
	}

	public void setLatitude(Integer latitude) {
		this.latitude = latitude;
	}

	public Integer getLongitude() {
		return longitude;
	}

	public void setLongitude(Integer longitude) {
		this.longitude = longitude;
	}

	public Short getOpenFlag() {
		return openFlag;
	}

	public void setOpenFlag(Short openFlag) {
		this.openFlag = openFlag;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFileAttr() {
		return fileAttr;
	}

	public void setFileAttr(String fileAttr) {
		this.fileAttr = fileAttr;
	}

	public String getAudioUrl() {
		return audioUrl;
	}

	public void setAudioUrl(String audioUrl) {
		this.audioUrl = audioUrl;
	}

	public Integer getSubType() {
		return subType;
	}

	public void setSubType(Integer subType) {
		this.subType = subType;
	}

	public Integer getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(Integer roomNo) {
		this.roomNo = roomNo;
	}

	public Short getIsEnd() {
		return isEnd;
	}

	public void setIsEnd(Short isEnd) {
		this.isEnd = isEnd;
	}

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	public String getResourceUrlSmall() {
		return resourceUrlSmall;
	}

	public void setResourceUrlSmall(String resourceUrlSmall) {
		this.resourceUrlSmall = resourceUrlSmall;
	}

	public Short getPlayType() {
		return playType;
	}

	public void setPlayType(Short playType) {
		this.playType = playType;
	}

	public Integer getAudioHideDay() {
		return audioHideDay;
	}

	public void setAudioHideDay(Integer audioHideDay) {
		this.audioHideDay = audioHideDay;
	}

	public String getSummaryUrl() {
		return summaryUrl;
	}

	public void setSummaryUrl(String summaryUrl) {
		this.summaryUrl = summaryUrl;
	}

	public String getPptImg() {
		return pptImg;
	}

	public void setPptImg(String pptImg) {
		this.pptImg = pptImg;
	}

	public Integer getIsHiden() {
		return isHiden;
	}

	public void setIsHiden(Integer isHiden) {
		this.isHiden = isHiden;
	}

	public Integer getMeetingType() {
		return meetingType;
	}

	public void setMeetingType(Integer meetingType) {
		this.meetingType = meetingType;
	}

	public Integer getMeetingLabel() {
		return meetingLabel;
	}

	public void setMeetingLabel(Integer meetingLabel) {
		this.meetingLabel = meetingLabel;
	}

	public String getRtmp() {
		return rtmp;
	}

	public void setRtmp(String rtmp) {
		this.rtmp = rtmp;
	}

	public String getHls() {
		return hls;
	}

	public void setHls(String hls) {
		this.hls = hls;
	}

	public String getOperatorUid() {
		return operatorUid;
	}

	public void setOperatorUid(String operatorUid) {
		this.operatorUid = operatorUid;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getUndoReason() {
		return undoReason;
	}

	public void setUndoReason(String undoReason) {
		this.undoReason = undoReason;
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Integer getUserState() {
		return userState;
	}

	public void setUserState(Integer userState) {
		this.userState = userState;
	}

	public String getEditUserName() {
		return editUserName;
	}

	public void setEditUserName(String editUserName) {
		this.editUserName = editUserName;
	}

	public List<Integer> getIssueTypeList() {
		return issueTypeList;
	}

	public void setIssueTypeList(List<Integer> issueTypeList) {
		this.issueTypeList = issueTypeList;
	}

	public String getShareName() {
		return shareName;
	}

	public void setShareName(String shareName) {
		this.shareName = shareName;
	}

	public List<ActivityAnalystVo> getAnalyst() {
		return analyst;
	}

	public void setAnalyst(List<ActivityAnalystVo> analyst) {
		this.analyst = analyst;
	}

	public List<ActivityContactVo> getContactList() {
		return contactList;
	}

	public void setContactList(List<ActivityContactVo> contactList) {
		this.contactList = contactList;
	}

	public List<ActivityRelevantVo> getVisitOrg() {
		return visitOrg;
	}

	public void setVisitOrg(List<ActivityRelevantVo> visitOrg) {
		this.visitOrg = visitOrg;
	}

	public List<ActivityRelevantVo> getJdPeople() {
		return jdPeople;
	}

	public void setJdPeople(List<ActivityRelevantVo> jdPeople) {
		this.jdPeople = jdPeople;
	}

	public List<ActivityRelevantVo> getLyPeople() {
		return lyPeople;
	}

	public void setLyPeople(List<ActivityRelevantVo> lyPeople) {
		this.lyPeople = lyPeople;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public List<ActivityTargetVo> getTargetList() {
		return targetList;
	}

	public void setTargetList(List<ActivityTargetVo> targetList) {
		this.targetList = targetList;
	}

	public Integer getAudioDuration() {
		return audioDuration;
	}

	public void setAudioDuration(Integer audioDuration) {
		this.audioDuration = audioDuration;
	}

	public List<ActivityTargetVo> getSecuList() {
		return secuList;
	}

	public void setSecuList(List<ActivityTargetVo> secuList) {
		this.secuList = secuList;
	}

	public List<ActivityTargetVo> getIndustryList() {
		return industryList;
	}

	public void setIndustryList(List<ActivityTargetVo> industryList) {
		this.industryList = industryList;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

	public int getIsJoinIn() {
		return isJoinIn;
	}

	public void setIsJoinIn(int isJoinIn) {
		this.isJoinIn = isJoinIn;
	}

	public String getWebcastId() {
		return webcastId;
	}

	public void setWebcastId(String webcastId) {
		this.webcastId = webcastId;
	}

	public String getWebcastNumber() {
		return webcastNumber;
	}

	public void setWebcastNumber(String webcastNumber) {
		this.webcastNumber = webcastNumber;
	}

	public String getOrganizerToken() {
		return organizerToken;
	}

	public void setOrganizerToken(String organizerToken) {
		this.organizerToken = organizerToken;
	}

	public String getPanelistToken() {
		return panelistToken;
	}

	public void setPanelistToken(String panelistToken) {
		this.panelistToken = panelistToken;
	}

	public String getAttendeeToken() {
		return attendeeToken;
	}

	public void setAttendeeToken(String attendeeToken) {
		this.attendeeToken = attendeeToken;
	}

	public String getOrganizerJoinUrl() {
		return organizerJoinUrl;
	}

	public void setOrganizerJoinUrl(String organizerJoinUrl) {
		this.organizerJoinUrl = organizerJoinUrl;
	}

	public String getPanelistJoinUrl() {
		return panelistJoinUrl;
	}

	public void setPanelistJoinUrl(String panelistJoinUrl) {
		this.panelistJoinUrl = panelistJoinUrl;
	}

	public String getAttendeeAShortJoinUrl() {
		return attendeeAShortJoinUrl;
	}

	public void setAttendeeAShortJoinUrl(String attendeeAShortJoinUrl) {
		this.attendeeAShortJoinUrl = attendeeAShortJoinUrl;
	}

	public String getAttendeeJoinUrl() {
		return attendeeJoinUrl;
	}

	public void setAttendeeJoinUrl(String attendeeJoinUrl) {
		this.attendeeJoinUrl = attendeeJoinUrl;
	}

	public String getVodId() {
		return vodId;
	}

	public void setVodId(String vodId) {
		this.vodId = vodId;
	}

	public String getVodNumber() {
		return vodNumber;
	}

	public void setVodNumber(String vodNumber) {
		this.vodNumber = vodNumber;
	}

	public int getPermission() {
		return permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}

	public int getIsCompanyWhiteList() {
		return isCompanyWhiteList;
	}

	public void setIsCompanyWhiteList(int isCompanyWhiteList) {
		this.isCompanyWhiteList = isCompanyWhiteList;
	}

	public int getApplyStatus() {
		return applyStatus;
	}

	public void setApplyStatus(int applyStatus) {
		this.applyStatus = applyStatus;
	}

	public long getAttendPersonCount() {
		return attendPersonCount;
	}

	public void setAttendPersonCount(long attendPersonCount) {
		this.attendPersonCount = attendPersonCount;
	}
}
