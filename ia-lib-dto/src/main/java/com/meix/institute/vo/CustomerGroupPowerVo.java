package com.meix.institute.vo;

import java.io.Serializable;

public class CustomerGroupPowerVo implements Serializable{

	private static final long serialVersionUID = 4936004885134082046L;
	
	private long powerId;
	private String powerName;
	private int dataType;
	private int dataSubType;
	private int share;
	private String shareName;
	private long level;
	private int isSelect;
	private int flag;
	
	public long getPowerId() {
		return powerId;
	}
	public void setPowerId(long powerId) {
		this.powerId = powerId;
	}
	public String getPowerName() {
		return powerName;
	}
	public void setPowerName(String powerName) {
		this.powerName = powerName;
	}
	public int getDataType() {
		return dataType;
	}
	public void setDataType(int dataType) {
		this.dataType = dataType;
	}
	public int getDataSubType() {
		return dataSubType;
	}
	public void setDataSubType(int dataSubType) {
		this.dataSubType = dataSubType;
	}
	public int getShare() {
		return share;
	}
	public void setShare(int share) {
		this.share = share;
	}
	public String getShareName() {
		return shareName;
	}
	public void setShareName(String shareName) {
		this.shareName = shareName;
	}
	public long getLevel() {
		return level;
	}
	public void setLevel(long level) {
		this.level = level;
	}
	public int getIsSelect() {
		return isSelect;
	}
	public void setIsSelect(int isSelect) {
		this.isSelect = isSelect;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	
}
