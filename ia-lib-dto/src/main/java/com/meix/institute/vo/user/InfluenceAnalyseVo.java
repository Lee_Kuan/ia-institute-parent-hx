package com.meix.institute.vo.user;

import lombok.Data;

/**
 * 影响力分析统计
 * Created by zenghao on 2020/3/30.
 */
@Data
public class InfluenceAnalyseVo {

	private int dataType;
	private long uid;
	private String userName;
	private String companyAbbr;
	private long count;//研报阅读数 活动报名数

}
