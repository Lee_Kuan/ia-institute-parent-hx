package com.meix.institute.vo.compliance;

/**
 * Created by zenghao on 2019/10/31.
 */
public class ReportComplianceVo extends ComplianceVo {
	private static final long serialVersionUID = -6762044234339733028L;

	private String infoType;

	public String getInfoType() {
		return infoType;
	}

	public void setInfoType(String infoType) {
		this.infoType = infoType;
	}
}
