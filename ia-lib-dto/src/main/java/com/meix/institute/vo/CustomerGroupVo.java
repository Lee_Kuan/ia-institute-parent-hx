package com.meix.institute.vo;

import java.io.Serializable;

public class CustomerGroupVo implements Serializable {
	private static final long serialVersionUID = 9093417843657020650L;
	
	private long id;
	private String groupName;
	private int groupType;
	private int createVia; //0-系统创建，1-手动创建
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public int getGroupType() {
		return groupType;
	}
	public void setGroupType(int groupType) {
		this.groupType = groupType;
	}
	public int getCreateVia() {
		return createVia;
	}
	public void setCreateVia(int createVia) {
		this.createVia = createVia;
	}
}
