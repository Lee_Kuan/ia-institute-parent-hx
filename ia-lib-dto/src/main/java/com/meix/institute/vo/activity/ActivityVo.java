package com.meix.institute.vo.activity;


import com.meix.institute.vo.BaseVo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ActivityVo extends BaseVo implements Serializable {
	private static final long serialVersionUID = 6399122485251607105L;

	public ActivityVo() {
		super();
		dataType = 8;
	}

	private long id;

	private int activityType;

	private String authorName;

	private long authorCode;

	private String authorHeadImgUrl;

	private long orgCode;

	private String orgName;

	private String title;

	private String researchObject;

	/**
	 * Map{
	 * innerCode,
	 * secuCode,
	 * relationType,
	 * secuAbbr
	 * }
	 */
	private List<Map<String, Object>> secuList;

	/**
	 * Map{
	 * contact,
	 * mobile
	 * }
	 */
	private List<Map<String, Object>> contact;

	private String address;

	private String activityDesc;

	private String startTime;

	private String endTime;

	private String createTime;

	private String fileAttr;//文件属性
	private int playType;//播放类型   0 未填写(默认) 1 直播 2 转播 3 录音

	/**
	 * Map{
	 * name
	 * code
	 * }
	 */
	private List<Map<String, Object>> analyst;

	private int isEnd;// 是否结束  0 未结束 1 结束
	private String city;//城市
	private String resourceUrl;//活动缩略图地址
	private String resourceUrlSmall;//活动缩略图地址(小图标)

	private int followFlag;//是否关注了该作者 0未关注 1关注


	public String getResourceUrlSmall() {
		return resourceUrlSmall;
	}

	public void setResourceUrlSmall(String resourceUrlSmall) {
		this.resourceUrlSmall = resourceUrlSmall;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getIsEnd() {
		return isEnd;
	}

	public void setIsEnd(int isEnd) {
		this.isEnd = isEnd;
	}

	public List<Map<String, Object>> getAnalyst() {
		return analyst;
	}

	public void setAnalyst(List<Map<String, Object>> analyst) {
		this.analyst = analyst;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getActivityType() {
		return activityType;
	}

	public void setActivityType(int activityType) {
		this.activityType = activityType;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public long getAuthorCode() {
		return authorCode;
	}

	public void setAuthorCode(long authorCode) {
		this.authorCode = authorCode;
	}

	public String getAuthorHeadImgUrl() {
		return authorHeadImgUrl;
	}

	public void setAuthorHeadImgUrl(String authorHeadImgUrl) {
		this.authorHeadImgUrl = authorHeadImgUrl;
	}

	public long getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(long orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResearchObject() {
		return researchObject;
	}

	public void setResearchObject(String researchObject) {
		this.researchObject = researchObject;
	}

	public List<Map<String, Object>> getSecuList() {
		return secuList;
	}

	public void setSecuList(List<Map<String, Object>> secuList) {
		this.secuList = secuList;
	}

	public List<Map<String, Object>> getContact() {
		return contact;
	}

	public void setContact(List<Map<String, Object>> contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getActivityDesc() {
		return activityDesc;
	}

	public void setActivityDesc(String activityDesc) {
		this.activityDesc = activityDesc;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	public String getFileAttr() {
		return fileAttr;
	}

	public void setFileAttr(String fileAttr) {
		this.fileAttr = fileAttr;
	}

	public int getPlayType() {
		return playType;
	}

	public void setPlayType(int playType) {
		this.playType = playType;
	}

	public int getFollowFlag() {
		return followFlag;
	}

	public void setFollowFlag(int followFlag) {
		this.followFlag = followFlag;
	}
}
