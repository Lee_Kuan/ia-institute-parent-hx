package com.meix.institute.vo.stock;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by zenghao on 2019/7/3.
 */
public class StockYieldVo implements Serializable {
	private static final long serialVersionUID = 8417324298142445289L;

	private int innerCode;
	private BigDecimal yieldRate;

	public int getInnerCode() {
		return innerCode;
	}

	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}

	public BigDecimal getYieldRate() {
		return yieldRate;
	}

	public void setYieldRate(BigDecimal yieldRate) {
		this.yieldRate = yieldRate;
	}
}
