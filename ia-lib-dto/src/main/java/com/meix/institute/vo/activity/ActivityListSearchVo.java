package com.meix.institute.vo.activity;

import java.io.Serializable;
import java.util.List;

public class ActivityListSearchVo implements Serializable {
	private static final long serialVersionUID = 4811980398869695472L;

	private long uid;

	private int activityType;

	private int activityRange;

	private int showNum;

	private int currentPage;

	private int shareType;

	private String orderStr;

	private String startDateTime;

	private String endDateTime;

	private String city;

	private String condition;

	private List<Integer> list;//查询的行业内码列表

	private long buyerCompanyCode;
	private long companyCode;//机构代码

	private int accountType;//账户类型

	private List<String> citys;

	private List<Integer> industryCodes;

	private int searchType;//搜索类型

	private String customQuery;//自定义查询条件语句

	private long activityId;//查询时的基准活动Id

	private int queryFlag;//查询方向标识 -1向前获取 1 向后获取

	private int getExtraList = 0;//是否获取额外列表数据 0 不获取 1 获取

	private String endTime;

	private int shareQueryFlag;//微信分享查询标志 0 普通查询（默认） 1 微信分享查询

	private int checkPermission = 1;//是否做权限过滤 0 不做权限过滤 1 做权限过滤

	private int isEnd = 0;//3 回放状态  10 正在进行中的或预告的金组合电话会议

	private int hasOrderFlag = 0;//是否有排序语句（用于自定义查询中） 0 没有 1 有

	private int stockRange;//股票范围 0 全部 1自选股 2 单个股票 3 组合相关个股

	private long categoryId;//自选股目录ID

	private List<Long> analystList;//分析师

	private int additionalFlag; //追加标志

	private int clientType;//客户端类型，1-美市，8-公众号，用来和发布渠道关联，对活动进行屏蔽

	private long insUid;//研究所内部表id
	private String mobile;//用户手机号，匹配我参加的活动

	public int getAdditionalFlag() {
		return additionalFlag;
	}

	public void setAdditionalFlag(int additionalFlag) {
		this.additionalFlag = additionalFlag;
	}

	public List<Long> getAnalystList() {
		return analystList;
	}

	public void setAnalystList(List<Long> analystList) {
		this.analystList = analystList;
	}

	public int getSearchType() {
		return searchType;
	}

	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}

	public List<Integer> getIndustryCodes() {
		return industryCodes;
	}

	public void setIndustryCodes(List<Integer> industryCodes) {
		this.industryCodes = industryCodes;
	}

	public List<String> getCitys() {
		return citys;
	}

	public void setCitys(List<String> citys) {
		this.citys = citys;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public long getBuyerCompanyCode() {
		return buyerCompanyCode;
	}

	public void setBuyerCompanyCode(long buyerCompanyCode) {
		this.buyerCompanyCode = buyerCompanyCode;
	}

	public List<Integer> getList() {
		return list;
	}

	public void setList(List<Integer> list) {
		this.list = list;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public int getActivityType() {
		return activityType;
	}

	public void setActivityType(int activityType) {
		this.activityType = activityType;
	}

	public int getActivityRange() {
		return activityRange;
	}

	public void setActivityRange(int activityRange) {
		this.activityRange = activityRange;
	}

	public int getShowNum() {
		return showNum;
	}

	public void setShowNum(int showNum) {
		this.showNum = showNum;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getShareType() {
		return shareType;
	}

	public void setShareType(int shareType) {
		this.shareType = shareType;
	}

	public String getOrderStr() {
		return orderStr;
	}

	public void setOrderStr(String orderStr) {
		this.orderStr = orderStr;
	}

	public String getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getCustomQuery() {
		return customQuery;
	}

	public void setCustomQuery(String customQuery) {
		this.customQuery = customQuery;
	}

	public long getActivityId() {
		return activityId;
	}

	public void setActivityId(long activityId) {
		this.activityId = activityId;
	}

	public int getQueryFlag() {
		return queryFlag;
	}

	public void setQueryFlag(int queryFlag) {
		this.queryFlag = queryFlag;
	}

	public int getGetExtraList() {
		return getExtraList;
	}

	public void setGetExtraList(int getExtraList) {
		this.getExtraList = getExtraList;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getShareQueryFlag() {
		return shareQueryFlag;
	}

	public void setShareQueryFlag(int shareQueryFlag) {
		this.shareQueryFlag = shareQueryFlag;
	}

	public int getCheckPermission() {
		return checkPermission;
	}

	public void setCheckPermission(int checkPermission) {
		this.checkPermission = checkPermission;
	}

	public int getIsEnd() {
		return isEnd;
	}

	public void setIsEnd(int isEnd) {
		this.isEnd = isEnd;
	}

	public int getHasOrderFlag() {
		return hasOrderFlag;
	}

	public void setHasOrderFlag(int hasOrderFlag) {
		this.hasOrderFlag = hasOrderFlag;
	}

	public int getStockRange() {
		return stockRange;
	}

	public void setStockRange(int stockRange) {
		this.stockRange = stockRange;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public int getClientType() {
		return clientType;
	}

	public void setClientType(int clientType) {
		this.clientType = clientType;
	}

	public long getInsUid() {
		return insUid;
	}

	public void setInsUid(long insUid) {
		this.insUid = insUid;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}
