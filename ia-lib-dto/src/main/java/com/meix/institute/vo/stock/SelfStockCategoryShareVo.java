package com.meix.institute.vo.stock;

import java.io.Serializable;

/**
 * @版权信息	:上海睿涉取有限公司
 * @部门		:开发部
 * @作者		:zhouwei
 * @E-mail  : zhouwei@51research.com
 * @创建日期	: 2015年1月19日
 * @Description: 自选股池分享
 */
public class SelfStockCategoryShareVo implements Serializable{
	private static final long serialVersionUID = 1L;
	private long groupId;//行业分组ID
	private long categoryId;//
	private String categoryName;//
	private int type;//1 机构  2 研究员 0 全部
	private long code;//代码
	private String createTime;//创建时间
	private String updateTime;//修改时间
	private int selected;//0 取消选中  1选中
	private String industryCodes;//行业代码(复数)
	private int industryCode;//行业代码
	
	public int getIndustryCode() {
		return industryCode;
	}
	public void setIndustryCode(int industryCode) {
		this.industryCode = industryCode;
	}
	public String getIndustryCodes() {
		return industryCodes;
	}
	public void setIndustryCodes(String industryCodes) {
		this.industryCodes = industryCodes;
	}
	public int getSelected() {
		return selected;
	}
	public void setSelected(int selected) {
		this.selected = selected;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public long getGroupId() {
		return groupId;
	}
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public long getCode() {
		return code;
	}
	public void setCode(long code) {
		this.code = code;
	}
	
}