package com.meix.institute.vo.user;

import java.io.Serializable;

public class UserPersonasVo extends UserPersonas implements Serializable {

	private static final long serialVersionUID = -6998331473573708763L;
	private long insCompanyCode;
	private String uuid;
	private String mobile;
	private String authorUuid;
	private String authorMobile;

	public long getInsCompanyCode() {
		return insCompanyCode;
	}

	public void setInsCompanyCode(long insCompanyCode) {
		this.insCompanyCode = insCompanyCode;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAuthorUuid() {
		return authorUuid;
	}

	public void setAuthorUuid(String authorUuid) {
		this.authorUuid = authorUuid;
	}

	public String getAuthorMobile() {
		return authorMobile;
	}

	public void setAuthorMobile(String authorMobile) {
		this.authorMobile = authorMobile;
	}
}
