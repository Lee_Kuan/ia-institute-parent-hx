package com.meix.institute.vo.stock;

import java.io.Serializable;

public class SelfStockItem implements Serializable {
	private static final long serialVersionUID = -9109745143823751672L;

	private long uid;
	private int innerCode;
	private int count;
	
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public int getInnerCode() {
		return innerCode;
	}
	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}
