package com.meix.institute.vo.user;

import lombok.Data;

/**
 * 影响力分析用户行为统计
 * Created by zenghao on 2020/3/31.
 */
@Data
public class InfluenceTrend {

	private long todayVisitorNum;//今日访客数
	private long todayVisitorChange;//今日访客数量变动

	private long monthActAttenderNum;//本月活动参加数
	private long todayActAttenderChange;//今日活动参加数变动
}
