package com.meix.institute.vo.stock;


import com.meix.institute.vo.BaseVo;

import java.io.Serializable;

/**
 * Created by zenghao on 2019/7/3.
 */
public class StockVo extends BaseVo implements Serializable {
	private static final long serialVersionUID = 2751470379518950533L;

	private long authorCode;//推荐人id
	private String authorName;//推荐人名字
	private String recDesc;//推荐理由
	private int innerCode;
	private String secuCode;
	private String secuAbbr;
	private String suffix;//后缀
	private int secuMarket;
	private int secuClass;//4 指数
	private int stop;//停牌标识
	private long companyCode;
	private int industryCode;
	private String industryName;
	private float dayYieldRate;
	private float monthYieldRate;
	private float accumulatedYieldRate;//创建以来收益率
	private String month;
	private int isSelfstock;
	private long categoryId;//自选股目录id
	private String categoryName;//目录名称
	private String createTime;

	public long getAuthorCode() {
		return authorCode;
	}

	public void setAuthorCode(long authorCode) {
		this.authorCode = authorCode;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getRecDesc() {
		return recDesc;
	}

	public void setRecDesc(String recDesc) {
		this.recDesc = recDesc;
	}

	public int getInnerCode() {
		return innerCode;
	}

	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}

	public String getSecuCode() {
		return secuCode;
	}

	public void setSecuCode(String secuCode) {
		this.secuCode = secuCode;
	}

	public String getSecuAbbr() {
		return secuAbbr;
	}

	public void setSecuAbbr(String secuAbbr) {
		this.secuAbbr = secuAbbr;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public int getSecuMarket() {
		return secuMarket;
	}

	public void setSecuMarket(int secuMarket) {
		this.secuMarket = secuMarket;
	}

	public int getSecuClass() {
		return secuClass;
	}

	public void setSecuClass(int secuClass) {
		this.secuClass = secuClass;
	}

	public int getStop() {
		return stop;
	}

	public void setStop(int stop) {
		this.stop = stop;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public int getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(int industryCode) {
		this.industryCode = industryCode;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public float getDayYieldRate() {
		return dayYieldRate;
	}

	public void setDayYieldRate(float dayYieldRate) {
		this.dayYieldRate = dayYieldRate;
	}

	public float getMonthYieldRate() {
		return monthYieldRate;
	}

	public void setMonthYieldRate(float monthYieldRate) {
		this.monthYieldRate = monthYieldRate;
	}

	public float getAccumulatedYieldRate() {
		return accumulatedYieldRate;
	}

	public void setAccumulatedYieldRate(float accumulatedYieldRate) {
		this.accumulatedYieldRate = accumulatedYieldRate;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getIsSelfstock() {
		return isSelfstock;
	}

	public void setIsSelfstock(int isSelfstock) {
		this.isSelfstock = isSelfstock;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
