package com.meix.institute.vo.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:用户点击状态
 */
@Data
public class UserRecordStateVo implements Serializable {
	private static final long serialVersionUID = -7469289676170683623L;
	private long id;
	private long uid;
	private long dataId;
	private int dataType;//1 调仓  2观点 3研报  
	private int dataState;//1 阅读  2 点赞 3关注 4提醒
	private String updateTime;
	private String createTime;
	private int deviceType;//请求类型 1 IOS 2 ANDROID 3网页 4 微信
	private long duration;//播放时长
	private int dataCause;//数据产生原因 1 偷看  0 正常
	private String openId;//外部ID
	private int ret;//保存后的状态 0:未关注 1：已关注 2：互相关注

	@Override
	public String toString() {
		return "UserRecordStateVo [id=" + id + ", uid=" + uid + ", dataId="
			+ dataId + ", dataType=" + dataType + ", dataState="
			+ dataState + ", updateTime=" + updateTime + ", createTime="
			+ createTime + ", deviceType=" + deviceType + ", duration="
			+ duration + "]";
	}

}
