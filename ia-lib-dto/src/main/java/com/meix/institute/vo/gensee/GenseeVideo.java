/**
 * @author jiawj 2018年1月24日
 */
package com.meix.institute.vo.gensee;

/**
 * 创建直播model
 */
public class GenseeVideo {

	//创建直播需要数据
	String subject;
	String startTime;
	String endTime;
	String telconf;
	String organizerToken;
	String panelistToken;
	String attendeeToken;
	String loginName;
	String password;
	
	//创建直播成功后返回数据
	String organizerJoinUrl;
	String panelistJoinUrl;
	String attendeeJoinUrl;
	String id;
	String number;
	String attendeeAShortJoinUrl;
	
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getTelconf() {
		return telconf;
	}
	public void setTelconf(String telconf) {
		this.telconf = telconf;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getOrganizerToken() {
		return organizerToken;
	}
	public void setOrganizerToken(String organizerToken) {
		this.organizerToken = organizerToken;
	}
	public String getPanelistToken() {
		return panelistToken;
	}
	public void setPanelistToken(String panelistToken) {
		this.panelistToken = panelistToken;
	}
	public String getAttendeeToken() {
		return attendeeToken;
	}
	public void setAttendeeToken(String attendeeToken) {
		this.attendeeToken = attendeeToken;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOrganizerJoinUrl() {
		return organizerJoinUrl;
	}
	public void setOrganizerJoinUrl(String organizerJoinUrl) {
		this.organizerJoinUrl = organizerJoinUrl;
	}
	public String getPanelistJoinUrl() {
		return panelistJoinUrl;
	}
	public void setPanelistJoinUrl(String panelistJoinUrl) {
		this.panelistJoinUrl = panelistJoinUrl;
	}
	public String getAttendeeJoinUrl() {
		return attendeeJoinUrl;
	}
	public void setAttendeeJoinUrl(String attendeeJoinUrl) {
		this.attendeeJoinUrl = attendeeJoinUrl;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getAttendeeAShortJoinUrl() {
		return attendeeAShortJoinUrl;
	}
	public void setAttendeeAShortJoinUrl(String attendeeAShortJoinUrl) {
		this.attendeeAShortJoinUrl = attendeeAShortJoinUrl;
	}
	@Override
	public String toString() {
		return "GeeseeVideo [subject=" + subject + ", startTime=" + startTime + ", organizerToken=" + organizerToken
				+ ", panelistToken=" + panelistToken + ", attendeeToken=" + attendeeToken + ", loginName=" + loginName
				+ ", password=" + password + ", organizerJoinUrl=" + organizerJoinUrl + ", panelistJoinUrl="
				+ panelistJoinUrl + ", attendeeJoinUrl=" + attendeeJoinUrl + ", id=" + id + ", number=" + number
				+ ", attendeeAShortJoinUrl=" + attendeeAShortJoinUrl + "]";
	}
}
