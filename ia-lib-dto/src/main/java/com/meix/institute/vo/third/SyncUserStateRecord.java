package com.meix.institute.vo.third;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by zenghao on 2020/4/16.
 */
@Data
public class SyncUserStateRecord implements Serializable {
	private static final long serialVersionUID = -7088401648455878350L;

	private long uid;
	private String mobile;
	private String userName;
	private String companyName;
	private int dataType;
	private long dataId;
	private int dataState;
	private long duration;//播放时长
	private int deviceType;//请求类型 1 IOS 2 ANDROID 3网页 4 微信
}
