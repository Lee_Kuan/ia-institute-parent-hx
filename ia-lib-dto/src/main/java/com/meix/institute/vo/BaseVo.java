package com.meix.institute.vo;

import java.io.Serializable;

public class BaseVo implements Serializable {
	private static final long serialVersionUID = -5317384994316244532L;
	protected int dataType = 0;//-1 广告 1 调仓  2观点 3研报 5 股票  6 模拟组合 8 活动 13晨会
	protected long dataId;
	protected String updateTime;

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
}
