package com.meix.institute.vo.activity;

import java.io.Serializable;

/**
 * 活动分析师
 *
 * @author likuan
 *         2019年8月6日11:31:34
 */
public class ActivityAnalystVo implements Serializable {

	private static final long serialVersionUID = -1782796607995246437L;
	private long code;    //分析师id
	private String name;//分析师名字
	private int followFlag;//关注标志
	private String headImageUrl;
	private String position;
	private String direction;
	private String headImg;

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFollowFlag() {
		return followFlag;
	}

	public void setFollowFlag(int followFlag) {
		this.followFlag = followFlag;
	}

	public String getHeadImageUrl() {
		return headImageUrl;
	}

	public void setHeadImageUrl(String headImageUrl) {
		this.headImageUrl = headImageUrl;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}
	
}
