package com.meix.institute.vo.user;

import java.io.Serializable;

/**
 * 研究所销售的用户
 */
public class InsCustomer implements Serializable {
	private static final long serialVersionUID = 4641766980085596003L;
	
	private long uid;
	private String companyName;
	private String userName;
	private String position;
	private String mobile;
	private int customerType; // 0-白名单，1-潜在用户
	
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getCustomerType() {
		return customerType;
	}
	public void setCustomerType(int customerType) {
		this.customerType = customerType;
	}
	
}
