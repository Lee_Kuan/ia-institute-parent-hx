package com.meix.institute.vo.company;

import java.io.Serializable;

/**
 * 机构公众号配置
 * Created by zenghao on 2019/7/10.
 */
public class CompanyWechatConfig implements Serializable {
	private static final long serialVersionUID = -3632809029588034048L;

	private long companyCode;
	private String appType;//公众号类型（哪个研究所）
	private String appId; 
	private String appSecret;
	private int clientType;

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public int getClientType() {
		return clientType;
	}

	public void setClientType(int clientType) {
		this.clientType = clientType;
	}
}
