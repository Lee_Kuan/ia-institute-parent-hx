package com.meix.institute.vo.wechat;

import java.io.Serializable;

/**
 * Created by zenghao on 2019/7/11.
 */
public class AccessTokenInfo implements Serializable {
	private static final long serialVersionUID = -3910949013086443787L;

	private int errcode;
	private String errmsg;
	private String ticket;
	private String access_token;
	private long expires_in;
	private String update_time;

	public int getErrcode() {
		return errcode;
	}

	public void setErrcode(int errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public long getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(long expires_in) {
		this.expires_in = expires_in;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

}
