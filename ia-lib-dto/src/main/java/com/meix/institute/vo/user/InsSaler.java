package com.meix.institute.vo.user;

import java.io.Serializable;

/**
 * 研究所销售
 * Created by zenghao on 2019/8/12.
 */
public class InsSaler implements Serializable {
	private static final long serialVersionUID = -944018502024231969L;

	private String mobile;
	private long salerUid;//ia_customer表id

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public long getSalerUid() {
		return salerUid;
	}

	public void setSalerUid(long salerUid) {
		this.salerUid = salerUid;
	}
}
