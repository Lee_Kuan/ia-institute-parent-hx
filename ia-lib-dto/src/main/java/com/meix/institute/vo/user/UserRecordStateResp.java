package com.meix.institute.vo.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserRecordStateResp implements Serializable {
	private static final long serialVersionUID = 1024935538383048306L;
	private long id;
	private long uid;
	private long dataId;
	private int dataType;//1 调仓  2观点 3研报  
	private int dataState;//1 阅读  2 点赞 3关注 4提醒
	private String createTime;
	private long duration;//播放时长

	//返回时补全信息
	private String userName;
	private String position;
	private String headImageUrl;
	private String companyAbbr;
	private String title;
	private int subType;//活动类型/研报子类型

}
