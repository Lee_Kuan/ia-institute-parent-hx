package com.meix.institute.vo;

import java.io.Serializable;

/**
 * 常量表
 * Created by zenghao on 2019/9/21.
 */
public class IaConstantVo implements Serializable {
	private static final long serialVersionUID = -2257533952542726845L;

	private long id;
	private int dm;
	private String ms;
	private int lb;
	private String createTime;
	private int iValue;
	private int innerCode;
	private String msjc;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getDm() {
		return dm;
	}

	public void setDm(int dm) {
		this.dm = dm;
	}

	public String getMs() {
		return ms;
	}

	public void setMs(String ms) {
		this.ms = ms;
	}

	public int getLb() {
		return lb;
	}

	public void setLb(int lb) {
		this.lb = lb;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public int getiValue() {
		return iValue;
	}

	public void setiValue(int iValue) {
		this.iValue = iValue;
	}

	public int getInnerCode() {
		return innerCode;
	}

	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}

	public String getMsjc() {
		return msjc;
	}

	public void setMsjc(String msjc) {
		this.msjc = msjc;
	}
}
