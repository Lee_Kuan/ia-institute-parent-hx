package com.meix.institute.vo.log;

import java.io.Serializable;

/**
 * 研究所用户画像对象(按手机号匹配)
 */
public class InsPersonaInfo implements Serializable {

	private static final long serialVersionUID = -148481189986926243L;
	private int channel; // 渠道
	private int userAction; // 用户行为

	private String mobile; // 用户id
	private String userName; // 用户名
	private String userCompanyName;
	private long dataId; // 数据id
	private int dataType; // 数据类型
	private long readDuration; // 阅读时长（秒）
	private long createTime; // 行为时间戳

	private String authorListJson;

	private String innerCode; // 股票内码(格式：123,898)
	private String compsIndustryCode; // 行业代码(格式：123,898)

	public int getChannel() {
		return channel;
	}

	public void setChannel(int channel) {
		this.channel = channel;
	}

	public int getUserAction() {
		return userAction;
	}

	public void setUserAction(int userAction) {
		this.userAction = userAction;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserCompanyName() {
		return userCompanyName;
	}

	public void setUserCompanyName(String userCompanyName) {
		this.userCompanyName = userCompanyName;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public long getReadDuration() {
		return readDuration;
	}

	public void setReadDuration(long readDuration) {
		this.readDuration = readDuration;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public String getAuthorListJson() {
		return authorListJson;
	}

	public void setAuthorListJson(String authorListJson) {
		this.authorListJson = authorListJson;
	}

	public String getInnerCode() {
		return innerCode;
	}

	public void setInnerCode(String innerCode) {
		this.innerCode = innerCode;
	}

	public String getCompsIndustryCode() {
		return compsIndustryCode;
	}

	public void setCompsIndustryCode(String compsIndustryCode) {
		this.compsIndustryCode = compsIndustryCode;
	}

	public static class AuthorInfo implements Serializable {
		private static final long serialVersionUID = 5961277716407793780L;
		private String authorMobile; // 作者手机号
		private String authorName; // 作者名称
		private String authorCompanyName;

		public String getAuthorMobile() {
			return authorMobile;
		}

		public void setAuthorMobile(String authorMobile) {
			this.authorMobile = authorMobile;
		}

		public String getAuthorName() {
			return authorName;
		}

		public void setAuthorName(String authorName) {
			this.authorName = authorName;
		}

		public String getAuthorCompanyName() {
			return authorCompanyName;
		}

		public void setAuthorCompanyName(String authorCompanyName) {
			this.authorCompanyName = authorCompanyName;
		}
	}
}
