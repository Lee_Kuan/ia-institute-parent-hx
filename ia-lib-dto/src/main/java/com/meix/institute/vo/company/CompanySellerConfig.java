package com.meix.institute.vo.company;

import java.io.Serializable;
import java.util.List;

/**
 * 机构销售配置
 * Created by zenghao on 2019/7/10.
 */
public class CompanySellerConfig implements Serializable {
	private static final long serialVersionUID = 1521320971123308958L;

	private long companyCode;
	private List<Long> sellerUidList;

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public List<Long> getSellerUidList() {
		return sellerUidList;
	}

	public void setSellerUidList(List<Long> sellerUidList) {
		this.sellerUidList = sellerUidList;
	}
}
