package com.meix.institute.vo.message;

import java.io.Serializable;
import java.util.List;

/**
 * 首页推荐权限变更消息
 * Created by zenghao on 2018/11/2.
 */
public class RecPermissionChangeMsg implements Serializable {
	private static final long serialVersionUID = -9217995937052657225L;
	
	private String action;
	private long userId;
	private int dataType;
	private long dataId;
	private List<Integer> userTypeList;
	private List<Long> companyIdList;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public List<Integer> getUserTypeList() {
		return userTypeList;
	}

	public void setUserTypeList(List<Integer> userTypeList) {
		this.userTypeList = userTypeList;
	}

	public List<Long> getCompanyIdList() {
		return companyIdList;
	}

	public void setCompanyIdList(List<Long> companyIdList) {
		this.companyIdList = companyIdList;
	}
}
