package com.meix.institute.vo.company;

import java.io.Serializable;

/**
 * 微信模板消息配置
 * Created by zenghao on 2019/7/19.
 */
public class CompanyWechatTemplete implements Serializable {
	private static final long serialVersionUID = -3837764488176067376L;

	private long companyCode;
	private String templeteId;
	private int msgType;//1 申请白名单权限

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public String getTempleteId() {
		return templeteId;
	}

	public void setTempleteId(String templeteId) {
		this.templeteId = templeteId;
	}

	public int getMsgType() {
		return msgType;
	}

	public void setMsgType(int msgType) {
		this.msgType = msgType;
	}
}
