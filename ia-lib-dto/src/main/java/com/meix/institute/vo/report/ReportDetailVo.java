package com.meix.institute.vo.report;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.meix.institute.vo.GroupPowerVo;
import com.meix.institute.vo.secu.SecuMainVo;

/**
 * @describe 研报
 */
public class ReportDetailVo implements Serializable {

	private static final long serialVersionUID = -6886589100844144734L;
	private int dataType = 3;
	private long id;

	private long uid;

	private long companyCode;

	/**
	 * 标题
	 */
	private String title;

	/**
	 * 内容
	 */
	private String content;

	/**
	 * 文件属性
	 */
	private String fileAttr;

	/**
	 * 标志  -1 撤销  1 可用
	 */
	private int flag;

	private String createTime;

	private long authorCode;

	private String authorName;

	private String authorHeadImgUrl;

	private long orgCode;

	private String orgName;

	private String audioUrl;

	private int readNum;

	private List<Integer> issueTypeList;
	private int share;
	private String shareName;//授权列表
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;

	private Date publishDate;
	/**
	 * 作者在职状态
	 * 0-在职，1-离职
	 */
	private int userState;

	private String editUserName;

	private List<SecuMainVo> secu;
	private List<SecuMainVo> industry;
	private List<String> rate;
	private List<String> author;

	private String fileUrl;

	private String operatorUid;
	private String operatorName;
	private String undoReason;
	private String refuseReason;
	private int status;
	private String infoType;
	private String infoSubType;
	private String infoGrade;

	private int permission; //权限：0-无权限，1-有权限
	private int isCompanyWhiteList;    //白名单用户标志：0-非白名单，1-白名单
	private int applyStatus;    //申请白名单权限状态：0-申请中，1-成功，2-失败

	private Date infoDate;
	private List<ReportAuthorVo> authorList;
	private List<SecuMainVo> secuList;

	private Date undoTime;
	private String evaluateDesc;
	private List<String> yjfl;
	private int contentType; //无连接为1
	private String publishDateText; // 发布时间文本
	private String picture;
	private long submenuId;
	private long rid;
	
	private List<GroupPowerVo> groupPower;
	private int powerSubType; //用作组权限判定的研报分类
	private int deepStatus; //1-普通报告，2-深度报告
	private int type; //0-研报，1-会议纪要
	private int hasPdfUrl;//是否有PDF原文
	
	public int getHasPdfUrl() {
		return hasPdfUrl;
	}

	public void setHasPdfUrl(int hasPdfUrl) {
		this.hasPdfUrl = hasPdfUrl;
	}

	public int getDeepStatus() {
		return deepStatus;
	}

	public void setDeepStatus(int deepStatus) {
		this.deepStatus = deepStatus;
	}

	public int getPowerSubType() {
		return powerSubType;
	}

	public void setPowerSubType(int powerSubType) {
		this.powerSubType = powerSubType;
	}

	public List<GroupPowerVo> getGroupPower() {
		return groupPower;
	}

	public void setGroupPower(List<GroupPowerVo> groupPower) {
		this.groupPower = groupPower;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public int getShare() {
		return share;
	}

	public void setShare(int share) {
		this.share = share;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getInfoSubType() {
		return infoSubType;
	}

	public void setInfoSubType(String infoSubType) {
		this.infoSubType = infoSubType;
	}

	public String getPublishDateText() {
		return publishDateText;
	}

	public void setPublishDateText(String publishDateText) {
		this.publishDateText = publishDateText;
	}

	public int getContentType() {
		return contentType;
	}

	public void setContentType(int contentType) {
		this.contentType = contentType;
	}

	public List<String> getYjfl() {
		return yjfl;
	}

	public void setYjfl(List<String> yjfl) {
		this.yjfl = yjfl;
	}

	public String getEvaluateDesc() {
		return evaluateDesc;
	}

	public void setEvaluateDesc(String evaluateDesc) {
		this.evaluateDesc = evaluateDesc;
	}

	private ReportInfoVo report;

	public ReportInfoVo getReport() {
		return report;
	}

	public void setReport(ReportInfoVo report) {
		this.report = report;
	}

	public Date getUndoTime() {
		return undoTime;
	}

	public void setUndoTime(Date undoTime) {
		this.undoTime = undoTime;
	}

	public List<ReportAuthorVo> getAuthorList() {
		return authorList;
	}

	public void setAuthorList(List<ReportAuthorVo> authorList) {
		this.authorList = authorList;
	}

	public List<SecuMainVo> getSecuList() {
		return secuList;
	}

	public void setSecuList(List<SecuMainVo> secuList) {
		this.secuList = secuList;
	}

	public Date getInfoDate() {
		return infoDate;
	}

	public void setInfoDate(Date infoDate) {
		this.infoDate = infoDate;
	}

	/**
	 * @return 0-无权限，1-有权限
	 */
	public int getPermission() {
		return permission;
	}

	/**
	 * @param permission 0-无权限，1-有权限
	 */
	public void setPermission(int permission) {
		this.permission = permission;
	}

	/**
	 * @return 0-非白名单用户，1-白名单用户
	 */
	public int getIsCompanyWhiteList() {
		return isCompanyWhiteList;
	}

	/**
	 * @param isCompanyWhiteList 0-非白名单用户，1-白名单用户
	 */
	public void setIsCompanyWhiteList(int isCompanyWhiteList) {
		this.isCompanyWhiteList = isCompanyWhiteList;
	}

	/**
	 * @return 0-申请中，1-成功，2-失败
	 */
	public int getApplyStatus() {
		return applyStatus;
	}

	/**
	 * @param applyStatus 0-申请中，1-成功，2-失败
	 */
	public void setApplyStatus(int applyStatus) {
		this.applyStatus = applyStatus;
	}

	public String getOperatorUid() {
		return operatorUid;
	}

	public void setOperatorUid(String operatorUid) {
		this.operatorUid = operatorUid;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getUndoReason() {
		return undoReason;
	}

	public void setUndoReason(String undoReason) {
		this.undoReason = undoReason;
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ReportDetailVo() {
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFileAttr() {
		return fileAttr;
	}

	public void setFileAttr(String fileAttr) {
		this.fileAttr = fileAttr;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public long getAuthorCode() {
		return authorCode;
	}

	public void setAuthorCode(long authorCode) {
		this.authorCode = authorCode;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getAuthorHeadImgUrl() {
		return authorHeadImgUrl;
	}

	public void setAuthorHeadImgUrl(String authorHeadImgUrl) {
		this.authorHeadImgUrl = authorHeadImgUrl;
	}

	public long getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(long orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getAudioUrl() {
		return audioUrl;
	}

	public void setAudioUrl(String audioUrl) {
		this.audioUrl = audioUrl;
	}

	public int getReadNum() {
		return readNum;
	}

	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}

	public List<Integer> getIssueTypeList() {
		return issueTypeList;
	}

	public void setIssueTypeList(List<Integer> issueTypeList) {
		this.issueTypeList = issueTypeList;
	}

	public String getShareName() {
		return shareName;
	}

	public void setShareName(String shareName) {
		this.shareName = shareName;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public int getUserState() {
		return userState;
	}

	public void setUserState(int userState) {
		this.userState = userState;
	}

	public String getEditUserName() {
		return editUserName;
	}

	public void setEditUserName(String editUserName) {
		this.editUserName = editUserName;
	}

	public List<SecuMainVo> getSecu() {
		return secu;
	}

	public void setSecu(List<SecuMainVo> secu) {
		this.secu = secu;
	}

	public List<SecuMainVo> getIndustry() {
		return industry;
	}

	public void setIndustry(List<SecuMainVo> industry) {
		this.industry = industry;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public List<String> getRate() {
		return rate;
	}

	public void setRate(List<String> rate) {
		this.rate = rate;
	}

	public List<String> getAuthor() {
		return author;
	}

	public void setAuthor(List<String> author) {
		this.author = author;
	}

	public String getInfoType() {
		return infoType;
	}

	public void setInfoType(String infoType) {
		this.infoType = infoType;
	}

	public String getInfoGrade() {
		return infoGrade;
	}

	public void setInfoGrade(String infoGrade) {
		this.infoGrade = infoGrade;
	}

	public long getSubmenuId() {
		return submenuId;
	}

	public void setSubmenuId(long submenuId) {
		this.submenuId = submenuId;
	}

	public long getRid() {
		return rid;
	}

	public void setRid(long rid) {
		this.rid = rid;
	}
}
