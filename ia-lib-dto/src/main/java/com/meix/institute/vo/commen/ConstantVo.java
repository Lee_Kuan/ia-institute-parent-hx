package com.meix.institute.vo.commen;

import java.io.Serializable;

/**
 * 用于返回客户端选择器的选项
 * Created by zenghao on 2019/8/5.
 */
public class ConstantVo implements Serializable {
	private static final long serialVersionUID = -6834252187834715020L;

	private String name;
	private long id;

	public ConstantVo() {
	}

	public ConstantVo(String name, long id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
