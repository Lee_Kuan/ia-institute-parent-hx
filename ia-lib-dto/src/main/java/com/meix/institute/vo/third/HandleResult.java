package com.meix.institute.vo.third;

import java.io.Serializable;

public class HandleResult implements Serializable {
	private static final long serialVersionUID = 6120368797390421789L;
	private int messageCode;// 错误代码;
	private String message = "";// 客户端显示消息
	private String keyword;//关键字

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(int messageCode) {
		this.messageCode = messageCode;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
}
