package com.meix.institute.vo.company;
import java.io.Serializable;

/**
 * @版权信息	:上海睿涉取有限公司
 * @部门		:开发部
 * @作者		:zhouwei
 * @E-mail  : zhouwei@51research.com
 * @创建日期	: 2014年8月17日
 * @Description:公司信息表
 */
public class CompanyInfoVo implements Serializable {

	private static final long serialVersionUID = -3300605808022269409L;
	private long id;
    private String companyName;
    private String companyAddress;
    private String companyAbbr;
    private String createTime;
    private String updateTime;
    private String chiSpelling;//拼音字母
    private String companySize;
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getCompanyName() {
        return companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public String getCompanyAddress() {
        return companyAddress;
    }
    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }
    public String getCompanyAbbr() {
        return companyAbbr;
    }
    public void setCompanyAbbr(String companyAbbr) {
        this.companyAbbr = companyAbbr;
    }
    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public String getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
    public String getChiSpelling() {
        return chiSpelling;
    }
    public void setChiSpelling(String chiSpelling) {
        this.chiSpelling = chiSpelling;
    }
    public String getCompanySize() {
        return companySize;
    }
    public void setCompanySize(String companySize) {
        this.companySize = companySize;
    }
    
}
