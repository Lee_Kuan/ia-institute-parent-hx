package com.meix.institute.vo.activity;

public class ActivityWithAdminInfo extends Activity {
	private static final long serialVersionUID = -4200300619911181955L;
	private boolean isHandledByAdmin = false;
	
	public boolean getIsHandledByAdmin() {
		return isHandledByAdmin;
	}
	
	public void setIsHandledByAdmin(boolean b) {
		isHandledByAdmin = b;
	}
}
