package com.meix.institute.vo.activity;

import java.io.Serializable;

/**
 * 路演相关，来访机构、接待人员、路演人员
 * @author likuan
 *
 */
public class ActivityRelevantVo implements Serializable {

	private static final long serialVersionUID = -8001592533514861825L;
	private long code;	//代码
	private String contact;	//名字
	
	public long getCode() {
		return code;
	}
	public void setCode(long code) {
		this.code = code;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	
}
