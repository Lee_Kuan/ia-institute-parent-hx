package com.meix.institute.vo.activity.gensee;

import lombok.Data;

import java.io.Serializable;

/**
 * 非系统电话会议--参加人员
 */
@Data
public class ActivityThirdJoinInfo implements Serializable {

	private static final long serialVersionUID = 3619702182305742755L;
	/**
	 * {
	 * "TelNo": "015338858117(主持人)",
	 * "Area": "",
	 * "EnterWay": "国内400接入",
	 * "EnterTime": "43284.8541203704",
	 * "LeaveTime": "43284.8989583333",
	 * "Duration": "65",
	 * "CommunFee": "9.75",
	 * "SevCharge": "6.5"
	 * }
	 */
	private long id;
	/**
	 * ia_activity 的 id
	 */
	private long activityId;
	/**
	 * 发起机构代码
	 */
	private long companyCode;
	/**
	 * 拨入号码
	 */
	private String telNo;

	/**
	 * 加入地区
	 */
	private String area;

	/**
	 * 加入途径
	 */
	private String enterWay;

	/**
	 * 加入时间
	 */
	private String joinTime;

	/**
	 * 离会时间
	 */
	private String leaveTime;

	/**
	 * 参会时长
	 */
	private int duration;

	/**
	 * 参会人姓名
	 */
	private String userName;
	/**
	 * 参会人公司
	 */
	private String userCompanyName;

	private String createTime;
	private String updateTime;
}
