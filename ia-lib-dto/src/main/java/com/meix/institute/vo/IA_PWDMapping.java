package com.meix.institute.vo;

import java.io.Serializable;
import java.util.Date;

public class IA_PWDMapping implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private long ID;
	private long uid;
	private int loginType;//1 手机 2网页
	private String mobileOrEmail;
	private String pwdOrUrl;
	private boolean isDeleted;
	private Date createTime;
	private Date updateTime;
	private boolean smsChange = false;//短信通道切换
	private String lastMobile;//原手机号
	private int messageCode;//错误代码

	public int getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(int messageCode) {
		this.messageCode = messageCode;
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getMobileOrEmail() {
		return mobileOrEmail;
	}

	public void setMobileOrEmail(String mobileOrEmail) {
		this.mobileOrEmail = mobileOrEmail;
	}

	public String getPwdOrUrl() {
		return pwdOrUrl;
	}

	public void setPwdOrUrl(String pwdOrUrl) {
		this.pwdOrUrl = pwdOrUrl;
	}

	public boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public int getLoginType() {
		return loginType;
	}

	public void setLoginType(int loginType) {
		this.loginType = loginType;
	}

	public boolean isSmsChange() {
		return smsChange;
	}

	public void setSmsChange(boolean smsChange) {
		this.smsChange = smsChange;
	}

	public String getLastMobile() {
		return lastMobile;
	}

	public void setLastMobile(String lastMobile) {
		this.lastMobile = lastMobile;
	}


}
