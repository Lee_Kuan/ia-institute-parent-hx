package com.meix.institute.vo.user;

import java.io.Serializable;
import java.util.Date;

public class InsUserLabelVo implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private Long id;
    private Long uid;
    private Integer labelType;
    private String labelCode;
    private Float score;
    private Integer createVia;
    private Date createdAt;
    private String createdBy;
    private Date updatedAt;
    private String updatedBy;
    
    
    private String labelName;
    
    public String getLabelName() {
        return labelName;
    }
    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getUid() {
        return uid;
    }
    public void setUid(Long uid) {
        this.uid = uid;
    }
    public Integer getLabelType() {
        return labelType;
    }
    public void setLabelType(Integer labelType) {
        this.labelType = labelType;
    }
    public String getLabelCode() {
        return labelCode;
    }
    public void setLabelCode(String labelCode) {
        this.labelCode = labelCode;
    }
    public Float getScore() {
        return score;
    }
    public void setScore(Float score) {
        this.score = score;
    }
    public Integer getCreateVia() {
        return createVia;
    }
    public void setCreateVia(Integer createVia) {
        this.createVia = createVia;
    }
    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    public Date getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
}
