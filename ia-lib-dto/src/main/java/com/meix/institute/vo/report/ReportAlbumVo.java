package com.meix.institute.vo.report;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @describe 研报合辑
 */
public class ReportAlbumVo implements Serializable {

	private static final long serialVersionUID = -6886589100844144734L;

	private long id;

	/**
	 * 标题
	 */
	private String title;

	private List<Integer> issueTypeList;
	private String shareName;//授权列表
	
	private Date publishDate;

	private String operatorUid;
	private String undoReason;
	private int status;

	private Date undoTime;

	private String authorName;
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Integer> getIssueTypeList() {
		return issueTypeList;
	}

	public void setIssueTypeList(List<Integer> issueTypeList) {
		this.issueTypeList = issueTypeList;
	}

	public String getShareName() {
		return shareName;
	}

	public void setShareName(String shareName) {
		this.shareName = shareName;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getOperatorUid() {
		return operatorUid;
	}

	public void setOperatorUid(String operatorUid) {
		this.operatorUid = operatorUid;
	}

	public String getUndoReason() {
		return undoReason;
	}

	public void setUndoReason(String undoReason) {
		this.undoReason = undoReason;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getUndoTime() {
		return undoTime;
	}

	public void setUndoTime(Date undoTime) {
		this.undoTime = undoTime;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	
}
