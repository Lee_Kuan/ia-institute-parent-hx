package com.meix.institute.vo.user;

import java.io.Serializable;

/**
 * 用户权限请求记录
 * Created by zenghao on 2019/7/10.
 */
public class UserPermReqVo implements Serializable {
	private static final long serialVersionUID = -6318134797322055914L;

	private long uid;
	private long companyCode;//研究所代码
	private int dataType;//数据类型 8活动
	private long dataId;//数据ID
	private int status;//状态 -1 未申请 0申请中 1申请成功 2申请失败
	private String createTime;
	private String updateTime;

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
}
