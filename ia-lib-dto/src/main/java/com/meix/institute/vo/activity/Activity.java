package com.meix.institute.vo.activity;

import java.io.Serializable;
import java.util.Date;

public class Activity implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;

	private long uid;

	private long companyCode;

	private String companyAbbr;

	private int activityType;

	private String title;

	private String researchObject;

	private String startTime;

	private String endTime;


	private String address;//详细地址

	private String mapPosition = "";//地址

	private String activityDesc = "";//活动描述

	private String createTime;

	private int readNum;

	private String analystList;

	private String targetList;

	private int isJoinIn;//是否加入会议  0未参加 1 参加

	private int isAnalyst;//是否是发起人 0 不是 1是

	private int isEnd;// 是否结束  0 未结束 1 结束
	private String city;//城市
	private int flag;//1 激活 -1 取消
	private int contentType;//内容类型：1 文本 2 富文本 3外部链接
	private int isRemind;//是否添加提醒 int 0未添加 1已添加
	private int latitude;//纬度
	private int longitude;//经度
	private int payFlag;//付费标志 0 不开通 1 开通
	private int orgFlag;//机构标志 1机构 0非机构
	private int openFlag;//开放标志 0 不开放 1 开放
	private String url;//外部资源链接
	private String fileAttr;//文件属性
	private int subType;//子类型  int 0 普通 1 金组合 
	private int roomNo;//直播间编号 int
	private int endStatus;//直播状态 2预告 3 回放 4进行中
	private String resourceUrl;//活动缩略图地址
	private String resourceUrlSmall;//活动缩略图地址(小图)
	private String webcastId;//直播ID
	private String webcastNumber;//直播编号
	private String organizerToken;//组织者口令
	private String panelistToken;//嘉宾口令
	private String attendeeToken;//普通参加者口令
	private String organizerJoinUrl;//组织者加入URL
	private String panelistJoinUrl;//嘉宾加入URL
	private String attendeeAShortJoinUrl;//普通参加者加入URL(不带token)
	private String attendeeJoinUrl;//普通参加者加入URL(带token)
	private String vodId;//点播ID
	private String vodNumber;//点播编号
	private String managerTips;//管理员记录信息
	private int playType;//播放类型   0 未填写(默认) 1 直播 2 转播 3 录音
	private String applyDeadlineTime;//报名截止时间
	private int evaluateFlag;//报名状态 0 未报名 1 已报名
	private String summaryUrl;//摘要链接
	private int audioHideDay;//录音隐藏天数
	private Date applyDeadlineTimeDateType;
	private int playTimes;//播放次数
	private int permission;// 是否有权限 0 没有 1 有
	private int vodDuration;// 时长
	private int internalType;// 1 每市内部电话会议
	private String pptImg;//ppt图片
	private int activityVipFlag;//阿尔法VIP标识 0 是，1 否
	
	/**
	 * 加入总结时间
	 * likuan
	 * 2019-06-17
	 */
	private String summaryCreateTime;
	
	/**
	 * 加入会议类型，会议标签，隐藏标志
	 * likuan
	 * 2019-07-5
	 */
	private int meetingType;
	private int meetingLabel;
	private int isHiden;
	private int activityLabel;
	
    public int getActivityLabel() {
        return activityLabel;
    }
    public void setActivityLabel(int activityLabel) {
        this.activityLabel = activityLabel;
    }
    public int getMeetingType() {
		return meetingType;
	}
	public void setMeetingType(int meetingType) {
		this.meetingType = meetingType;
	}
	public int getMeetingLabel() {
		return meetingLabel;
	}
	public void setMeetingLabel(int meetingLabel) {
		this.meetingLabel = meetingLabel;
	}
	public int getIsHiden() {
		return isHiden;
	}
	public void setIsHiden(int isHiden) {
		this.isHiden = isHiden;
	}
	
	
	public String getSummaryCreateTime() {
		return summaryCreateTime;
	}

	public void setSummaryCreateTime(String summaryCreateTime) {
		this.summaryCreateTime = summaryCreateTime;
	}

	public String getPptImg() {
		return pptImg;
	}

	public void setPptImg(String pptImg) {
		this.pptImg = pptImg;
	}

	public int getInternalType() {
		return internalType;
	}

	public void setInternalType(int internalType) {
		this.internalType = internalType;
	}

	public int getVodDuration() {
		return vodDuration;
	}

	public void setVodDuration(int vodDuration) {
		this.vodDuration = vodDuration;
	}

	public int getPermission() {
		return permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}

	public String getResourceUrlSmall() {
		return resourceUrlSmall;
	}

	public void setResourceUrlSmall(String resourceUrlSmall) {
		this.resourceUrlSmall = resourceUrlSmall;
	}

	public Date getApplyDeadlineTimeDateType() {
		return applyDeadlineTimeDateType;
	}

	public void setApplyDeadlineTimeDateType(Date applyDeadlineTimeDateType) {
		this.applyDeadlineTimeDateType = applyDeadlineTimeDateType;
	}

	public String getVodId() {
		return vodId;
	}

	public void setVodId(String vodId) {
		this.vodId = vodId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getOrgFlag() {
		return orgFlag;
	}

	public void setOrgFlag(int orgFlag) {
		this.orgFlag = orgFlag;
	}

	public int getOpenFlag() {
		return openFlag;
	}

	public void setOpenFlag(int openFlag) {
		this.openFlag = openFlag;
	}

	public int getPayFlag() {
		return payFlag;
	}

	public void setPayFlag(int payFlag) {
		this.payFlag = payFlag;
	}

	public int getLatitude() {
		return latitude;
	}

	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}

	public int getLongitude() {
		return longitude;
	}

	public void setLongitude(int longitude) {
		this.longitude = longitude;
	}

	public int getIsRemind() {
		return isRemind;
	}

	public void setIsRemind(int isRemind) {
		this.isRemind = isRemind;
	}

	public int getContentType() {
		return contentType;
	}

	public void setContentType(int contentType) {
		this.contentType = contentType;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getIsEnd() {
		return isEnd;
	}

	public void setIsEnd(int isEnd) {
		this.isEnd = isEnd;
	}

	public int getIsAnalyst() {
		return isAnalyst;
	}

	public void setIsAnalyst(int isAnalyst) {
		this.isAnalyst = isAnalyst;
	}

	public int getIsJoinIn() {
		return isJoinIn;
	}

	public void setIsJoinIn(int isJoinIn) {
		this.isJoinIn = isJoinIn;
	}

	public String getAnalystList() {
		return analystList;
	}

	public void setAnalystList(String analystList) {
		this.analystList = analystList;
	}

	public String getTargetList() {
		return targetList;
	}

	public void setTargetList(String targetList) {
		this.targetList = targetList;
	}

	public int getReadNum() {
		return readNum;
	}

	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {

		this.createTime = createTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyAbbr() {
		return companyAbbr;
	}

	public void setCompanyAbbr(String companyAbbr) {
		this.companyAbbr = companyAbbr;
	}

	public int getActivityType() {
		return activityType;
	}

	public void setActivityType(int activityType) {
		this.activityType = activityType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResearchObject() {
		return researchObject;
	}

	public void setResearchObject(String researchObject) {
		this.researchObject = researchObject;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMapPosition() {
		return mapPosition;
	}

	public void setMapPosition(String mapPosition) {
		this.mapPosition = mapPosition;
	}

	public String getActivityDesc() {
		return activityDesc;
	}

	public void setActivityDesc(String activityDesc) {
		this.activityDesc = activityDesc;
	}

	public String getFileAttr() {
		return fileAttr;
	}

	public void setFileAttr(String fileAttr) {
		this.fileAttr = fileAttr;
	}

	public int getSubType() {
		return subType;
	}

	public void setSubType(int subType) {
		this.subType = subType;
	}

	public int getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}

	public int getEndStatus() {
		return endStatus;
	}

	public void setEndStatus(int endStatus) {
		this.endStatus = endStatus;
	}

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	public String getWebcastId() {
		return webcastId;
	}

	public void setWebcastId(String webcastId) {
		this.webcastId = webcastId;
	}

	public String getWebcastNumber() {
		return webcastNumber;
	}

	public void setWebcastNumber(String webcastNumber) {
		this.webcastNumber = webcastNumber;
	}

	public String getOrganizerToken() {
		return organizerToken;
	}

	public void setOrganizerToken(String organizerToken) {
		this.organizerToken = organizerToken;
	}

	public String getPanelistToken() {
		return panelistToken;
	}

	public void setPanelistToken(String panelistToken) {
		this.panelistToken = panelistToken;
	}

	public String getAttendeeToken() {
		return attendeeToken;
	}

	public void setAttendeeToken(String attendeeToken) {
		this.attendeeToken = attendeeToken;
	}

	public String getOrganizerJoinUrl() {
		return organizerJoinUrl;
	}

	public void setOrganizerJoinUrl(String organizerJoinUrl) {
		this.organizerJoinUrl = organizerJoinUrl;
	}

	public String getPanelistJoinUrl() {
		return panelistJoinUrl;
	}

	public void setPanelistJoinUrl(String panelistJoinUrl) {
		this.panelistJoinUrl = panelistJoinUrl;
	}

	public String getAttendeeAShortJoinUrl() {
		return attendeeAShortJoinUrl;
	}

	public void setAttendeeAShortJoinUrl(String attendeeAShortJoinUrl) {
		this.attendeeAShortJoinUrl = attendeeAShortJoinUrl;
	}

	public String getAttendeeJoinUrl() {
		return attendeeJoinUrl;
	}

	public void setAttendeeJoinUrl(String attendeeJoinUrl) {
		this.attendeeJoinUrl = attendeeJoinUrl;
	}

	public String getVodNumber() {
		return vodNumber;
	}

	public void setVodNumber(String vodNumber) {
		this.vodNumber = vodNumber;
	}

	public String getManagerTips() {
		return managerTips;
	}

	public void setManagerTips(String managerTips) {
		this.managerTips = managerTips;
	}

	public int getPlayType() {
		return playType;
	}

	public void setPlayType(int playType) {
		this.playType = playType;
	}

	public String getApplyDeadlineTime() {
		return applyDeadlineTime;
	}

	public void setApplyDeadlineTime(String applyDeadlineTime) {
		this.applyDeadlineTime = applyDeadlineTime;
	}

	public int getEvaluateFlag() {
		return evaluateFlag;
	}

	public void setEvaluateFlag(int evaluateFlag) {
		this.evaluateFlag = evaluateFlag;
	}

	public String getSummaryUrl() {
		return summaryUrl;
	}

	public void setSummaryUrl(String summaryUrl) {
		this.summaryUrl = summaryUrl;
	}

	public int getAudioHideDay() {
		return audioHideDay;
	}

	public void setAudioHideDay(int audioHideDay) {
		this.audioHideDay = audioHideDay;
	}

	public int getPlayTimes() {
		return playTimes;
	}

	public void setPlayTimes(int playTimes) {
		this.playTimes = playTimes;
	}

	public int getActivityVipFlag() {
		return activityVipFlag;
	}

	public void setActivityVipFlag(int activityVipFlag) {
		this.activityVipFlag = activityVipFlag;
	}
}
