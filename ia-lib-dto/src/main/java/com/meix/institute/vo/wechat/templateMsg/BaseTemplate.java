package com.meix.institute.vo.wechat.templateMsg;

/**
 * 模版消息基类
 * 不可修改字段名
 *
 * @author wangrr
 */
public class BaseTemplate {
	String touser;//openId
	String template_id;//模版消息Id
	String url;//模版点击后链接的地方
	String topcolor;//模版头颜色

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTopcolor() {
		return topcolor;
	}

	public void setTopcolor(String topcolor) {
		this.topcolor = topcolor;
	}

	static public class TemplateReserve extends BaseTemplate {
		Data data;

		public Data getData() {
			return data;
		}

		public void setData(Data data) {
			this.data = data;
		}
	}
}
