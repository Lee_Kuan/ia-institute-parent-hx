package com.meix.institute.vo.user;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class UserInfo implements Serializable {
	private static final long serialVersionUID = 8752326737372988111L;

	public static final String MOBILE_PREFIX = "***";

	private long id;
	private String userName;
	private int gender;// 性别 0 无 1 男 2女
	private String position;// 职位
	private String areaCode;//手机区号
	private String mobile;
	private String phoneNumber;
	private String email;
	private String comment;
	private String updateTime;
	private String clientType;
	private String mobileOrEmail;
	private String pwdOrUrl;
	private String token;
	private int isDelete;
	private String createTime;
	private long cardID;
	private String cardUrl;
	private String headImageUrl;// 头像图片
	private String password;// 密码
	private String operatorId;// 操作人ID
	private String operatorName;// 操作人姓名
	private long companyCode;// 公司ID
	private String companyName;
	private String companyAbbr;// 公司简称
	private int followNum;
	private int readNum;
	private int followFlag;
	private String chiSpelling;// 拼音字母
	private int roleType;// 用户 用户组操作权限
	private List<Map<String, Object>> sysLabel;//系统行业标签
	private int userReadNum;//用户被点击次数
	private int accountType;//账户类型 1 正常账户  2非机构账户  3第三方账户登录
	private String companySize;

	private int isInVerify;//是否在审核中 0不是 1是
	private String insUuid;//关联研究所内部用户user字段
	private int issueType;//名片上传渠道 1公众号 4小程序
	private String thirdId; // 第三方唯一标识
	private int authStatus; // 认证状态：0 未认证 1 认证中 2 认证失败 3 认证成功
	private String reason; // 认证失败原因

	private String nickName;
	private String wechatHeadUrl;
	private String direction;//分析师研究方向
	private String companyAddress;//公司地址
	private int privacyStatus; // 隐私策略状态：(0 未操作 1 仅浏览  2 同意并继续)

	private long salerUid;
	
	private int identify; // 身份：0-个人，1-普通，2-专业

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getIdentify() {
		return identify;
	}

	public void setIdentify(int identify) {
		this.identify = identify;
	}

	public long getSalerUid() {
		return salerUid;
	}

	public void setSalerUid(long salerUid) {
		this.salerUid = salerUid;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getWechatHeadUrl() {
		return wechatHeadUrl;
	}

	public void setWechatHeadUrl(String wechatHeadUrl) {
		this.wechatHeadUrl = wechatHeadUrl;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getAuthStatus() {
		return authStatus;
	}

	public void setAuthStatus(int authStatus) {
		this.authStatus = authStatus;
	}

	public String getThirdId() {
		return thirdId;
	}

	public void setThirdId(String thirdId) {
		this.thirdId = thirdId;
	}

	public String getCompanySize() {
		return companySize;
	}

	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public int getUserReadNum() {
		return userReadNum;
	}

	public void setUserReadNum(int userReadNum) {
		this.userReadNum = userReadNum;
	}

	public List<Map<String, Object>> getSysLabel() {
		return sysLabel;
	}

	public void setSysLabel(List<Map<String, Object>> sysLabel) {
		this.sysLabel = sysLabel;
	}

	public int getRoleType() {
		return roleType;
	}

	public void setRoleType(int roleType) {
		this.roleType = roleType;
	}

	public String getChiSpelling() {
		return chiSpelling;
	}

	public void setChiSpelling(String chiSpelling) {
		this.chiSpelling = chiSpelling;
	}

	public int getFollowFlag() {
		return followFlag;
	}

	public void setFollowFlag(int followFlag) {
		this.followFlag = followFlag;
	}

	public int getFollowNum() {
		return followNum;
	}

	public void setFollowNum(int followNum) {
		this.followNum = followNum;
	}

	public int getReadNum() {
		return readNum;
	}

	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPwdOrUrl() {
		return pwdOrUrl;
	}

	public void setPwdOrUrl(String pwdOrUrl) {
		this.pwdOrUrl = pwdOrUrl;
	}

	public String getMobileOrEmail() {
		return mobileOrEmail;
	}

	public void setMobileOrEmail(String mobileOrEmail) {
		this.mobileOrEmail = mobileOrEmail;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public long getCardID() {
		return cardID;
	}

	public void setCardID(long cardID) {
		this.cardID = cardID;
	}

	public String getCardUrl() {
		return cardUrl;
	}

	public void setCardUrl(String cardUrl) {
		this.cardUrl = cardUrl;
	}

	public String getHeadImageUrl() {
		return headImageUrl;
	}

	public void setHeadImageUrl(String headImageUrl) {
		this.headImageUrl = headImageUrl;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCompanyAbbr() {
		return companyAbbr;
	}

	public void setCompanyAbbr(String companyAbbr) {
		this.companyAbbr = companyAbbr;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getIsInVerify() {
		return isInVerify;
	}

	public void setIsInVerify(int isInVerify) {
		this.isInVerify = isInVerify;
	}

	public String getInsUuid() {
		return insUuid;
	}

	public void setInsUuid(String insUuid) {
		this.insUuid = insUuid;
	}

	public int getIssueType() {
		return issueType;
	}

	public void setIssueType(int issueType) {
		this.issueType = issueType;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public int getPrivacyStatus() {
		return privacyStatus;
	}

	public void setPrivacyStatus(int privacyStatus) {
		this.privacyStatus = privacyStatus;
	}

}
