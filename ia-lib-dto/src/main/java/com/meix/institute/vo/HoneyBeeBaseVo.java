package com.meix.institute.vo;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by zenghao on 2018/9/12.
 */
public class HoneyBeeBaseVo implements Serializable {

	private static final long serialVersionUID = -2765266161149017835L;

	private int dataType;//-1 广告 1 调仓  2观点 3研报 5 股票  6 模拟组合 8 活动 13晨会
	private long dataId;
	private String updateTime;
	private long queueId;//瀑布流在redis队列中的id（时间戳）

	private Map<String, Object> item;

	//打分相关
	private double ranking;

	private long id;
	private int sortNo;
	private int status = 2;//状态 0 已删除 1 应用 2 不应用
	private String title;
	private String content;
	private String applyTime;
	private String resourceUrl;
	private String createTime;//创建时间
	//推荐类型 0 默认推荐类型 1 清算后组合无调仓推荐 2 清算后组合有调仓推荐 3 仓位变化超过30% 4 新建组合 5 组合日收益前三 6 组合收益快速上涨（0.01）
	private int waterflowRecType = 0;
	private String from;//来源

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getSortNo() {
		return sortNo;
	}

	public void setSortNo(int sortNo) {
		this.sortNo = sortNo;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(String applyTime) {
		this.applyTime = applyTime;
	}

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public int getWaterflowRecType() {
		return waterflowRecType;
	}

	public void setWaterflowRecType(int waterflowRecType) {
		this.waterflowRecType = waterflowRecType;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public long getQueueId() {
		return queueId;
	}

	public void setQueueId(long queueId) {
		this.queueId = queueId;
	}

	public Map<String, Object> getItem() {
		return item;
	}

	public void setItem(Map<String, Object> item) {
		this.item = item;
	}

	public double getRanking() {
		return ranking;
	}

	public void setRanking(double ranking) {
		this.ranking = ranking;
	}

}
