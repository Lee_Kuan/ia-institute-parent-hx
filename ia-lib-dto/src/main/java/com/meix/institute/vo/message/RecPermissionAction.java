package com.meix.institute.vo.message;

import java.io.Serializable;

/**
 * 首页推荐相关权限变更
 * Created by zenghao on 2018/11/2.
 */
public interface RecPermissionAction extends Serializable {
	public static final String CODE_UNINTERESTED = "01001";
	public static final String CODE_FOLLOW_USER = "01002";
	public static final String CODE_UNFOLLOW_USER = "01003";
	public static final String CODE_SUBSCRIBE_COMB = "01004";
	public static final String CODE_UNSUBSCRIBE_COMB = "01005";
	public static final String CODE_ADD_SELF_STOCK = "01006";
	public static final String CODE_DELETE_SELF_STOCK = "01007";
	public static final String CODE_HIDE_COMB = "01008";
	public static final String CODE_SHOW_COMB = "01009";
	public static final String CODE_CHANGE_COMPANY = "01010";

	public static final String CODE_CLOSE_ACTIVITY = "02001";
	public static final String CODE_ADD_ACTIVITY_USER_PERM = "02002";
	public static final String CODE_MODIFY_ACTIVITY_USER_PERM = "02003";
	public static final String CODE_CANCEL_APPLY_DATA = "02004";
	public static final String CODE_DELETE_DATA = "02005";
	public static final String CODE_ADD_DATA_USER_PERM = "02006";
	public static final String CODE_MODIFY_DATA_USER_PERM = "02007";
	public static final String CODE_CHANGE_REPORT_PERM = "02008";

	String getCode();

	String getName();

	enum UserAction implements RecPermissionAction {
		UNINTERESTED(CODE_UNINTERESTED, "不感兴趣的"),
		FOLLOW_USER(CODE_FOLLOW_USER, "关注用户"),
		UNFOLLOW_USER(CODE_UNFOLLOW_USER, "取消关注用户"),
		SUBSCRIBE_COMB(CODE_SUBSCRIBE_COMB, "订阅组合"),
		UNSUBSCRIBE_COMB(CODE_UNSUBSCRIBE_COMB, "取消订阅组合"),
		ADD_SELF_STOCK(CODE_ADD_SELF_STOCK, "增加自选股"),
		DELETE_SELF_STOCK(CODE_DELETE_SELF_STOCK, "删除自选股"),
		HIDE_COMB(CODE_HIDE_COMB, "隐藏组合"),
		SHOW_COMB(CODE_SHOW_COMB, "显示组合"),
		CHANGE_COMPANY(CODE_CHANGE_COMPANY, "用户机构变更"),;

		private String code;
		private String name;

		UserAction(String code, String name) {
			this.code = code;
			this.name = name;
		}

		@Override
		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		@Override
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	enum SysOperatorAction implements RecPermissionAction {
		CLOSE_ACTIVITY(CODE_CLOSE_ACTIVITY, "关闭活动"),
		ADD_ACTIVITY_USER_PERM(CODE_ADD_ACTIVITY_USER_PERM, "新增活动用户权限"),
		MODIFY_ACTIVITY_USER_PERM(CODE_MODIFY_ACTIVITY_USER_PERM, "修改活动用户权限"),
		CANCEL_APPLY_DATA(CODE_CANCEL_APPLY_DATA, "运营配置按时间内容取消应用"),
		DELETE_DATA(CODE_DELETE_DATA, "运营配置按时间内容删除"),
		ADD_DATA_USER_PERM(CODE_ADD_DATA_USER_PERM, "运营配置按时间内容新增用户权限"),
		MODIFY_DATA_USER_PERM(CODE_MODIFY_DATA_USER_PERM, "运营配置按时间内容修改用户权限"),
		CHANGE_REPORT_PERM(CODE_CHANGE_REPORT_PERM, "机构研报权限变更"),;

		private String code;
		private String name;

		SysOperatorAction(String code, String name) {
			this.code = code;
			this.name = name;
		}

		@Override
		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		@Override
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
}
