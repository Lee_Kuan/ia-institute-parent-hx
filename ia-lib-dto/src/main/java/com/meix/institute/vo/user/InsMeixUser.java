package com.meix.institute.vo.user;

import java.io.Serializable;

/**
 * 研究所商机用户与销售对应关系
 * Created by zenghao on 2019/8/9.
 */
public class InsMeixUser implements Serializable {
	private static final long serialVersionUID = -568503086746342188L;

	private long id;
	private long companyCode;// 公司ID
	private String userName;
	private String position;// 职位
	private String mobile;
	private long operatorId;// 操作人ID
	private long customerCompanyCode;
	private String customerCompanyName;
	private String expireTime;//账户到期日期
	private long salerId;
	private String salerName;
	private long uid;
	private long operUid;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(long companyCode) {
		this.companyCode = companyCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public long getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(long operatorId) {
		this.operatorId = operatorId;
	}

	public long getCustomerCompanyCode() {
		return customerCompanyCode;
	}

	public void setCustomerCompanyCode(long customerCompanyCode) {
		this.customerCompanyCode = customerCompanyCode;
	}

	public String getCustomerCompanyName() {
		return customerCompanyName;
	}

	public void setCustomerCompanyName(String customerCompanyName) {
		this.customerCompanyName = customerCompanyName;
	}

	public String getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}

	public long getSalerId() {
		return salerId;
	}

	public void setSalerId(long salerId) {
		this.salerId = salerId;
	}

	public String getSalerName() {
		return salerName;
	}

	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public long getOperUid() {
		return operUid;
	}

	public void setOperUid(long operUid) {
		this.operUid = operUid;
	}
}
