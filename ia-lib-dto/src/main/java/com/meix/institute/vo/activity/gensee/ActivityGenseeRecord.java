package com.meix.institute.vo.activity.gensee;

import lombok.Data;

import java.io.Serializable;

/**
 * 直播收听记录
 * Created by zenghao on 2020/4/15.
 */
@Data
public class ActivityGenseeRecord implements Serializable {
	private static final long serialVersionUID = -8552345495112606345L;

	private long id;
	private long jsId;
	private long activityId;
	private String genseeId;
	private int genseeType;//收听类型 1直播 2回放
	private long uid;
	private String area;
	private String company;
	private String joinTime;
	private String leaveTime;
	private int joinType;
	private String mobile;
	private String nickname;
	private String startTime;
	private String endTime;
	private long audioStartTime;
	private long audioEndTime;
	private String createTime;
	private String updateTime;
}
