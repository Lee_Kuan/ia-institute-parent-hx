package com.meix.institute.vo.user;

import java.io.Serializable;

/**
 * @Description:社区动态的的数据模型
 */
public class UserTrendVo implements Serializable {
	private static final long serialVersionUID = 4526575819857601655L;

	private long id;
	private long authorCode;//作者ID
	private String authorName;//作者姓名
	private int authorType;//作者类型  4、每市用户 19、专家
	private String headImageUrl;//头像链接
	private String content;//内容
	private long dataId;
	private int type;//1 调仓 2 点评 3 研报  6 模拟组合 8 活动
	private String createTime;
	private long compareId;//增量查询Id
	private long orgCode;//公司编码
	private String orgName;//公司名称
	private int contractFlg;//睿行研标志  默认为0 0 无合同关系 1 我的研究员 2 我的客户
	private String resourceId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAuthorCode() {
		return authorCode;
	}

	public void setAuthorCode(long authorCode) {
		this.authorCode = authorCode;
	}

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public long getCompareId() {
		return compareId;
	}

	public void setCompareId(long compareId) {
		this.compareId = compareId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public int getAuthorType() {
		return authorType;
	}

	public void setAuthorType(int authorType) {
		this.authorType = authorType;
	}

	public String getHeadImageUrl() {
		return headImageUrl;
	}

	public void setHeadImageUrl(String headImageUrl) {
		this.headImageUrl = headImageUrl;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(long orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public int getContractFlg() {
		return contractFlg;
	}

	public void setContractFlg(int contractFlg) {
		this.contractFlg = contractFlg;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

}
