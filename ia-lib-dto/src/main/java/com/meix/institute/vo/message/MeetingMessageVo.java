package com.meix.institute.vo.message;

import lombok.Data;

import java.io.Serializable;

@Data
public class MeetingMessageVo implements Serializable {
	private static final long serialVersionUID = 6165368912747432543L;

	private long id;//自增长字段
	private long sender;
	private long activityId;//活动ID
	private String message = "";
	private String createTime = "";
	private String resourceUrl = "";
	private int messageType;//消息类型
	private String title = "";//标题
	private int isDeleted;//撤销
	private String fileAttr = "";//文件属性
	private int mediaTime = 0;//播放时长 单位：秒

}
