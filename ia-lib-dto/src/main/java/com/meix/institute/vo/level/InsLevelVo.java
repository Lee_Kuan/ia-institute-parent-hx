package com.meix.institute.vo.level;

public class InsLevelVo {

	private long levelId;
	private String levelName;
	private int count;
	public long getLevelId() {
		return levelId;
	}
	public void setLevelId(long levelId) {
		this.levelId = levelId;
	}
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
}
