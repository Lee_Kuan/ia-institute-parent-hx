package com.meix.institute.vo.stock;

import java.io.Serializable;

/**
 * 
 * @author zhangzhen(J)
 * @date 2016年4月6日上午9:41:50
 * @describe 分享的股票池里的列表
 */
public class SharedStockListVo implements Serializable{

    private static final long serialVersionUID = 1L;

    private String secuCode;
	
	private String secuAbbr;
	
	private int innerCode;
	
	private String operateTime;//“yyyy-MM-dd HH:mm:ss”
	
	private int flag;//1 调入  -1 调出

	public String getSecuCode() {
		return secuCode;
	}

	public void setSecuCode(String secuCode) {
		this.secuCode = secuCode;
	}

	public String getSecuAbbr() {
		return secuAbbr;
	}

	public void setSecuAbbr(String secuAbbr) {
		this.secuAbbr = secuAbbr;
	}

	public int getInnerCode() {
		return innerCode;
	}

	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}

	public String getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}
	
	
}
