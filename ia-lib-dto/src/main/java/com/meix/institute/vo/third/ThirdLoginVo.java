package com.meix.institute.vo.third;

import java.io.Serializable;

/**
 * @版权信息	:上海睿涉取有限公司
 * @部门		:开发部
 * @作者		:zhouwei
 * @E-mail  : zhouwei@51research.com
 * @创建日期	: 2016年9月2日
 * @Description:第三方登录
 */
public class ThirdLoginVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4568575352533144027L;
	private int type;//第三方账户类型：  1 微信 2微博 3 qq 
	private String openId;//
	private int clientType;//1：手机 2：pc网页'
	private String token;
	private String appVersion;
	private String osVersion;
	private String mapPosition;
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public int getClientType() {
		return clientType;
	}
	public void setClientType(int clientType) {
		this.clientType = clientType;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	public String getMapPosition() {
		return mapPosition;
	}
	public void setMapPosition(String mapPosition) {
		this.mapPosition = mapPosition;
	}
}
