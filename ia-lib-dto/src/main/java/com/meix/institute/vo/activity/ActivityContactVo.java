package com.meix.institute.vo.activity;

import java.io.Serializable;

/**
 * 活动联系人
 * @author likuan
 * 2019年8月6日13:19:00
 */
public class ActivityContactVo implements Serializable {
	
	private static final long serialVersionUID = 830143842292049583L;
	private long uid;
	private String contact;//联系人描述
	private String mobile;//联系人、可接入电话
	
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
