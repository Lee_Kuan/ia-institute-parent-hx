package com.meix.institute.vo;

import java.io.Serializable;

public class SecToken implements Serializable{

	private static final long serialVersionUID = -7868491595878983307L;
	
	private String token;
	private String ltpaToken;

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getLtpaToken() {
		return ltpaToken;
	}
	public void setLtpaToken(String ltpaToken) {
		this.ltpaToken = ltpaToken;
	}
}
