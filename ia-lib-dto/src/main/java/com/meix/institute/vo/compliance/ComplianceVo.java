package com.meix.institute.vo.compliance;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.meix.institute.vo.activity.ActivityAnalystVo;
import com.meix.institute.vo.activity.ActivityTargetVo;

/**
 * 研究所合规监控
 * Created by zenghao on 2019/7/31.
 */
public class ComplianceVo implements Serializable {

	private static final long serialVersionUID = 2535299907041769312L;
	private Long id;

	/**
	 * 状态：0-未审核，1-已通过，2-已拒绝，3-已撤销
	 */
	private Integer activityStatus;

	/**
	 * 公司代码
	 */
	private Long companyCode;

	/**
	 * 发起人机构名称(创建人机构)
	 */
	private String companyAbbr = "";

	/**
	 * 活动类型：6 调研 7电话会议 8路演 11策略 12会议
	 */
	private Integer activityType;

	/**
	 * 标题
	 */
	private String title = "";

	/**
	 * 开始时间
	 */
	private Date startTime;

	/**
	 * 结束时间
	 */
	private Date endTime;

	/**
	 * 操作人UID
	 */
	private String operatorUid = "";

	/**
	 * 操作人姓名
	 */
	private String operatorName = "";

	/**
	 * 撤销原因
	 */
	private String undoReason = "";

	/**
	 * 拒绝原因
	 */
	private String refuseReason = "";

	/**
	 * 创建时间
	 */
	private Date createdAt;

	/**
	 * 创建人
	 */
	private String createdBy = "";

	/**
	 * 更新时间
	 */
	private Date updatedAt;

	/**
	 * 更新人
	 */
	private String updatedBy = "";

	private String authorName = "";//作者名字
	private Integer userState;//用户在职状态 0-在职，1-离职
	private String editUserName = "";//编辑人名
	private List<Integer> issueTypeList;//发布渠道
	private String shareName;

	private List<ActivityAnalystVo> analyst;
	private String summary = "";

	private List<ActivityTargetVo> secuList;

	private Date publishDate;

	private Date undoTime;

	public Date getUndoTime() {
		return undoTime;
	}

	public void setUndoTime(Date undoTime) {
		this.undoTime = undoTime;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getActivityStatus() {
		return activityStatus;
	}

	public void setActivityStatus(Integer activityStatus) {
		this.activityStatus = activityStatus;
	}

	public Long getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(Long companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyAbbr() {
		return companyAbbr;
	}

	public void setCompanyAbbr(String companyAbbr) {
		this.companyAbbr = companyAbbr;
	}

	public Integer getActivityType() {
		return activityType;
	}

	public void setActivityType(Integer activityType) {
		this.activityType = activityType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getOperatorUid() {
		return operatorUid;
	}

	public void setOperatorUid(String operatorUid) {
		this.operatorUid = operatorUid;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getUndoReason() {
		return undoReason;
	}

	public void setUndoReason(String undoReason) {
		this.undoReason = undoReason;
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Integer getUserState() {
		return userState;
	}

	public void setUserState(Integer userState) {
		this.userState = userState;
	}

	public String getEditUserName() {
		return editUserName;
	}

	public void setEditUserName(String editUserName) {
		this.editUserName = editUserName;
	}

	public List<Integer> getIssueTypeList() {
		return issueTypeList;
	}

	public void setIssueTypeList(List<Integer> issueTypeList) {
		this.issueTypeList = issueTypeList;
	}

	public String getShareName() {
		return shareName;
	}

	public void setShareName(String shareName) {
		this.shareName = shareName;
	}

	public List<ActivityAnalystVo> getAnalyst() {
		return analyst;
	}

	public void setAnalyst(List<ActivityAnalystVo> analyst) {
		this.analyst = analyst;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public List<ActivityTargetVo> getSecuList() {
		return secuList;
	}

	public void setSecuList(List<ActivityTargetVo> secuList) {
		this.secuList = secuList;
	}

}
