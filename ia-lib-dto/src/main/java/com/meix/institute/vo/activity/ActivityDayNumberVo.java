package com.meix.institute.vo.activity;

import java.io.Serializable;

public class ActivityDayNumberVo implements Serializable {

	private static final long serialVersionUID = 7635898138750308305L;
	private int num;
	private String startDate;
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	
}
