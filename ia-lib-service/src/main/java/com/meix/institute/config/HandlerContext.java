package com.meix.institute.config;

import com.meix.institute.annotation.HandleDataType;
import com.meix.institute.api.DataHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2020/4/12.
 */
@Configuration
public class HandlerContext {

	@Autowired
	private List<DataHandler> dataHandlerList;

	private Map<Integer, DataHandler> handlerMap;

	@PostConstruct
	private void init() {
		handlerMap = new HashMap<>();
		for (DataHandler handler : dataHandlerList) {
			HandleDataType anno = handler.getClass().getAnnotation(HandleDataType.class);
			if (anno == null) {
				continue;
			}
			handlerMap.put(anno.value(), handler);
		}
	}

	public DataHandler getHandler(int dataType) {
		if (handlerMap == null || !handlerMap.containsKey(dataType)) {
			return null;
		}
		return handlerMap.get(dataType);
	}

}
