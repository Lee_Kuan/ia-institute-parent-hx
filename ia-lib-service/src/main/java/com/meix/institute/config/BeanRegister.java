package com.meix.institute.config;

import com.meix.institute.config.bo.SecretProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * 注册Bean
 * Created by zenghao on 2020/4/27.
 */
@Configuration
public class BeanRegister {

	@Autowired
	private SecretProperty secretProperty;

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	public PdfConfig pdfConfig() throws Exception {
		PdfConfig pdfConfig = new PdfConfig();
		pdfConfig.setPdfServer(secretProperty.getStringValue("pdf.server"));
		pdfConfig.setPdfHost(secretProperty.getStringValue("pdf.host"));
		pdfConfig.setPdfUserName(secretProperty.getStringValue("pdf.username"));
		pdfConfig.setPdfPassword(secretProperty.getStringValue("pdf.password"));
		return pdfConfig;
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	public HxSmsConfig hxSmsConfig() {
		HxSmsConfig hxSmsConfig = new HxSmsConfig();
		hxSmsConfig.setUrl(secretProperty.getStringValue("hx.url"));
		hxSmsConfig.setUsername(secretProperty.getStringValue("hx.username"));
		hxSmsConfig.setPassword(secretProperty.getStringValue("hx.password"));
		return hxSmsConfig;
	}
}
