package com.meix.institute.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class CommonServerProperties {
	@Value("${spring.profiles.active}")
	private String env;

	@Value("${ins.companyCode}")
	private long companyCode; // 公司代码

	@Value("${ins.defaultUser}")
	private String defaultUser;

	@Value("${ins.defaultPassword}")
	private String defaultPassword;

	@Value("${ins.file.server}")
	private String fileServer;

	@Value("${ins.file.dir}")
	private String fileDir;

	@Value("${ins.file.tmpdir}")
	private String fileTmpdir;

	@Value("${ins.file.reportServer}")
	private String reportServer;

	@Value("${ins.file.uploadServer}")
	private String uploadServer;

	@Value("${meix.appId}")
	private String meixAppId;

	@Value("${meix.appSecret}")
	private String meixAppSecret;

	@Value("${meix.syncUrl}")
	private String meixSyncUrl;

	@Value("${wx.apiServer}")
	private String apiServer;//微信api服务域名

	@Value("${wx.openServer}")
	private String openServer;//微信openid服务域名
	
	@Value("${ins.email}")
	private String email;
}
