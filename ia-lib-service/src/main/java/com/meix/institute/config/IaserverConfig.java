package com.meix.institute.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by zenghao on 2019/8/26.
 */
@Component
@ConfigurationProperties(prefix = "ins.sync")
public class IaserverConfig {
	/**
	 * app版本（调用接口需要传递）
	 */
	private String appVersion;
	/**
	 * ia-router服务地址
	 */
	private String routerHome;
	/**
	 * 研究所内网项目地址
	 */
	private String insInnerHome;

	/**
	 * ia-web服务地址
	 */
	private String webServiceHome;
	
	
	public String getWebServiceHome() {
		return webServiceHome;
	}

	public void setWebServiceHome(String webServiceHome) {
		this.webServiceHome = webServiceHome;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getRouterHome() {
		return routerHome;
	}

	public void setRouterHome(String routerHome) {
		this.routerHome = routerHome;
	}

	public String getInsInnerHome() {
		return insInnerHome;
	}

	public void setInsInnerHome(String insInnerHome) {
		this.insInnerHome = insInnerHome;
	}
}
