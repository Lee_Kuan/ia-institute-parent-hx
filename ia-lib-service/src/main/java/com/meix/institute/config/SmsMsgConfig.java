package com.meix.institute.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 短信内容配置
 */
@Component
@ConfigurationProperties(prefix = "sms-msg", ignoreUnknownFields = true)
public class SmsMsgConfig {

	private String login;
	private String register;
	
	public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
}
