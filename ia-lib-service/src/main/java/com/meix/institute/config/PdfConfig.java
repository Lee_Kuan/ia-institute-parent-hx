package com.meix.institute.config;

import lombok.Data;

/**
 * Created by zenghao on 2020/4/27.
 */
@Data
public class PdfConfig {

	private String pdfServer;

	private String pdfHost;

	private String pdfUserName;

	private String pdfPassword;

}
