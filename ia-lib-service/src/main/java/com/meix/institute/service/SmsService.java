package com.meix.institute.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.config.HxSmsConfig;
import com.meix.institute.constant.MeixConstants;

/**
 * 整合短信发送
 * Created by zenghao on 2020/4/24.
 */
@Service
public class SmsService {

	@Autowired
	private HxSmsConfig hxSmsConfig;
	@Autowired
	private CommonServerProperties commonServerProperties;

	public String sendSMS(String mobile, String msg) {
		String result = null;
		if (commonServerProperties.getEnv().contains(MeixConstants.ACTIVE_LOCAL) 
				|| commonServerProperties.getEnv().contains(MeixConstants.ACTIVE_DEV)
				) {
			//测试环境使用美联软通
			MTSmsService.sendMessage(mobile, "", msg, "1");
		} else {
			//正式环境短信
			result = HxSmsService.sendSMS(hxSmsConfig.getUrl(), hxSmsConfig.getUsername(), hxSmsConfig.getPassword(), mobile, "1", msg);
		}
		return result;
	}

	public String sendSMS(List<String> mobiles, String msg) {
		String result = null;
		if (commonServerProperties.getEnv().contains(MeixConstants.ACTIVE_LOCAL) 
				|| commonServerProperties.getEnv().contains(MeixConstants.ACTIVE_DEV)
				) {
			//测试环境使用美联软通
			for (String mobile : mobiles) {
				MTSmsService.sendMessage(mobile, "", msg, "1");
			}
		} else {
			//正式环境短信
			StringBuffer mobile = new StringBuffer();
			int count = 0;
			for (String tmp : mobiles) {
				mobile.append(",");
				mobile.append(tmp);
				count ++;
				if (count >= 1000) {
					result = HxSmsService.sendSMS(hxSmsConfig.getUrl(), hxSmsConfig.getUsername(), hxSmsConfig.getPassword(), mobile.toString().substring(1), String.valueOf(count), msg);
					mobile = new StringBuffer();
					count = 0;
				}
			}
			if (count > 0) {
				result = HxSmsService.sendSMS(hxSmsConfig.getUrl(), hxSmsConfig.getUsername(), hxSmsConfig.getPassword(), mobile.toString().substring(1), String.valueOf(count), msg);
			}
		}
		return result;
	}
}
