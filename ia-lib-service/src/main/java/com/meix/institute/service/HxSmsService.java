package com.meix.institute.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.meix.institute.util.HttpUtil;

import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2020/03/30.
 */
public class HxSmsService {
	private static Logger log = LoggerFactory.getLogger(HxSmsService.class);

	
	  private static final Map<String, String> errorMap = new HashMap<String, String>() { 
		  private static final long serialVersionUID = 976346252084437759L;
	  
		  { 
			  put("-1", "参数为空。信息、电话号码等有空指针，登陆失败"); 
			  put("-2", "电话号码个数超过1000"); 
			  put("-10", "申请缓存空间失败"); 
			  put("-11", "电话号码中有非数字字符"); 
			  put("-12", "有异常电话号码"); 
			  put("-13", "电话号码个数与实际个数不相等"); 
			  put("-14", "实际号码个数超过1000");
			  put("-101", "发送消息等待超时"); 
			  put("-102", "发送或接收消息失败"); 
			  put("-103", "接收消息超时"); 
			  put("-200", "其他错误"); 
			  put("-999", "web服务器内部错误"); 
		  } 
	  };
	 

	/**
	 * 
	 * @param serverUrl
	 * @param userId
	 * @param password
	 * @param phoneNums 目标号码，用英文逗号(,)分隔，最大1000个号码。一次提交的号码类型不受限制，但手机会做验证，若有不合法的手机号将会被退回。号码段类型分为：移动、联通、电信手机 注意：请不要使用中文的逗号
						手机号码规则：
						如果只发送中国的号码，号码前面无需加国际电话区号(0086)；
						如果发送的号码当中既有中国也有国外，那么号码规则必须是：00+国家电话区号+手机号码。
						如：香港号码56455811,中国号码13265661400，那么发送时应该填0085256455811,008613265661400
	 * @param count		号码个数（最大1000个）
	 * @param msg 		短信内容， 内容长度不大于990个汉字
	 * @return
	 */
	public static String sendSMS(String serverUrl, String userId, String password, String phoneNums, String count, String msg) {
		try {

			Map<String, String> params = new HashMap<>();
			params.put("userId", userId);
			params.put("password", password);
			params.put("pszMobis", phoneNums);
			params.put("pszMsg", msg);
			params.put("iMobiCount", count);
			params.put("pszSubPort", "*");
			params.put("SvrType", "MEIXYJWX");
			log.info("serverUrl:{},params:{}", serverUrl, JSONObject.fromObject(params));
			Map<String, String> headMap = new HashMap<>();
			headMap.put("Content-Type", "application/x-www-form-urlencoded");
			byte[] resp = HttpUtil.postHttpBasic(serverUrl, headMap, params);
//								HttpUtil.getHttp(serverUrl, params, 5000);
			String res = resp == null ? null : new String(resp);
			if (res == null) {
				return "接口请求异常";
			} else {
				log.info("res:{}", res);
				if (res.contains("<")) {
					res = res.substring(0, res.lastIndexOf("<"));
				} else {
					return "解析结果失败";
				}
				if (res.contains(">")) {
					res = res.substring(res.lastIndexOf(">") + 1);
				} else {
					return "解析结果失败";
				}
				log.info("parse res:{}", res);
				if (errorMap.containsKey(res)) {
					return errorMap.get(res);
				} else {
					return null;
				}
			}
		} catch (Exception e) {
			log.error("", e);
			return "发送短信失败!";
		}
	}
	/******************调用样例参考********************/
 	/*
	  public static void main(String[] args) {
		SMSServer server = new SMSServer();
		try {
			String text;
			text = server.SMS("http://服务器地址/HttpInterface/HttpService.ashx", "0", " topuser", "e10adc3949ba59abbe56e057f20f883e", "13501296033", "短信测试内容","UTF-8");		System.out.print(text);  //返回值（String）：0、-1、-2、-3…
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/

}
