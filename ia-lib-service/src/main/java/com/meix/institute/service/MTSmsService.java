package com.meix.institute.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.meix.institute.config.MtSmsConfig;
import com.meix.institute.util.ApplicationUtil;
import com.meix.institute.util.StringUtil;
import com.research.sms.SmsClientSend;
import com.research.sms.SmsMessageInfo;

/**
 * @Description:美联软通短信
 */
public class MTSmsService {
	protected static final Logger log = LoggerFactory.getLogger(MTSmsService.class);
	public static final String CN_AREA_CODE = "86";

	private static MtSmsConfig mtSmsConfig;

	static {
		mtSmsConfig = ApplicationUtil.getBean(MtSmsConfig.class);
	}

	/**
	 * 发送美联软通短信
	 *
	 * @param mobile
	 * @param areaCode
	 * @param content
	 * @param _flag    替换掉睿舍取
	 */
	public static void sendMessage(String mobile, String areaCode, String content, String _flag) {
		if (StringUtil.isBlank(areaCode) || !StringUtil.isNumber(areaCode)) {
			areaCode = CN_AREA_CODE;
		}
		//81 09012345678 819012345678 以国际编码开头
		if (mobile.contains(",")) {
			String[] mobiles = mobile.split(",");
			StringBuilder sb = new StringBuilder();
			for (String value : mobiles) {
				sb.append(areaCode).append(value).append(",");
			}
			String phoneNumbers = sb.toString();
			mobile = phoneNumbers.substring(0, phoneNumbers.length() - 1);
		} else {
			mobile = areaCode + mobile;
		}
		String flag = mtSmsConfig.getFlag();
		if (_flag != null) {
			flag = _flag;
		}
		if (flag.equals("1")) {
			content = content.replaceAll("【睿舍取】", "");
		}
		Thread t = new Thread(new SmsThread(mobile, content, flag));
		t.start();
	}

	private static String msS5c(String mobile, String content) throws IOException {
		StringBuffer sb = new StringBuffer("http://m.5c.com.cn/api/send/?");
		sb.append("apikey=" + mtSmsConfig.getApikey());
		sb.append("&username=" + mtSmsConfig.getUsername());
		sb.append("&password=" + mtSmsConfig.getPassword());
		sb.append("&mobile=" + mobile);
		sb.append("&content=" + com.meix.institute.util.StringUtils.encodeUrl(content, "GBK"));
		URL url = new URL(sb.toString());
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		BufferedReader in = new BufferedReader(new InputStreamReader(
			url.openStream()));
		String returnCode = in.readLine();
		if (!returnCode.toLowerCase().contains("success")) {
			throw new IOException("msS5c发送失败,参数：" + sb.toString());
		}
		String message = in.readLine();
		in.close();
		connection.disconnect();
		return message;
	}

	private static class SmsThread implements Runnable {
		private String mobile;
		private String content;
		private String flag;

		public SmsThread(String mobile, String content, String flag) {
			this.mobile = mobile;
			this.content = content;
			this.flag = flag;
		}

		@Override
		public void run() {
			try {
				if (!StringUtils.isEmpty(flag)) {
					if ("1".equals(flag)) {
						msS5c(mobile, content);
					} else {
						SmsMessageInfo sms = SmsClientSend.send(mobile, content);
						String returnCode = sms.getReturnStatus();
						if (!returnCode.toLowerCase().contains("success")) {
							throw new IOException("发送失败");
						}
					}
				} else {
					msS5c(mobile, content);
				}
			} catch (IOException e) {
				log.error("{}|{}\n短信发送异常 FLAG{}", mobile, content, flag);
				e.printStackTrace();
				if (!StringUtils.isEmpty(flag)) {
					if ("1".equals(flag)) {
						SmsMessageInfo sms = SmsClientSend.send(mobile, content);
						sms.getReturnStatus();
					} else {
						try {
							msS5c(mobile, content);
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				} else {
					SmsClientSend.send(mobile, content);
				}
			}

		}

	}
}
