package com.meix.institute.api;

import java.util.List;
import java.util.Map;

import com.meix.institute.core.Item;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsCode;

/**
 * 枚举字典
 * @author Xueyc
 *
 */
public interface ICodeService {

    /**
     * 获得枚举列表
     * @param name 代码分类
     * @return 枚举列表
     */
    List<Item> findItem(String name);
    
    /**
     * 获得枚举说明
     * @param name 代码分类
     * @param code 代码
     * @return 枚举列表
     */
    String getText(String name, String code);
    
    /**
     * 获得枚举键值对
     * @param name 代码分类
     * @param code 代码
     * @return 枚举键值对
     */
    Item getItem(String name, String code);
    
    /**
     * 保存
     * @param code
     */
    void insert(InsCode code);
    
    /**
     * 批量保存
     * @param code
     */
    void insertList(List<InsCode> code);

    /**
     * 根据文本获取枚举ID
     * @param name 代码分类
     * @param text 文本内容
     * @return
     */
	String getCode(String name, String text);
	
	/**
	 * 保存
	 * @param code
	 */
	void save(InsCode code, String user);
	
	/**
	 * 根据唯一键获取
	 * @param name
	 * @param code
	 * @return
	 */
    InsCode selectByBizKey(String name, String code);
    
    /**
     * 根据唯一键删除
     * @param name
     * @param code
     */
    void deleteByBizKey(String name, String code);

    /**
     * 获取携宁行业代码与申万行业代码对应关系
     * @param type 0-全部，1-未匹配
     * @return
     */
	public Paged<Map<String, Object>> getIndustryCodes(int type, int currentPage, int showNum);

	/**
	 * 设置携宁行业代码与申万行业代码对应关系
	 * @param industryCode
	 * @param xnindCode
	 */
	public void setIndustryCode(int industryCode, int xnindCode);
	    
}
