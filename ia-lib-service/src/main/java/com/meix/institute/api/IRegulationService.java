package com.meix.institute.api;

import com.meix.institute.vo.HoneyBeeBaseVo;

import java.util.List;
import java.util.Set;

/**
 * Created by zenghao on 2019/10/10.
 */
public interface IRegulationService {

	/**
	 * 热点推荐
	 *
	 * @return
	 */
	List<HoneyBeeBaseVo> getHotRecommendList(int currentPage, int showNum, Set<Integer> selfStockSet, int issueType);

	/**
	 * 首页推荐
	 *
	 * @param uid
	 * @param currentPage
	 * @param showNum
	 * @param lastId
	 * @param selfStockSet
	 * @return
	 */
	List<HoneyBeeBaseVo> getRecommendList(long uid, int currentPage, int showNum, String lastId, Set<Integer> selfStockSet, int issueType);

}
