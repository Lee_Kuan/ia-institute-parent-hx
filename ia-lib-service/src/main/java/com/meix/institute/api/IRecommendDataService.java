package com.meix.institute.api;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by zenghao on 2019/10/9.
 */
public interface IRecommendDataService {

	Map<String, Object> getRecommendDataByIds(int dataType, List<Long> ids, long uid, Set<Integer> selfStockSet);

	/**
	 * 批量查询详情
	 *
	 * @param dataType
	 * @param ids
	 * @param uid
	 * @param selfStockSet
	 * @return
	 */
	Map<String, Map<String, Object>> getRecDataMapByIds(int dataType, List<Long> ids, long uid, Set<Integer> selfStockSet);
}
