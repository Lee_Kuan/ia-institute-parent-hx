package com.meix.institute.api;

import java.util.List;

import com.meix.institute.core.Item;

/**
 * 枚举字典
 * @author Xueyc
 *
 */
public interface IDictTagService {
    
    /**
     * 重置缓存
     */
    void reset();
    
    /**
     * 获得枚举列表
     * @param name 代码分类
     * @return 枚举列表
     */
    List<Item> findItem(String name);
    
    /**
     * 获得枚举说明
     * @param name 代码分类
     * @param code 代码
     * @return 枚举列表
     */
    String getText(String name, String code);
    
    /**
     * 获得枚举键值对
     * @param name 代码分类
     * @param code 代码
     * @return 枚举键值对
     */
    Item getItem(String name, String code);

}
