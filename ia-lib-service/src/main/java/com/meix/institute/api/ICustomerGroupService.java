package com.meix.institute.api;

import java.util.List;
import java.util.Map;

import com.meix.institute.vo.CustomerGroupVo;
import com.meix.institute.vo.GroupPowerVo;

import net.sf.json.JSONArray;

/**
 * @Description:用户组服务
 */
public interface ICustomerGroupService {

	/**
	 * 修改用户组
	 * @param uid
	 * @param groupId
	 * @param fromGroup
	 * @param user 
	 */
	public void saveUserGroup(long uid, long groupId, long fromGroup, String user);

	/**
	 * 获取用户组列表
	 * @param groupType
	 * @param isSelect 0-不是用来选择，返回列表数据，0-用来选择，只返回id和名称
	 * @param currentPage 
	 * @param showNum 
	 * @return
	 */
	public List<Map<String, Object>> getCustomerGroupList(int groupType, int isSelect, int showNum, int currentPage);

	/**
	 * 获取用户组总数
	 * @param groupType
	 * @return
	 */
	public int getCustomerGroupCount(int groupType);

	/**
	 * 删除用户组，只允许删除手动创建的
	 * @param groupId
	 * @param uuid 
	 * @return
	 */
	public int deleteCustomerGroup(long groupId, String uuid);

	/**
	 * 保存用户组信息
	 * @param groupId
	 * @param groupName
	 * @param groupType
	 * @param uuid
	 * @param powers
	 * @return groupId
	 */
	public long saveCustomerGroup(long groupId, String groupName, int groupType, String uuid, JSONArray powers);

	/**
	 * 获取用户组授权
	 * @param groupId
	 * @param groupType
	 * @return
	 */
	public List<Map<String, Object>> getCustomerGroupPower(long groupId, int groupType);

	/**
	 * 保存用户组用户
	 * @param groupId
	 * @param powers
	 * @param user
	 * @param fromGroup
	 */
	public void saveCustomerGroupUser(long groupId, JSONArray powers, String user, long fromGroup);

	/**
	 * 保存用户组授权
	 * @param dataId
	 * @param dataType
	 * @param dataSubType
	 * @param groupList
	 * @return share,公开返回0，否则返回1
	 */
	public int saveDataGroupShare(long dataId, int dataType, int dataSubType, JSONArray groupList);

	/**
	 * 获取内容的权限,按照level倒叙排列，出现有权限标志时，列表后面比此level小的位置都应该有权限
	 * @param uid 
	 * @param dataId
	 * @param dataType
	 * @param dataSubType
	 * @param share
	 * @return
	 */
	public List<GroupPowerVo> getResourceGroupPower(long uid, long dataId, int dataType, int dataSubType, int share);

	/**
	 * 获取默认有此处权限的组列表
	 * @param dataType 3-研报类，8-活动类，3000-合辑类，13-晨会类
	 * @param dataSubType 具体内容类型
	 * @param share 要查询哪个位置的权限，1-列表权限，2-详情权限，3-直播/全部内容，4-回放/原文
	 * @return
	 */
	public List<CustomerGroupVo> getDefaultPowerGroup(int dataType, int dataSubType, int share);

	/**
	 * 获取组内人员id
	 * @param groupId
	 * @return
	 */
	public List<Long> getGroupUserList(long groupId);

	/**
	 * 获取用户组名
	 * @param groupId
	 * @return
	 */
	public Map<String, Object> getCustomerGroupInfo(long groupId);

	/**
	 * 获取数据授权列表
	 * @param dataId
	 * @param dataType
	 * @param dataSubType
	 * @param share 权限位置
	 * @return
	 */
	public List<Map<String, Object>> getDataShareList(long dataId, int dataType, int dataSubType, int share);

	/**
	 * 获取用户属于的用户组列表名称，以","隔开
	 * @param uid
	 * @return
	 */
	public String getGroupNameListByUser(long uid);
}
