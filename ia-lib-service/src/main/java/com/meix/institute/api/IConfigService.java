package com.meix.institute.api;

import java.util.List;
import java.util.Map;

import com.meix.institute.entity.InsMessagePushConfig;
import com.meix.institute.vo.InsMessagePushConfigVo;

public interface IConfigService {

	public long saveMessagePushConfig(InsMessagePushConfig config);

	public List<InsMessagePushConfig> getPushConfigList(int messageType, int currentPage, int showNum);

	public int getPushConfigCount(int messageType);

	public InsMessagePushConfigVo getPushConfigDetail(int id);

	public void userMessageConfigPushJob();

	/**
	 * 获取接收人列表，未推送的查询用户表，并集配置表，已推送的查询结果表
	 * @param id 推送id
	 * @param receiverType 接收人类型，1-白名单，2-潜在用户，4-游客（之和）
	 * @param pushStatus 推送状态0-未推送，1-已推送
	 * @param string 
	 * @param currentPage 分页
	 * @param showNum 分页
	 * @return
	 */
	public List<Map<String, Object>> getReceiverList(int id, List<Integer> receiverType, int pushStatus, int pushType, int currentPage, int showNum);

	public int getReceiverCount(int id, List<Integer> receiverType, Integer pushStatus);

	/**
	 * 获取系统配置连接
	 * @param type
	 * @return
	 */
	public String getSystemUrl(String type);
	
}
