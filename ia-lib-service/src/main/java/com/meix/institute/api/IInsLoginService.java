package com.meix.institute.api;

import org.springframework.stereotype.Service;

import com.meix.institute.pojo.InsUserInfo;
import com.meix.institute.pojo.InsUserSearch;
import com.meix.institute.vo.SecToken;

@Service
public interface IInsLoginService {
    
    /**
     * 密码认证
     * @param search
     * @return
     */
	InsUserInfo authByPassword(InsUserSearch search);
	
	/**
	 * 保存token
	 * @param token
	 * @param user
	 * @param clientType
	 */
    void saveToken(String token, String user, Integer clientType);
    
	/**
	 * 保存token
	 * @param token
	 * @param user
	 * @param clientType
	 * @param ltpaToken
	 */
    void saveToken(String token, String user, Integer clientType, String ltpaToken);
    
    /**
     * 修改密码
     * @param id
     * @param newPwd
     * @param user
     */
    void modifyPwd(long id, String newPwd, String user);
    
    /**
     * 注销(设置token无效)
     * @param user
     */
    void logout(String user, Integer clientType);
    
    /**
     * 获取token
     * @param user
     * @param clientType 
     * @return
     */
    SecToken getTokenByUser(String user, Integer clientType);

    /**
     * 生成token并返回
     * @param uid
     * @param user
     * @param companyCode
     * @param userState
     * @param clientType
     * @param ltpaToken
     * @return
     */
	public String createToken(long uid, String user, long companyCode, int userState, int clientType, String ltpaToken);

	/**
	 * 将三方token转换成meix token
	 * @param ltpaToken
	 * @param clientType
	 * @return
	 */
	public String getTokenByLtpaToken(String ltpaToken, String clientType);

	/**
	 * 存储三方token与meix token映射
	 * @param token
	 * @param ltpaToken
	 */
	public void updateToken(String token, String ltpaToken);
}
