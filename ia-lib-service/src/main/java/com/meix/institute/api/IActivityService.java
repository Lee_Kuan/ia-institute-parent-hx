package com.meix.institute.api;


import com.meix.institute.entity.InsActivity;
import com.meix.institute.entity.InsActivityAnalyst;
import com.meix.institute.entity.InsActivityPerson;
import com.meix.institute.vo.activity.*;
import com.meix.institute.vo.activity.gensee.ActivityGenseeRecord;
import com.meix.institute.vo.activity.gensee.ActivityThirdJoinInfo;
import com.meix.institute.vo.gensee.GenseeVideo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 活动服务
 *
 * @author likuan
 * @date 2019年7月30日15:45:34
 */
public interface IActivityService {
	/**
	 * @param resId
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @Description: 保存活动
	 */
	public long saveActivity(long resId, String uuid, InsActivity activity, GenseeVideo genseeVideo, JSONArray analyst, JSONArray industry,
	                         JSONArray secu, JSONArray contact, JSONArray issueType);

	InsActivity getBaseActivityById(long dataId);

	/**
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @Description: 活动详情
	 */
	public InsActivityVo getActivityDetail(long uid, long activityId, int checkPermission) throws IllegalAccessException, InvocationTargetException;


	/**
	 * @param stockRange
	 * @param industry
	 * @param citys
	 * @param combId
	 * @param innerCode
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @Description: 查询活动接口
	 */
	public List<ActivityVo> getActivityList(ActivityListSearchVo searchVo, int stockRange, JSONArray industry,
	                                        JSONArray industry2, long version, JSONArray citys, long combId, int innerCode) throws IllegalAccessException, InvocationTargetException;

	public Map<String, Object> convertSecuInfo(Map<String, Object> secuMap, int relationCodeHg, String researchObject,
	                                           int activityType);

	/**
	 * 按月获取每天的活动数量
	 */
	public List<ActivityDayNumberVo> getActivityNumOfDayByMonth(long uid, String uuid, String date);

	/**
	 * 更新活动状态
	 *
	 * @param activityId
	 * @param isEnd
	 * @param uuid
	 * @return
	 */
	public int updateActivityStatus(long activityId, int isEnd, String uuid);

	/**
	 * @param activityId
	 * @param uid
	 * @return
	 * @Title: getJoinPerson、
	 * @Description:获取参加人员信息
	 * @return: LowerHashMap
	 */
	public InsActivityPerson getJoinPerson(long activityId, long uid);

	public List<InsActivityAnalyst> getActivityAnalyst(long dataId);

	public List<OrgOrUserVo> getOrgOrUserContactList(int currentPage, int showNum, String condition, int type);

	/**
	 * @return 0、已参加，1、新增
	 * @Description: 活动参加人员
	 */
	public int saveJoinActivity(long uid, long activityId, String userName, String mobile,
	                            String companyName,
	                            String position);

	public int getActivityStatus(int activityType, int originalIsEnd, int endStatus, Date startTime,
	                             Date endTime);

	public int getActivityIsEnd(InsActivity activity);

	List<Integer> getActivityIssueType(long activityId);

	/**
	 * 获取获取相关股票和Conps行业
	 *
	 * @param dataId
	 * @return
	 */
	public Map<String, Object> getActivityInnerCodeAndIndustry(long dataId);

	/**
	 * 参加活动
	 *
	 * @param uid
	 * @param activityId
	 * @param user
	 */
	public void attendActivity(long uid, long activityId, String user);

	InsActivityVo getActivityDetail(long uid, long activityId, int checkPermission, boolean getCustomerId)
		throws IllegalAccessException, InvocationTargetException;

	public void updateActivity(InsActivity activity, GenseeVideo genseeVideo);

	public void deleteActivity(long activityId, String uuid);

	/**
	 * 删除直播收听记录
	 *
	 * @param activityId
	 * @return
	 */
	int deleteGenseeRecord(long activityId);

	/**
	 * 保存直播收听记录
	 *
	 * @param list
	 * @return
	 */
	int saveGenseeRecordList(List<ActivityGenseeRecord> list);

	void syncGenseeRecord(String startDate, String endDate, long activityId);

	/**
	 * 删除电话接入收听记录
	 *
	 * @param activityId
	 * @return
	 */
	int deleteTeleJoinRecord(long activityId);

	/**
	 * 保存电话接入收听记录
	 *
	 * @param list
	 * @return
	 */
	int saveTeleJoinRecordList(List<ActivityThirdJoinInfo> list);

	void syncTeleJoinRecord(String startDate, String endDate, long activityId);

	public void deleteGenseeVod(long activityId);

	public void saveGenseeVodList(List<JSONObject> list);

}
