package com.meix.institute.api;

import java.util.List;

import com.meix.institute.core.Key;
import com.meix.institute.core.Paged;
import com.meix.institute.pojo.InsSyncSndReqInfo;
import com.meix.institute.pojo.InsSyncSndReqSearch;

public interface IInsSyncSndReqService {
    
    /**
     * 保存同步数据
     * @param info
     * @param user
     * @return
     */
    Key save(InsSyncSndReqInfo info, String user);
    
    /**
     * 根据主键获取同步数据
     * @param id
     * @return
     */
    InsSyncSndReqInfo selectByPrimaryKey(Long id);
    
    /**
     * 查找同步异常请求
     * @return
     */
    List<InsSyncSndReqInfo> findExceptionReq();
    
    /**
     * 发送请求列表
     * @param search
     * @param user
     * @return
     */
    Paged<InsSyncSndReqInfo> paged(InsSyncSndReqSearch search, String user);
    
    /**
     * 关闭
     * @param id
     * @param remarks
     * @param user
     */
	void close(Long id, String remarks, String user);

	public void retry(Long id, String user);

}
