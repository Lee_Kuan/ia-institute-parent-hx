package com.meix.institute.api;

import java.util.List;

import com.meix.institute.entity.InsActivity;
import com.meix.institute.search.ComplianceListSearchVo;
import com.meix.institute.vo.activity.InsActivityVo;

/**
 * Created by zenghao on 2019/7/31.
 */
public interface IInsActivityService {

	InsActivity getActivity(long activityId, long companyCode);

	List<InsActivityVo> getComplianceList(ComplianceListSearchVo searchVo);

	long getComplianceCount(ComplianceListSearchVo searchVo);

	void examineActivity(InsActivity activity);

	public List<InsActivityVo> getManagementList(ComplianceListSearchVo searchVo);

	public int getManagementCount(ComplianceListSearchVo searchVo);

	public void examineJoinPersonList(long id, int optType, String uuid);

	public void activityStartRemind(long companyCode);

}
