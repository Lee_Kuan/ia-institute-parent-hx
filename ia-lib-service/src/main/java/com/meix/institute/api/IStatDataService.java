package com.meix.institute.api;

import com.meix.institute.search.ClueSearchVo;
import com.meix.institute.search.DataRecordStatSearchVo;
import com.meix.institute.search.UserRecordSearchVo;
import com.meix.institute.vo.company.CompanyClueInfo;
import com.meix.institute.vo.company.CompanyClueUserStat;
import com.meix.institute.vo.stat.DataRecordStatVo;
import com.meix.institute.vo.stat.HeatDataVo;
import com.meix.institute.vo.user.UserClueStat;
import com.meix.institute.vo.user.UserRecordStatVo;

import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/8/2.
 */
public interface IStatDataService {

	/**
	 * 研究所电话会议数据阅读、分享次数统计
	 */
	void executeActivityRecordStatJob();

	/**
	 * 用户阅读、分享研究所电话会议次数统计
	 */
	void executeUserRecordStatJob();

	/**
	 * 研究所研报数据阅读、分享次数统计
	 */
	void executeResearchReportStatJob();

	/**
	 * 研究所关注分析师商机
	 */
	void executeInsSubscribeAnalystJob();

	void executeInsJoinActivityJob();

	void executeInsListenActivityJob();

	void executeInsReadResearchJob();

	int insertClueUserStatList(List<CompanyClueUserStat> list);

	int removeClueUserStatRange(long startId, long endId);

	List<DataRecordStatVo> getDataRecordStatList(DataRecordStatSearchVo statSearchVo);

	long getDataRecordStatCount(DataRecordStatSearchVo statSearchVo);

	List<UserRecordStatVo> getUserRecordStatList(UserRecordSearchVo searchVo);

	long getUserRecordStatCount(UserRecordSearchVo searchVo);

	List<CompanyClueInfo> getClueList(ClueSearchVo searchVo);

	long getClueCount(ClueSearchVo searchVo);

	List<UserClueStat> getUserClueStatList(ClueSearchVo searchVo);

	long getUserClueStatCount(ClueSearchVo searchVo);

	int removeHeatDataRange(long startId, long endId);

	void insertHeatDataList(List<HeatDataVo> list);

	/**
	 * 获取热点数据
	 *
	 * @param search
	 * @return
	 */
	List<Map<String, Object>> getHeatData(Map<String, Object> search);

	/**
	 * 获取热点数据数量
	 *
	 * @param search
	 * @return
	 */
	Map<String, Object> getHeatDataCount(Map<String, Object> search);

	/**
	 * 分析师数据总览列表
	 *
	 * @param search
	 * @return
	 */
	List<Map<String, Object>> getUserReadData(Map<String, Object> search);

	/**
	 * 分析师数据总览数量
	 *
	 * @param search
	 * @return
	 */
	Map<String, Object> getUserReadDataCount(Map<String, Object> search);

	List<Map<String, Object>> getUserReadDataByUser(Map<String, Object> search);

	Map<String, Object> getUserReadDataByUserCount(Map<String, Object> search);
}
