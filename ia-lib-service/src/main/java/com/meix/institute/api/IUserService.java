package com.meix.institute.api;

import java.util.List;
import java.util.Map;

import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsUser;
import com.meix.institute.response.PersonalInfoPo;
import com.meix.institute.search.ClueSearchVo;
import com.meix.institute.search.ComplianceCommentSearchVo;
import com.meix.institute.vo.Comment;
import com.meix.institute.vo.CommentVo;
import com.meix.institute.vo.IA_PWDMapping;
import com.meix.institute.vo.user.UserClueStat;
import com.meix.institute.vo.user.UserContentReadStat;
import com.meix.institute.vo.user.UserIdVo;
import com.meix.institute.vo.user.UserInfo;
import com.meix.institute.vo.user.UserRecommendationInfo;
import com.meix.institute.vo.user.UserRecordStateResp;
import com.meix.institute.vo.user.UserRecordStateVo;

import net.sf.json.JSONArray;

/**
 * Created by zenghao on 2019/9/19.
 */
public interface IUserService {
	/**
	 * 获取我关注的人员列表
	 *
	 * @author jiawj
	 * @since 2015年4月13日 上午10:08:22
	 */
	public List<Map<String, Object>> getMyConcrenedPersons(Map<String, Object> params);

	/**
	 * @Dscription获取所有用户
	 * @Param
	 * @Return
	 */
	public List<UserInfo> getUser(Map<String, Object> params);

	/**
	 * 待审核用户列表
	 *
	 * @param params
	 * @return
	 */
	List<Map<String, Object>> getUncheckedUserList(Map<String, Object> params);

	int getUncheckedUserCount(Map<String, Object> params);

	/**
	 * @return
	 * @Title: getPersonalInfo、
	 * @Description:获取个人信息
	 * @return: PersonalInfoPo
	 */
	public PersonalInfoPo getPersonalInfo(long uid, long authorId);

	/**
	 * @author zhangzhen(J)
	 * @date 2015年11月12日上午10:24:47
	 * @Description: 保存用户标签
	 */
	public void saveSystemLabel(Map<String, Object> params);

	/**
	 * @param uid
	 * @param searchType  1 返回所有系统标签，2只返回选择的系统标签
	 * @param requireType 1、用户设置行业标签(默认) 2、活动设置行业
	 * @author zhangzhen(J)
	 * @date 2015年11月12日上午9:57:37
	 * @Description: 获取行业系统标签
	 */
	public List<Map<String, Object>> getSystemLabel(long uid, int searchType, int requireType);

	/**
	 * @author zhangzhen(J)
	 * @date 2016年3月28日下午2:04:46
	 * @Description: 通过微信的openId获取用户id
	 */
	@SuppressWarnings("rawtypes")
	public Map getUidByOpenId(String openId, int clientType);

	/**
	 * 2017年7月21日下午4:10:46<br>
	 * 保存服务留言
	 *
	 * @param hideNameFlag
	 * @author zhangzhen(J)
	 */
	long saveServerComment(long uid, long pointId, long pointGroupId, Float score, String comment, long dataId, int dataType, int hideNameFlag);

	/**
	 * @param uid         当前用户
	 * @param id          被评价者的id
	 * @param type        : 3研报 8活动
	 * @param currentPage
	 * @param showNum
	 * @return
	 * @Description:获取评价列表
	 */
	public List<CommentVo> getCommentList(long uid, long id, int type, long version, int currentPage, int showNum,
	                                      JSONArray ids, long companyCode, String condition);

	/**
	 * @param uid
	 * @param searchType 0热门关注人员列表 1我关注的人 2关注我的人 3互相关注的人 4热门关注人员关注的人员列表
	 * @return
	 * @Title: getFocusedPersons、
	 * @Description:关注列表
	 */
	public List<Map<String, Object>> getFocusedPersons(long uid, int searchType, int showNum, int currentPage);

	/**
	 * 获取用户的关注状态
	 *
	 * @param list
	 * @param followEachOtherList
	 * @param bCheckFocusEachOther
	 * @return
	 */
	public List<Map<String, Object>> getUserInfoList(List<UserIdVo> list, List<UserIdVo> followEachOtherList, boolean bCheckFocusEachOther);

	/**
	 * 保存最后一次点击记录
	 *
	 * @param vo
	 */
	public void saveLastUserRecord(UserRecordStateVo vo);

	/**
	 * 获取公众号消息推送所需要的openId
	 *
	 * @param clientType
	 * @author likuan    2019年6月28日14:55:23
	 */
	public String getOpenIdByUid(long uid, String appId, int clientType);

	/**
	 * @param areaCode
	 * @param account
	 * @return
	 * @Title: getUserInfo、
	 * @Description:获取用户信息
	 * @return: UserInfo
	 */
	public UserInfo getUserInfo(long uid, String areaCode, String account);

	UserInfo getUserInfoByUuid(String user);

	/**
	 * @param vo
	 * @return
	 * @Title: insertUserRecordState、
	 * @Description:保存用户记录状态
	 * @return: int
	 */
	public int saveUserRecordState(UserRecordStateVo vo);

	/**
	 * @param vo
	 * @param log          是否记录流水日志
	 * @param isPushFollow 是否推送关注
	 * @return
	 * @Title: insertUserRecordState、
	 * @Description:保存用户记录状态
	 * @return: int
	 */
	public int saveUserRecordState(UserRecordStateVo vo, boolean log, boolean isPushFollow);

	/**
	 * @param vo
	 * @param log          是否记录流水日志
	 * @param isPushFollow 是否推送关注
	 * @param queue        是否加入到队列中处理
	 * @return int
	 * @Title: saveUserRecordState、
	 * @Description:保存用户记录状态
	 */
	public int saveUserRecordState(UserRecordStateVo vo, boolean log, boolean isPushFollow, boolean queue);

	/**
	 * 批量保存用户记录状态
	 *
	 * @param vo
	 * @return
	 */
	int saveUserRecordStateList(List<UserRecordStateVo> vo);

	/**
	 * @author zhangzhen(J)
	 * @date 2016年10月13日下午3:11:33
	 * @Description: 保存留言
	 */
	public void saveComment(Comment comment);

	/**
	 * @Dscription保存验证码
	 * @Createtime 2014-4-17下午01:48:30
	 * @Param
	 * @Return
	 */
	public String saveVerCode(IA_PWDMapping bean);

	/**
	 * @param mobile    手机号
	 * @param loginType 登录类型 1 手机  2 邮箱 3 二维码 4 微信
	 * @param verCode   验证码
	 * @return boolean
	 * @Title: checkVerCode、
	 * @Description:校验验证是否有效
	 */
	public boolean checkVerCode(String mobile, int loginType, String verCode);

	/**
	 * 通过uid跟新该用户的CompanyCode
	 *
	 * @param uid
	 * @param userName
	 */
	public void updateUserCompanyCode(long uid, long companyCode, String userName);

	UserInfo addUser(String mobile, String companyName, String nickname);

	/**
	 * @param user
	 * @return
	 * @Title: addUser、
	 * @Description:添加用户
	 * @return: int
	 */
	public long addUser(UserInfo user);

	int updateUser(UserInfo user);

	/**
	 * 研究所登录绑定微信
	 *
	 * @param openId
	 * @param appId
	 * @param uid
	 * @param clientType
	 */
	public void bindYJSAccount(String openId, String appId, long uid, int clientType);

	/**
	 * @param uid
	 * @Title: unbindThirdAccount、
	 * @Description:解绑第三方账户
	 * @return: void
	 */
	public void unbindThirdAccount(long uid, String openId, int clientType);

	/**
	 * @param openId
	 * @param uid
	 * @Title: bindMeixAccount、
	 * @Description:绑定每市账户
	 * @return: void
	 */
	public void bindMeixAccount(String openId, String appId, long uid, int clientType);

	/**
	 * @Dscription登录验证,成功后保存用户信息
	 * @Param
	 * @Return
	 */
	public UserInfo login(IA_PWDMapping bean, UserInfo user, Integer clientType, boolean resetToken);

	/**
	 * @Dscription退出登录
	 * @Createtime 2014-4-24下午03:41:08
	 * @Param
	 * @Return
	 */
	public String loginOut(String token);

	/**
	 * @Description: 获取用户最新名片
	 */
	public Map<String, Object> selectCardByUid(long uid, String mobile, int type);

	/**
	 * 解绑微信账号绑定
	 *
	 * @author likuan    2019年7月31日19:10:02
	 */
	public void unbindweixin(String mobile, int clientType);

	/**
	 * @param params
	 * @return
	 * @Title: addCustomerCard、
	 * @Description:添加客户名片
	 * @return: int
	 */
	@SuppressWarnings("rawtypes")
	public long addCustomerCard(Map params);

	/**
	 * @param uid
	 * @param cardId
	 * @param authStatus
	 * @return
	 * @Title: updateUserCard、
	 * @Description:更新用户名片
	 * @return: int
	 */
	public int updateUserCard(long uid, long cardId, int authStatus, String companyAbbr, String email, String position, String username);

	/**
	 * 获取用户列表
	 *
	 * @param params
	 * @return
	 */
	Paged<Map<String, Object>> getContactList(Map<String, Object> params);

	/**
	 * 用户信息审核
	 */
	void updateUserAuditStatus(Map<String, Object> params);

	/**
	 * @Dscription 登录成功后设置验证为作废状态
	 * @Param
	 */
	public boolean updateVerCode(String mobile, int loginType);

	public List<UserRecommendationInfo> getRelatedRecommendations(long uid, long authorId, long dataId, int dataType,
	                                                              int currentPage, int showNum, int clientType, int type);

	boolean isFocusedPerson(long uid, long authorCode);

	UserInfo getThirdUserInfo(String thirdId, String mobile);

	/**
	 * 保存第三方用户
	 *
	 * @param userInfo
	 */
	public long saveThirdUser(UserInfo userInfo);

	void callLabelScoreUpdate();

	/**
	 * 获取分析师列表
	 *
	 * @param params
	 * @return
	 */
	public Paged<Map<String, Object>> getAnalystList(Map<String, Object> params);

	public void saveLoginInfo(long id);

	/**
	 * 获取用户列表
	 *
	 * @param accountType
	 * @param authStatus
	 * @param condition
	 * @param showNum
	 * @param currentPage
	 * @param sortRule
	 * @param sortField
	 * @param groupId 
	 * @param isExcept 
	 * @return
	 */
	public List<Map<String, Object>> getCustomerList(int accountType, int authStatus, String condition, int currentPage, int showNum, String sortField, int sortRule, long groupId, int isExcept);

	public List<Map<String, Object>> getCustomerList(int accountType, int authStatus, String condition, int currentPage, int showNum,
	                                                 String updatedAt, String sortField, int sortRule, long groupId, int isExcept);

	public int getCustomerCount(int accountType, int authStatus, String condition, long groupId, int isExcept);

	/**
	 * 更新用户信息
	 *
	 * @param userInfo
	 */
	public void updateUserInfo(UserInfo userInfo);

	public List<UserClueStat> getAuthedUserClueList(ClueSearchVo searchVo);

	public int getAuthedUserClueCount(ClueSearchVo searchVo);

	public List<CommentVo> getCommentList(ComplianceCommentSearchVo searchVo);

	public int getCommentCount(ComplianceCommentSearchVo searchVo);

	public void examineComment(long commentId, int optType, String uuid);

	public void updateWechatInfo(UserInfo userInfo);

	public List<UserInfo> getPushWechatMsgUserList(long companyCode);

	public void userLabelPush(long companyCode);

	public boolean isFromMyFocusedPerson(long uid, int dataType, long dataId);

	public boolean isFromMyFocusedStock(long uid, int dataType, long dataId);

	public void saveUserLabelPushRecord(long uid, long dataId, int dataType);

	public void updateThirdIdByMobile(String thirdId, String mobile);

	public void updateUserPrivacyPolicyStatus(long uid, int status, String user);

	List<UserRecordStateResp> getUserRecordStateList(Map<String, Object> params);

	/**
	 * 用户发布数据被浏览次数统计
	 *
	 * @param params
	 * @return
	 */
	List<UserContentReadStat> getUserContentReadStat(Map<String, Object> params);

	/**
	 * 发布内容数
	 *
	 * @param params
	 * @return
	 */
	long getUserContentCount(Map<String, Object> params);

	long getDataReadNum(long uid, String startTime, String endTime);

	long getInsUidByCustomerId(long uid);

	public UserInfo getUserInfoByUserName(String salerName);

	public UserInfo getWhiteUserInfo(String mobile);

	public void deleteWhiteUser(String uuid, long uid, int accountType);

	long addUser(UserInfo user, boolean checkExist);

	public void updateUserIdentify(long uid, int identify);

	/**
	 * 通过oa通用账号获取用户信息
	 * @param loginName
	 * @return
	 */
	public InsUser getUserInfoByOaName(String loginName);

	/**
	 * 账号封禁
	 * @param uid
	 * @param uuid
	 */
	public void deleteUser(long uid, String uuid);

	/**
	 * 存储后台管理操作日志
	 * @param params
	 */
	public void saveUserOperatorLog(Map<String, Object> params);

}
