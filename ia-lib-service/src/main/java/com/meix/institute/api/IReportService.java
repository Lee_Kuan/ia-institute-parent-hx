package com.meix.institute.api;


import com.meix.institute.entity.InsReportAlbum;
import com.meix.institute.entity.ReportInfo;
import com.meix.institute.entity.ReportRelation;
import com.meix.institute.search.ComplianceListSearchVo;
import com.meix.institute.vo.report.ReportAlbumVo;
import com.meix.institute.vo.report.ReportDetailVo;
import net.sf.json.JSONArray;
import oracle.sql.BLOB;

import java.util.List;
import java.util.Map;

/**
 * @author likuan
 * @date 2019年9月23日13:46:49
 * @describe 研报
 */
public interface IReportService {

	public long saveReport(long resId, String uuid, ReportInfo report, List<ReportRelation> relation, JSONArray issueType);

	public ReportDetailVo getReportDetail(long researchId, boolean getCustomerId);

	public List<ReportDetailVo> getReportList(ComplianceListSearchVo searchVo);

	public int getReportCount(ComplianceListSearchVo searchVo);

	public void examineReport(ReportInfo report);

	/**
	 * 获取带权限的研报详情
	 *
	 * @param id
	 * @param uid
	 * @return
	 */
	public ReportDetailVo getReportDetail(long id, long uid);

	/**
	 * 根据第三方id删除研报信息
	 *
	 * @param thirdId
	 */
	public void deleteByThirdId(String thirdId);

	/**
	 * 获取研报相关个股和comps行业
	 *
	 * @param dataId
	 * @return
	 */
	public Map<String, Object> getReportInnerCodeAndIndustry(long dataId);

	public List<ReportDetailVo> getReportAllList(long uid, int type, long companyCode, int showNum, int currentPage, int clientType,
	                                             long authorCode, List<String> infoTypeLists, String publishDateFm, String publishDateTo, String condition);

	public List<Map<String, Object>> getReportAlbumList(int type, int showNum, int currentPage);

	public List<ReportDetailVo> getReportListByAlbum(long bid, long submenuId, int currentPage, int showNum, int type, int clientType);

	public Map<String, Object> getReportAlbumDetail(long uid, long bid);

	public long saveReportAlbum(long resId, String uuid, long id, long companyCode, String title, String subtit, String albumDesc, String coverLgUrl,
	                            String coverLgName, String coverSmUrl, String coverSmName, int catalog, int no, int visible, int isTop,
	                            int share, JSONArray subObj, JSONArray researchObj, JSONArray issueType, String url, int type);

	public int getReportAlbumCount(int type, int visible, String condition, String startTime, String endTime);

	public int getReportAlbumCount(int type, int visible, String condition, String startTime, String endTime, int status, int share);

	public List<Map<String, Object>> getReportAlbumList(int type, int visible, String condition, String startTime,
	                                                    String endTime, int showNum, int currentPage, int status, int clientType, int sortField, int sortRule, int share);

	public Map<String, Object> getReportAlbumDetail(int type, long bid);

	public List<ReportAlbumVo> getReportAlbumList(ComplianceListSearchVo searchVo);

	public int getReportAlbumCount(ComplianceListSearchVo searchVo);

	public void examineReportAlbum(InsReportAlbum reportAlbum);

	public void saveReportPdf(Long researchId, String pdfPath);

	public List<Map<String, Object>> getReportAllList(int type, int showNum, int currentPage, String lastSyncTime);

	/**
	 * 从oracle获取pdf文件
	 * @param fileId
	 * @return
	 */
	public BLOB getReportFile(String fileId);

	public int getReportDeepStatus(long researchId);

}
