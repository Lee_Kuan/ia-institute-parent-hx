package com.meix.institute.api;

import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;

import com.meix.institute.pojo.InsSyncRecReqInfo;
import com.meix.institute.pojo.InsSyncSndReqInfo;

import net.sf.json.JSONObject;

/**
 * 请求每市服务器做同步操作
 * Created by zenghao on 2020/4/9.
 */
public interface IMeixSyncService {

	public void reqMeixSync(int dataType, String action, Map<String, Object> params);

	public Date getLastSendDateTime(String action);

	public String reqMeixFileSync(File file);

	public String reqMeixFileSync(InputStream is, String fileName);

	public InsSyncRecReqInfo handleSndSyncAction(InsSyncRecReqInfo reqRecord);

	public InsSyncSndReqInfo reqMeixSync(InsSyncSndReqInfo sndRecord);

	public JSONObject reqMeixStockYield(String params);

	public JSONObject getGoldStockCombYield();

}
