package com.meix.institute.api;

import com.meix.institute.core.Paged;
import com.meix.institute.entity.SecuMain;

import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/8/28.
 */
public interface ISecumainService {

	void saveSecumainList(List<SecuMain> list);

	List<SecuMain> getSecuMain(int secuType, int type, int showNum, String condition, int industryCode);

	/**
	 * @return
	 * @Title: getIndustryInfo、
	 * @Description:获取所有行业
	 * @return: List<ListItem>
	 */
	public List<SecuMain> getIndustryInfo();

	SecuMain getSecuMain(int innerCode);
	
	/**
	 * 获取comps行业列表
	 * @param search
	 * @param uuid
	 * @return
	 */
    Paged<Map<String, Object>> getCompsIndustry(Map<String, Object> search, String uuid);
    
    /**
     * 根据股票代码获取股票信息
     * @param secuCode 股票代码
     * @return
     */
    SecuMain getSecuMainBySecuCode(String secuCode);
}
