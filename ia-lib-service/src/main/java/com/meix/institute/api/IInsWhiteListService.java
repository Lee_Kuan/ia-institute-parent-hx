package com.meix.institute.api;

import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.meix.institute.entity.InsActivityPerson;
import com.meix.institute.entity.InsCompanyInstituteUserActivity;

import net.sf.json.JSONArray;

public interface IInsWhiteListService {

	/**
	 * 保存白名单用户信息
	 * @param groupId 
	 * @param levelName 
	 * @param levelId 
	 */

	public List<String> saveWhiteUserInfo(String uuid, JSONArray datas, long groupId);
	
	public List<String> saveWhiteUserInfo(String uuid, JSONArray datas, long groupId, XSSFSheet sheet);
	
	public List<String> saveJoinPerson(String uuid, long activityId, JSONArray datas);

	public List<String> saveJoinPerson(String uuid, long activityId, JSONArray datas, XSSFSheet sheet);
	
	public List<InsActivityPerson> getJoinPersonList(long activityId, int showNum, int currentPage, String condition, int status);

	public int getJoinPersonCount(long activityId, String condition);

	public String getActivityTitle(long activityId);

	public void addActivityUserInfo(List<InsCompanyInstituteUserActivity> userList);

	public void deleteActivityUser(long activityId);

	public void deleteWhiteUser(String uuid, long companyCode, JSONArray ids, long groupId);

	public List<InsCompanyInstituteUserActivity> getActivityUserList(long activityId);

}
