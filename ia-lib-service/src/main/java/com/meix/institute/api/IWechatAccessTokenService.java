package com.meix.institute.api;

import com.meix.institute.entity.WechatAccessToken;

/**
 * Created by zenghao on 2019/9/27.
 */
public interface IWechatAccessTokenService {

	WechatAccessToken getWechatAccessToken(long companyCode, String appId);

	int saveWechatAccessToken(long companyCode, String appId, String accessToken, long expiresIn, String updateTime);

	int updateWechatAccessToken(long companyCode, String appId, String accessToken, long expiresIn, String updateTime);

	WechatAccessToken getWechatTicket(long companyCode, String appId);

	int saveWechatTicket(long companyCode, String appId, String accessToken, long expiresIn, String updateTime);

	int updateWechatTicket(long companyCode, String appId, String accessToken, long expiresIn, String updateTime);
}
