package com.meix.institute.api;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.core.Key;
import com.meix.institute.entity.InsVerificationCode;
import com.meix.institute.mapper.InsVerificationCodeMapper;
import com.meix.institute.vo.commen.InsVerificationCodeVo;

@Service
public class IVerificationCodeService {
    
    @Autowired
    private InsVerificationCodeMapper insVerificationCodeMapper;
    
    /**
     * 保存
     * @param vo
     * @return
     */
    public Key save(InsVerificationCodeVo vo) {
        InsVerificationCode search = new InsVerificationCode();
        search.setType(vo.getType());
        search.setDate(vo.getDate());
        search.setJid(vo.getJid());
        search = insVerificationCodeMapper.selectOne(search);
        Date _now = new Date();
        Long id = null;
        if (search == null || search == null) { // 新增
            InsVerificationCode record = new InsVerificationCode();
            BeanUtils.copyProperties(vo, record);
            record.setCreatedAt(_now);
            record.setUpdatedAt(_now);
            insVerificationCodeMapper.insert(record);
            id = record.getId();
        } else { // 更新
            search.setCode(vo.getCode());
            search.setUpdatedAt(_now);
            insVerificationCodeMapper.updateByPrimaryKeySelective(search);
            id = search.getId();
        }
        return new Key(id, "");
    }
    
    public InsVerificationCodeVo selectOne(InsVerificationCodeVo vo) {
        InsVerificationCode record = new InsVerificationCode();
        BeanUtils.copyProperties(vo, record);
        record = insVerificationCodeMapper.selectOne(record);
        if (record == null) {
            return null;
        }
        BeanUtils.copyProperties(record, vo);
        return vo;
    }
}   
