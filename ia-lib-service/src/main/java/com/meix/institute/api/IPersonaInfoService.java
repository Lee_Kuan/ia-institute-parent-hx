package com.meix.institute.api;

import com.meix.institute.vo.log.InsPersonaUuidInfo;

/**
 * 用户画像接口
 * Created by zenghao on 2019/10/12.
 */
public interface IPersonaInfoService {

	void sendPersonaUuid2Meix(InsPersonaUuidInfo insPersonaUuidInfo);

}
