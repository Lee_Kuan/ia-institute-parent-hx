package com.meix.institute.api;

import java.util.List;

import lombok.Data;

@Data
public class WechatArticleAction {
    
    private String action;
    private Object data;
    private List<String> scope;
    private String type;
    
}
