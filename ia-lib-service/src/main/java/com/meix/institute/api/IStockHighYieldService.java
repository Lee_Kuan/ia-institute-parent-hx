package com.meix.institute.api;

import com.meix.institute.vo.HoneyBeeBaseVo;

import java.util.List;
import java.util.Set;

/**
 * 股票日收益超过3%的数据推送到首页
 * Created by zenghao on 2019/7/3.
 */
public interface IStockHighYieldService {

	void refreshCache();

	List<HoneyBeeBaseVo> getList(long uid, Set<Integer> selfstockSet);

}
