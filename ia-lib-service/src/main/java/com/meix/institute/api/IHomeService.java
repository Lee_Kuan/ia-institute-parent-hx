package com.meix.institute.api;

import com.meix.institute.entity.InsBanner;
import com.meix.institute.response.HoneybeeMsgInfo;
import com.meix.institute.vo.banner.InsBannerVo;
import net.sf.json.JSONArray;

import java.util.List;

/**
 * 首页数据
 * Created by zenghao on 2019/10/8.
 */
public interface IHomeService {

	HoneybeeMsgInfo getHotRecommendList(long uid, int currentPage, int showNum, int issueType);

	HoneybeeMsgInfo getMainPageWaterFlowList(long uid, int currentPage, int showNum, String lastId, int issueType);

	List<InsBannerVo> getBannerList(int applyModule, int bannerType, int currentPage, int showNum);

	/**
	 * 新增banner
	 *
	 * @param banner
	 */
	public void saveBanner(InsBanner banner);

	/**
	 * 更新banner排序
	 *
	 * @param uuid
	 * @param ids  [id1,id2,...]
	 */
	public void saveBannerSort(String uuid, JSONArray ids);

	/**
	 * 获取banner列表（后台管理）
	 */
	public List<InsBannerVo> getBannerList();

	/**
	 * 删除banner
	 *
	 * @param id
	 */
	public void deleteBanner(long id);
}
