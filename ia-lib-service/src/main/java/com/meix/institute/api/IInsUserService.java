package com.meix.institute.api;

import com.meix.institute.core.Key;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsUser;
import com.meix.institute.pojo.InsUserInfo;
import com.meix.institute.pojo.InsUserSearch;
import com.meix.institute.vo.user.InsUserLabelVo;
import com.meix.institute.vo.user.UserInfo;

import net.sf.json.JSONArray;

import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/8/1.
 */
public interface IInsUserService {

	/**
	 * 获取用户信息
	 *
	 * @param uid
	 * @return
	 */
	InsUser getUserInfo(long uid);

	InsUser getUserInfo(String uuid);

	/**
	 * 更新用户状态
	 *
	 * @param uid
	 * @param userStatus
	 * @param optUser
	 */
	void updateUserStatus(long uid, int userStatus, String optUser);

	/**
	 * 检验重复
	 *
	 * @param search
	 * @return
	 */
	int countByDuplicate(InsUserSearch search);

	/**
	 * 保存用户信息
	 * <p>ID 为空添加， 非空  更新</p>
	 *
	 * @param insUserInfo
	 */
	Key save(InsUserInfo insUserInfo, String user);

	/**
	 * 重置密码
	 *
	 * @param uid
	 * @param password 新密码
	 * @param user
	 */
	void resetPwd(long uid, String password, String user);

	/**
	 * 根据主键获取用户信息
	 *
	 * @param uid
	 * @return
	 */
	InsUserInfo selectByPrimaryKey(long uid);

	/**
	 * 获取内部员工列表
	 *
	 * @param params
	 * @return
	 */
	Paged<Map<String, Object>> getContactList(Map<String, Object> params);

	/**
	 * 根据手机号或邮件获取用户信息
	 *
	 * @param mobileOrEmail
	 * @return
	 * @@param userState 用户在职状态 0-在职，1-离职
	 */
	InsUserInfo getUserByMobileOrEmail(String mobileOrEmail, Integer userState);

	/**
	 * 获取待审核用户列表
	 *
	 * @param search
	 * @param user
	 * @return
	 */
	Paged<Map<String, Object>> getNotAuditCustomerList(InsUserSearch search, String user);

	
	public List<UserInfo> getCustomerList(int currentPage, int showNum, String condition);


	/**
	 * 名片审核
	 *
	 * @param userInfo
	 * @param user
	 */
	void auditCustomer(UserInfo userInfo, String user);

	/**
	 * 用户画像列表
	 *
	 * @param search
	 * @param user
	 * @return
	 */
	Paged<Map<String, Object>> getUserLableList(Map<String, Object> search, String user);

	/**
	 * 添加用户标签
	 *
	 * @param data
	 * @param user
	 */
	void addUserLable(Map<String, Object> data, String user);

	/**
	 * 删除用户标签
	 *
	 * @param labelId
	 * @param user
	 */
	void deleteUserLable(Long labelId, String user);

	/**
	 * 获取用户画像标签
	 *
	 * @param contactId
	 * @param user
	 * @return
	 */
	List<InsUserLabelVo> getUserLabelByContactId(long contactId, String user);

	/**
	 * 获取研究所用户信息
	 *
	 * @param contactId
	 * @param user
	 * @return
	 */
	UserInfo selectCustomerInfoById(long contactId, String user);

	/**
	 * 获取用户最关注的标签
	 *
	 * @param user
	 * @return
	 */
	List<Map<String, Object>> getUserFocusLabel(String user);

	/**
	 * 获取某个用户对某个股票的画像分
	 * @param uid
	 * @param innerCode
	 * @param industryCode
	 * @return
	 */
	float getUserLabelTotalScore(long uid, int innerCode, int industryCode);

	List<String> saveInsUser(String uuid, long companyCode, JSONArray datas);

	public void updateUserRole(long uid, int role, String user);

	/**
	 * 用户离职，内部账号封禁
	 * @param user
	 * @param uuid
	 */
	public void deleteUser(String user, String uuid);

}
