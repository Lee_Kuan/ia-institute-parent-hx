package com.meix.institute.api;

import java.io.InputStream;

import com.meix.institute.core.Filepath;
import com.meix.institute.core.Mime;

/**
 * 附件服务接口(这个接口不能进行远程调用)
 * 1. 集成FsService
 * 2. 集成ContentService
 * 
 * @author xueyc
 */
public interface MimeService {

    /**
     * 保存文件到文件系统
     * @param filename 文件名
     * @param is 文件流
     * @return 文件路径
     */
    Filepath write(Mime mime, InputStream is, String user);
    
}
