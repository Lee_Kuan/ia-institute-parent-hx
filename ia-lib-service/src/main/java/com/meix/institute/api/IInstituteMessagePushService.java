package com.meix.institute.api;

import java.util.Date;
import java.util.List;

import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.company.CompanyWechatConfig;
import com.meix.institute.vo.user.InsSaler;
import com.meix.institute.vo.user.UserInfo;

/**
 * Created by zenghao on 2019/9/27.
 */
public interface IInstituteMessagePushService {

	void sendPermReqNotice(String appType, long uid, int dataType, long dataId);

	void sendNotice2Saler(InsSaler insSaler, CompanyInfoVo companyInfoCache, CompanyWechatConfig wechatConfig,
	                      String smsContent, String wxContent, String wxTitle, String emailSubject);

	InsSaler getInsSaler(long companyCode, String userMobile, long uid);

	void instituteMessagePush(String appId, int dataType, long dataId, int messageType, String messageInfo,
	                          String remark, String title, int flag, String userName, String messageFrom, long companyCode,
	                          List<Long> secuMain, List<Long> analyst, long uid, String mobile);

	void messagePush(long companyCode, long uid, int messageType, String title, int dataType, int clueType,
	                 String mobile);

	void undoMessagePush(int dataType, long dataId, String reason, long companyCode);

	/**
	 * 成为白名单通知
	 *
	 * @param companyCode
	 * @param userInfo
	 * @param expireDate
	 */
	void sendBecomeWlNotice(long companyCode, UserInfo userInfo, Date expireDate);

	/**
	 * 用户审核结果通知
	 *
	 * @param companyCode
	 * @param uid
	 */
	void sendCardAuthNotice(long companyCode, long uid, int auditResult);

	/**
	 * 
	 * @param id 配置id
	 * @param messageType 0-短信推送，1-邮件推送
	 * @param receiverType 接收人类型，2-白名单，3-潜在用户，4-游客
	 * @param comment 手机号/邮箱 以英文","隔开
	 * @param content 
	 */
	public void pushUserConfigMessage(long id, int messageType, int receiverType, String comment, String title, String content);

	public void pushActivityAuditRes(int auditRes, String title, String reason, String date, String uuid);

	/**
	 * 成为白名单推送
	 * @param mobiles
	 */
	public void pushBecomeWhiteList(List<String> mobiles);
}
