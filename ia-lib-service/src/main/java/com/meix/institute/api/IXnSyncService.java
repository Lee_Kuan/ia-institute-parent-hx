package com.meix.institute.api;

import com.meix.institute.core.BaseResp;
import com.meix.institute.entity.InsSyncTableJsid;


/**
 * 贝格 同步接口
 *
 * @author xueyc
 * @author zenghao
 * @author likuan
 * @since 2020-4-3
 */
public interface IXnSyncService {
    
    /**
     * 贝格同步action
     * 
     * @param syncRecord 同步参数
     * @param syncVia 同步方式： 1 系统自动 2 手动触发
     * @param isSyncAll 是否全量同步 ： true 全量  false 非全量
     * @param user 操作用户
     * @return
     */
    BaseResp action(InsSyncTableJsid syncRecord, Integer syncVia, Boolean isSyncAll, String user);
    
}
