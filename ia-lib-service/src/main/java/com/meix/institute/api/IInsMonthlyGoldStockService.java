package com.meix.institute.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.meix.institute.entity.InsCompanyMonthlyGoldStock;
import com.meix.institute.search.StockYieldSearchVo;
import com.meix.institute.vo.stock.GoldStockSelfUserInfo;
import com.meix.institute.vo.stock.MonthlyGoldStockVo;

import net.sf.json.JSONArray;

public interface IInsMonthlyGoldStockService {


	/**
	 * 批量导入月度金股
	 *
	 * @param stockList
	 */
	public void addGoldStock(List<InsCompanyMonthlyGoldStock> stockList);

	/**
	 * 检查此股票是否已存在
	 *
	 * @param innerCode
	 * @param month
	 * @return innerCode
	 */
	public long getStockInnerCode(String stockNo, String month, long companyCode);

	/**
	 * 获取月度金股统计列表
	 *
	 * @param showNum
	 * @param currentPage
	 * @param companyCode
	 * @return
	 */
	public Map<String, Object> getGoldStockList(int showNum, int currentPage, long companyCode);

	public int getGoldStockCount(long companyCode);
	/**
	 * 删除月度金股推荐
	 *
	 * @param month
	 * @param innerCode
	 * @param companyCode
	 */
	public void deleteGoldStock(String month, long innerCode, long companyCode);

	/**
	 * 获取月度金股此月的详细列表
	 *
	 * @param showNum
	 * @param currentPage
	 * @param companyCode
	 * @param month
	 * @param homeShowFlag
	 * @return
	 */
	public List<MonthlyGoldStockVo> getGoldStockMonthList(int showNum, int currentPage, long companyCode,
	                                                       String month, int homeShowFlag);
	
	public int getGoldStockMonthCount(long companyCode, String month, int homeShowFlag);
	
	/**
	 * 保存月度金股信息
	 */
	public List<String> saveGoldStockMonth(String uuid, long companyCode, String month, JSONArray datas);

	public List<GoldStockSelfUserInfo> getSelfStockUserList(int showNum, int currentPage, long innerCode);

	public int getSelfStockUserCount(long innerCode);

	public void updateGoldStockPrice(InsCompanyMonthlyGoldStock goldStock);
	
	/**
	 * 月度金股上下架操作
	 * @param id 金股唯一ID
	 * @param homeShowFlag 首页显示标志
	 * @param user
	 */
    public void goldPullOffShelves(Long id, Integer homeShowFlag, String user);

	public List<String> saveGoldStockMonth(String uuid, long companyCode, String month, JSONArray datas,
			XSSFSheet sheet);

	public Map<Integer, Map<String, BigDecimal>> getStockPriceMap(List<StockYieldSearchVo> searchList);

}
