package com.meix.institute.api;

import java.util.List;

import com.meix.institute.vo.company.CompanyClueInfo;
import com.meix.institute.vo.company.CompanyWechatConfig;
import com.meix.institute.vo.user.InsCustomer;
import com.meix.institute.vo.user.InsMeixUser;

/**
 * Created by zenghao on 2019/9/4.
 */
public interface IInstituteService {

	int saveInsMeixUser(InsMeixUser insMeixUser);

	int updateInsMeixUser(InsMeixUser insMeixUser);

	InsMeixUser getExistInsMeixUser(long companyCode, long customerCompanyCode);

	List<CompanyWechatConfig> getWechatConfig();

	CompanyWechatConfig getWechatConfigByAppType(String appType);

	CompanyWechatConfig getWechatConfigByAppId(String appId);

	CompanyWechatConfig getSPConfigByCompanyCode(long companyCode);

	/**
	 * 新商机推送
	 *
	 * @param companyClueInfo
	 */
	void sendNewClueNotice(CompanyClueInfo companyClueInfo);

	public List<InsCustomer> getCustomersBySaler(long salerUid, int currentPage, int showNum);

	public int getCustomerCountBySaler(long salerUid);

    CompanyWechatConfig getWechatConfigByAppTypeAndClientType(String appType, Integer clientType);

}
