package com.meix.institute.api;

import com.meix.institute.vo.third.HandleResult;
import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2020/4/8.
 */
public interface DataHandler {

	/**
	 * 处理同步操作的数据
	 *
	 * @param jsonObject
	 * @return 返回null时表示同步成功
	 * @throws Exception
	 */
	HandleResult handleData(String action, JSONObject jsonObject) throws Exception;

}
