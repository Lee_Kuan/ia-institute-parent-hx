package com.meix.institute.api;

import com.meix.institute.vo.IaConstantVo;

import java.util.List;

/**
 * Created by zenghao on 2019/9/21.
 */
public interface IConstantService {

	void saveConstantList(List<IaConstantVo> list);

	int removeConstantRange(long startId, long endId);
}
