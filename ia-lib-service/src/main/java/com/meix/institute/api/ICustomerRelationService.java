package com.meix.institute.api;

import com.meix.institute.entity.CustomerRelation;

/**
 * 用户唯一id表，用于同步画像信息
 * Created by zenghao on 2019/11/26.
 */
public interface ICustomerRelationService {

	CustomerRelation saveCustomerRelation(long companyCode, String mobile);

	CustomerRelation getCustomerRelationByMobile(String mobile);

	CustomerRelation getCustomerRelationByUuid(String uuid);

}
