package com.meix.institute.api;

public interface ITaskService {
    
    /**
     * 数据中心——热度 接口
     */
    void executeHeatData();
    
    /**
     * 清空热度数据
     */
    void clearHeadData();
    
    /**
     * copy 研究所热度数据
     */
    void copyInstituteHeatData();
}
