package com.meix.institute.api;

import com.meix.institute.entity.RecommenedInfoLog;

/**
 * Created by zenghao on 2019/7/24.
 */
public interface IRecommendInfoLogService {

	void saveRecommendInfoLog(RecommenedInfoLog recommenedInfoLog);

	RecommenedInfoLog findRecommendInfoLog(long id);
}
