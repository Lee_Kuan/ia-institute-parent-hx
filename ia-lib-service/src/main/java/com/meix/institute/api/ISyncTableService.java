package com.meix.institute.api;

import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsSyncTableJsid;
import com.meix.institute.pojo.InsSyncTableJsidInfo;
import com.meix.institute.pojo.InsSyncTableJsidSearch;



/**
 * 同步信息记录
 * Created by zenghao on 2020/4/5.
 */
public interface ISyncTableService {
    
    /**
     * 获取最新同步数据
     * 
     * @param tableName 同步类型
     * @param user 内部用户
     * @return
     */
    InsSyncTableJsid getLastSyncTableRecord(String tableName, String user);

	/**
	 * 重试
	 * 
	 * @param id 数据id
	 * @param user 操作人
	 */
    void retry(Long id, String user);
    
    /**
     * 关闭
     * @param id 数据id
     * @param remarks 关闭备注
     * @param user 操作人
     */
    void close(Long id, String remarks, String user);
    
    /**
     * 获取同步管理列表
     * 
     * @param search
     * @return
     */
    Paged<InsSyncTableJsidInfo> paged(InsSyncTableJsidSearch search);

}
