package com.meix.institute.api;

import com.meix.institute.entity.InsSelfStock;
import com.meix.institute.vo.stock.StockVo;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by zenghao on 2019/9/23.
 */
public interface ISelfStockService {
	List<StockVo> getUserSelfStockList(Map<String, Object> params);

	public void addSelfStock(int innerCode, long uid);

	public void deleteSelfStock(int innerCode, long uid, String uuid);

	/**
	 * 查询自选股列表
	 * @param uid :-1时查询所有用户
	 * @param offset
	 * @param showNum :0时不进行分页
	 * @return
	 */
	public List<InsSelfStock> getSelfStockList(long uid, int offset, int showNum);

	InsSelfStock getSelfStock(long uid, int innerCode);

	Set<Integer> getSelfStockList(long uid);

	/**
	 * 修改自选股数据
	 *
	 * @param innerCode
	 * @param uid
	 * @param isDeleted 0添加 1删除
	 * @return
	 */
	int updateSelfStock(int innerCode, long uid, int isDeleted);

	int checkStockIsExist(long uid, int innerCode);

}
