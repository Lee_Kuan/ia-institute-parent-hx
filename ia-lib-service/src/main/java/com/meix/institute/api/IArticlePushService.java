package com.meix.institute.api;

import java.util.Date;
import java.util.List;

import com.meix.institute.core.BaseResp;
import com.meix.institute.core.DataResp;
import com.meix.institute.core.Key;
import com.meix.institute.core.Paged;
import com.meix.institute.pojo.InsArticleGroupInfo;
import com.meix.institute.pojo.InsArticleGroupPreviewInfo;
import com.meix.institute.pojo.InsArticleGroupSearch;
import com.meix.institute.pojo.InsArticleInfo;
import com.meix.institute.pojo.InsArticleOtherInfo;
import com.meix.institute.pojo.InsArticleSearch;

/**
 * 订阅号文章推送服务
 * 
 * @author xueyc
 * @since 2020年4月15日
 *
 */
public interface IArticlePushService {
    
    /**
     * 订阅号文章列表
     * 
     * @param InsArticleSearch
     * @param user
     * @return Paged<InsArticlePushInfo>
     */
    Paged<InsArticleInfo> paged(InsArticleSearch search, String user);
    
    /**
     * 保存
     * 
     * @param InsArticleInfo
     * @param user
     */
    Key save(InsArticleInfo info, String user);
    
    /**
     * 删除
     * 
     * @param InsArticleInfo
     * @param user
     */
    void delete(Long id, String user);
    
    /**
     * 订阅号文章详情
     * 
     * @param id
     * @param user
     * @return
     */
    InsArticleInfo getArticleDetails(Long id, String user);
    
    /**
     * 文章群发列表
     * 
     * @param search
     * @param user
     * @return
     */
    Paged<InsArticleGroupInfo> groupPage(InsArticleSearch search, String user);
    
    /**
     * 文章群发详情
     * 
     * @param id
     * @param loadContent 是否加载content
     * @param user
     * @return
     */
    InsArticleGroupInfo getArticleGroupDetails(Long id, Boolean loadContent, String user);
    
    /**
     * 文章群发删除
     * 
     * @param id
     * @param user
     */
    void deleteGroup(Long id, String user);
    
    /**
     * 群发推送
     * 
     * @param id
     * @param pulishTime
     * @param user
     * @return
     */
    DataResp groupPublish(Long id, String mediaId, Date pulishTime, String user);
    
    /**
     * 文章群发保存
     * 
     * @param data
     * @param user
     * @return 
     */
    Key groupSave(InsArticleGroupInfo info, String user);
    
    /**
     * 预览
     * 
     * @param data
     * @param type
     * @param accounts
     * @param user
     * @return
     */
    DataResp articlePreview(String data, String type, List<String> accounts, String user);
    
    /**
     * 群发下架
     * 
     * @param id
     * @param user
     * @return
     */
    BaseResp groupUndo(Long id, String user);
    
    /**
     * 保存订阅号其它素材（非图文）
     * 
     * @param otherInfo
     * @param user
     * @return
     */
    Key saveOther(InsArticleOtherInfo info, String user);
    
    /**
     * 群发素材添加
     * 
     * @param id
     * @param user
     * @return
     */
    DataResp addGroupMaterial(Long id, String user);
    
    /**
     * 预览素材添加
     * @param articleIds
     * @return
     */
    DataResp addGroupPreviewMaterial(List<Long> articleIds, String user);
    
    /**
     * 群发保存并发布
     * 
     * @param data
     * @param user
     */
    DataResp groupSaveAndPublish(InsArticleGroupInfo data, String user);
    
    /**
     * 群发预览
     * @param info
     * @param user
     * @return
     */
    DataResp groupPreview(InsArticleGroupPreviewInfo info, String user);
    
    /**
     * 群发查询
     * 
     * @param search 群发条件
     * @return 群发列表
     */
    List<InsArticleGroupInfo> groupFind(InsArticleGroupSearch search);
}
