package com.meix.institute.api;


import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.stock.StockVo;
import com.meix.institute.vo.user.InfluenceAnalyseVo;
import com.meix.institute.vo.user.InfluenceTrend;

import java.util.List;
import java.util.Map;

/**
 * @Description:公司设置
 */
public interface ICompanyService {

	public CompanyInfoVo getCompanyInfoByName(String companyName);

	CompanyInfoVo getCompanyInfoById(long companyCode);

	/**
	 * 查询公司月度金股数据
	 *
	 * @param params
	 * @return
	 */
	List<StockVo> getCompanyMonthlyStockVo(Map<String, Object> params);

	/**
	 * @param vo
	 * @return
	 * @Title: addCompanyInfo、
	 * @Description:保存公司信息
	 * @return: int
	 */
	public Long addCompanyInfo(CompanyInfoVo vo);

	List<InfluenceAnalyseVo> getCompanyInfluenceAnalysis(Map<String, Object> params);

	InfluenceTrend getCompanyInfluenceTrend(Map<String, Object> params);

	public List<CompanyInfoVo> getCompanyList(String condition, int currentPage, int showNum);

}
