package com.meix.institute.api;

/**
 * Created by zenghao on 2020/3/31.
 */
public interface IUserLoginRecordService {

	int saveUserLoginRecord(long uid, int clientType);

}
