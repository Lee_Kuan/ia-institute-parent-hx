package com.meix.institute.api;

import com.meix.institute.vo.HoneyBeeBaseVo;

import java.util.List;
import java.util.Set;

/**
 * 券商月度金股
 * Created by zenghao on 2019/7/3.
 */
public interface ICompanyMonthlyStockService {

	void refreshCache();

	List<HoneyBeeBaseVo> getList(long uid, long companyCode, Set<Integer> selfstockSet);

}
