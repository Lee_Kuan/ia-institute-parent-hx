package com.meix.institute.api;

/**
 * 美市数据同步 job服务接口
 *
 * @author likuan
 * @since 2020年4月15日13:47:52
 */
public interface IMeixSyncJobService {

	public void syncWhiteUserToMeix();

	public void syncReportToMeix(boolean isSyncAll);
    
}
