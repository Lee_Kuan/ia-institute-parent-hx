package com.meix.institute.api;

import com.meix.institute.core.DataResp;

public interface WechatArticleHandler {

    DataResp action(WechatArticleAction action, String user);
}
