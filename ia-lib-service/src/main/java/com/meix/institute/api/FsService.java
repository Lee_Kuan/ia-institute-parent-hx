package com.meix.institute.api;

import java.io.File;
import java.io.InputStream;

/**
 * 文件系统服务接口，存取文件
 * @author xueyc
 */
public interface FsService {

    /**
     * 保存文件到文件系统
     * @param filename 文件名
     * @param is 文件流
     * @return 文件路径
     */
    String write(String filename, InputStream is);
    /**
     * 保存文件到文件系统
     * @param filename 文件名
     * @param bytes 二进制信息
     * @return 文件路径
     */
    String write(String filename, byte[] bytes);

    /**
     * 得到文件内容
     * @param uuid 文件路径
     * @return 文件流
     */
    InputStream getAsInputStream(String filepath);
    /**
     * 得到文件内容
     * @param uuid 文件路径
     * @return 二进制信息
     */
    byte[] getAsInputBytes(String filepath);
    
    /**
     * 下载文件到服务器
     * 
     * @param filename 文件名
     * @param is 文件流
     * @return 文件
     */
    File createTempFile(String filename, InputStream is);
    
    /**
     * 下载文件到服务器
     * 
     * @param filename 文件名
     * @param bytes 二进制信息
     * @return 文件
     */
    File createTempFile(String filename, byte[] bytes);

}
