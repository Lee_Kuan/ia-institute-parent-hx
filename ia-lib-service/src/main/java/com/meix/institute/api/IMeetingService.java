package com.meix.institute.api;


import java.util.List;

import com.meix.institute.entity.MorningMeeting;
import com.meix.institute.search.ComplianceListSearchVo;
import com.meix.institute.vo.meeting.MorningMeetingVo;

import net.sf.json.JSONArray;

/**
 * @author likuan
 * @date 2019年9月23日10:34:14
 * @describe 晨会
 */
public interface IMeetingService {

	/**
	 * @author likuan
	 * @date 2019年9月23日10:34:25
	 * @Description: 保存晨会
	 */
	public long saveMeeting(long id, String uuid, MorningMeeting meeting, JSONArray issueType);

	/**
	 * @param meetingId
	 * @return
	 * @Dscription: 获取晨会详情
	 */
	public MorningMeetingVo getMeetingDetail(long meetingId);

	public List<MorningMeetingVo> getMeetingList(ComplianceListSearchVo searchVo);

	public int getMeetingCount(ComplianceListSearchVo searchVo);

	public void examineMeeting(MorningMeeting meeting);

	/**
	 * 获取权限控制的晨会详情
	 * @param id
	 * @param uid
	 * @return
	 */
	public MorningMeetingVo getMeetingDetail(long id, long uid);

	/**
	 * 获取权限控制的晨会列表
	 * @param uid
	 * @param companyCode
	 * @param showNum
	 * @param currentPage
	 * @param clientType 
	 * @param meetingType 
	 * @return
	 */
	public List<MorningMeetingVo> getMeetingList(long uid, long companyCode, int showNum, int currentPage, int clientType, int meetingType);
}
