package com.meix.institute.api;

import java.util.List;

import com.meix.institute.core.Key;
import com.meix.institute.core.Paged;
import com.meix.institute.pojo.InsSyncRecReqInfo;
import com.meix.institute.pojo.InsSyncRecReqSearch;

public interface IInsSyncRecReqService {
    
    /**
     * 保存同步数据
     * @param info
     * @param user
     * @return
     */
    Key save(InsSyncRecReqInfo info, String user);
    
    /**
     * 根据主键获取同步数据
     * @param id
     * @return
     */
    InsSyncRecReqInfo selectByPrimaryKey(Long id);
    
    /**
     * 查找同步异常请求
     * @return
     */
    List<InsSyncRecReqInfo> findExceptionReq();
    
    /**
     * 接收请求列表
     * @param search
     * @param user
     * @return
     */
    Paged<InsSyncRecReqInfo> paged(InsSyncRecReqSearch search, String user);
    
    /**
     * 关闭
     * @param id
     * @param remarks
     * @param user
     */
	void close(Long id, String remarks, String user);

	public void retry(Long id, String user);
    
}
