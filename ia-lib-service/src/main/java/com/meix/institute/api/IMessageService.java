package com.meix.institute.api;

import com.meix.institute.response.MeetingMessagePo;
import com.meix.institute.vo.message.MeetingMessageVo;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/9/26.
 */
public interface IMessageService {

	/**
	 * @param mmv
	 * @return
	 * @Title: saveMeetingMessage、
	 * @Description:保存会议留言
	 */
	public Map<String, Object> saveMeetingMessage(MeetingMessageVo mmv);

	/**
	 * @param uid
	 * @param activityId
	 * @param speakerId
	 * @param messageId
	 * @param messageFlag
	 * @param showNum
	 * @return
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @Title: getMeetingMessages、
	 * @Description:获取会议留言
	 */
	public List<MeetingMessagePo> getMeetingMessages(long uid, long activityId,
	                                                 long speakerId, long messageId, int messageFlag, long version, int showNum) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException;

	boolean sendEmailWithResult(String sendTo, String subject, String content, boolean html);

	public String sendEmailWithResult(String sendTo, String subject, String content);
}
