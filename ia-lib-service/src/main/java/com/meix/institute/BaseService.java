package com.meix.institute;

import static com.meix.institute.constant.MeixConstants.SHARE_GROUP;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.meix.institute.api.ICompanyService;
import com.meix.institute.api.IDictTagService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.api.IUserService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.impl.IaServiceImpl;
import com.meix.institute.search.StockYieldSearchVo;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.vo.DataShareVo;
import com.meix.institute.vo.HoneyBeeBaseVo;
import com.meix.institute.vo.user.UserInfo;
import com.meix.institute.vo.user.UserRecordStateVo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2019/9/19.
 */
public class BaseService {

	private static Logger log = LoggerFactory.getLogger(BaseService.class);
	@Autowired
	protected IUserService userServiceImpl;
	@Autowired
	protected ICompanyService companySerciveImpl;
	@Autowired
	protected IInsUserService insUserService;
	@Autowired
	protected CommonServerProperties commonServerProperties;
	@Autowired
	protected IaServiceImpl iaServiceImpl;
	@Autowired
	private IDictTagService tagService;
	@Autowired
	private IMeixSyncService meixSyncServiceImpl;

	protected int getAccountType(long uid) {
		UserInfo user = userServiceImpl.getUserInfo(uid, null, null);
		if (user != null) {
			return user.getAccountType();
		}
		return 0;
	}

	/**
	 * @param uid
	 * @param id
	 * @param dataType
	 * @param dataState 记录状态  1阅读  2点赞 3 关注 4提醒
	 * @Title: saveUserRecordState、
	 * @Description:保存用户记录状态
	 * @return: void
	 */
	protected void saveUserRecordState(long uid, long id, int dataType, int dataState, int dataCause, String openId) {
		UserRecordStateVo vo = new UserRecordStateVo();
		vo.setDataId(id);
		vo.setUid(uid);
		vo.setDataState(dataState);
		vo.setDataType(dataType);
		vo.setDataCause(dataCause);
		vo.setOpenId(openId);
		userServiceImpl.saveUserRecordState(vo);
	}

	protected void saveUserRecordState(long uid, long id, int dataType, int dataState, int dataCause) {
		saveUserRecordState(uid, id, dataType, dataState, dataCause, null);
	}

	protected void saveUserReadRecordState(long uid, long id, int dataType) {
		saveUserRecordState(uid, id, dataType, MeixConstants.RECORD_READ, 0, null);
	}

	protected void saveUserReadRecordState(long uid, long id, int dataType, String openId) {
		saveUserRecordState(uid, id, dataType, MeixConstants.RECORD_READ, 0, openId);
	}

	/**
	 * @param uid
	 * @return
	 * @Title: getUserInfo、
	 * @Description:获取用户信息
	 * @return: UserInfoVo
	 */
	protected UserInfo getUserInfo(long uid) {
		if (uid == 0) {
			return null;
		}
		UserInfo info = userServiceImpl.getUserInfo(uid, null, null);
		if (info == null) {
			return null;
		}
		return info;
	}

	/**
	 * @param uid
	 * @return
	 * @Title: getShareType、
	 * @Description:获取分享类型 买方 卖方
	 * @return: int
	 */
	public int getShareType(long uid) {
		UserInfo user = getUserInfo(uid);
		if (user == null) {
			return 0;
		}
		return 0;
	}

	public int getShareType(UserInfo user) {
		if (user == null) {
			return 0;
		}
		return 0;
	}

	public String getMarketAbbr(int secuMarket) {
		switch (secuMarket) {
			case 72:
				return "HK";
			default:
				return "A";
		}
	}

	public boolean isAMarket(String suffix) {
		if (!suffix.contains("HK")) {
			return true;
		} else
			return false;
	}

	public boolean isHKMarket(String suffix) {
		if (suffix.contains("HK")) {
			return true;
		} else
			return false;
	}

	public String getShareName(String scope) {
		return tagService.getText(MeixConstants.SCOPE, scope);
	}

	public String getShareDesc(int share) {
		switch (share) {
			case SHARE_GROUP:
				return "依据权限开放";
		}
		return "内容完全公开";
	}

	public String getTypeName(int type) {
		String typeName;
		switch (type) {
			case MeixConstants.USER_YB:
				typeName = "研报";
				break;
			case 4:
				typeName = "会议纪要";
				break;
			case MeixConstants.CHAT_MESSAGE_DHHY:
				typeName = "电话会议";
				break;
			case MeixConstants.CHAT_MESSAGE_DY:
				typeName = "调研";
				break;
			case MeixConstants.CHAT_MESSAGE_CL:
				typeName = "策略会";
				break;
			case MeixConstants.REPORT_ALBUM:
				typeName = "研报合辑";
				break;
			case MeixConstants.MEETING_ALBUM:
				typeName = "纪要合辑";
				break;
			case MeixConstants.USER_CH:
				typeName = "晨会";
				break;
			default:
				typeName = "其他";
				break;
		}
		return typeName;
	}

	public Map<Integer, BigDecimal> getStockIntervalYield(List<StockYieldSearchVo> list) {
		if (list == null || list.size() == 0) {
			return null;
		}
		try {
			Map<Integer, BigDecimal> result = new HashMap<>();
//			Map<String, Object> params = new HashMap<>();
//			params.put("dataJson", GsonUtil.obj2Json(list));
//			params.put("tempToken", MeixConstants.TEMP_TOKEN);
//			JSONObject data = iaServiceImpl.httpPostByWehcat(params, HttpApi.SYNC_STOCK_INTERVAL_YIELD);
			JSONObject data = meixSyncServiceImpl.reqMeixStockYield(GsonUtil.obj2Json(list));
			if (data == null) {
				return null;
			}
			int messageCode = data.getInt("messageCode");
			if (messageCode != MeixConstantCode.M_1008) {
				String message = data.getString("message");
				log.error(message);
				return null;
			}
			JSONArray jsonArray = data.getJSONArray("data");
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject jobj = jsonArray.getJSONObject(i);
				double yieldRate = jobj.getDouble("yieldRate");
				int innerCode = jobj.getInt("innerCode");
				result.put(innerCode, new BigDecimal(yieldRate));
			}
			return result;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}
	
	public Map<Integer, Map<String, BigDecimal>> getStockPrice(List<StockYieldSearchVo> list) {
		if (list == null || list.size() == 0) {
			return null;
		}
		try {
			Map<Integer, Map<String, BigDecimal>> result = new HashMap<>();
//			Map<String, Object> params = new HashMap<>();
//			params.put("dataJson", GsonUtil.obj2Json(list));
//			params.put("tempToken", MeixConstants.TEMP_TOKEN);
			JSONObject data = meixSyncServiceImpl.reqMeixStockYield(GsonUtil.obj2Json(list));//iaServiceImpl.httpPostByWehcat(params, HttpApi.SYNC_STOCK_INTERVAL_YIELD);
			if (data == null) {
				return null;
			}
			int messageCode = data.getInt("messageCode");
			if (messageCode != MeixConstantCode.M_1008) {
				String message = data.getString("message");
				log.error(message);
				return null;
			}
			JSONArray jsonArray = data.getJSONArray("data");
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject jobj = jsonArray.getJSONObject(i);
				int innerCode = jobj.getInt("innerCode");
				double startPrice = MapUtils.getDoubleValue(jobj, "startPrice", 0);
				double endPrice = MapUtils.getDoubleValue(jobj, "endPrice", 0);
				Map<String, BigDecimal> map = new HashMap<>();
				map.put("startPrice", new BigDecimal(startPrice));
				map.put("endPrice", new BigDecimal(endPrice));
				result.put(innerCode, map);
			}
			return result;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	protected String getSmsContent(String templateContent, String templateParam) {
		if (templateContent == null) {
			return null;
		}

		JSONObject jsonParam = JSONObject.fromObject(templateParam);
		for (Object key : jsonParam.keySet()) {
			templateContent = templateContent.replace("${" + key + "}", jsonParam.getString((String) key));
		}

		return templateContent;
	}

	protected boolean checkCompanyCode(long companyCode, HoneyBeeBaseVo vo) {
		switch (vo.getDataType()) {
			case MeixConstants.USER_YB: {
				Map<String, Object> item = vo.getItem();
				if (companyCode == MapUtils.getLongValue(item, "companyCode", 0)) {
					return true;
				}
				break;
			}
			case MeixConstants.USER_HD: {
				Map<String, Object> item = vo.getItem();
				if (companyCode == MapUtils.getLongValue(item, "orgCode", 0)) {
					return true;
				}
				break;
			}
			case MeixConstants.USER_GP: {
				if (vo.getWaterflowRecType() == 1) {
					return true;
				} else if (vo.getWaterflowRecType() == 2) {
					Map<String, Object> item = vo.getItem();
					if (companyCode == MapUtils.getLongValue(item, "companyCode", 0)) {
						return true;
					}
				}
				break;
			}
			default:
				return false;
		}
		return false;
	}

	protected List<Integer> getReceiverTypeList(Integer receiverType) {
		List<Integer> resList = new ArrayList<>();
		int i = 1;
		while (receiverType > 0) {
			if (receiverType % 2 == 1) {
				resList.add(i);
			}
			receiverType = receiverType / 2;
			i *= 2;
		}
		return resList;
	}
	
	private static final String SHARE_PUBLIC_NAME = "公开";
	private static final String SHARE_DEFAULT_NAME = "默认可见分组人员";
	private static final String SHARE_GROUP_NAME = "指定分组人员";
	private static final String SHARE_INNER_NAME = "全部内部用户";
	private static final String SHARE_WL_NAME = "全部白名单用户";
	private static final String SHARE_NORMAL_NAME = "全部已认证用户";
	private static final String SHARE_TOURIST_NAME = "全部游客";
	
	protected List<DataShareVo> getShareNameCollection() {
		List<DataShareVo> list = new ArrayList<>();
		list.add(getShareVo(MeixConstants.SHARE_DEFAULT, 0));
		list.add(getShareVo(MeixConstants.SHARE_GROUP, 0));
		list.add(getShareVo(MeixConstants.SHARE_ACCOUNTTYPE, MeixConstants.ACCOUNT_INNER));
		list.add(getShareVo(MeixConstants.SHARE_ACCOUNTTYPE, MeixConstants.ACCOUNT_WL));
		list.add(getShareVo(MeixConstants.SHARE_ACCOUNTTYPE, MeixConstants.ACCOUNT_NORMAL));
		list.add(getShareVo(MeixConstants.SHARE_ACCOUNTTYPE, MeixConstants.ACCOUNT_TOURIST));
		return list;
	}
	
	private DataShareVo getShareVo(int share, int dm) {
		DataShareVo shareVo = new DataShareVo();
		shareVo.setName(getShareName(share, dm));
		shareVo.setShare(share);
		shareVo.setDm(dm);
		shareVo.setLocation(getShareId(share, dm));
		shareVo.setIsSelect(0);
		return shareVo;
	}
	//唯一id，手动生成，不允许修改（前端需要认识）
	protected int getShareId(int share, int dm) {
		return (share + 2) * 10000 + dm;
	}
	protected String getShareName(int share, int dm) {
		if (share == MeixConstants.SHARE_PUBLIC) {
			return SHARE_PUBLIC_NAME;
		}
		if (share == MeixConstants.SHARE_DEFAULT) {
			return SHARE_DEFAULT_NAME;
		}
		if (share == MeixConstants.SHARE_GROUP) {
			return SHARE_GROUP_NAME;
		}
		if (share == MeixConstants.SHARE_ACCOUNTTYPE) {
			if (dm == MeixConstants.ACCOUNT_INNER) {
				return SHARE_INNER_NAME;
			}
			if (dm == MeixConstants.ACCOUNT_WL) {
				return SHARE_WL_NAME;
			}
			if (dm == MeixConstants.ACCOUNT_NORMAL) {
				return SHARE_NORMAL_NAME;
			}
			if (dm == MeixConstants.ACCOUNT_TOURIST) {
				return SHARE_TOURIST_NAME;
			}
		}
		return "未知";
	}
	
	protected void saveUserOperatorLog(String user, long dataId, int dataType, int operatorType, int operatorSubType, String clientstr) {
		if (StringUtils.isBlank(user) || dataType == 0 || operatorType == 0) {
			return;
		}
		Map<String, Object> params = new HashMap<>();
		params.put("user", user);
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		params.put("operator", operatorType);
		params.put("operatorSub", operatorSubType);
		params.put("params", clientstr);
		userServiceImpl.saveUserOperatorLog(params);
	}
}
