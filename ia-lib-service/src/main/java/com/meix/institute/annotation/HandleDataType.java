package com.meix.institute.annotation;

import java.lang.annotation.*;

/**
 * 同步处理数据类型
 * Created by zenghao on 2020/4/12.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface HandleDataType {

	int value();

}
