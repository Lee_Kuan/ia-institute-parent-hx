package com.meix.institute;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Created by zenghao on 2019/7/30.
 */
@Service
@EnableScheduling
public class TaskSchedule {

	/**
	 * 每天删除用户密码错误缓存
	 */
	@Scheduled(cron = "0 0 0 * * ?")
	public void clearErrPwdCount() {
	}

}
