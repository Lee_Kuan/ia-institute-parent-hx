package com.meix.institute.thread;

import com.meix.institute.api.IActivityService;
import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.api.IReportService;
import com.meix.institute.api.IUserDao;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.constant.SyncConstants;
import com.meix.institute.enums.UserAction;
import com.meix.institute.mapper.SecuMapper;
import com.meix.institute.util.ApplicationUtil;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.vo.log.InsPersonaInfo;
import com.meix.institute.vo.user.UserInfo;
import com.meix.institute.vo.user.UserRecordStateVo;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class SendPersonaInfoToMeixThread implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(SendPersonaInfoToMeixThread.class);
	private static IMeixSyncService meixSyncService;
	private static IUserDao userDaoImpl;
	private static IReportService reportService;
	private static IActivityService activityService;
	private static SecuMapper secuMapper;

	static {
		meixSyncService = ApplicationUtil.getBean(IMeixSyncService.class);
		userDaoImpl = ApplicationUtil.getBean(IUserDao.class);
		reportService = ApplicationUtil.getBean(IReportService.class);
		activityService = ApplicationUtil.getBean(IActivityService.class);
		secuMapper = ApplicationUtil.getBean(SecuMapper.class);
	}

	private List<UserRecordStateVo> voList;

	public SendPersonaInfoToMeixThread(List<UserRecordStateVo> voList) {
		this.voList = voList;
	}

	@Override
	public void run() {
		if (voList == null || voList.size() == 0) {
			return;
		}

		for (UserRecordStateVo vo : voList) {
			if (vo == null || vo.getUid() == 0) {
				return;
			}

			// 1 阅读  2 点赞 3关注 4提醒 5 点击 7搜索 8分享  9阅读时长 12 加自选 13 取消自选  14 报名/预约 活动
			int dataState = vo.getDataState();
			// 3研报 4用户 5 股票  8 活动
			int dataType = vo.getDataType();

			int to_userAction = vo.getDataState();
			long duration = vo.getDuration();

			switch (dataState) {
				case MeixConstants.RECORD_READ: {
					//阅读
					switch (dataType) {
						case MeixConstants.USER_YB:
						case MeixConstants.USER_HD:
						case MeixConstants.USER_YH:
							duration = 1;
							break;
						default:
							return;
					}
					break;
				}
				case MeixConstants.RECORD_DURATION: {
					//阅读时长
					switch (dataType) {
						case MeixConstants.USER_YB:
						case MeixConstants.USER_HD:
							break;
						default:
							return;
					}
					break;
				}
				case MeixConstants.RECORD_SHARE: {
					//分享
					switch (dataType) {
						case MeixConstants.USER_YB:
						case MeixConstants.USER_HD:
							to_userAction = UserAction.SHARE.getCode();
							break;
						default:
							return;
					}
					break;
				}
				case MeixConstants.RECORD_FOLLOW: {
					//关注
					switch (dataType) {
						case MeixConstants.USER_YH:
							if (vo.getRet() > 0) {
								to_userAction = UserAction.FOCUS.getCode();
							} else if (vo.getRet() == 0) {
								to_userAction = UserAction.UNFOCUS.getCode();
							} else {
								return;
							}
							break;
						default:
							return;
					}
					break;
				}
				case MeixConstants.RECORD_SEARCH: {
					//搜索
					break;
				}
				case MeixConstants.RECORD_ADD_SELF_STOCK: {
					//加自选
					break;
				}
				case MeixConstants.RECORD_DEL_SELF_STOCK: {
					//取消自选
					break;
				}
				case MeixConstants.RECORD_JOIN_ACTIVITY: {
					//参加活动
					to_userAction = UserAction.BOOK.getCode();
					break;
				}
				default:
					return;
			}

			try {
				UserInfo userInfo = userDaoImpl.getUserInfo(vo.getUid(), null, null);
				if (userInfo == null) {
					return;
				}

				InsPersonaInfo insPersonaInfo = new InsPersonaInfo();
				insPersonaInfo.setChannel(0);
				insPersonaInfo.setUserAction(to_userAction);
				insPersonaInfo.setUserName(userInfo.getUserName());
				insPersonaInfo.setMobile(userInfo.getMobile());
				insPersonaInfo.setUserCompanyName(userInfo.getCompanyName());
				insPersonaInfo.setDataId(vo.getDataId());
				insPersonaInfo.setDataType(vo.getDataType());
				insPersonaInfo.setReadDuration(duration);
				insPersonaInfo.setCreateTime(System.currentTimeMillis());
				if (MeixConstants.USER_YB == vo.getDataType()) {
					Map<String, Object> map = reportService.getReportInnerCodeAndIndustry(vo.getDataId());
					String mobile = MapUtils.getString(map, "mobile");
					if (StringUtil.isNotEmpty(mobile)) {
						List<InsPersonaInfo.AuthorInfo> authorList = new ArrayList<>();
						String[] mobileArr = mobile.split(",");
						for (String account : mobileArr) {
							UserInfo author = userDaoImpl.getUserInfo(0, null, account);
							if (account == null) {
								continue;
							}
							InsPersonaInfo.AuthorInfo authorInfo = new InsPersonaInfo.AuthorInfo();
							authorInfo.setAuthorName(author.getUserName());
							authorInfo.setAuthorMobile(author.getMobile());
							authorInfo.setAuthorCompanyName(author.getCompanyName());
							authorList.add(authorInfo);
						}
						insPersonaInfo.setAuthorListJson(GsonUtil.obj2Json(authorList));
					}
					insPersonaInfo.setInnerCode(MapUtils.getString(map, "innerCode"));
					insPersonaInfo.setCompsIndustryCode(MapUtils.getString(map, "labelId"));
				} else if (MeixConstants.USER_HD == vo.getDataType()) {
					Map<String, Object> map = activityService.getActivityInnerCodeAndIndustry(vo.getDataId());
					String mobile = MapUtils.getString(map, "mobile");
					if (StringUtil.isNotEmpty(mobile)) {
						List<InsPersonaInfo.AuthorInfo> authorList = new ArrayList<>();
						String[] mobileArr = mobile.split(",");
						for (String account : mobileArr) {
							UserInfo author = userDaoImpl.getUserInfo(0, null, account);
							if (account == null) {
								continue;
							}
							InsPersonaInfo.AuthorInfo authorInfo = new InsPersonaInfo.AuthorInfo();
							authorInfo.setAuthorName(author.getUserName());
							authorInfo.setAuthorMobile(author.getMobile());
							authorInfo.setAuthorCompanyName(author.getCompanyName());
							authorList.add(authorInfo);
						}
						insPersonaInfo.setAuthorListJson(GsonUtil.obj2Json(authorList));
					}
					insPersonaInfo.setInnerCode(MapUtils.getString(map, "innerCode"));
					insPersonaInfo.setCompsIndustryCode(MapUtils.getString(map, "labelId"));
				} else if (MeixConstants.USER_GP == vo.getDataType()) {
					String labelId = secuMapper.getCompsIndustryByInnerCode((int) vo.getDataId());
					insPersonaInfo.setInnerCode(String.valueOf(vo.getDataId()));
					insPersonaInfo.setCompsIndustryCode(labelId);
				} else if (MeixConstants.USER_YH == vo.getDataType()) {
					UserInfo author = userDaoImpl.getUserInfo(vo.getDataId(), null, null);
					if (author != null) {
						InsPersonaInfo.AuthorInfo authorInfo = new InsPersonaInfo.AuthorInfo();
						authorInfo.setAuthorName(author.getUserName());
						authorInfo.setAuthorMobile(author.getMobile());
						authorInfo.setAuthorCompanyName(author.getCompanyName());
						insPersonaInfo.setAuthorListJson(GsonUtil.obj2Json(Collections.singletonList(authorInfo)));
					}
				} else {
					//其他类型不处理
					return;
				}
				Map<String, Object> params = new HashMap<>();
				params.put("dataJson", GsonUtil.obj2Json(Collections.singletonList(insPersonaInfo)));
				meixSyncService.reqMeixSync(MeixConstants.USER_YH, SyncConstants.ACTION_SAVE_PERSONA_INFO, params);
			} catch (Exception e) {
				log.error("", e);
			}
		}
	}

}
