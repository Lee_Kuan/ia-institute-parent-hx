package com.meix.institute.thread;

import com.meix.institute.api.*;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.entity.CustomerRelation;
import com.meix.institute.enums.UserAction;
import com.meix.institute.mapper.SecuMapper;
import com.meix.institute.util.ApplicationUtil;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.vo.log.InsPersonaUuidInfo;
import com.meix.institute.vo.user.UserInfo;
import com.meix.institute.vo.user.UserRecordStateVo;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class SendPersonaUuidToMeixThread implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(SendPersonaUuidToMeixThread.class);
	private static IPersonaInfoService personaInfoService;
	private static IUserDao userDaoImpl;
	private static IReportService reportService;
	private static IActivityService activityService;
	private static SecuMapper secuMapper;
	private static ICustomerRelationService customerRelationService;
	private static CommonServerProperties commonServerProperties;

	static {
		personaInfoService = ApplicationUtil.getBean(IPersonaInfoService.class);
		userDaoImpl = ApplicationUtil.getBean(IUserDao.class);
		reportService = ApplicationUtil.getBean(IReportService.class);
		activityService = ApplicationUtil.getBean(IActivityService.class);
		secuMapper = ApplicationUtil.getBean(SecuMapper.class);
		customerRelationService = ApplicationUtil.getBean(ICustomerRelationService.class);
		commonServerProperties = ApplicationUtil.getBean(CommonServerProperties.class);
	}

	private UserRecordStateVo vo;

	public SendPersonaUuidToMeixThread(UserRecordStateVo vo) {
		this.vo = vo;
	}

	@Override
	public void run() {

		if (vo == null || vo.getUid() == 0) {
			return;
		}

		// 1 阅读  2 点赞 3关注 4提醒 5 点击 6 播放 7预约 8分享  9阅读时长 12 加自选 13 取消自选  14 报名/预约 活动
		int dataState = vo.getDataState();
		// 3研报 4用户 5 股票  8 活动
		int dataType = vo.getDataType();

		int to_userAction = vo.getDataState();
		long duration = vo.getDuration();

		if (dataState == 1 && dataType == 3) { // 阅读研报
			duration = 1;
		} else if (dataState == 1 && dataType == 8) { // 阅读活动
			duration = 1;
		} else if (dataState == 9 && dataType == 3) { // 阅读时长研报
		} else if (dataState == 9 && dataType == 8) { // 阅读时长活动
		} else if (dataState == 8 && dataType == 3) { // 分享研报
			to_userAction = UserAction.SHARE.getCode();
		} else if (dataState == 8 && dataType == 8) { // 分享活动
			to_userAction = UserAction.SHARE.getCode();
		} else if (dataState == 3 && dataType == 4 && vo.getRet() > 0) { //关注作者
			to_userAction = UserAction.FOCUS.getCode();
		} else if (dataState == 3 && dataType == 4 && vo.getRet() == 0) { // 取消关注作者
			to_userAction = UserAction.UNFOCUS.getCode();
		} else if (dataState == 12) { // 加自选

		} else if (dataState == 13) { // 取消自选

		} else if (dataState == 14) { // 参加活动
			to_userAction = UserAction.BOOK.getCode();
		} else {
			return;
		}

		try {
			UserInfo userInfo = userDaoImpl.getUserInfo(vo.getUid(), null, null);
			if (userInfo == null) {
				return;
			}

			CustomerRelation userRelation = customerRelationService.getCustomerRelationByMobile(userInfo.getMobile());
			if (userRelation == null) {
				userRelation = customerRelationService.saveCustomerRelation(commonServerProperties.getCompanyCode(), userInfo.getMobile());
			}

			InsPersonaUuidInfo insPersonaUuidInfo = new InsPersonaUuidInfo();
			insPersonaUuidInfo.setChannel(0);
			insPersonaUuidInfo.setUserAction(to_userAction);
			insPersonaUuidInfo.setInsCompanyCode(commonServerProperties.getCompanyCode());
			insPersonaUuidInfo.setUuid(userRelation.getUuid());
			insPersonaUuidInfo.setDataId(vo.getDataId());
			insPersonaUuidInfo.setDataType(vo.getDataType());
			insPersonaUuidInfo.setReadDuration(duration);
			insPersonaUuidInfo.setCreateTime(System.currentTimeMillis());
			if (3 == vo.getDataType()) {
				Map<String, Object> map = reportService.getReportInnerCodeAndIndustry(vo.getDataId());
				String mobile = MapUtils.getString(map, "mobile");
				if (StringUtil.isNotEmpty(mobile)) {
					List<InsPersonaUuidInfo.AuthorInfo> authorList = new ArrayList<>();
					String[] mobileArr = mobile.split(",");
					for (String account : mobileArr) {
						UserInfo author = userDaoImpl.getUserInfo(0, null, account);
						if (account == null) {
							continue;
						}
						CustomerRelation authorRelation = customerRelationService.getCustomerRelationByMobile(author.getMobile());
						if (authorRelation == null) {
							authorRelation = customerRelationService.saveCustomerRelation(commonServerProperties.getCompanyCode(), author.getMobile());
						}
						InsPersonaUuidInfo.AuthorInfo authorInfo = new InsPersonaUuidInfo.AuthorInfo();
						authorInfo.setAuthorUuid(authorRelation.getUuid());
						authorList.add(authorInfo);
					}
					insPersonaUuidInfo.setAuthorListJson(GsonUtil.obj2Json(authorList));
				}
				insPersonaUuidInfo.setInnerCode(MapUtils.getString(map, "innerCode"));
				insPersonaUuidInfo.setCompsIndustryCode(MapUtils.getString(map, "labelId"));
			} else if (8 == vo.getDataType()) {
				Map<String, Object> map = activityService.getActivityInnerCodeAndIndustry(vo.getDataId());
				String mobile = MapUtils.getString(map, "mobile");
				if (StringUtil.isNotEmpty(mobile)) {
					List<InsPersonaUuidInfo.AuthorInfo> authorList = new ArrayList<>();
					String[] mobileArr = mobile.split(",");
					for (String account : mobileArr) {
						UserInfo author = userDaoImpl.getUserInfo(0, null, account);
						if (account == null) {
							continue;
						}
						CustomerRelation authorRelation = customerRelationService.getCustomerRelationByMobile(author.getMobile());
						if (authorRelation == null) {
							authorRelation = customerRelationService.saveCustomerRelation(commonServerProperties.getCompanyCode(), author.getMobile());
						}
						InsPersonaUuidInfo.AuthorInfo authorInfo = new InsPersonaUuidInfo.AuthorInfo();
						authorInfo.setAuthorUuid(authorRelation.getUuid());
						authorList.add(authorInfo);
					}
					insPersonaUuidInfo.setAuthorListJson(GsonUtil.obj2Json(authorList));
				}
				insPersonaUuidInfo.setInnerCode(MapUtils.getString(map, "innerCode"));
				insPersonaUuidInfo.setCompsIndustryCode(MapUtils.getString(map, "labelId"));
			} else if (5 == vo.getDataType()) {
				String labelId = secuMapper.getCompsIndustryByInnerCode((int) vo.getDataId());
				insPersonaUuidInfo.setInnerCode(String.valueOf(vo.getDataId()));
				insPersonaUuidInfo.setCompsIndustryCode(labelId);
			} else if (4 == vo.getDataType()) {
				UserInfo author = userDaoImpl.getPersonalInfo(vo.getDataId(), vo.getDataId());
				if (author != null) {
					CustomerRelation authorRelation = customerRelationService.getCustomerRelationByMobile(author.getMobile());
					if (authorRelation == null) {
						authorRelation = customerRelationService.saveCustomerRelation(commonServerProperties.getCompanyCode(), author.getMobile());
					}

					InsPersonaUuidInfo.AuthorInfo authorInfo = new InsPersonaUuidInfo.AuthorInfo();
					authorInfo.setAuthorUuid(authorRelation.getUuid());
					insPersonaUuidInfo.setAuthorListJson(GsonUtil.obj2Json(Collections.singletonList(authorInfo)));
				}
			}
			personaInfoService.sendPersonaUuid2Meix(insPersonaUuidInfo);
		} catch (Exception e) {
			log.error("", e);
		}
	}

}
