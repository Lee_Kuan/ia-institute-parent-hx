package com.meix.institute.impl;

import com.meix.institute.BaseService;
import com.meix.institute.api.ICompanyService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.ISecumainService;
import com.meix.institute.api.ISelfStockService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsCompanyMonthlyGoldStock;
import com.meix.institute.entity.InsUser;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.mapper.*;
import com.meix.institute.search.StockYieldSearchVo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.SortUtils;
import com.meix.institute.util.StringUtil;
import com.meix.institute.vo.GroupPowerVo;
import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.stock.StockVo;
import com.meix.institute.vo.user.InfluenceAnalyseVo;
import com.meix.institute.vo.user.InfluenceTrend;
import com.meix.institute.vo.user.UserInfo;
import org.apache.commons.collections.MapUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;

/**
 * @Description:公司设置
 */
@Service
public class CompanyServiceImpl extends BaseService implements ICompanyService {

	private static final SortUtils.MonthlyStockDescComparator monthlyStockComparator = new SortUtils.MonthlyStockDescComparator();
	private static Logger log = LoggerFactory.getLogger(CompanyServiceImpl.class);

	@Autowired
	private CompanyDaoImpl companyDaoImpl;
	@Autowired
	private InsCompanyMonthlyGoldStockMapper goldStockMapper;
	@Autowired
	private ReportMapper myReportMapper;
	@Autowired
	private ActivityMapper myActivityMapper;
	@Autowired
	private UserDaoImpl userDaoImpl;
	@Autowired
	private ISecumainService secumainService;
	@Autowired
	private IInsUserService insUserService;
	@Autowired
	private ISelfStockService selfStockService;
	@Autowired
	private ICustomerGroupService groupService;

	@Override
	public CompanyInfoVo getCompanyInfoByName(String companyName) {
		return companyDaoImpl.getCompanyInfoByName(companyName);
	}

	@Override
	public CompanyInfoVo getCompanyInfoById(long companyCode) {
		CompanyInfoVo company = companyDaoImpl.getCompanyInfoById(companyCode);
		return company;
	}

	@Override
	public List<StockVo> getCompanyMonthlyStockVo(Map<String, Object> params) {
		long uid = MapUtils.getLongValue(params, "uid", 0);
		long companyCode = MapUtils.getLongValue(params, "companyCode", 0);
		String month = MapUtils.getString(params, "month", null);
		List<GroupPowerVo> powerList = groupService.getResourceGroupPower(uid, 0, MeixConstants.USER_GP, 0, MeixConstants.SHARE_LIST);
		if (powerList == null || powerList.isEmpty() || powerList.get(0).getPower() == 0) { //无权限
			return Collections.emptyList();
		}
		if (StringUtil.isBlank(month)) {
			String currentMonth = DateUtil.dateToStr(System.currentTimeMillis(), "yyyy-MM");
			Example example = new Example(InsCompanyMonthlyGoldStock.class);
			example.createCriteria().andEqualTo("companyCode", companyCode)
				.andLessThanOrEqualTo("month", currentMonth)
				.andEqualTo("homeShowFlag", 1);
			example.orderBy("month").desc();
			RowBounds rowBounds = new RowBounds(0, 1);
			List<InsCompanyMonthlyGoldStock> stockList = goldStockMapper.selectByExampleAndRowBounds(example, rowBounds);
			if (stockList != null && stockList.size() > 0) {
				month = stockList.get(0).getMonth();
			}
		}

		Example example = new Example(InsCompanyMonthlyGoldStock.class);
		example.createCriteria().andEqualTo("companyCode", companyCode)
			.andEqualTo("month", month)
			.andEqualTo("homeShowFlag", 1);
		example.orderBy("month").desc();

		//去掉分页
		List<InsCompanyMonthlyGoldStock> stockList = goldStockMapper.selectByExample(example);
		if (stockList == null || stockList.size() == 0) {
			return Collections.emptyList();
		}

		List<StockVo> list = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		String endDate = DateUtil.dateToStr(calendar.getTimeInMillis(), "yyyy-MM-dd");
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		String startDate = DateUtil.dateToStr(calendar.getTimeInMillis(), "yyyy-MM-dd");

		List<StockYieldSearchVo> searchList = new ArrayList<>();
		for (InsCompanyMonthlyGoldStock item : stockList) {
			SecuMain secuMain = secumainService.getSecuMain(item.getInnerCode());
			if (secuMain == null) {
				continue;
			}
			StockVo stockVo = new StockVo();
			stockVo.setCompanyCode(item.getCompanyCode());
			stockVo.setRecDesc(item.getRecDesc());
			stockVo.setInnerCode(item.getInnerCode());
			stockVo.setMonth(item.getMonth());
			stockVo.setSecuCode(secuMain.getSecuCode());
			stockVo.setSecuAbbr(secuMain.getSecuAbbr());
			stockVo.setSuffix(secuMain.getSuffix());
			stockVo.setSecuMarket(secuMain.getSecuMarket());
			stockVo.setSecuClass(secuMain.getSecuCategory());
			stockVo.setIndustryCode(secuMain.getIndustryCode());
			stockVo.setIndustryName(secuMain.getIndustryName());
			stockVo.setAuthorName(item.getRecPerson());

			StockYieldSearchVo searchVo = new StockYieldSearchVo();
			searchVo.setInnerCode(item.getInnerCode());
			searchVo.setStartDate(startDate);
			searchVo.setEndDate(endDate);
			searchList.add(searchVo);

			int exsit = selfStockService.checkStockIsExist(uid, item.getInnerCode());
			stockVo.setIsSelfstock(exsit);
			//月度金股数据中存的内部员工id，转换为公众号用户id
			InsUser insUser = insUserService.getUserInfo(item.getRecPerson());
			if (insUser != null) {
				stockVo.setAuthorName(insUser.getUserName());
				UserInfo userInfo = userServiceImpl.getUserInfo(0, null, insUser.getMobile());
				if (userInfo != null) {
					stockVo.setAuthorCode(userInfo.getId());
				}
			}
			list.add(stockVo);
		}

		Map<Integer, BigDecimal> yieldMap = getStockIntervalYield(searchList);
		if (yieldMap != null) {
			for (StockVo item : list) {
				BigDecimal stockYield = yieldMap.get(item.getInnerCode());
				if (stockYield != null) {
					item.setMonthYieldRate(stockYield.setScale(4, BigDecimal.ROUND_HALF_UP).floatValue());
				}
			}
		}
		list.sort(monthlyStockComparator);
		return list;
	}

	@CacheEvict(value = "companyCodeCache", allEntries = true)
	// 清空 accountCache 缓存
	public void reload(long companyCode) {
	}

	public long getCompanyCodeByReportOrgCode(long orgCode) {
		long companyCode = 0;
		if (MeixConstants.companyOrgRela.containsKey(orgCode)) {
			companyCode = MeixConstants.companyOrgRela.get(orgCode);
		}
		return companyCode;
	}


	@Override
	public Long addCompanyInfo(CompanyInfoVo vo) {

		companyDaoImpl.addCompanyInfo(vo);
		return vo.getId();
	}

	@Override
	public List<InfluenceAnalyseVo> getCompanyInfluenceAnalysis(Map<String, Object> params) {
		List<InfluenceAnalyseVo> result = null;
		int dataType = MapUtils.getIntValue(params, "dataType", 0);
		if (dataType == MeixConstants.USER_YB) {
			result = myReportMapper.getResearchReportReadRank(params);
		} else if (dataType == MeixConstants.USER_HD) {
			result = myActivityMapper.getActivityAttendRank(params);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public InfluenceTrend getCompanyInfluenceTrend(Map<String, Object> params) {
		InfluenceTrend influenceTrend = new InfluenceTrend();
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			//本月
			String startTime = DateUtil.dateToStr(calendar.getTimeInMillis(), DateUtil.dateFormat);
			String endTime = DateUtil.getCurrentDateTime();
			long monthActAttenderNum = myActivityMapper.getActAttenderCount(commonServerProperties.getCompanyCode(), startTime, endTime);
			influenceTrend.setMonthActAttenderNum(monthActAttenderNum);

			//今天
			startTime = DateUtil.dateToStr(DateUtil.getStartDate(new Date()), DateUtil.dateFormat);
			long todayVisitorNum = userDaoImpl.getLoginRecordCount(commonServerProperties.getCompanyCode(), startTime, endTime);
			influenceTrend.setTodayVisitorNum(todayVisitorNum);

			long todayActAttenderNum = myActivityMapper.getActAttenderCount(commonServerProperties.getCompanyCode(), startTime, endTime);

			//昨天
			calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			startTime = DateUtil.dateToStr(DateUtil.getStartDate(calendar.getTime()), DateUtil.dateFormat);
			endTime = DateUtil.dateToStr(DateUtil.getEndDate(calendar.getTime()), DateUtil.dateFormat);
			long lastVisitorNum = userDaoImpl.getLoginRecordCount(commonServerProperties.getCompanyCode(), startTime, endTime);
			long lastActAttenderNum = myActivityMapper.getActAttenderCount(commonServerProperties.getCompanyCode(), startTime, endTime);

			influenceTrend.setTodayVisitorChange(todayVisitorNum - lastVisitorNum);
			influenceTrend.setTodayActAttenderChange(todayActAttenderNum - lastActAttenderNum);
		} catch (Exception e) {
			log.error("", e);
		}
		return influenceTrend;
	}

	@Override
	public List<CompanyInfoVo> getCompanyList(String condition, int currentPage, int showNum) {
		return companyDaoImpl.getCompanyList(condition, currentPage, showNum);
	}

}
