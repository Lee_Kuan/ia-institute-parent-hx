package com.meix.institute.impl;

import com.meix.institute.api.IPersonaInfoService;
import com.meix.institute.constant.HttpApi;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.vo.log.InsPersonaUuidInfo;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zenghao on 2019/10/12.
 */
@Service
public class PersonaInfoServiceImpl implements IPersonaInfoService {
	private static Logger log = LoggerFactory.getLogger(PersonaInfoServiceImpl.class);
	@Autowired
	private IaServiceImpl iaServiceImpl;

	@Override
	public void sendPersonaUuid2Meix(InsPersonaUuidInfo insPersonaUuidInfo) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataJson", GsonUtil.obj2Json(Collections.singletonList(insPersonaUuidInfo)));
		params.put("tempToken", MeixConstants.TEMP_TOKEN);
		JSONObject data = iaServiceImpl.httpPostByWehcat(params, HttpApi.SYNC_SAVE_PERSONA_UUID);
		if (data == null) {
			log.error("发送画像到美市服务器失败");
			return;
		}
		int messageCode = data.getInt("messageCode");
		if (messageCode != MeixConstantCode.M_1008) {
			String message = data.getString("message");
			log.error("发送画像到美市服务器失败，原因：{}", message);
		}
	}

}
