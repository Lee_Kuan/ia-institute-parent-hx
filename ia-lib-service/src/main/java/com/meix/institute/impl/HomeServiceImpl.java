package com.meix.institute.impl;

import com.meix.institute.BaseService;
import com.meix.institute.api.IHomeService;
import com.meix.institute.api.IRegulationService;
import com.meix.institute.api.ISelfStockService;
import com.meix.institute.api.IUserDao;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsBanner;
import com.meix.institute.mapper.BannerMapper;
import com.meix.institute.mapper.InsBannerMapper;
import com.meix.institute.response.HoneybeeMsgInfo;
import com.meix.institute.response.StockRelationDataPo;
import com.meix.institute.response.components.CommonComponent;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.BaseVo;
import com.meix.institute.vo.HoneyBeeBaseVo;
import com.meix.institute.vo.activity.ActivityVo;
import com.meix.institute.vo.banner.InsBannerVo;
import com.meix.institute.vo.meeting.MorningMeetingListVo;
import com.meix.institute.vo.stock.StockVo;
import com.meix.institute.vo.user.UserInfo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * Created by zenghao on 2019/10/8.
 */
@Service
public class HomeServiceImpl extends BaseService implements IHomeService {
	private static Logger log = LoggerFactory.getLogger(HomeServiceImpl.class);
	@Autowired
	private IUserDao userDao;
	@Autowired
	private ISelfStockService selfStockService;
	@Autowired
	private IRegulationService regulationService;
	@Autowired
	private BannerMapper myBannerMapper;
	@Autowired
	private InsBannerMapper bannerMapper;

	@Override
	public HoneybeeMsgInfo getHotRecommendList(long uid, int currentPage, int showNum, int issueType) {
		Set<Integer> selfStockSet = selfStockService.getSelfStockList(uid);

		List<HoneyBeeBaseVo> waterFlowList = regulationService.getHotRecommendList(currentPage, showNum, selfStockSet, issueType);

		long startTime1 = System.currentTimeMillis();
		HoneybeeMsgInfo honeybeeMsgInfo = new HoneybeeMsgInfo();
		UserInfo uic = getUserInfo(uid);
		if (uic == null) {
			uic = new UserInfo();
		}
		List<Long> focusedPersons = null;
		if (uid != 0) {
			focusedPersons = userDao.getFocusedPersons(uid);
		}
		long startTime2 = System.currentTimeMillis();
		log.info("【getHotRecommendList】获取用户自选股、关注的人等信息耗时：{}", startTime2 - startTime1);
		log.info("【getHotRecommendList】获取热点推荐列表耗时：{}", System.currentTimeMillis() - startTime2);
		if (waterFlowList == null || waterFlowList.size() == 0) {
			honeybeeMsgInfo.setData(Collections.emptyList());
			return honeybeeMsgInfo;
		}
		//返回的首页瀑布流数据列表(研报、调仓等)
		List<CommonComponent> mainPageWaterFlowList = new ArrayList<>();
		//数据详情
		CommonComponent waterFlowDetailInfo = null;
		long startTime3 = System.currentTimeMillis();

		int count = waterFlowList.size();
		log.info("【getRecommendedInfo】 count：{}，showNum：{}，querySize:{}", count, showNum, waterFlowList.size());
		for (int i = 0; i < count; i++) {
			HoneyBeeBaseVo queueData = waterFlowList.get(i);
			HoneyBeeBaseVo waterFlowVo = new HoneyBeeBaseVo();
			BeanUtils.copyProperties(queueData, waterFlowVo);
			waterFlowDetailInfo = new CommonComponent();
			waterFlowDetailInfo.setWaterflowRecType(queueData.getWaterflowRecType());
			waterFlowDetailInfo.setRecScore(waterFlowVo.getRanking());
			waterFlowDetailInfo.setTitle(waterFlowVo.getTitle());
			waterFlowDetailInfo.setContent(waterFlowVo.getContent());
			BaseVo item = null;
			switch (waterFlowVo.getDataType()) {
				case MeixConstants.USER_YB:
				case MeixConstants.USER_HD:
				case MeixConstants.USER_CH:
				case MeixConstants.USER_GP:
					try {
						item = getWaterFlowInfo(focusedPersons, waterFlowVo, uic, waterFlowDetailInfo, selfStockSet);
					} catch (Exception e) {
						log.error("", e);
					}
					if (item == null) {
						log.info("【getHotRecommendList】，waterFlowVo:{}", JSONObject.fromObject(waterFlowVo));
						continue;
					}
					//根据ia_sys_waterFlow中配置的数据查找每条数据的详细信息
					waterFlowDetailInfo.setItem(item);
					waterFlowDetailInfo.setId(waterFlowVo.getDataId());
					break;
				default:
					break;
			}
			waterFlowDetailInfo.setQueueId(waterFlowVo.getQueueId());
			// 有发布时间就使用发布时间
			if (!StringUtils.isNullorWhiteSpace(waterFlowVo.getApplyTime())) {
				waterFlowDetailInfo.setPublishTime(waterFlowVo.getApplyTime());
			}
			waterFlowDetailInfo.setType(waterFlowVo.getDataType());
			waterFlowDetailInfo.setFrom(waterFlowVo.getFrom()); //二期优化

			waterFlowDetailInfo.setQueueId(waterFlowVo.getQueueId());
			mainPageWaterFlowList.add(waterFlowDetailInfo);
		}
		log.info("【getHotRecommendList】热点推荐列表拼装cell耗时：{}ms", System.currentTimeMillis() - startTime3);

		honeybeeMsgInfo.setData(mainPageWaterFlowList);
		return honeybeeMsgInfo;
	}

	@Override
	public HoneybeeMsgInfo getMainPageWaterFlowList(long uid, int currentPage, int showNum, String lastId, int issueType) {
		Set<Integer> selfStockSet = selfStockService.getSelfStockList(uid);

		List<HoneyBeeBaseVo> waterFlowList = regulationService.getRecommendList(uid, currentPage, showNum, lastId, selfStockSet, issueType);

		long startTime1 = System.currentTimeMillis();
		HoneybeeMsgInfo honeybeeMsgInfo = new HoneybeeMsgInfo();
		log.info("【getRecommendedInfo】 lastId：{}", lastId);
		List<String> heavyLogoList = null;
		if (lastId != null && !lastId.equals("0")) {
			try {
				heavyLogoList = GsonUtil.json2List(lastId, String.class);
			} catch (Exception e) {
				log.error("", e);
			}
		}
		if (heavyLogoList == null) {
			heavyLogoList = new ArrayList<>();
		}
		List<String> lastIds = new ArrayList<>();
		lastIds.addAll(heavyLogoList);
		UserInfo uic = getUserInfo(uid);
		if (uic == null) {
			uic = new UserInfo();
		}
		List<Long> focusedPersons = null;
		if (uid != 0) {
			focusedPersons = userDao.getFocusedPersons(uid);
		}
		long startTime2 = System.currentTimeMillis();
		log.info("【getMainPageWaterFlowList】获取用户自选股、关注的人等信息耗时：{}", startTime2 - startTime1);
		log.info("【getMainPageWaterFlowList】获取推荐列表耗时：{}", System.currentTimeMillis() - startTime2);
		if (waterFlowList == null || waterFlowList.size() == 0) {
			honeybeeMsgInfo.setData(Collections.emptyList());
			Map<String, Object> objMap = new HashMap<>();
			objMap.put("lastId", heavyLogoList);
			honeybeeMsgInfo.setObject(objMap);
			return honeybeeMsgInfo;
		}
		//返回的首页瀑布流数据列表(研报、调仓等)
		List<CommonComponent> mainPageWaterFlowList = new ArrayList<>();
		//数据详情
		CommonComponent waterFlowDetailInfo = null;
		long startTime3 = System.currentTimeMillis();

//		int count = Math.min(showNum, waterFlowList.size());
		int count = waterFlowList.size();
		log.info("【getRecommendedInfo】 count：{}，showNum：{}，querySize:{}", count, showNum, waterFlowList.size());
		for (int i = 0; i < count; i++) {
			HoneyBeeBaseVo queueData = waterFlowList.get(i);
			HoneyBeeBaseVo waterFlowVo = new HoneyBeeBaseVo();
			BeanUtils.copyProperties(queueData, waterFlowVo);
			waterFlowDetailInfo = new CommonComponent();
			waterFlowDetailInfo.setWaterflowRecType(queueData.getWaterflowRecType());
			waterFlowDetailInfo.setRecScore(waterFlowVo.getRanking());
			waterFlowDetailInfo.setTitle(waterFlowVo.getTitle());
			waterFlowDetailInfo.setContent(waterFlowVo.getContent());
			BaseVo item = null;
			switch (waterFlowVo.getDataType()) {
				case MeixConstants.USER_YB:
				case MeixConstants.USER_HD:
				case MeixConstants.USER_CH:
				case MeixConstants.USER_GP:
					try {
						item = getWaterFlowInfo(focusedPersons, waterFlowVo, uic, waterFlowDetailInfo, selfStockSet);
					} catch (Exception e) {
						log.error("", e);
					}
					if (item == null) {
						log.info("【getMainPageWaterFlowList】，waterFlowVo:{}", JSONObject.fromObject(waterFlowVo));
						continue;
					}
					//根据ia_sys_waterFlow中配置的数据查找每条数据的详细信息
					waterFlowDetailInfo.setItem(item);
					waterFlowDetailInfo.setId(waterFlowVo.getDataId());
					break;
				default:
					break;
			}
			waterFlowDetailInfo.setQueueId(waterFlowVo.getQueueId());
			// 有发布时间就使用发布时间
			if (!StringUtils.isNullorWhiteSpace(waterFlowVo.getApplyTime())) {
				waterFlowDetailInfo.setPublishTime(waterFlowVo.getApplyTime());
			}
			waterFlowDetailInfo.setType(waterFlowVo.getDataType());
			waterFlowDetailInfo.setFrom(waterFlowVo.getFrom()); //二期优化

			waterFlowDetailInfo.setQueueId(waterFlowVo.getQueueId());
			mainPageWaterFlowList.add(waterFlowDetailInfo);
		}
		log.info("【getMainPageWaterFlowList】推荐列表拼装cell耗时：{}ms", System.currentTimeMillis() - startTime3);

		honeybeeMsgInfo.setData(mainPageWaterFlowList);
		Map<String, Object> objMap = new HashMap<>();
		objMap.put("lastId", heavyLogoList);
		log.info("【getRecommendedInfo】lastIdList size：{}", heavyLogoList.size());
		log.info("【getRecommendedInfo】lastIdList：{}", GsonUtil.obj2Json(heavyLogoList));
		honeybeeMsgInfo.setObject(objMap);
		return honeybeeMsgInfo;
	}

	/**
	 * 瀑布流来源
	 *
	 * @param uid
	 * @param vo
	 * @param focusedPersons
	 */
	@SuppressWarnings("unchecked")
	private void fillLabel(long uid, HoneyBeeBaseVo vo, List<Long> focusedPersons, Set<Integer> selfStockSet) {
		int dataType = vo.getDataType();

		if (dataType == MeixConstants.USER_HD) {
			Map<String, Object> activityVo = vo.getItem();
			if (activityVo != null) {
				List<Map<String, Object>> secuList = (List<Map<String, Object>>) activityVo.get("secuList");

				if (isActivityByFocusedPerson(focusedPersons, activityVo)) {
					vo.setFrom("我关注的作者");
				} else if (secuList != null && secuList.size() > 0) {
					for (int i = 0; i < secuList.size(); i++) {
						Map<String, Object> secuMap = secuList.get(i);
						int innerCode = MapUtils.getIntValue(secuMap, "innerCode", 0);
						if (selfStockSet != null && selfStockSet.contains(innerCode)) {
							vo.setFrom("我的自选股");
							break;
						}
					}
				}
			}
		} else {
			switch (dataType) {
				case MeixConstants.USER_YB: {
					break;
				}
				default: {
					break;
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public boolean isActivityByFocusedPerson(List<Long> focusedPersons, Map<String, Object> activityVo) {
		List<Map<String, Object>> analysts = (List<Map<String, Object>>) activityVo.get("analyst");
		if (analysts == null) {
			return false;
		}

		for (int i = 0; i < analysts.size(); i++) {
			Map<String, Object> map = analysts.get(i);
			long authorCode = MapUtils.getLongValue(map, "code", 0);
			if (focusedPersons != null && focusedPersons.contains(authorCode)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @param focusedPersons
	 * @param vo
	 * @param uic
	 * @param waterFlowDetailInfo 接口返回的结果集
	 * @return
	 * @throws Exception
	 */
	public BaseVo getWaterFlowInfo(List<Long> focusedPersons, HoneyBeeBaseVo vo,
	                               UserInfo uic, CommonComponent waterFlowDetailInfo,
	                               Set<Integer> selfStockSet) throws Exception {
		if (uic != null) {
			fillLabel(uic.getId(), vo, focusedPersons, selfStockSet);
		}

		List<Long> ids = new ArrayList<>();
		long dataId = vo.getDataId();
		ids.add(vo.getDataId());
		int dataType = vo.getDataType();

		BaseVo item = null;
		switch (dataType) {
			case MeixConstants.USER_YB: {//研报
				Map<String, Object> stockRelationDataPo = vo.getItem();
				if (stockRelationDataPo == null) {
					log.warn("【getWaterFlowInfo】研报信息不存在,ids:{}", JSONArray.fromObject(ids).toString());
					break;
				}

				waterFlowDetailInfo.setTitle((String) stockRelationDataPo.get("title"));
				item = GsonUtil.json2Obj(GsonUtil.obj2Json(stockRelationDataPo), StockRelationDataPo.class);
				break;
			}
			case MeixConstants.USER_HD: {//活动
				Map<String, Object> activityVo = vo.getItem();
				if (activityVo == null) {
					log.warn("【getWaterFlowInfo】活动信息不存在,dataId:{}", JSONArray.fromObject(ids).toString());
					break;
				}

				waterFlowDetailInfo.setTitle((String) activityVo.get("title"));
				String fileAttr = MapUtils.getString(activityVo, "fileAttr", null);
				activityVo.put("fileAttr", null);
				item = GsonUtil.json2Obj(GsonUtil.obj2Json(activityVo), ActivityVo.class);
				if (item != null) {
					((ActivityVo) item).setFileAttr(fileAttr);
				}
				break;
			}
			case MeixConstants.USER_CH: {//晨会
				Map<String, Object> meeting = vo.getItem();
				if (meeting == null) {
					log.warn("【getWaterFlowInfo】晨会信息不存在,dataId:{}", JSONArray.fromObject(ids).toString());
					break;
				}

				waterFlowDetailInfo.setTitle((String) meeting.get("title"));
				String fileAttr = MapUtils.getString(meeting, "fileAttr", null);
				meeting.put("fileAttr", null);
				item = GsonUtil.json2Obj(GsonUtil.obj2Json(meeting), MorningMeetingListVo.class);
				if (item != null) {
					((MorningMeetingListVo) item).setFileAttr(fileAttr);
				}
				break;
			}
			case MeixConstants.USER_GP: {
				Map<String, Object> stock = vo.getItem();
				if (stock == null) {
					log.warn("【getWaterFlowInfo】股票信息不存在,dataId:{}", JSONArray.fromObject(ids).toString());
					break;
				}
				waterFlowDetailInfo.setTitle((String) stock.get("title"));
				item = GsonUtil.json2Obj(GsonUtil.obj2Json(stock), StockVo.class);
				break;
			}
			default:
				break;
		}
		if (item == null) {
			return null;
		}
		item.setDataId(dataId);
		switch (dataType) {
			case MeixConstants.USER_YB: {
				break;
			}
			case MeixConstants.USER_HD: {
				ActivityVo activityVo = (ActivityVo) item;
				List<Map<String, Object>> analystList = activityVo.getAnalyst();
				if (analystList != null && analystList.size() > 0) {
					Map<String, Object> analystInfo = analystList.get(0);
					long code = MapUtils.getLongValue(analystInfo, "code", 0);
					if (code != 0) {
						if (focusedPersons != null && focusedPersons.contains(code)) {
							activityVo.setFollowFlag(1);
						}
					}
				}
				break;
			}
			case MeixConstants.USER_CH: {
				break;
			}
			case MeixConstants.USER_GP: {
//				log.info(GsonUtil.obj2Json(item));
				break;
			}
		}
		return item;
	}

	@Override
	public List<InsBannerVo> getBannerList(int applyModule, int bannerType, int currentPage, int showNum) {
		return myBannerMapper.getBannerList(applyModule, bannerType, currentPage, showNum);
	}

	@Override
	public void saveBanner(InsBanner banner) {
		bannerMapper.insert(banner);
	}

	@Override
	public void saveBannerSort(String uuid, JSONArray ids) {
		if (ids != null && !ids.isEmpty()) {
			Date nowDate = new Date();
			int i = 1;
			for (Object temp : ids) {
				long id = Long.valueOf(String.valueOf(temp));
				InsBanner banner = new InsBanner();
				banner.setId(id);
				banner.setUpdatedAt(nowDate);
				banner.setUpdatedBy(uuid);
				banner.setNo(i);
				i++;
				bannerMapper.updateByPrimaryKeySelective(banner);
			}
		}
	}

	@Override
	public List<InsBannerVo> getBannerList() {
		Example example = new Example(InsBanner.class);
		example.orderBy("no").asc().orderBy("createdAt").desc();
		List<InsBanner> list = bannerMapper.selectByExample(example);
		List<InsBannerVo> res = new ArrayList<>();
		for (InsBanner temp : list) {
			InsBannerVo vo = new InsBannerVo();
			vo.setId(temp.getId());
			vo.setComment(temp.getComment());
			vo.setResourceId(temp.getResourceId());
			vo.setResourceUrl(temp.getResourceUrl());
			vo.setTitle(temp.getTitle());
			vo.setType(temp.getType());
			vo.setTypeName(getTypeName(temp.getType()));
			vo.setUpdatedAt(temp.getUpdatedAt());
			res.add(vo);
		}
		return res;
	}

	@Override
	public void deleteBanner(long id) {
		bannerMapper.deleteByPrimaryKey(id);
	}

}
