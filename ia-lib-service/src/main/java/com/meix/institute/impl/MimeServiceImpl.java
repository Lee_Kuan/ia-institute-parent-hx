package com.meix.institute.impl;

import com.meix.institute.api.FsService;
import com.meix.institute.api.MimeService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.core.Filepath;
import com.meix.institute.core.Mime;
import com.meix.institute.entity.Attachment;
import com.meix.institute.mapper.AttachmentMapper;
import com.meix.institute.util.UuidUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.util.Date;

@Service
public class MimeServiceImpl implements MimeService {

	@Autowired
	private AttachmentMapper attachmentMapper;
	@Autowired
	private FsService fsService;
	@Autowired
	private CommonServerProperties serverProperties;

	@Transactional
	@Override
	public Filepath write(Mime mime, InputStream is, String user) {
		if (StringUtils.isBlank(user)) {
			user = serverProperties.getDefaultUser();
		}
		//新建文件信息
		Date _now = new Date();
		String uuid = UuidUtil.generate32bitUUID();
		Attachment record = new Attachment();
		String fileName = mime.getFileName();
		record.setUuid(uuid);
		record.setContentType(mime.getContentType());
		record.setFileName(fileName);
		record.setFilePath("dummy");
		record.setUpdatedAt(_now);
		record.setUpdatedBy(user);
		record.setCreatedAt(_now);
		record.setCreatedBy(user);
		attachmentMapper.insert(record);
		Long id = record.getId();

		//加入扩展名
		String fn = Long.toString(id);
		if (StringUtils.isNotBlank(fileName)) {
			int indexOf = fileName.lastIndexOf(".");
			if (indexOf > 0) {
				fn += fileName.substring(indexOf);
			}
		}

		//保存附件
		String filePath = fsService.write(fn, is);

		//回写路径
		record = new Attachment();
		record.setId(id);
		record.setFilePath(filePath);
		attachmentMapper.updateByPrimaryKeySelective(record);

		Filepath result = new Filepath();
		result.setUuid(uuid);
		result.setPath(serverProperties.getUploadServer() + filePath);
		return result;
	}

}
