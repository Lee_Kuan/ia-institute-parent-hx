package com.meix.institute.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.BaseService;
import com.meix.institute.api.IConfigService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsMessagePushConfig;
import com.meix.institute.mapper.ConfigMapper;
import com.meix.institute.mapper.InsMessagePushConfigMapper;
import com.meix.institute.vo.InsMessagePushConfigVo;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class ConfigServiceImpl extends BaseService implements IConfigService {

	@Autowired
	private InsMessagePushConfigMapper pushConfigMapper;
	@Autowired
	private IInstituteMessagePushService messagePushService;
	@Autowired
	private ConfigMapper myConfigMapper;
	
	@Override
	public long saveMessagePushConfig(InsMessagePushConfig config) {
		if (config.getId() > 0) {
			pushConfigMapper.updateByPrimaryKeySelective(config);
		} else {
			config.setCreatedAt(config.getUpdatedAt());
			config.setCreatedBy(config.getUpdatedBy());
			pushConfigMapper.insert(config);
		}
		return config.getId();
	}
	@Override
	public List<InsMessagePushConfig> getPushConfigList(int messageType, int currentPage, int showNum) {
		Example example = new Example(InsMessagePushConfig.class);
		if (messageType > -1) {
			example.createCriteria().andEqualTo("messageType", messageType);
		}
		example.orderBy("updatedAt").desc();
		RowBounds rowBounds = new RowBounds(currentPage * showNum, showNum);
		return pushConfigMapper.selectByExampleAndRowBounds(example, rowBounds);
	}
	@Override
	public int getPushConfigCount(int messageType) {
		Example example = new Example(InsMessagePushConfig.class);
		if (messageType > -1) {
			example.createCriteria().andEqualTo("messageType", messageType);
		}
		return pushConfigMapper.selectCountByExample(example);
	}
	@Override
	public InsMessagePushConfigVo getPushConfigDetail(int id) {
		InsMessagePushConfig res = pushConfigMapper.selectByPrimaryKey(id);
		InsMessagePushConfigVo vo = new InsMessagePushConfigVo();
		BeanUtils.copyProperties(res, vo);
		vo.setReceiverType(getReceiverTypeList(res.getReceiverType()));
		return vo;
	}
	@Override
	public void userMessageConfigPushJob() {
		Example example = new Example(InsMessagePushConfig.class);
		Criteria creteria = example.createCriteria();
		creteria.andEqualTo("pushType", 1).andEqualTo("pushStatus", 0);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.SECOND, 0);
		Date dateTo = cal.getTime();
		cal.add(Calendar.MINUTE, -1);
		Date dateFm = cal.getTime();
		creteria.andBetween("pushTime", dateFm, dateTo);
		List<InsMessagePushConfig> list = pushConfigMapper.selectByExample(example);
		if (list != null && !list.isEmpty()) {
			for (InsMessagePushConfig temp : list) {
				messagePushService.pushUserConfigMessage(temp.getId(), temp.getMessageType(), 
						temp.getReceiverType(), temp.getComment(), temp.getTitle(), temp.getContent());
			}
		}
	}
	@Override
	public List<Map<String, Object>> getReceiverList(int id, List<Integer> receiverType, int pushStatus, int messageType,
			int currentPage, int showNum) {
		List<Map<String, Object>> list = null;
		List<Integer> accountTypeList = new ArrayList<>();
		if (pushStatus == 0) {//未推送
			for (int i : receiverType) {
				if (i == 1) {
					accountTypeList.add(MeixConstants.ACCOUNT_WL);
				} else if (i == 2) {
					accountTypeList.add(MeixConstants.ACCOUNT_NORMAL);
				} else if (i == 4) {
					accountTypeList.add(MeixConstants.ACCOUNT_TOURIST);
				}
			}
			if (accountTypeList.size() > 0) {
				list = myConfigMapper.getCustomerList(accountTypeList, messageType, currentPage, showNum);
			}
		} else {
			list = myConfigMapper.getReceiverList(id, currentPage, showNum);
		}
		return list;
	}
	@Override
	public int getReceiverCount(int id, List<Integer> receiverType, Integer pushStatus) {
		
		int count = 0;
		List<Integer> accountTypeList = new ArrayList<>();
		if (pushStatus == 0) {//未推送
			for (int i : receiverType) {
				if (i == 1) {
					accountTypeList.add(MeixConstants.ACCOUNT_WL);
				} else if (i == 2) {
					accountTypeList.add(MeixConstants.ACCOUNT_NORMAL);
				} else if (i == 4) {
					accountTypeList.add(MeixConstants.ACCOUNT_TOURIST);
				}
			}
			if (accountTypeList.size() > 0) {
				count = myConfigMapper.getCustomerCount(accountTypeList);
			}
		} else {
			count = myConfigMapper.getReceiverCount(id);
		}
		return count;
	}
	@Override
	public String getSystemUrl(String type) {
		return myConfigMapper.getSystemUrl(type);
	}
	
}
