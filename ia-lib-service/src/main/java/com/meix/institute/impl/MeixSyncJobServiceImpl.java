package com.meix.institute.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.api.IMeixSyncJobService;
import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.api.IReportService;
import com.meix.institute.api.IUserService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.constant.SyncConstants;
import com.meix.institute.util.DateUtil;

import oracle.sql.BLOB;

/**
 * 美市数据同步 job服务
 *
 * @author likuan
 * @since 2020年4月15日13:47:52
 */
@Service
public class MeixSyncJobServiceImpl implements IMeixSyncJobService {

	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IUserService userServiceImpl;
	@Autowired
	private IReportService reportServiceImpl;
	@Autowired
	private IMeixSyncService syncService;

	private static final Logger log = LoggerFactory.getLogger(MeixSyncJobServiceImpl.class);

	@Override
	public void syncWhiteUserToMeix() {
		List<Map<String, Object>> list = new ArrayList<>();
		int size = 0;
		Date lastSyncDate = syncService.getLastSendDateTime(SyncConstants.ACTION_USER_WL);
		String lastSyncTime = lastSyncDate == null ? "1991-01-01 00:00:00" : DateUtil.formatDate(lastSyncDate);
		int currentPage = 0;
		while (currentPage == 0 || list.size() == 1000) {
			try {
				list = userServiceImpl.getCustomerList(MeixConstants.ACCOUNT_WL, -1, null, currentPage, 1000, lastSyncTime, null, -1, 0, 0);
			} catch (Exception e) {
				log.error("", e);
			}
			currentPage++;
			if (list == null || list.size() == 0) {
				break;
			}
			size += list.size();
			Map<String, Object> params = new HashMap<>();
			params.put("userList", list);
			params.put("companyCode", commonServerProperties.getCompanyCode());
			syncService.reqMeixSync(MeixConstants.USER_YJS_WL, SyncConstants.ACTION_USER_WL, params);
		}
		log.info("syncWhiteUserToMeix success! total:" + size);
	}

	@Override
	public void syncReportToMeix(boolean isSyncAll) {
		List<Map<String, Object>> list = new ArrayList<>();
		int size = 0;
		Date lastSyncDate = syncService.getLastSendDateTime(SyncConstants.ACTION_SAVE_REPORT);
		String lastSyncTime = lastSyncDate == null || isSyncAll ? "1991-01-01 00:00:00" : DateUtil.formatDate(lastSyncDate);
		int currentPage = 0;
		while (currentPage == 0 || list.size() == 100) {
			try {
				list = reportServiceImpl.getReportAllList(-1, 100, currentPage, lastSyncTime); //研报
				currentPage++;
				if (list == null || list.size() == 0) {
					break;
				}
				for (Map<String, Object> temp : list) {
					String fileUrl = MapUtils.getString(temp, "fileUrl", "");
					if (StringUtils.isNotBlank(fileUrl)) {
						BLOB res = reportServiceImpl.getReportFile(fileUrl);
						InputStream is = res.getBinaryStream();
//		            int len = (int) res.length();
//		            byte buffer[] = new byte[len];
//					while ((len = input.read(buffer)) != -1) {
//						os.write(buffer, 0, len);
//					}
//					os.flush();
//					os.close();
//					input.close();
//					InputStream is = HttpUtil.smbGetIs(pdfConfig.getPdfServer() + fileUrl, auth);
						if (is != null) {
							String fileName[] = fileUrl.split("/");
							fileUrl = syncService.reqMeixFileSync(is, fileName[fileName.length - 1]);
							try {
								is.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
					if (StringUtils.isNotBlank(fileUrl)) {
						temp.put("fileUrl", fileUrl);
					}
				}
				size += list.size();
				Map<String, Object> params = new HashMap<>();
				params.put("reportList", list);
				params.put("companyCode", commonServerProperties.getCompanyCode());
				syncService.reqMeixSync(MeixConstants.USER_YB, SyncConstants.ACTION_SAVE_REPORT, params);
			} catch (Exception e) {
				log.error("", e);
			}
		}
		log.info("syncReportToMeix success! total:" + size);
	}

}
