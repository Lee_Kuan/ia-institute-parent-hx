package com.meix.institute.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.api.FsService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.core.Filepath;
import com.meix.institute.util.DateUtil;

/**
 * 文件系统默认实现
 * 
 * @author xueyc
 *
 */
@Service
public class FsServiceImpl implements FsService {
    
    @Autowired
    private CommonServerProperties secretProperty;
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Override
    public String write(String filename, InputStream is) {
        Filepath folder = getFolder();
        File file = createFile(folder.getPath(), filename);
        
        logger.info("write file to {}", file.getAbsolutePath());
        try {
            OutputStream output = new FileOutputStream(file);
            IOUtils.copy(is, output);
            output.flush();
            output.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return getFilepath(folder.getPubPath(), filename);
    }

    @Override
    public String write(String filename, byte[] bytes) {
    	Filepath folder = getFolder();
        File file = createFile(folder.getPath(), filename);
        
        logger.info("write file to {}", file.getAbsolutePath());
        try {
            OutputStream output = new FileOutputStream(file);
            output.write(bytes);
            output.flush();
            output.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return getFilepath(folder.getPubPath(), filename);
    }

    @Override
    public InputStream getAsInputStream(String filepath) {
        File file = getFile(filepath);
        
        logger.info("get file from {}", file.getAbsolutePath());
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] getAsInputBytes(String filepath) {
        File file = getFile(filepath);
        
        logger.info("get file from {}", file.getAbsolutePath());
        try {
            InputStream is = new FileInputStream(file);
            return IOUtils.toByteArray(is);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Filepath getFolder() {
    	Filepath filePath = new Filepath();
        String dir = DateUtil.dateToStr(new Date(), "yyyy/MM/dd/");
        String dirPath = getBaseDir() + dir;
        File folder = new File(dirPath);
        filePath.setPath(dirPath);
        filePath.setPubPath(dir);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return filePath;
    }

    private String getFilepath(String folder, String filename) {
        return folder + filename;
    }

    private File createFile(String folder, String filename) {
        return new File(folder, filename);
    }

    private File getFile(String filepath) {
        return new File(getBaseDir(), filepath);
    }

    private String getBaseDir() {
        
        String baseDir = secretProperty.getFileDir();
        if (baseDir == null) {
            baseDir = System.getProperty("java.io.tmpdir");
        }
        if (!baseDir.endsWith("/")) {
            baseDir += "/";
        }
        return baseDir;
    }

    @Override
    public File createTempFile(String filename, InputStream is) {
        
        String fileTmpdir = secretProperty.getFileTmpdir();
        if (!fileTmpdir.endsWith("/")) {
            fileTmpdir += "/";
        }
        File file = new File(fileTmpdir + filename);
        
        logger.info("write file to {}", file.getAbsolutePath());
        try {
            OutputStream output = new FileOutputStream(file);
            IOUtils.copy(is, output);
            output.flush();
            output.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }

    @Override
    public File createTempFile(String filename, byte[] bytes) {
        String fileTmpdir = secretProperty.getFileTmpdir();
        if (!fileTmpdir.endsWith("/")) {
            fileTmpdir += "/";
        }
        File file = new File(fileTmpdir + filename);        
        logger.info("write file to {}", file.getAbsolutePath());
        try {
            OutputStream output = new FileOutputStream(file);
            output.write(bytes);
            output.flush();
            output.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }

}
