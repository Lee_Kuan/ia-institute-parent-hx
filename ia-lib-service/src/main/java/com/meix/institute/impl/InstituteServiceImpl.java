package com.meix.institute.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.BaseService;
import com.meix.institute.api.IInstituteDao;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IInstituteService;
import com.meix.institute.util.MessageUtil;
import com.meix.institute.vo.company.CompanyClueInfo;
import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.company.CompanyWechatConfig;
import com.meix.institute.vo.user.InsCustomer;
import com.meix.institute.vo.user.InsMeixUser;
import com.meix.institute.vo.user.InsSaler;
import com.meix.institute.vo.user.UserInfo;

/**
 * Created by zenghao on 2019/9/4.
 */
@Service
public class InstituteServiceImpl extends BaseService implements IInstituteService {
	protected final Logger log = LoggerFactory.getLogger(InstituteServiceImpl.class);
	@Autowired
	private IInstituteDao instituteDaoImpl;
	@Autowired
	private IInstituteMessagePushService insMessagePushService;

	@Override
	public int saveInsMeixUser(InsMeixUser insMeixUser) {
		return instituteDaoImpl.saveInsMeixUser(insMeixUser);
	}

	@Override
	public int updateInsMeixUser(InsMeixUser insMeixUser) {
		return instituteDaoImpl.updateInsMeixUser(insMeixUser);
	}

	@Override
	public InsMeixUser getExistInsMeixUser(long companyCode, long customerCompanyCode) {
		return instituteDaoImpl.getExistInsMeixUser(companyCode, customerCompanyCode);
	}

	@Override
	public List<CompanyWechatConfig> getWechatConfig() {
		return instituteDaoImpl.getWechatConfig();
	}

	@Override
	public CompanyWechatConfig getWechatConfigByAppType(String appType) {
		return instituteDaoImpl.getWechatConfigByAppType(appType);
	}

	@Override
	public CompanyWechatConfig getWechatConfigByAppId(String appId) {
		return instituteDaoImpl.getWechatConfigByAppId(appId);
	}

	@Override
	public CompanyWechatConfig getSPConfigByCompanyCode(long companyCode) {
		return instituteDaoImpl.getSPConfigByCompanyCode(companyCode);
	}

	@Override
	public void sendNewClueNotice(CompanyClueInfo companyClueInfo) {
		CompanyInfoVo companyInfoCache = companySerciveImpl.getCompanyInfoById(companyClueInfo.getCompanyCode());
		if (companyInfoCache == null) {
			return;
		}
		CompanyWechatConfig wechatConfig = instituteDaoImpl.getWechatConfigByCompanyCode(companyClueInfo.getCompanyCode());
		if (wechatConfig == null) {
			return;
		}
		UserInfo userInfo = userServiceImpl.getUserInfo(companyClueInfo.getUid(), null, null);
		if (userInfo == null) {
			return;
		}
		InsSaler insSaler = insMessagePushService.getInsSaler(companyInfoCache.getId(), userInfo.getMobile(), userInfo.getId());
		if (insSaler == null) {
			return;
		}
		String smsContent = MessageUtil.getAssignSalerNotice(userInfo, companyClueInfo.getClueType(), true);
		String wxContent = smsContent;
		String wxTitle = "您有新的商机！";
		String emailSubject = "您有新的商机！";
		insMessagePushService.sendNotice2Saler(insSaler, companyInfoCache, wechatConfig, smsContent, wxContent, wxTitle, emailSubject);
	}

	@Override
	public List<InsCustomer> getCustomersBySaler(long salerUid, int currentPage, int showNum) {
		return instituteDaoImpl.getCustomersBySaler(salerUid, currentPage, showNum);
	}

	@Override
	public int getCustomerCountBySaler(long salerUid) {
		return instituteDaoImpl.getCustomerCountBySaler(salerUid);
	}

    @Override
    public CompanyWechatConfig getWechatConfigByAppTypeAndClientType(String appType, Integer clientType) {
        return instituteDaoImpl.getWechatConfigByAppTypeAndClientType(appType, clientType);
    }

}
