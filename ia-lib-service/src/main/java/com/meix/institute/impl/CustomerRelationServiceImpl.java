package com.meix.institute.impl;

import com.meix.institute.api.ICustomerRelationService;
import com.meix.institute.entity.CustomerRelation;
import com.meix.institute.mapper.CustomerRelationMapper;
import com.meix.institute.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;

/**
 * Created by zenghao on 2019/11/26.
 */
@Service
public class CustomerRelationServiceImpl implements ICustomerRelationService {

	@Autowired
	private CustomerRelationMapper customerRelationMapper;

	@Override
	public CustomerRelation saveCustomerRelation(long companyCode, String mobile) {
		CustomerRelation customerRelation = new CustomerRelation();
		customerRelation.setCompanyCode(companyCode);
		customerRelation.setMobile(mobile);
		customerRelation.setUuid(StringUtils.getId24());
		Date date = new Date();
		customerRelation.setCreatedAt(date);
		customerRelation.setUpdatedAt(date);
		customerRelationMapper.insert(customerRelation);
		return customerRelation;
	}

	@Override
	public CustomerRelation getCustomerRelationByMobile(String mobile) {
		Example example = new Example(CustomerRelation.class);
		example.createCriteria().andEqualTo("mobile", mobile);
		return customerRelationMapper.selectOneByExample(example);
	}

	@Override
	public CustomerRelation getCustomerRelationByUuid(String uuid) {
		Example example = new Example(CustomerRelation.class);
		example.createCriteria().andEqualTo("uuid", uuid);
		return customerRelationMapper.selectOneByExample(example);
	}
}
