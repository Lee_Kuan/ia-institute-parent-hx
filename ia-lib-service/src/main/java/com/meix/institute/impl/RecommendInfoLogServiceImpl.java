package com.meix.institute.impl;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.api.IRecommendInfoLogService;
import com.meix.institute.entity.RecommenedInfoLog;
import com.meix.institute.mapper.RecommenedInfoLogMapper;

/**
 * Created by yingw on 2019/7/24.
 */
@Service
public class RecommendInfoLogServiceImpl implements IRecommendInfoLogService {
	private static Logger log = LoggerFactory.getLogger(RecommendInfoLogServiceImpl.class);

	@Autowired
	private RecommenedInfoLogMapper recommendInfoLogMapper;

	@PostConstruct
	public void init() {
		log.info("service init!!!");
	}

	@Override
	public void saveRecommendInfoLog(RecommenedInfoLog recommendInfoLog) {
//		Example example = new Example(RecommenedInfoLog.class);
		recommendInfoLogMapper.insert(recommendInfoLog);
	}

	@Override
	public RecommenedInfoLog findRecommendInfoLog(long id) {
//		RecommenedInfoLog recommendInfoLog = recommendInfoLogMapper.selectByPrimaryKey(id);
//		return recommendInfoLog;

		RecommenedInfoLog recommenedInfoLog = recommendInfoLogMapper.getById(id);
		return recommenedInfoLog;

//		return null;
	}
}
