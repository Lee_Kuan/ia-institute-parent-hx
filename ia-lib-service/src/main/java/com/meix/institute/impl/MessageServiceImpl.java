package com.meix.institute.impl;

import com.meix.institute.BaseService;
import com.meix.institute.api.IMessageDao;
import com.meix.institute.api.IMessageService;
import com.meix.institute.response.MeetingMessagePo;
import com.meix.institute.util.Des;
import com.meix.institute.util.OssResourceUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.vo.message.MeetingMessageVo;
import com.meix.institute.vo.user.UserInfo;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/9/26.
 */
@Service
public class MessageServiceImpl extends BaseService implements IMessageService {
	protected final Logger log = LoggerFactory.getLogger(MessageServiceImpl.class);
	@Autowired
	private IMessageDao messageDaoImpl;
	@Autowired(required = false)
	private JavaMailSenderImpl mailSender;

	@Override
	public Map<String, Object> saveMeetingMessage(MeetingMessageVo mmv) {
		messageDaoImpl.saveMeetingMessageDetail(mmv);
		messageDaoImpl.saveMeetingMessage(mmv);

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("id", mmv.getId());

		return resultMap;
	}

	@Override
	public List<MeetingMessagePo> getMeetingMessages(long uid, long activityId,
	                                                 long speakerId, long messageId, int messageFlag, long version, int showNum) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		List<MeetingMessageVo> list = messageDaoImpl.getMeetingMessages(uid, activityId, speakerId, messageId, messageFlag, showNum);
		MeetingMessagePo po = null;
		String message = null;
		//缓存用户信息
		Map<Long, UserInfo> userMap = new HashMap<>();
		UserInfo user = null;
		List<MeetingMessagePo> result = new ArrayList<>();
		for (MeetingMessageVo vo : list) {
			po = new MeetingMessagePo();
			//赋值
			PropertyUtils.copyProperties(po, vo);

			//解码消息
			message = Des.decryptBASE64AsString(vo.getMessage());
			po.setMessage(message);
			//获取发送者信息
			if (userMap.containsKey(po.getSender())) {
				user = userMap.get(po.getSender());
			} else {
				user = getUserInfo(po.getSender());
				userMap.put(user.getId(), user);
			}
			String headImage = user.getWechatHeadUrl();
			if (StringUtil.isBlank(headImage)) {
				headImage = OssResourceUtil.getHeadImage(user.getHeadImageUrl());
			}
			po.setSenderImageUrl(headImage);
			po.setSenderName(user.getUserName());
			po.setSenderCompany(user.getCompanyAbbr());

			result.add(po);
		}
		return result;
	}

	@Override
	public boolean sendEmailWithResult(String sendTo, String subject, String content, boolean html) {
		if (mailSender == null) {
			log.info("没有配置邮箱信息！！！无法发送邮件");
			return false;
		}
		MimeMessageHelper mimeMessageHelper;
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");
			mimeMessageHelper.setFrom(mailSender.getUsername());
			mimeMessageHelper.setTo(sendTo);
			mimeMessageHelper.setSubject(subject);
			mimeMessageHelper.setText(content, html);
			mailSender.send(mimeMessage);
			return true;
		} catch (Exception e) {
			log.error("发送邮件失败");
			log.error("", e);
			return false;
		}
	}

	@Override
	public String sendEmailWithResult(String sendTo, String subject, String content) {
		if (mailSender == null) {
			log.info("没有配置邮箱信息！！！无法发送邮件");
			return "没有配置邮箱信息！！！无法发送邮件";
		}
		MimeMessageHelper mimeMessageHelper;
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");
			mimeMessageHelper.setFrom(mailSender.getUsername());
			mimeMessageHelper.setTo(sendTo);
			mimeMessageHelper.setSubject(subject);
			mimeMessageHelper.setText(content, true);
			mailSender.send(mimeMessage);
			return null;
		} catch (Exception e) {
			log.error("发送邮件失败");
			log.error("", e);
			return e.getMessage();
		}
	}
}
