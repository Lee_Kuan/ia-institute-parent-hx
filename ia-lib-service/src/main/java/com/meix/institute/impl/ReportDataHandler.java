package com.meix.institute.impl;

import com.meix.institute.annotation.HandleDataType;
import com.meix.institute.api.DataHandler;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.vo.third.HandleResult;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by zenghao on 2020/4/8.
 */
@Service
@HandleDataType(MeixConstants.USER_YB)
public class ReportDataHandler implements DataHandler {
	private Logger log = LoggerFactory.getLogger(ReportDataHandler.class);

	@Override
	public HandleResult handleData(String action, JSONObject jsonObject) {
		log.info("同步参数：{}", GsonUtil.obj2Json(jsonObject));
		return null;
	}
}
