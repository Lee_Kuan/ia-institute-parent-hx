package com.meix.institute.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.api.IActivityService;
import com.meix.institute.api.IReportService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsActivityPerson;
import com.meix.institute.excel.BaseInfoExcel;
import com.meix.institute.excel.CompanyClueExcel;
import com.meix.institute.excel.DataRecordStatExcel;
import com.meix.institute.excel.ExportComment;
import com.meix.institute.excel.ExportCustomer;
import com.meix.institute.excel.ExportGoldStock;
import com.meix.institute.excel.ExportJoinPerson;
import com.meix.institute.excel.ExportWhiteUser;
import com.meix.institute.excel.UserRecordStatExcel;
import com.meix.institute.excel.common.ExcelExportUtil;
import com.meix.institute.excel.common.MeixExcelExportServer;
import com.meix.institute.excel.common.MeixExportParams;
import com.meix.institute.util.StringUtil;
import com.meix.institute.vo.CommentVo;
import com.meix.institute.vo.activity.ActivityAnalystVo;
import com.meix.institute.vo.activity.InsActivityVo;
import com.meix.institute.vo.company.CompanyClueInfo;
import com.meix.institute.vo.report.ReportAuthorVo;
import com.meix.institute.vo.report.ReportDetailVo;
import com.meix.institute.vo.stat.DataRecordStatVo;
import com.meix.institute.vo.stock.MonthlyGoldStockVo;
import com.meix.institute.vo.user.UserRecordStatVo;

import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;

/**
 * Created by zenghao on 2019/4/23.
 */
@Service
public class ExcelExportService {
	private final Logger log = LoggerFactory.getLogger(ExcelExportService.class);

//	private static final byte[] RGB_YELLOW = {(byte) 254, (byte) 193, 69};
//	private static final byte[] RGB_RED = {(byte) 253, 103, 90};
//	private static final byte[] RGB_BLUE = {65, (byte) 163, (byte) 255};

	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@Autowired
	private IReportService reportServiceImpl;
	@Autowired
	private IActivityService activityServiceImpl;

	public Workbook exportDataRecordStatExcel(List<DataRecordStatVo> dataList, boolean closeExcel) {
		try {
			if (dataList == null) {
				return null;
			}
			long start = System.currentTimeMillis();
			String sheetName = "数据总览";
			List<DataRecordStatExcel> excelList = new ArrayList<>();
			for (DataRecordStatVo info : dataList) {
				String dataType = "";
				if (info.getDataType() == MeixConstants.USER_YB) {
					dataType = "研报";
				} else {
					dataType = MeixConstants.getCNActivityType(info.getActivityType());
				}
				DataRecordStatExcel excel = new DataRecordStatExcel();
				excel.setDataType(dataType);
				excel.setAuthorName(info.getAuthorName());
				excel.setTitle(info.getTitle());
				excel.setReadNum(String.valueOf(info.getReadNum()));
				excel.setShareNum(String.valueOf(info.getShareNum()));
				String time = null;
				if (info.getDuration() < 3600) {
					time = info.getDuration() / 60 + "分";
				} else {
					time = info.getDuration() / 3600 + "小时";
				}
				excel.setDuration(time);
				excel.setStartTime(info.getStartTime());
				excelList.add(excel);
			}
			ExportParams exportParams = new MeixExportParams("数据总览", sheetName);
			exportParams.setType(ExcelType.XSSF);
			Workbook wb = ExcelExportUtil.exportBigExcel(exportParams, DataRecordStatExcel.class, excelList);
			excelList.clear();
			if (closeExcel) {
				ExcelExportUtil.closeExportBigExcel();
			}
			log.info("导出活动阅读分享统计耗时{}ms", (System.currentTimeMillis() - start));
			return wb;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	/**
	 * 活动阅读分享统计导出
	 *
	 * @return
	 */
	@Deprecated
	public Workbook getDataRecordStatExcel(List<DataRecordStatVo> dataList) {
		try {
			if (dataList == null) {
				return null;
			}
			long start = System.currentTimeMillis();
			Workbook wb = new XSSFWorkbook();
			MeixExcelExportServer server = new MeixExcelExportServer();
			String sheetName = "数据总览";
			List<DataRecordStatExcel> excelList = new ArrayList<>();
			for (DataRecordStatVo info : dataList) {
				String dataType = "";
				if (info.getDataType() == MeixConstants.USER_YB) {
					dataType = "研报";
				} else {
					dataType = MeixConstants.getCNActivityType(info.getActivityType());
				}
				DataRecordStatExcel excel = new DataRecordStatExcel();
				excel.setDataType(dataType);
				excel.setAuthorName(info.getAuthorName());
				excel.setTitle(info.getTitle());
				excel.setReadNum(String.valueOf(info.getReadNum()));
				excel.setShareNum(String.valueOf(info.getShareNum()));
				String time = null;
				if (info.getDuration() < 3600) {
					time = info.getDuration() / 60 + "分";
				} else {
					time = info.getDuration() / 3600 + "小时";
				}
				excel.setDuration(time);
				excelList.add(excel);
			}
			ExportParams exportParams = new MeixExportParams("数据总览", sheetName);
			exportParams.setType(ExcelType.XSSF);
			server.insertDataToSheet(wb, exportParams, DataRecordStatExcel.class, excelList);
			log.info("导出活动阅读分享统计耗时{}ms", (System.currentTimeMillis() - start));
			return wb;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	/**
	 * 用户阅读分享行为导出
	 *
	 * @return
	 */
	public Workbook getUserRecordStatExcel(List<UserRecordStatVo> dataList) {
		try {
			if (dataList == null) {
				return null;
			}
			long start = System.currentTimeMillis();
			Workbook wb = new SXSSFWorkbook();
			MeixExcelExportServer server = new MeixExcelExportServer();
			String sheetName = "效果分析报表";

			int intervalLine = 0;
			if (dataList.size() > 0) {
				intervalLine = 1;
				String contentType = "";
				String title = "";
				String authorName = "";

				//TODO 查询内容标题、作者等信息
				int dataType = dataList.get(0).getDataType();
				long dataId = dataList.get(0).getDataId();
				if (dataType == MeixConstants.USER_YB) {
					contentType = "研报";
					ReportDetailVo report = reportServiceImpl.getReportDetail(dataId, true);
					if (report != null) {
						title = report.getTitle();
						List<ReportAuthorVo> authorList = report.getAuthorList();
						if (authorList != null && authorList.size() > 0) {
							for (ReportAuthorVo author : authorList) {
								if (StringUtil.isNotEmpty(author.getAuthorName())) {
									if (StringUtil.isNotEmpty(authorName)) {
										authorName += "、";
									}
									authorName += author.getAuthorName();
								}
							}
						}
					}
				} else if (dataType == MeixConstants.USER_HD) {
					InsActivityVo activity = activityServiceImpl.getActivityDetail(0, dataId, 0);
					if (activity != null) {
						contentType = MeixConstants.getCNActivityType(activity.getActivityType() == null ? 0 : activity.getActivityType());
						title = activity.getTitle();
//						authorName = detailVo.getAuthorName();
						List<ActivityAnalystVo> authorList = activity.getAnalyst();
						if (authorList != null && authorList.size() > 0) {
							for (ActivityAnalystVo author : authorList) {
								if (StringUtil.isNotEmpty(author.getName())) {
									if (StringUtil.isNotEmpty(authorName)) {
										authorName += "、";
									}
									authorName += author.getName();
								}
							}
						}
					}
				}

				List<BaseInfoExcel> baseList = new ArrayList<>();
				BaseInfoExcel name = new BaseInfoExcel();
				name.setAttr("类型");
				name.setValue(contentType);
				baseList.add(name);

				BaseInfoExcel author = new BaseInfoExcel();
				author.setAttr("标题");
				author.setValue(title);
				baseList.add(author);

				BaseInfoExcel industry = new BaseInfoExcel();
				industry.setAttr("作者");
				industry.setValue(authorName);
				baseList.add(industry);
				ExportParams baseInfo = new MeixExportParams("基本情况", sheetName, false, ExcelType.XSSF);
				server.insertDataToSheet(wb, baseInfo, BaseInfoExcel.class, baseList);
			}

			List<UserRecordStatExcel> excelList = new ArrayList<>();
			for (UserRecordStatVo info : dataList) {
				UserRecordStatExcel excel = new UserRecordStatExcel();
				excel.setUserName(info.getUserName());
				excel.setMobile(info.getMobile());
				excel.setCompanyAbbr(info.getCompanyAbbr());
				excel.setPosition(info.getPosition());
				excel.setCreateTime(info.getCreateTime());
				excel.setReadNum(String.valueOf(info.getReadNum()));
				excel.setShareNum(String.valueOf(info.getShareNum()));
				String time = null;
				if (info.getDuration() < 3600) {
					time = info.getDuration() / 60 + "分";
				} else {
					time = info.getDuration() / 3600 + "小时";
				}
				excel.setDuration(time);
				excelList.add(excel);
			}
			ExportParams exportParams = new MeixExportParams("效果分析报表", sheetName);
			exportParams.setType(ExcelType.XSSF);
			server.insertDataToSheet(wb, exportParams, UserRecordStatExcel.class, excelList, intervalLine);
			log.info("导出用户阅读分享行为耗时{}ms", (System.currentTimeMillis() - start));
			return wb;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	public Workbook exportClueExcel(List<CompanyClueInfo> dataList, boolean closeExcel) {
		try {
			if (dataList == null) {
				return null;
			}
			long start = System.currentTimeMillis();
			String sheetName = "线索池";
			List<CompanyClueExcel> excelList = new ArrayList<>();
			for (CompanyClueInfo info : dataList) {
				String customerType = "潜在用户";
				switch (info.getCustomerType()) {
					case 0:
						customerType = "白名单用户";
						break;
					case 1:
						customerType = "潜在用户";
						break;
				}
				CompanyClueExcel excel = new CompanyClueExcel();
				excel.setCustomerCompanyAbbr(info.getCustomerCompanyAbbr());
				excel.setClueName(info.getClueName());
				excel.setCustomerType(customerType);
				excel.setSalerName(info.getSalerName());
				excel.setCreateTime(info.getCreateTime());
				excelList.add(excel);
			}
			ExportParams exportParams = new MeixExportParams("线索池", sheetName);
			exportParams.setType(ExcelType.XSSF);
			Workbook wb = ExcelExportUtil.exportBigExcel(exportParams, CompanyClueExcel.class, excelList);
			excelList.clear();
			if (closeExcel) {
				ExcelExportUtil.closeExportBigExcel();
			}
			log.info("导出线索池耗时{}ms", (System.currentTimeMillis() - start));
			return wb;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	/**
	 * 线索池导出
	 *
	 * @return
	 */
	@Deprecated
	public Workbook getClueExcel(List<CompanyClueInfo> dataList) {
		try {
			if (dataList == null) {
				return null;
			}
			long start = System.currentTimeMillis();
			Workbook wb = new XSSFWorkbook();
			MeixExcelExportServer server = new MeixExcelExportServer();
			String sheetName = "线索池";
			List<CompanyClueExcel> excelList = new ArrayList<>();
			for (CompanyClueInfo info : dataList) {
				String customerType = "潜在用户";
				switch (info.getCustomerType()) {
					case 0:
						customerType = "白名单用户";
						break;
					case 1:
						customerType = "潜在用户";
						break;
				}
				CompanyClueExcel excel = new CompanyClueExcel();
				excel.setCustomerCompanyAbbr(info.getCustomerCompanyAbbr());
				excel.setClueName(info.getClueName());
				excel.setCustomerType(customerType);
				excel.setSalerName(info.getSalerName());
				excel.setCreateTime(info.getCreateTime());
				excelList.add(excel);
			}
			ExportParams exportParams = new MeixExportParams("线索池", sheetName);
			exportParams.setType(ExcelType.XSSF);
			server.insertDataToSheet(wb, exportParams, CompanyClueExcel.class, excelList);
			log.info("导出线索池耗时{}ms", (System.currentTimeMillis() - start));
			return wb;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	public Workbook getGoldStockMonthExcel(List<MonthlyGoldStockVo> list) {
		try {
			if (list == null) {
				return null;
			}
			long start = System.currentTimeMillis();
			Workbook wb = new XSSFWorkbook();
			MeixExcelExportServer server = new MeixExcelExportServer();
			String sheetName = "月度金股";
			List<ExportGoldStock> excelList = new ArrayList<>();
			for (MonthlyGoldStockVo info : list) {
				ExportGoldStock excel = new ExportGoldStock();
				excel.setSecuMain(info.getSecuAbbr() + info.getSecuCode());
				excel.setRecPerson(info.getRecPersonName());
				excel.setRecDesc(info.getRecDesc());
				excel.setMonthRate(info.getMonthRate().multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP) + "%");
				excel.setSelfUserNum(info.getSelfUserNum());
				excel.setEditTime(dateFormat.format(info.getUpdatedAt()));
				excelList.add(excel);
			}
			ExportParams exportParams = new MeixExportParams("月度金股", sheetName);
			exportParams.setType(ExcelType.XSSF);
			server.insertDataToSheet(wb, exportParams, ExportGoldStock.class, excelList);
			log.info("导出月度金股耗时{}ms", (System.currentTimeMillis() - start));
			return wb;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	public Workbook getJoinPersonExcel(String title, List<InsActivityPerson> list) {
		try {
			if (list == null) {
				return null;
			}
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			long start = System.currentTimeMillis();
			Workbook wb = new XSSFWorkbook();
			MeixExcelExportServer server = new MeixExcelExportServer();
			List<ExportJoinPerson> excelList = new ArrayList<>();
			for (InsActivityPerson info : list) {
				ExportJoinPerson excel = new ExportJoinPerson();
				excel.setUserName(info.getUserName());
				excel.setOrgName(info.getCompanyName());
				excel.setPosition(info.getPosition());
				excel.setMobile(info.getMobile());
				excel.setSalerName(info.getSalerName());
				excel.setJoinTime(df.format(info.getCreatedAt()));
				excel.setStatus(getStatusName(info.getStatus()));
				excelList.add(excel);
			}
			ExportParams exportParams = new MeixExportParams(title, title);
//			ExportParams exportParams = new ExportParams(title, title);
			exportParams.setType(ExcelType.XSSF);
			server.insertDataToSheet(wb, exportParams, ExportJoinPerson.class, excelList);
			log.info("导出活动报名名单：{}耗时{}ms", title, (System.currentTimeMillis() - start));
			return wb;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	private String getStatusName(int status) {
		switch (status) {
			case 0:
				return "未审核";
			case 1:
				return "已通过";
			case 2:
				return "已拒绝";
			case 3:
				return "已撤销";
			default:
				return "未知";
		}
	}

	public Workbook getCustomerExcel(String title, List<Map<String, Object>> list) {
		try {
			if (list == null) {
				return null;
			}
			long start = System.currentTimeMillis();
			Workbook wb = new XSSFWorkbook();
			MeixExcelExportServer server = new MeixExcelExportServer();
			List<ExportCustomer> excelList = new ArrayList<>();
			for (Map<String, Object> info : list) {
				ExportCustomer excel = new ExportCustomer();

				excel.setUserName(MapUtils.getString(info, "userName"));
				excel.setMobile(MapUtils.getString(info, "mobile"));
				excel.setLoginDays(MapUtils.getIntValue(info, "loginDays"));
				String dateStr = MapUtils.getString(info, "firstLoginTime");
				if (StringUtils.isNotBlank(dateStr) && dateStr.length() > 16) {
					dateStr = dateStr.substring(0, 16);
				}
				excel.setFirstLoginTime(dateStr);
				dateStr = MapUtils.getString(info, "lastLoginTime");
				if (StringUtils.isNotBlank(dateStr) && dateStr.length() > 16) {
					dateStr = dateStr.substring(0, 16);
				}
				excel.setLastLoginTime(dateStr);
				excelList.add(excel);
			}
			ExportParams exportParams = new MeixExportParams(title, title);
//			ExportParams exportParams = new ExportParams(title, title);
			exportParams.setType(ExcelType.XSSF);
			server.insertDataToSheet(wb, exportParams, ExportCustomer.class, excelList);
			log.info("导出游客信息：{}（{}）耗时{}ms", title, list.size(), (System.currentTimeMillis() - start));
			return wb;
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
	}

	public Workbook getCommentExcel(String excelName, List<CommentVo> list) {
		try {
			if (list == null) {
				return null;
			}
			long start = System.currentTimeMillis();
			Workbook wb = new XSSFWorkbook();
			MeixExcelExportServer server = new MeixExcelExportServer();
			List<ExportComment> excelList = new ArrayList<>();
			for (CommentVo info : list) {
				ExportComment excel = new ExportComment();
				excel.setComment(info.getCommentContent());
				excel.setUserName(info.getAuthorName());
				excel.setTime(info.getTime());
				excel.setStatus(getStatusName(info.getStatus()));
				excelList.add(excel);
			}
			ExportParams exportParams = new MeixExportParams(excelName, excelName);
//			ExportParams exportParams = new ExportParams(excelName, excelName);
			exportParams.setType(ExcelType.XSSF);
			server.insertDataToSheet(wb, exportParams, ExportComment.class, excelList);
			log.info("导出活动报名名单：{}耗时{}ms", excelName, (System.currentTimeMillis() - start));
			return wb;
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	public Workbook getWhiteUserExcel(String excelName, List<Map<String, Object>> list) {
		try {
			if (list == null) {
				return null;
			}
			long start = System.currentTimeMillis();
			Workbook wb = new XSSFWorkbook();
			MeixExcelExportServer server = new MeixExcelExportServer();
			List<ExportWhiteUser> excelList = new ArrayList<>();
			for (Map<String, Object> info : list) {
				ExportWhiteUser excel = new ExportWhiteUser();
				excel.setMobile(MapUtils.getString(info, "mobile"));
				excel.setOrgName(MapUtils.getString(info, "companyName"));
				excel.setPosition(MapUtils.getString(info, "position"));
				excel.setSalerName(MapUtils.getString(info, "salerName"));
				excel.setUserName(MapUtils.getString(info, "userName"));
				excelList.add(excel);
			}
			ExportParams exportParams = new MeixExportParams(excelName, excelName);
//			ExportParams exportParams = new ExportParams(excelName, excelName);
			exportParams.setType(ExcelType.XSSF);
			server.insertDataToSheet(wb, exportParams, ExportWhiteUser.class, excelList);
			log.info("导出白名单：{}耗时{}ms", excelName, (System.currentTimeMillis() - start));
			return wb;
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}
}
