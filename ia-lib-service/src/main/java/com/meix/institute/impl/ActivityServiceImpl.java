package com.meix.institute.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.beanutils.converters.StringConverter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.http.util.TextUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meix.institute.BaseService;
import com.meix.institute.api.IActivityService;
import com.meix.institute.api.ICompanyService;
import com.meix.institute.api.IConstDao;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.IInsWhiteListService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.api.ISecumainService;
import com.meix.institute.api.ISelfStockDao;
import com.meix.institute.api.IUserService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.constant.SyncConstants;
import com.meix.institute.entity.InsActivity;
import com.meix.institute.entity.InsActivityAnalyst;
import com.meix.institute.entity.InsActivityContact;
import com.meix.institute.entity.InsActivityIssueType;
import com.meix.institute.entity.InsActivityJoin;
import com.meix.institute.entity.InsActivityPerson;
import com.meix.institute.entity.InsActivityTarget;
import com.meix.institute.entity.InsCompanyInstituteUserActivity;
import com.meix.institute.entity.InsUser;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.impl.cache.ServiceCache;
import com.meix.institute.mapper.ActivityGenseeMapper;
import com.meix.institute.mapper.ActivityMapper;
import com.meix.institute.mapper.InsActivityAnalystMapper;
import com.meix.institute.mapper.InsActivityContactMapper;
import com.meix.institute.mapper.InsActivityIssueTypeMapper;
import com.meix.institute.mapper.InsActivityJoinMapper;
import com.meix.institute.mapper.InsActivityMapper;
import com.meix.institute.mapper.InsActivityPersonMapper;
import com.meix.institute.mapper.InsActivityTargetMapper;
import com.meix.institute.mapper.InsUserMapper;
import com.meix.institute.mapper.SecuMapper;
import com.meix.institute.pojo.InsUserInfo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.OssResourceUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.GroupPowerVo;
import com.meix.institute.vo.activity.Activity;
import com.meix.institute.vo.activity.ActivityAnalystVo;
import com.meix.institute.vo.activity.ActivityContactVo;
import com.meix.institute.vo.activity.ActivityDayNumberVo;
import com.meix.institute.vo.activity.ActivityListSearchVo;
import com.meix.institute.vo.activity.ActivityTargetVo;
import com.meix.institute.vo.activity.ActivityVo;
import com.meix.institute.vo.activity.InsActivityVo;
import com.meix.institute.vo.activity.OrgOrUserVo;
import com.meix.institute.vo.activity.gensee.ActivityGenseeRecord;
import com.meix.institute.vo.activity.gensee.ActivityThirdJoinInfo;
import com.meix.institute.vo.cache.ConstantVo;
import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.gensee.GenseeVideo;
import com.meix.institute.vo.secu.SecuMainVo;
import com.meix.institute.vo.stock.SelfStockVo;
import com.meix.institute.vo.user.UserInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import tk.mybatis.mapper.entity.Example;

/**
 * @author likuan
 * @date 2019年8月2日15:33:26
 * @describe 活动
 */
@Transactional
@Service
public class ActivityServiceImpl extends BaseService implements IActivityService {

	protected final Logger log = LoggerFactory.getLogger(ActivityServiceImpl.class);
	@Autowired
	private InsActivityMapper activityMapper;
	@Autowired
	private InsActivityIssueTypeMapper issueTypeMapper;
	@Autowired
	private InsActivityAnalystMapper analystMapper;
	@Autowired
	private InsActivityContactMapper contactMapper;
	@Autowired
	private InsActivityPersonMapper personMapper;
	@Autowired
	private InsActivityTargetMapper targetMapper;
	@Autowired
	private InsActivityJoinMapper activityJoinMapper;
	@Autowired
	private ActivityGenseeMapper activityGenseeMapper;
	@Autowired
	private IInsUserService insUserService;
	@Autowired
	private ActivityMapper myActivityMapper;
	@Autowired
	private SecuMapper mySecuMapper;
	@Autowired
	private IUserService userService;
	@Autowired
	private ISecumainService secumainService;
	@Autowired
	private ISelfStockDao selfStockDaoImpl;
	@Autowired
	private ServiceCache serviceCache;
	@Autowired
	private IConstDao constDaoImpl;
	@Autowired
	private InsUserMapper userMapper;
	@Autowired
	private ICompanyService companyService;
	@Autowired
	private IMeixSyncService meixSyncService;
	@Autowired
	private IInstituteMessagePushService messagePushService;
	@Autowired
	private IInsWhiteListService whiteListService;
	@Autowired
	private ICustomerGroupService groupService;

	/**
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @Description: 保存活动
	 */
	@Override
	public long saveActivity(long resId, String uuid, InsActivity activity, GenseeVideo genseeVideo, JSONArray analyst, JSONArray industry,
	                         JSONArray secu, JSONArray contact, JSONArray issueType) {
		long activityId = activity.getId();

		if (activity.getActivityType() == MeixConstants.CHAT_MESSAGE_DHHY && activity.getIsArtificial() == 1) {
			activity.setActivityStatus(MeixConstants.ACTIVITY_STATUS_0);
		} else {
			activity.setActivityStatus(MeixConstants.ACTIVITY_STATUS_1);
		}
		if (activityId == 0) {
			// 新增的
			activityId = resId > 0 ? resId : StringUtils.getId(activity.getCompanyCode());
			activity.setId(activityId);
			activity.setCreatedAt(new Date());
			activity.setCreatedBy(uuid);
			activity.setUpdatedAt(new Date());
			activity.setUpdatedBy(uuid);
			if (activityMapper.insert(activity) < 1) {
				return 0;
			}
			if (activity.getActivityStatus() == MeixConstants.ACTIVITY_STATUS_1) {
				messagePushService.messagePush(activity.getCompanyCode(), activityId, InstituteMessagePushServiceImpl.CREATE_ACTIVITY, activity.getTitle(), activity.getActivityType(), 0, null);
			}
		} else {
			activity.setUpdatedAt(new Date());
			activity.setUpdatedBy(uuid);
			// 修改的
			if (activityMapper.updateByPrimaryKeySelective(activity) < 1) {
				return 0;
			}
			// 删除所有从表数据
			Example example = new Example(InsActivityAnalyst.class);
			example.createCriteria().andEqualTo("activityId", activityId);
			analystMapper.deleteByExample(example);
			example = new Example(InsActivityContact.class);
			example.createCriteria().andEqualTo("activityId", activityId);
			contactMapper.deleteByExample(example);
			example = new Example(InsActivityTarget.class);
			example.createCriteria().andEqualTo("activityId", activityId);
			targetMapper.deleteByExample(example);
			example = new Example(InsActivityIssueType.class);
			example.createCriteria().andEqualTo("activityId", activityId);
			issueTypeMapper.deleteByExample(example);
		}

		saveActivityGenseeVideo(activity.getId(), genseeVideo);

		if (analyst != null && analyst.size() > 0) {
			List<InsActivityAnalyst> analystList = new ArrayList<>();
			for (int i = 0; i < analyst.size(); i++) {
				InsUser insUser = insUserService.getUserInfo(analyst.getLong(i));
				if (insUser == null) {
					continue;
				}
				InsActivityAnalyst params = new InsActivityAnalyst();
				params.setActivityId(activityId);
				params.setUid(insUser.getId());
				params.setAnalystName(insUser.getUserName());

				params.setCreatedAt(new Date());
				params.setCreatedBy(uuid);
				params.setUpdatedAt(new Date());
				params.setUpdatedBy(uuid);

				analystList.add(params);
			}
			if (analystList.size() > 0) {
				analystMapper.insertList(analystList);
			}
		}
		//行业标签
		List<InsActivityTarget> targetList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(industry)) {
			for (int i = 0; i < industry.size(); i++) {
				InsActivityTarget params = new InsActivityTarget();
				params.setActivityId(activityId);
				params.setInnerCode(industry.getLong(i));

				params.setCreatedAt(new Date());
				params.setCreatedBy(uuid);
				params.setUpdatedAt(new Date());
				params.setUpdatedBy(uuid);
				targetList.add(params);
			}
		}
		//股票标签
		if (secu != null && secu.size() > 0) {
			for (int i = 0; i < secu.size(); i++) {
				InsActivityTarget params = new InsActivityTarget();
				params.setActivityId(activityId);
				params.setInnerCode(secu.getLong(i));

				params.setCreatedAt(new Date());
				params.setCreatedBy(uuid);
				params.setUpdatedAt(new Date());
				params.setUpdatedBy(uuid);
				targetList.add(params);
			}
		}
		//行业标签
		if (targetList.size() > 0) {
			targetMapper.insertList(targetList);
		}
		if (issueType != null && issueType.size() > 0) {
			List<InsActivityIssueType> issueList = new ArrayList<>();
			for (int i = 0; i < issueType.size(); i++) {
				InsActivityIssueType params = new InsActivityIssueType();
				params.setActivityId(activityId);
				params.setIssueType(issueType.getInt(i));

				params.setCreatedAt(new Date());
				params.setCreatedBy(uuid);
				params.setUpdatedAt(new Date());
				params.setUpdatedBy(uuid);
				issueList.add(params);
			}
			issueTypeMapper.insertList(issueList);
		}

		if (contact != null && contact.size() > 0) {
			List<InsActivityContact> contactList = new ArrayList<>();
			for (int i = 0; i < contact.size(); i++) {
				JSONObject obj = contact.getJSONObject(i);
				long id = MapUtils.getLong(obj, "contactId", 0L);
				String con = MapUtils.getString(obj, "contact", null);
				String mobile = MapUtils.getString(obj, "mobile", null);
				if (id == 0 && con == null && mobile == null) {
					continue;
				}
				InsActivityContact params = new InsActivityContact();
				params.setActivityId(activityId);
				params.setUid(id);
				params.setContact(con);
				params.setMobile(mobile);

				params.setCreatedAt(new Date());
				params.setCreatedBy(uuid);
				params.setUpdatedAt(new Date());
				params.setUpdatedBy(uuid);

				contactList.add(params);
			}
			contactMapper.insertList(contactList);
		}

		return activityId;
	}

	@Override
	public InsActivity getBaseActivityById(long dataId) {
		InsActivity activity = activityMapper.selectByPrimaryKey(dataId);
		return activity;
	}

	/**
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @Description: 活动详情
	 */
	@Override
	public InsActivityVo getActivityDetail(long uid, long activityId, int checkPermission) throws IllegalAccessException, InvocationTargetException {
		return getActivityDetail(uid, activityId, checkPermission, true);
	}

	@Override
	public InsActivityVo getActivityDetail(long uid, long activityId, int checkPermission, boolean getCustomerId) throws IllegalAccessException, InvocationTargetException {
		InsActivity activity = activityMapper.selectByPrimaryKey(activityId);

		InsActivityVo insActivityVo = new InsActivityVo();
		if (activity == null) {
			return insActivityVo;
		}
		ConvertUtils.register(new DateConverter(null), java.util.Date.class);
		ConvertUtils.register(new StringConverter(""), String.class);
		BeanUtils.copyProperties(insActivityVo, activity);

		if (insActivityVo.getCity() == null) {
			insActivityVo.setCity("");
		}
		if (insActivityVo.getAddress() == null) {
			insActivityVo.setAddress("");
		}

		//活动报名截止时间报错，为空是使用开始时间
		if (insActivityVo.getApplyDeadlineTime() == null) {
			insActivityVo.setApplyDeadlineTime(insActivityVo.getStartTime());
		}

		Example example = new Example(InsActivityIssueType.class);
		example.createCriteria().andEqualTo("activityId", activityId);
		if (uid == 0) {
			InsUser user = insUserService.getUserInfo(insActivityVo.getCreatedBy());
			if (user != null) {
				insActivityVo.setAuthorName(user.getUserName());
			} else {
				insActivityVo.setAuthorName("");
			}
	
			//设置发布渠道信息
			List<InsActivityIssueType> typeList = issueTypeMapper.selectByExample(example);
			if (typeList != null && typeList.size() > 0) {
				List<Integer> issueTypeList = new ArrayList<>(typeList.size());
				for (InsActivityIssueType type : typeList) {
					issueTypeList.add(type.getIssueType());
				}
				insActivityVo.setIssueTypeList(issueTypeList);
			}
		}
		/********   分析师      ********/
		List<InsActivityAnalyst> analystList = analystMapper.selectByExample(example);

		if (analystList == null) {
			analystList = new ArrayList<>();
		}

		ActivityAnalystVo notSysAnalyst = null;//非系统用户的分析师
		List<ActivityAnalystVo> analystVo = new ArrayList<>();
		for (InsActivityAnalyst analyst : analystList) {
			if (analyst.getUid() == 0) {
				if (notSysAnalyst == null) {
					notSysAnalyst = new ActivityAnalystVo();
					notSysAnalyst.setName(analyst.getAnalystName());
					notSysAnalyst.setCode(0);
				}
				continue;
			}
			ActivityAnalystVo vo = new ActivityAnalystVo();
			long customerId = 0;
			//内部用户id转为customer的id
			String imageUrl = null;
			long id = analyst.getUid();
			String position = "";
			String direction = "";
			InsUserInfo insUser = insUserService.selectByPrimaryKey(id);
			if (insUser != null) {
				String uuid = insUser.getUser();
				imageUrl = OssResourceUtil.getHeadImage(insUser.getHeadImageUrl());
				position = insUser.getPositionText();
				direction = insUser.getDirection();
				if (getCustomerId) {
					UserInfo userInfo = userService.getUserInfoByUuid(uuid);
					if (userInfo != null) {
						customerId = userInfo.getId();
						imageUrl = OssResourceUtil.getHeadImage(userInfo.getHeadImageUrl());
					}
				} else {
					customerId = id;
				}
			}
			if (customerId == 0) {
				if (notSysAnalyst == null) {
					notSysAnalyst = new ActivityAnalystVo();
					notSysAnalyst.setName(analyst.getAnalystName());
					notSysAnalyst.setCode(0);
					notSysAnalyst.setPosition(position);
					notSysAnalyst.setDirection(direction);
				}
				continue;
			}
			vo.setCode(customerId);
			vo.setName(insUser.getUserName());
			vo.setHeadImageUrl(imageUrl);
			vo.setHeadImg(imageUrl);
			vo.setPosition(position);
			vo.setDirection(direction);
			analystVo.add(vo);
		}
		if (uid > 0 && analystVo.size() > 0) {
			//第一个分析师是否关注标志
			long authorCode = analystVo.get(0).getCode();
			if (authorCode > 0) {
				boolean followFlag = userService.isFocusedPerson(uid, authorCode);
				analystVo.get(0).setFollowFlag(followFlag ? 1 : 0);
			}
		}

		//非每市用户的分析师放在分析师信息中
		if (analystVo.size() == 0 && notSysAnalyst != null) {
			analystVo.add(notSysAnalyst);
		}
		insActivityVo.setAnalyst(analystVo);

		/****** 上市公司、行业 ******/
		if (uid == 0) {
			List<SecuMainVo> secuList = mySecuMapper.getActivityTargetList(activityId);
			List<ActivityTargetVo> secuVo = new ArrayList<>();
			List<ActivityTargetVo> industryVo = new ArrayList<>();
			if (secuList != null && secuList.size() > 0) {
				for (SecuMainVo secu : secuList) {
					ActivityTargetVo vo = new ActivityTargetVo();
					vo.setIndustryCode(secu.getIndustryCode());
					vo.setInnerCode(secu.getInnerCode());
					vo.setSecuAbbr(secu.getSecuAbbr());
					vo.setSecuCode(secu.getSecuCode() + secu.getSuffix());
	//				vo.setRelartionType(getRelationType(secu));
					int relationType = getRelationType(secu);
					vo.setRelartionType(relationType);
					if (relationType == 1) {
						secuVo.add(vo);
					} else if (relationType == 2) {
						industryVo.add(vo);
					}
	
				}
	//			insActivityVo.setTargetList(secuVo);
			}
			insActivityVo.setSecuList(secuVo);
			insActivityVo.setIndustryList(industryVo);
		}
		Example personExample = new Example(InsActivityPerson.class);
		personExample.createCriteria().andEqualTo("activityId", activityId);
		long count = personMapper.selectCountByExample(personExample);
		insActivityVo.setAttendPersonCount(count);

		/*******  联系人、可接入电话  路演为券商联络人 *******/
		List<InsActivityContact> contactList = contactMapper.selectByExample(example);
		List<ActivityContactVo> contactVo = new ArrayList<>();
		if (contactList != null && contactList.size() > 0) {
			for (InsActivityContact contact : contactList) {
				long customerId = 0;
				//内部用户id转为customer的id
				long id = contact.getUid();
				InsUser insUser = insUserService.getUserInfo(id);
				if (insUser != null) {
					String uuid = insUser.getUser();
					UserInfo userInfo = userService.getUserInfoByUuid(uuid);
					if (userInfo != null) {
						customerId = userInfo.getId();
					}
				}
				ActivityContactVo vo = new ActivityContactVo();
				vo.setUid(customerId);
				vo.setContact(contact.getContact());
				vo.setMobile(contact.getMobile());
				contactVo.add(vo);
			}
		}
		insActivityVo.setContactList(contactVo);
		insActivityVo.setContact(contactVo);
		int isEnd = activity.getIsEnd() == null ? 0 : activity.getIsEnd();
		insActivityVo.setIsEnd((short) getActivityStatus(activity.getActivityType(), isEnd, isEnd, activity.getStartTime(), activity.getEndTime()));

		boolean hasLivePer = true;
		boolean hasReplayPer = true;
		if (uid > 0) {
			//是否参会 isJoin, status
			Map<String, Object> joinMap = myActivityMapper.getIsJoinActivity(uid, activityId);
			if (joinMap == null) {
				insActivityVo.setIsJoinIn(0);
				insActivityVo.setJoinStatus(0);
			} else {
				insActivityVo.setIsJoinIn(MapUtils.getIntValue(joinMap, "isJoin", 0));
				insActivityVo.setJoinStatus(MapUtils.getIntValue(joinMap, "status", 0));
			}
			UserInfo selector = getUserInfo(uid);

			//权限状态
//			int permission = myActivityMapper.getPermissionStatus(uid, activityId);
			int permission = getPermissionStatus(selector, activity);
			boolean isPub = false;
			// 2 白名单有所有权限
			if (permission == 2 || activity.getShare() == 0) {
				permission = 1;
				isPub = true;
			}
			List<GroupPowerVo> powerList = null;
			powerList = groupService.getResourceGroupPower(uid, activityId, MeixConstants.USER_HD, activity.getActivityType(), 0);
			insActivityVo.setActivityDesc("");
			hasLivePer = false;
			hasReplayPer = false;
			if (powerList != null && !powerList.isEmpty()) {
				for (GroupPowerVo tmp : powerList) {
					if (isPub || (tmp.getShare() == MeixConstants.SHARE_DETAIL && tmp.getPower() == 1)) { //详情权限
						permission = 1;
						insActivityVo.setActivityDesc(activity.getActivityDesc());
					}
					if (isPub || (tmp.getShare() == MeixConstants.SHARE_LIVE && tmp.getPower() == 1)) { // 直播权限
						permission = 1;
						hasLivePer = true;
					}
					if (isPub || (tmp.getShare() == MeixConstants.SHARE_REPLAY && tmp.getPower() == 1)) { // 回放权限
						permission = 1;
						hasReplayPer = true;
					}
					if (isPub) {
						tmp.setPower(1);
					}
				}
			}
			insActivityVo.setGroupPower(powerList == null ? new ArrayList<>() : powerList);
			insActivityVo.setPermission(permission);

		} else {
			List<Map<String, Object>> whiteList = myActivityMapper.getActivityWhiteList(activityId);
			insActivityVo.setShareList(whiteList);
			insActivityVo.setShareName(getShareDesc(insActivityVo.getShare()));
			InsUser createUser = insUserService.getUserInfo(activity.getCreatedBy());
			insActivityVo.setCreateUid(createUser != null ? createUser.getId() : 0);
		}
		
		JSONObject fileAttrObj = null;
		//0-无回放，1-有回放
		if (activity.getReplayType() == 0) {
			insActivityVo.setFileAttr(null);
		} else if (!StringUtils.isEmpty(activity.getFileAttr()) && hasReplayPer) {
			try {
				fileAttrObj = JSONObject.fromObject(activity.getFileAttr());
				String url = MapUtils.getString(fileAttrObj, "webAudioUrl", "");
				if (StringUtils.isBlank(url)) {
					url = MapUtils.getString(fileAttrObj, "webRadioUrl", "");
				}
				insActivityVo.setAudioUrl(url);
				insActivityVo.setAudioDuration(MapUtils.getInteger(fileAttrObj, "mt", 0));
			} catch (Exception e) {
				insActivityVo.setAudioUrl("");
			}
		}

		//直播信息
		Map<String, Object> webcastInfo = myActivityMapper.getWebcastInfo(activityId);
		if (hasLivePer && webcastInfo != null) {
			insActivityVo.setWebcastId(MapUtils.getString(webcastInfo, "webcastId"));
			insActivityVo.setWebcastNumber(MapUtils.getString(webcastInfo, "webcastNumber"));
			insActivityVo.setOrganizerToken(MapUtils.getString(webcastInfo, "organizerToken"));
			insActivityVo.setPanelistToken(MapUtils.getString(webcastInfo, "panelistToken"));
			insActivityVo.setAttendeeToken(MapUtils.getString(webcastInfo, "attendeeToken"));
			insActivityVo.setOrganizerJoinUrl(MapUtils.getString(webcastInfo, "organizerJoinUrl"));
			insActivityVo.setPanelistJoinUrl(MapUtils.getString(webcastInfo, "panelistJoinUrl"));
			insActivityVo.setAttendeeAShortJoinUrl(MapUtils.getString(webcastInfo, "attendeeAShortJoinUrl"));
			insActivityVo.setAttendeeJoinUrl(MapUtils.getString(webcastInfo, "attendeeJoinUrl"));
		}
		//点播信息
		if (hasReplayPer && activity.getReplayType() == 1) {//有回放
			Map<String, Object> vodInfo = myActivityMapper.getVodInfo(activityId);
			if (vodInfo != null) {
				insActivityVo.setVodId(MapUtils.getString(vodInfo, "vodId"));
				insActivityVo.setVodNumber(MapUtils.getString(vodInfo, "vodNumber"));
				insActivityVo.setOrganizerJoinUrl(MapUtils.getString(webcastInfo, "organizerJoinUrl"));
				insActivityVo.setPanelistJoinUrl(MapUtils.getString(webcastInfo, "panelistJoinUrl"));
				insActivityVo.setAttendeeAShortJoinUrl(MapUtils.getString(webcastInfo, "attendeeAShortJoinUrl"));
				insActivityVo.setAttendeeJoinUrl(MapUtils.getString(webcastInfo, "attendeeJoinUrl"));
			}
		}

		return insActivityVo;
	}

	private int getPermissionStatus(UserInfo userInfo, InsActivity activity) {
		String mobile = null;
		if (userInfo != null) {
			mobile = userInfo.getMobile();
		}
		if (activity.getShare() == MeixConstants.SHARE_PUBLIC) {
			return 1;
		}
		if (mobile != null) {
			List<InsCompanyInstituteUserActivity> list = whiteListService.getActivityUserList(activity.getId());
			for (InsCompanyInstituteUserActivity whiteUser : list) {
				if (mobile.equals(whiteUser.getMobile())) {
					return 2;
				}
			}
		}
		return 0;
	}

	@Override
	public int getActivityIsEnd(InsActivity activity) {
		if (activity == null) {
			return -1;
		}
		if (activity.getEndTime() == null) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			if (calendar.getTime().after(activity.getStartTime())) {
				return 1;
			} else {
				return 0;
			}
		}
		if (activity.getEndTime().before(new Date())) {
			return 1;
		} else {
			return 0;
		}
	}

	private int getRelationType(SecuMainVo secu) {
		if (secu.getInnerCode() == 1) {
			return 3;    //自定义标签（与原系统兼容，新系统应该用不到，先放这）
		} else if ((secu.getSecuCategory() == 4 && secu.getIndustryCode() > 0) || "000300".equals(secu.getSecuCode())) {
			return 2;    //行业
		} else {
			return 1;    //股票
		}
	}

	@Override
	public int getActivityStatus(int activityType, int originalIsEnd,
	                             int endStatus, Date startTime, Date endTime) {
		//金组合电话会议才有2预告 3 回放 4进行中三种状态
		if (activityType == MeixConstants.CHAT_MESSAGE_DHHY) {
			if (endStatus != 0) {
				return endStatus;
			}
			if (endTime == null) {
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(startTime.getTime());
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endTime = cal.getTime();
			}
			int playStatus = getPlayStatus(startTime, endTime);
			if (playStatus != -2) {
				return playStatus;
			}
		}

		return originalIsEnd;
	}

	protected int getPlayStatus(Date startDate, Date endDate) {
		Date curDate = new Date();
		if (startDate != null && startDate.after(curDate)) {
			return 2;//预告
		} else if (startDate != null && endDate != null && startDate.before(curDate) && endDate.after(curDate)) {
			return 4;//进行中
		} else if (endDate != null && endDate.before(curDate)) {
			return 3;//回放
		} else {
			return -2;
		}
	}

	@Override
	public int updateActivityStatus(long activityId, int isEnd, String uuid) {
		return myActivityMapper.updateActivityStatus(activityId, isEnd, uuid);
	}

	@Override
	public InsActivityPerson getJoinPerson(long activityId, long uid) {
		Example example = new Example(InsActivityPerson.class);
		example.createCriteria().andEqualTo("activityId", activityId).andEqualTo("uid", uid);
		example.orderBy("ID DESC");
		return personMapper.selectOneByExample(example);
	}

	@Override
	public List<InsActivityAnalyst> getActivityAnalyst(long activityId) {
		Example example = new Example(InsActivityAnalyst.class);
		example.createCriteria().andEqualTo("activityId", activityId);
		example.orderBy("id").desc();
		return analystMapper.selectByExample(example);
	}

	/**
	 * @param stockRange
	 * @param industry
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @author zhangzhen(J)
	 * @date 2016年1月7日上午11:12:37
	 * @Description: 查询活动接口
	 */
	public List<ActivityVo> getActivityList(ActivityListSearchVo searchVo, int stockRange, JSONArray industry,
	                                        JSONArray industry2, long version, JSONArray citys, long combId, int innerCode) throws IllegalAccessException, InvocationTargetException {
		//添加处理condition
		if (StringUtils.isNotBlank(searchVo.getCondition())) {
			StringBuffer nameCondition = new StringBuffer();
			String condition = searchVo.getCondition();
			for (int i = 0; i < condition.length(); i++) {
				char ch = condition.charAt(i);
				if (ch >= 'a' && ch <= 'z') {
					ch = (char) (ch + 'Ａ' - 'a');
				} else if (ch >= 'A' && ch <= 'Z') {
					ch = (char) (ch + 'Ａ' - 'A');
				}
				nameCondition.append(ch);
			}
			searchVo.setCondition(nameCondition.toString());
		}

		List<SelfStockVo> secuList = new ArrayList<SelfStockVo>();//自选股内的股票列表
		List<Integer> list = new ArrayList<Integer>();
		switch (stockRange) {
			case 1://自选股
				long companyCode = 0;
				UserInfo userInfo = userService.getUserInfo(searchVo.getUid(), null, null);
				if (userInfo != null) {
					companyCode = userInfo.getCompanyCode();
				}
				secuList = selfStockDaoImpl.getStockListByOneCategory(searchVo.getCategoryId(), companyCode);
				break;
			case 2: {//单个股票
				list.add(innerCode);
				//A股股票所属行业相关的活动也需要
				SecuMain smc = secumainService.getSecuMain(innerCode);
				if (isAMarket(smc.getSuffix())) {
					list.add(serviceCache.getIndustryByCode(smc.getIndustryCode()).getInnerCode());
				}
			}
			break;
		}

		if (20 == searchVo.getSearchType() || 21 == searchVo.getSearchType()) {
			Example example = new Example(InsActivityTarget.class);
			example.createCriteria().andEqualTo("activityId", searchVo.getActivityId());
			List<InsActivityTarget> insActivityTarget = targetMapper.selectByExample(example);
			if (insActivityTarget != null && insActivityTarget.size() > 0) {
				for (InsActivityTarget target : insActivityTarget) {
					list.add(target.getInnerCode().intValue());
				}
			}
		}

		//行业筛选取
		for (int i = 0; i < industry.size(); i++) {
			list.add(serviceCache.getIndustryByCode(industry.getInt(i)).getInnerCode());
		}
		List<Integer> industryCodes = new ArrayList<Integer>();
		//1.3.4传值为行业内码
		for (int i = 0; i < industry2.size(); i++) {
			list.add(industry2.getInt(i));
			ConstantVo cv = serviceCache.getIndustryLabelByInnerCode(industry2.getInt(i));
			if (cv != null) {
				industryCodes.add(cv.getCode());
			}
		}
		for (SelfStockVo vo : secuList) {
			int tmpInnerCode = vo.getInnerCode();
			list.add(tmpInnerCode);
			//A股股票所属行业相关的活动也需要
			SecuMain smc = secumainService.getSecuMain(tmpInnerCode);
			if (smc != null && isAMarket(smc.getSuffix())) {
				list.add(serviceCache.getIndustryByCode(smc.getIndustryCode()).getInnerCode());
			}
		}
		if (list.size() > 0) {
			if (searchVo.getList() != null && searchVo.getList().size() > 0) {
				searchVo.getList().addAll(list);
			} else {
				searchVo.setList(list);
			}
		} else if (stockRange != 0) {
			return new ArrayList<ActivityVo>();
		}
		if (industryCodes.size() > 0) {
			searchVo.setIndustryCodes(industryCodes);
		}
		UserInfo user = userService.getUserInfo(searchVo.getUid(), null, null);
		if (user == null) {
		} else {
			//我公司预约的活动
			if (searchVo.getActivityRange() == 4) {
				searchVo.setBuyerCompanyCode(user.getCompanyCode());
			}
		}

		//搜索类型查寻
		switch (searchVo.getSearchType()) {
			case 2://我参加的活动
				searchVo.setActivityRange(2);
				break;
			case 10://热门活动
				boolean firstPage = searchVo.getCurrentPage() == 0;
				if (searchVo.getQueryFlag() != 0) {
					firstPage = searchVo.getActivityId() == 0;
				}
				if (firstPage) {
					searchVo.setStartDateTime(DateUtil.getCurrentDateTime());
					searchVo.setCurrentPage(0);
					searchVo.setShowNum(20);
				} else {
					//BUG #14010 APP端热门活动列表的数据重复，应该只取前20条
					return new ArrayList<ActivityVo>();
				}
				break;
			case 11://京沪深活动
				citys.add("北京");
				citys.add("上海");
				citys.add("深圳");
				break;
			case 12://机构活动
				searchVo.setCompanyCode(Long.parseLong(searchVo.getCondition()));
				searchVo.setCondition(null);
				break;
			default:
				break;
		}

		if (citys.size() > 0) {
			List<String> cityList = new ArrayList<String>();
			Map<String, String> cityListMap = new HashMap<String, String>();
			for (int i = 0; i < citys.size(); i++) {
				if ("其他".equals(citys.getString(i))) {
					continue;
				}
				cityList.add(citys.getString(i));
				cityListMap.put(citys.getString(i), citys.getString(i));
			}

			if (cityList.size() > 0) {
				searchVo.setCitys(cityList);
			}
		}
		String customQueryKey = searchVo.getCustomQuery();
		if (customQueryKey != null) {
			String customQueryCondition = constDaoImpl.getCustomQueryCondition(customQueryKey);
			searchVo.setCustomQuery(customQueryCondition);
			if (customQueryCondition.contains("order by") || customQueryCondition.contains("ORDER BY")) {
				searchVo.setHasOrderFlag(1);
			} else {
				searchVo.setHasOrderFlag(0);
			}
			searchVo.setSearchType(-1);
		}

		int isActivityVip = 0;

		List<Activity> activityList = null;
		int searchType = searchVo.getSearchType();
		long activityId = searchVo.getActivityId();
		String orderStr = searchVo.getOrderStr();
		int showNum = searchVo.getShowNum();
		int queryFlag = searchVo.getQueryFlag();
		int isEnd = searchVo.getIsEnd();
		//金组合电话会议列表第一次进来需要在最前面放两条回放中记录
		if (searchType == 13 && activityId == 0 && orderStr.contains("a.ID ASC")
			&& queryFlag != -1 && isEnd == -2 && searchVo.getShareQueryFlag() == 0) {
			if (searchVo.getStartDateTime().substring(0, 10).equals(DateUtil.getCurrentDate())) {
				searchVo.setOrderStr(" a.StartTime ASC, a.ID ASC ");
				searchVo.setShowNum(2);
				searchVo.setQueryFlag(-1);
				searchVo.setGetExtraList(1);
				activityList = myActivityMapper.getActivityList(searchVo);

				int listSize = 0;
				if (activityList != null) {
					listSize = activityList.size();
					if (listSize > 0) {
						Activity activity = activityList.get(listSize - 1);
						String startTime = activity.getStartTime();
						searchVo.setStartDateTime(startTime);
						searchVo.setActivityId(activity.getId());
					}
				}

				searchVo.setOrderStr(orderStr);
				searchVo.setShowNum(showNum - listSize);
				searchVo.setQueryFlag(1);
			}

			searchVo.setGetExtraList(0);
			if (activityList != null) {
				activityList.addAll(myActivityMapper.getActivityList(searchVo));
			} else {
				activityList = myActivityMapper.getActivityList(searchVo);
			}
		} else {
			activityList = myActivityMapper.getActivityList(searchVo);
		}

		/**
		 * 调研、路演、策略会标签下，若没有当前日期之后的活动信息，则显示用户曾经参加过的对应活动信息（按开始时间降序排列
		 * @version pc3.0
		 **/
		if ((activityList == null || activityList.size() < 1) && searchVo.getAdditionalFlag() == 1 &&
			(searchVo.getActivityType() == 6 || searchVo.getActivityType() == 8 || searchVo.getActivityType() == 11)) {
			ActivityListSearchVo searchVo2 = new ActivityListSearchVo();
			searchVo2.setActivityRange(2);// 我参加的
			searchVo2.setUid(searchVo.getUid());
			//searchVo2.setSearchType(2);
			searchVo2.setShowNum(searchVo.getShowNum());
			searchVo2.setCurrentPage(searchVo.getCurrentPage());
			searchVo2.setOrderStr("a.StartTime DESC");
			activityList = myActivityMapper.getActivityList(searchVo2);
		}

		return convertVos(searchVo.getUid(), activityList, version, isActivityVip);
	}

	/**
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @author zhangzhen(J)
	 * @date 2016年1月7日上午11:15:52
	 * @Description: Activity  to ActivityVo
	 */
	private List<ActivityVo> convertVos(long searchUid, List<Activity> activityList,
	                                    long version, int isActivityVip) throws IllegalAccessException, InvocationTargetException {
		List<ActivityVo> voList = new ArrayList<>();
		ActivityVo vo = null;
		List<Map<String, Object>> nameList = null;
		String checkStr = "name:";
		for (Activity activity : activityList) {
			vo = new ActivityVo();
			nameList = new ArrayList<>();
			if (vo.getResearchObject() == null) {
				vo.setResearchObject("");
			}
			BeanUtils.copyProperties(vo, activity);

			UserInfo author = getUserInfo(activity.getUid());
			if (author != null) {
				vo.setAuthorName(author.getUserName());
				vo.setAuthorCode(author.getId());
			}
			if (author == null) {
				vo.setAuthorHeadImgUrl("");
			} else {
				String headImgUrl = OssResourceUtil.getHeadImage(author.getHeadImageUrl());
				vo.setAuthorHeadImgUrl(headImgUrl);
			}
			long companyCode = activity.getCompanyCode();
			String companyAbbr = activity.getCompanyAbbr();
			if (!TextUtils.isEmpty(companyAbbr)) {
				vo.setOrgName(companyAbbr);
				vo.setOrgCode(companyCode);
			} else {
				if (author != null) {
					vo.setOrgName(author.getCompanyAbbr());
					vo.setOrgCode(author.getCompanyCode());
				}
			}

			/**
			 * likuan  活动总结状态中用到的活动发起人状态
			 */
			/******* 分析师/发起人 *******/
			if (activity.getAnalystList() != null) {
				String analystList = activity.getAnalystList();
				String[] analyst = analystList.split(",");
				Map<String, Object> analystMap = null;
				for (String uid : analyst) {
					UserInfo user = null;
					//字符串与数据库定义对应
					//手动输入的分析师
					if (uid.contains(checkStr)) {
						user = new UserInfo();
						user.setUserName(new StringBuffer(uid.substring(uid.indexOf(checkStr) + checkStr.length(), uid.length())).toString());
						user.setId(0);
					} else {
						//系统中的
						InsUser insUser = insUserService.getUserInfo(Long.valueOf(uid));
						if (insUser != null) {
							user = userService.getUserInfoByUuid(insUser.getUser());
						}
					}
					if (user != null) {
						analystMap = new HashMap<>();
						analystMap.put("name", user.getUserName());
						analystMap.put("code", user.getId());
						nameList.add(analystMap);
					}
				}
			}
			vo.setAnalyst(nameList);

			/****** 上市公司、行业 ******/
			Map<String, Object> secuMap = null;
			List<Map<String, Object>> secuMapList = new ArrayList<>();
			String target = "";
			//宏观、策略、事件 转换为 上证指数
			if (activity.getActivityType() != MeixConstants.CHAT_MESSAGE_DY && activity.getResearchObject() != null && !"".equals(activity.getResearchObject())) {
				secuMapList.add(convertSecuInfo(secuMap, MeixConstants.RELATION_CODE_HG, activity.getResearchObject(), activity.getActivityType()));
			}
			if (activity.getTargetList() != null) {
				target = activity.getTargetList();
			}
			String[] targetCode = target.split(",");
			int count = 0;
			for (String code : targetCode) {
				if (count > 9)
					break;
				if (code == null || "".equals(code)) {
					continue;
				}
				secuMapList.add(convertSecuInfo(secuMap, Integer.valueOf(code), null, activity.getActivityType()));
				count++;
			}
			vo.setSecuList(secuMapList);
			//对于有录音文件的电话会议，根据会议起始时间设置电话会议的状态
			vo.setIsEnd(getActivityStatus(activity.getActivityType(),
				activity.getIsEnd(),
				activity.getEndStatus(), activity.getStartTime(), activity.getEndTime()));
			vo.setPlayType(activity.getPlayType());

			voList.add(vo);
		}
		return voList;
	}

	//对于金组合电话会议，根据会议起始时间获取电话会议的状态
	public int getActivityStatus(int activityType, int originalIsEnd,
	                             int endStatus, String startTime, String endTime) {
		if (originalIsEnd == -1) {
			return originalIsEnd;
		}
		//金组合电话会议才有2预告 3 回放 4进行中三种状态
		if (activityType == MeixConstants.CHAT_MESSAGE_DHHY) {
			if (endStatus != 0) {
				return endStatus;
			}
			if (endTime == null) {
				endTime = DateUtil.getCurrentDate() + " 23:59:59";
			}
			Date startDate = DateUtil.stringToDate(startTime);
			Date endDate = DateUtil.stringToDate(endTime);
			int playStatus = getPlayStatus(startDate, endDate);
			if (playStatus != -2) {
				return playStatus;
			}
		}

		return originalIsEnd;
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年1月14日下午7:06:11
	 * @Description: 缓存中取股票行业信息
	 */
	@Override
	public Map<String, Object> convertSecuInfo(Map<String, Object> secuMap, int code, String researchObject, int activityType) {
		secuMap = new HashMap<String, Object>();
		SecuMain secuMain = secumainService.getSecuMain(code);
		// ia_constant LB=3的自定义标签
		if (secuMain == null || code == 1) {
			@SuppressWarnings("unchecked")
			Map<String, Object> industry = constDaoImpl.getIndustryLabel(code);
			secuMap.put("innerCode", code);
			secuMap.put("industryCode", 0);
			secuMap.put("secuAbbr", MapUtils.getString(industry, "MS"));
			secuMap.put("relationType", 3);
			secuMap.put("secuClass", 4);
			return secuMap;
		}
		secuMap.put("innerCode", code);
		secuMap.put("industryCode", secuMain.getIndustryCode());
		secuMap.put("secuClass", secuMain.getSecuCategory());
		if (activityType != MeixConstants.CHAT_MESSAGE_DY && researchObject != null && !"".equals(researchObject)) {
			secuMap.put("secuAbbr", researchObject);
		} else {
			secuMap.put("secuCode", secuMain.getSecuCode() + secuMain.getSuffix());
			secuMap.put("secuAbbr", secuMain.getSecuAbbr());
		}

		if (secuMain.getInnerCode() == 1) {
			secuMap.put("relationType", 3);
		} else if (secuMain.getSecuCategory() == 4 && secuMain.getIndustryCode() > 0 || secuMain.getSecuCode() == "000300") {
			secuMap.put("relationType", 2);
		} else {
			secuMap.put("relationType", 1);
		}

		return secuMap;
	}

	/**
	 * 按月获取每天的活动数量
	 */
	@Override
	public List<ActivityDayNumberVo> getActivityNumOfDayByMonth(long uid, String uuid, String date) {
		return myActivityMapper.getActivityNumOfDayByMonth(date);
	}

	@Override
	public List<OrgOrUserVo> getOrgOrUserContactList(int currentPage, int showNum, String condition, int type) {
		List<OrgOrUserVo> res = new ArrayList<>();

		if (type == 3) {
			List<UserInfo> userList = insUserService.getCustomerList(currentPage, showNum, condition);
			for (UserInfo user : userList) {
				OrgOrUserVo vo = new OrgOrUserVo();
				vo.setCode(user.getId());
				vo.setName(user.getUserName());
				vo.setContactId(user.getId());
				vo.setContactName(user.getUserName());
				res.add(vo);
			}
		} else if (type == 2) {
			RowBounds rowBounds = new RowBounds(currentPage * showNum, showNum);
			Example example = new Example(InsUser.class);
			example.createCriteria().andLike("userName", "%" + condition + "%");
			List<InsUser> userList = userMapper.selectByExampleAndRowBounds(example, rowBounds);
			for (InsUser user : userList) {
				OrgOrUserVo vo = new OrgOrUserVo();
				vo.setCode(user.getId());
				vo.setName(user.getUserName());
				vo.setContactId(user.getId());
				vo.setContactName(user.getUserName());
				vo.setUser(user.getUser());
				res.add(vo);
			}
		} else if (type == 1) {
			List<CompanyInfoVo> list = companyService.getCompanyList(condition, currentPage, showNum);
			list = list == null ? new ArrayList<>() : list;
			for (CompanyInfoVo temp : list) {
				OrgOrUserVo vo = new OrgOrUserVo();
				vo.setCode(temp.getId());
				vo.setName(temp.getCompanyAbbr());
				vo.setContactId(temp.getId());
				vo.setContactName(temp.getCompanyAbbr());
				vo.setOrgCode(temp.getId());
				vo.setOrgName(temp.getCompanyAbbr());
				res.add(vo);
			}
		}
		return res;
	}

	/**
	 * @return 0、已参加，1、新增
	 * @Description: 活动参加人员
	 */
	public int saveJoinActivity(long uid, long activityId, String userName, String mobile,
	                            String companyName,
	                            String position) {
		int result = 0;
		//参加活动
		Example example = new Example(InsActivityPerson.class);
		example.createCriteria().andEqualTo("mobile", mobile).andEqualTo("activityId", activityId);
		InsActivityPerson person = new InsActivityPerson();
		person.setCreatedAt(new Date());
		result = personMapper.updateByExampleSelective(person, example);
		if (result == 0) {
			person.setUid(uid);
			person.setActivityId(activityId);
			person.setUserName(userName);
			person.setMobile(mobile);
			person.setStatus(0);
			person.setType(0);
			person.setCompanyName(companyName);
			person.setPosition(position);
			person.setUpdatedAt(new Date());
			person.setCreatedBy(mobile);
			person.setUpdatedBy(mobile);
			result = personMapper.insert(person);
		}
		return result;
	}

	@Override
	public List<Integer> getActivityIssueType(long activityId) {
		Example example = new Example(InsActivityIssueType.class);
		example.createCriteria().andEqualTo("activityId", activityId);

		//设置发布渠道信息
		List<InsActivityIssueType> typeList = issueTypeMapper.selectByExample(example);
		if (typeList != null && typeList.size() > 0) {
			List<Integer> issueTypeList = new ArrayList<>(typeList.size());
			for (InsActivityIssueType type : typeList) {
				issueTypeList.add(type.getIssueType());
			}
			return issueTypeList;
		}
		return null;
	}

	@Override
	public Map<String, Object> getActivityInnerCodeAndIndustry(long activityId) {
		return myActivityMapper.getActivityInnerCodeAndIndustry(activityId);
	}

	@Override
	public void attendActivity(long uid, long activityId, String user) {
		// 1.校验是参加记录是否已经存在
		InsActivityJoin record = new InsActivityJoin();
		record.setUid(uid);
		record.setActivityId(activityId);
		record = activityJoinMapper.selectOne(record);
		if (record != null && record.getId() != null) {
			return;
		}
		// 2.添加
		record = new InsActivityJoin();
		record.setUid(uid);
		record.setActivityId(activityId);
		record.setJoinAt(new Date());
		activityJoinMapper.insert(record);
	}

	@Override
	public void updateActivity(InsActivity activity, GenseeVideo genseeVideo) {
		activityMapper.updateByPrimaryKeySelective(activity);
		saveActivityGenseeVideo(activity.getId(), genseeVideo);
	}

	private void saveActivityGenseeVideo(long activityId, GenseeVideo genseeVideo) {
//		log.info("genseeVideo:" + JSONObject.fromObject(genseeVideo));
		if (genseeVideo != null && !TextUtils.isEmpty(genseeVideo.getId())) {
			myActivityMapper.deleteActivityGenseeVideo(genseeVideo.getId());
			Map<String, Object> webcastParams = new HashMap<>();
			webcastParams.put("activityId", activityId);
			webcastParams.put("webcastId", genseeVideo.getId());
			webcastParams.put("webcastNumber", genseeVideo.getNumber());
			webcastParams.put("organizerToken", genseeVideo.getOrganizerToken());
			webcastParams.put("panelistToken", genseeVideo.getPanelistToken());
			webcastParams.put("attendeeToken", genseeVideo.getAttendeeToken());
			webcastParams.put("organizerJoinUrl", genseeVideo.getOrganizerJoinUrl());
			webcastParams.put("panelistJoinUrl", genseeVideo.getPanelistJoinUrl());
			webcastParams.put("attendeeAShortJoinUrl", genseeVideo.getAttendeeAShortJoinUrl());
			webcastParams.put("attendeeJoinUrl", genseeVideo.getAttendeeJoinUrl());
			myActivityMapper.saveActivityGenseeWebcast(webcastParams);
		}

	}

	@Override
	public void deleteActivity(long activityId, String uuid) {
		InsActivity activity = new InsActivity();
		activity.setId(activityId);
		activity.setActivityStatus(-1);
		activity.setUpdatedBy(uuid);
		activity.setUpdatedAt(new Date());
		activityMapper.updateByPrimaryKeySelective(activity);
		Map<String, Object> params = new HashMap<>();
		params.put("id", activityId);
		params.put("companyCode", commonServerProperties.getCompanyCode());
		meixSyncService.reqMeixSync(MeixConstants.USER_HD, SyncConstants.ACTION_DELETE_ACTIVITY, params);
	}

	@Override
	public int deleteGenseeRecord(long activityId) {
		return activityGenseeMapper.deleteGenseeRecord(activityId);
	}

	@Override
	public int saveGenseeRecordList(List<ActivityGenseeRecord> list) {
		if (list == null || list.size() == 0) {
			return 0;
		}
		return activityGenseeMapper.saveGenseeRecordList(list);
	}

	@Override
	public void syncGenseeRecord(String startDate, String endDate, long activityId) {
		List<Long> idList = null;
		if (activityId == 0) {
			idList = activityGenseeMapper.getUnsyncActivityId(startDate, endDate);
		} else {
			idList = Collections.singletonList(activityId);
		}
		if (idList == null || idList.size() == 0) {
			return;
		}
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", commonServerProperties.getCompanyCode());
		params.put("idList", idList);
		meixSyncService.reqMeixSync(MeixConstants.USER_HD, SyncConstants.ACTION_GET_GENSEE_RECORD_LIST, params);
	}

	@Override
	public int deleteTeleJoinRecord(long activityId) {
		return activityGenseeMapper.deleteTeleJoinRecord(activityId);
	}

	@Override
	public int saveTeleJoinRecordList(List<ActivityThirdJoinInfo> list) {
		return activityGenseeMapper.saveTeleJoinRecordList(list);
	}

	@Override
	public void syncTeleJoinRecord(String startDate, String endDate, long activityId) {
		List<Long> idList = null;
		if (activityId == 0) {
			idList = activityGenseeMapper.getUnsyncTeleActivityId(startDate, endDate);
		} else {
			idList = Collections.singletonList(activityId);
		}
		if (idList == null || idList.size() == 0) {
			return;
		}
		Map<String, Object> params = new HashMap<>();
		params.put("companyCode", commonServerProperties.getCompanyCode());
		params.put("idList", idList);
		meixSyncService.reqMeixSync(MeixConstants.USER_HD, SyncConstants.ACTION_GET_TELE_JOIN_RECORD_LIST, params);
	}

	@Override
	public void deleteGenseeVod(long activityId) {
		myActivityMapper.deleteGenseeVod(activityId);
	}

	@Override
	public void saveGenseeVodList(List<JSONObject> list) {
		myActivityMapper.saveGenseeVodList(list);
	}
}
