package com.meix.institute.impl;

import com.meix.institute.BaseService;
import com.meix.institute.api.*;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsActivityPerson;
import com.meix.institute.entity.InsMessagePushConfig;
import com.meix.institute.entity.InsMessagePushResult;
import com.meix.institute.entity.InsUser;
import com.meix.institute.mapper.InsMessagePushConfigMapper;
import com.meix.institute.mapper.InsMessagePushResultMapper;
import com.meix.institute.service.SmsService;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.MessageUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.util.WechatUtil;
import com.meix.institute.vo.activity.ActivityAnalystVo;
import com.meix.institute.vo.activity.InsActivityVo;
import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.company.CompanyWechatConfig;
import com.meix.institute.vo.company.CompanyWechatTemplete;
import com.meix.institute.vo.meeting.MorningMeetingVo;
import com.meix.institute.vo.report.ReportAuthorVo;
import com.meix.institute.vo.report.ReportDetailVo;
import com.meix.institute.vo.user.InsMeixUser;
import com.meix.institute.vo.user.InsSaler;
import com.meix.institute.vo.user.UserInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/9/27.
 */
@Service
@SuppressWarnings("unused")
public class InstituteMessagePushServiceImpl extends BaseService implements IInstituteMessagePushService {
	protected final Logger log = LoggerFactory.getLogger(InstituteMessagePushServiceImpl.class);
	@Autowired
	private IActivityService activityServiceImpl;
	@Autowired
	private IReportService reportServiceImpl;
	@Autowired
	private IMeetingService meetingServiceImpl;
	@Autowired
	private ICompanyService companySerciveImpl;
	@Autowired
	private IUserService userServiceImpl;
	@Autowired
	private IMessageService messageServiceImpl;
	@Autowired
	private IInstituteDao instituteDaoImpl;
	@Autowired
	private IInsUserService insUserService;
	@Autowired
	private InsMessagePushResultMapper pushResultMapper;
	@Autowired
	private InsMessagePushConfigMapper pushConfigMapper;
	@Autowired
	private SmsService smsService;
	@Autowired
	private IInsWhiteListService insWhiteListService;

	public final static int JOIN_ACTIVITY_CHANGED = 0;//报名的活动信息发生变化
	public final static int FOCUSED_ANALYST_AND_SELFSTOCK = 1;//关注的分析师、自选股相关的内容产生；重点内容推送（策略会）
	public final static int JOIN_ACTIVITY_SUCCESS = 2;//报名通过的通知
	public final static int SEND_NOTICE_TOSALER = 3;//指派销售推送
	public final static int CREATE_ACTIVITY = 4;//新建活动
	public final static int CREATE_ALBUM = 5;//新建合辑
	public final static int CREATE_REPORT = 6;//新建会议纪要
	public final static int CREATE_MORNING_MEETING = 7;//新建晨会
	public final static int ACTIVITY_START_REMIND = 8;//电话会议开始前30分钟提醒
	public final static int USER_LABEL_PUSH = 9;//用户画像推送

	@Override
	public void sendPermReqNotice(String appType, long uid, int dataType, long dataId) {
		CompanyWechatConfig wechatConfig = instituteDaoImpl.getWechatConfigByAppType(appType);
		if (wechatConfig == null) {
			return;
		}
		UserInfo userInfo = userServiceImpl.getUserInfo(uid, null, null);
		if (userInfo == null ||
			StringUtil.isBlank(userInfo.getMobile()) ||
			userInfo.getMobile().startsWith(UserInfo.MOBILE_PREFIX)) {
			return;
		}
		switch (dataType) {
			case MeixConstants.USER_HD: {
				InsActivityVo activity = null;
				try {
					activity = activityServiceImpl.getActivityDetail(0, dataId, 0);
				} catch (Exception e) {
					log.error("", e);
				}
				if (activity == null) {
					log.error("不存在的活动{}", dataId);
					return;
				}
				CompanyInfoVo companyInfoCache = companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode());
				if (companyInfoCache == null) {
					return;
				}
				String smsContent = MessageUtil.getInstitutePermSalerNotice(userInfo, activity, true);
				String wxContent = MessageUtil.getInstitutePermSalerNotice(userInfo, activity, false);
				String wxTitle = MessageUtil.getInstituteWechatSalerTitle(activity);
				String emailSubject = "活动白名单申请";
				sendNotice2Saler(userInfo, companyInfoCache, wechatConfig, smsContent, wxContent, wxTitle, emailSubject);

				smsContent = MessageUtil.getInstitutePermUserNotice(companyInfoCache, activity, true);
				wxTitle = MessageUtil.getInstituteWechatUserTitle(companyInfoCache, activity);
				wxContent = MessageUtil.getInstitutePermUserNotice(companyInfoCache, activity, false);
				String remark = "请您注意查收对口销售参会成功与否的提醒！";
				sendNotice2ReqUser(userInfo, companyInfoCache, wechatConfig, smsContent, wxContent, wxTitle, emailSubject, remark);
				break;
			}
			case MeixConstants.USER_YB: {
				ReportDetailVo bean = null;
				try {
					bean = reportServiceImpl.getReportDetail(dataId, true);
				} catch (Exception e) {
					log.error("", e);
				}
				if (bean == null) {
					log.error("不存在的研报{}", dataId);
					return;
				}
				CompanyInfoVo companyInfoCache = companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode());
				if (companyInfoCache == null) {
					return;
				}
				String smsContent = MessageUtil.getInstitutePermSalerNotice(userInfo, bean, true);
				String wxContent = MessageUtil.getInstitutePermSalerNotice(userInfo, bean, false);
				String wxTitle = "您有新的客户申请查看研报原文";
				String emailSubject = "查看研报原文申请";
				sendNotice2Saler(userInfo, companyInfoCache, wechatConfig, smsContent, wxContent, wxTitle, emailSubject);

				smsContent = MessageUtil.getInstitutePermUserNotice(bean, true);
				wxTitle = "您申请查看研报原文";
				wxContent = MessageUtil.getInstitutePermUserNotice(bean, false);
				String remark = "请您注意查收对口销售审核结果的提醒！";
				sendNotice2ReqUser(userInfo, companyInfoCache, wechatConfig, smsContent, wxContent, wxTitle, emailSubject, remark);
				break;
			}
			default:
				break;
		}
	}

	private InsSaler getInsSaler(long companyCode, long salerId) {
		if (salerId <= 0) {
			return null;
		}
		InsUser saler = insUserService.getUserInfo(salerId);
		if (saler != null) {
			String mobile = saler.getMobile();
			UserInfo salerUser = userServiceImpl.getUserInfo(0, null, mobile);
			if (salerUser != null && salerUser.getCompanyCode() == companyCode) {
				long salerUid = salerUser.getId();
				InsSaler insSaler = new InsSaler();
				insSaler.setMobile(mobile);
				insSaler.setSalerUid(salerUid);
				return insSaler;
			}
		}
		return null;
	}

	/**
	 * 获取研究所某个用户的对口销售
	 *
	 * @param companyCode 研究所公司代码
	 * @param userMobile  用户手机号
	 * @return
	 */
	public InsSaler getInsSaler(long companyCode, String userMobile, long uid) {
		UserInfo user = userServiceImpl.getUserInfo(0, null, userMobile);
		long salerId = 0;
		if (user != null) {
			salerId = user.getSalerUid();
		} else {
			InsMeixUser meixUser = instituteDaoImpl.getExistInsMeixUser(companyCode, uid);
			if (meixUser != null) {
				salerId = meixUser.getSalerId();
			}
		}
		if (salerId > 0) {
			return getInsSaler(companyCode, salerId);
		}
		log.error("公司{}没有配置销售人员", companyCode);
		return null;
	}

	/**
	 * 发送通知到对口销售
	 *
	 * @param userInfo
	 * @param companyInfoCache
	 * @param wechatConfig
	 * @param smsContent
	 * @param wxContent
	 * @param wxTitle
	 * @param emailSubject
	 */
	private void sendNotice2Saler(UserInfo userInfo, CompanyInfoVo companyInfoCache, CompanyWechatConfig wechatConfig,
	                              String smsContent, String wxContent, String wxTitle, String emailSubject) {
		InsSaler insSaler = getInsSaler(companyInfoCache.getId(), userInfo.getMobile(), userInfo.getId());
		if (insSaler == null) {
			return;
		}
		sendNotice2Saler(insSaler, companyInfoCache, wechatConfig, smsContent, wxContent, wxTitle, emailSubject);
	}

	/**
	 * 发送通知到指定销售
	 *
	 * @param insSaler
	 * @param companyInfoCache
	 * @param wechatConfig
	 * @param smsContent
	 * @param wxContent
	 * @param wxTitle
	 * @param emailSubject
	 */
	public void sendNotice2Saler(InsSaler insSaler, CompanyInfoVo companyInfoCache, CompanyWechatConfig wechatConfig,
	                             String smsContent, String wxContent, String wxTitle, String emailSubject) {
		String openId = userServiceImpl.getOpenIdByUid(insSaler.getSalerUid(), wechatConfig.getAppType(), MeixConstants.CLIENT_TYPE_2);
		if (StringUtil.isNotEmpty(openId)) {//有绑定微信
			CompanyWechatTemplete templete = instituteDaoImpl.getCompanyWechatTemplete(companyInfoCache.getId(), MeixConstants.WX_TEMPLETE_WHITELIST_REQ);
			if (templete == null) {
				log.error("公司{}没有配置类型为{}的微信模板！！", companyInfoCache.getCompanyAbbr(), MeixConstants.WX_TEMPLETE_WHITELIST_REQ);
				return;
			}
			String templeteInfo = WechatUtil.getServiceNoticeTemplate(templete.getTempleteId(), openId, wxTitle, wxContent, companyInfoCache.getCompanyAbbr(), "请尽快联系客户确认！", null);
			boolean ret = WechatUtil.sendTemplateMsg(companyInfoCache.getId(), wechatConfig.getAppId(), wechatConfig.getAppSecret(), templeteInfo);
			if (ret) {
				return;
			}
		}
		InsUser saler = insUserService.getUserInfo(insSaler.getSalerUid());
		if (saler == null) {
			log.error("不存在的销售：{}！！无法发送通知", insSaler.getSalerUid());
			return;
		}
		if (StringUtil.isNotEmpty(saler.getEmail())) {
			if (StringUtil.isBlank(smsContent)) {
				return;
			}
			String emailContent = "尊敬的" + saler.getUserName() + ":</br>" + smsContent;
			boolean ret = messageServiceImpl.sendEmailWithResult(saler.getEmail(), emailSubject, emailContent, true);
			if (ret) {
				return;
			}
		}
		if (StringUtil.isBlank(saler.getMobile()) || saler.getMobile().startsWith(UserInfo.MOBILE_PREFIX)) {//没有手机号
			log.error("销售：{}手机号{}！！无法发送通知", insSaler.getSalerUid(), saler.getMobile());
			return;
		}
		sendSms(saler.getMobile(), smsContent);
	}

	private void sendNotice2ReqUser(UserInfo userInfo, CompanyInfoVo companyInfoCache, CompanyWechatConfig wechatConfig,
	                                String smsContent, String wxContent, String wxTitle, String emailSubject, String remark) {
		sendNotice2ReqUser(userInfo, companyInfoCache, wechatConfig, smsContent, wxContent, wxTitle, emailSubject, remark, null);
	}

	private void sendNotice2ReqUser(UserInfo userInfo, CompanyInfoVo companyInfoCache, CompanyWechatConfig wechatConfig,
	                                String smsContent, String wxContent, String wxTitle, String emailSubject, String remark, String url) {
		if (userInfo == null) {
			return;
		}
		if (userInfo.getId() > 0) {
			String openId = userServiceImpl.getOpenIdByUid(userInfo.getId(), wechatConfig.getAppType(), MeixConstants.CLIENT_TYPE_2);
			if (StringUtil.isNotEmpty(openId)) {//有绑定微信
				CompanyWechatTemplete templete = instituteDaoImpl.getCompanyWechatTemplete(companyInfoCache.getId(), MeixConstants.WX_TEMPLETE_WHITELIST_REQ);
				if (templete == null) {
					log.error("公司{}没有配置类型为{}的微信模板！！", companyInfoCache.getCompanyAbbr(), MeixConstants.WX_TEMPLETE_WHITELIST_REQ);
					return;
				}
				String templeteInfo = WechatUtil.getServiceNoticeTemplate(templete.getTempleteId(), openId, wxTitle, wxContent, companyInfoCache.getCompanyAbbr(), remark, url);
				boolean ret = WechatUtil.sendTemplateMsg(companyInfoCache.getId(), wechatConfig.getAppId(), wechatConfig.getAppSecret(), templeteInfo);
				if (ret) {
					return;
				}
			}
			if (StringUtil.isNotEmpty(userInfo.getEmail())) {
				if (StringUtil.isBlank(smsContent)) {
					return;
				}
				String emailContent = "尊敬的" + userInfo.getUserName() + ":</br>" + smsContent;
				boolean ret = messageServiceImpl.sendEmailWithResult(userInfo.getEmail(), emailSubject, emailContent, true);
				if (ret) {
					return;
				}
			}
		}
		if (StringUtil.isBlank(userInfo.getMobile()) || userInfo.getMobile().startsWith(UserInfo.MOBILE_PREFIX)) {//没有手机号
			log.error("用户：{}手机号{}！！无法发送通知", userInfo.getId(), userInfo.getMobile());
			return;
		}
		sendSms(userInfo.getMobile(), smsContent);
	}

//	private boolean sendNotice2ReqUserByWechat(UserInfo userInfo, CompanyInfoVo companyInfoCache, CompanyWechatConfig wechatConfig,
//	                                           String smsContent, String wxContent, String wxTitle, String emailSubject, String remark, String url) {
//
//		if (userInfo == null) {
//			return false;
//		}
//		if (userInfo.getId() > 0) {
//			String openId = userServiceImpl.getOpenIdByUid(userInfo.getId(), wechatConfig.getAppType());
//			if (StringUtil.isNotEmpty(openId)) {//有绑定微信
//				CompanyWechatTemplete templete = instituteDaoImpl.getCompanyWechatTemplete(companyInfoCache.getId(), MeixConstants.WX_TEMPLETE_WHITELIST_REQ);
//				if (templete == null) {
//					log.error("公司{}没有配置类型为{}的微信模板！！", companyInfoCache.getCompanyAbbr(), MeixConstants.WX_TEMPLETE_WHITELIST_REQ);
//					return false;
//				}
//				String templeteInfo = WechatUtil.getServiceNoticeTemplate(templete.getTempleteId(), openId, wxTitle, wxContent, companyInfoCache.getCompanyAbbr(), remark, url);
//				boolean res = WechatUtil.sendTemplateMsg(companyInfoCache.getId(), wechatConfig.getAppId(), wechatConfig.getAppSecret(), templeteInfo);
//				return res;
//			}
//		}
//		return false;
//	}

	@Override
	public void undoMessagePush(int dataType, long dataId, String reason, long companyCode) {
		MeixConstants.fixedThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				try {
//			CompanyWechatConfig wechatConfig = instituteDaoImpl.getWechatConfigByAppType(String.valueOf(companyCode));
//			if (wechatConfig == null) {
//				return;
//			}
//
//			CompanyWechatTemplete templete = instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(), MeixConstants.WX_TEMPLETE_WHITELIST_REQ);
//			if (templete == null) {
//				return;
//			}
//			CompanyInfoVo companyInfoCache = companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode());
//			if (companyInfoCache == null) {
//				return;
//			}
//			String wxTitle = "研究资源被下架！";
					StringBuffer sb = new StringBuffer();
					sb.append("您发布");
					String title = "";
//			String smsContent = "您发布的";
//			String title = "";
					List<Long> uids = new ArrayList<>();
					if (dataType == MeixConstants.USER_YB || dataType == 4) { //研报、会议纪要
						ReportDetailVo report = reportServiceImpl.getReportDetail(dataId, false);
						if (report == null) {
							return;
						}
						try {
//							if (report.getIssueTypeList() != null) {
//								if (report.getIssueTypeList().contains(4)) {
//									sb.append("在小程序中");
//								} else if (report.getIssueTypeList().contains(0)) {
//									sb.append("在每市平台");
//								} else if (report.getIssueTypeList().contains(1)) {
//									sb.append("在公众号中");
//								}
//							}
							sb.append("的");
							sb.append(report.getType() == 0 ? "研报" : "会议纪要");
//							sb.append(report.getTitle());
							title = report.getTitle();
//					smsContent += "研报";
//					title = report.getTitle();
							List<ReportAuthorVo> authors = report.getAuthorList();
							for (ReportAuthorVo author : authors) {
								if (author.getAuthorCode() > 0) {
									uids.add(author.getAuthorCode());
								}
							}
						} catch (Exception e) {
							log.error("", e);
							return;
						}
					} else if (dataType == MeixConstants.USER_CH) {
						MorningMeetingVo meeting = meetingServiceImpl.getMeetingDetail(dataId);
						if (meeting == null) {
							return;
						}
//						if (meeting.getIssueTypeList() != null) {
//							if (meeting.getIssueTypeList().contains(4)) {
//								sb.append("在小程序中");
//							} else if (meeting.getIssueTypeList().contains(0)) {
//								sb.append("在每市平台");
//							} else if (meeting.getIssueTypeList().contains(1)) {
//								sb.append("在公众号中");
//							}
//						}
						sb.append("的晨会");
						title = meeting.getTitle();
//						sb.append(meeting.getTitle());
						if (meeting.getAuthorCode() > 0) {
							uids.add(meeting.getAuthorCode());
						}
					} else if (dataType == MeixConstants.REPORT_ALBUM || dataType == MeixConstants.MEETING_ALBUM) {
						int type = 0;
						if (dataType == MeixConstants.MEETING_ALBUM) {
							type = 1;
						}
						Map<String, Object> album = reportServiceImpl.getReportAlbumDetail(type, dataId);
						if (album == null) {
							return;
						}
//						Object issueType = MapUtils.getObject(album, "issueType");
//						if (issueType != null) {
//							String str = issueType.toString();
//							if (str.contains("4")) {
//								sb.append("在小程序中");
//							} else if (str.contains("0")) {
//								sb.append("在每市平台");
//							} else if (str.contains("1")) {
//								sb.append("在公众号中");
//							}
//						}
						sb.append("的");
						if (dataType == MeixConstants.REPORT_ALBUM) {
							sb.append("研报合辑");
						} else {
							sb.append("会议纪要合辑");
						}
						title = MapUtils.getString(album, "title", "");
//						sb.append(MapUtils.getString(album, "title", ""));
//						sb.append("》，已被合规人员撤销，撤销原因：");
						String uuid = MapUtils.getString(album, "createdBy");
						if (StringUtils.isNotBlank(uuid)) {
							InsUser user = insUserService.getUserInfo(uuid);
							if (user != null) {
								uids.add(user.getId());
							}
						}
					} else {
						try {
							InsActivityVo activity = activityServiceImpl.getActivityDetail(0, dataId, 0, false);
							if (activity == null) {
								return;
							}
//							if (activity.getIssueTypeList() != null) {
//								if (activity.getIssueTypeList().contains(4)) {
//									sb.append("在小程序中");
//								} else if (activity.getIssueTypeList().contains(0)) {
//									sb.append("在每市平台");
//								} else if (activity.getIssueTypeList().contains(1)) {
//									sb.append("在公众号中");
//								}
//							}
							sb.append("的");
							sb.append(getActivityTypeDesc(activity.getActivityType()));
//							sb.append("《");
							title = activity.getTitle();
//							sb.append(activity.getTitle());
//							sb.append("》，已被合规人员撤销，撤销原因：");
//					smsContent += getActivityTypeDesc(dataType);
//					title = activity.getTitle();
							List<ActivityAnalystVo> authors = activity.getAnalyst();
							if (authors != null && !authors.isEmpty()) {
								for (ActivityAnalystVo author : authors) {
									if (author.getCode() > 0) {
										uids.add(author.getCode());
									}
								}
							} else {
								uids.add(activity.getCreateUid());
							}
						} catch (Exception e) {
							return;
						}
					}
					sb.append("，目前已被合规人员紧急下架，下架原因：");
					sb.append(reason);
					sb.append(" 请尽快处理《");
					sb.append(title);
					sb.append("》");
//			smsContent += "目前已被合规人员紧急下架，下架原因：" + reason + " 请尽快处理《" + title + "》";
//			String remark = "";

//			String emailSubject = smsContent;
					List<String> mobiles = new ArrayList<>();
					for (Long uid : uids) {
						InsUser userInfo = insUserService.getUserInfo(uid);
						if (userInfo == null) {
							continue;
						}
						if (StringUtils.isBlank(userInfo.getMobile()) || "null".equals(userInfo.getMobile()) || userInfo.getMobile().contains("**")) {
							continue;
						}
						mobiles.add(userInfo.getMobile());
//				sendNotice2ReqUser(userInfo, companyInfoCache, wechatConfig,
//					smsContent, content, wxTitle, emailSubject, remark);
					}
					sendSms(mobiles, sb.toString());
				} catch (Exception e) {
					log.error("资源撤销推送异常：", e);
				}
			}
		});
	}

	@Override
	public void sendBecomeWlNotice(long companyCode, UserInfo userInfo, Date expireDate) {
		if (userInfo == null) {
			log.warn("无效的用户，无法发送成为白名单通知");
			return;
		}
		CompanyWechatConfig wechatConfig = instituteDaoImpl.getWechatConfigByAppType(String.valueOf(companyCode));
		if (wechatConfig == null) {
			return;
		}

		CompanyWechatTemplete templete = instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(), MeixConstants.WX_TEMPLETE_WHITELIST_REQ);
		if (templete == null) {
			return;
		}
		String wxTitle = "您已成功成为白名单用户！";

		CompanyInfoVo companyInfoCache = companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode());
		if (companyInfoCache == null) {
			return;
		}
		String content = "您已成为" + companyInfoCache.getCompanyAbbr() + "白名单用户";
		if (expireDate != null) {
			content += "，有效期至" + DateUtil.dateToStr(expireDate, "yyyy年MM月dd日");
		}
		content += "！";
		String remark = "";
		String smsContent = "恭喜您，您已成为白名单用户，享受众多研究资源权限。";
		String emailSubject = wxTitle;
		String finalContent = content;
		MeixConstants.fixedThreadPool.execute(() -> {
			sendNotice2ReqUser(userInfo, companyInfoCache, wechatConfig,
				smsContent, finalContent, wxTitle, emailSubject, remark);
		});
	}

	@Override
	public void sendCardAuthNotice(long companyCode, long uid, int auditResult) {
		UserInfo userInfo = userServiceImpl.getUserInfo(uid, null, null);
		if (userInfo == null) {
			log.warn("无效的用户，无法发送账号审核结果通知");
			return;
		}
//		CompanyWechatConfig wechatConfig = instituteDaoImpl.getWechatConfigByAppType(String.valueOf(companyCode));
//		if (wechatConfig == null) {
//			return;
//		}
//
//		CompanyWechatTemplete templete = instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(), MeixConstants.WX_TEMPLETE_WHITELIST_REQ);
//		if (templete == null) {
//			return;
//		}
//		CompanyInfoVo companyInfoCache = companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode());
//		if (companyInfoCache == null) {
//			return;
//		}
//		String wxTitle;
		String content;
//		String smsContent;
//		String emailSubject;
		if (auditResult == 0) {
			//审核不通过
//			wxTitle = "很抱歉，您的实名申请未通过！";
			content = "抱歉，您的小程序实名申请未通过。可能原因：1.名片不清晰。2.非投研机构用户。如有疑问请联系邮箱：";
			content += commonServerProperties.getEmail();
			content += ".";
//			smsContent = "抱歉，您的公众号实名申请未通过。可能原因：1.名片不清晰。2.非投研机构用户。";
//			smsContent += "如有疑问，请致电：021-38934111。";
//			emailSubject = wxTitle;
		} else {
			//审核通过
//			wxTitle = "恭喜您，您的实名申请已通过！";
			content = "恭喜您，您的小程序实名申请已通过，请登录小程序体验。";
//			smsContent = "恭喜您，您的公众号实名申请已通过，请登录公众号体验。";
//			emailSubject = wxTitle;
		}
//		String remark = "";
		String finalContent = content;
//		String finalSmsContent = smsContent;
		MeixConstants.fixedThreadPool.execute(() -> {
//			sendNotice2ReqUser(userInfo, companyInfoCache, wechatConfig,
//				finalSmsContent, finalContent, wxTitle, emailSubject, remark);
			sendSms(userInfo.getMobile(), finalContent);
		});
	}

	@Override
	public void messagePush(long companyCode, long uid, int messageType, String title, int dataType, int clueType, String mobile) {
		instituteMessagePush(String.valueOf(companyCode), dataType, uid, messageType, null,
			null, title, clueType, null, null, 0, null, null, uid, mobile);
	}

	@Override
	public void instituteMessagePush(String appId, int dataType, long dataId, int messageType, String messageInfo,
	                                 String remark, String title, int flag, String userName, String messageFrom, long companyCode, List<Long> secuMain, List<Long> analyst, long uid, String mobile) {

		MeixConstants.fixedThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				switch (messageType) {
//					case InstituteMessagePushServiceImpl.JOIN_ACTIVITY_CHANGED:    //报名的活动信息发生变化
//						joinActivityChanged(appId, dataType, dataId, companyCode, title, flag, messageFrom);
//						break;
//					case InstituteMessagePushServiceImpl.FOCUSED_ANALYST_AND_SELFSTOCK:    //关注的分析师、自选股相关的内容产生；重点内容推送（策略会）
//						focusedAnalystAndSelfStock(appId, dataType, dataId, companyCode, title, flag, messageFrom, secuMain, analyst);
//						break;
					case InstituteMessagePushServiceImpl.JOIN_ACTIVITY_SUCCESS: //报名通过通知
						joinActivitySuccess(uid, appId, dataType, title, flag);
						break;
					case InstituteMessagePushServiceImpl.SEND_NOTICE_TOSALER: //指派销售推送
						sendNoticeToSaler(mobile, uid, appId, flag);
						break;
					case InstituteMessagePushServiceImpl.CREATE_ACTIVITY: //新建活动
//						createActivity(appId, dataType, dataId, title);
						break;
					case InstituteMessagePushServiceImpl.CREATE_ALBUM: //新建合辑
//						createAlbum(appId, dataType, dataId, title);
						break;
					case InstituteMessagePushServiceImpl.CREATE_REPORT: //新建会议纪要
//						createReport(appId, dataType, dataId, title);
						break;
					case InstituteMessagePushServiceImpl.CREATE_MORNING_MEETING: //新建晨会
//						createMorningMeeting(appId, dataType, dataId, title, mobile);
						break;
					case InstituteMessagePushServiceImpl.ACTIVITY_START_REMIND: //活动开始前提醒
						activityStartRemind(appId, dataType, dataId, title);
						break;
					case InstituteMessagePushServiceImpl.USER_LABEL_PUSH: //用户画像推送
//						userLabelPush(appId, dataType, dataId, title, uid, mobile);//mobile为startTime
						break;
					default:
						break;
				}
			}
		});
	}

	private void sendNoticeToSaler(String salerMobile, long uid, String appId, int clueType) {

		UserInfo clueUser = userServiceImpl.getUserInfo(uid, null, null);
		if (clueUser == null) {
			return;
		}
		String content = MessageUtil.getAssignSalerNotice(clueUser, clueType, false);
//		String remark = "";
//		String smsContent = content + remark;
//		String emailSubject = smsContent;
		sendSms(salerMobile, content);
		/*
		 * CompanyWechatConfig wechatConfig =
		 * instituteDaoImpl.getWechatConfigByAppType(appId); if (wechatConfig == null) {
		 * return; }
		 * 
		 * CompanyWechatTemplete templete =
		 * instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(),
		 * MeixConstants.WX_TEMPLETE_WHITELIST_REQ); if (templete == null) { return; }
		 * String wxTitle = "您有新的商机！";
		 * 
		 * // UserInfo userInfo = userServiceImpl.getUserInfo(0, null, salerMobile); //
		 * InsUserInfo saler = insUserService.getUserByMobileOrEmail(salerMobile, null);
		 * UserInfo userInfo = new UserInfo(); // if (saler == null) {
		 * userInfo.setMobile(salerMobile); // } else {
		 * 
		 * // } CompanyInfoVo companyInfoCache =
		 * companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode()); if
		 * (companyInfoCache == null) { return; } sendNotice2ReqUser(userInfo,
		 * companyInfoCache, wechatConfig, smsContent, content, wxTitle, emailSubject,
		 * remark);
		 */
	}

	private void joinActivitySuccess(long uid, String appId, int dataType, String title, int optType) {

		UserInfo userInfo = userServiceImpl.getUserInfo(uid, null, null);
		if (userInfo == null) {
			return;
		}
//		String wxTitle = "恭喜，您的申请已通过！";
//		String content = String.format("您申请参加的%s《%s》已通过，点击立即查看详情。", getActivityTypeDesc(dataType), title);
//
//		String remark = "";
//		if (optType == 2) {
//			wxTitle = "抱歉，您的申请未通过！";
//			content = String.format("您申请参加的%s《%s》，未获通过，请联系您的对口销售进行确认。", getActivityTypeDesc(dataType), title);
//			remark = "";
//		}
		String smsContent = getInstitutePermUserNotice(dataType, title, optType);
//		String emailSubject = smsContent;

		sendSms(userInfo.getMobile(), smsContent);
		/*
		 * CompanyWechatConfig wechatConfig =
		 * instituteDaoImpl.getWechatConfigByAppType(appId); if (wechatConfig == null) {
		 * return; }
		 * 
		 * CompanyWechatTemplete templete =
		 * instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(),
		 * MeixConstants.WX_TEMPLETE_WHITELIST_REQ); if (templete == null) { return; }
		 * CompanyInfoVo companyInfoCache =
		 * companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode()); if
		 * (companyInfoCache == null) { return; }
		 * 
		 * sendNotice2ReqUser(userInfo, companyInfoCache, wechatConfig, smsContent,
		 * content, wxTitle, emailSubject, remark);
		 */
	}

	private void createActivity(String appId, int dataType, long dataId, String title) {

		/*
		 * CompanyWechatConfig wechatConfig =
		 * instituteDaoImpl.getWechatConfigByAppType(appId); if (wechatConfig == null) {
		 * return; }
		 * 
		 * CompanyWechatTemplete templete =
		 * instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(),
		 * MeixConstants.WX_TEMPLETE_WHITELIST_REQ); if (templete == null) { return; }
		 * CompanyInfoVo companyInfoCache =
		 * companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode()); if
		 * (companyInfoCache == null) { return; }
		 */
		String activityType = getActivityTypeDesc(dataType);
//		String wxTitle = "有新的" + activityType + "信息!";
		String content = String.format("发布新%s《%s》，立即查看详情！", activityType, title);

//		String remark = "";
		String smsContent = content;
//		String emailSubject = smsContent;
		List<UserInfo> userList = userServiceImpl.getPushWechatMsgUserList(Long.valueOf(appId));
		for (UserInfo userInfo : userList) {
//			sendNotice2ReqUserByWechat(userInfo, companyInfoCache, wechatConfig,
//				smsContent, content, wxTitle, emailSubject, remark, url);
			sendSms(userInfo.getMobile(), smsContent);
		}
	}

	private void createAlbum(String appId, int dataType, long dataId, String title) {

		/*
		 * CompanyWechatConfig wechatConfig =
		 * instituteDaoImpl.getWechatConfigByAppType(appId); if (wechatConfig == null) {
		 * return; }
		 * 
		 * CompanyWechatTemplete templete =
		 * instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(),
		 * MeixConstants.WX_TEMPLETE_WHITELIST_REQ); if (templete == null) { return; }
		 * CompanyInfoVo companyInfoCache =
		 * companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode()); if
		 * (companyInfoCache == null) { return; }
		 */
//		String wxTitle = "重磅推出!";
		String content = String.format("%s《%s》已发布，请及时查看！", dataType == 0 ? "研报合辑" : "纪要合辑", title);

//		String remark = "";
		String smsContent = content;
//		String emailSubject = smsContent;
		List<UserInfo> userList = userServiceImpl.getPushWechatMsgUserList(Long.valueOf(appId));
		for (UserInfo userInfo : userList) {
//			sendNotice2ReqUserByWechat(userInfo, companyInfoCache, wechatConfig,
//				smsContent, content, wxTitle, emailSubject, remark, url);
			sendSms(userInfo.getMobile(), smsContent);
		}
	}

	private void createReport(String appId, int dataType, long dataId, String title) {

		/*
		 * CompanyWechatConfig wechatConfig =
		 * instituteDaoImpl.getWechatConfigByAppType(appId); if (wechatConfig == null) {
		 * return; }
		 * 
		 * CompanyWechatTemplete templete =
		 * instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(),
		 * MeixConstants.WX_TEMPLETE_WHITELIST_REQ); if (templete == null) { return; }
		 * CompanyInfoVo companyInfoCache =
		 * companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode()); if
		 * (companyInfoCache == null) { return; }
		 */
//		String wxTitle = "会议纪要新鲜出炉!";
		String content = String.format("新鲜出炉！会议纪要《%s》已发布，立即查看！", title);

//		String remark = "";
		String smsContent = content;
//		String emailSubject = smsContent;
		List<UserInfo> userList = userServiceImpl.getPushWechatMsgUserList(Long.valueOf(appId));
		for (UserInfo userInfo : userList) {
//			sendNotice2ReqUserByWechat(userInfo, companyInfoCache, wechatConfig,
//				smsContent, content, wxTitle, emailSubject, remark, url);
			sendSms(userInfo.getMobile(), smsContent);
		}
	}

	private void createMorningMeeting(String appId, int dataType, long dataId, String title, String url) {

		/*
		 * CompanyWechatConfig wechatConfig =
		 * instituteDaoImpl.getWechatConfigByAppType(appId); if (wechatConfig == null) {
		 * return; }
		 * 
		 * CompanyWechatTemplete templete =
		 * instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(),
		 * MeixConstants.WX_TEMPLETE_WHITELIST_REQ); if (templete == null) { return; }
		 * CompanyInfoVo companyInfoCache =
		 * companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode()); if
		 * (companyInfoCache == null) { return; }
		 */
//		String wxTitle = "新鲜出炉!";
		String content = String.format("今天的《%s》，请查收！", title);

//		String remark = "";
		String smsContent = content;
//		String emailSubject = smsContent;
		List<UserInfo> userList = userServiceImpl.getPushWechatMsgUserList(Long.valueOf(appId));
		for (UserInfo userInfo : userList) {
//			sendNotice2ReqUserByWechat(userInfo, companyInfoCache, wechatConfig,
//				smsContent, content, wxTitle, emailSubject, remark, url);
			sendSms(userInfo.getMobile(), smsContent);
		}
	}

	/**
	 * 推送给预约成功的人（短信）
	 * @param appId
	 * @param dataType
	 * @param dataId
	 * @param title
	 */
	private void activityStartRemind(String appId, int dataType, long dataId, String title) {

		/*
		 * CompanyWechatConfig wechatConfig =
		 * instituteDaoImpl.getWechatConfigByAppType(appId); if (wechatConfig == null) {
		 * return; }
		 * 
		 * CompanyWechatTemplete templete =
		 * instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(),
		 * MeixConstants.WX_TEMPLETE_WHITELIST_REQ); if (templete == null) { return; }
		 * CompanyInfoVo companyInfoCache =
		 * companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode()); if
		 * (companyInfoCache == null) { return; }
		 */
//		String wxTitle = "电话会议即将开始!";
		String content = String.format("您预约的电话会议《%s》还有十分钟就要开始啦，请及时参会。", title);

//		String remark = "";
		String smsContent = content;
//		String emailSubject = smsContent;
//		List<UserInfo> userList = userServiceImpl.getPushWechatMsgUserList(Long.valueOf(appId));
		List<InsActivityPerson> userList = insWhiteListService.getJoinPersonList(dataId, -1, 0, null, 1);
		List<String> mobiles = new ArrayList<>();
		for (InsActivityPerson userInfo : userList) {
//			sendNotice2ReqUserByWechat(userInfo, companyInfoCache, wechatConfig,
//				smsContent, content, wxTitle, emailSubject, remark, url);
			if (StringUtils.isBlank(userInfo.getMobile()) || "null".equals(userInfo.getMobile()) || userInfo.getMobile().contains("**")) {
				continue;
			}
			mobiles.add(userInfo.getMobile());
		}
		sendSms(mobiles, smsContent);
	}

	private void userLabelPush(String appId, int dataType, long dataId, String title, long uid, String startTime) {
		/*
		 * CompanyWechatConfig wechatConfig =
		 * instituteDaoImpl.getWechatConfigByAppType(appId); if (wechatConfig == null) {
		 * return; }
		 * 
		 * CompanyWechatTemplete templete =
		 * instituteDaoImpl.getCompanyWechatTemplete(wechatConfig.getCompanyCode(),
		 * MeixConstants.WX_TEMPLETE_WHITELIST_REQ); if (templete == null) { return; }
		 * CompanyInfoVo companyInfoCache =
		 * companySerciveImpl.getCompanyInfoById(wechatConfig.getCompanyCode()); if
		 * (companyInfoCache == null) { return; }
		 */

		UserInfo userInfo = userServiceImpl.getUserInfo(uid, null, null);
		if (userInfo == null) {
			return;
		}

		String wxTitle = "您可能感兴趣的内容！";
		if (userServiceImpl.isFromMyFocusedStock(uid, dataType, dataId)) {
			wxTitle += "\n您的自选股相关！";
		}
		if (userServiceImpl.isFromMyFocusedPerson(uid, dataType, dataId)) {
			wxTitle += "\n您关注的分析师有新成果！";
		}
		String content;
		if (dataType == 3) { //研报
			content = String.format("研报《%s》,新鲜出炉。及时获得最新投研成果！", title);
			//https://dev.meix.com/h5_gs/detail/report/2567740866123101/256
//			url += "detail/report/" + dataId + "/" + commonProperties.getCompanyCode();
		} else {
			content = String.format("%s《%s》，将在%s开始。请准时参加！", getActivityTypeDesc(dataType), title, startTime);
			if (dataType == 7) {
				//https://dev.meix.com/h5_gs/detail/telMeeting/256?id=2567683687110807
//				url += "detail/telMeeting/" + commonProperties.getCompanyCode() + "?id=" + dataId;
			} else {
				//https://dev.meix.com/h5_gs/detail/active/2567734519349820/256
//				url += "detail/activity/" + dataId + "/" + commonProperties.getCompanyCode();
			}
		}

//		String remark = "";
		String smsContent = wxTitle + content;
//		String emailSubject = smsContent;
//		boolean res = sendNotice2ReqUserByWechat(userInfo, companyInfoCache, wechatConfig,
//			smsContent, content, wxTitle, emailSubject, remark, url);
		String res = sendSms(userInfo.getMobile(), smsContent);
		if (res == null) {
			userServiceImpl.saveUserLabelPushRecord(uid, dataId, dataType == 3 ? 3 : 8);
		}
	}

	private String getInstitutePermUserNotice(int activityType, String title, int optType) {
		String content = null;
		if (optType == 1) {
			content = "恭喜您";
		} else {
			content = "抱歉";
		}
		content += "，您申请参加的";
		content += getActivityTypeDesc(activityType);
		content += "《" + title + "》";
		if (optType == 1) {
			content += "已通过报名审批，请确认活动相关信息，注意参加。";
		} else {
			content += "未获得通过，如有疑问，请联系您的对口销售进行确认。";
		}
		return content;
	}

	private String getActivityTypeDesc(int activityType) {
		switch (activityType) {
			case MeixConstants.CHAT_MESSAGE_DY: {
				return "调研";
			}
			case MeixConstants.CHAT_MESSAGE_LY: {
				return "路演";
			}
			case MeixConstants.CHAT_MESSAGE_CL: {
				return "策略会";
			}
			case MeixConstants.CHAT_MESSAGE_DHHY: {
				return "电话会议";
			}
			case MeixConstants.CHAT_MESSAGE_HY: {
				return "会议";
			}
			default:
				return "";
		}
	}

//	private void joinActivityChanged(String appId, int dataType, long activityId, long companyCode, String title, int flag, String messageFrom) {
//	}
//
//	private void focusedAnalystAndSelfStock(String appId, int dataType, long activityId, long companyCode, String title,
//	                                        int flag, String messageFrom, List<Long> secuMain, List<Long> analyst) {
//
//	}

	@Override
	public void pushUserConfigMessage(long id, int messageType, int receiverType, String comment, String title, String content) {
		MeixConstants.fixedThreadPool.execute(() -> {
			List<InsMessagePushResult> resList = new ArrayList<>();
//			if (receiverType == 2 || receiverType == 3 || receiverType == 4) { //白名单，潜在用户（已认证用户非白），游客（未认证用户）
			List<Integer> receiverList = getReceiverTypeList(receiverType);
			List<String> mobiles = new ArrayList<>();
			if (!receiverList.isEmpty()) {
				int accountType;
				for (int rec : receiverList) {
					if (rec == 1) {
						accountType = 2;
					} else if (rec == 2) {
						accountType = 3;
					} else if (rec == 4) {
						accountType = 4;
					} else {
						continue;
					}
					List<Map<String, Object>> list = userServiceImpl.getCustomerList(accountType, -1, null, 0, -1, null, -1, 0, 0);
					for (Map<String, Object> temp : list) {
						InsMessagePushResult vo = new InsMessagePushResult();
						String email = MapUtils.getString(temp, "email");
						String mobile = MapUtils.getString(temp, "mobile");
						long uid = MapUtils.getLongValue(temp, "id");
						vo.setCreatedAt(new Date());
						vo.setPushId(id);
						vo.setPushType(messageType);
						vo.setUid(uid);
						vo.setUpdatedAt(new Date());
						if (messageType == 1) { //邮件
							pushMessage(messageType, mobile, email, title, content, vo);
						} else if (messageType == 0) { //短信
							if (StringUtils.isNotBlank(mobile)) {
								vo.setContact(mobile);
								mobiles.add(mobile);
							} else {
								vo.setContact("未知");
								vo.setStatus(2);
								vo.setComment("手机号为空");
							}
						} else {
							vo.setContact("未知");
							vo.setStatus(2);
							vo.setComment("未知的方式");
						}
						
						resList.add(vo);
					}
				}
			}
			if (StringUtils.isNotBlank(comment)) {
				String[] comList = comment.split(",");
				for (String temp : comList) {
					InsMessagePushResult vo = new InsMessagePushResult();
					vo.setCreatedAt(new Date());
					vo.setPushId(id);
					vo.setPushType(messageType);
					vo.setUid(0L);
					vo.setUpdatedAt(new Date());
					if (messageType == 1) { //邮件
						pushMessage(messageType, temp, temp, title, content, vo);
					} else if (messageType == 0) { //短信
						if (StringUtils.isNotBlank(temp)) {
							vo.setContact(temp);
							mobiles.add(temp);
						} else {
							vo.setContact("未知");
							vo.setStatus(2);
							vo.setComment("手机号为空");
						}
					} else {
						if (StringUtils.isNotBlank(temp)) {
							vo.setContact(temp);
						} else {
							vo.setContact("未知");
						}
						vo.setStatus(2);
						vo.setComment("未知的方式");
					}
					resList.add(vo);
				}
			}
			if (!mobiles.isEmpty()) {
				pushMessage(mobiles, content, resList);
			}
			pushResultMapper.insertList(resList);
			InsMessagePushConfig config = new InsMessagePushConfig();
			config.setId(id);
			config.setPushStatus(1);
			pushConfigMapper.updateByPrimaryKeySelective(config);
		});
	}

	private void pushMessage(List<String> mobiles, String content, List<InsMessagePushResult> resList) {
		String res = sendSms(mobiles, content);
		int status = 1;
		String comment = "";
		if (res == null) {
			//success
			status = 1;
		} else {
			status = 2;
			comment = res.length() > 200 ? res.substring(0, 200) : res;
		}
		for (InsMessagePushResult tmp : resList) {
			tmp.setStatus((tmp.getStatus() == null || tmp.getStatus() == 0) ? status : tmp.getStatus());
			tmp.setComment(tmp.getComment() != null ? tmp.getComment() : comment);
		}
	}
	private void pushMessage(int messageType, String mobile, String email, String title, String content, InsMessagePushResult vo) {
		if (messageType == 0 && StringUtils.isNotBlank(mobile)) {
//			vo.setContact(mobile);
//			String res = sendSms(mobile, content);
//			if (res == null) {
//				//success
//				vo.setStatus(1);
//			} else {
//				vo.setStatus(2);
//				vo.setComment(res == null ? "未知错误！" : res.length() > 200 ? res.substring(0, 200) : res);
//			}
		} else if (messageType == 1 && StringUtils.isNotBlank(email)) {
			vo.setContact(email);
			String res = messageServiceImpl.sendEmailWithResult(email, title, content);
			if (res == null) {
				//success
				vo.setStatus(1);
			} else {
				vo.setStatus(2);
				vo.setComment(res == null ? "未知错误！" : res.length() > 200 ? res.substring(0, 200) : res);
			}
		} else {
			vo.setContact("未知");
			vo.setStatus(2);
			vo.setComment(messageType == 0 ? "手机号为空" : messageType == 1 ? "邮箱为空" : "未知的方式");
		}
	}

	@Override
	public void pushActivityAuditRes(int auditRes, String title, String reason, String startTime, String uuid) {
		MeixConstants.fixedThreadPool.execute(() -> {
			StringBuffer sb = new StringBuffer();
			sb.append("您发起的电话会议《");
			sb.append(title);
			if (auditRes == 1) {
				sb.append("》已预约成功，将于");
				sb.append(startTime);
				sb.append("如期召开，请悉知。");
			} else {
				sb.append("》预约失败，失败原因：");
				sb.append(reason);
				sb.append("，请前往修改。");
			}
			if (auditRes == 1 || auditRes == 2) {
				InsUser userInfo = insUserService.getUserInfo(uuid);
				if (userInfo == null) {
					return;
				}
				if (StringUtil.isNotEmpty(userInfo.getEmail())) {
					String emailContent = sb.toString();
					messageServiceImpl.sendEmailWithResult(userInfo.getEmail(), auditRes == 1 ? "活动审核通过通知" : "活动审核不通过通知", emailContent, true);
				}
				sendSms(userInfo.getMobile(), sb.toString());
			}
		});
	}

	@Override
	public void pushBecomeWhiteList(List<String> mobiles) {
		MeixConstants.fixedThreadPool.execute(() -> {
//			List<String> mobiles = new ArrayList<>();
//			for (Object tmp : data) {
//				JSONObject obj = JSONObject.fromObject(tmp);
//				if (obj.containsKey("mobile")) {
//					String mobile = obj.getString("mobile");
//					if (mobile == null || "".equals(mobile) || "null".equals(mobile)) {
//						continue;
//					}
//					mobiles.add(mobile);
//				}
//			}
			sendSms(mobiles, "恭喜您，您已成为白名单用户，享受众多研究资源权限。");
		});
	}
	
	private String sendSms(String mobile, String content) {
		if (StringUtils.isBlank(mobile) || "null".equals(mobile) || mobile.contains("**")) {
			return "手机号为空！";
		}
		log.info("smsSend:[{}][{}]", mobile, content);
//		return null;
		return smsService.sendSMS(mobile, content);
	}
	
	private String sendSms(List<String> mobiles, String content) {
		if (mobiles == null || mobiles.isEmpty()) {
			return "手机号为空！";
		}
		log.info("smsSend:[{}]", content);
//		return null;
		return smsService.sendSMS(mobiles, content);
	}
}
