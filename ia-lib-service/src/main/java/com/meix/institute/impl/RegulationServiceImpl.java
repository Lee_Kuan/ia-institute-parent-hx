package com.meix.institute.impl;

import com.meix.institute.api.ICompanyMonthlyStockService;
import com.meix.institute.api.IRecommendDataService;
import com.meix.institute.api.IRegulationService;
import com.meix.institute.api.IStockHighYieldService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.mapper.HomeDaoImpl;
import com.meix.institute.mapper.MeetingMapper;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.vo.HoneyBeeBaseVo;
import com.meix.institute.vo.meeting.MorningMeetingVo;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by zenghao on 2019/10/10.
 */
@Service
public class RegulationServiceImpl implements IRegulationService {
	Logger log = LoggerFactory.getLogger(RegulationServiceImpl.class);

	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IStockHighYieldService stockHighYieldService;
	@Autowired
	private ICompanyMonthlyStockService companyMonthlyStockService;
	@Autowired
	private HomeDaoImpl homeDao;
	@Autowired
	private IRecommendDataService recommendDataService;
	@Autowired
	private MeetingMapper meetingMapper;

	@Override
	public List<HoneyBeeBaseVo> getHotRecommendList(int currentPage, int showNum, Set<Integer> selfStockSet, int issueType) {
		long startTime = System.currentTimeMillis();
		List<HoneyBeeBaseVo> hotList = homeDao.getLastWeekHotRecommend(currentPage, showNum, issueType);
		if (currentPage == 0 && hotList.size() < showNum) {
			//第一页数据不足时，填充一周之前数据
			showNum = showNum - hotList.size();
			log.info("一周内热门数据仅有{}条，填充一周前数据{}条", hotList.size(), showNum);
			List<HoneyBeeBaseVo> oldList = homeDao.getHistoryHotRecommend(currentPage, showNum, issueType);
			hotList.addAll(oldList);
		}
		if (currentPage == 0 && showNum > 5 && hotList.size() > 5) {
			//查看更多时，需要筛选阅读数大于10的数据，且要保留首页的5条数据
			int size = hotList.size();
			for (int i = size - 1; i >= 5; i--) {
				HoneyBeeBaseVo item = hotList.get(i);
				if (item.getRanking() >= 10d) {
					break;
				} else {
					hotList.remove(i);
				}
			}
		}
		for (HoneyBeeBaseVo vo : hotList) {
			try {
				List<Long> ids = new ArrayList<>();
				ids.add(vo.getDataId());
				Map<String, Object> item = recommendDataService.getRecommendDataByIds(vo.getDataType(), ids, 0, selfStockSet);
				if (item == null) {
					continue;
				}
				if (MeixConstants.USER_YB == vo.getDataType()) {
					String title = MapUtils.getString(item, "title", "");
					String from = MapUtils.getString(item, "from", "");
					vo.setTitle(title);
					vo.setFrom(from);
				}
				item.put("dataId", vo.getDataId());
				item.put("dataType", vo.getDataType());
				vo.setItem(item);
			} catch (Exception e) {
				log.error("", e);
			}
		}
		log.info("获取热点推荐数据列表耗时{}ms", (System.currentTimeMillis() - startTime));
		return hotList;
	}

	/**
	 * 自选股大幅日涨跌、券商月度金股
	 *
	 * @param uid
	 * @return
	 */
	private List<HoneyBeeBaseVo> getWxMpSpecialList(long uid, Set<Integer> selfStockSet) {
		List<HoneyBeeBaseVo> specialList = new ArrayList<>();

		long startTime = System.currentTimeMillis();
		List<HoneyBeeBaseVo> companyStockList = companyMonthlyStockService.getList(uid, commonServerProperties.getCompanyCode(), selfStockSet);
		log.info("首页月度金股耗时{}ms", (System.currentTimeMillis() - startTime));
		if (companyStockList != null && companyStockList.size() > 0) {
			log.info("【RegulationServiceImpl】:券商月度金股数据筛选剩余{}条", companyStockList.size());
			specialList.addAll(companyStockList);
		}

		startTime = System.currentTimeMillis();
		List<HoneyBeeBaseVo> highDayYieldList = stockHighYieldService.getList(uid, selfStockSet);
		log.info("首页日收益超过3%自选股耗时{}ms", (System.currentTimeMillis() - startTime));
		if (highDayYieldList != null && highDayYieldList.size() > 0) {
			log.info("【RegulationServiceImpl】:股票日涨跌超过3%数据筛选剩余{}条", highDayYieldList.size());
			specialList.addAll(highDayYieldList);
		}
		return specialList;
	}

	@Override
	@Transactional
	public List<HoneyBeeBaseVo> getRecommendList(long uid, int currentPage, int showNum, String lastId, Set<Integer> selfStockSet, int issueType) {
		long startTime = System.currentTimeMillis();
		log.info("进入首页查询列表");
		List<HoneyBeeBaseVo> waterFlowList = homeDao.getSysWaterflowList(uid, currentPage * showNum, showNum, issueType);
		log.info("结束首页查询列表");
		Map<Integer, List<Long>> dataTypeMap = new HashMap<>();
		for (HoneyBeeBaseVo vo : waterFlowList) {
			List<Long> idList = null;
			if (dataTypeMap.containsKey(vo.getDataType())) {
				idList = dataTypeMap.get(vo.getDataType());
			} else {
				idList = new ArrayList<>();
				dataTypeMap.put(vo.getDataType(), idList);
			}
			idList.add(vo.getDataId());
		}
		Map<String, Map<String, Object>> detailMap = new HashMap<>();
		for (Map.Entry<Integer, List<Long>> entry : dataTypeMap.entrySet()) {
			Map<String, Map<String, Object>> map = recommendDataService.getRecDataMapByIds(entry.getKey(), entry.getValue(), uid, selfStockSet);
			detailMap.putAll(map);
		}
		for (HoneyBeeBaseVo vo : waterFlowList) {
			try {
				Map<String, Object> item = detailMap.get(vo.getDataType() + "_" + vo.getDataId());
				if (item == null) {
					continue;
				}
				if (MeixConstants.USER_YB == vo.getDataType()) {
					String title = MapUtils.getString(item, "title", "");
					String from = MapUtils.getString(item, "from", "");
					vo.setTitle(title);
					vo.setFrom(from);
				}
				item.put("dataId", vo.getDataId());
				item.put("dataType", vo.getDataType());
				vo.setItem(item);
			} catch (Exception e) {
				log.error("", e);
			}
		}
		List<HoneyBeeBaseVo> specialList = getWxMpSpecialList(uid, selfStockSet);
		insertSpecialList(currentPage, waterFlowList, specialList);
		insertMeetingData(currentPage, waterFlowList);
		log.info("获取推荐数据列表耗时{}ms", (System.currentTimeMillis() - startTime));
		return waterFlowList;
	}

	/**
	 * 插入股票数据
	 */
	private void insertSpecialList(int currentPage, List<HoneyBeeBaseVo> waterFlowList, List<HoneyBeeBaseVo> specialList) {
		if (waterFlowList == null || specialList == null || specialList.size() == 0) {
			return;
		}

		if (waterFlowList.size() == 0) {
			if (currentPage == 0) {
				//第一页没数据，全量插入
				waterFlowList.addAll(specialList);
			}
			return;
		}
		List<HoneyBeeBaseVo> tempList = new ArrayList<>();
		int j = currentPage * 3;
		int mj = j + 3;
		for (int i = 0; i < waterFlowList.size(); i++) {
			tempList.add(waterFlowList.get(i));
			if ((i + 1) % 3 == 0 && j < mj && j < specialList.size()) {
				tempList.add(specialList.get(j));
				j++;
			}
		}
		waterFlowList.clear();
		waterFlowList.addAll(tempList);

//		if (currentPage == 0) {
//			double endScore = 0;
//			String endTime = "";//最后一条数据得分
//			if (waterFlowList.get(waterFlowList.size() - 1).getRanking() > 0) {
//				//第一页数据最后得分大于0
//				endScore = waterFlowList.get(waterFlowList.size() - 1).getRanking();
//			} else {
//				endTime = waterFlowList.get(waterFlowList.size() - 1).getApplyTime();
//			}
//			for (HoneyBeeBaseVo special : specialList) {
//				if (endScore > 0) {
//					if (special.getRanking() >= endScore) {
//						//得分在第一页数据之前
//						waterFlowList.add(special);
//					}
//				} else if (endScore == 0) {
//					//最后一个数据得分为0，加入得分大于0的或者更新时间大于endTime的
//					if (special.getRanking() > 0 || special.getApplyTime().compareTo(endTime) >= 0) {
//						waterFlowList.add(special);
//					}
//				}
//			}
//		} else {
//			double startScore = waterFlowList.get(0).getRanking();
//			double endScore = waterFlowList.get(waterFlowList.size() - 1).getRanking();
//			String startTime = waterFlowList.get(0).getApplyTime();
//			String endTime = waterFlowList.get(waterFlowList.size() - 1).getApplyTime();
//			for (HoneyBeeBaseVo special : specialList) {
//				if (startScore > 0 && endScore > 0) {
//					//第一个数据和最后一个数据得分都大于0
//					if (special.getRanking() < startScore && special.getRanking() >= endScore) {
//						waterFlowList.add(special);
//					}
//				} else if (startScore > 0 && endScore == 0) {
//					//第一个数据得分大于0，最后一个数据得分等于0
//					if (special.getRanking() < startScore && special.getRanking() > 0) {
//						waterFlowList.add(special);
//					} else if (special.getRanking() == 0 && special.getApplyTime().compareTo(endTime) >= 0) {
//						waterFlowList.add(special);
//					}
//				} else if (startScore == 0 && endScore == 0) {
//					//所有数据得分都为0
//					if (special.getApplyTime().compareTo(startTime) < 0 && special.getApplyTime().compareTo(endTime) >= 0) {
//						waterFlowList.add(special);
//					}
//				}
//			}
//		}
//		//排序
//		waterFlowList.sort(comparator);
	}

	/**
	 * 插入当天的一条晨会
	 *
	 * @param currentPage
	 * @param waterFlowList
	 */
	private void insertMeetingData(int currentPage, List<HoneyBeeBaseVo> waterFlowList) {
		if (waterFlowList == null) {
			return;
		}
		if (currentPage != 0) {
			//仅在第一页插入
			return;
		}
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		if (hour < 7 || hour >= 10) {
			//仅在7点到10点之间插入
			return;
		}
		MorningMeetingVo morningMeetingVo = meetingMapper.getCurDateMeeting(commonServerProperties.getCompanyCode(), MeixConstants.ISSUETYPE_APPLET);
		if (morningMeetingVo != null) {
			HoneyBeeBaseVo vo = new HoneyBeeBaseVo();
			vo.setDataId(morningMeetingVo.getId());
			vo.setDataType(MeixConstants.USER_CH);
			vo.setApplyTime(DateUtil.getCurrentDateTime());
			vo.setStatus(1);
			Map<String, Object> item = GsonUtil.json2Map(GsonUtil.obj2Json(morningMeetingVo));
			item.put("dataType", MeixConstants.USER_CH);
			vo.setItem(item);
			waterFlowList.add(0, vo);
		}
	}
}
