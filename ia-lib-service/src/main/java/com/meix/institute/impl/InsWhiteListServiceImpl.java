package com.meix.institute.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.apache.ibatis.session.RowBounds;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.meix.institute.BaseService;
import com.meix.institute.api.ICompanyService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IInsLoginService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.IInsWhiteListService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IUserService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsActivity;
import com.meix.institute.entity.InsActivityPerson;
import com.meix.institute.entity.InsCompanyInstituteUserActivity;
import com.meix.institute.entity.InsUser;
import com.meix.institute.mapper.CustomerGroupMapper;
import com.meix.institute.mapper.InsActivityMapper;
import com.meix.institute.mapper.InsActivityPersonMapper;
import com.meix.institute.mapper.InsCompanyInstituteUserActivityMapper;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.user.UserInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * @author likuan
 * @date 2019年7月1日14:18:59
 * @describe 研究所
 */
@Service
public class InsWhiteListServiceImpl extends BaseService implements IInsWhiteListService {
	protected final Logger log = LoggerFactory.getLogger(InsWhiteListServiceImpl.class);
	@Autowired
	private InsActivityPersonMapper personMapper;
	@Autowired
	private InsActivityMapper activityMapper;
	@Autowired
	private IInsUserService insUserService;
	@Autowired
	private IUserService userServiceImpl;
	@Autowired
	private IInstituteMessagePushService messagePush;
	@Autowired
	private InsActivityMapper insActivityMapper;
	@Autowired
	private InsCompanyInstituteUserActivityMapper activityUserMapper;
	@Autowired
	private ICompanyService companyServiceImpl;
	@Autowired
	private ICustomerGroupService customerGroupService;
	@Autowired
	private CustomerGroupMapper groupMapper;
	@Autowired
	private IInsLoginService insLoginService;


	@Override
	public List<String> saveWhiteUserInfo(String uuid, JSONArray datas, long groupId) {
		return saveWhiteUserInfo(uuid, datas, groupId, null);
	}

	@Override
	public List<String> saveWhiteUserInfo(String uuid, JSONArray datas, long groupId, XSSFSheet sheet) {
		List<String> errInfo = new ArrayList<>();
		String info = "备注";
		String nowDate = DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm:ss");
		List<String> mobiles = new ArrayList<>();
		for (int i = 0; i < datas.size(); i++) {
			try {
				if (sheet != null) {
					sheet.getRow(i).createCell(7).setCellValue(info);
				}
				JSONObject obj = (JSONObject) datas.get(i);
				UserInfo user = new UserInfo();
				user.setId(MapUtils.getLongValue(obj, "id", 0));
				long uid = user.getId();
				user.setAccountType(MeixConstants.ACCOUNT_WL);
				user.setAuthStatus(MeixConstants.CUSTOMER_AUTH_SUCCESS);
				info = "成功";
				
				String userName = obj.getString("userName");
				if (userName == null || "".equals(userName) || "null".equals(userName)) {
					errInfo.add("第" + (i + 1) + "条姓名为空！");
					info = "姓名为空";
					continue;
				}
				user.setUserName(userName);
				String position = MapUtils.getString(obj, "position", "");
				user.setPosition(position);
				if (position == null || "".equals(position) || "null".equals(position)) {
					user.setPosition("");
				}
				String mobile = obj.getString("mobile");
				if (mobile == null || "".equals(mobile) || "null".equals(mobile)) {
					errInfo.add("第" + (i + 1) + "条电话为空！");
					info = "电话为空";
					continue;
				}
				user.setMobile(mobile);
				user.setPhoneNumber(MapUtils.getString(obj, "phoneNumber", ""));
				String companyName = obj.getString("companyName");
				long companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
				if (companyCode == 0 && (companyName == null || "".equals(companyName) || "null".equals(companyName))) {
					errInfo.add("第" + (i + 1) + "条公司为空！");
					info = "公司为空";
					continue;
				}
				if (companyCode == 0) {
				CompanyInfoVo companyVo = companyServiceImpl.getCompanyInfoByName(companyName);
					if (companyVo == null) {
						companyVo = new CompanyInfoVo();
						companyVo.setCompanyAbbr(companyName);
						companyVo.setCompanyName(companyName);
						companyVo.setCreateTime(nowDate);
						companyVo.setUpdateTime(nowDate);
						companyVo.setChiSpelling(PinyinHelper.getShortPinyin(companyName).toUpperCase());
						companyCode = companyServiceImpl.addCompanyInfo(companyVo);
					} else {
						companyCode = companyVo.getId();
					}
				}
				user.setCompanyCode(companyCode);
				String email = MapUtils.getString(obj, "email", "");
//					if (email == null || "".equals(email) || "null".equals(email)) {
//						errInfo.add("第" + (i + 1) + "条邮箱为空！");
//						info = "邮箱为空";
//						continue;
//					}
				user.setEmail(email);
				String salerName = MapUtils.getString(obj, "salerName");
				long salerUid = MapUtils.getLongValue(obj, "salerId", 0);
				if (salerUid == 0) {
					if (StringUtils.isNotBlank(salerName)) {
						UserInfo userInfo = userServiceImpl.getUserInfoByUserName(salerName);
						if (userInfo != null) {
							salerUid = userInfo.getId();
						}
					}
				}
				user.setSalerUid(salerUid);
				//用户本身公司名称
				user.setCompanyName(companyName);
				user.setCompanyAbbr(companyName);
				//用户所属研究所的ID
				//			user.setCompanyCode(companyCode);
				user.setUpdateTime(nowDate);
				if (user.getId() > 0) {
					UserInfo userInfo = userServiceImpl.getUserInfo(user.getId(), null, null);
					if (userInfo != null && userInfo.getAccountType() != MeixConstants.ACCOUNT_WL) {
						mobiles.add(user.getMobile()); //成为白名单
					}
					userServiceImpl.updateUser(user);
				} else {
					
					user.setCreateTime(nowDate);
					uid = userServiceImpl.addUser(user, true);
					mobiles.add(user.getMobile()); //新增成为白名单
					if (uid < 0) {
						info = "手机号已存在";
						errInfo.add("第" + (i + 1) + "条手机号已存在！");
						continue;
					} else {
						info = "成功";
					}
				}
				if (groupId > 0) {
					customerGroupService.saveUserGroup(uid, groupId, 0, uuid);
				}
			} catch (Exception e) {
				log.error("", e);
				errInfo.add("第" + (i + 1) + "条未知错误！");
				info = "未知错误";
			}
		}
		if (sheet != null) {
			sheet.getRow(datas.size()).createCell(7).setCellValue(info);
		} else { //手动添加成为白名单的发送推送（sheet == null）已有用户成为白名单、新增成为白名单
			messagePush.pushBecomeWhiteList(mobiles);
		}
		errInfo.add(0, info);
		return errInfo;
	}

	@Override
	public List<String> saveJoinPerson(String uuid, long activityId, JSONArray datas) {
		return saveJoinPerson(uuid, activityId, datas, null);
	}

	@Override
	public List<String> saveJoinPerson(String uuid, long activityId, JSONArray datas, XSSFSheet sheet) {
		List<String> errInfo = new ArrayList<>();
		InsUser insUser = insUserService.getUserInfo(uuid);
		if (insUser == null) {
			errInfo.add("非系统用户无权限操作");
			return errInfo;
		}

		String info = "备注";
		List<InsActivityPerson> personList = new ArrayList<>();
		for (int i = 0; i < datas.size(); i++) {
			try {
				if (sheet != null) {
					sheet.getRow(i).createCell(5).setCellValue(info);
				}
				JSONObject dataObj = datas.getJSONObject(i);

				long id = MapUtils.getLongValue(dataObj, "id", 0);
				String userName = MapUtils.getString(dataObj, "userName", null);
				if (StringUtils.isBlank(userName)) {
					errInfo.add("第" + (i + 1) + "条姓名为空");
					info = "姓名为空";
					continue;
				}

				String mobile = MapUtils.getString(dataObj, "mobile", null);
				if (StringUtils.isBlank(mobile)) {
					errInfo.add("第" + (i + 1) + "条手机号为空");
					info = "手机号为空";
					continue;
				}
				Long uid = MapUtils.getLongValue(dataObj, "uid", 0);
				if (uid == 0) {
					UserInfo user = userServiceImpl.getUserInfo(0, null, mobile);
					if (user != null) {
						uid = user.getId();
					}
				}
				String companyName = MapUtils.getString(dataObj, "companyName", null);
				if (StringUtils.isBlank(companyName)) {
					errInfo.add("第" + (i + 1) + "条公司为空");
					info = "公司为空";
					continue;
				}
				String salerName = MapUtils.getString(dataObj, "salerName");
				long salerUid = MapUtils.getLongValue(dataObj, "salerId", 0);
				if (salerUid == 0) {
					if (StringUtils.isNotBlank(salerName)) {
						UserInfo userInfo = userServiceImpl.getUserInfoByUserName(salerName);
						if (userInfo != null) {
							salerUid = userInfo.getId();
						}
					}
				}

				String position = MapUtils.getString(dataObj, "position", null);
				InsActivityPerson person = new InsActivityPerson();
				person.setUid(uid);
				person.setSalerId(salerUid);
				person.setActivityId(activityId);
				person.setUserName(userName);
				person.setMobile(mobile);
				person.setCompanyName(companyName);
				person.setPosition(position);
				person.setType(1);    //代为报名
				person.setStatus(1);    //代为报名，审核通过
				person.setOperatorId(uuid);
				person.setOperatorName(insUser.getUserName());
				person.setSalerName(salerName);

				person.setUpdatedAt(new Date());
				person.setUpdatedBy(uuid);

				if (id == 0) {
					person.setCreatedAt(new Date());
					person.setCreatedBy(uuid);
					personList.add(person);
				} else {
					person.setId(id);
					personMapper.updateByPrimaryKeySelective(person);
				}
				//发通知
				UserInfo user = userServiceImpl.getUserInfo(0, null, mobile);
				InsActivity activity = insActivityMapper.selectByPrimaryKey(person.getActivityId());
				if (user != null && activity != null) {
					messagePush.messagePush(insUser.getCompanyCode(), user.getId(), 2, activity.getTitle(), activity.getActivityType(), 1, null);
				}
				info = "成功";
			} catch (Exception e) {
				info = "未知错误";
			}
		}
		if (sheet != null) {
			sheet.getRow(datas.size()).createCell(5).setCellValue(info);
		}
		if (personList.size() > 0) {
			this.addJoinPerson(personList);
		}

		return errInfo;
	}

	private void addJoinPerson(List<InsActivityPerson> personList) {
		personMapper.insertList(personList);
	}

	@Override
	public List<InsActivityPerson> getJoinPersonList(long activityId, int showNum, int currentPage, String condition, int status) {
		Example example = new Example(InsActivityPerson.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("activityId", activityId);
		if (StringUtils.isNotEmpty(condition)) {
			criteria.andLike("userName", "%" + condition + "%");
		}
		if (status > -1) {
			criteria.andEqualTo("status", status);
		}
		example.orderBy("createdAt").desc();
		if (showNum > 0) {
			RowBounds rowBounds = new RowBounds(showNum * currentPage, showNum);
			return personMapper.selectByExampleAndRowBounds(example, rowBounds);
		} else {
			return personMapper.selectByExample(example);
		}
	}

	@Override
	public int getJoinPersonCount(long activityId, String condition) {
		Example example = new Example(InsActivityPerson.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("activityId", activityId);
		if (StringUtils.isNotEmpty(condition)) {
			criteria.andLike("userName", "%" + condition + "%");
		}
		int count = personMapper.selectCountByExample(example);
		return count;
	}

	@Override
	public String getActivityTitle(long activityId) {
		InsActivity activity = activityMapper.selectByPrimaryKey(activityId);
		if (activity != null) {
			return activity.getTitle();
		}
		return "";
	}

	@Override
	public void addActivityUserInfo(List<InsCompanyInstituteUserActivity> userList) {
		activityUserMapper.insertList(userList);
	}

	@Override
	public void deleteActivityUser(long activityId) {
		Example example = new Example(InsCompanyInstituteUserActivity.class);
		example.createCriteria().andEqualTo("activityId", activityId);
		activityUserMapper.deleteByExample(example);
	}

	@Override
	public void deleteWhiteUser(String uuid, long companyCode, JSONArray ids, long groupId) {
		if (ids != null && !ids.isEmpty()) {
			for (Object tmp : ids) {
				long uid = Long.valueOf(String.valueOf(tmp));
				if (groupId > 0) { //从组内删除
					groupMapper.deleteCustomerFromGroup(uid, groupId);
				} else { //未传入组信息,则进行账号封禁操作
					groupMapper.deleteCustomerFromAllGroup(uid);
					UserInfo userInfo = getUserInfo(uid);
					if (userInfo != null) {
						if (userInfo.getAccountType() == MeixConstants.ACCOUNT_WL) {
							userServiceImpl.deleteWhiteUser(uuid, uid, 3);
						} else if (userInfo.getAccountType() == MeixConstants.ACCOUNT_INNER) {
							insLoginService.logout(userInfo.getMobile(), 0);
							userServiceImpl.deleteUser(uid, uuid);
							if (StringUtils.isNotBlank(userInfo.getInsUuid())) {
								insLoginService.logout(userInfo.getInsUuid(), 0);
								insUserService.deleteUser(userInfo.getInsUuid(), uuid);
							}
						} else if (userInfo.getAccountType() == MeixConstants.ACCOUNT_NORMAL) {
							insLoginService.logout(userInfo.getMobile(), 0);
							userServiceImpl.deleteUser(uid, uuid);
						}
					}
				}
			}
		}
	}

	@Override
	public List<InsCompanyInstituteUserActivity> getActivityUserList(long activityId) {
		Example example = new Example(InsCompanyInstituteUserActivity.class);
		Criteria cri = example.createCriteria();
		cri.andEqualTo("activityId", activityId);
		Criteria exp = example.and();
		exp.andIsNull("expireTime").orGreaterThanOrEqualTo("expireTime", new Date());
		return activityUserMapper.selectByExample(example);
	}

}
