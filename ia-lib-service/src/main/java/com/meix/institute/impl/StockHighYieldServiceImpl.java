package com.meix.institute.impl;

import com.meix.institute.BaseService;
import com.meix.institute.api.ISecumainService;
import com.meix.institute.api.ISelfStockService;
import com.meix.institute.api.IStockHighYieldService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsSelfStock;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.search.StockYieldSearchVo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.MathUtil;
import com.meix.institute.vo.HoneyBeeBaseVo;
import com.meix.institute.vo.stock.StockYieldVo;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by zenghao on 2019/7/3.
 */
@Service
public class StockHighYieldServiceImpl extends BaseService implements IStockHighYieldService {
	private static Logger log = LoggerFactory.getLogger(StockHighYieldServiceImpl.class);

	@Autowired
	private ISecumainService secumainService;
	@Autowired
	private ISelfStockService selfStockService;

	private ConcurrentLinkedQueue<Long> waterflowIdList = new ConcurrentLinkedQueue<>();
	protected static final String CACHE_KEY = "StockHighYieldCache";
	protected static CacheManager cacheManager = null;
	protected Cache cache = null;
	private String updateTime = null;

	protected boolean isRefreshing = false;

	protected String getCacheName() {
		return CACHE_KEY;
	}

	@Override
	public void refreshCache() {
		if (isRefreshing) {
			log.debug("[{}] 正在缓存......", getCacheName());
			return;
		}

		isRefreshing = true;

		if (cacheManager == null) {
			cacheManager = CacheManager.newInstance(this.getClass().getResource("/ehcache.xml"));
		}
		if (cache == null) {
			cache = cacheManager.getCache(getCacheName());
		}

		Thread thread = new Thread() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				log.debug("[{}] 开始缓存......", getCacheName());
				try {
					doRefreshCache();
				} catch (Exception e) {
					log.error("", e);
				} finally {
					isRefreshing = false;
				}
				log.debug("[{}] 结束缓存...... 耗时:{}ms, cacheSize:{}", getCacheName(),
					(System.currentTimeMillis() - startTime), cache.getSize());
			}
		};
		thread.start();
	}

	protected void doRefreshCache() throws Exception {
		try {
			log.info("[{}] doRefreshCache updateTime:{}", getCacheName(), updateTime);
			List<HoneyBeeBaseVo> list = getStockHighYieldList();
			if (list == null) {
				return;
			}
			log.info("[{}] doRefreshCache listSize:{}", getCacheName(), list.size());

			//第二天更新数据时清空历史数据
			if (updateTime != null && updateTime.compareTo(DateUtil.getCurrentDate()) < 0) {
				cache.removeAll();
				waterflowIdList.clear();
			}

			for (int i = 0; i < list.size(); i++) {
				HoneyBeeBaseVo vo = list.get(i);
				String curUpdateTime = vo.getUpdateTime();
				if (curUpdateTime != null && (updateTime == null || curUpdateTime.compareTo(updateTime) > 0)) {
					updateTime = curUpdateTime;
				}
				long id = vo.getDataId();
				if (vo.getStatus() != 1) {
					removeElement(id);
					waterflowIdList.remove(id);
					log.debug("[{}] doRefreshCache remove id:{}", getCacheName(), id);
				} else {
					if (waterflowIdList.contains(id)) {
						continue;
					}
					addElement(id, vo);
					waterflowIdList.add(id);
					log.debug("[{}] doRefreshCache add id:{}", getCacheName(), id);
				}
			}
			list.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<HoneyBeeBaseVo> getList(long uid, Set<Integer> selfstockSet) {
		List<HoneyBeeBaseVo> voList = new ArrayList<>();
		List<StockYieldSearchVo> searchList = new ArrayList<>();
		for (Long id : waterflowIdList) {
			HoneyBeeBaseVo vo = getElement(id);
			if (vo == null) {
				continue;
			}

			if (!selfstockSet.contains(id.intValue())) {
				continue;
			}

			try {
				HoneyBeeBaseVo copyVo = (HoneyBeeBaseVo) BeanUtils.cloneBean(vo);
//				log.info("数据id : {}", vo.getDataId());
				copyVo.getItem().put("authorCode", uid);
				copyVo.getItem().put("isSelfstock", 1);
				int innerCode = new Long(vo.getDataId()).intValue();
				InsSelfStock selfStockVo = selfStockService.getSelfStock(uid, innerCode);
				if (selfStockVo != null) {
					String createTime = DateUtil.dateToStr(selfStockVo.getCreatedAt(), DateUtil.dateFormat);
					copyVo.getItem().put("createTime", createTime);
					String startDate = createTime.substring(0, 10);
					if (!startDate.equals(DateUtil.getCurrentDate())) {//如果是当天加入自选股，不计算收益
						Calendar calendarEnd = Calendar.getInstance();
						String endDate = DateUtil.dateToStr(calendarEnd.getTimeInMillis(), "yyyy-MM-dd");

						StockYieldSearchVo searchVo = new StockYieldSearchVo();
						searchVo.setInnerCode(innerCode);
						searchVo.setStartDate(startDate);
						searchVo.setEndDate(endDate);
						searchList.add(searchVo);
					}
				}
				voList.add(copyVo);
				int industryCode = MapUtils.getIntValue(copyVo.getItem(), "industryCode", 0);
				float score = insUserService.getUserLabelTotalScore(uid, innerCode, industryCode);
				copyVo.setRanking(score);
			} catch (Exception e) {
				log.error("", e);
			}
		}

		Map<Integer, BigDecimal> yieldMap = getStockIntervalYield(searchList);
		if (yieldMap != null) {
			for (HoneyBeeBaseVo vo : voList) {
				BigDecimal stockYield = yieldMap.get(new Long(vo.getDataId()).intValue());
				if (stockYield != null) {
					Map<String, Object> item = vo.getItem();
					item.put("accumulatedYieldRate", stockYield.setScale(4, BigDecimal.ROUND_HALF_UP).floatValue());
				}
			}
		}
		return voList;
	}

	/**
	 * 获取股票日收益快速上涨数据
	 *
	 * @return
	 */
	public List<HoneyBeeBaseVo> getStockHighYieldList() {
		List<InsSelfStock> list = selfStockService.getSelfStockList(-1, 0, 0);
		if (list != null && list.size() > 0) {
			List<HoneyBeeBaseVo> result = new ArrayList<>();
			log.info("[getStockHighYieldList] list size:{}", list.size());

			List<StockYieldSearchVo> searchList = new ArrayList<>();

			String curDate = DateUtil.getCurrentDate();

			for (InsSelfStock insSelfStock : list) {
				StockYieldSearchVo searchVo = new StockYieldSearchVo();
				searchVo.setInnerCode(insSelfStock.getInnerCode());
				searchVo.setStartDate(curDate);
				searchVo.setEndDate(curDate);
				searchList.add(searchVo);
			}

			List<StockYieldVo> yieldList = new ArrayList<>();

			Map<Integer, BigDecimal> yieldMap = getStockIntervalYield(searchList);
			if (yieldMap != null) {
				for (InsSelfStock insSelfStock : list) {
					BigDecimal stockYield = yieldMap.get(insSelfStock.getInnerCode());
					if (stockYield != null &&
						(stockYield.compareTo(new BigDecimal(0.03)) >= 0 ||
							stockYield.compareTo(new BigDecimal(-0.03)) <= 0)) {
						StockYieldVo item = new StockYieldVo();
						item.setInnerCode(insSelfStock.getInnerCode());
						item.setYieldRate(stockYield);
						yieldList.add(item);
					}
				}
			}

			for (StockYieldVo yieldVo : yieldList) {
				SecuMain cache = secumainService.getSecuMain(yieldVo.getInnerCode());
				if (cache == null) {
//					log.info("股票{}没有缓存数据", yieldVo.getInnerCode());
					continue;
				}
				HoneyBeeBaseVo vo = new HoneyBeeBaseVo();
				vo.setDataId(yieldVo.getInnerCode());
				vo.setDataType(MeixConstants.USER_GP);
				vo.setApplyTime(DateUtil.getCurrentDateTime());
				vo.setStatus(1);
				vo.setWaterflowRecType(1);//1 自选股推荐 2 月度金股推荐
				if (yieldVo.getYieldRate().compareTo(BigDecimal.ZERO) > 0) {
					vo.setContent("<span style=\"margin:0 0\">" + "自选股中股票今日涨幅" + "<span style = \"color:#FA4F4D;margin:0 0\">" +
						"+" + MathUtil.percentFormat(yieldVo.getYieldRate()) + "</span>" + "</span>");
				} else {
					vo.setContent("<span style=\"margin:0 0\">" + "自选股中股票今日跌幅" + "<span style = \"color:#33B850;margin:0 0\">" +
						MathUtil.percentFormat(yieldVo.getYieldRate()) + "</span>" + "</span>");
				}

				Map<String, Object> item = new HashMap<>();
				item.put("dataId", vo.getDataId());
				item.put("dataType", vo.getDataType());
				item.put("innerCode", cache.getInnerCode());
				item.put("secuCode", cache.getSecuCode());
				item.put("secuAbbr", cache.getSecuAbbr());
				item.put("suffix", cache.getSuffix());
				item.put("secuMarket", cache.getSecuMarket());
				item.put("secuClass", cache.getSecuCategory());
				item.put("industryCode", cache.getIndustryCode());
				item.put("industryName", cache.getIndustryName());
				item.put("dayYieldRate", yieldVo.getYieldRate().setScale(4, BigDecimal.ROUND_HALF_UP));
				item.put("recDesc", vo.getContent());
				vo.setItem(item);
				result.add(vo);
			}
			return result;
		}
		return null;
	}

	private HoneyBeeBaseVo getElement(long id) {
		cache.acquireReadLockOnKey(id);
		HoneyBeeBaseVo vo = null;
		Element element = cache.get(id);
		if (element != null) {
			vo = (HoneyBeeBaseVo) element.getObjectValue();
		}
		cache.releaseReadLockOnKey(id);

		return vo;
	}

	private HoneyBeeBaseVo addElement(long id, HoneyBeeBaseVo vo) {
		cache.acquireWriteLockOnKey(id);
		cache.put(new Element(id, vo));
		cache.releaseWriteLockOnKey(id);

		return vo;
	}

	private void removeElement(long id) {
		cache.acquireWriteLockOnKey(id);
		cache.remove(id);
		cache.releaseWriteLockOnKey(id);
	}
}
