package com.meix.institute.impl;

import com.meix.institute.api.IUserLoginRecordService;
import com.meix.institute.entity.UserLoginRecord;
import com.meix.institute.mapper.UserLoginRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by zenghao on 2020/3/31.
 */
@Service
public class UserLoginRecordServiceImpl implements IUserLoginRecordService {

	@Autowired
	private UserLoginRecordMapper userLoginRecordMapper;

	@Override
	public int saveUserLoginRecord(long uid, int clientType) {
		UserLoginRecord record = new UserLoginRecord();
		record.setUid(uid);
		record.setClientType(clientType);
		record.setCreateTime(new Date());
		return userLoginRecordMapper.insert(record);
	}
}
