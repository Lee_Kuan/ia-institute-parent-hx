package com.meix.institute.impl;

import static com.meix.institute.constant.MeixConstants.SHARE_PUBLIC;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.beanutils.converters.StringConverter;
import org.apache.commons.collections.MapUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.BaseService;
import com.meix.institute.api.ICompanyService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IDictTagService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IReportService;
import com.meix.institute.api.XnSyncDao;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsReportAlbum;
import com.meix.institute.entity.InsReportAlbumIssueType;
import com.meix.institute.entity.InsReportAlbumResearch;
import com.meix.institute.entity.InsReportAlbumSubmenu;
import com.meix.institute.entity.InsReportAttach;
import com.meix.institute.entity.InsReportIssueType;
import com.meix.institute.entity.InsUser;
import com.meix.institute.entity.ReportInfo;
import com.meix.institute.entity.ReportRelation;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.mapper.InsReportAlbumIssueTypeMapper;
import com.meix.institute.mapper.InsReportAlbumMapper;
import com.meix.institute.mapper.InsReportAlbumResearchMapper;
import com.meix.institute.mapper.InsReportAlbumSubmenuMapper;
import com.meix.institute.mapper.InsReportAttachMapper;
import com.meix.institute.mapper.InsReportIssueTypeMapper;
import com.meix.institute.mapper.InsUserMapper;
import com.meix.institute.mapper.ReportInfoMapper;
import com.meix.institute.mapper.ReportMapper;
import com.meix.institute.mapper.ReportRelationMapper;
import com.meix.institute.mapper.SecuMainMapper;
import com.meix.institute.mapper.UserDaoImpl;
import com.meix.institute.pojo.InsUserInfo;
import com.meix.institute.search.ComplianceListSearchVo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.OssResourceUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.GroupPowerVo;
import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.report.ReportAlbumVo;
import com.meix.institute.vo.report.ReportAuthorVo;
import com.meix.institute.vo.report.ReportDetailVo;
import com.meix.institute.vo.report.ReportInfoVo;
import com.meix.institute.vo.secu.SecuMainVo;
import com.meix.institute.vo.user.UserInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import oracle.sql.BLOB;
import tk.mybatis.mapper.entity.Example;

/**
 * @describe 晨会
 */
@Service
public class ReportServiceImpl extends BaseService implements IReportService {

	@Autowired
	private ReportInfoMapper reportMapper;
	@Autowired
	private ReportRelationMapper relaMapper;
	@Autowired
	private InsReportIssueTypeMapper issueTypeMapper;
	@Autowired
	private InsUserServiceImpl userService;
	@Autowired
	private SecuMainMapper secuMapper;
	@Autowired
	private ReportMapper myReportMapper;
	@Autowired
	private UserDaoImpl myUserMapper;
	@Autowired
	private InsUserMapper userMapper;
	@Autowired
	private ICompanyService companyServiceImpl;
	@Autowired
	private IDictTagService dictTagService;
	@Autowired
	private InsReportAlbumMapper albumMapper;
	@Autowired
	private InsReportAlbumSubmenuMapper albumSubmenuMapper;
	@Autowired
	private InsReportAlbumResearchMapper albumResearchMapper;
	@Autowired
	private InsReportAlbumIssueTypeMapper albumIssueMapper;
	@Autowired
	private InsReportAttachMapper reportAttachMapper;
	@Autowired
	private InsUserMapper insUserMapper;
//	@Autowired
//	private IUserService userServiceImpl;
	@Autowired
	private IInstituteMessagePushService messagePushService;
	@Autowired
	private ICustomerGroupService groupService;
	@Autowired
	private XnSyncDao xnSyncDao;
	
	@Override
	public long saveReport(long resId, String uuid, ReportInfo report, List<ReportRelation> relations, JSONArray issueType) {
		long researchId = report.getResearchId() == null ? 0l : report.getResearchId();
		if (researchId == 0l) {
			// 新增的
			researchId = resId;
			report.setResearchId(researchId);
			report.setCreatedAt(new Date());
			report.setCreatedBy(uuid);
			report.setUpdatedAt(new Date());
			report.setUpdatedBy(uuid);
			if (report.getPublishDate() == null) {
				report.setPublishDate(new Date());
			}
			if (reportMapper.insert(report) < 1) {
				return 0;
			}
			if (report.getType() == 1) { //会议纪要
				messagePushService.messagePush(report.getOrgCode(), researchId, InstituteMessagePushServiceImpl.CREATE_REPORT, report.getTitle(), report.getType(), 0, null);
			}
		} else {
			report.setUpdatedAt(new Date());
			report.setUpdatedBy(uuid);
			// 修改的
			if (reportMapper.updateByPrimaryKeySelective(report) < 1) {
				return 0;
			}
			// 删除所有从表数据,防止更新授权时无此信息导致信息被删
			if (relations != null) {
				Example example = new Example(ReportRelation.class);
				example.createCriteria().andEqualTo("researchId", researchId);
				relaMapper.deleteByExample(example);
			}

			Example example = new Example(InsReportIssueType.class);
			example.createCriteria().andEqualTo("researchId", researchId);
			issueTypeMapper.deleteByExample(example);

		}

		if (relations != null && relations.size() > 0) {
			for (ReportRelation item : relations) {
				item.setResearchId(researchId);
			}
			relaMapper.insertList(relations);
		}

		if (issueType != null && issueType.size() > 0) {
			List<InsReportIssueType> issueList = new ArrayList<>();
			for (int i = 0; i < issueType.size(); i++) {
				InsReportIssueType params = new InsReportIssueType();
				params.setResearchId(researchId);
				params.setIssueType(issueType.getInt(i));

				params.setCreatedAt(new Date());
				params.setCreatedBy(uuid);
				params.setUpdatedAt(new Date());
				params.setUpdatedBy(uuid);
				issueList.add(params);
			}
			issueTypeMapper.insertList(issueList);
		}

		return researchId;
	}

	@Override
	public ReportDetailVo getReportDetail(long researchId, boolean getCustomerId) {
		ReportInfo report = reportMapper.selectByPrimaryKey(researchId);

		ReportDetailVo reportDetailVo = new ReportDetailVo();
		if (report == null) {
			return reportDetailVo;
		}
		ConvertUtils.register(new DateConverter(null), java.util.Date.class);
		ConvertUtils.register(new StringConverter(""), String.class);
		BeanUtils.copyProperties(report, reportDetailVo);

		reportDetailVo.setId(report.getResearchId());
		reportDetailVo.setPublishDate(report.getPublishDate());
		reportDetailVo.setContent(report.getSummary());
		InsUser user = userService.getUserInfo(reportDetailVo.getUpdatedBy());
		reportDetailVo.setEditUserName(user == null ? "" : user.getUserName());

		CompanyInfoVo companyInfoVo = companyServiceImpl.getCompanyInfoById(report.getOrgCode());
		if (companyInfoVo != null) {
			reportDetailVo.setOrgName(companyInfoVo.getCompanyName());
		}
		Example example = new Example(InsReportIssueType.class);
		example.createCriteria().andEqualTo("researchId", researchId);

		//设置发布渠道信息
		List<InsReportIssueType> typeList = issueTypeMapper.selectByExample(example);
		if (typeList != null && typeList.size() > 0) {
			List<Integer> issueTypeList = new ArrayList<>(typeList.size());
			for (InsReportIssueType type : typeList) {
				issueTypeList.add(type.getIssueType());
			}
			reportDetailVo.setIssueTypeList(issueTypeList);
		}

		Example relaExample = new Example(ReportRelation.class);
		relaExample.createCriteria().andEqualTo("researchId", researchId);
		relaExample.orderBy("no").asc();
		List<ReportRelation> relas = relaMapper.selectByExample(relaExample);
		List<SecuMainVo> secuList = new ArrayList<>();
		List<SecuMainVo> industryList = new ArrayList<>();
		List<String> rate = new ArrayList<>();
		List<String> author = new ArrayList<>();
		List<ReportAuthorVo> authorList = new ArrayList<>();
		Example secuExample = new Example(SecuMain.class);
		for (ReportRelation rela : relas) {
			secuExample.clear();
			if (rela.getType() == 2) {
				secuExample.createCriteria().andEqualTo("innerCode", rela.getCode());
				SecuMain secu = secuMapper.selectOneByExample(secuExample);
				if (secu != null) {
					SecuMainVo target = new SecuMainVo();
					target.setIndustryCode(secu.getIndustryCode());
					target.setInnerCode(secu.getInnerCode());
					target.setSecuAbbr(secu.getSecuAbbr());
					target.setSecuCode(secu.getSecuCode());
					target.setSuffix(secu.getSuffix());
					secuList.add(target);
				}
			} else if (rela.getType() == 3) {
				if (rela.getCode() == 0) {
					SecuMainVo target = new SecuMainVo();
					target.setIndustryCode(0);
					target.setInnerCode(3145);
					target.setSecuAbbr("沪深300");
					target.setSecuCode("000300");
					target.setSuffix(".SH");
					industryList.add(target);
				} else {
					//secuExample.createCriteria().andEqualTo("secuCategory", 4).andEqualTo("industryCode", Long.valueOf(rela.getContent()));
					secuExample.createCriteria().andEqualTo("secuCategory", 4).andEqualTo("industryCode", rela.getCode());
					SecuMain secu = secuMapper.selectOneByExample(secuExample);
					if (secu != null) {
						SecuMainVo target = new SecuMainVo();
						target.setIndustryCode(secu.getIndustryCode());
						target.setInnerCode(secu.getInnerCode());
						target.setSecuAbbr(secu.getSecuAbbr());
						target.setSecuCode(secu.getSecuCode());
						target.setSuffix(secu.getSuffix());
						industryList.add(target);
					}
				}
			} else if (rela.getType() == 0) {
//				rate.add(rela.getContent());
			} else if (rela.getType() == 1) {
				author.add(rela.getContent());
				ReportAuthorVo authorVo = new ReportAuthorVo();
				authorVo.setAuthorCode(Long.valueOf(rela.getCode()));
				authorVo.setAuthorName(rela.getContent());
				// followNum, reportNum, readNum
				Example ex = new Example(ReportRelation.class);
				ex.createCriteria().andEqualTo("type", 1).andEqualTo("code", rela.getCode());
				int count = relaMapper.selectCountByExample(ex);
				authorVo.setReportNum(count);
				if (getCustomerId) {
					InsUser insUser = userMapper.selectByPrimaryKey(rela.getCode());
					if (insUser != null) {
						authorVo.setOrgCode(insUser.getCompanyCode());
						authorVo.setAuthorHeadImgUrl(insUser.getHeadImageUrl());
						CompanyInfoVo company = companyServiceImpl.getCompanyInfoById(insUser.getCompanyCode());
						authorVo.setOrgName(company == null ? "" : company.getCompanyName());
						UserInfo uInfo = myUserMapper.getUserInfoByUuid(insUser.getUser());
						if (uInfo != null) {
							authorVo.setIsMeixUser(1);
							authorVo.setAuthorCode(rela.getCode());
							authorVo.setId(rela.getCode());
	
							int followNum = myUserMapper.getUserFollowNum(uInfo.getId());
							int readNum = myUserMapper.getReadNum(uInfo.getId());
							authorVo.setFollowNum(followNum);
							authorVo.setReadNum(readNum);
							authorVo.setName(rela.getContent());
						}
					}
				} else {
					authorVo.setAuthorCode(rela.getCode());
				}
				authorList.add(authorVo);
			}
		}
		reportDetailVo.setSecu(secuList);
		reportDetailVo.setIndustry(industryList);
		if (StringUtils.isNotBlank(report.getInfoGrade())) {
			rate.add(report.getInfoGrade());
		}
		reportDetailVo.setRate(rate);
		reportDetailVo.setAuthor(author);
		reportDetailVo.setAuthorList(authorList);

		String authNames = report.getAuthNames();
		if (StringUtils.isNotBlank(authNames) && authNames.endsWith(",")) {
			authNames = authNames.substring(0, authNames.length() - 1);
		}
		reportDetailVo.setAuthorName(authNames);
		reportDetailVo.setShareName(getShareDesc(report.getShare()));
		
		reportDetailVo.setFileUrl(myReportMapper.getReportFileUrl(reportDetailVo.getId()));
		return reportDetailVo;
	}

	@Override
	public List<ReportDetailVo> getReportList(ComplianceListSearchVo searchVo) {
		Example example = buildComplianceListExample(searchVo);
		example.excludeProperties("summary");
		RowBounds rowBounds = new RowBounds(searchVo.getCurrentPage(), searchVo.getShowNum());
		List<ReportInfo> list = reportMapper.selectByExampleAndRowBounds(example, rowBounds);
		if (list == null || list.size() == 0) {
			return Collections.emptyList();
		}
		List<ReportDetailVo> result = new ArrayList<>(list.size());


		for (ReportInfo item : list) {
			ReportDetailVo reportDetailVo = new ReportDetailVo();
			ConvertUtils.register(new StringConverter(""), String.class);
			BeanUtils.copyProperties(item, reportDetailVo);

			reportDetailVo.setUndoTime(item.getUpdatedAt());
			reportDetailVo.setId(item.getResearchId());
			//设置创建人和修改人信息
//			InsUser createUser = userService.getUserInfo(item.getCreatedBy());
//			if (createUser != null) {
//				meetingVo.setAuthorName(createUser.getUserName());
//				meetingVo.setUserState(createUser.getUserState());
//			}
			InsUser updateUser = userService.getUserInfo(item.getUpdatedBy());
			if (updateUser != null) {
				reportDetailVo.setEditUserName(updateUser.getUserName());
			}

			reportDetailVo.setShareName(getShareDesc(item.getShare()));
			//设置发布渠道信息
			InsReportIssueType issueTypeSearch = new InsReportIssueType();
			issueTypeSearch.setResearchId(item.getResearchId());
			List<InsReportIssueType> typeList = issueTypeMapper.select(issueTypeSearch);
			if (typeList != null && typeList.size() > 0) {
				List<Integer> issueTypeList = new ArrayList<>(typeList.size());
				for (InsReportIssueType type : typeList) {
					issueTypeList.add(type.getIssueType());
				}
				reportDetailVo.setIssueTypeList(issueTypeList);
			}
			result.add(reportDetailVo);

			String authNames = item.getAuthNames();
			if (StringUtils.isNotBlank(authNames) && authNames.endsWith(",")) {
				authNames = authNames.substring(0, authNames.length() - 1);
			}
			reportDetailVo.setAuthorName(authNames);
		}

		list.clear();
		return result;
	}

	@Override
	public int getReportCount(ComplianceListSearchVo searchVo) {
		Example example = buildComplianceListExample(searchVo);
		return reportMapper.selectCountByExample(example);
	}

	private Example buildComplianceListExample(ComplianceListSearchVo searchVo) {
		Example example = new Example(ReportInfo.class);
		Example.Criteria criteria = example.createCriteria().andEqualTo("type", searchVo.getType());
//		if (searchVo.getCompanyCode() > 0) {
//			criteria.andEqualTo("orgCode", searchVo.getCompanyCode());
//		}
		if (searchVo.getStatusList() != null && searchVo.getStatusList().size() > 0) {
			criteria.andIn("status", searchVo.getStatusList());
		}
		if (searchVo.getShare() > -1) {
			criteria.andEqualTo("share", searchVo.getShare());
		}
		if (!StringUtils.isNullorWhiteSpace(searchVo.getCondition())) {
			Example.Criteria criteria2 = example.createCriteria();
			criteria2.orLike("title", "%" + searchVo.getCondition() + "%").orLike("authNames", "%" + searchVo.getCondition() + "%");
			example.and(criteria2);

		}
		if (searchVo.getSortType() == 0) {
			if (searchVo.getSortRule() == -1) {
				example.orderBy("publishDate").desc();
			} else {
				example.orderBy("publishDate").asc();
			}
		} else if (searchVo.getSortType() == 1) {
			if (searchVo.getSortRule() == -1) {
				example.orderBy("updatedAt").desc();
			} else {
				example.orderBy("updatedAt").asc();
			}
		} else {
			example.orderBy("publishDate").desc();
		}
		return example;
	}

	@Override
	public void examineReport(ReportInfo report) {
		reportMapper.updateByPrimaryKeySelective(report);
	}

	@Override
	public List<ReportDetailVo> getReportAllList(long uid, int type, long companyCode, int showNum, int currentPage, int clientType, long code, List<String> infoTypeLists, String publishDateFm, String publishDateTo, String condition) {
		List<ReportDetailVo> list = myReportMapper.getReportList(uid, type, companyCode, showNum, currentPage, clientType, code, infoTypeLists, publishDateFm, publishDateTo, condition, null);

		return coverReportListVo(uid, list);
	}

	@Override
	public ReportDetailVo getReportDetail(long reportId, long uid) {
		ReportDetailVo report = myReportMapper.getReportDetail(reportId, uid);
		ReportDetailVo reportDetailVo = new ReportDetailVo();
		if (report == null) {
			return null;
		}
		ConvertUtils.register(new StringConverter(""), String.class);
		BeanUtils.copyProperties(report, reportDetailVo);
		String infoType = report.getInfoType();
		reportDetailVo.setFileAttr(report.getFileUrl());
		Example relaExample = new Example(ReportRelation.class);
		relaExample.createCriteria().andEqualTo("researchId", report.getId());
		relaExample.orderBy("no").asc();
		List<ReportRelation> relas = relaMapper.selectByExample(relaExample);
		List<SecuMainVo> secuList = new ArrayList<>();
		List<SecuMainVo> industryList = new ArrayList<>();
		List<String> rate = new ArrayList<>();
		List<String> author = new ArrayList<>();
		List<ReportAuthorVo> authorList = new ArrayList<>();
		Example secuExample = new Example(SecuMain.class);
		for (ReportRelation rela : relas) {
			secuExample.clear();
			if (rela.getType() == 2) {
				secuExample.createCriteria().andEqualTo("innerCode", rela.getCode());
				SecuMain secu = secuMapper.selectOneByExample(secuExample);
				SecuMainVo target = new SecuMainVo();
				if (secu != null) {
					target.setIndustryCode(secu.getIndustryCode());
					target.setInnerCode(secu.getInnerCode());
					target.setSecuAbbr(secu.getSecuAbbr());
					target.setSecuCode(secu.getSecuCode());
					target.setSuffix(secu.getSuffix());
				}
				secuList.add(target);

			} else if (rela.getType() == 3) {
//				secuExample.createCriteria().andEqualTo("secuCategory", 4).andEqualTo("industryCode", Long.valueOf(rela.getContent()));
				SecuMainVo target = new SecuMainVo();
				if (rela.getCode() > 0) {
					secuExample.createCriteria().andEqualTo("secuCategory", 4).andEqualTo("industryCode", rela.getCode());
					SecuMain secu = secuMapper.selectOneByExample(secuExample);
					if (secu != null) {
						target.setIndustryCode(secu.getIndustryCode());
						target.setInnerCode(secu.getInnerCode());
						target.setSecuAbbr(secu.getSecuAbbr());
						target.setSecuCode(secu.getSecuCode());
						target.setSuffix(secu.getSuffix());
					}
				} else {
					target.setSecuAbbr(rela.getXnindName());
				}
				industryList.add(target);

			} else if (rela.getType() == 0) {
//				rate.add(rela.getContent());
			} else if (rela.getType() == 1) {
				author.add(rela.getContent());
				ReportAuthorVo authorVo = new ReportAuthorVo();
				authorVo.setAuthorCode(Long.valueOf(rela.getCode()));
				authorVo.setAuthorName(rela.getContent());
				// followNum, reportNum, readNum
				Example example = new Example(ReportRelation.class);
				example.createCriteria().andEqualTo("type", 1).andEqualTo("code", rela.getCode());
				int count = relaMapper.selectCountByExample(example);
				authorVo.setReportNum(count);

				if (rela.getCode() > 0) {
					InsUserInfo user = insUserService.selectByPrimaryKey(rela.getCode());
					if (user != null && user.getId() > 0) {
						authorVo.setQualifyNo(user.getQualifyno());
						authorVo.setOrgCode(user.getCompanyCode());
						authorVo.setAuthorHeadImgUrl(OssResourceUtil.getHeadImage(user.getHeadImageUrl()));
						CompanyInfoVo company = companyServiceImpl.getCompanyInfoById(user.getCompanyCode());
						authorVo.setOrgName(company == null ? "" : company.getCompanyName());
						// 显示职位
						authorVo.setPosition(user.getPositionText());
						authorVo.setDirection(user.getDirection());
						UserInfo uInfo = myUserMapper.getUserInfoByUuid(user.getUser());
						if (uInfo != null) {
							authorVo.setIsMeixUser(1);
							authorVo.setAuthorCode(uInfo.getId());
							authorVo.setId(uInfo.getId());

							int followNum = myUserMapper.getUserFollowNum(uInfo.getId());
							int readNum = myUserMapper.getReadNum(uInfo.getId());
							authorVo.setFollowNum(followNum);
							authorVo.setReadNum(readNum);
							authorVo.setName(rela.getContent());
							boolean isFocused = myUserMapper.isFocusedPerson(uid, uInfo.getId());
							authorVo.setFollowFlag(isFocused ? 1 : 0);
							if (StringUtils.isBlank(user.getHeadImageUrl())) {
								authorVo.setAuthorHeadImgUrl(StringUtils.isNotBlank(
										uInfo.getHeadImageUrl()) ? OssResourceUtil.getHeadImage(uInfo.getHeadImageUrl())
												: OssResourceUtil.getHeadImage(uInfo.getWechatHeadUrl()));
							}
						}
					}
				}
				authorList.add(authorVo);
			}
		}
		reportDetailVo.setSecu(secuList);
		reportDetailVo.setIndustry(industryList);
		if (StringUtils.isNotBlank(report.getInfoGrade())) {
			rate.add(report.getInfoGrade());
		}
		reportDetailVo.setRate(rate);
		reportDetailVo.setAuthor(author);

		int readNum = myUserMapper.getReportReadNum(report.getId());
		reportDetailVo.setReadNum(readNum);

		reportDetailVo.setCompanyCode(report.getOrgCode());
		reportDetailVo.setInfoDate(report.getPublishDate());
		reportDetailVo.setSecuList(secuList);
		reportDetailVo.setAuthorList(authorList);

		String authNames = report.getAuthorName();
		if (StringUtils.isNotBlank(authNames) && authNames.endsWith(",")) {
			authNames = authNames.substring(0, authNames.length() - 1);
		}
		reportDetailVo.setAuthorName(authNames);
		ReportInfoVo vo = new ReportInfoVo();
		vo.setApplyStatus(reportDetailVo.getApplyStatus());
		vo.setIndustry(reportDetailVo.getIndustry());
		vo.setIsCompanyWhiteList(reportDetailVo.getIsCompanyWhiteList());
		vo.setPdfUrl(reportDetailVo.getFileUrl());
		vo.setPermission(reportDetailVo.getPermission());
		vo.setSecu(reportDetailVo.getSecuList());
		vo.setAuthor(authorList);
		List<String> yjfl = new ArrayList<>();
		yjfl.add(dictTagService.getText(MeixConstants.REPORT_TYPE, infoType));
		vo.setYjfl(yjfl);
		reportDetailVo.setYjfl(yjfl);
		reportDetailVo.setReport(vo);
		CompanyInfoVo company = companyServiceImpl.getCompanyInfoById(report.getOrgCode());
		reportDetailVo.setOrgName(company == null ? "" : company.getCompanyName());
		reportDetailVo.setEvaluateDesc(report.getInfoGrade());
		if (StringUtils.isBlank(reportDetailVo.getFileUrl())) {
			reportDetailVo.setContentType(1);
		}
		List<GroupPowerVo> groupPower = null;
		int permission = 0;
		boolean isPub = false;
		if (reportDetailVo.getShare() == 0) {
			permission = 1;
			isPub = true;
		}
		reportDetailVo.setHasPdfUrl(StringUtils.isBlank(report.getFileUrl()) ? 0 : 1);
		groupPower = groupService.getResourceGroupPower(uid, reportId, MeixConstants.USER_YB, reportDetailVo.getPowerSubType(), 0);
		reportDetailVo.setContent("");
		reportDetailVo.setFileUrl("");
		if (groupPower != null && !groupPower.isEmpty()) {
			for (GroupPowerVo tmp : groupPower) {
				if (isPub || (tmp.getShare() == MeixConstants.SHARE_DETAIL && tmp.getPower() == 1)) {
					permission = 1;
					reportDetailVo.setContent(report.getContent());
				}
				if (isPub || (tmp.getShare() == MeixConstants.SHARE_LIVE && tmp.getPower() == 1 && reportDetailVo.getDeepStatus() == 1)) { // 普通报告原文
					permission = 1;
					reportDetailVo.setFileUrl(report.getFileUrl());
				}
				if (isPub || (tmp.getShare() == MeixConstants.SHARE_REPLAY && tmp.getPower() == 1 && reportDetailVo.getDeepStatus() == 2)) { // 深度报告原文
					permission = 1;
					reportDetailVo.setFileUrl(report.getFileUrl());
				}
				if (isPub) {
					tmp.setPower(1);
				}
			}
		}
		reportDetailVo.setPermission(permission);
		reportDetailVo.setGroupPower(groupPower == null ? new ArrayList<>() : groupPower);
		return reportDetailVo;
	}

	@Override
	public void deleteByThirdId(String thirdId) {
		ReportInfo param = new ReportInfo();
		param.setThirdId(thirdId);
		ReportInfo record = reportMapper.selectOne(param);
		if (record == null) {
			return;
		}
		Long researchId = record.getResearchId();
		reportMapper.deleteByPrimaryKey(researchId);

		// 删除发布渠道
		InsReportIssueType issRecord = new InsReportIssueType();
		issRecord.setResearchId(researchId);
		issueTypeMapper.delete(issRecord);

		// 删除附加信息
		ReportRelation relaRecord = new ReportRelation();
		relaRecord.setResearchId(researchId);
		relaMapper.delete(relaRecord);

	}

	@Override
	public Map<String, Object> getReportInnerCodeAndIndustry(long dataId) {
		return myReportMapper.getReportInnerCodeAndIndustry(dataId);
	}

	@Override
	public List<Map<String, Object>> getReportAlbumList(int type, int showNum, int currentPage) {
		return myReportMapper.getReportAlbumList(type, 1, null, null, null, showNum, currentPage, -1, -1, 0, 0, -1);
	}

	@Override
	public List<ReportAlbumVo> getReportAlbumList(ComplianceListSearchVo searchVo) {
		Example example = buildComplianceAlbumExample(searchVo);
		RowBounds rowBounds = new RowBounds(searchVo.getCurrentPage(), searchVo.getShowNum());
		List<InsReportAlbum> list = albumMapper.selectByExampleAndRowBounds(example, rowBounds);
		List<ReportAlbumVo> resList = new ArrayList<>();
		for (InsReportAlbum temp : list) {
			ReportAlbumVo vo = new ReportAlbumVo();
			vo.setId(temp.getId());
			Example issueExample = new Example(InsReportAlbumIssueType.class);
			issueExample.createCriteria().andEqualTo("albumId", temp.getId());
			List<InsReportAlbumIssueType> issueType = albumIssueMapper.selectByExample(issueExample);
			List<Integer> issueRes = new ArrayList<>();
			if (issueType != null) {
				for (InsReportAlbumIssueType issue : issueType) {
					issueRes.add(issue.getIssueType());
				}
			}
			Example userExample = new Example(InsUser.class);
			userExample.createCriteria().andEqualTo("user", temp.getCreatedBy() == null ? "" : temp.getCreatedBy());
			InsUser user = userMapper.selectOneByExample(userExample);
			vo.setAuthorName(user == null ? "" : user.getUserName());
			vo.setIssueTypeList(issueRes);
			vo.setOperatorUid(temp.getOperatorUid());
			vo.setPublishDate(temp.getCreatedAt());
			vo.setShareName(getShareDesc(temp.getShare()));
			vo.setStatus(temp.getStatus());
			vo.setTitle(temp.getTitle());
			vo.setUndoReason(temp.getUndoReason());
			vo.setUndoTime(temp.getUndoTime());
			resList.add(vo);
		}
		return resList;
	}

	@Override
	public int getReportAlbumCount(ComplianceListSearchVo searchVo) {
		Example example = buildComplianceAlbumExample(searchVo);
		return albumMapper.selectCountByExample(example);
	}

	private Example buildComplianceAlbumExample(ComplianceListSearchVo searchVo) {
		Example example = new Example(InsReportAlbum.class);
		Example.Criteria criteria = example.createCriteria().andEqualTo("catalog", searchVo.getType());
		if (searchVo.getStatusList() != null && searchVo.getStatusList().size() > 0) {
			criteria.andIn("status", searchVo.getStatusList());
		}
		if (!StringUtils.isNullorWhiteSpace(searchVo.getCondition())) {
			Example.Criteria criteria2 = example.createCriteria();
			criteria2.orLike("title", "%" + searchVo.getCondition() + "%");//.orLike("authNames", "%" + searchVo.getCondition() + "%");
			example.and(criteria2);
			Example userExample = new Example(InsUser.class);
			userExample.createCriteria().andLike("userName", "%" + searchVo.getCondition() + "%");
			List<InsUser> userList = insUserMapper.selectByExample(userExample);
			if (userList != null && userList.size() > 0) {
				List<String> uuidList = new ArrayList<>();
				for (InsUser user : userList) {
					uuidList.add(user.getUser());
				}
				criteria2.orIn("createdBy", uuidList);
			}
		}
		if (searchVo.getSortType() == 0) {
			if (searchVo.getSortRule() == -1) {
				example.orderBy("createdAt").desc();
			} else {
				example.orderBy("createdAt").asc();
			}
		} else if (searchVo.getSortType() == 1) {
			if (searchVo.getSortRule() == -1) {
				example.orderBy("undoTime").desc();
			} else {
				example.orderBy("undoTime").asc();
			}
		} else {
			example.orderBy("createdAt").desc();
		}
		return example;
	}

	@Override
	public List<Map<String, Object>> getReportAlbumList(int type, int visible, String condition, String startTime, String endTime, int showNum, int currentPage, int status, int clientType, int sortField, int sortRule, int share) {

		List<Map<String, Object>> list = myReportMapper.getReportAlbumList(type, visible, condition, startTime, endTime, showNum, currentPage, status, clientType, sortField, sortRule, share);
		list = list == null ? new ArrayList<>() : list;
		Example example = new Example(InsReportAlbumIssueType.class);
		for (Map<String, Object> temp : list) {
			long id = (Long) temp.get("id");
			example.clear();
			example.createCriteria().andEqualTo("albumId", id);
			List<InsReportAlbumIssueType> resList = albumIssueMapper.selectByExample(example);
			List<Integer> issueType = new ArrayList<>();
			if (resList != null) {
				for (InsReportAlbumIssueType res : resList) {
					issueType.add(res.getIssueType());
				}
			}
			temp.put("shareName", getShareDesc(MapUtils.getIntValue(temp, "share", 0)));
			temp.put("issueType", issueType);
		}
		return list;
	}

	@Override
	public List<ReportDetailVo> getReportListByAlbum(long bid, long submenuId, int currentPage, int showNum, int type, int clientType) {
		List<ReportDetailVo> list = myReportMapper.getReportListByAlbum(bid, submenuId, currentPage, showNum, type, clientType);

		return coverReportListVo(0, list);
	}

	private List<ReportDetailVo> coverReportListVo(long uid, List<ReportDetailVo> list) {
		List<ReportDetailVo> resList = new ArrayList<>();
		if (list != null & list.size() > 0) {
			for (ReportDetailVo report : list) {
				ReportDetailVo reportDetailVo = new ReportDetailVo();
				ConvertUtils.register(new StringConverter(""), String.class);
				BeanUtils.copyProperties(report, reportDetailVo);

				reportDetailVo.setFileAttr(report.getFileUrl());

//				List<SecuMainVo> secuList = new ArrayList<>();
//				List<SecuMainVo> industryList = new ArrayList<>();
//				List<String> rate = new ArrayList<>();
//				List<String> author = new ArrayList<>();
//				List<ReportAuthorVo> authorVo = new ArrayList<>();
//				
//				Example relaExample = new Example(ReportRelation.class);
//				relaExample.createCriteria().andEqualTo("researchId", report.getId());
//				relaExample.orderBy("no").asc();
//				List<ReportRelation> relas = relaMapper.selectByExample(relaExample);
//				Example secuExample = new Example(SecuMain.class);
//				for (ReportRelation rela : relas) {
//					secuExample.clear();
//					if (rela.getType() == 2) {
//						secuExample.createCriteria().andEqualTo("innerCode", rela.getCode());
//						SecuMain secu = secuMapper.selectOneByExample(secuExample);
//						SecuMainVo target = new SecuMainVo();
//						if (secu != null) {
//							target.setIndustryCode(secu.getIndustryCode());
//							target.setInnerCode(secu.getInnerCode());
//							target.setSecuAbbr(secu.getSecuAbbr());
//							target.setSecuCode(secu.getSecuCode());
//							target.setSuffix(secu.getSuffix());
//						} else {
//							target.setSecuAbbr(rela.getContent());
//						}
//						secuList.add(target);
//
//
//					} else if (rela.getType() == 3) {
//						//					secuExample.createCriteria().andEqualTo("secuCategory", 4).andEqualTo("industryCode", Long.valueOf(rela.getContent()));
//						secuExample.createCriteria().andEqualTo("secuCategory", 4).andEqualTo("industryCode", rela.getCode());
//						SecuMain secu = secuMapper.selectOneByExample(secuExample);
//						SecuMainVo target = new SecuMainVo();
//						if (secu != null) {
//							target.setIndustryCode(secu.getIndustryCode());
//							target.setInnerCode(secu.getInnerCode());
//							target.setSecuAbbr(secu.getSecuAbbr());
//							target.setSecuCode(secu.getSecuCode());
//							target.setSuffix(secu.getSuffix());
//						} else {
//							target.setSecuAbbr(rela.getContent());
//						}
//						industryList.add(target);
//
//
//					} else if (rela.getType() == 0) {
//						//					rate.add(rela.getContent());
//					} else if (rela.getType() == 1) {
//						author.add(rela.getContent());
//						ReportAuthorVo vo = new ReportAuthorVo();
//						vo.setAuthorName(rela.getContent());
//						vo.setAuthorCode(0);
//						if (rela.getCode() > 0) {
//							InsUser user = userMapper.selectByPrimaryKey(rela.getCode());
//							if (user != null) {
//								UserInfo userInfo = userServiceImpl.getThirdUserInfo(user.getThirdId(), user.getMobile());
//								if (userInfo != null) {
//									vo.setAuthorCode(userInfo.getId());
//									vo.setAuthorHeadImgUrl(OssResourceUtil.getHeadImage(userInfo.getHeadImageUrl()));
//									vo.setAuthorName(userInfo.getUserName());
//									boolean followFlag = userServiceImpl.isFocusedPerson(uid, userInfo.getId());
//									vo.setFollowFlag(followFlag ? 1 : 0);
//								}
//							}
//						}
//						authorVo.add(vo);
//					}
//				}
//				reportDetailVo.setSecu(secuList);
//				reportDetailVo.setIndustry(industryList);
//				rate.add(report.getInfoGrade() == null ? "" : report.getInfoGrade());
//				reportDetailVo.setRate(rate);
//				reportDetailVo.setAuthor(author);
					
				reportDetailVo.setCompanyCode(report.getOrgCode());
				reportDetailVo.setInfoDate(report.getPublishDate());

				String authNames = report.getAuthorName();
				if (StringUtils.isNotBlank(authNames) && authNames.endsWith(",")) {
					authNames = authNames.substring(0, authNames.length() - 1);
				}
				reportDetailVo.setAuthorName(authNames);

				ReportInfoVo vo = new ReportInfoVo();
				vo.setApplyStatus(reportDetailVo.getApplyStatus());
//				vo.setIndustry(industryList);
//				vo.setSecu(secuList);
				vo.setPermission(reportDetailVo.getPermission());
				vo.setIsCompanyWhiteList(reportDetailVo.getIsCompanyWhiteList());
				vo.setApplyStatus(reportDetailVo.getApplyStatus());
				vo.setPdfUrl(reportDetailVo.getFileUrl());
//				vo.setAuthor(authorVo);
				reportDetailVo.setPublishDateText(DateUtil.formatReprotPublishDate(report.getPublishDate()));
				List<String> yjfl = new ArrayList<>();
				reportDetailVo.setInfoType(dictTagService.getText(MeixConstants.REPORT_TYPE, report.getInfoType()));
				yjfl.add(reportDetailVo.getInfoType());
				vo.setYjfl(yjfl);
				reportDetailVo.setYjfl(yjfl);
				reportDetailVo.setReport(vo);
//				CompanyInfoVo company = companyServiceImpl.getCompanyInfoById(report.getOrgCode());
//				reportDetailVo.setOrgName(company == null ? "" : company.getCompanyName());
				reportDetailVo.setEvaluateDesc(report.getInfoGrade());
				resList.add(reportDetailVo);
			}
		}
		return resList;
	}

	/**
	 * wechat
	 */
	@Override
	public Map<String, Object> getReportAlbumDetail(long uid, long bid) {
		Map<String, Object> detail = myReportMapper.getReportAlbumDetail(bid);
		if (detail != null) {
			int share = MapUtils.getIntValue(detail, "share", 0);
			int permission = getPermission(share);
			List<GroupPowerVo> groupPower = null;
			if (share == 1) {
				groupPower = groupService.getResourceGroupPower(uid, bid, MeixConstants.REPORT_ALBUM, 0, 0);
				if (groupPower != null && !groupPower.isEmpty()) {
					for (GroupPowerVo vo : groupPower) {
						if (vo.getShare() == MeixConstants.SHARE_DETAIL && vo.getPower() == 1) {
							permission = 1;
						}
					}
				}
			}
			detail.put("permission", permission);
			detail.put("groupPower", groupPower == null ? new ArrayList<>() : groupPower);
			List<Map<String, Object>> submenus = myReportMapper.getReportAlbumSubmenuList(bid);
			detail.put("submenus", submenus == null ? new ArrayList<>() : submenus);
		}
		return detail;
	}
	
	private int getPermission(int share) {//详情权限
		if (SHARE_PUBLIC == share) {
			return 1;
		} else {
			return 0;
		}
	}

	/**
	 * mng
	 */
	@Override
	public Map<String, Object> getReportAlbumDetail(int type, long bid) {
		Map<String, Object> detail = myReportMapper.getReportAlbumDetail(bid);
		if (detail != null) {
			int share = MapUtils.getIntValue(detail, "share", 0);
			detail.put("shareName", getShareDesc(share));
			Example example = new Example(InsReportAlbumIssueType.class);
			example.createCriteria().andEqualTo("albumId", bid);
			List<InsReportAlbumIssueType> issueType = albumIssueMapper.selectByExample(example);
			List<Integer> issueRes = new ArrayList<>();
			if (issueType != null) {
				for (InsReportAlbumIssueType temp : issueType) {
					issueRes.add(temp.getIssueType());
				}
			}
			detail.put("issueType", issueRes);
			List<Map<String, Object>> submenus = myReportMapper.getReportAlbumSubmenuList(bid);
			List<ReportDetailVo> list = myReportMapper.getReportListByAlbum(bid, 0, 0, -1, type, -1);
			list = list == null ? new ArrayList<>() : list;
			submenus = submenus == null ? new ArrayList<>() : submenus;
			for (Map<String, Object> submenu : submenus) {
				List<Map<String, Object>> temp = new ArrayList<>();
				for (int i = 0; i < list.size(); i++) {
					ReportDetailVo vo = list.get(i);
					if (vo.getSubmenuId() == (Long) submenu.get("id")) {
						Map<String, Object> map = new HashMap<>();
						map.put("id", vo.getRid());
						map.put("researchId", vo.getId());
						map.put("title", vo.getTitle());
						map.put("authorName", vo.getAuthorName());
						map.put("publishDate", vo.getPublishDate());
						temp.add(map);
						list.remove(i);
						i--;
					}
				}
				submenu.put("research", temp);
			}
			detail.put("submenu", submenus);
			List<Map<String, Object>> temp = new ArrayList<>();
			for (int i = 0; i < list.size(); i++) {
				ReportDetailVo vo = list.get(i);
				Map<String, Object> map = new HashMap<>();
				map.put("id", vo.getRid());
				map.put("researchId", vo.getId());
				map.put("title", vo.getTitle());
				map.put("authorName", vo.getAuthorName());
				map.put("publishDate", vo.getPublishDate());
				temp.add(map);
			}
			detail.put("research", temp);
		}
		return detail;
	}

	@Override
	public long saveReportAlbum(long resId, String uuid, long id, long companyCode, String title, String subtit, String albumDesc, String coverLgUrl,
	                            String coverLgName, String coverSmUrl, String coverSmName, int catalog, int no, int visible, int isTop,
	                            int share, JSONArray subObj, JSONArray researchObj, JSONArray issueType, String url, int atype) {
		InsReportAlbum album = new InsReportAlbum();
		album.setId(id);
		album.setTitle(title);
		album.setSubtit(subtit);
		album.setAlbumDesc(albumDesc);
		album.setCoverLgUrl(coverLgUrl);
		album.setCoverLgName(coverLgName);
		album.setCoverSmUrl(coverSmUrl);
		album.setCoverSmName(coverSmName);
		album.setCatalog(catalog);
		album.setNo(no);
		album.setVisible(visible);
		album.setIsTop(isTop);
		Date date = new Date();
		album.setUpdatedAt(date);
		album.setUpdatedBy(uuid);
		album.setStatus(1);
		album.setShare(share);
		album.setUrl(url);
		album.setType(atype);
		if (id == 0) {
			id = resId;
			album.setId(id);
			album.setCreatedAt(date);
			album.setCreatedBy(uuid);
			albumMapper.insert(album);
			messagePushService.messagePush(companyCode, album.getId(), InstituteMessagePushServiceImpl.CREATE_ALBUM, album.getTitle(), album.getCatalog(), 0, null);
		} else {

			albumMapper.updateByPrimaryKeySelective(album);
			Example example = new Example(InsReportAlbumIssueType.class);
			example.createCriteria().andEqualTo("albumId", id);
			albumIssueMapper.deleteByExample(example);
		}
		if (issueType != null && !issueType.isEmpty()) {
			List<InsReportAlbumIssueType> issueTypeList = new ArrayList<>();
			for (Object issue : issueType) {
				int type = (int) issue;
				InsReportAlbumIssueType vo = new InsReportAlbumIssueType();
				vo.setAlbumId(album.getId());
				vo.setIssueType(type);
				vo.setCreatedBy(uuid);
				vo.setUpdatedBy(uuid);
				vo.setCreatedAt(date);
				vo.setUpdatedAt(date);
				issueTypeList.add(vo);
			}
			albumIssueMapper.insertList(issueTypeList);
		}
		List<Long> ids = new ArrayList<>();
		List<Long> rids = new ArrayList<>();
		if (subObj != null && !subObj.isEmpty()) {
			for (Object temp : subObj) {
				JSONObject submenu = JSONObject.fromObject(temp);
				InsReportAlbumSubmenu subMenu = new InsReportAlbumSubmenu();
				subMenu.setBid(id);
				subMenu.setId(MapUtils.getLongValue(submenu, "id", 0));
				subMenu.setName(MapUtils.getString(submenu, "name", ""));
				subMenu.setNo(MapUtils.getIntValue(submenu, "no", 10000));
				subMenu.setUpdatedAt(date);
				if (subMenu.getId() == 0) {
					subMenu.setId(StringUtils.getId(companyCode));
					ids.add(subMenu.getId());
					subMenu.setCreatedAt(date);
					albumSubmenuMapper.insert(subMenu);
				} else {
					ids.add(subMenu.getId());
					albumSubmenuMapper.updateByPrimaryKeySelective(subMenu);
				}
				if (submenu.containsKey("research")) {
					JSONArray research = submenu.getJSONArray("research");
					if (research != null && !research.isEmpty()) {
						rids.clear();
						for (Object tempRes : research) {
							JSONObject obj = JSONObject.fromObject(tempRes);
							InsReportAlbumResearch researchVo = new InsReportAlbumResearch();
							researchVo.setId(MapUtils.getLongValue(obj, "id", 0));
							researchVo.setBid(id);
							researchVo.setSubmenuId(subMenu.getId());
							researchVo.setResearchId(MapUtils.getLongValue(obj, "researchId", 0));
							researchVo.setIsTop(MapUtils.getIntValue(obj, "isTop", 0));
							researchVo.setNo(MapUtils.getIntValue(obj, "no", 10000));
							researchVo.setUpdatedAt(date);
							if (researchVo.getId() == 0) {
								researchVo.setId(StringUtils.getId(companyCode));
								rids.add(researchVo.getId());
								researchVo.setCreatedAt(date);
								albumResearchMapper.insert(researchVo);
							} else {
								rids.add(researchVo.getId());
								albumResearchMapper.updateByPrimaryKeySelective(researchVo);
							}
						}
						Example example = new Example(InsReportAlbumResearch.class);
						example.createCriteria().andEqualTo("bid", id).andEqualTo("submenuId", subMenu.getId()).andNotIn("id", rids);
						albumResearchMapper.deleteByExample(example);
					}
				}
			}
			Example example = new Example(InsReportAlbumSubmenu.class);
			example.createCriteria().andEqualTo("bid", id).andNotIn("id", ids);
			albumSubmenuMapper.deleteByExample(example);

			Example exampleRes = new Example(InsReportAlbumResearch.class);
			exampleRes.createCriteria().andEqualTo("bid", id).andNotEqualTo("submenuId", 0).andNotIn("submenuId", ids);
			albumResearchMapper.deleteByExample(exampleRes);
		} else {
			Example exampleRes = new Example(InsReportAlbumResearch.class);
			exampleRes.createCriteria().andEqualTo("bid", id).andNotEqualTo("submenuId", 0);
			albumResearchMapper.deleteByExample(exampleRes);
		}
		if (researchObj != null && !researchObj.isEmpty()) {
			rids.clear();
			for (Object tempRes : researchObj) {
				JSONObject obj = JSONObject.fromObject(tempRes);
				InsReportAlbumResearch researchVo = new InsReportAlbumResearch();
				researchVo.setId(MapUtils.getLongValue(obj, "id", 0));
				researchVo.setBid(id);
				researchVo.setSubmenuId(0L);
				researchVo.setResearchId(MapUtils.getLongValue(obj, "researchId", 0));
				researchVo.setIsTop(MapUtils.getIntValue(obj, "isTop", 0));
				researchVo.setNo(MapUtils.getIntValue(obj, "no", 10000));
				researchVo.setUpdatedAt(date);
				if (researchVo.getId() == 0) {
					researchVo.setId(StringUtils.getId(companyCode));
					rids.add(researchVo.getId());
					researchVo.setCreatedAt(date);
					albumResearchMapper.insert(researchVo);
				} else {
					rids.add(researchVo.getId());
					albumResearchMapper.updateByPrimaryKeySelective(researchVo);
				}
			}

			Example exampleRes = new Example(InsReportAlbumResearch.class);
			exampleRes.createCriteria().andEqualTo("bid", id).andEqualTo("submenuId", 0).andNotIn("id", rids);
			albumResearchMapper.deleteByExample(exampleRes);
		} else {
			Example exampleRes = new Example(InsReportAlbumResearch.class);
			exampleRes.createCriteria().andEqualTo("bid", id).andEqualTo("submenuId", 0);
			albumResearchMapper.deleteByExample(exampleRes);
		}
		return id;
	}

	@Override
	public int getReportAlbumCount(int type, int visible, String condition, String startTime, String endTime) {
		return getReportAlbumCount(type, visible, condition, startTime, endTime, -1, -1);
	}

	@Override
	public int getReportAlbumCount(int type, int visible, String condition, String startTime, String endTime, int status, int share) {
		return myReportMapper.getReportAlbumCount(type, visible, condition, startTime, endTime, status, share);
	}

	@Override
	public void examineReportAlbum(InsReportAlbum reportAlbum) {
		albumMapper.updateByPrimaryKeySelective(reportAlbum);
	}

	@Override
	public void saveReportPdf(Long researchId, String pdfPath) {
		// 先删除
		InsReportAttach record = new InsReportAttach();
		record.setResearchId(researchId);
		reportAttachMapper.delete(record);
		// 可能存在多个
		String[] pdfPathSub = pdfPath.split(",");
		for (String path : pdfPathSub) {
			InsReportAttach ira = new InsReportAttach();
			ira.setResearchId(researchId);
			ira.setFileUrl(getPdfPath(path));
			ira.setSourceUrl(path);
			reportAttachMapper.insert(ira);
		}
	}
	
	private String getPdfPath(String sourcePath) {
		return sourcePath == null ? null : sourcePath.replaceAll("\\\\", "/").replaceFirst("D://", "/").replaceFirst("D:/", "/");
	}

	// sync to meix
	@Override
	public List<Map<String, Object>> getReportAllList(int type, int showNum, int currentPage, String lastSyncTime) {
		List<ReportDetailVo> list = myReportMapper.getReportList(0, type, 0, showNum, currentPage, -1, 0, null, null, null, null, lastSyncTime);
		List<Map<String, Object>> resList = new ArrayList<>();
		if (list != null) {
			for (ReportDetailVo temp : list) {
				Map<String, Object> vo = new HashMap<>();
				vo.put("id", temp.getId());
				vo.put("orgCode", temp.getOrgCode());
				vo.put("title", temp.getTitle());
				vo.put("publishDate", temp.getPublishDate());
				vo.put("summary", temp.getContent());
				vo.put("infoType", dictTagService.getText(MeixConstants.REPORT_TYPE, temp.getInfoType()));
				vo.put("infoSubType", temp.getInfoSubType());
				vo.put("infoGrade", temp.getInfoGrade());
				vo.put("authNames", temp.getAuthorName());
				vo.put("fileUrl", temp.getFileUrl());
				vo.put("type", temp.getType());
				Example relaExample = new Example(ReportRelation.class);
				relaExample.createCriteria().andEqualTo("researchId", temp.getId());
				relaExample.orderBy("no").asc();
				List<ReportRelation> relas = relaMapper.selectByExample(relaExample);
				String innerCodes = "";
				String industrys = "";
				for (ReportRelation rela : relas) {
					if (rela.getType() == 2) { //innerCode
						innerCodes += rela.getCode() + ",";
					} else if (rela.getType() == 3) {// industryCode
						industrys += rela.getCode() + ",";
					}
				}
				vo.put("innerCodes", innerCodes.length() > 0 ? innerCodes.substring(0, innerCodes.length() - 1) : "");
				vo.put("industrys", industrys.length() > 0 ? industrys.substring(0, industrys.length() - 1) : "");
				resList.add(vo);
			}
		}
		return resList;
	}

	@Override
	public BLOB getReportFile(String fileId) {
		Map<String, Object> res = xnSyncDao.getReportFile(fileId);
		if (res != null) {
			//一定用大写CONTENT，否则oracle数据库查询结果不匹配
			return (BLOB) res.get("CONTENT");
		}
		return null;
	}

	@Override
	public int getReportDeepStatus(long researchId) {
		ReportInfo report = reportMapper.selectByPrimaryKey(researchId);
		return report == null ? 0 : (report.getDeepStatus() == null ? 0 : report.getDeepStatus());
	}

}
