package com.meix.institute.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.api.IXnSyncService;
import com.meix.institute.api.ISyncTableService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.BaseResp;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsSyncTableJsid;
import com.meix.institute.mapper.InsSyncTableJsidMapper;
import com.meix.institute.pojo.InsSyncTableJsidInfo;
import com.meix.institute.pojo.InsSyncTableJsidSearch;

import tk.mybatis.mapper.entity.Example;

/**
 * Created by zenghao on 2020/4/5.
 */
@Service
public class SyncTableServiceImpl implements ISyncTableService {

	@Autowired
	private InsSyncTableJsidMapper syncTableJsIdMapper;
	@Autowired
	private IXnSyncService bgSyncService;
	
    @Override
    public InsSyncTableJsid getLastSyncTableRecord(String tableName, String user) {
        Example example = new Example(InsSyncTableJsid.class);
        example.createCriteria().andEqualTo("tableName", tableName);
        example.orderBy("jsUpdateTimeFm").desc();
        RowBounds rowBounds = new RowBounds(0, 1);
        List<InsSyncTableJsid> list = syncTableJsIdMapper.selectByExampleAndRowBounds(example, rowBounds);
        if (list == null || list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public void retry(Long id, String user) {
        InsSyncTableJsid record = syncTableJsIdMapper.selectByPrimaryKey(id);
        if (record == null || StringUtils.equals(MeixConstants.SYNC_STATE_SUCCESS, record.getState())) {
            return;
        }
        record.setRetryCnt(record.getRetryCnt() + 1);
        record.setRetryMax(record.getRetryMax() + 1);
        record.setUpdatedAt(new Date());
        record.setUpdatedBy(user);
        InsSyncTableJsid syncRecord = new InsSyncTableJsid();
        syncRecord.setTableName(record.getTableName());
        BaseResp action = bgSyncService.action(syncRecord, MeixConstants.CREATED_VIA_2, false, user);
        
        if (action.isSuccess()) {
            record.setState(MeixConstants.SYNC_STATE_SUCCESS);
        } else {
            record.setRemarks(action.getErr_desc());
            record.setState(MeixConstants.SYNC_STATE_FAIL);
        }
        syncTableJsIdMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public void close(Long id, String remarks, String user) {
        InsSyncTableJsid record = new InsSyncTableJsid();
        record.setState(MeixConstants.SYNC_STATE_CLOSE);
        record.setId(id);
        record.setUpdatedAt(new Date());
        record.setUpdatedBy(user);
        record.setRemarks(remarks);
        syncTableJsIdMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Paged<InsSyncTableJsidInfo> paged(InsSyncTableJsidSearch search) {
        Paged<InsSyncTableJsidInfo> paged = new Paged<InsSyncTableJsidInfo>();
        Example example = new Example(InsSyncTableJsid.class);
        example.createCriteria().andEqualTo(search);
        example.orderBy("id").desc();
        int count = syncTableJsIdMapper.selectCountByExample(example);
        paged.setCount(count);
        if (count < 1) {
            paged.setList(Collections.emptyList());
        }
        RowBounds rowBounds = new RowBounds(search.getCurrentPage(), search.getShowNum());
        List<InsSyncTableJsid> list = syncTableJsIdMapper.selectByExampleAndRowBounds(example, rowBounds);
        List<InsSyncTableJsidInfo> infoList = getServiceList(list);
        paged.setList(infoList);
        return paged;
    }
    
    private List<InsSyncTableJsidInfo> getServiceList(List<InsSyncTableJsid> daolist) {
        if (daolist != null && daolist.size() > 0) {
            List<InsSyncTableJsidInfo> list = new ArrayList<InsSyncTableJsidInfo>();
            daolist.forEach((info)->{
                InsSyncTableJsidInfo data = new InsSyncTableJsidInfo();
                BeanUtils.copyProperties(info, data);
                list.add(data);
            });
            return list;
        }
        return Collections.emptyList();
    }
	
}
