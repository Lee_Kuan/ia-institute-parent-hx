package com.meix.institute.impl;

import com.meix.institute.BaseService;
import com.meix.institute.api.ISecumainService;
import com.meix.institute.api.ISelfStockService;
import com.meix.institute.entity.InsSelfStock;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.mapper.InsSelfStockMapper;
import com.meix.institute.search.StockYieldSearchVo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.vo.stock.StockVo;
import org.apache.commons.collections.MapUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by zenghao on 2019/9/23.
 */
@Service
public class SelfStockServiceImpl extends BaseService implements ISelfStockService {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private InsSelfStockMapper selfStockMapper;
	@Autowired
	private ISecumainService secumainService;

	@Override
	public List<StockVo> getUserSelfStockList(Map<String, Object> params) {
		long startTime = System.currentTimeMillis();
		long uid = MapUtils.getLongValue(params, "uid", 0);
		int currentPage = MapUtils.getIntValue(params, "currentPage", 0);
		int showNum = MapUtils.getIntValue(params, "showNum", 20);
		List<InsSelfStock> stockList = getSelfStockList(uid, currentPage, showNum);
		if (stockList == null || stockList.size() == 0) {
			return Collections.emptyList();
		}

		String endDate = DateUtil.getCurrentDate();
		List<StockYieldSearchVo> searchList = new ArrayList<>();
		List<StockVo> list = new ArrayList<>();
		for (InsSelfStock item : stockList) {
			SecuMain secuMain = secumainService.getSecuMain(item.getInnerCode());
			if (secuMain == null) {
				continue;
			}

			StockVo vo = new StockVo();
			vo.setInnerCode(item.getInnerCode());
			vo.setSecuCode(secuMain.getSecuCode());
			vo.setSecuAbbr(secuMain.getSecuAbbr());
			vo.setSuffix(secuMain.getSuffix());
			vo.setSecuMarket(secuMain.getSecuMarket());
			vo.setSecuClass(secuMain.getSecuCategory());
			vo.setIndustryCode(secuMain.getIndustryCode());
			vo.setIndustryName(secuMain.getIndustryName());
			vo.setAuthorCode(item.getUid());
			vo.setIndustryCode(secuMain.getIndustryCode());
			vo.setCreateTime(DateUtil.dateToStr(item.getCreatedAt(), "yyyy-MM-dd HH:mm:ss"));
			vo.setIsSelfstock(1);
			String startDate = DateUtil.dateToStr(item.getCreatedAt(), "");

			StockYieldSearchVo searchVo = new StockYieldSearchVo();
			searchVo.setInnerCode(item.getInnerCode());
			searchVo.setStartDate(startDate);
			searchVo.setEndDate(endDate);
			searchList.add(searchVo);

			list.add(vo);
		}

		Map<Integer, BigDecimal> yieldMap = getStockIntervalYield(searchList);
		if (yieldMap != null) {
			for (StockVo item : list) {
				BigDecimal stockYield = yieldMap.get(item.getInnerCode());
				if (stockYield != null) {
					item.setAccumulatedYieldRate(stockYield.setScale(4, BigDecimal.ROUND_HALF_UP).floatValue());
				}
			}
		}
		log.info("查询研究所自选股列表耗时{}ms", (System.currentTimeMillis() - startTime));
		return list;
	}

	@Override
	public void addSelfStock(int innerCode, long uid) {
		InsSelfStock selfStock = new InsSelfStock();
		selfStock.setUid(uid);
		selfStock.setInnerCode(innerCode);
		selfStock.setCreatedAt(new Date());
		selfStock.setUpdatedAt(new Date());
		selfStock.setIsDeleted(0);
		selfStockMapper.insert(selfStock);
	}

	@Override
	public void deleteSelfStock(int innerCode, long uid, String uuid) {
		Example example = new Example(InsSelfStock.class);
		example.createCriteria().andEqualTo("innerCode", innerCode).andEqualTo("uid", uid);
		InsSelfStock selfStock = new InsSelfStock();
		selfStock.setIsDeleted(1);

		selfStock.setUpdatedAt(new Date());
		selfStock.setUpdatedBy(uuid);
		selfStockMapper.updateByExampleSelective(selfStock, example);
	}

	@Override
	public List<InsSelfStock> getSelfStockList(long uid, int offset, int showNum) {
		Example example = new Example(InsSelfStock.class);
		Example.Criteria criteria = example.createCriteria();
		if (uid != -1) {
			criteria.andEqualTo("uid", uid);
		}
		criteria.andEqualTo("isDeleted", 0);
		example.orderBy("createdAt").desc();
		if (showNum > 0) {
			RowBounds rowBounds = new RowBounds(offset, showNum);
			return selfStockMapper.selectByExampleAndRowBounds(example, rowBounds);
		}
		return selfStockMapper.selectByExample(example);
	}

	@Override
	public InsSelfStock getSelfStock(long uid, int innerCode) {
		Example example = new Example(InsSelfStock.class);
		example.createCriteria().andEqualTo("uid", uid)
			.andEqualTo("innerCode", innerCode)
			.andEqualTo("isDeleted", 0);
		return selfStockMapper.selectOneByExample(example);
	}

	@Override
	public Set<Integer> getSelfStockList(long uid) {
		List<InsSelfStock> list = getSelfStockList(uid, 0, 0);
		if (list == null || list.size() == 0) {
			return Collections.emptySet();
		}
		Set<Integer> result = new HashSet<>();
		for (InsSelfStock item : list) {
			result.add(item.getInnerCode());
		}
		return result;
	}

	@Override
	public int updateSelfStock(int innerCode, long uid, int isDeleted) {
		int searchStatus = isDeleted == 0 ? 1 : 0;
		Example example = new Example(InsSelfStock.class);
		example.createCriteria().andEqualTo("uid", uid)
			.andEqualTo("innerCode", innerCode)
			.andEqualTo("isDeleted", searchStatus);
		InsSelfStock selfStock = new InsSelfStock();
		selfStock.setUid(uid);
		selfStock.setInnerCode(innerCode);
		if (isDeleted == 0) {
			selfStock.setCreatedAt(new Date());
		}
		selfStock.setUpdatedAt(new Date());
		selfStock.setIsDeleted(isDeleted);
		return selfStockMapper.updateByExampleSelective(selfStock, example);
	}

	@Override
	public int checkStockIsExist(long uid, int innerCode) {
		Example example = new Example(InsSelfStock.class);
		example.createCriteria().andEqualTo("innerCode", innerCode)
			.andEqualTo("uid", uid)
			.andEqualTo("isDeleted", 0);
		return selfStockMapper.selectCountByExample(example);
	}
}
