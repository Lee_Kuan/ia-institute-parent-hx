package com.meix.institute.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.BaseService;
import com.meix.institute.api.IActivityService;
import com.meix.institute.api.ICompanyService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IDataStatDao;
import com.meix.institute.api.IDictTagService;
import com.meix.institute.api.IInsLoginService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IMeetingService;
import com.meix.institute.api.IReportService;
import com.meix.institute.api.IUserDao;
import com.meix.institute.api.IUserService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsActivity;
import com.meix.institute.entity.InsActivityAnalyst;
import com.meix.institute.entity.InsUser;
import com.meix.institute.entity.ReportInfo;
import com.meix.institute.impl.cache.ServiceCache;
import com.meix.institute.mapper.InsUserMapper;
import com.meix.institute.mapper.ReportInfoMapper;
import com.meix.institute.mapper.ThridPartyDaoImpl;
import com.meix.institute.response.PersonalInfoPo;
import com.meix.institute.search.ClueSearchVo;
import com.meix.institute.search.ComplianceCommentSearchVo;
import com.meix.institute.thread.SendPersonaInfoToMeixThread;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.Des;
import com.meix.institute.util.OssResourceUtil;
import com.meix.institute.util.SecurityUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.Comment;
import com.meix.institute.vo.CommentVo;
import com.meix.institute.vo.GroupPowerVo;
import com.meix.institute.vo.IA_PWDMapping;
import com.meix.institute.vo.SecToken;
import com.meix.institute.vo.company.CompanyClueInfo;
import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.meeting.MorningMeetingVo;
import com.meix.institute.vo.report.ReportDetailVo;
import com.meix.institute.vo.stock.SelfStockCategoryVo;
import com.meix.institute.vo.user.UserClueStat;
import com.meix.institute.vo.user.UserContentReadStat;
import com.meix.institute.vo.user.UserIdVo;
import com.meix.institute.vo.user.UserInfo;
import com.meix.institute.vo.user.UserLabelScoreVo;
import com.meix.institute.vo.user.UserRecommendationInfo;
import com.meix.institute.vo.user.UserRecordStateResp;
import com.meix.institute.vo.user.UserRecordStateVo;

import net.sf.json.JSONArray;
import tk.mybatis.mapper.entity.Example;

/**
 * Created by zenghao on 2019/9/19.
 */
@Service
public class UserServiceImpl extends BaseService implements IUserService {
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	private IUserDao userDaoImpl;
	@Autowired
	private IDataStatDao dataStatDaoImpl;
	@Autowired
	private ServiceCache serviceCache;
	@Autowired
	private IActivityService activityService;
	@Autowired
	private ThridPartyDaoImpl thridPartyDaoImpl;
	@Autowired
	private IInsLoginService insLoginService;
	@Autowired
	private IInsUserService insUserService;
	@Autowired
	private CommonServerProperties serverProperties;
	@Autowired
	private ICompanyService companyService;
	@Autowired
	private IReportService reportService;
	@Autowired
	private IDictTagService dictTagService;
	@Autowired
	private InsUserMapper insUserMapper;
	@Autowired
	private ReportInfoMapper reportMapper;
	@Autowired
	private IInstituteMessagePushService messagePushService;
	@Autowired
	private IMeetingService meetingServiceImpl;
	@Autowired
	private ICustomerGroupService groupServiceImpl;

	private static List<SelfStockCategoryVo> stockList = new ArrayList<>();

	//加载开户用户的初始化数据
	static {
		SelfStockCategoryVo stockVo = new SelfStockCategoryVo();
		stockVo.setCategoryName("自选一");
		stockList.add(stockVo);
		stockVo = new SelfStockCategoryVo();
		stockVo.setCategoryName("自选二");
		stockList.add(stockVo);
		stockVo = new SelfStockCategoryVo();
		stockVo.setCategoryName("自选三");
		stockList.add(stockVo);
		stockVo = new SelfStockCategoryVo();
		stockVo.setCategoryName("自选四");
		stockList.add(stockVo);
	}

	@Override
	public List<Map<String, Object>> getMyConcrenedPersons(Map<String, Object> params) {
		List<Map<String, Object>> list = userDaoImpl.selectMyConcrenedPersons(params);
		if (list == null || list.size() == 0) {
			return list;
		}
		for (Map<String, Object> item : list) {
			String headImageUrl = (String) item.get("headImageUrl");
			item.put("headImageUrl", headImageUrl);
			item.put("position", dictTagService.getText(MeixConstants.POSITION, (String) item.get("position")));
		}
		return list;
	}

	@Override
	public List<UserInfo> getUser(Map<String, Object> params) {
		return userDaoImpl.getUser(params);
	}

	@Override
	public List<Map<String, Object>> getUncheckedUserList(Map<String, Object> params) {
		List<Map<String, Object>> list = userDaoImpl.getUncheckedUserList(params);
		for (Map<String, Object> item : list) {
			String url = MapUtils.getString(item, "cardUrl", null);
			String cardUrl = serverProperties.getFileServer();
			if (url != null && !url.startsWith("http")) {
				if (!cardUrl.endsWith("/")) {
					cardUrl = cardUrl + "/";
				}
				cardUrl += "file/show" + url;
				item.put("cardUrl", cardUrl);
			}
		}
		return list;
	}

	@Override
	public int getUncheckedUserCount(Map<String, Object> params) {
		return userDaoImpl.getUncheckedUserCount(params);
	}

	@Override
	public PersonalInfoPo getPersonalInfo(long uid, long authorId) {
		
		if (uid != authorId) {
			List<GroupPowerVo> powerList = groupServiceImpl.getResourceGroupPower(uid, 0, MeixConstants.USER_YH, 0, MeixConstants.SHARE_DETAIL);
			if (powerList == null || powerList.isEmpty() || powerList.get(0).getPower() == 0) { //无权限
				return new PersonalInfoPo(0);
			}
		}
		UserInfo user = userDaoImpl.getPersonalInfo(uid, authorId);
		if (user == null) {
			return new PersonalInfoPo(1);
		}
		PersonalInfoPo po = new PersonalInfoPo(1);
		po.setAccountType(user.getAccountType());
		po.setDirection(user.getDirection());
		po.setTelephone(user.getMobile());
		po.setAuthorCode(user.getId());
		po.setAuthorName(user.getUserName());
		if (StringUtils.isBlank(user.getUserName()) || MeixConstants.DEFAULT_NAME.equals(user.getUserName())) {
			if (StringUtils.isNotBlank(user.getNickName())) {
				po.setAuthorName(user.getNickName());
			}
		}
		po.setOrgName(user.getCompanyAbbr());
		po.setPosition(user.getPosition());
		po.setComment(user.getComment());
		String headImgUrl = OssResourceUtil.getHeadImage(user.getHeadImageUrl());
		if (StringUtil.isBlank(headImgUrl)) {
			headImgUrl = user.getWechatHeadUrl();
		}
		po.setAuthorHeadImgUrl(headImgUrl);
		po.setAuthStatus(user.getAuthStatus());
		po.setCompanyAbbr(user.getCompanyAbbr());
		po.setEmail(user.getEmail());
		po.setReason(StringUtils.isBlank(user.getReason()) ? MeixConstants.DEFAULT_REASON : user.getReason());
		// 头像为空，取上传名片
		String cardUrl = user.getCardUrl();
		po.setAuthorCardUrl(cardUrl);
		// 关注标志
		boolean focusedPerson = userDaoImpl.isFocusedPerson(uid, authorId);
		po.setFollowFlag(focusedPerson ? 1 : 0);
		// 关注数
		long focusedPersonCount = userDaoImpl.getFocusedPersonCount(authorId);
		po.setSubscribeNum(focusedPersonCount);
		// 粉丝数
		int uesrFollowNum = userDaoImpl.getUesrFollowNum(authorId);
		po.setFollowNum(uesrFollowNum);
		// 浏览量
		int readNum = userDaoImpl.getReadNum(authorId);
		po.setReadNum(readNum);
		po.setIdentify(user.getIdentify());

		return po;
	}

	@Override
	public void saveSystemLabel(Map<String, Object> params) {
		userDaoImpl.deleteSystemLabel(params);
		if (MapUtils.getInteger(params, "size") > 0)
			userDaoImpl.saveSystemLabel(params);
	}

	@Override
	public List<Map<String, Object>> getSystemLabel(long uid, int searchType, int requireType) {
		List<Map<String, Object>> list = userDaoImpl.getSystemLabel(uid, searchType);
		Iterator<Map<String, Object>> it = list.iterator();
		while (it.hasNext()) {
			Map<String, Object> temp = it.next();
			int ivalue = MapUtils.getIntValue(temp, "ivalue", 1);
			if (requireType > 1) {
				//移除能整除requireType的标签
				if (ivalue % requireType == 0) {
					it.remove();
					continue;
				}
				//活动、新建组合等不需要勾选状态
				temp.put("selected", 0);
				int dm = MapUtils.getIntValue(temp, "DM");
				//单行业，多行业
				temp.put("type", serviceCache.checkSWIndustry(dm) ? 1 : 2);
			}
			temp.remove("ivalue");
		}
		return list;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getUidByOpenId(String openId, int clientType) {
		return userDaoImpl.getUidByOpenId(openId, clientType);
	}

	@Override
	public long saveServerComment(long uid, long pointId, long pointGroupId,
	                              Float score, String commentStr, long dataId, int dataType, int hideNameFlag) {
//		if (getAccountType(uid) != MeixConstants.ACCOUNT_INNER &&
//			getAccountType(uid) != MeixConstants.ACCOUNT_WL &&
//			getAccountType(uid) != MeixConstants.ACCOUNT_NORMAL) {
//			return 0;
//		}
		Comment comment;
		long authorId = 0;
		List<Long> authorIds = new ArrayList<Long>();

		if (dataType == MeixConstants.USER_GD) {//观点
			return 0;
		} else if (dataType == MeixConstants.USER_GDFW || dataType == MeixConstants.USER_YBFW || dataType == MeixConstants.USER_DHHYFW
			|| dataType == MeixConstants.USER_LYFW || dataType == MeixConstants.USER_DYFW) {//专辑

			authorId = dataId;
			authorIds.add(authorId);
		} else if (dataType == MeixConstants.USER_YB) {
//			ReportBean vo = cacheServiceImpl.getReportInfo(dataId);
//			List<Map<String, Object>> authors = reportServiceImpl.getReportAuthorList(uid, vo, 2);
//			for (Map<String, Object> temp : authors) {
//				long authorCode = MapUtils.getLongValue(temp, "authorCode", 0);
//				if (authorCode != 0) {
//					authorIds.add(authorCode);
//				}
//			}
		} else if (dataType == MeixConstants.USER_HD) {
			List<InsActivityAnalyst> analystList = activityService.getActivityAnalyst(dataId);
			for (InsActivityAnalyst map : analystList) {
				long analystId = map.getUid();
				if (analystId != 0) {
					authorIds.add(analystId);
				}
			}
		}
		comment = new Comment();
		comment.setUid(uid);
		comment.setDataId(dataId);
		comment.setDataType(dataType);
		comment.setOpenId("");
		comment.setComment(commentStr);
		comment.setOrgFlag(getAccountType(uid));
		comment.setScore(score);
		comment.setHideNameFlag(hideNameFlag);
		comment.setStatus(MeixConstants.COMMENT_STATUS_0);
		userDaoImpl.saveComment(comment);

		if (authorIds.size() > 0) {
			userDaoImpl.saveCommentAuthorId(comment.getId(), authorIds);
		}
		if (dataType == 8) {
			userDaoImpl.saveCommentRelation(uid, dataId, dataType);
		}

		return dataType;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CommentVo> getCommentList(long uid, long id, int type, long version, int currentPage,
	                                      int showNum, JSONArray jonsIds, long companyCode, String condition) {
		Set<Long> ids = null;
		if (jonsIds != null) {
			ids = new HashSet<Long>(jonsIds);
		}
		List<CommentVo> list = userDaoImpl.getCommentList(id, type, currentPage * showNum, showNum, ids, companyCode, condition);
		for (int i = 0; i < list.size(); i++) {
			CommentVo cv = list.get(i);
//			唯一资源id (2.9.0)
			cv.setResourceId(String.valueOf(cv.getId()) + "_21");
			//活动相关信息 (2.7.0)
			if (8 == type) {
				long dataId = cv.getDataId();
				InsActivity info = activityService.getBaseActivityById(dataId);
				//所有活动都返回活动信息
				if (null != info) {
					Map<String, Object> activity = new HashMap<>();
					activity.put("id", info.getId());
					activity.put("activityType", info.getActivityType());
					activity.put("resourceUrl", info.getResourceUrl());
					activity.put("title", info.getTitle());
					activity.put("startTime", info.getStartTime());
					List<InsActivityAnalyst> analystList = activityService.getActivityAnalyst(dataId);
					String analystName = "";
					if (analystList != null && analystList.size() > 0) {
						for (InsActivityAnalyst analyst : analystList) {
							String name = analyst.getAnalystName();
							if (StringUtils.isNotEmpty(name)) {
								analystName += name + "、";
							}
						}
						if (analystName.endsWith("、")) {
							analystName = analystName.substring(0, analystName.length() - 1);
						}
					}
					activity.put("analystName", analystName);
					activity.put("analyst", analystList);
					cv.setActivity(activity);
				}
			}
			String commentObj = cv.getCommentObject();
			cv.setCommentObject("已评价「" + commentObj + "」");
			String commentContent = cv.getCommentContent();
			if (TextUtils.isEmpty(commentContent)) {
				cv.setCommentContent("我给这次" + cv.getTitle() + Math.round(cv.getScore()) + "分。");
			}
			UserInfo uic = getUserInfo(cv.getAuthorCode());
			if (uic != null) {
				cv.setAuthorName(uic.getUserName());
				cv.setAuthorHeadImgUrl(OssResourceUtil.getHeadImage(uic.getHeadImageUrl()));
				cv.setOrgCode(uic.getCompanyCode());
				cv.setOrgName(uic.getCompanyAbbr());
				cv.setPosition(uic.getPosition());
			}
			switch (cv.getType()) {
				case MeixConstants.USER_YB:
				case MeixConstants.USER_YBFW:
					ReportDetailVo rb = reportService.getReportDetail(cv.getDataId(), true);
					if (rb != null) {
						cv.setCommentObject("已评价「" + rb.getTitle() + "」");
					} else {
						cv.setCommentObject("");
					}
					break;
			}
		}

		return list;
	}

	@Override
	public List<Map<String, Object>> getFocusedPersons(long uid, int searchType, int showNum, int currentPage) {
		currentPage = showNum * currentPage;
		List<UserIdVo> userIdList = null;
		List<UserIdVo> followEachOtherList = userDaoImpl.getMyFocusedPersonsFocusMe(uid, -1, currentPage);
		boolean bCheckFocusEachOther = true;
		switch (searchType) {
			case 0://热门关注人员列表
			case 4://热门关注人员关注的人员列表
				userIdList = new ArrayList<UserIdVo>();
				List<UserIdVo> focusedPersonList = userDaoImpl.getFocusedPersonsByMyFocusedPersons(uid, showNum, currentPage);
				List<UserIdVo> hotPersonList = userDaoImpl.getHotFocusedPersons(uid, showNum, currentPage);
				if (focusedPersonList != null && focusedPersonList.size() > 0) {
					userIdList.addAll(focusedPersonList);
					for (int i = 0; i < hotPersonList.size(); i++) {
						UserIdVo uiv = hotPersonList.get(i);
						if (!isUserAdded(uiv, focusedPersonList)) {
							userIdList.add(uiv);
							if (userIdList.size() == 8) {
								break;
							}
						}
					}
				} else if (hotPersonList != null) {
					userIdList.addAll(hotPersonList);
				}

				break;
			case 1://我关注的人
				userIdList = userDaoImpl.getMyFocusedPersons(uid, showNum, currentPage);
				break;
			case 2://关注我的人
				bCheckFocusEachOther = false;
				userIdList = userDaoImpl.getPersonsFocusMe(uid, showNum, currentPage);
				break;
			case 3://我关注了并且关注了我的人
				bCheckFocusEachOther = false;
				userIdList = userDaoImpl.getMyFocusedPersonsFocusMe(uid, showNum, currentPage);
				break;
		}

		return getUserInfoList(userIdList, followEachOtherList, bCheckFocusEachOther);
	}

	/**
	 * @param list 用户ID列表
	 * @author Oliver
	 * @Description: rides取用户信息
	 */
	public List<Map<String, Object>> getUserInfoList(List<UserIdVo> list, List<UserIdVo> followEachOtherList, boolean bCheckFocusEachOther) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		if (list == null) {
			return result;
		}
		for (UserIdVo contactId : list) {
			int type = contactId.getType();
			switch (type) {
				case 4:
					UserInfo userInfo = getUserInfo(contactId.getId());
					if (null != userInfo) {
						Map<String, Object> contact = new HashMap<String, Object>();
						contact.put("headImageUrl", OssResourceUtil.getHeadImage(userInfo.getHeadImageUrl()));
						contact.put("authorCode", contactId.getId());
						contact.put("authorName", userInfo.getUserName());
						if (StringUtils.isBlank(userInfo.getUserName()) || MeixConstants.DEFAULT_NAME.equals(userInfo.getUserName())) {
							if (StringUtils.isNotBlank(userInfo.getNickName())) {
								contact.put("authorName", userInfo.getNickName());
							}
						}
						contact.put("orgCode", userInfo.getCompanyCode());
						contact.put("orgName", userInfo.getCompanyAbbr());
						contact.put("authorType", 4);
						if (bCheckFocusEachOther) {
							if (isFollowEachOther(contactId, followEachOtherList)) {
								contact.put("focusStatus", 2);
							} else {
								contact.put("focusStatus", contactId.getFocusStatus());
							}
						} else {
							contact.put("focusStatus", contactId.getFocusStatus());
						}
					}
					break;
				case 19:
					break;
				default:
					break;
			}
		}
		return result;
	}

	/**
	 * 判断该用户是否已经加入了用户列表
	 *
	 * @param userList 当前的用户列表
	 * @return
	 */
	private boolean isUserAdded(UserIdVo uiv, List<UserIdVo> userList) {
		for (UserIdVo user : userList) {
			if (uiv.getId() == user.getId() && uiv.getType() == user.getType()) {
				return true;
			}
		}

		return false;
	}

	/*
		 * 保存最后一次点击记录
		 * @see com.meix.service.IUserService#saveLastUserRecord(com.meix.model.user.UserRecordStateVo)
		 */
	@Override
	public void saveLastUserRecord(UserRecordStateVo vo) {
		//需求修改，保存用户最后一次记录修改为保存用户所有的点击记录
		userDaoImpl.saveLastUserRecord(vo);
	}

	@Override
	public String getOpenIdByUid(long uid, String appId, int clientType) {
		return userDaoImpl.getOpenIdByUid(uid, appId, clientType);
	}

	@Override
	public UserInfo getUserInfo(long uid, String areaCode, String mobileOrEmail) {
		return userDaoImpl.getUserInfo(uid, areaCode, mobileOrEmail);
	}

	@Override
	public UserInfo getUserInfoByUuid(String user) {
		return userDaoImpl.getUserInfoByUuid(user);
	}

	@Override
	public int saveUserRecordState(UserRecordStateVo vo) {
		return saveUserRecordState(vo, true, true);
	}

	@Override
	public int saveUserRecordState(UserRecordStateVo vo, boolean log, boolean isPushFollow) {
		int state = saveUserRecordState(vo, log, isPushFollow, true);
		vo.setRet(state);
		// 推送至每市
		MeixConstants.fixedThreadPool.execute(new SendPersonaInfoToMeixThread(Collections.singletonList(vo)));
		return state;
	}


	/**
	 * @return ret 如果是进行关注用户操作 0:未关注 1：已关注 2：互相关注
	 */
	@Override
	public int saveUserRecordState(UserRecordStateVo vo, boolean log, boolean isPushFollow, boolean queue) {
		if (StringUtils.isEmpty(vo.getCreateTime())) {
			vo.setCreateTime(DateUtil.getCurrentDateTime());
		}
		int count = 0;
		boolean isExist = true;
		//获取用户信息
		UserInfo user;
		if (vo.getUid() > 0) {
			user = getUserInfo(vo.getUid(), null, null);
		} else {
			user = new UserInfo();
		}
		//无效数据
		if (user == null) {
			return 0;
		}
		//保存流水记录
		int logCount = 0;
		if (log) {
			logCount = userDaoImpl.saveUserRecordStateLog(vo);
		}

		if (vo.getDataType() == MeixConstants.USER_HOME_PAGE ||
			vo.getDataType() == MeixConstants.USER_APPLET ||
			vo.getDataType() == MeixConstants.PAGE_RESEARCH ||
			vo.getDataType() == MeixConstants.PAGE_ACTIVITY ||
			vo.getDataType() == MeixConstants.PAGE_MINE ||
			vo.getDataType() == MeixConstants.REPORT_ALBUM_DETAIL ||
			vo.getDataType() == MeixConstants.MEETING_ALBUM_DETAIL ||
			vo.getDataType() == MeixConstants.USER_HYJY
			) {
			//这些数据只存log
			return logCount;
		}

		//外部用户 微信阅读活动加入统计
		if (vo.getUid() == 0 && (vo.getDataType() != MeixConstants.USER_HD
			&& vo.getDataType() != MeixConstants.USER_CH && vo.getDataType() != MeixConstants.USER_YB)) {
			//保存流水记录
			return logCount;
		}
		isExist = checkUserRecordState(vo);
		if (vo.getUid() == 0 && StringUtils.isEmpty(vo.getOpenId())) {
			isExist = false;
		}
		//组合 调仓 观点 活动（外部用户除外）的阅读数点一次算一次
		if (vo.getDataState() == MeixConstants.RECORD_READ) {
			switch (vo.getDataType()) {
				case MeixConstants.USER_YB: {//阅读数记录包含用户多次阅读行为
					break;
				}
				default:
					if (isExist) {
						logger.info("重复的用户行为，uid:{}，dataType:{}，dataId:{}，dataState:{}", vo.getUid(), vo.getDataType(), vo.getDataId(), vo.getDataState());
						return count;
					}
					break;
			}
		}
		//统计记录的数量
		int ret = calcRecordNum(isExist, isPushFollow, vo);
		//组合取消关注，关闭提醒
		if (isExist && vo.getDataState() == MeixConstants.RECORD_FOLLOW) {
			vo.setDataState(MeixConstants.RECORD_REMIND);
			userDaoImpl.deleteUserRecordState(vo);
		}

		if ((vo.getDataType() == MeixConstants.USER_YH)
			&& vo.getDataState() == MeixConstants.RECORD_FOLLOW) {
			return ret;
		} else {
			return 1;
		}
	}

	@Override
	public int saveUserRecordStateList(List<UserRecordStateVo> voList) {
		int index = 0;
		for (UserRecordStateVo vo : voList) {
			//点击 和播放 保存到日志表
			if (vo.getDataState() == MeixConstants.RECORD_DURATION || vo.getDataState() == MeixConstants.RECORD_SHARE) {
				continue;
			} else if (vo.getDataState() == MeixConstants.RECORD_CLICK) {
				continue;
			}
			index += saveUserRecordState(vo, false, false);
		}

		if (voList.size() > 0) {
			List<UserRecordStateVo> tmpList = new ArrayList<>();
			for (int i = 0; i < voList.size(); i++) {
				tmpList.add(voList.get(i));
				if (i != 0 && i % 400 == 0) {
					index += userDaoImpl.batchInsertUserRecordStateLog(tmpList);
					tmpList.clear();
				}
			}
			if (tmpList.size() > 0) {
				index += userDaoImpl.batchInsertUserRecordStateLog(tmpList);
			}
		}
		return index;
	}

	/**
	 * @param vo
	 * @return
	 * @Title: checkUserRecordState、
	 * @Description:检查记录是否存在，并保存
	 * @return: boolean
	 */
	private boolean checkUserRecordState(UserRecordStateVo vo) {
		boolean isExist = true;
		//活动 观点  调仓的阅读批量上传
		if ((vo.getDataState() == MeixConstants.RECORD_READ &&
			(vo.getDataType() == MeixConstants.USER_HD ||
				vo.getDataType() == MeixConstants.USER_CH ||
				vo.getDataType() == MeixConstants.USER_YB))
			) {
			//外部用户
			if (vo.getUid() == 0) {
				return isExist;
			}
			UserRecordStateVo value = userDaoImpl.getUserRecordState(vo.getDataType(), vo.getDataId(), vo.getUid(), vo.getDataState());
			if (value == null) {
				isExist = false;
				try {
					userDaoImpl.saveUserRecordState(vo);
				} catch (Exception e) {
					logger.error("用户记录重复:" + vo.toString());
					isExist = true;
				}
			}
			return isExist;
		}

		if (userDaoImpl.updateUserRecordState(vo) == 0) {
			isExist = false;
			userDaoImpl.saveUserRecordState(vo);
		} else {
			if (vo.getDataState() == MeixConstants.RECORD_REMIND) {
				//取消提醒
				userDaoImpl.deleteUserRecordState(vo);
			}
		}
		return isExist;
	}

	/**
	 * @param isExist
	 * @param isPushFollow
	 * @param vo
	 * @Title: calcRecordNum、
	 * @Description:统计记录数量
	 * @return: int 0:未关注 1：已关注 2：互相关注
	 */
	private int calcRecordNum(boolean isExist, boolean isPushFollow, UserRecordStateVo vo) {
		int ret = 0;
		//关注
		if (vo.getDataState() == MeixConstants.RECORD_FOLLOW) {
			logger.info("【统计记录数量】isExist：{}，isPushFollow：{}，vo：{}", isExist, isPushFollow, vo);
			if (isExist) {
				//取消关注
				userDaoImpl.deleteUserRecordState(vo);
			} else {
				ret = 1;
				//如果是每市用户，加关注后需要判断是否是互相关注状态
				if (vo.getDataType() == MeixConstants.USER_YH) {
					long uid = vo.getUid();
					List<UserIdVo> followEachOtherList = userDaoImpl.getMyFocusedPersonsFocusMe(uid, -1, 0);
					boolean bIsFollowEachOther = isFollowEachOther(vo.getDataId(), followEachOtherList);
					if (bIsFollowEachOther) {
						ret = 2;
					}
				}
			}
		}
		return ret;
	}

	/**
	 * 判断该用户是否与登录用户互相关注了
	 *
	 * @param uiv                 用户ID信息
	 * @param followEachOtherList 与登录用户互相关注了的用户列表
	 * @return
	 */
	private boolean isFollowEachOther(UserIdVo uiv, List<UserIdVo> followEachOtherList) {
		if (followEachOtherList == null || uiv == null) {
			return false;
		}
		for (UserIdVo followUser : followEachOtherList) {
			if (uiv.getId() == followUser.getId() && uiv.getType() == followUser.getType()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 判断该用户是否与登录用户互相关注了
	 *
	 * @param uid                 用户ID信息
	 * @param followEachOtherList 与登录用户互相关注了的用户列表
	 * @return
	 */
	private boolean isFollowEachOther(long uid, List<UserIdVo> followEachOtherList) {
		if (followEachOtherList == null) {
			return false;
		}
		for (UserIdVo followUser : followEachOtherList) {
			if (uid == followUser.getId() && followUser.getType() == 4) {
				return true;
			}
		}

		return false;
	}

	@Override
	public void saveComment(Comment comment) {
		userDaoImpl.saveComment(comment);
	}

	@Override
	public String saveVerCode(IA_PWDMapping bean) {
		Date tday = new Date();
		Map<String, Object> params = new HashMap<>();
		params.put("mobileOrEmail", bean.getMobileOrEmail());
		params.put("loginType", bean.getLoginType());
		params.put("tday", DateUtil.dateToStr(tday, "yyyy-MM-dd HH:mm"));
		IA_PWDMapping obj = userDaoImpl.getUserVerCode(bean.getMobileOrEmail(), bean.getLoginType(), DateUtil.dateToStr(tday, "yyyy-MM-dd HH:mm"));
		if (obj != null) {
			//判断上次发送验证码的时间是否在5分钟之内;
			if ((obj.getCreateTime().getTime() + 300000) > new Date().getTime()) {
				logger.info("获取5分钟之内的验证码");
				bean.setSmsChange(true);
				return obj.getPwdOrUrl();
			}
		}
		String verCode = "";
		verCode = StringUtils.getFixLenthString(6);
		bean.setIsDeleted(false);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) + 5); //验证码改为5分钟有效
		bean.setCreateTime(new Date());
		bean.setUpdateTime(calendar.getTime());
		bean.setPwdOrUrl(verCode);
		userDaoImpl.updateVerCode(bean.getMobileOrEmail(), bean.getLoginType());
		userDaoImpl.saveVerCode(bean);
		return verCode;
	}

	@Override
	public boolean checkVerCode(String mobile, int loginType, String verCode) {
		IA_PWDMapping obj = userDaoImpl.getUserVerCode(mobile, loginType, DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm"));
		//验证验证码是否正确
		if (obj == null) {
			logger.info("您的验证码已经过期，请重新获取!");
			return false;
		}
		if (verCode != null && verCode.equals(obj.getPwdOrUrl())) {
			return true;
		}
		return false;
	}

	@Override
	public void updateUserCompanyCode(long uid, long companyCode, String userName) {
		userDaoImpl.updateUserCompanyCode(uid, companyCode, userName);
	}

	@Override
	public UserInfo addUser(String mobile, String companyName, String nickname) {
		// 用户参数
		UserInfo userInfo = new UserInfo();
		userInfo.setUserName(nickname); // 简称
		userInfo.setMobile(mobile); // 手机号
		userInfo.setCompanyName(companyName); // 机构名称
		userInfo.setAccountType(MeixConstants.ACCOUNT_NORMAL); // 账户类型

		CompanyInfoVo companyInfoVo = companySerciveImpl.getCompanyInfoByName(companyName);
		if (companyInfoVo == null) {
			CompanyInfoVo vo = new CompanyInfoVo();
			vo.setCompanyName(companyName);
			vo.setCompanyAbbr(companyName);
			long companyCode = companySerciveImpl.addCompanyInfo(vo);
			userInfo.setCompanyCode(companyCode);
		} else {
			userInfo.setCompanyCode(companyInfoVo.getId());
		}
		return userInfo;
	}

	@Override
	public long addUser(UserInfo user) {
		return addUser(user, false);
	}

	@Override
	public long addUser(UserInfo user, boolean checkExist) {
		try {
			//验证用户是否存在
			@SuppressWarnings("rawtypes")
			List<Map> list = userDaoImpl.getUserByMobile(user.getMobile());
			if (list.size() > 0) {
				//已存在
				long uid = MapUtils.getLongValue(list.get(0), "ID", 0l);
				user.setId(uid);
				if (checkExist) {
					return -1;
				} else {
					return uid;
				}
			}
			//随机六位数 初始化密码
			String password = StringUtils.getFixLenthString(6);
			user.setPassword(Des.encrypt(password));

			userDaoImpl.addUser(user);
		} catch (Exception e) {
			logger.error("", e);
		}
		return user.getId();
	}

	@Override
	public int updateUser(UserInfo user) {
		return userDaoImpl.updateUser(user);
	}

	@Override
	public void bindYJSAccount(String openId, String appId, long uid, int clientType) {
		// 删除appid / openid 下绑定的所有用户
		thridPartyDaoImpl.deleteUserByOpenId(openId, appId, clientType);
		/**
		 * 如果用户已经存在绑定关系，则新增会存在多个openId对应一个uid情况
		 * 所以要将uid清掉 by:likuan @2019年11月26日14:31:10
		 */
		thridPartyDaoImpl.deleteUserOpenId(uid, null, clientType);
		// 添加
		thridPartyDaoImpl.insertUserOpenId(uid, openId, appId, clientType);
	}

	@Override
	public void unbindThirdAccount(long uid, String openId, int clientType) {
		thridPartyDaoImpl.deleteUserOpenId(uid, openId, clientType);
	}

	@Override
	public void bindMeixAccount(String openId, String appId, long uid, int clientType) {
		Map<String, Object> userOpenId = thridPartyDaoImpl.getOpenIdByUidAndOpenId(uid, openId, clientType);
		if (MapUtils.getLong(userOpenId, "id", 0l) == 0) {
			thridPartyDaoImpl.insertUserOpenId(uid, openId, appId, clientType);
		}
	}

	@Override
	public UserInfo login(IA_PWDMapping bean, UserInfo user, Integer clientType, boolean resetToken) {
		userDaoImpl.updateVerCode(bean.getMobileOrEmail(), bean.getLoginType());
		//获取未失效token
		String token = null;
		if (resetToken) {
			//绑定openid时，必须重置token，保证单点登录
			insLoginService.logout(user.getMobile(), clientType);
		} else {
			SecToken secToken = insLoginService.getTokenByUser(bean.getMobileOrEmail(), clientType);
			token = secToken == null ? null : secToken.getToken();
		}
		if (StringUtils.isBlank(token)) {
			// 生成token
			token = SecurityUtil.generateLoginToken(user.getId(), user.getMobile(), user.getCompanyCode(), 0, clientType);
			// 保存token
			insLoginService.saveToken(token, user.getMobile(), clientType);
		}
		user.setToken(token);
		return user;
	}

	@Override
	public String loginOut(String token) {
		int updateState = userDaoImpl.loginOut(token);
		if (updateState == 0) return null;
		return "Y";
	}

	@Override
	public Map<String, Object> selectCardByUid(long uid, String mobile, int type) {

		return userDaoImpl.selectCardByUid(uid, mobile, type);
	}

	@Override
	public void unbindweixin(String mobile, int clientType) {
		userDaoImpl.unbindweixin(mobile, clientType);
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	public long addCustomerCard(Map params) {
		params.put("id", 0);
		userDaoImpl.addCustomerCard(params);
		return MapUtils.getLongValue(params, "id", 0);
	}

	@Override
	public int updateUserCard(long uid, long cardId, int authStatus, String companyAbbr, String email, String position, String username) {
		return userDaoImpl.updateUserCard(uid, cardId, authStatus, companyAbbr, email, position, username);
	}

	@Override
	public Paged<Map<String, Object>> getContactList(Map<String, Object> params) {
		Paged<Map<String, Object>> data = insUserService.getContactList(params);
		if (data == null || data.getCount() == 0) {
			return data;
		}
		List<Map<String, Object>> list = data.getList();
		if (list != null && list.size() > 0) {
			long uid = MapUtils.getLongValue(params, "uid", 0);

			for (Map<String, Object> item : list) {
				String user = MapUtils.getString(item, "user");
				String mobile = MapUtils.getString(item, "mobile");
				String userName = MapUtils.getString(item, "contactName");
				String email = MapUtils.getString(item, "email");
				String position = MapUtils.getString(item, "position");
				position = dictTagService.getText(MeixConstants.POSITION, position);
				UserInfo userInfo;
				if (StringUtils.isBlank(user)) {
					userInfo = addInsUser(userName, mobile, email, position, user);
				} else {
					userInfo = userDaoImpl.getUserInfoByUuid(user);
					if (userInfo == null) {
						userInfo = addInsUser(userName, mobile, email, position, user);
					}
				}
				item.put("contactName", userInfo.getUserName());
				item.put("position", position);
				item.put("contactId", userInfo.getId());
				item.put("orgCode", userInfo.getCompanyCode());
				CompanyInfoVo companyInfoVo = companyService.getCompanyInfoById(userInfo.getCompanyCode());
				if (companyInfoVo != null) {
					item.put("orgName", companyInfoVo.getCompanyName());
				}
				item.put("contactImageUrl", OssResourceUtil.getHeadImage(userInfo.getHeadImageUrl()));
				boolean follow = userDaoImpl.isFocusedPerson(uid, userInfo.getId());
				item.put("followFlag", follow ? 1 : 0);
			}
		}
		return data;
	}

	private UserInfo addInsUser(String userName, String mobile, String email, String position, String uuid) {
		UserInfo user = new UserInfo();
		user.setAccountType(MeixConstants.ACCOUNT_INNER);
		user.setMobile(mobile);
		user.setEmail(email);
		user.setUserName(userName);
		user.setCompanyCode(serverProperties.getCompanyCode());
		user.setPosition(position);
		user.setIsDelete(MeixConstants.ACCOUNT_JH);
		user.setRoleType(MeixConstants.ROLE_NORMAL);
		user.setChiSpelling(userName);
		user.setInsUuid(uuid);
		userServiceImpl.addUser(user);
		return user;
	}

	@Override
	public void updateUserAuditStatus(Map<String, Object> params) {
		String operatorId = MapUtils.getString(params, "operatorId");
		long customerId = MapUtils.getIntValue(params, "customerId", 0);
		long cardId = MapUtils.getLongValue(params, "cardId", 0);
		String userName = MapUtils.getString(params, "userName");
		String companyName = MapUtils.getString(params, "companyName");
		String position = MapUtils.getString(params, "position");
		int auditResult = MapUtils.getIntValue(params, "auditResult", 0);//审核结果 0 不通过 1通过
		String reason = MapUtils.getString(params, "reason");
		long companyCode = MapUtils.getLongValue(params, "companyCode", 0);
		String email = MapUtils.getString(params, "email", "");
		long groupId = MapUtils.getLongValue(params, "groupId", 0);
		String user = MapUtils.getString(params, "uuid");
		UserInfo userInfo = userDaoImpl.getUserInfo(customerId, null, null);
		if (userInfo == null) {
			return;
		}
		if (auditResult == 0) {
			userDaoImpl.userAuditNotPass(customerId, operatorId, reason);
			userDaoImpl.updateCustomerCardStatus(cardId, -1);
			return;
		} else if (auditResult == 1 || auditResult == 2) {

			if (companyCode == 0) {
				CompanyInfoVo companyInfoVo = companySerciveImpl.getCompanyInfoByName(companyName);
				if (companyInfoVo == null) {
					//公司不存在，需要创建公司
					CompanyInfoVo vo = new CompanyInfoVo();
					vo.setCompanyName(companyName);
					vo.setCompanyAbbr(companyName);
					companyCode = companySerciveImpl.addCompanyInfo(vo);
				} else {
					companyCode = companyInfoVo.getId();
				}
			}
			userDaoImpl.userAuditPass(customerId, userName, operatorId, companyCode, position, auditResult, cardId, email);
			userDaoImpl.updateCustomerCardStatus(cardId, 1);
			groupServiceImpl.saveUserGroup(customerId, groupId, 0, user);
			try {
				//保存商机
				CompanyClueInfo clue = new CompanyClueInfo();
				clue.setUuid(StringUtils.getId24());
				clue.setCompanyCode(commonServerProperties.getCompanyCode());
				clue.setUid(customerId);
				clue.setUserName(userName);
				clue.setMobile(userInfo.getMobile());
				clue.setPosition(position);
				clue.setAddress(userInfo.getCompanyAddress());
				clue.setCustomerCompanyAbbr(companyName);
				clue.setCustomerCompanyCode(companyCode);
				clue.setClueType(MeixConstants.REAL_NAME_AUTH);
				clue.setCustomerType(1);
				clue.setLatestRecordTime(DateUtil.getCurrentDateTime());
				clue.setCreateTime(clue.getLatestRecordTime());
				dataStatDaoImpl.saveClue(clue);
			} catch (Exception e) {
				logger.error("", e);
			}
		}
	}

	@Override
	public boolean updateVerCode(String mobile, int loginType) {
		return userDaoImpl.updateVerCode(mobile, loginType);
	}

	@Override
	public List<UserRecommendationInfo> getRelatedRecommendations(long uid, long authorId, long dataId, int dataType,
	                                                              int currentPage, int showNum, int clientType, int type) {
		int accountType = getAccountType(uid);
		//根据内容的股票、行业获取推荐列表
		List<UserRecommendationInfo> list = null;
		if (type == 2) {
			list = userDaoImpl.getRelatedRecommendationsByAuthor(uid, authorId, dataId, dataType, currentPage, showNum, clientType, accountType);
		} else {
			list = userDaoImpl.getRelatedRecommendations(uid, dataId, dataType, currentPage, showNum, clientType, accountType);
			if (list == null || list.size() == 0) {
				//获取内容作者的其他内容
				list = userDaoImpl.getRelatedRecommendationsByAuthor(uid, 0, dataId, dataType, currentPage, showNum, clientType, accountType);
			}
			if (list == null || list.size() == 0) {
				//获取最新的资源按时间倒叙
				list = userDaoImpl.getRelatedRecommendationsByTime(uid, dataId, dataType, currentPage, showNum, clientType, accountType);
			}
		}
		if (list == null || list.size() == 0) {
			return list;
		}
		for (UserRecommendationInfo info : list) {
			info.setId(info.getDataId());
			if (info.getDataType() == MeixConstants.USER_YB) {
				//reportRate, authorList
//				Example relaExample = new Example(ReportRelation.class);
//				relaExample.createCriteria().andEqualTo("researchId", info.getDataId()).andEqualTo("type", 0);
//				List<ReportRelation> relas = relaMapper.selectByExample(relaExample);
				List<String> rateList = new ArrayList<>();
//				if (relas != null) {
//					for (ReportRelation rela : relas) {
//						rateList.add(rela.getContent());
//					}
//				}
				if (StringUtils.isNotBlank(info.getReportGrade())) {
					rateList.add(info.getReportGrade());
				}

				info.setEvaluateDesc(info.getReportGrade());
				info.setReportRate(rateList);
				List<UserInfo> authorList = userDaoImpl.getReportAuthorInfo(info.getDataId());
				info.setAuthorList(authorList);
				List<String> aList = new ArrayList<>();
				for (UserInfo author : authorList) {
					aList.add(author.getUserName());
				}
				info.setAuthor(aList);
				List<String> secuList = userDaoImpl.getRelatedRecommendationsSecuInfo(info.getDataId(), info.getDataType());
				if (secuList != null && secuList.size() > 0) {
					info.setSecuList(secuList);
					info.setSecuAbbr(secuList.get(0));
				}
				List<String> yjfl = new ArrayList<>();
				yjfl.add(dictTagService.getText(MeixConstants.REPORT_TYPE, info.getInfoType()));
				info.setInfoType(dictTagService.getText(MeixConstants.REPORT_TYPE, info.getInfoType()));
				info.setYjfl(yjfl);
			} else {
				if (info.getIsEnd() == 0) {
					InsActivity activity = new InsActivity();
					activity.setStartTime(info.getActivityStartTime());
					activity.setEndTime(info.getActivityEndTime());
					int isEnd = activityService.getActivityIsEnd(activity);
					int status = activityService.getActivityStatus(7,
						isEnd, info.getIsEnd(), activity.getStartTime(), activity.getEndTime());
					info.setIsEnd(status);
				}
				List<UserInfo> authorList = userDaoImpl.getActivityAnalystInfo(info.getDataId());
				info.setAuthorList(authorList);
				List<String> secuList = userDaoImpl.getRelatedRecommendationsSecuInfo(info.getDataId(), info.getDataType());
				info.setSecuList(secuList);
				if (secuList != null && secuList.size() > 0) {
					info.setSecuAbbr(secuList.get(0));
				}
			}
		}
		return list;
	}


	@Override
	public boolean isFocusedPerson(long uid, long authorCode) {
		return userDaoImpl.isFocusedPerson(uid, authorCode);
	}

	@Override
	public UserInfo getThirdUserInfo(String thirdId, String mobile) {
		Long uid = userDaoImpl.getUidByThirdId(thirdId);
		if (uid == null || uid == 0) {
			//根据手机号再次查询，防止出现重复添加账号
			UserInfo userInfo = userDaoImpl.getUserInfo(0, null, mobile);
			return userInfo;
		}
		UserInfo userInfo = userDaoImpl.getUserInfo(uid, null, null);
		return userInfo;
	}

	@Override
	public long saveThirdUser(UserInfo user) {
		String thirdId = user.getThirdId();
		Long uid = userDaoImpl.getUidByThirdId(thirdId);
		if (uid == null || uid == 0) {
			//根据手机号再次查询，防止出现重复添加账号
			UserInfo userInfo = userDaoImpl.getUserInfo(0, null, user.getMobile());
			if (userInfo != null) {
				uid = userInfo.getId();
			}
		}
		if (uid != null && uid > 0) { // 更新
			user.setId(uid);
			userDaoImpl.updateUser(user);
		} else { // 新增
			userDaoImpl.addUser(user);
			uid = user.getId();
		}
		return uid;
	}

	@Override
	public void callLabelScoreUpdate() {
		List<UserLabelScoreVo> scoreList = userDaoImpl.getUserLabelScoreList();
		List<Long> existId = new ArrayList<>();
		if (scoreList != null && !scoreList.isEmpty()) {
			for (UserLabelScoreVo vo : scoreList) {
				UserLabelScoreVo exist = userDaoImpl.getUserLabelScore(vo);
				if (exist != null) {
					vo.setId(exist.getId());
					userDaoImpl.updateUserLabelScore(vo);
				} else {
					userDaoImpl.addUserLabelScore(vo);
				}
				existId.add(vo.getId());
			}
		}
		userDaoImpl.deleteUserLabelScore(existId);
	}

	@Override
	public Paged<Map<String, Object>> getAnalystList(Map<String, Object> params) {
		// 参数
		int userType = MapUtils.getIntValue(params, "userType", 0); //用户类型： 0 全部 1 分析师  2 销售 3 其他
		long uid = MapUtils.getLongValue(params, "uid", 0);
		if (userType > 0) params.put("userType", userType); // 用户类型
		if (userType == 1) params.put("checkStatus", 3); // 分析师类型:默认已审核
		Paged<Map<String, Object>> paged = new Paged<Map<String, Object>>();

		List<GroupPowerVo> powerList = groupServiceImpl.getResourceGroupPower(uid, 0, MeixConstants.USER_YH, 0, MeixConstants.SHARE_LIST);
		if (powerList == null || powerList.isEmpty() || powerList.get(0).getPower() == 0) { //无权限
			paged.setCount(0);
			paged.setList(Collections.emptyList());
			return paged;
		}
		int count = insUserMapper.countAnalystList(params);
		if (count == 0) {
			paged.setCount(0);
			paged.setList(Collections.emptyList());
			return paged;
		}
		List<InsUser> list = insUserMapper.getAnalystList(params);
		List<Map<String, Object>> result = new ArrayList<>();
		for (InsUser item : list) {
			Map<String, Object> map = new HashMap<>();
			map.put("user", item.getUser());
			map.put("contactName", item.getUserName());
			map.put("orgCode", item.getCompanyCode());
			map.put("contactImageUrl", OssResourceUtil.getHeadImage(item.getHeadImageUrl()));
			map.put("direction", item.getDirection());
			map.put("followFlag", 0);
			map.put("position", item.getPosition());
			map.put("mobile", item.getMobile());
			map.put("role", item.getRole());
			if (item.getRole() != null) {
				map.put("roleText", dictTagService.getText(MeixConstants.ROLE, item.getRole().toString()));
			}
			// 关联ia_customer 表
			UserInfo userInfo = userDaoImpl.getUserInfoByUuid(item.getUser());
			if (userInfo == null) {
				userInfo = addInsUser(item.getUserName(), item.getMobile(), item.getEmail(), MapUtils.getString(map, "position"), item.getUser());
			}
			
			if (StringUtils.isEmpty(item.getHeadImageUrl())) {
				map.put("contactImageUrl", StringUtils.isNotBlank(
						userInfo.getHeadImageUrl()) ? OssResourceUtil.getHeadImage(userInfo.getHeadImageUrl())
								: OssResourceUtil.getHeadImage(userInfo.getWechatHeadUrl()));
			}
			// 登陆用户id
			map.put("contactId", userInfo.getId());
			// 关注标志
			boolean follow = userDaoImpl.isFocusedPerson(MapUtils.getLongValue(params, "uid", 0), userInfo.getId());
			map.put("followFlag", follow ? 1 : 0);

			result.add(map);
		}
		paged.setCount(count);
		paged.setList(result);
		return paged;
	}

	@Override
	public void saveLoginInfo(long uid) {
		if (userDaoImpl.updateLoginInfo(uid) == 0) {
			userDaoImpl.insertLoginInfo(uid);
		}
	}

	@Override
	public void updateUserInfo(UserInfo userInfo) {
		userDaoImpl.updateUser(userInfo);
	}

	@Override
	public List<Map<String, Object>> getCustomerList(int accountType, int authStatus, String condition, int currentPage, int showNum, String sortField, int sortRule, long groupId, int isExcept) {
		return getCustomerList(accountType, authStatus, condition, currentPage, showNum, null, sortField, sortRule, groupId, isExcept);
	}

	//sync to meix
	@Override
	public List<Map<String, Object>> getCustomerList(int accountType, int authStatus, String condition, int currentPage, int showNum, String updatedAt, String sortField, int sortRule, long groupId, int isExcept) {
		List<Map<String, Object>> list = userDaoImpl.getCustomerList(accountType, authStatus, condition, currentPage, showNum, updatedAt, sortField, sortRule, groupId, isExcept);
		for (Map<String, Object> item : list) {
			String userName = MapUtils.getString(item, "userName");
			String nickName = MapUtils.getString(item, "nickName");
			if (StringUtils.isBlank(userName) || MeixConstants.DEFAULT_NAME.equals(userName)) {
				if (StringUtils.isNotBlank(nickName)) {
					item.put("userName", nickName);
				}
			}
		}
		return list;
	}

	@Override
	public int getCustomerCount(int accountType, int authStatus, String condition, long groupId, int isExcept) {
		return userDaoImpl.getCustomerCount(accountType, authStatus, condition, groupId, isExcept);
	}

	@Override
	public List<UserClueStat> getAuthedUserClueList(ClueSearchVo searchVo) {
		return userDaoImpl.getAuthedUserClueList(searchVo);
	}

	@Override
	public int getAuthedUserClueCount(ClueSearchVo searchVo) {
		return userDaoImpl.getAuthedUserClueCount(searchVo);
	}

	@Override
	public List<CommentVo> getCommentList(ComplianceCommentSearchVo searchVo) {
		return userDaoImpl.getCommentList(searchVo);
	}

	@Override
	public int getCommentCount(ComplianceCommentSearchVo searchVo) {
		return userDaoImpl.getCommentCount(searchVo);
	}

	@Override
	public void examineComment(long commentId, int optType, String uuid) {
		userDaoImpl.examineComment(commentId, optType, uuid);
	}

	@Override
	public void updateWechatInfo(UserInfo userInfo) {
		userDaoImpl.updateWechatInfo(userInfo);
	}

	@Override
	public List<UserInfo> getPushWechatMsgUserList(long companyCode) {
		return userDaoImpl.getPushWechatMsgUserList(companyCode);
	}

	@Override
	public void userLabelPush(long companyCode) {
		List<Map<String, Object>> list = userDaoImpl.getUserLabelPushInfo();
		long uid, dataId;
		int type, dataType, activityType;
		String title, startTime;
		for (Map<String, Object> obj : list) {
			uid = MapUtils.getLongValue(obj, "uid");
			dataId = MapUtils.getLongValue(obj, "dataId");
			dataType = MapUtils.getIntValue(obj, "dataType");
			activityType = MapUtils.getIntValue(obj, "activityType");
			title = MapUtils.getString(obj, "title");
			startTime = MapUtils.getString(obj, "startTime");
			if (uid == 0 || dataId == 0 || dataType == 0) {
				continue;
			}
			if (dataType == 3) {
				type = 3;
			} else {
				type = activityType;
			}
			messagePushService.instituteMessagePush(String.valueOf(companyCode), dataType, dataId, InstituteMessagePushServiceImpl.USER_LABEL_PUSH,
				null, null, title, type, null, null, companyCode, null, null, uid, startTime);
		}
	}

	@Override
	public void saveUserLabelPushRecord(long uid, long dataId, int dataType) {
		userDaoImpl.saveUserLabelPushRecord(uid, dataId, dataType);
	}

	@Override
	public boolean isFromMyFocusedPerson(long uid, int dataType, long dataId) {
		return userDaoImpl.isFromMyFocusedPerson(uid, dataType, dataId);
	}

	@Override
	public boolean isFromMyFocusedStock(long uid, int dataType, long dataId) {
		return userDaoImpl.isFromMyFocusedStock(uid, dataType, dataId);
	}

	@Override
	public void updateThirdIdByMobile(String thirdId, String mobile) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("thirdId", thirdId);
		params.put("mobile", mobile);
		userDaoImpl.updateThirdIdByMobile(params);
	}

	@Override
	public void updateUserPrivacyPolicyStatus(long uid, int status, String user) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("status", status);
		userDaoImpl.updateUserPrivacyPolicyStatus(params);
	}

	@Override
	public List<UserRecordStateResp> getUserRecordStateList(Map<String, Object> params) {
		List<UserRecordStateResp> list = userDaoImpl.getUserRecordStateList(params);
		if (list == null || list.size() == 0) {
			return Collections.emptyList();
		}
		for (UserRecordStateResp item : list) {
			switch (item.getDataType()) {
				case MeixConstants.USER_YB:
					ReportInfo report = reportMapper.selectByPrimaryKey(item.getDataId());
					if (report != null) {
						item.setTitle(report.getTitle());
						item.setSubType(report.getType());
					}
					break;
				case MeixConstants.USER_HD:
					InsActivity activity = activityService.getBaseActivityById(item.getDataId());
					if (activity != null) {
						item.setTitle(activity.getTitle());
						item.setSubType(activity.getActivityType());
					}
					break;
				case MeixConstants.USER_CH:
					MorningMeetingVo morningMeetingVo = meetingServiceImpl.getMeetingDetail(item.getDataId());
					if (morningMeetingVo != null) {
						item.setTitle(morningMeetingVo.getTitle());
					}
					break;
				default:
					break;
			}
		}
		return list;
	}

	@Override
	public List<UserContentReadStat> getUserContentReadStat(Map<String, Object> params) {
		List<UserContentReadStat> list = userDaoImpl.getUserContentReadStat(params);
		if (list == null || list.size() == 0) {
			return Collections.emptyList();
		}
		String startTime = MapUtils.getString(params, "startTime", null);
		String endTime = MapUtils.getString(params, "endTime", null);
		for (UserContentReadStat item : list) {
			long readerNum = userDaoImpl.getDataReaderNum(item.getDataType(), item.getDataId(), startTime, endTime);
			item.setReaderNum(readerNum);
		}
		return list;
	}

	@Override
	public long getUserContentCount(Map<String, Object> params) {
		long contentNum = userDaoImpl.getUserContentCount(params);
		return contentNum;
	}

	@Override
	public long getDataReadNum(long uid, String startTime, String endTime) {
		return userDaoImpl.getDataReadNum(uid, startTime, endTime);
	}

	@Override
	public long getInsUidByCustomerId(long uid) {
		UserInfo userInfo = insUserMapper.selectCustomerInfoById(uid);
		if (userInfo == null || StringUtil.isBlank(userInfo.getInsUuid())) {
			return 0;
		}
		InsUser insUser = insUserMapper.selectByUuid(userInfo.getInsUuid());
		if (insUser == null) {
			return 0;
		}
		return insUser.getId();
	}

	@Override
	public UserInfo getUserInfoByUserName(String salerName) {
		return userDaoImpl.getUserInfoByUserName(salerName);
	}

	@Override
	public UserInfo getWhiteUserInfo(String mobile) {
		return userDaoImpl.getUserInfo(0, null, mobile, 1);
	}

	@Override
	public void deleteWhiteUser(String uuid, long uid, int accountType) {
		userDaoImpl.deleteWhiteUser(uuid, uid, accountType);
	}

	@Override
	public void updateUserIdentify(long uid, int identify) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", uid);
		params.put("identify", identify);
		userDaoImpl.updateUserIdentify(params);
	}

	@Override
	public InsUser getUserInfoByOaName(String loginName) {
		Example example = new Example(InsUser.class);
		example.createCriteria().andEqualTo("oaName", loginName);
		try {
			return insUserMapper.selectOneByExample(example);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void deleteUser(long uid, String uuid) {
		Map<String, Object> params = new HashMap<>();
		params.put("operatorId", uuid);
		params.put("id", uid);
		userDaoImpl.deleteUser(params);
	}

	@Override
	public void saveUserOperatorLog(Map<String, Object> params) {
		userDaoImpl.saveUserOperatorLog(params);
	}

}
