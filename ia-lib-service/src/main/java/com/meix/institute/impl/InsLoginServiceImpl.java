package com.meix.institute.impl;

import java.util.Date;

import org.apache.commons.codec.binary.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.api.IInsLoginService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsSecToken;
import com.meix.institute.entity.InsUser;
import com.meix.institute.mapper.InsSecTokenMapper;
import com.meix.institute.mapper.InsUserMapper;
import com.meix.institute.pojo.InsSecTokenS;
import com.meix.institute.pojo.InsUserInfo;
import com.meix.institute.pojo.InsUserS;
import com.meix.institute.pojo.InsUserSearch;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.MD5Util;
import com.meix.institute.util.RandomUtil;
import com.meix.institute.util.SecurityUtil;
import com.meix.institute.vo.SecToken;

import tk.mybatis.mapper.entity.Example;

@Service
public class InsLoginServiceImpl implements IInsLoginService {
    
	@Autowired
	private InsUserMapper insUserMapper;
	@Autowired
	private InsSecTokenMapper insSecTokenMapper;
	
	//private static final Logger log = LoggerFactory.getLogger(LoginServiceImpl.class);
	private static final int EXPIRES_DAY = 15;

	@Override
	public InsUserInfo authByPassword(InsUserSearch search) {
	    
	    InsUserS insUserS = new InsUserS();
	    insUserS.setMobileOrEmail(search.getUserName());
	    insUserS.setId(search.getId());
	    insUserS.setUserState(search.getUserState());
	    InsUser record = insUserMapper.getUserByMobileOrEmail(insUserS);
	    
	    if (record == null) {
	        return null;
	    }
	    
	    String salt = record.getSalt();
	    String password = MD5Util.md5(salt + search.getPassword());
	    if (!StringUtils.equals(password, record.getPassword())) {
	        return null;
	    }
	    
	    InsUserInfo userInfo = new InsUserInfo();
	    BeanUtils.copyProperties(record, userInfo);
		return userInfo;
		
	}
	
	@Override
    public void saveToken(String token, String user, Integer clientType) {
		saveToken(token, user, clientType, null);
    }

    @Override
    public void saveToken(String token, String user, Integer clientType, String ltpaToken) {
        // 重新生成
        Date _now = new Date();
        InsSecToken record = new InsSecToken();
        record.setUser(user);
        record.setAccessToken(token);
        record.setClientType(clientType == null ? 0 : Integer.valueOf(clientType));
        record.setCreatedAt(_now);
        record.setCreatedBy(user);
        record.setUpdatedAt(_now);
        record.setUpdatedBy(user);
        record.setExpiresAt(DateUtil.getDate(EXPIRES_DAY));
        record.setIsDeleted(MeixConstants.DELETE_NO);
        record.setLtpaToken(ltpaToken);
        insSecTokenMapper.insert(record);
    }

    @Override
    public void modifyPwd(long id, String newPwd, String user) {
        String salt = RandomUtil.generateRandom(5);
        InsUser record = new InsUser();
        record.setId(id);
        record .setSalt(salt);
        record.setPassword(MD5Util.md5(salt + newPwd));
        record.setUpdatedAt(new Date());
        record.setUpdatedBy(user);
        insUserMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public void logout(String user, Integer clientType) {
        InsSecTokenS record = new InsSecTokenS();
        record.setUser(user);
        record.setClientType(clientType);
        insSecTokenMapper.logout(record);
    }

    @Override
    public SecToken getTokenByUser(String user, Integer clientType) {
        return insSecTokenMapper.getTokenByUser(user, clientType);
    }

	@Override
	public String createToken(long uid, String user, long companyCode, int userState, int clientType,
			String ltpaToken) {
		//过期以前的token
		this.logout(user, clientType);
		String token = SecurityUtil.generateLoginToken(uid, user, companyCode, userState, clientType);
        // 保存token
        this.saveToken(token, user, clientType, ltpaToken);
		return token;
	}

	@Override
	public String getTokenByLtpaToken(String ltpaToken, String clientType) {
		if (com.meix.institute.util.StringUtils.isBlank(ltpaToken)) {
			return null;
		}
		return insSecTokenMapper.getTokenByLtpaToken(ltpaToken, clientType);
	}

	@Override
	public void updateToken(String token, String ltpaToken) {
		InsSecToken record = new InsSecToken();
		record.setUpdatedAt(new Date());
		record.setLtpaToken(ltpaToken);
		Example example = new Example(InsSecToken.class);
		example.createCriteria().andEqualTo("accessToken", token);
		insSecTokenMapper.updateByExampleSelective(record, example);
	}

}
