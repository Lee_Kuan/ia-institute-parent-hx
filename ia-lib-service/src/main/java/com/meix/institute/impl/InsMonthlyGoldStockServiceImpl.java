package com.meix.institute.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.BaseService;
import com.meix.institute.api.IInsMonthlyGoldStockService;
import com.meix.institute.entity.InsCompanyMonthlyGoldStock;
import com.meix.institute.entity.InsSelfStock;
import com.meix.institute.entity.InsUser;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.mapper.InsCompanyMonthlyGoldStockMapper;
import com.meix.institute.mapper.InsSelfStockMapper;
import com.meix.institute.mapper.InsUserMapper;
import com.meix.institute.mapper.MonthlyGoldStockMapper;
import com.meix.institute.mapper.SecuMainMapper;
import com.meix.institute.search.StockYieldSearchVo;
import com.meix.institute.vo.stock.GoldStockSelfUserInfo;
import com.meix.institute.vo.stock.MonthlyGoldStockListVo;
import com.meix.institute.vo.stock.MonthlyGoldStockVo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class InsMonthlyGoldStockServiceImpl extends BaseService implements IInsMonthlyGoldStockService{

	protected final Logger log = LoggerFactory.getLogger(InsWhiteListServiceImpl.class);
	@Autowired
	private InsCompanyMonthlyGoldStockMapper goldStockMapper;
	@Autowired
	private SecuMainMapper secuMainMapper;
	@Autowired
	private MonthlyGoldStockMapper myStockMapper;
	@Autowired
	private InsSelfStockMapper selfStockMapper;
	@Autowired
	private InsUserMapper insUserMapper;
	
	private DateFormat monthFormat = new SimpleDateFormat("yyyy-MM");
	
	@Override
	public long getStockInnerCode(String stockNo, String month, long companyCode) {
		Example example = new Example(SecuMain.class);
		if(stockNo.contains(".")) {
			String[] stock = stockNo.split("\\.");
			example.createCriteria().andEqualTo("secuCode", stock[0]).andEqualTo("suffix", "." + stock[1]);
		} else {
			example.createCriteria().orEqualTo("secuCode", stockNo).orCondition("replace(SECU_ABBR, ' ', '')=", stockNo);//.orEqualTo("secuAbbr", stockNo)
		}
		SecuMain secuMain = secuMainMapper.selectOneByExample(example);
		long innerCode = secuMain == null ? 0 : secuMain.getInnerCode();
		if (innerCode > 0) {
			example.clear();
			example = new Example(InsCompanyMonthlyGoldStock.class);
			example.createCriteria().andEqualTo("companyCode", companyCode).andEqualTo("innerCode", innerCode).andEqualTo("month", month);
			if (goldStockMapper.selectCountByExample(example) > 0) {
				//重复
				return -1;
			}
		}
		return innerCode;
	}

	@Override
	public void addGoldStock(List<InsCompanyMonthlyGoldStock> stockList) {
		goldStockMapper.insertList(stockList);
	}

	@Override
	public Map<String, Object> getGoldStockList(int showNum, int currentPage, long companyCode) {
		List<MonthlyGoldStockListVo> list = myStockMapper.getMonthlyGoldStockList(showNum, currentPage, companyCode);
		Calendar cal = Calendar.getInstance();
		int day = cal.get(Calendar.DAY_OF_MONTH);
		String nowMonth = monthFormat.format(cal.getTime());
		cal.add(Calendar.MONTH, 1);
		String month = monthFormat.format(cal.getTime());
		boolean newMonth = true;
		boolean haveNowMonth = false;
		int addCount = 0;
		for (MonthlyGoldStockListVo info : list) {
			if (month.equals(info.getMonth()) && day >= 20) {
				//允许编辑
				info.setStatus(1);
				newMonth = false;
			}
			if (nowMonth.equals(info.getMonth())) {//本月可编辑
				info.setStatus(1);
				haveNowMonth = true;
			}
		}
		if (newMonth && day >= 20) {
			//增加新月份信息
//			int m = cal.get(Calendar.MONTH);
//			cal.set(Calendar.MONTH, m + 1);
//			month = monthFormat.format(cal.getTime());
			MonthlyGoldStockListVo info = new MonthlyGoldStockListVo();
			info.setCount(0);
			info.setMonth(month);
			info.setStatus(1);
			list.add(0, info);
			//list.add(info);
			addCount ++;
		} else {
			newMonth = false;
		}
		if (!haveNowMonth) {
			MonthlyGoldStockListVo info = new MonthlyGoldStockListVo();
			info.setCount(0);
			info.setMonth(nowMonth);
			info.setStatus(1);
			list.add(0, info);
			addCount ++;
		}
		Map<String, Object> res = new HashMap<>();
		res.put("list", list);
		res.put("newMonth", addCount);
		return res;
	}
	
	@Override
	public int getGoldStockCount(long companyCode) {
		return myStockMapper.getMonthlyGoldStockCount(companyCode);
	}
	
	@Override
	public void deleteGoldStock(String month, long innerCode, long companyCode) {
		Example example = new Example(InsCompanyMonthlyGoldStock.class);
		example.createCriteria().andEqualTo("companyCode", companyCode).andEqualTo("month", month).andEqualTo("innerCode", innerCode);
		goldStockMapper.deleteByExample(example);
	}

	@Override
	public List<MonthlyGoldStockVo> getGoldStockMonthList(int showNum, int currentPage, long companyCode,
	                                                       String month, int homeShowFlag) {
		Example example = new Example(InsCompanyMonthlyGoldStock.class);
		Criteria criteria = example.createCriteria();
		if (companyCode > 0) {
			criteria.andEqualTo("companyCode", companyCode);
		}
		criteria.andEqualTo("month", month);
		if (0 == homeShowFlag || 1 == homeShowFlag) {
		    criteria.andEqualTo("homeShowFlag", homeShowFlag);
		}
		List<InsCompanyMonthlyGoldStock> list = null;
		if (showNum >= 0) {
			RowBounds rowBounds = new RowBounds(showNum * currentPage, showNum);
			list = goldStockMapper.selectByExampleAndRowBounds(example, rowBounds);
		} else {
			list = goldStockMapper.selectByExample(example);
		}
		if (list == null) {
			return new ArrayList<>();
		}
		List<MonthlyGoldStockVo> resList = new ArrayList<>();
		Example selfStockEx = new Example(InsSelfStock.class);
		Example secuMainEx = new Example(SecuMain.class);
		for (InsCompanyMonthlyGoldStock info : list) {
			
			Example userExample = new Example(InsUser.class);
			userExample.createCriteria().andEqualTo("user", info.getRecPerson());
			InsUser user = insUserMapper.selectOneByExample(userExample);
			selfStockEx.clear();
			selfStockEx.createCriteria().andEqualTo("innerCode", info.getInnerCode()).andEqualTo("isDeleted", 0);
			secuMainEx.clear();
			secuMainEx.createCriteria().andEqualTo("innerCode", info.getInnerCode());
			SecuMain secuMain = secuMainMapper.selectOneByExample(secuMainEx);
			int count = selfStockMapper.selectCountByExample(selfStockEx);
			MonthlyGoldStockVo res = new MonthlyGoldStockVo(
					info.getId(), 
					info.getCompanyCode(),
					info.getRecPerson(),
					user == null ? info.getRecPerson() : user.getUserName(),
					info.getRecDesc(),
					info.getInnerCode(),
					secuMain.getSecuAbbr(),
					secuMain.getSecuCode() + secuMain.getSuffix(),
					info.getMonth(),
					new BigDecimal(0),
					new BigDecimal(0),
					count,
					info.getCreatedAt(),
					info.getCreatedBy(),
					info.getUpdatedAt(),
					info.getUpdatedBy()
					);
			res.setHomeShowFlag(info.getHomeShowFlag());
			float addPrice = info.getAddPrice().floatValue();
			float addMonthPrice = info.getAddMonthPrice().floatValue();
			float nowPrice = info.getNowPrice().floatValue();
			if (addPrice > 0) {
				if (addMonthPrice > 0) {
					res.setMonthRate(new BigDecimal((addMonthPrice - addPrice) / addPrice));
				}
				if (nowPrice > 0) {
					res.setNowRate(new BigDecimal((nowPrice - addPrice) / addPrice));
				}
			}
			resList.add(res);
		}
		return resList;
	}

	@Override
	public int getGoldStockMonthCount(long companyCode, String month, int homeShowFlag) {
		Example example = new Example(InsCompanyMonthlyGoldStock.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("companyCode", companyCode).andEqualTo("month", month);
		if (0 == homeShowFlag || 1 == homeShowFlag) {
            criteria.andEqualTo("homeShowFlag", homeShowFlag);
        }
		int count = goldStockMapper.selectCountByExample(example);
		return count;
	}
	
	@Override
	public List<String> saveGoldStockMonth(String uuid, long companyCode, String month, JSONArray datas) {
		return saveGoldStockMonth(uuid, companyCode, month, datas, null);
	}
	
	@Override
	public List<String> saveGoldStockMonth(String uuid, long companyCode, String month, JSONArray datas,
			XSSFSheet sheet) {
		List<String> errInfo = new ArrayList<>();
		String info = "备注";
		Map<String, Integer> existStock = new HashMap<>();
		List<InsCompanyMonthlyGoldStock> stockList = new ArrayList<>();
		for (int i = 0; i < datas.size(); i++) {
			try {
				if (sheet != null) {
					sheet.getRow(i).createCell(4).setCellValue(info);
				}
				JSONObject obj = (JSONObject) datas.get(i);
				String stockNo = obj.getString("stockNo");
				if (StringUtils.isBlank(stockNo) || "null".equals(stockNo)) {
					errInfo.add("第" + (i + 1) + "条股票未匹配！");
					info = "股票信息为空";
					continue;
				}
				long innerCode = this.getStockInnerCode(stockNo, month, companyCode);
				if (innerCode == 0) {
					errInfo.add("第" + (i + 1) + "条股票未匹配！");
					info = "股票未匹配";
					continue;
				} else if (innerCode == -1 || existStock.containsKey(innerCode + "")) {    //重复
					errInfo.add("第" + (i + 1) + "条股票重复！");
					info = "重复";
					continue;
				}
				String recDesc = obj.getString("recDesc");
				if (recDesc == null || "".equals(recDesc) || "null".equals(recDesc)) {
					recDesc = "";
				}
				String user = MapUtils.getString(obj, "user", null);
				if (user == null) {
					user = MapUtils.getString(obj, "recPerson", "");
				}
				InsCompanyMonthlyGoldStock stock = new InsCompanyMonthlyGoldStock();
				stock.setRecPerson(user);
				stock.setCompanyCode(companyCode);
				stock.setInnerCode((int) innerCode);
				stock.setRecDesc(recDesc);
				stock.setMonth(month);
				
				stock.setAddPrice(new BigDecimal(0));
				stock.setAddMonthPrice(new BigDecimal(0));
				stock.setNowPrice(new BigDecimal(0));
				
				stock.setCreatedAt(new Date());
				stock.setCreatedBy(uuid);
				stock.setUpdatedAt(new Date());
				stock.setUpdatedBy(uuid);
				stock.setHomeShowFlag(1);
				stockList.add(stock);
				existStock.put(innerCode + "", 0);
				info = "成功";
			} catch (Exception e) {
				info = "未知错误";
				e.printStackTrace();
			}
		}
		if (sheet != null) {
			sheet.getRow(datas.size()).createCell(4).setCellValue(info);
		}
		if (stockList.size() > 0) {
			this.addGoldStock(stockList);
		}
		return errInfo;
	}

	@Override
	public List<GoldStockSelfUserInfo> getSelfStockUserList(int showNum, int currentPage, long innerCode) {
		return myStockMapper.getSelfStockUserList(showNum, currentPage, innerCode);
	}

	@Override
	public int getSelfStockUserCount(long innerCode) {
		Example example = new Example(InsSelfStock.class);
		example.createCriteria().andEqualTo("innerCode", innerCode).andEqualTo("isDeleted", 0);
		return selfStockMapper.selectCountByExample(example);
	}

	@Override
	public void updateGoldStockPrice(InsCompanyMonthlyGoldStock goldStock) {
		goldStockMapper.updateByPrimaryKeySelective(goldStock);
	}

    @Override
    public void goldPullOffShelves(Long id, Integer homeShowFlag, String user) {
        InsCompanyMonthlyGoldStock record = new InsCompanyMonthlyGoldStock();
        record.setId(id);
        record.setHomeShowFlag(homeShowFlag);
        record.setUpdatedAt(new Date());
        record.setUpdatedBy(user);
        goldStockMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Map<Integer, Map<String, BigDecimal>> getStockPriceMap(List<StockYieldSearchVo> searchList) {
    	return getStockPrice(searchList);
    }
}
