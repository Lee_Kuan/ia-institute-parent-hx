package com.meix.institute.impl;

import com.meix.institute.annotation.HandleDataType;
import com.meix.institute.api.DataHandler;
import com.meix.institute.api.IUserDao;
import com.meix.institute.api.IUserService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.constant.SyncConstants;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.vo.third.HandleResult;
import com.meix.institute.vo.third.SyncUserStateRecord;
import com.meix.institute.vo.user.UserInfo;
import com.meix.institute.vo.user.UserPersonasVo;
import com.meix.institute.vo.user.UserRecordStateVo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zenghao on 2020/4/8.
 */
@Service
@HandleDataType(MeixConstants.USER_YH)
public class UserDataHandler implements DataHandler {
	private static Logger log = LoggerFactory.getLogger(UserDataHandler.class);

	@Autowired
	private IUserDao userDao;
	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IUserService userService;

	@Override
	public HandleResult handleData(String action, JSONObject jsonObject) {
		HandleResult result = null;
		switch (action) {
			case SyncConstants.ACTION_GET_PERSONA_INFO_LIST:
				result = handlePersonaInfoListReq(jsonObject);
				break;
			case SyncConstants.ACTION_SAVE_USER_STATE_LIST:
				result = handleUserStateList(jsonObject);
				break;
		}
		return result;
	}

	private HandleResult handlePersonaInfoListReq(JSONObject jsonObject) {
		HandleResult result = new HandleResult();
		try {
			int messageCode = MapUtils.getIntValue(jsonObject, "messageCode", 0);
			if (messageCode != MeixConstantCode.M_1008) {
				result.setMessage("同步画像失败");
				result.setMessageCode(MeixConstantCode.M_1009);
				return result;
			}

			JSONObject object = jsonObject.getJSONObject("object");
			String uidListJson = MapUtils.getString(object, "uidListJson", null);

			JSONArray dataArray = jsonObject.getJSONArray("data");
			if (dataArray == null || dataArray.size() == 0) {
				log.info("用户画像列表为空:{}", jsonObject);
				result.setMessage("用户画像列表为空");
				result.setMessageCode(MeixConstantCode.M_1009);
				return result;
			}

			List<UserPersonasVo> dataList = GsonUtil.json2List(dataArray.toString(), UserPersonasVo.class);

			if (dataList == null || dataList.size() == 0) {
				result.setMessage("用户画像列表为空");
				result.setMessageCode(MeixConstantCode.M_1009);
				return result;
			}
			long startTime = System.currentTimeMillis();
			List<Long> uidList = GsonUtil.json2List(uidListJson, Long.class);
			//先把这批用户的画像数据删除
			userDao.deleteUserPersonas(uidList);
			for (UserPersonasVo label : dataList) {
				UserInfo userInfo = userDao.getUserInfo(0, null, label.getMobile());
				if (userInfo == null) {
					continue;
				}
				label.setCreatedBy(commonServerProperties.getDefaultUser());
				label.setUpdatedBy(commonServerProperties.getDefaultUser());
				label.setUid(userInfo.getId());
				if (label.getLabelType() == 0) {
					UserInfo author = userDao.getUserInfo(0, null, label.getAuthorMobile());
					if (author == null) {
						continue;
					}
					label.setLabelCode(String.valueOf(author.getId()));
				}

				if (userDao.getUserPersonas(label.getLabelId()) == null) {
					userDao.addUserPersonas(label);
				} else {
					userDao.updateUserPersonas(label);
				}
			}

			log.info("同步用户画像列表{}条,耗时{}ms", dataArray.size(), (System.currentTimeMillis() - startTime));
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("处理画像数据列表失败", e);
			result.setMessage("处理画像数据列表失败");
			result.setMessageCode(MeixConstantCode.M_1009);
		}
		return result;
	}

	private HandleResult handleUserStateList(JSONObject jsonObject) {
		HandleResult result = new HandleResult();
		try {
			JSONArray dataArray = jsonObject.getJSONArray("data");
			if (dataArray == null || dataArray.size() == 0) {
				log.info("用户行为列表为空:{}", jsonObject);
				result.setMessage("用户行为列表为空");
				result.setMessageCode(MeixConstantCode.M_1009);
				return result;
			}

			List<SyncUserStateRecord> dataList = GsonUtil.json2List(dataArray.toString(), SyncUserStateRecord.class);

			if (dataList == null || dataList.size() == 0) {
				result.setMessage("用户行为列表为空");
				result.setMessageCode(MeixConstantCode.M_1009);
				return result;
			}
			long startTime = System.currentTimeMillis();
			List<UserRecordStateVo> recordStateVoList = new ArrayList<>();
			for (SyncUserStateRecord item : dataList) {
				if (item.getDataState() != 6) {
					//时长
					continue;
				}
				UserInfo userInfo = userDao.getUserInfo(0, null, item.getMobile());
				//非内部和白名单不保存
				if (userInfo == null || (userInfo.getAccountType() != MeixConstants.ACCOUNT_INNER &&
					userInfo.getAccountType() != MeixConstants.ACCOUNT_WL)) {
					continue;
				}
				UserRecordStateVo userRecordStateVo = new UserRecordStateVo();
				userRecordStateVo.setUid(userInfo.getId());
				userRecordStateVo.setDataId(item.getDataId());
				userRecordStateVo.setDataType(item.getDataType());
				userRecordStateVo.setDataState(MeixConstants.RECORD_DURATION);
				userRecordStateVo.setDuration(item.getDuration());
				userRecordStateVo.setDeviceType(item.getDeviceType());
				recordStateVoList.add(userRecordStateVo);
			}
			userService.saveUserRecordStateList(recordStateVoList);
			log.info("同步用户行为列表{}条,耗时{}ms", dataArray.size(), (System.currentTimeMillis() - startTime));
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("处理用户行为列表失败", e);
			result.setMessage("处理用户行为列表失败");
			result.setMessageCode(MeixConstantCode.M_1009);
		}
		return result;
	}
}
