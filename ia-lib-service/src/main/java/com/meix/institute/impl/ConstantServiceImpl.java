package com.meix.institute.impl;

import com.meix.institute.api.IConstDao;
import com.meix.institute.api.IConstantService;
import com.meix.institute.vo.IaConstantVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zenghao on 2019/9/21.
 */
@Service
public class ConstantServiceImpl implements IConstantService {

	@Autowired
	private IConstDao constDao;

	@Override
	public void saveConstantList(List<IaConstantVo> list) {
		constDao.insertConstantList(list);
	}

	@Override
	public int removeConstantRange(long startId, long endId) {
		return constDao.removeConstantRange(startId, endId);
	}
}
