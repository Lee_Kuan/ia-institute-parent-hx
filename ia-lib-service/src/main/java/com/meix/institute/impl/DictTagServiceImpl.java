package com.meix.institute.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.api.ICodeService;
import com.meix.institute.api.IDictTagService;
import com.meix.institute.core.Item;

@Service
public class DictTagServiceImpl implements IDictTagService{
    
    private final ThreadLocal<Map<String, Item>> tlItem = new ThreadLocal<Map<String, Item>>();
    private final ThreadLocal<Map<String, List<Item>>> tlList = new ThreadLocal<Map<String, List<Item>>>();
    
    @Autowired
    private ICodeService codeService;
    
    @Override
    public void reset() {
        //单个元素
        Map<String, Item> mapItem = tlItem.get();
        if (mapItem == null) {
            mapItem = new HashMap<String, Item>();
            tlItem.set(mapItem);
        }
        mapItem.clear();

        //列表元素
        Map<String, List<Item>> mapList = tlList.get();
        if (mapList == null) {
            mapList = new HashMap<String, List<Item>>();
            tlList.set(mapList);
        }
        mapList.clear();
    }
    
    @Override
    public List<Item> findItem(String name) {
        //查询缓存
        Map<String, List<Item>> map = tlList.get();
        List<Item> items = map.get(name);

        //获取值并放入缓存
        if (items == null) {
            items = codeService.findItem(name);
            map.put(name, items);
        }

        return items;
    }

    @Override
    public String getText(String name, String code) {
        //代码为空
        if (StringUtils.isBlank(code)) {
            return "";
        }

        //获取值
        Item item = getItem(name, code);
        String text = "";
        if (item != null) {
            text = item.getText();
        }
        return text;
    }

    @Override
    public Item getItem(String name, String code) {
        //代码为空
        if (StringUtils.isBlank(code)) {
            return null;
        }

        //查询缓存
        Map<String, Item> map = tlItem.get();
        Item item = null;
        String key = String.format("%s/%s", name, code);
        if (map != null && map.containsKey(key)) {
	        item = map.get(key);
        }
        //获取值并放入缓存
        if (item == null) {
            item = codeService.getItem(name, code);
            if (map == null) {
            	map = new HashMap<>();
            }
            map.put(key, item);
        }

        return item;
    }

}
