package com.meix.institute.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.annotation.HandleDataType;
import com.meix.institute.api.DataHandler;
import com.meix.institute.api.IActivityService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IUserService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.constant.SyncConstants;
import com.meix.institute.entity.InsActivity;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.vo.activity.gensee.ActivityGenseeRecord;
import com.meix.institute.vo.activity.gensee.ActivityThirdJoinInfo;
import com.meix.institute.vo.gensee.GenseeVideo;
import com.meix.institute.vo.third.HandleResult;
import com.meix.institute.vo.user.UserInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2020/4/8.
 */
@Service
@HandleDataType(MeixConstants.USER_HD)
public class ActivityDataHandler implements DataHandler {
	private static Logger log = LoggerFactory.getLogger(ActivityDataHandler.class);

	@Autowired
	private IActivityService activityServiceImpl;
	@Autowired
	private IUserService userService;
	@Autowired
	private IInstituteMessagePushService pushService;

//	@Value("${ins.defaultUser}")
//	private String defaultUser;

	@Override
	public HandleResult handleData(String action, JSONObject jsonObject) throws Exception {
		log.info("同步参数：{}{}", action, GsonUtil.obj2Json(jsonObject));
		HandleResult result = new HandleResult();
//		Activity activity = GsonUtil.json2Obj(jsonObject.toString(), Activity.class);
//		List<Activity> activityList = GsonUtil.json2List(jsonObject.toString(), Activity.class);
		switch (action) {
			case SyncConstants.ACTION_SAVE_ACTIVITY:
				saveActivity(jsonObject);
				break;
			case SyncConstants.ACTION_UPDATE_ACTIVITY_STATUS:
				updateActivityStatus(jsonObject);
				break;
			case SyncConstants.ACTION_GET_GENSEE_RECORD_LIST:
				result = syncGenseeRecord(jsonObject);
				break;
			case SyncConstants.ACTION_GET_TELE_JOIN_RECORD_LIST:
				result = syncTeleJoinRecord(jsonObject);
				break;
			case SyncConstants.ACTION_UPLOAD_ACTIVITY_FILE:
				updateActivityFile(jsonObject);
				break;
			case SyncConstants.ACTION_SAVE_GENSEE_VOD_LIST:
				saveGenseeVodList(jsonObject);
				break;
			case SyncConstants.ACTION_DELETE_GENSEE_VOD:
				deleteGenseeVod(jsonObject);
				break;
			default:
				break;
		}
		if (result == null) {
			result = new HandleResult();
		}
//		result.setKeyword("1");
		return result;
	}

	private void deleteGenseeVod(JSONObject jsonObject) throws Exception {
		long activityId = MapUtils.getLongValue(jsonObject, "id", 0);
		if (activityId == 0) {
			throw new Exception("activityId为空");
		}
		activityServiceImpl.deleteGenseeVod(activityId);
	}

	private void saveGenseeVodList(JSONObject jsonObject) throws Exception {
		if (!jsonObject.containsKey("vodList")) {
			throw new Exception("数据为空");
		}
		JSONArray arr = jsonObject.getJSONArray("vodList");
		List<JSONObject> list = new ArrayList<>();
		for (Object obj : arr) {
			list.add(JSONObject.fromObject(obj));
		}
		if (list.isEmpty()) {
			throw new Exception("数据为空");
		}
		activityServiceImpl.saveGenseeVodList(list);
	}

	private void updateActivityFile(JSONObject jsonObject) throws Exception {
		long activityId = MapUtils.getLongValue(jsonObject, "id", 0);
		if (activityId == 0) {
			throw new Exception("activityId为空");
		}
		String fileAttr = MapUtils.getString(jsonObject, "fileAttr", null);
		InsActivity activity = new InsActivity();
		activity.setId(activityId);
		activity.setIsEnd((short) 3);
		activity.setFileAttr(fileAttr);
		activity.setUpdatedAt(new Date());
//		activity.setUpdatedBy(defaultUser);
		activityServiceImpl.updateActivity(activity, null);
	}

	private HandleResult syncGenseeRecord(JSONObject jsonObject) {
		HandleResult result = new HandleResult();
		try {
			int messageCode = MapUtils.getIntValue(jsonObject, "messageCode", 0);
			if (messageCode != MeixConstantCode.M_1008) {
				result.setMessage("同步直播收听记录失败");
				result.setMessageCode(MeixConstantCode.M_1009);
				return result;
			}

			JSONArray dataArray = jsonObject.getJSONArray("data");
			if (dataArray == null || dataArray.size() == 0) {
				log.info("直播收听记录列表为空:{}", jsonObject);
				result.setMessage("直播收听记录列表为空");
				result.setMessageCode(MeixConstantCode.M_1008);
				return result;
			}
			for (int i = 0; i < dataArray.size(); i++) {
				long activityId = dataArray.getJSONObject(i).getLong("activityId");
				JSONArray data = dataArray.getJSONObject(i).getJSONArray("data");
				List<ActivityGenseeRecord> dataList = GsonUtil.json2List(data.toString(), ActivityGenseeRecord.class);
				if (activityId <= 0 || dataList == null || dataList.size() == 0) {
					continue;
				}
				long startTime = System.currentTimeMillis();
				//先删除
				activityServiceImpl.deleteGenseeRecord(activityId);
				Iterator<ActivityGenseeRecord> iterator = dataList.iterator();
				while (iterator.hasNext()) {
					ActivityGenseeRecord item = iterator.next();
					UserInfo userInfo = userService.getUserInfo(0, null, item.getMobile());
					//非内部和白名单不保存
					if (userInfo == null || (userInfo.getAccountType() != MeixConstants.ACCOUNT_INNER &&
						userInfo.getAccountType() != MeixConstants.ACCOUNT_WL)) {
						iterator.remove();
						continue;
					}

//				if (userInfo != null) {
					item.setUid(userInfo.getId());
					item.setCompany(userInfo.getCompanyName());
//				} else {
//					String nickname = item.getNickname();
//					if (StringUtil.isBlank(nickname)) {
//						nickname = "匿名用户";
//					} else {
//						if (nickname.contains("_")) {
//							nickname = nickname.split("_")[1];
//						}
//					}
//					UserInfo insCustomer = userService.addUser(item.getMobile(), item.getCompany(), nickname);
//					if (insCustomer != null) {
//						item.setUid(insCustomer.getId());
//						item.setCompany(insCustomer.getCompanyName());
//					}
//				}
				}
				activityServiceImpl.saveGenseeRecordList(dataList);
				log.info("同步直播收听记录列表{}条,耗时{}ms", dataList.size(), (System.currentTimeMillis() - startTime));
			}

			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("处理直播收听记录列表失败", e);
			result.setMessage("处理直播收听记录列表失败");
			result.setMessageCode(MeixConstantCode.M_1009);
		}
		return result;
	}

	private HandleResult syncTeleJoinRecord(JSONObject jsonObject) {
		HandleResult result = new HandleResult();
		try {
			int messageCode = MapUtils.getIntValue(jsonObject, "messageCode", 0);
			if (messageCode != MeixConstantCode.M_1008) {
				result.setMessage("同步电话接入记录失败");
				result.setMessageCode(MeixConstantCode.M_1009);
				return result;
			}

			JSONArray dataArray = jsonObject.getJSONArray("data");
			if (dataArray == null || dataArray.size() == 0) {
				log.info("电话接入收听记录列表为空:{}", jsonObject);
				result.setMessage("电话接入收听记录列表为空");
				result.setMessageCode(MeixConstantCode.M_1008);
				return result;
			}

			for (int i = 0; i < dataArray.size(); i++) {
				long activityId = dataArray.getJSONObject(i).getLong("activityId");
				JSONArray data = dataArray.getJSONObject(i).getJSONArray("data");
				List<ActivityThirdJoinInfo> dataList = GsonUtil.json2List(data.toString(), ActivityThirdJoinInfo.class);
				if (activityId <= 0 || dataList == null || dataList.size() == 0) {
					continue;
				}
				long startTime = System.currentTimeMillis();
				//先删除
				activityServiceImpl.deleteTeleJoinRecord(activityId);
				activityServiceImpl.saveTeleJoinRecordList(dataList);
				log.info("同步电话接入收听记录列表{}条,耗时{}ms", dataArray.size(), (System.currentTimeMillis() - startTime));
			}
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.error("处理电话接入收听记录列表失败", e);
			result.setMessage("处理电话接入收听记录列表失败");
			result.setMessageCode(MeixConstantCode.M_1009);
		}
		return result;
	}

	private void updateActivityStatus(JSONObject jsonObject) throws Exception {
		long activityId = MapUtils.getLongValue(jsonObject, "activityId", 0);
		if (activityId == 0) {
			throw new Exception("activityId为空");
		}
		int isEnd = MapUtils.getIntValue(jsonObject, "isEnd", 0);
		if (isEnd != 3 && isEnd != 4) {
			throw new Exception("无效的状态isEnd：" + isEnd);
		}
		InsActivity activity = new InsActivity();
		activity.setId(activityId);
		activity.setIsEnd((short) isEnd);
		activity.setUpdatedAt(new Date());
//		activity.setUpdatedBy(defaultUser);
		activityServiceImpl.updateActivity(activity, null);
	}

	private void saveActivity(JSONObject jsonObject) throws Exception {
		InsActivity activity = new InsActivity();
		try {
			long id = MapUtils.getLongValue(jsonObject, "id", 0);
			if (id == 0) {
				throw new Exception("无效的活动");
			}
			String fileAttr = MapUtils.getString(jsonObject, "fileAttr", "");
			if (fileAttr != null && fileAttr.length() > 0) {
				JSONObject fileAttrObj = JSONObject.fromObject(fileAttr);
				fileAttrObj.put("ct", System.currentTimeMillis());
				activity.setFileAttr(fileAttrObj.toString());
			}

			String joinNumber = MapUtils.getString(jsonObject, "joinNumber", null);
			String joinPassword = MapUtils.getString(jsonObject, "joinPassword", null);

			String refuseReason = MapUtils.getString(jsonObject, "reason", null);
			int status = MapUtils.getIntValue(jsonObject, "status", -1);
			activity.setId(id);
			activity.setJoinNumber(joinNumber);
			activity.setJoinPassword(joinPassword);
			activity.setRefuseReason(refuseReason);
			activity.setUpdatedAt(new Date());
			if (status > -1) {
				activity.setActivityStatus(status);
			}
//			activity.setUpdatedBy(defaultUser);

			String webcastId = MapUtils.getString(jsonObject, "webcastId", null);
			String webcastNumber = MapUtils.getString(jsonObject, "webcastNumber");
			String organizerToken = MapUtils.getString(jsonObject, "organizerToken");
			String panelistToken = MapUtils.getString(jsonObject, "panelistToken");
			String attendeeToken = MapUtils.getString(jsonObject, "attendeeToken");
			String organizerJoinUrl = MapUtils.getString(jsonObject, "organizerJoinUrl");
			String panelistJoinUrl = MapUtils.getString(jsonObject, "panelistJoinUrl");
			String attendeeAShortJoinUrl = MapUtils.getString(jsonObject, "attendeeAShortJoinUrl");
			String attendeeJoinUrl = MapUtils.getString(jsonObject, "attendeeJoinUrl");

			GenseeVideo genseeVideo = new GenseeVideo();
			genseeVideo.setId(webcastId);
			genseeVideo.setNumber(webcastNumber);
			genseeVideo.setOrganizerToken(organizerToken);
			genseeVideo.setPanelistToken(panelistToken);
			genseeVideo.setAttendeeToken(attendeeToken);
			genseeVideo.setOrganizerJoinUrl(organizerJoinUrl);
			genseeVideo.setPanelistJoinUrl(panelistJoinUrl);
			genseeVideo.setAttendeeAShortJoinUrl(attendeeAShortJoinUrl);
			genseeVideo.setAttendeeJoinUrl(attendeeJoinUrl);
			InsActivity before = activityServiceImpl.getBaseActivityById(id);
			if (before == null) {
				throw new Exception("无效的活动");
			}
			activityServiceImpl.updateActivity(activity, genseeVideo);

			if (before.getActivityStatus() != status) {
				pushService.pushActivityAuditRes(status, before.getTitle(), refuseReason, DateUtil.formatDate(before.getStartTime(), "yyyy-MM-dd HH:mm"), before.getCreatedBy());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
