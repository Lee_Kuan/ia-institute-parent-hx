package com.meix.institute.impl;

import com.meix.institute.BaseService;
import com.meix.institute.api.ICompanyMonthlyStockService;
import com.meix.institute.api.IInsMonthlyGoldStockService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.ISecumainService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.search.StockYieldSearchVo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.MathUtil;
import com.meix.institute.vo.HoneyBeeBaseVo;
import com.meix.institute.vo.stock.MonthlyGoldStockVo;
import com.meix.institute.vo.user.UserInfo;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by zenghao on 2019/7/3.
 */
@Service
public class CompanyMonthlyStockServiceImpl extends BaseService implements ICompanyMonthlyStockService {
	private static Logger log = LoggerFactory.getLogger(CompanyMonthlyStockServiceImpl.class);

	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IInsMonthlyGoldStockService monthlyGoldStockService;
	@Autowired
	private ISecumainService secumainService;
	@Autowired
	private IInsUserService insUserService;

	private ConcurrentLinkedQueue<String> waterflowIdList = new ConcurrentLinkedQueue<>();
	protected static final String CACHE_KEY = "CompanyMonthlyStockCache";
	protected static CacheManager cacheManager = null;
	protected Cache cache = null;
	private String updateTime = null;

	protected boolean isRefreshing = false;

	protected String getCacheName() {
		return CACHE_KEY;
	}

//	@PostConstruct
//	public void init() {
//		refreshCache();
//	}

	@Override
	public void refreshCache() {
		if (isRefreshing) {
			log.debug("[{}] 正在缓存......", getCacheName());
			return;
		}

		isRefreshing = true;

		if (cacheManager == null) {
			cacheManager = CacheManager.newInstance(this.getClass().getResource("/ehcache.xml"));
		}
		if (cache == null) {
			cache = cacheManager.getCache(getCacheName());
		}

		Thread thread = new Thread() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				log.debug("[{}] 开始缓存......", getCacheName());
				try {
					doRefreshCache();
				} catch (Exception e) {
					log.error("", e);
				} finally {
					isRefreshing = false;
				}
				log.debug("[{}] 结束缓存...... 耗时:{}ms, cacheSize:{}", getCacheName(),
					(System.currentTimeMillis() - startTime), cache.getSize());
			}
		};
		thread.start();
	}

	protected void doRefreshCache() throws Exception {
		try {
			log.info("[{}] doRefreshCache updateTime:{}", getCacheName(), updateTime);
			List<HoneyBeeBaseVo> list = getCompanyMonthlyStockList();
			if (list == null) {
				cache.removeAll();
				waterflowIdList.clear();
				return;
			}
			log.info("[{}] doRefreshCache listSize:{}", getCacheName(), list.size());

			//第二月更新数据时清空历史数据
			if (updateTime != null && updateTime.substring(0, 7).compareTo(DateUtil.dateToStr(System.currentTimeMillis(), "yyyy-MM")) < 0) {
				cache.removeAll();
				waterflowIdList.clear();
			}

			for (int i = 0; i < list.size(); i++) {
				HoneyBeeBaseVo vo = list.get(i);
				String curUpdateTime = vo.getUpdateTime();
				if (curUpdateTime != null && (updateTime == null || curUpdateTime.compareTo(updateTime) > 0)) {
					updateTime = curUpdateTime;
				}
				float curYield = MapUtils.getFloatValue(vo.getItem(), "monthYieldRate", 0f);
				long companyCode = MapUtils.getLongValue(vo.getItem(), "companyCode", 0);
				String id = companyCode + "_" + vo.getDataId();//唯一id
				if (vo.getStatus() != 1 || curYield < 0.03) {
					removeElement(id);
					waterflowIdList.remove(id);
					log.debug("[{}] doRefreshCache remove id:{}", getCacheName(), id);
				} else {
					if (waterflowIdList.contains(id)) {
						HoneyBeeBaseVo lastVo = getElement(id);
						float lastYield = MapUtils.getFloatValue(lastVo.getItem(), "monthYieldRate", 0f);
						int lastGrade = new BigDecimal(lastYield).divide(new BigDecimal(0.03), 2, BigDecimal.ROUND_HALF_UP).intValue();
						int curGrade = new BigDecimal(curYield).divide(new BigDecimal(0.03), 2, BigDecimal.ROUND_HALF_UP).intValue();
						if (curGrade > lastGrade) {//收益提升超过3%则重新更新到首页
							removeElement(id);
							log.info("{}收益提升超过3%,lastYield:{},curYield:{}", id, lastYield, curYield);
						} else {
							lastVo.getItem().put("monthYieldRate", curYield);
							removeElement(id);
							if (curGrade <= 0) {
								continue;
							}
							lastVo.setContent("<span style=\"margin:0 0\">" + "研究所月度金股本月收益超过" + "<span style=\"color:#FA4F4D;margin:0 0\">" +
								"+" + MathUtil.percentFormat(new BigDecimal(curGrade * 0.03)) + "</span>" + "</span>");
							addElement(id, lastVo);
							log.info("{}收益提升不超过3%，更新收益信息,lastYield:{},curYield:{}", id, lastYield, curYield);
							continue;
						}
					}
					int curGrade = new BigDecimal(curYield).divide(new BigDecimal(0.03), 2, BigDecimal.ROUND_HALF_UP).intValue();
					if (curGrade <= 0) {
						continue;
					}
					vo.setContent("<span style=\"margin:0 0\">" + "研究所月度金股本月收益超过" + "<span style=\"color:#FA4F4D;margin:0 0\">" +
						"+" + MathUtil.percentFormat(new BigDecimal(curGrade * 0.03)) + "</span>" + "</span>");
					addElement(id, vo);
					waterflowIdList.add(id);
					log.debug("[{}] doRefreshCache add id:{}", getCacheName(), id);
				}
			}
			list.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<HoneyBeeBaseVo> getList(long uid, long companyCode, Set<Integer> selfstockSet) {
		List<HoneyBeeBaseVo> voList = new ArrayList<>();
		for (String id : waterflowIdList) {
			HoneyBeeBaseVo vo = getElement(id);
			if (vo == null) {
				continue;
			}

			//下架的数据不展示
			if (vo.getStatus() != 1) {
				continue;
			}

			long voCompanyCode = MapUtils.getLongValue(vo.getItem(), "companyCode", 0);
			if (voCompanyCode != companyCode) {
				continue;
			}

			try {
				HoneyBeeBaseVo copyVo = (HoneyBeeBaseVo) BeanUtils.cloneBean(vo);
//				log.info("数据{} : {}", vo.getDataId(), GsonUtil.obj2Json(copyVo));
				if (selfstockSet.contains(Long.valueOf(copyVo.getDataId()).intValue())) {
					copyVo.getItem().put("isSelfstock", 1);
				} else {
					copyVo.getItem().put("isSelfstock", 0);
				}
				int innerCode = MapUtils.getIntValue(copyVo.getItem(), "innerCode", 0);
				int industryCode = MapUtils.getIntValue(copyVo.getItem(), "industryCode", 0);
				float score = insUserService.getUserLabelTotalScore(uid, innerCode, industryCode);
				copyVo.setRanking(score);
				voList.add(copyVo);
			} catch (Exception e) {
				log.error("", e);
			}
		}
		return voList;
	}

	/**
	 * 获取公司月度金股数据
	 *
	 * @return
	 */
	public List<HoneyBeeBaseVo> getCompanyMonthlyStockList() {
		String month = DateUtil.dateToStr(System.currentTimeMillis(), "yyyy-MM");
		List<MonthlyGoldStockVo> list = monthlyGoldStockService.getGoldStockMonthList(100, 0, commonServerProperties.getCompanyCode(), month, -1);
		if (list != null && list.size() > 0) {
			List<HoneyBeeBaseVo> result = new ArrayList<>();
			log.info("[getCompanyMonthlyStockList] list size:{}", list.size());
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			String startDate = DateUtil.dateToStr(calendar.getTimeInMillis(), "yyyy-MM-dd");
			Calendar calendarEnd = Calendar.getInstance();
			String endDate = DateUtil.dateToStr(calendarEnd.getTimeInMillis(), "yyyy-MM-dd");

			List<StockYieldSearchVo> searchList = new ArrayList<>();
			for (MonthlyGoldStockVo stockVo : list) {
				SecuMain cache = secumainService.getSecuMain(stockVo.getInnerCode());
				if (cache == null) {
//					log.info("股票{}没有缓存数据", stockVo.getInnerCode());
					continue;
				}
				StockYieldSearchVo searchVo = new StockYieldSearchVo();
				searchVo.setInnerCode(stockVo.getInnerCode());
				searchVo.setStartDate(startDate);
				searchVo.setEndDate(endDate);
				searchList.add(searchVo);

				HoneyBeeBaseVo vo = new HoneyBeeBaseVo();
				vo.setDataId(stockVo.getInnerCode());
				vo.setDataType(MeixConstants.USER_GP);
				vo.setApplyTime(DateUtil.getCurrentDateTime());
				//下架状态
				vo.setStatus(stockVo.getHomeShowFlag() == null ? 0 : stockVo.getHomeShowFlag());
				vo.setWaterflowRecType(2);//1 自选股推荐 2 月度金股推荐

				Map<String, Object> item = new HashMap<>();
				item.put("dataId", vo.getDataId());
				item.put("dataType", vo.getDataType());
				item.put("innerCode", cache.getInnerCode());
				item.put("secuCode", cache.getSecuCode());
				item.put("secuAbbr", cache.getSecuAbbr());
				item.put("suffix", cache.getSuffix());
				item.put("secuMarket", cache.getSecuMarket());
				item.put("secuClass", cache.getSecuCategory());
				item.put("industryCode", cache.getIndustryCode());
				item.put("industryName", cache.getIndustryName());
				item.put("companyCode", stockVo.getCompanyCode());
				UserInfo userInfoCache = userServiceImpl.getUserInfoByUuid(stockVo.getRecPerson());
				if (userInfoCache != null) {
					item.put("authorCode", userInfoCache.getId());
				}
				item.put("authorName", stockVo.getRecPersonName());
				item.put("recDesc", stockVo.getRecDesc());
				item.put("month", stockVo.getMonth());
				vo.setItem(item);
				result.add(vo);
			}

			Map<Integer, BigDecimal> yieldMap = getStockIntervalYield(searchList);
			if (yieldMap != null) {
				for (HoneyBeeBaseVo vo : result) {
					BigDecimal stockYield = yieldMap.get(new Long(vo.getDataId()).intValue());
					if (stockYield != null) {
						Map<String, Object> item = vo.getItem();
						item.put("monthYieldRate", stockYield.setScale(4, BigDecimal.ROUND_HALF_UP).floatValue());
					}
				}
			}
			return result;
		}
		return null;
	}

	private HoneyBeeBaseVo getElement(String id) {
		cache.acquireReadLockOnKey(id);
		HoneyBeeBaseVo vo = null;
		Element element = cache.get(id);
		if (element != null) {
			vo = (HoneyBeeBaseVo) element.getObjectValue();
		}
		cache.releaseReadLockOnKey(id);

		return vo;
	}

	private HoneyBeeBaseVo addElement(String id, HoneyBeeBaseVo vo) {
		cache.acquireWriteLockOnKey(id);
		cache.put(new Element(id, vo));
		cache.releaseWriteLockOnKey(id);

		return vo;
	}

	private void removeElement(String id) {
		cache.acquireWriteLockOnKey(id);
		cache.remove(id);
		cache.releaseWriteLockOnKey(id);
	}
}
