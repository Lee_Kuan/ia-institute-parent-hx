package com.meix.institute.impl;

import com.meix.institute.BaseService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IMeetingService;
import com.meix.institute.entity.InsMorningMeetingIssueType;
import com.meix.institute.entity.InsUser;
import com.meix.institute.entity.MorningMeeting;
import com.meix.institute.mapper.InsMorningMeetingIssueTypeMapper;
import com.meix.institute.mapper.InsUserMapper;
import com.meix.institute.mapper.MeetingMapper;
import com.meix.institute.mapper.MorningMeetingMapper;
import com.meix.institute.search.ComplianceListSearchVo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.meeting.MorningMeetingVo;
import net.sf.json.JSONArray;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @describe 晨会
 */
@Service
public class MeetingServiceImpl extends BaseService implements IMeetingService {

	@Autowired
	private MorningMeetingMapper meetingMapper;
	@Autowired
	private InsMorningMeetingIssueTypeMapper issueTypeMapper;
	@Autowired
	private InsUserServiceImpl userService;
	@Autowired
	private MeetingMapper myMeetingMapper;
	@Autowired
	private InsUserMapper insUserMapper;
	@Autowired
	private IInstituteMessagePushService messagePushService;

	@Override
	public long saveMeeting(long id, String uuid, MorningMeeting meeting, JSONArray issueType) {
		long meetingId = meeting.getId();
		meeting.setStatus(1);
		if (meetingId == 0) {
			// 新增的
			meetingId = id;
			meeting.setId(meetingId);
			meeting.setCreatedAt(new Date());
			meeting.setCreatedBy(uuid);
			meeting.setUpdatedAt(new Date());
			meeting.setUpdatedBy(uuid);
			if (meetingMapper.insert(meeting) < 1) {
				return 0;
			}
			messagePushService.messagePush(meeting.getCompanyCode(), meetingId, InstituteMessagePushServiceImpl.CREATE_MORNING_MEETING, meeting.getTitle(), 0, 0, meeting.getUrl());
		} else {
			meeting.setUpdatedAt(new Date());
			meeting.setUpdatedBy(uuid);
			// 修改的
			if (meetingMapper.updateByPrimaryKeySelective(meeting) < 1) {
				return 0;
			}
			// 删除所有从表数据

			Example example = new Example(InsMorningMeetingIssueType.class);
			example.createCriteria().andEqualTo("meetingId", meetingId);
			issueTypeMapper.deleteByExample(example);

		}

		if (issueType != null && issueType.size() > 0) {
			List<InsMorningMeetingIssueType> issueList = new ArrayList<>();
			for (int i = 0; i < issueType.size(); i++) {
				InsMorningMeetingIssueType params = new InsMorningMeetingIssueType();
				params.setMeetingId(meetingId);
				params.setIssueType(issueType.getInt(i));

				params.setCreatedAt(new Date());
				params.setCreatedBy(uuid);
				params.setUpdatedAt(new Date());
				params.setUpdatedBy(uuid);
				issueList.add(params);
			}
			issueTypeMapper.insertList(issueList);
		}

		return meetingId;
	}

	@Override
	public MorningMeetingVo getMeetingDetail(long meetingId) {
		MorningMeeting meeting = meetingMapper.selectByPrimaryKey(meetingId);
		MorningMeetingVo meetingVo = new MorningMeetingVo();
		if (meeting == null) {
			return meetingVo;
		}
		if (meeting.getType() == null) {
			meeting.setType(0);
		}
		ConvertUtils.register(new DateConverter(null), java.util.Date.class);
		BeanUtils.copyProperties(meeting, meetingVo);
		Date publishDate = meeting.getPublishDate() == null ? meeting.getCreatedAt() : meeting.getPublishDate();
		meetingVo.setPublishDate(publishDate);

		InsUser user = userService.getUserInfo(meetingVo.getCreatedBy());
		if (user != null) {
			meetingVo.setAuthorName(user.getUserName());
			meetingVo.setAuthorCode(user.getId());
		}
		meetingVo.setShare(meeting.getShare());
		meetingVo.setShareName(getShareDesc(meeting.getShare()));
		Example example = new Example(InsMorningMeetingIssueType.class);
		example.createCriteria().andEqualTo("meetingId", meetingId);

		//设置发布渠道信息
		List<InsMorningMeetingIssueType> typeList = issueTypeMapper.selectByExample(example);
		if (typeList != null && typeList.size() > 0) {
			List<Integer> issueTypeList = new ArrayList<>(typeList.size());
			for (InsMorningMeetingIssueType type : typeList) {
				issueTypeList.add(type.getIssueType());
			}
			meetingVo.setIssueTypeList(issueTypeList);
		}

//		meetingVo.setShare(shareVoList);

		return meetingVo;
	}

	@Override
	public List<MorningMeetingVo> getMeetingList(ComplianceListSearchVo searchVo) {
		Example example = buildComplianceListExample(searchVo);
		RowBounds rowBounds = new RowBounds(searchVo.getCurrentPage(), searchVo.getShowNum());
		List<MorningMeeting> list = meetingMapper.selectByExampleAndRowBounds(example, rowBounds);
		if (list == null || list.size() == 0) {
			return Collections.emptyList();
		}
		List<MorningMeetingVo> result = new ArrayList<>(list.size());
		for (MorningMeeting item : list) {
			if (item.getType() == null) {
				item.setType(0);
			}
			MorningMeetingVo meetingVo = new MorningMeetingVo();
			ConvertUtils.register(new DateConverter(null), java.util.Date.class);
			BeanUtils.copyProperties(item, meetingVo);

			Date publishDate = item.getPublishDate() == null ? item.getCreatedAt() : item.getPublishDate();
			meetingVo.setPublishDate(publishDate);
			meetingVo.setCreatedAt(item.getCreatedAt());
			meetingVo.setCreateTime(DateUtil.dateToStr(item.getCreatedAt(), DateUtil.dateFormat));
			meetingVo.setUndoTime(item.getUpdatedAt());
			meetingVo.setShareName(getShareDesc(item.getShare()));
			//设置创建人和修改人信息
			InsUser createUser = userService.getUserInfo(item.getCreatedBy());
			if (createUser != null) {
				meetingVo.setAuthorName(createUser.getUserName());
				meetingVo.setUserState(createUser.getUserState());
			}
			InsUser updateUser = userService.getUserInfo(item.getUpdatedBy());
			if (updateUser != null) {
				meetingVo.setEditUserName(updateUser.getUserName());
			}

			//设置发布渠道信息
			InsMorningMeetingIssueType issueTypeSearch = new InsMorningMeetingIssueType();
			issueTypeSearch.setMeetingId(item.getId());
			List<InsMorningMeetingIssueType> typeList = issueTypeMapper.select(issueTypeSearch);
			if (typeList != null && typeList.size() > 0) {
				List<Integer> issueTypeList = new ArrayList<>(typeList.size());
				for (InsMorningMeetingIssueType type : typeList) {
					issueTypeList.add(type.getIssueType());
				}
				meetingVo.setIssueTypeList(issueTypeList);
			}
			result.add(meetingVo);
		}
		list.clear();
		return result;
	}

	@Override
	public int getMeetingCount(ComplianceListSearchVo searchVo) {
		Example example = buildComplianceListExample(searchVo);
		return meetingMapper.selectCountByExample(example);
	}

	private Example buildComplianceListExample(ComplianceListSearchVo searchVo) {
		Example example = new Example(MorningMeeting.class);
		Example.Criteria criteria = example.createCriteria();
//		if (searchVo.getCompanyCode() > 0) {
//			criteria.andEqualTo("companyCode", searchVo.getCompanyCode());
//		}
		if (searchVo.getStatusList() != null && searchVo.getStatusList().size() > 0) {
			criteria.andIn("status", searchVo.getStatusList());
		}
		if (searchVo.getShare() > -1) {
			criteria.andEqualTo("share", searchVo.getShare());
		}
		if (searchVo.getSubType() > -1) {
			criteria.andEqualTo("meetingType", searchVo.getSubType());
		}
		if (StringUtils.isNotBlank(searchVo.getStartTime())) {
			criteria.andGreaterThanOrEqualTo("publishDate", searchVo.getStartTime());
		}
		if (StringUtils.isNotBlank(searchVo.getEndTime())) {
			criteria.andLessThanOrEqualTo("publishDate", searchVo.getEndTime());
		}
		if (!StringUtils.isNullorWhiteSpace(searchVo.getCondition())) {

			Example userExample = new Example(InsUser.class);
			userExample.createCriteria().andLike("userName", "%" + searchVo.getCondition() + "%");
			List<InsUser> userList = insUserMapper.selectByExample(userExample);
			Example.Criteria condition = example.and();
			condition.andLike("title", "%" + searchVo.getCondition() + "%");
			if (userList != null && userList.size() > 0) {
				List<String> uuidList = new ArrayList<>();
				for (InsUser user : userList) {
					uuidList.add(user.getUser());
				}
				condition.orIn("createdBy", uuidList);
			}
		}
		if (searchVo.getSortType() == 0) {
			if (searchVo.getSortRule() == -1) {
				example.orderBy("publishDate").desc();
			} else {
				example.orderBy("publishDate").asc();
			}
		} else if (searchVo.getSortType() == 1) {
			if (searchVo.getSortRule() == -1) {
				example.orderBy("updatedAt").desc();
			} else {
				example.orderBy("updatedAt").asc();
			}
		} else {
			example.orderBy("publishDate").desc();
		}
		return example;
	}

	@Override
	public void examineMeeting(MorningMeeting meeting) {
		meetingMapper.updateByPrimaryKeySelective(meeting);
	}

	@Override
	public MorningMeetingVo getMeetingDetail(long meetingId, long uid) {
		MorningMeetingVo vo = myMeetingMapper.getMeetingDetail(meetingId, uid);
//		if (vo. == MeixConstants.SHARE_DEFAULT) {
//			groupPower = groupService.getResourceGroupPower(uid, MeixConstants.REPORT_ALBUM, 0, 0);
//			if (groupPower != null && !groupPower.isEmpty()) {
//				for (GroupPowerVo vo : groupPower) {
//					if (vo.getShare() == MeixConstants.SHARE_DETAIL) {
//						permission = 1;
//					}
//				}
//			}
//		} else if (permission == 1) {
//			groupPower = groupService.getResourceGroupPower(uid, MeixConstants.REPORT_ALBUM, 0, 1);
//		}
//		detail.put("groupPower", groupPower == null ? new ArrayList<>() : groupPower);
		return vo;
	}

	@Override
	public List<MorningMeetingVo> getMeetingList(long uid, long companyCode, int showNum, int currentPage, int clientType, int meetingType) {
		List<MorningMeetingVo> list = myMeetingMapper.getMeetingList(uid, companyCode, showNum, currentPage, clientType, meetingType);
		return list;
	}
}
