package com.meix.institute.impl;

import com.meix.institute.api.IInsSyncSndReqService;
import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.Key;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsSyncSndReq;
import com.meix.institute.mapper.InsSyncSndReqMapper;
import com.meix.institute.pojo.InsSyncSndReqInfo;
import com.meix.institute.pojo.InsSyncSndReqS;
import com.meix.institute.pojo.InsSyncSndReqSearch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class InsSyncSndReqServiceImpl implements IInsSyncSndReqService {

	@Autowired
	private InsSyncSndReqMapper insSyncSndReqMapper;
	@Autowired
	private IMeixSyncService meixSyncService;

	@Override
	public Key save(InsSyncSndReqInfo info, String user) {
		Long id = info.getId();
		InsSyncSndReq record = new InsSyncSndReq();
		BeanUtils.copyProperties(info, record);
		Key key = new Key();

		if (null == id || id == 0l) { // 新增
			insSyncSndReqMapper.insert(record);
			key.setId(record.getId());
		} else {  // 更新
			insSyncSndReqMapper.updateByPrimaryKeySelective(record);
			key.setId(id);
		}
		return key;
	}

	@Override
	public InsSyncSndReqInfo selectByPrimaryKey(Long id) {
		InsSyncSndReq record = insSyncSndReqMapper.selectByPrimaryKey(id);
		InsSyncSndReqInfo info = new InsSyncSndReqInfo();
		BeanUtils.copyProperties(record, info);
		return info;
	}

	@Override
	public List<InsSyncSndReqInfo> findExceptionReq() {
		List<InsSyncSndReq> daolist = insSyncSndReqMapper.findExceptionReq();
		return getServiceList(daolist);
	}

	@Override
	public Paged<InsSyncSndReqInfo> paged(InsSyncSndReqSearch search, String user) {
		InsSyncSndReqS daoSearch = new InsSyncSndReqS();
		BeanUtils.copyProperties(search, daoSearch);
		int count = insSyncSndReqMapper.count(daoSearch);
		Paged<InsSyncSndReqInfo> paged = new Paged<InsSyncSndReqInfo>();
		paged.setCount(count);
		if (count > 0) {
			List<InsSyncSndReq> daolist = insSyncSndReqMapper.find(daoSearch);
			paged.setList(getServiceList(daolist));
		} else {
			paged.setList(Collections.emptyList());
		}
		return paged;
	}

	@Override
	public void close(Long id, String remarks, String user) {
		InsSyncSndReq record = new InsSyncSndReq();
		record.setState(MeixConstants.SYNC_STATE_CLOSE);
		record.setId(id);
		record.setUpdatedAt(new Date());
		record.setUpdatedBy(user);
		record.setRemarks(remarks);
		insSyncSndReqMapper.updateByPrimaryKeySelective(record);
	}

	private List<InsSyncSndReqInfo> getServiceList(List<InsSyncSndReq> daolist) {
		if (daolist != null && daolist.size() > 0) {
			List<InsSyncSndReqInfo> list = new ArrayList<InsSyncSndReqInfo>();
			daolist.forEach((info) -> {
				InsSyncSndReqInfo data = new InsSyncSndReqInfo();
				BeanUtils.copyProperties(info, data);
				list.add(data);
			});
			return list;
		}
		return Collections.emptyList();
	}

	@Override
	public void retry(Long id, String user) {
		InsSyncSndReqInfo info = selectByPrimaryKey(id);
		if (info == null || StringUtils.equals(MeixConstants.SYNC_STATE_SUCCESS, info.getState())) {
			return;
		}
		info.setRetryCnt(info.getRetryCnt() + 1);
		info.setRetryMax(info.getRetryMax());
		info.setUpdatedAt(new Date());
		info.setUpdatedBy(user);

		info = meixSyncService.reqMeixSync(info);
		//优化更新
		info.setReqBody(null);
		this.save(info, user);
	}

}
