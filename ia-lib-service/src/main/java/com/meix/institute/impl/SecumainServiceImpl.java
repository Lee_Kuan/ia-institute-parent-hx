package com.meix.institute.impl;

import com.meix.institute.api.ISecumainService;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.mapper.SecuMainMapper;
import com.meix.institute.mapper.SecuMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/8/28.
 */
@Service
public class SecumainServiceImpl implements ISecumainService {

	@Autowired
	private SecuMainMapper secuMainMapper;
	@Autowired
	private SecuMapper secuMapper;

	@Override
	public void saveSecumainList(List<SecuMain> list) {
		for (SecuMain item : list) {
			Example example = new Example(SecuMain.class);
			example.createCriteria().andEqualTo("innerCode", item.getInnerCode());
			SecuMain exist = secuMainMapper.selectOneByExample(example);
			if (exist == null) {
				secuMainMapper.deleteByPrimaryKey(item.getId());
				secuMainMapper.insert(item);
			} else {
				secuMainMapper.updateByExampleSelective(item, example);
			}
		}
	}

	@Override
	public List<SecuMain> getSecuMain(int secuType, int type, int showNum, String condition, int industryCode) {
		StringBuffer nameCondition = new StringBuffer();
		for (int i = 0; i < condition.length(); i++) {
			char ch = condition.charAt(i);
			if (ch >= 'a' && ch <= 'z') {
				ch = (char) (ch + 'Ａ' - 'a');
			} else if (ch >= 'A' && ch <= 'Z') {
				ch = (char) (ch + 'Ａ' - 'A');
			}
			nameCondition.append(ch);
		}
		if (secuType == 0) {
			List<SecuMain> list = secuMapper.getSecuMain(type, showNum, condition, nameCondition.toString(), industryCode);
			return list;
		} else {
			List<SecuMain> list = secuMapper.getSecuMain(type, showNum * 3 / 5, condition, nameCondition.toString(), industryCode);
			List<SecuMain> HKList = secuMapper.getSecuMainForHK(type, showNum * 2 / 5, condition, nameCondition.toString(), industryCode);
			list.addAll(HKList);
			return list;
		}
	}

	@Override
	public List<SecuMain> getIndustryInfo() {
		Example example = new Example(SecuMain.class);
		example.createCriteria().andEqualTo("secuCategory", 4).andGreaterThan("industryCode", 0);
		Example.Criteria or = example.or();
		or.andEqualTo("secuCode", "000300");
		return secuMainMapper.selectByExample(example);
	}

	@Override
	public SecuMain getSecuMain(int innerCode) {
		Example example = new Example(SecuMain.class);
		example.createCriteria().andEqualTo("innerCode", innerCode);
		return secuMainMapper.selectOneByExample(example);
	}

	@Override
	public Paged<Map<String, Object>> getCompsIndustry(Map<String, Object> search, String uuid) {
		List<Map<String, Object>> list = secuMapper.findCompsIndustry(search);
		Paged<Map<String, Object>> paged = new Paged<Map<String, Object>>();
		if (CollectionUtils.isEmpty(list)) {
			paged.setList(Collections.emptyList());
			paged.setCount(0);
			return paged;
		}
		int count = secuMapper.countCompsIndustry(search);
		paged.setList(list);
		paged.setCount(count);
		return paged;
	}

	@Override
	public SecuMain getSecuMainBySecuCode(String secuCode) {
		Example example = new Example(SecuMain.class);
		example.createCriteria().andEqualTo("secuCode", secuCode).andEqualTo("secuCategory", 1);
		return secuMainMapper.selectOneByExample(example);
	}

}
