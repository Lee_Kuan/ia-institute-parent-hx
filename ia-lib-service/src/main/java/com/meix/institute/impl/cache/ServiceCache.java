package com.meix.institute.impl.cache;

import com.meix.institute.api.IConstDao;
import com.meix.institute.api.ISecumainService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.vo.cache.ConstantVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ServiceCache {
	protected final static Logger log = LoggerFactory.getLogger(ServiceCache.class);
	@Autowired
	private IConstDao constDaoImpl;
	@Autowired
	private ISecumainService secumainService;

	//行业字典 键是行业代码
	private static Map<Integer, SecuMain> industryMap = new HashMap<>();
	//行业标签字典(新) 键是内码
	private static Map<Integer, ConstantVo> industryLabelMap = new HashMap<>();
	//行业标签字典(新) 键是代码
	private static Map<Integer, ConstantVo> industryLabelMapOfCode = new HashMap<>();
	//港股行业对应表
	private static Map<String, ConstantVo> hkIndustryLabelMap = new HashMap<>();

	public Map<Integer, SecuMain> getIndustryMap() {
		return industryMap;
	}

	public Map<Integer, ConstantVo> getIndustryLabelMap() {
		return industryLabelMap;
	}

	public Map<Integer, ConstantVo> getIndustryLabelMapOfCode() {
		return industryLabelMapOfCode;
	}

	public Map<String, ConstantVo> getHKIndustryLabelMapOfCode() {
		return hkIndustryLabelMap;
	}

	@PostConstruct
	public void loadIndustryInfo() {
		try {
			List<SecuMain> list = secumainService.getIndustryInfo();
			SecuMain vo = null;
			for (int i = 0; i < list.size(); i++) {
				vo = list.get(i);
				vo.setIndustryName(vo.getSecuAbbr().replace("申万", ""));
				industryMap.put(vo.getIndustryCode(), vo);
			}
		} catch (Exception e) {
			log.error("初始化行业字典出错!", e);
		}
	}

	/**
	 * @Title: loadIndustryLabel、
	 * @Description:加载行业标签数据（新）
	 * @return: void
	 */
	@PostConstruct
	public void loadIndustryLabel() {
		try {
			List<ConstantVo> list = constDaoImpl.getIndustryLabelList();
			ConstantVo vo = null;
			for (int i = 0; i < list.size(); i++) {
				vo = list.get(i);
				industryLabelMap.put(vo.getInnerCode(), vo);
				industryLabelMapOfCode.put(vo.getCode(), vo);
			}
			//模拟组合
			vo = new ConstantVo();
			vo.setCode(MeixConstants.COMB_INDUSTRY_QHY);
			vo.setName("多行业");
			vo.setInnerCode(MeixConstants.COMB_INDUSTRY_QHY);
			industryLabelMapOfCode.put(vo.getCode(), vo);
			industryLabelMap.put(vo.getInnerCode(), vo);
			vo = new ConstantVo();
			vo.setCode(MeixConstants.COMB_INDUSTRY_ZT);
			vo.setName("主题");
			vo.setInnerCode(MeixConstants.COMB_INDUSTRY_ZT);
			industryLabelMapOfCode.put(vo.getCode(), vo);
			industryLabelMap.put(vo.getInnerCode(), vo);

		} catch (Exception e) {
			log.error("初始化行业标签字典出错!", e);
		}
	}

	public ConstantVo getIndustryLabelByInnerCode(int innerCode) {
		ConstantVo vo = industryLabelMap.get(innerCode);
		return vo;
	}

	/**
	 * @param code
	 * @return
	 * @Title: convertIndustryCode、
	 * @Description:自定义行业属性对应的指数为沪深300
	 * @return: int
	 */
	public int convertIndustryCode(int code) {
		if (getIndustryMap().containsKey(code)) {
			return code;
		}

		return 0;//沪深300
	}

	public SecuMain getIndustryByCode(int code) {
		return industryMap.get(code);
	}

	/**
	 * @param code
	 * @return
	 * @Title: checkSWIndustry、
	 * @Description:检查代码是否属于申万行业
	 * @return: boolean
	 */
	public boolean checkSWIndustry(int code) {
		if (code == 0) {
			return false;
		}
		return industryMap.containsKey(code);
	}
}
