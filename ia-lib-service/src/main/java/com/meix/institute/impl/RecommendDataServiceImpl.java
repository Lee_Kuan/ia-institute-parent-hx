package com.meix.institute.impl;

import com.meix.institute.BaseService;
import com.meix.institute.api.*;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.impl.cache.ServiceCache;
import com.meix.institute.mapper.MeetingMapper;
import com.meix.institute.mapper.RecommendDataDaoImpl;
import com.meix.institute.util.*;
import com.meix.institute.vo.HoneyBeeBaseVo;
import com.meix.institute.vo.meeting.MorningMeetingVo;
import com.meix.institute.vo.report.ReportAuthorVo;
import com.meix.institute.vo.report.ReportDetailVo;
import com.meix.institute.vo.secu.SecuMainVo;
import com.meix.institute.vo.user.UserInfo;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by zenghao on 2019/10/9.
 */
@Service
public class RecommendDataServiceImpl extends BaseService implements IRecommendDataService {
	private static Logger log = LoggerFactory.getLogger(RecommendDataServiceImpl.class);
	@Autowired
	private RecommendDataDaoImpl recommendDataDao;
	@Autowired
	private ISecumainService secumainService;
	@Autowired
	private IActivityService activityService;
	@Autowired
	private IReportService reportService;
	@Autowired
	private ServiceCache serviceCache;
	@Autowired
	private MeetingMapper meetingMapper;
	@Autowired
	private IDictTagService dictTagService;

	@Override
	public Map<String, Object> getRecommendDataByIds(int dataType, List<Long> ids, long uid, Set<Integer> selfStockSet) {
		List<Map<String, Object>> list = getRecommendDataListByIds(dataType, ids, uid, selfStockSet);
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public Map<String, Map<String, Object>> getRecDataMapByIds(int dataType, List<Long> ids, long uid, Set<Integer> selfStockSet) {
		List<Map<String, Object>> list = getRecommendDataListByIds(dataType, ids, uid, selfStockSet);
		if (list == null || list.size() == 0) {
			return null;
		}
		Map<String, Map<String, Object>> map = new HashMap<>();
		for (Map<String, Object> item : list) {
			long id = MapUtils.getLongValue(item, "id", 0);
			if (id == 0) {
				continue;
			}
			map.put(dataType + "_" + id, item);
		}
		return map;
	}

	public List<Map<String, Object>> getRecommendDataListByIds(int dataType, List<Long> ids, long uid, Set<Integer> selfStockSet) {
		List<Map<String, Object>> list;
		switch (dataType) {
			case MeixConstants.USER_HD: {
				long startTime = System.currentTimeMillis();
				list = recommendDataDao.getRecommendDataListByIds("recommendDataMapper.getActivityListByIds", ids);
				if (list == null || list.size() == 0) {
					return null;
				}
				for (Map<String, Object> vo : list) {
					try {
						fillActivityDetail(vo);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				log.info("查询活动{}信息耗时{}ms", ids, (System.currentTimeMillis() - startTime));
				return list;
			}
			case MeixConstants.USER_YB: {
				long startTime = System.currentTimeMillis();
				list = getReportInfo(ids, uid, selfStockSet);
				if (list == null || list.size() == 0) {
					return null;
				}
				log.info("查询研报{}信息耗时{}ms", ids, (System.currentTimeMillis() - startTime));
				return list;
			}
			case MeixConstants.USER_CH: {
				list = getMorningMeetingDetail(ids);
				return list;
			}
		}
		return null;
	}

	private List<Map<String, Object>> getMorningMeetingDetail(List<Long> ids) {
		List<MorningMeetingVo> list = meetingMapper.getMeetingDetailList(ids);
		if (list == null || list.size() == 0) {
			return null;
		}
		List<Map<String, Object>> resultList = new ArrayList<>();
		for (MorningMeetingVo morningMeetingVo : list) {
			HoneyBeeBaseVo vo = new HoneyBeeBaseVo();
			vo.setDataId(morningMeetingVo.getId());
			vo.setDataType(MeixConstants.USER_CH);
			vo.setApplyTime(DateUtil.getCurrentDateTime());
			vo.setStatus(1);
			Map<String, Object> item = GsonUtil.json2Map(GsonUtil.obj2Json(morningMeetingVo));
			resultList.add(item);
		}
		return resultList;
	}

	/**
	 * @date 2016年1月7日上午11:15:52
	 * @Description: Activity  to ActivityVo
	 */
	private void fillActivityDetail(Map<String, Object> vo) {
		int activityType = MapUtils.getIntValue(vo, "activityType", 0);
		String researchObject = MapUtils.getString(vo, "researchObject");
		List<Map<String, Object>> nameList = new ArrayList<>();
		String checkStr = "name:";
		vo.remove("buyer");
		if (researchObject == null) {
			vo.put("researchObject", "");
		}
		vo.put("authorHeadImgUrl", OssResourceUtil.getHeadImage(MapUtils.getString(vo, "authorHeadImgUrl")));

		/******* 分析师/发起人 *******/
		if (vo.get("analystList") != null) {
			String analystList = (String) vo.get("analystList");
			String[] analyst = analystList.split(",");
			Map<String, Object> analystMap = null;
			List<Long> analystId = null;
			for (String uid : analyst) {
				UserInfo user = null;
				//字符串与数据库定义对应
				//手动输入的分析师
				if (uid.contains(checkStr)) {
					user = new UserInfo();
					user.setUserName(uid.substring(uid.indexOf(checkStr) + checkStr.length()));
					user.setId(0);
				} else {
					//系统中的
					user = getUserInfo(Long.valueOf(uid));
				}
				analystMap = new HashMap<>();
				analystMap.put("name", user.getUserName());
				analystMap.put("code", user.getId());
				analystMap.put("headImageUrl", OssResourceUtil.getHeadImage(user.getHeadImageUrl()));
				nameList.add(analystMap);
				if (user.getId() != 0) {
					if (analystId == null) {
						analystId = new ArrayList<>();
					}
					analystId.add(user.getId());
				}
			}
			vo.put("analyst", nameList);
			if (analystId != null) {
				vo.put("analystId", GsonUtil.obj2Json(analystId));
			}
		}

		/****** 上市公司、行业 ******/
		List<Map<String, Object>> secuMapList = new ArrayList<>();
		String target = "";
		//宏观、策略、事件 转换为 上证指数
		if (MeixConstants.CHAT_MESSAGE_DY != activityType && !StringUtils.isNullorWhiteSpace(researchObject)) {
			secuMapList.add(convertSecuInfo(MeixConstants.RELATION_CODE_HG, researchObject, activityType));
		}
		if (vo.get("targetList") != null) {
			target = (String) vo.get("targetList");
		}
		String[] targetCode = target.split(",");
		int count = 0;
		for (String code : targetCode) {
			if (count > 9)
				break;
			if (code == null || "".equals(code)) {
				continue;
			}
			secuMapList.add(convertSecuInfo(Integer.valueOf(code), null, activityType));
			count++;
		}
		vo.put("secuList", secuMapList);
		if (secuMapList.size() > 0) {
			int innerCode = MapUtils.getIntValue(secuMapList.get(0), "innerCode", 0);
			if (innerCode != 0) {
				vo.put("innerCode", innerCode);
			}
		}
		String endTime = MapUtils.getString(vo, "endTime");
		try {
			//对于有录音文件的电话会议，根据会议起始时间设置电话会议的状态
			vo.put("isEnd", activityService.getActivityStatus(activityType,
				MapUtils.getIntValue(vo, "isEnd", 0),
				MapUtils.getIntValue(vo, "endStatus", 0),
				DateUtil.stringToDate((String) vo.get("startTime")),
				DateUtil.stringToDate(endTime)));
		} catch (Exception e) {
			log.error("报错数据：{}", GsonUtil.obj2Json(vo));
			log.error("", e);
		}
		switch (activityType) {
			case MeixConstants.CHAT_MESSAGE_DY:
				if (!StringUtils.isNullorWhiteSpace(researchObject)) {
					vo.put("subtitle", "调研对象：" + researchObject);
				}
				break;
			case MeixConstants.CHAT_MESSAGE_LY:
				if (nameList.size() > 0) {
					String analystName = MapUtils.getString(nameList.get(0), "name");
					if (!StringUtils.isNullorWhiteSpace(analystName)) {
						vo.put("subtitle", "分析师：" + analystName);
					}
				}
				break;
			case MeixConstants.CHAT_MESSAGE_CL:
				String startTime = MapUtils.getString(vo, "startTime");
				if (!StringUtils.isNullorWhiteSpace(startTime, endTime)) {
					try {
						Date startDate = DateUtil.stringToDate(startTime);
						Date endDate = DateUtil.stringToDate(endTime);
						long second = (endDate.getTime() - startDate.getTime()) / 1000;
						long day = second / 86400;
						if (second % 86400 > 0) {
							day += 1;
						}
						if (day > 0) {
							vo.put("subtitle", "会议时间：" + day + "天");
						}
					} catch (Exception e) {
						log.error("", e);
					}
				}
				break;
			default:
				break;
		}
	}

	/**
	 * @date 2016年1月14日下午7:06:11
	 * @Description: 缓存中取股票行业信息
	 */
	private Map<String, Object> convertSecuInfo(int code, String researchObject, int activityType) {
		Map<String, Object> secuMap = new HashMap<>();
		SecuMain secuMain = secumainService.getSecuMain(code);
		if (secuMain == null || code == 1) {
			secuMap.put("innerCode", code);
			secuMap.put("industryCode", 0);
			secuMap.put("secuAbbr", "全行业");
			secuMap.put("relationType", 3);
			secuMap.put("secuClass", 4);
			return secuMap;
		}
		secuMap.put("innerCode", code);
		secuMap.put("industryCode", secuMain.getIndustryCode());
		secuMap.put("secuClass", secuMain.getSecuCategory());
		if (activityType != MeixConstants.CHAT_MESSAGE_DY && researchObject != null && !"".equals(researchObject)) {
			secuMap.put("secuAbbr", researchObject);
		} else {
			secuMap.put("secuCode", secuMain.getSecuCode() + secuMain.getSuffix());
			secuMap.put("secuAbbr", secuMain.getSecuAbbr());
		}

		if (secuMain.getInnerCode() == 1) {
			secuMap.put("relationType", 3);
		} else if (secuMain.getSecuCategory() == 4 && secuMain.getIndustryCode() > 0 || secuMain.getSecuCode().equals("000300")) {
			secuMap.put("relationType", 2);
		} else {
			secuMap.put("relationType", 1);
		}

		return secuMap;
	}

	private List<Map<String, Object>> getReportInfo(List<Long> idList, long uid, Set<Integer> selfStockSet) {
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String, Object> po = null;
		ReportDetailVo vo = null;
		SecuMain secu = null;
		//获取研报数据
		for (int i = 0; i < idList.size(); i++) {
			vo = reportService.getReportDetail(idList.get(i), uid);
			if (vo == null) {
				log.info("没有命中研报缓存:" + idList.get(i));
				continue;
			}
			po = new HashMap<>();
			List<ReportAuthorVo> authorList = vo.getAuthorList();
			for (ReportAuthorVo author : authorList) {
				if (author.getIsMeixUser() == 1 && author.getFollowFlag() == 1) {
					po.put("from", "我关注的作者");
				}
			}
			List<SecuMainVo> secuList = vo.getSecu();
			for (SecuMainVo secuMainVo : secuList) {
				int innerCode = secuMainVo.getInnerCode();
				if (selfStockSet != null && selfStockSet.contains(innerCode)) {
					po.put("from", "我的自选股");
				}
			}

			Map<String, Object> report = new HashMap<>();

			//redis 没有对应关系的作者id
			report.put("author", getReportAuthorList(vo));

			List<ReportAuthorVo> auths = vo.getAuthorList();
			if (vo.getAuthorList().size() > 0) {
				boolean hadAuthor = false;
				for (ReportAuthorVo a : auths) {
					if (a.getAuthorCode() > 0 && a.getIsMeixUser() == 1) {
						hadAuthor = true;
						po.put("authorName", a.getAuthorName());
						po.put("authorCode", a.getAuthorCode());
						po.put("authorHeadImgUrl", a.getAuthorHeadImgUrl());
						po.put("followFlag", a.getFollowFlag());
						break;
					}
				}
				if (!hadAuthor) {
					po.put("authorName", auths.get(0).getAuthorName());
					po.put("authorCode", 0);
					po.put("authorHeadImgUrl", "");
				}

			} else {
				po.put("authorName", "");
				po.put("authorCode", 0L);
				po.put("authorHeadImgUrl", "");
			}
			po.put("title", vo.getTitle().replaceAll("（", "（").replaceAll("）", ")"));

			Map<String, Object> secuReport = null;
			Map<String, Object> industryReport = null;
			List<Map<String, Object>> secuReportList = new ArrayList<>();
			List<Map<String, Object>> industryReportList = new ArrayList<>();
			Map<Integer, Integer> checkMap = new HashMap<>();
			SecuMainVo secuActivityVo = null;
			if (vo.getSecuList() != null && vo.getSecuList().size() > 0) {
				secuActivityVo = vo.getSecuList().get(0);
				for (SecuMainVo secuMap : vo.getSecuList()) {
					if (!checkMap.containsKey(secuMap.getInnerCode())) {
						secuReport = new HashMap<>();
						secuReport.put("innerCode", secuMap.getInnerCode());
						secuReport.put("secuAbbr", secuMap.getSecuAbbr());
						secuReport.put("secuCode", secuMap.getSecuCode());
						secuReportList.add(secuReport);
						checkMap.put(secuMap.getInnerCode(), 0);
					}
				}
			}
			po.put("innerCode", secuActivityVo == null ? 0 : secuActivityVo.getInnerCode());
			po.put("secuAbbr", secuActivityVo == null ? "" : secuActivityVo.getSecuAbbr());
			po.put("secuCode", secuActivityVo == null ? "" : secuActivityVo.getSecuCode());
			List<SecuMainVo> industryList = vo.getIndustry();
			//行业代码
			if ((int) po.get("innerCode") == 0 && industryList.size() > 0) {
				secu = serviceCache.getIndustryByCode(industryList.get(0).getIndustryCode());
				if (secu != null) {
					po.put("innerCode", secu.getInnerCode());
					po.put("secuAbbr", secu.getSecuAbbr());
					po.put("secuCode", secu.getSecuCode());
				}

				checkMap = new HashMap<>();
				for (int j = 0; j < industryList.size(); j++) {
					secu = serviceCache.getIndustryByCode(industryList.get(j).getIndustryCode());
					if (!checkMap.containsKey(secu.getInnerCode())) {
						industryReport = new HashMap<>();
						industryReport.put("innerCode", secu.getInnerCode());
						industryReport.put("secuCode", secu.getSecuCode());
						industryReport.put("secuAbbr", secu.getSecuAbbr());
						industryReportList.add(industryReport);
						checkMap.put(secu.getInnerCode(), 0);
					}
				}
			}
			po.put("id", vo.getId());
			po.put("reportId", String.valueOf(vo.getId()));
			po.put("evaluateDesc", vo.getInfoGrade());
			po.put("evaluateState", 0);
			po.put("infoDate", vo.getInfoDate());
			po.put("orgCode", vo.getOrgCode());
			po.put("orgName", vo.getOrgName());

			report.put("industry", industryReportList);
			report.put("secu", secuReportList);

			String ybfl = dictTagService.getText(MeixConstants.REPORT_TYPE, vo.getInfoType());
			List<String> typeList = new ArrayList<>();
			if (StringUtil.isNotEmpty(vo.getInfoType())) {
				typeList.add(ybfl);
			}
			po.put("yjfl", typeList);
			po.put("infoSubType", vo.getInfoSubType());
			po.put("report", report);
			po.put("ybfl", ybfl);
			po.put("picture", vo.getPicture());
			vo.setInfoType(ybfl);

			list.add(po);
		}
		return list;
	}

	public List<Map<String, Object>> getReportAuthorList(ReportDetailVo vo) {
		List<ReportAuthorVo> authorList = vo.getAuthorList();
		List<Map<String, Object>> author = new ArrayList<>();
		for (ReportAuthorVo temp : authorList) {
			Map<String, Object> data = new HashMap<>();
			if (temp.getIsMeixUser() == 1) {
				data.put("id", temp.getAuthorCode());
			} else {
				data.put("id", 0);
			}
			data.put("name", temp.getAuthorName());
			data.put("followFlag", temp.getFollowFlag());
			data.put("authorHeadImgUrl", temp.getAuthorHeadImgUrl());
			author.add(data);
		}

		return author;
	}
}
