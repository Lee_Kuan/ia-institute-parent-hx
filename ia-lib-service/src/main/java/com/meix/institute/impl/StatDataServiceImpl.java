package com.meix.institute.impl;

import com.meix.institute.api.*;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsActivity;
import com.meix.institute.entity.InsUser;
import com.meix.institute.search.ClueSearchVo;
import com.meix.institute.search.DataRecordStatSearchVo;
import com.meix.institute.search.UserRecordSearchVo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.company.CompanyClueInfo;
import com.meix.institute.vo.company.CompanyClueUserStat;
import com.meix.institute.vo.stat.DataRecordStatVo;
import com.meix.institute.vo.stat.HeatDataVo;
import com.meix.institute.vo.user.UserClueStat;
import com.meix.institute.vo.user.UserInfo;
import com.meix.institute.vo.user.UserRecordStatVo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by zenghao on 2019/8/2.
 */
@Service
public class StatDataServiceImpl implements IStatDataService {
	private static Logger log = LoggerFactory.getLogger(StatDataServiceImpl.class);
	@Autowired
	private IDataStatDao dataStatDaoImpl;

	@Autowired
	private IUserService userService;
	@Autowired
	private IInstituteService instituteServiceImpl;
	@Autowired
	private IActivityService activityService;
	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IInsUserService insUserService;

	@Override
	public void executeActivityRecordStatJob() {
		MeixConstants.serverThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				List<DataRecordStatVo> activityList = dataStatDaoImpl.getAllInsActivityList();
				if (activityList == null || activityList.size() == 0) {
					return;
				}
				for (DataRecordStatVo item : activityList) {
					try {
						long readNum = dataStatDaoImpl.getDataReadStat(item.getDataId(), item.getDataType());
						item.setReadNum(readNum);
						long shareNum = dataStatDaoImpl.getDataShareStat(item.getDataId(), item.getDataType(), item.getActivityType());
						item.setShareNum(shareNum);
//						//没有阅读分享数据时不保存记录
//						if (readNum == 0 && shareNum == 0) {
//							continue;
//						}
						long readDuration = dataStatDaoImpl.getReadDuration(item.getDataId(), item.getDataType());
						item.setDuration(readDuration);
						DataRecordStatVo exist = dataStatDaoImpl.getExistDataRecordStat(item.getCompanyCode(), item.getDataId(), item.getDataType());
						if (exist == null) {
							dataStatDaoImpl.saveDataRecordStat(item);
						} else {
							item.setId(exist.getId());
							dataStatDaoImpl.updateDataRecordStat(item);
						}
						Thread.sleep(1);
					} catch (Exception e) {
						log.error("", e);
					}
				}
				log.info("定时统计电话会议的用户行为数耗时{}ms", (System.currentTimeMillis() - startTime));
			}
		});
	}

	@Override
	public void executeUserRecordStatJob() {
		MeixConstants.serverThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				List<DataRecordStatVo> activityList = dataStatDaoImpl.getAllInsActivityList();
				if (activityList == null || activityList.size() == 0) {
					return;
				}
				Map<Long, UserInfo> userMap = new HashMap<>();
				Map<Long, InsActivity> activityMap = new HashMap<>();
				Set<Long> uidSet = new HashSet<>();
				for (DataRecordStatVo item : activityList) {
					try {
						uidSet.clear();
						List<UserRecordStatVo> statList = dataStatDaoImpl.getUserRecordList(item.getDataId(), item.getDataType(), item.getActivityType());
						if (statList != null && statList.size() > 0) {
							for (UserRecordStatVo stat : statList) {
								UserInfo userInfo;
								if (userMap.containsKey(stat.getUid())) {
									userInfo = userMap.get(stat.getUid());
								} else {
									userInfo = userService.getUserInfo(stat.getUid(), null, null);
									if (userInfo != null) {
										userMap.put(stat.getUid(), userInfo);
									}
								}
								if (userInfo == null) {
									continue;
								}
								uidSet.add(userInfo.getId());
								InsActivity activity;
								if (activityMap.containsKey(item.getDataId())) {
									activity = activityMap.get(item.getDataId());
								} else {
									activity = activityService.getBaseActivityById(item.getDataId());
									activityMap.put(item.getDataId(), activity);
								}
								if (activity == null) {
									continue;
								}

								stat.setInsCompanyCode(activity.getCompanyCode());
								stat.setDataId(item.getDataId());
								stat.setDataType(item.getDataType());
								stat.setUserName(userInfo.getUserName());
								stat.setMobile(userInfo.getMobile());
								stat.setPosition(userInfo.getPosition());
								stat.setCompanyCode(userInfo.getCompanyCode());
								stat.setCompanyAbbr(userInfo.getCompanyAbbr());
								UserRecordStatVo exist = dataStatDaoImpl.getExistUserRecordStat(stat.getUid(), item.getDataId(), item.getDataType());
								if (exist == null) {
									dataStatDaoImpl.saveUserRecordStat(stat);
								} else {
									stat.setId(exist.getId());
									dataStatDaoImpl.updateUserRecordStat(stat);
								}
								Thread.sleep(1);
							}
						}
						//删除无阅读记录的用户统计数据（防止行为数据删除后统计数据不准）
						dataStatDaoImpl.removeUserRecordStat(item.getDataType(), item.getDataId(), uidSet);
					} catch (Exception e) {
						log.error("", e);
					}
				}
				userMap.clear();
				activityMap.clear();
				log.info("定时统计用户行为数耗时{}ms", (System.currentTimeMillis() - startTime));
			}
		});
	}

	@Override
	public void executeResearchReportStatJob() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2011);
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.DATE, 1);
		String startDate = DateUtil.dateToStr(calendar.getTimeInMillis(), "yyyy-MM-dd");
		calendar.add(Calendar.MONTH, 1);
		String endDate = DateUtil.dateToStr(calendar.getTimeInMillis(), "yyyy-MM-dd");
		List<Long> ridList = dataStatDaoImpl.getInsReportIdList(startDate, endDate);
		String curDate = DateUtil.getCurrentDate();
		while (curDate.compareTo(startDate) > 0) {

			if (ridList != null && ridList.size() > 0) {
				List<Long> idList = new ArrayList<>(ridList.size());
				idList.addAll(ridList);
				log.info("{}至{}研报数量{}", startDate, endDate, idList.size());

				MeixConstants.serverThreadPool.execute(new Runnable() {
					@Override
					public void run() {
						long startTime = System.currentTimeMillis();
						Map<Long, UserInfo> userMap = new HashMap<>();
						Set<Long> uidSet = new HashSet<>();
						for (long rid : idList) {
							try {
								uidSet.clear();
								DataRecordStatVo item = dataStatDaoImpl.getInsReportByRid(rid);
								if (item == null) {
									continue;
								}
								long readNum = dataStatDaoImpl.getDataReadStat(item.getDataId(), item.getDataType());
								item.setReadNum(readNum);
								long shareNum = dataStatDaoImpl.getDataShareStat(item.getDataId(), item.getDataType(), item.getActivityType());
								item.setShareNum(shareNum);
								long duration = dataStatDaoImpl.getReadDuration(item.getDataId(), item.getDataType());
								item.setDuration(duration);
//								//没有阅读分享数据时不保存记录
//								if (readNum == 0 && shareNum == 0) {
//									continue;
//								}
								DataRecordStatVo exist = dataStatDaoImpl.getExistDataRecordStat(item.getCompanyCode(), item.getDataId(), item.getDataType());
								if (exist == null) {
									dataStatDaoImpl.saveDataRecordStat(item);
								} else {
									item.setId(exist.getId());
									dataStatDaoImpl.updateDataRecordStat(item);
								}

								List<UserRecordStatVo> statList = dataStatDaoImpl.getUserRecordList(item.getDataId(), item.getDataType(), item.getActivityType());
								if (statList != null && statList.size() > 0) {
									for (UserRecordStatVo stat : statList) {
										UserInfo userInfo;
										if (userMap.containsKey(stat.getUid())) {
											userInfo = userMap.get(stat.getUid());
										} else {
											userInfo = userService.getUserInfo(stat.getUid(), null, null);
											if (userInfo != null) {
												userMap.put(stat.getUid(), userInfo);
											}
										}
										if (userInfo == null) {
											continue;
										}
										uidSet.add(userInfo.getId());
										stat.setInsCompanyCode(item.getCompanyCode());
										stat.setDataId(item.getDataId());
										stat.setDataType(item.getDataType());
										stat.setUserName(userInfo.getUserName());
										stat.setMobile(userInfo.getMobile());
										stat.setPosition(userInfo.getPosition());
										stat.setCompanyCode(userInfo.getCompanyCode());
										stat.setCompanyAbbr(userInfo.getCompanyAbbr());
										UserRecordStatVo existUserRecordStat = dataStatDaoImpl.getExistUserRecordStat(stat.getUid(), item.getDataId(), item.getDataType());
										if (existUserRecordStat == null) {
											dataStatDaoImpl.saveUserRecordStat(stat);
										} else {
											stat.setId(existUserRecordStat.getId());
											dataStatDaoImpl.updateUserRecordStat(stat);
										}
										Thread.sleep(1);
									}
								}
								//删除无阅读记录的用户统计数据（防止行为数据删除后统计数据不准）
								dataStatDaoImpl.removeUserRecordStat(item.getDataType(), item.getDataId(), uidSet);
							} catch (Exception e) {
								log.error("", e);
							}
						}
						userMap.clear();
						idList.clear();
						log.info("定时统计研报的用户行为数耗时{}ms", (System.currentTimeMillis() - startTime));
					}
				});
			}

			if (ridList != null) {
				ridList.clear();
			}

			startDate = endDate;
			calendar.add(Calendar.MONTH, 1);
			endDate = DateUtil.dateToStr(calendar.getTimeInMillis(), "yyyy-MM-dd");
			ridList = dataStatDaoImpl.getInsReportIdList(startDate, endDate);
		}
	}

	@Override
	public void executeInsSubscribeAnalystJob() {
		MeixConstants.serverThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				List<CompanyClueInfo> subscribeList = dataStatDaoImpl.getSubscribeAnalystRecordList(commonServerProperties.getCompanyCode());
				if (subscribeList == null || subscribeList.size() == 0) {
					return;
				}
				//按研究所和用户公司分组
				Map<String, Integer> subscribeCountMap = new HashMap<>();//公司订阅研究所次数，key：CompanyCode_CustomerCompanyCode
				Map<String, List<CompanyClueInfo>> clueInfoMap = new HashMap<>();//公司订阅研究所记录，key：CompanyCode_CustomerCompanyCode
//				Map<Long, String> loginTimeMap = new HashMap<>();
				for (CompanyClueInfo item : subscribeList) {
					//设置最后登录信息
//					if (!loginTimeMap.containsKey(item.getUid())) {
//						String loginTime = userSettingCache.getLoginTime(item.getUid());
//						loginTimeMap.put(item.getUid(), loginTime);
//					}
//					item.setLatestLoginTime(loginTimeMap.get(item.getUid()));

					String key = item.getCompanyCode() + "_" + item.getCustomerCompanyCode();
					if (subscribeCountMap.containsKey(key)) {
						int count = subscribeCountMap.get(key);
						count += 1;
						subscribeCountMap.put(key, count);
						List<CompanyClueInfo> recordList = clueInfoMap.get(key);
						recordList.add(item);
					} else {
						subscribeCountMap.put(key, 1);
						List<CompanyClueInfo> recordList = new ArrayList<>();
						recordList.add(item);
						clueInfoMap.put(key, recordList);
					}
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						log.error("", e);
					}
				}
				Set<Map.Entry<String, Integer>> entrySet = subscribeCountMap.entrySet();
				Iterator<Map.Entry<String, Integer>> iterator = entrySet.iterator();
				while (iterator.hasNext()) {
					Map.Entry<String, Integer> entry = iterator.next();
					int value = entry.getValue();
					if (value < 3) {
						iterator.remove();//删除小于3次的机构
						clueInfoMap.remove(entry.getKey());
					} else {
						CompanyClueInfo latestRecord = clueInfoMap.get(entry.getKey()).get(0);
						latestRecord.setClueType(MeixConstants.SUBSCRIBE_ANALYST_3);
						latestRecord.setIssueType(1);
						CompanyClueInfo exist = dataStatDaoImpl.getExistClue(latestRecord.getCompanyCode(),
							latestRecord.getCustomerCompanyCode(), latestRecord.getClueType(), latestRecord.getLatestRecordTime());
						boolean newClue = false;
						if (exist == null) {
							latestRecord.setUuid(StringUtils.getId24());
							dataStatDaoImpl.saveClue(latestRecord);
							exist = latestRecord;
							newClue = true;
						} else {
							//没有新纪录生成，不操作数据
							if (latestRecord.getLatestRecordTime().equals(exist.getLatestRecordTime())) {
								continue;
							}
							dataStatDaoImpl.updateClue(latestRecord);
						}
						List<CompanyClueInfo> detailList = clueInfoMap.get(entry.getKey());
						saveClueUserStat(exist.getUuid(), detailList);
						try {
							if (newClue) {
								instituteServiceImpl.sendNewClueNotice(latestRecord);
							}
						} catch (Exception e) {
							log.error("", e);
						}
					}
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						log.error("", e);
					}
				}
				log.info("定时统计关注分析师商机耗时{}ms", (System.currentTimeMillis() - startTime));
			}
		});
	}

	private void saveClueUserStat(String clueUuid, List<CompanyClueInfo> detailList) {
		//按用户分组，统计次数
		Map<Long, CompanyClueUserStat> userStatMap = new HashMap<>();
		List<CompanyClueUserStat> statList = new ArrayList<>();
		for (CompanyClueInfo item : detailList) {
			if (userStatMap.containsKey(item.getUid())) {
				CompanyClueUserStat stat = userStatMap.get(item.getUid());
				long times = stat.getTimes() + 1;
				stat.setTimes(times);
			} else {
				CompanyClueUserStat stat = new CompanyClueUserStat();
				BeanUtils.copyProperties(item, stat);
				stat.setClueUuid(clueUuid);
				stat.setTimes(1);
				stat.setLatestRecordTime(item.getLatestRecordTime());
				stat.setClueType(MeixConstants.SUBSCRIBE_ANALYST_3);
				userStatMap.put(item.getUid(), stat);
				statList.add(stat);
			}
		}
		dataStatDaoImpl.removeClueUserStat(clueUuid);
		dataStatDaoImpl.insertClueUserStatList(statList);
	}

	@Override
	public void executeInsJoinActivityJob() {
		MeixConstants.serverThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				List<CompanyClueInfo> list = dataStatDaoImpl.getJoinActivityRecordList(commonServerProperties.getCompanyCode());
				if (list == null || list.size() == 0) {
					return;
				}
//				Map<Long, String> loginTimeMap = new HashMap<>();
				for (CompanyClueInfo item : list) {
					//设置最后登录信息
//					if (!loginTimeMap.containsKey(item.getUid())) {
//						String loginTime = userSettingCache.getLoginTime(item.getUid());
//						loginTimeMap.put(item.getUid(), loginTime);
//					}
//					item.setLatestLoginTime(loginTimeMap.get(item.getUid()));

					item.setClueType(MeixConstants.JOIN_ACTIVITY_3);
					item.setIssueType(1);
					CompanyClueInfo exist = dataStatDaoImpl.getExistClue(item.getCompanyCode(),
						item.getCustomerCompanyCode(), item.getClueType(), item.getLatestRecordTime());
					boolean newClue = false;
					if (exist == null) {
						item.setUuid(StringUtils.getId24());
						dataStatDaoImpl.saveClue(item);
						exist = item;
						newClue = true;
					} else {
						//没有新纪录生成，不操作数据
						if (item.getLatestRecordTime().equals(exist.getLatestRecordTime())) {
							continue;
						}
						dataStatDaoImpl.updateClue(item);
					}
					List<CompanyClueUserStat> detailList = dataStatDaoImpl.getJoinActivityRecordDetailList(exist.getCompanyCode(), exist.getCustomerCompanyCode());
					for (CompanyClueUserStat detail : detailList) {
						//设置最后登录信息
//						if (!loginTimeMap.containsKey(detail.getUid())) {
//							String loginTime = userSettingCache.getLoginTime(detail.getUid());
//							loginTimeMap.put(detail.getUid(), loginTime);
//						}
//						detail.setLatestLoginTime(loginTimeMap.get(detail.getUid()));

						detail.setClueUuid(exist.getUuid());
						detail.setClueType(MeixConstants.JOIN_ACTIVITY_3);
					}
					dataStatDaoImpl.removeClueUserStat(exist.getUuid());
					dataStatDaoImpl.insertClueUserStatList(detailList);
					try {
						if (newClue) {
							instituteServiceImpl.sendNewClueNotice(item);
						}
					} catch (Exception e) {
						log.error("", e);
					}
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						log.error("", e);
					}
				}
				log.info("定时统计参加活动商机耗时{}ms", (System.currentTimeMillis() - startTime));
			}
		});
	}

	@Override
	public void executeInsListenActivityJob() {
		MeixConstants.serverThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				List<CompanyClueInfo> list = dataStatDaoImpl.getListenTeleActivityList();
				if (list == null || list.size() == 0) {
					return;
				}
//				Map<Long, String> loginTimeMap = new HashMap<>();
				for (CompanyClueInfo item : list) {
					//设置最后登录信息
//					if (!loginTimeMap.containsKey(item.getUid())) {
//						String loginTime = userSettingCache.getLoginTime(item.getUid());
//						loginTimeMap.put(item.getUid(), loginTime);
//					}
//					item.setLatestLoginTime(loginTimeMap.get(item.getUid()));

					item.setClueType(MeixConstants.LISTEN_ACTIVITY_3);
					item.setIssueType(1);
					CompanyClueInfo exist = dataStatDaoImpl.getExistClue(item.getCompanyCode(),
						item.getCustomerCompanyCode(), item.getClueType(), item.getLatestRecordTime());
					boolean newClue = false;
					if (exist == null) {
						item.setUuid(StringUtils.getId24());
						dataStatDaoImpl.saveClue(item);
						exist = item;
						newClue = true;
					} else {
						//没有新纪录生成，不操作数据
						if (item.getLatestRecordTime().equals(exist.getLatestRecordTime())) {
							continue;
						}
						dataStatDaoImpl.updateClue(item);
					}
					List<CompanyClueUserStat> detailList = dataStatDaoImpl.getListenTeleActivityDetailList(exist.getCompanyCode(), exist.getCustomerCompanyCode());
					for (CompanyClueUserStat detail : detailList) {
						//设置最后登录信息
//						if (!loginTimeMap.containsKey(detail.getUid())) {
//							String loginTime = userSettingCache.getLoginTime(detail.getUid());
//							loginTimeMap.put(detail.getUid(), loginTime);
//						}
//						detail.setLatestLoginTime(loginTimeMap.get(detail.getUid()));

						detail.setClueUuid(exist.getUuid());
						detail.setClueType(MeixConstants.LISTEN_ACTIVITY_3);
					}
					dataStatDaoImpl.removeClueUserStat(exist.getUuid());
					dataStatDaoImpl.insertClueUserStatList(detailList);
					try {
						if (newClue) {
							instituteServiceImpl.sendNewClueNotice(item);
						}
					} catch (Exception e) {
						log.error("", e);
					}
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						log.error("", e);
					}
				}
				log.info("定时统计收听电话会议商机耗时{}ms", (System.currentTimeMillis() - startTime));
			}
		});
	}

	@Override
	public void executeInsReadResearchJob() {
		MeixConstants.serverThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				List<CompanyClueInfo> list = dataStatDaoImpl.getReadResearchReportList();
				if (list == null || list.size() == 0) {
					return;
				}
//				Map<Long, String> loginTimeMap = new HashMap<>();
				for (CompanyClueInfo item : list) {
					//设置最后登录信息
//					if (!loginTimeMap.containsKey(item.getUid())) {
//						String loginTime = userSettingCache.getLoginTime(item.getUid());
//						loginTimeMap.put(item.getUid(), loginTime);
//					}
//					item.setLatestLoginTime(loginTimeMap.get(item.getUid()));

					item.setClueType(MeixConstants.READ_RESEARCH_REPORT_5);
					item.setIssueType(1);
					CompanyClueInfo exist = dataStatDaoImpl.getExistClue(item.getCompanyCode(),
						item.getCustomerCompanyCode(), item.getClueType(), item.getLatestRecordTime());
					boolean newClue = false;
					if (exist == null) {
						item.setUuid(StringUtils.getId24());
						dataStatDaoImpl.saveClue(item);
						exist = item;
						newClue = true;
					} else {
						//没有新纪录生成，不操作数据
						if (item.getLatestRecordTime().equals(exist.getLatestRecordTime())) {
							continue;
						}
						dataStatDaoImpl.updateClue(item);
					}
					List<CompanyClueUserStat> detailList = dataStatDaoImpl.getReadReportDetailList(exist.getCompanyCode(), exist.getCustomerCompanyCode());
					for (CompanyClueUserStat detail : detailList) {
						//设置最后登录信息
//						if (!loginTimeMap.containsKey(detail.getUid())) {
//							String loginTime = userSettingCache.getLoginTime(detail.getUid());
//							loginTimeMap.put(detail.getUid(), loginTime);
//						}
//						detail.setLatestLoginTime(loginTimeMap.get(detail.getUid()));

						detail.setClueUuid(exist.getUuid());
						detail.setClueType(MeixConstants.READ_RESEARCH_REPORT_5);
					}
					dataStatDaoImpl.removeClueUserStat(exist.getUuid());
					dataStatDaoImpl.insertClueUserStatList(detailList);
					try {
						if (newClue) {
							instituteServiceImpl.sendNewClueNotice(item);
						}
					} catch (Exception e) {
						log.error("", e);
					}
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						log.error("", e);
					}
				}
				log.info("定时统计阅读研报商机耗时{}ms", (System.currentTimeMillis() - startTime));
			}
		});
	}

	@Override
	public int insertClueUserStatList(List<CompanyClueUserStat> list) {
		return dataStatDaoImpl.insertClueUserStatList(list);
	}

	@Override
	public int removeClueUserStatRange(long startId, long endId) {
		return dataStatDaoImpl.removeClueUserStatRange(startId, endId);
	}

	@Override
	public List<DataRecordStatVo> getDataRecordStatList(DataRecordStatSearchVo statSearchVo) {
		List<DataRecordStatVo> list = dataStatDaoImpl.getDataRecordStatList(statSearchVo);
		return list;
	}

	@Override
	public long getDataRecordStatCount(DataRecordStatSearchVo statSearchVo) {
		return dataStatDaoImpl.getDataRecordStatCount(statSearchVo);
	}

	@Override
	public List<UserRecordStatVo> getUserRecordStatList(UserRecordSearchVo searchVo) {
		List<UserRecordStatVo> list = dataStatDaoImpl.getUserRecordStatList(searchVo);
		return list;
	}

	@Override
	public long getUserRecordStatCount(UserRecordSearchVo searchVo) {
		return dataStatDaoImpl.getUserRecordStatCount(searchVo);
	}

	@Override
	public List<CompanyClueInfo> getClueList(ClueSearchVo searchVo) {
		List<CompanyClueInfo> list = dataStatDaoImpl.getClueList(searchVo);
		for (CompanyClueInfo item : list) {
			//查询白名单表，存在则为白名单用户，不存在则为潜在用户
			InsUser saler = insUserService.getUserInfo(item.getSalerId());
//			UserInfo saler = userService.getUserInfo(0, null, item.getMobile());
			if (saler != null) {
//				item.setSalerId(saler.getSalerUid());
				item.setSalerName(saler.getUserName());
			}
			List<CompanyClueUserStat> userStatList = dataStatDaoImpl.getClueUserStatList(item.getUuid());
			if (userStatList == null) {
				item.setUserStatList(Collections.emptyList());
			} else {
				for (CompanyClueUserStat userStat : userStatList) {
//					int dataType = userStat.getDataType();
//					long dataId = userStat.getDataId();
					String clueAction = MeixConstants.getClueAction(userStat.getClueType());
//					if (MeixConstants.WHITE_LIST_REQ == userStat.getClueType()) {
//						switch (dataType) {
//							case MeixConstants.USER_YB: {
//								ReportDetailVo reportDetailVo = reportService.getReportDetail(dataId);
//								if (reportDetailVo != null && StringUtil.isNotEmpty(reportDetailVo.getTitle())) {
//									clueAction = "申请研报《" + reportDetailVo.getTitle() + "》权限";
//								}
//								break;
//							}
//							case MeixConstants.USER_HD: {
//								InsActivity insActivity = activityService.getBaseActivityById(dataId);
//								if (insActivity != null && StringUtil.isNotEmpty(insActivity.getTitle())) {
//									String activityType = MeixConstants.getCNActivityType(insActivity.getActivityType());
//									clueAction = "申请" + activityType + "《" + insActivity.getTitle() + "》权限";
//								}
//								break;
//							}
//							default: {
//								break;
//							}
//						}
//					}
					userStat.setClueAction(clueAction);
				}
				item.setUserStatList(userStatList);
			}
//			item.setCustomerCompanyType(MeixConstants.convertCombCompanyType(item.getCustomerCompanyType()));
			item.setCustomerCompanyType(0);
			item.setClueName(MeixConstants.getClueName(item.getClueType()));
		}
		return list;
	}

	@Override
	public long getClueCount(ClueSearchVo searchVo) {
		return dataStatDaoImpl.getClueCount(searchVo);
	}

	@Override
	public List<UserClueStat> getUserClueStatList(ClueSearchVo searchVo) {
		List<UserClueStat> list = dataStatDaoImpl.getUserClueStatList(searchVo);
		for (UserClueStat item : list) {
			//查询白名单表，存在则为白名单用户，不存在则为潜在用户
			UserInfo saler = userService.getUserInfo(0, null, item.getMobile());
			if (saler != null) {
				item.setSalerId(saler.getSalerUid());
				item.setSalerName(saler.getUserName());
			}
			item.setCustomerCompanyType(MeixConstants.convertCombCompanyType(item.getCustomerCompanyType()));
		}
		return list;
	}

	@Override
	public long getUserClueStatCount(ClueSearchVo searchVo) {
		return dataStatDaoImpl.getUserClueStatCount(searchVo);
	}

	@Override
	public int removeHeatDataRange(long startId, long endId) {
		return dataStatDaoImpl.removeHeatDataRange(startId, endId);
	}

	@Override
	public void insertHeatDataList(List<HeatDataVo> list) {
		dataStatDaoImpl.insertHeatDataList(list);
	}

	@Override
	public List<Map<String, Object>> getHeatData(Map<String, Object> search) {
		int dataType = MapUtils.getIntValue(search, "dataType", 0);
		int type = MapUtils.getIntValue(search, "type", 1); // 默认按一个月
		List<Map<String, Object>> heatData = dataStatDaoImpl.getHeatData(search);
		if (CollectionUtils.isNotEmpty(heatData) && 2 == dataType) {
			heatData.forEach((item) -> {
				// 统计行业下的个股
				String dataCode = MapUtils.getString(item, "dataCode");
				List<Map<String, Object>> secus = dataStatDaoImpl.statisticalInnerCodeByCompsIndustry(dataCode, type);
				item.put("secus", secus);
			});
		}
		return heatData;
	}

	@Override
	public Map<String, Object> getHeatDataCount(Map<String, Object> search) {
		return dataStatDaoImpl.getHeatDataCount(search);
	}

	@Override
	public List<Map<String, Object>> getUserReadData(Map<String, Object> search) {
		return dataStatDaoImpl.getUserReadData(search);
	}

	@Override
	public Map<String, Object> getUserReadDataCount(Map<String, Object> search) {
		return dataStatDaoImpl.getUserReadDataCount(search);
	}

	@Override
	public List<Map<String, Object>> getUserReadDataByUser(Map<String, Object> search) {
		return dataStatDaoImpl.getUserReadDataByUser(search);
	}

	@Override
	public Map<String, Object> getUserReadDataByUserCount(Map<String, Object> search) {
		return dataStatDaoImpl.getUserReadDataByUserCount(search);
	}
}
