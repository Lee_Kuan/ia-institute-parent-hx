package com.meix.institute.impl;

import com.meix.institute.api.IWechatAccessTokenService;
import com.meix.institute.entity.WechatAccessToken;
import com.meix.institute.mapper.WechatAccessTokenMapper;
import com.meix.institute.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;

/**
 * Created by zenghao on 2019/9/27.
 */
@Service
public class WechatAccessTokenServiceImpl implements IWechatAccessTokenService {

	private static final int TYPE_ACCESS_TOKEN = 1;
	private static final int TYPE_JSAPI_TICKET = 2;

	@Autowired
	private WechatAccessTokenMapper wechatAccessTokenMapper;

	@Override
	public WechatAccessToken getWechatAccessToken(long companyCode, String appId) {
		Example example = new Example(WechatAccessToken.class);
		example.createCriteria().andEqualTo("type", TYPE_ACCESS_TOKEN).andEqualTo("companyCode", companyCode).andEqualTo("appId", appId);
		return wechatAccessTokenMapper.selectOneByExample(example);
	}

	@Override
	public int saveWechatAccessToken(long companyCode, String appId, String accessToken, long expiresIn, String updateTime) {
		WechatAccessToken token = new WechatAccessToken();
		token.setCompanyCode(companyCode);
		token.setAppId(appId);
		token.setAccessToken(accessToken);
		token.setType(TYPE_ACCESS_TOKEN);
		token.setExpiresIn(expiresIn);
		Date time = DateUtil.stringToDate(updateTime);
		token.setCreateTime(time);
		token.setUpdateTime(time);
		return wechatAccessTokenMapper.insert(token);
	}

	@Override
	public int updateWechatAccessToken(long companyCode, String appId, String accessToken, long expiresIn, String updateTime) {
		Example example = new Example(WechatAccessToken.class);
		example.createCriteria().andEqualTo("companyCode", companyCode).andEqualTo("appId", appId).andEqualTo("type", TYPE_ACCESS_TOKEN);
		WechatAccessToken token = new WechatAccessToken();
		token.setAccessToken(accessToken);
		token.setExpiresIn(expiresIn);
		token.setUpdateTime(DateUtil.stringToDate(updateTime));
		return wechatAccessTokenMapper.updateByExampleSelective(token, example);
	}

	@Override
	public WechatAccessToken getWechatTicket(long companyCode, String appId) {
		Example example = new Example(WechatAccessToken.class);
		example.createCriteria().andEqualTo("type", TYPE_JSAPI_TICKET).andEqualTo("companyCode", companyCode).andEqualTo("appId", appId);
		return wechatAccessTokenMapper.selectOneByExample(example);
	}

	@Override
	public int saveWechatTicket(long companyCode, String appId, String accessToken, long expiresIn, String updateTime) {
		WechatAccessToken token = new WechatAccessToken();
		token.setCompanyCode(companyCode);
		token.setAppId(appId);
		token.setAccessToken(accessToken);
		token.setType(TYPE_JSAPI_TICKET);
		token.setExpiresIn(expiresIn);
		Date time = DateUtil.stringToDate(updateTime);
		token.setCreateTime(time);
		token.setUpdateTime(time);
		return wechatAccessTokenMapper.insert(token);
	}

	@Override
	public int updateWechatTicket(long companyCode, String appId, String accessToken, long expiresIn, String updateTime) {
		Example example = new Example(WechatAccessToken.class);
		example.createCriteria().andEqualTo("companyCode", companyCode).andEqualTo("appId", appId).andEqualTo("type", TYPE_JSAPI_TICKET);
		WechatAccessToken token = new WechatAccessToken();
		token.setAccessToken(accessToken);
		token.setExpiresIn(expiresIn);
		token.setUpdateTime(DateUtil.stringToDate(updateTime));
		return wechatAccessTokenMapper.updateByExampleSelective(token, example);
	}
}
