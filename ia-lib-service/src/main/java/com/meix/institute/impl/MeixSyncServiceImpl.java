package com.meix.institute.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.api.DataHandler;
import com.meix.institute.api.IInsSyncSndReqService;
import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.config.HandlerContext;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.Paged;
import com.meix.institute.pojo.InsSyncRecReqInfo;
import com.meix.institute.pojo.InsSyncSndReqInfo;
import com.meix.institute.pojo.InsSyncSndReqSearch;
import com.meix.institute.util.Des;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.HttpUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.third.HandleResult;

import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2020/4/9.
 */
@Service
public class MeixSyncServiceImpl implements IMeixSyncService {
	private Logger log = LoggerFactory.getLogger(MeixSyncServiceImpl.class);
	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IInsSyncSndReqService insSyncSndReqService;
	@Autowired
	private HandlerContext handlerContext;
	
	@Override
	public InsSyncRecReqInfo handleSndSyncAction(InsSyncRecReqInfo reqRecord) {
		
		try {
			if (StringUtils.isBlank(reqRecord.getReqBody())) {
				reqRecord.setRemarks("Meix请求参数【data】为空");
				reqRecord.setState(MeixConstants.SYNC_STATE_FAIL);
			} else {
				DataHandler handler = handlerContext.getHandler(reqRecord.getDataType());
				if (handler == null) {
					log.info("Meix无效的同步操作");
					reqRecord.setRemarks("Meix无效的同步操作");
					reqRecord.setState(MeixConstants.SYNC_STATE_FAIL);
					return reqRecord;
				}
				JSONObject params = JSONObject.fromObject(reqRecord.getReqBody());
				HandleResult handleReault = handler.handleData(reqRecord.getMethod(), params);
				reqRecord.setRemarks(GsonUtil.obj2Json(handleReault));
				reqRecord.setKeyword(handleReault.getKeyword());
				reqRecord.setState(MeixConstants.SYNC_STATE_SUCCESS);
			}
		} catch (Exception e) {
			log.warn("receiveMeixRecReqQueue异常：{}", GsonUtil.obj2Json(reqRecord));
			log.error("", e);
			e.printStackTrace();
			reqRecord.setState(MeixConstants.SYNC_STATE_FAIL);
			reqRecord.setRemarks(e.getMessage() == null ? null : e.getMessage().length() < 300 ? e.getMessage() : e.getMessage().substring(0, 300));
		}
		return reqRecord;
	}
	@Override
	public void reqMeixSync(int dataType, String action, Map<String, Object> params) {
		InsSyncSndReqInfo sndRecord = new InsSyncSndReqInfo();
		Date _now = new Date();
		sndRecord.setReqBody(GsonUtil.obj2Json(params));
		sndRecord.setMethod(action);
		sndRecord.setCreatedAt(_now);
		sndRecord.setUpdatedAt(_now);
		sndRecord.setRetryCnt(1);
		sndRecord.setRetryMax(3);
		sndRecord.setRecApp("17");
		sndRecord.setDataType(dataType);
		sndRecord.setSndApp(String.valueOf(commonServerProperties.getCompanyCode()));
		sndRecord.setState(MeixConstants.SYNC_STATE_NEW);
		sndRecord = reqMeixSync(sndRecord);
		insSyncSndReqService.save(sndRecord, null);
	}
	@Override
	public InsSyncSndReqInfo reqMeixSync(InsSyncSndReqInfo sndRecord) {
		String key = StringUtils.md5(commonServerProperties.getMeixAppSecret()).substring(0, 16);
		String dataEncrypt = Des.encrypt(sndRecord.getReqBody(), key);
		//sign（appId_appSecret_dataEncrypt的md5）验证
		String config = commonServerProperties.getMeixAppId() + "_" + commonServerProperties.getMeixAppSecret() + "_" + dataEncrypt;
		String sign = StringUtils.md5(config);
		Map<String, Object> reqParams = new HashMap<>();
		reqParams.put("appId", commonServerProperties.getMeixAppId());
		reqParams.put("sign", sign);
		reqParams.put("data", dataEncrypt);
		reqParams.put("action", sndRecord.getMethod());
		reqParams.put("dataType", sndRecord.getDataType());
		String url = commonServerProperties.getMeixSyncUrl() + "/dataSync/syncAction";
		JSONObject resp = HttpUtil.postHttpsBasic(url, null, reqParams, 10000);
		sndRecord.setResult(GsonUtil.obj2Json(resp));
		if (resp == null) {
			sndRecord.setState(MeixConstants.SYNC_STATE_FAIL);
		} else {
			sndRecord.setState(MeixConstants.SYNC_STATE_SUCCESS);
		}
		return sndRecord;
	}

	@Override
	public Date getLastSendDateTime(String action) {
		InsSyncSndReqSearch search = new InsSyncSndReqSearch();
		search.setMethod(action);
		search.setCurrentPage(0);
		search.setShowNum(1);
		Paged<InsSyncSndReqInfo> paged = insSyncSndReqService.paged(search, null);
		List<InsSyncSndReqInfo> list = paged.getList();
		if (list.size() > 0) {
			return list.get(0).getCreatedAt();
		}
		return null;
	}

	@Override
	public String reqMeixFileSync(File file) {
		String data = String.valueOf(System.currentTimeMillis());
		String key = StringUtils.md5(commonServerProperties.getMeixAppSecret()).substring(0, 16);
		String dataEncrypt = Des.encrypt(data, key);
		//sign（appId_appSecret_dataEncrypt的md5）验证
		String config = commonServerProperties.getMeixAppId() + "_" + commonServerProperties.getMeixAppSecret() + "_" + dataEncrypt;
		String sign = StringUtils.md5(config);
		Map<String, Object> reqParams = new HashMap<>();
		reqParams.put("appId", commonServerProperties.getMeixAppId());
		reqParams.put("sign", sign);
		reqParams.put("data", dataEncrypt);
		String url = commonServerProperties.getMeixSyncUrl() + "/dataSync/syncFile";
		JSONObject resp = HttpUtil.httpPostFile(file, url, JSONObject.fromObject(reqParams));
		if (MapUtils.getIntValue(resp, "messageCode", 0) == MeixConstantCode.M_1008) {
			return resp.getJSONObject("object").getString("url");
		}
		return null;
	}

	@Override
	public String reqMeixFileSync(InputStream is, String fileName) {
		File tempFile = null;
		try {
			String data = String.valueOf(System.currentTimeMillis());
			String key = StringUtils.md5(commonServerProperties.getMeixAppSecret()).substring(0, 16);
			String dataEncrypt = Des.encrypt(data, key);
			//sign（appId_appSecret_dataEncrypt的md5）验证
			String config = commonServerProperties.getMeixAppId() + "_" + commonServerProperties.getMeixAppSecret() + "_" + dataEncrypt;
			String sign = StringUtils.md5(config);
			Map<String, Object> reqParams = new HashMap<>();
			reqParams.put("appId", commonServerProperties.getMeixAppId());
			reqParams.put("sign", sign);
			reqParams.put("data", dataEncrypt);
			String url = commonServerProperties.getMeixSyncUrl() + "/dataSync/syncFile";
			tempFile = new File(System.currentTimeMillis() + fileName);
			FileOutputStream os = new FileOutputStream(tempFile, false);
			IOUtils.copy(is, os);
			os.flush();
			os.close();
			JSONObject resp = HttpUtil.httpPostFile(tempFile, url, JSONObject.fromObject(reqParams));
			if (MapUtils.getIntValue(resp, "messageCode", 0) == MeixConstantCode.M_1008) {
				return resp.getJSONObject("object").getString("url");
			}
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (tempFile != null && tempFile.exists()) {
				tempFile.delete();
			}
		}
		return null;
	}

	@Override
	public JSONObject reqMeixStockYield(String params) {
		String key = StringUtils.md5(commonServerProperties.getMeixAppSecret()).substring(0, 16);
		String dataEncrypt = Des.encrypt(params, key);
		//sign（appId_appSecret_dataEncrypt的md5）验证
		String config = commonServerProperties.getMeixAppId() + "_" + commonServerProperties.getMeixAppSecret() + "_" + dataEncrypt;
		String sign = StringUtils.md5(config);
		Map<String, Object> reqParams = new HashMap<>();
		reqParams.put("appId", commonServerProperties.getMeixAppId());
		reqParams.put("sign", sign);
		reqParams.put("data", dataEncrypt);
		String url = commonServerProperties.getMeixSyncUrl() + "/dataSync/getStockYield";
		JSONObject resp = HttpUtil.postHttpsBasic(url, null, reqParams, 10000);
		return resp;
	}
	
	@Override
	public JSONObject getGoldStockCombYield() {
		String key = StringUtils.md5(commonServerProperties.getMeixAppSecret()).substring(0, 16);
		String dataEncrypt = Des.encrypt(String.valueOf(System.currentTimeMillis()), key);
		//sign（appId_appSecret_dataEncrypt的md5）验证
		String config = commonServerProperties.getMeixAppId() + "_" + commonServerProperties.getMeixAppSecret() + "_" + dataEncrypt;
		String sign = StringUtils.md5(config);
		Map<String, Object> reqParams = new HashMap<>();
		reqParams.put("appId", commonServerProperties.getMeixAppId());
		reqParams.put("sign", sign);
		reqParams.put("data", dataEncrypt);
		String url = commonServerProperties.getMeixSyncUrl() + "/dataSync/getGoldStockCombYield";
		JSONObject resp = HttpUtil.postHttpsBasic(url, null, reqParams, 10000);
		return resp;
	}
	
	public void syncActivity(int dataType, String action, Map<String, Object> params) {
		MeixConstants.serverThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				String resourceUrl = MapUtils.getString(params, "resourceUrl", "");
				String resourceUrlSmall = MapUtils.getString(params, "resourceUrlSmall", "");
				try {
					InputStream is = null;
					if (StringUtils.isNotBlank(resourceUrl)) {
						is = HttpUtil.getFileByHttp(resourceUrl);
						if (is != null) {
							String fileName[] = resourceUrl.split("/");
							resourceUrl = reqMeixFileSync(is, fileName[fileName.length - 1]);
							is.close();
						}
						if (resourceUrl != null) {
							params.put("resourceUrl", resourceUrl);
						}
					}
					if (StringUtils.isNotBlank(resourceUrlSmall)) {
						is = HttpUtil.getFileByHttp(resourceUrlSmall);
						if (is != null) {
							String fileName[] = resourceUrlSmall.split("/");
							resourceUrlSmall = reqMeixFileSync(is, fileName[fileName.length - 1]);
							is.close();
						}
						
						if (resourceUrlSmall != null) {
							params.put("resourceUrlSmall", resourceUrlSmall);
						}
					}
				} catch (IOException e) {
					log.error("save activity upload file to meix failed!");
					e.printStackTrace();
				}
				reqMeixSync(dataType, action, params);
			}
		});
	}
	
}
