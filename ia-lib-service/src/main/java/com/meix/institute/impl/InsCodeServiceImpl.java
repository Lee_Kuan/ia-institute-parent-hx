package com.meix.institute.impl;

import com.meix.institute.api.ICodeService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.Item;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsCode;
import com.meix.institute.mapper.InsCodeMapper;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InsCodeServiceImpl implements ICodeService {
	@Autowired
	private InsCodeMapper insCodeMapper;

	@Override
	public List<Item> findItem(String name) {
		if (StringUtils.isBlank(name)) {
			return Collections.emptyList();
		}
		InsCode record = new InsCode();
		record.setName(name);
//		if (StringUtils.equals(name, MeixConstants.DIRECTION)) {
//		    record.setUdf2("1");
//		}
		if (StringUtils.equals(name, MeixConstants.REPORT_SEARCH_TYPE)) {
		    record.setUdf1("1");
		}
		List<InsCode> data = insCodeMapper.select(record);
		if (CollectionUtils.isNotEmpty(data)) {
			List<Item> list = new ArrayList<Item>();
			data.forEach((it) -> {
				list.add(new Item(it.getCode(), it.getText()));
			});
			return list;
		}
		return null;
	}

	@Override
	public String getText(String name, String code) {
		if (StringUtils.isBlank(name) || StringUtils.isBlank(code)) {
			return null;
		}
		InsCode record = new InsCode();
		record.setName(name);
		record.setCode(code);
		record = insCodeMapper.selectOne(record);
		if (record != null) {
			return record.getText();
		}
		return null;
	}

	@Override
	public Item getItem(String name, String code) {
		if (StringUtils.isBlank(name) || StringUtils.isBlank(code)) {
			return null;
		}
		InsCode record = new InsCode();
		record.setName(name);
		record.setCode(code);
		record = insCodeMapper.selectOne(record);
		if (record != null) {
			return new Item(record.getCode(), record.getText());
		}
		return null;
	}

	@Override
	public void insert(InsCode code) {
		insCodeMapper.insert(code);
	}

	@Override
	public void insertList(List<InsCode> code) {
		if (code == null || code.size() == 0) {
			return;
		}
		insCodeMapper.insertList(code);
	}

	@Override
	public String getCode(String name, String text) {
		InsCode record = new InsCode();
		record.setName(name);
		record.setText(text);
		record = insCodeMapper.selectOne(record);
		if (record != null) {
			return record.getCode();
		}
		return null;
	}

	@Override
	public void save(InsCode code, String user) {
		if (code == null) {
			return;
		}
		Long id = checkDuplicate(code);
		Date _now = new Date();
		if (id == null) { // 新增
			code.setCreatedAt(_now);
			code.setCreatedBy(user);
			code.setUpdatedAt(_now);
			code.setUpdatedBy(user);
			insCodeMapper.insert(code);
		} else { // 更新
			code.setId(id);
			code.setUpdatedAt(_now);
			code.setUpdatedBy(user);
			insCodeMapper.updateByPrimaryKeySelective(code);
		}
	}

	private Long checkDuplicate(InsCode code) {
		if (code.getId() != null) {
			return code.getId();
		}
		InsCode record = new InsCode();
		record.setName(code.getName());
		record.setCode(code.getCode());
		record = insCodeMapper.selectOne(record);
		if (record != null && record.getId() != null) {
			return record.getId();
		}
		return null;
	}

	@Override
	public InsCode selectByBizKey(String name, String code) {
		InsCode record = new InsCode();
		record.setName(name);
		record.setCode(code);
		return insCodeMapper.selectOne(record);
	}

	@Override
	public void deleteByBizKey(String name, String code) {
		InsCode record = new InsCode();
		record.setName(name);
		record.setCode(code);
		insCodeMapper.delete(record);
	}

	@Override
	public Paged<Map<String, Object>> getIndustryCodes(int type, int currentPage, int showNum) {
		List<Map<String, Object>> res = new ArrayList<>();
		Example example = new Example(InsCode.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("name", MeixConstants.REPORT_INDUSTRY);
		if (type == 1) {
		    criteria.andIsNull("udf1");
		}
		int count = insCodeMapper.selectCountByExample(example);
		if (count > 0) {
			List<InsCode> data = null;
			if (showNum > 0) {
				RowBounds rowBounds = new RowBounds(currentPage * showNum, showNum);
				data = insCodeMapper.selectByExampleAndRowBounds(example, rowBounds);
			} else {
				data = insCodeMapper.selectByExample(example);
			}
			if (CollectionUtils.isNotEmpty(data)) {
				data.forEach((it) -> {
					Map<String, Object> item = new HashMap<>();
					item.put("xnindCode", Integer.valueOf(it.getCode()));
					item.put("xnindName", it.getText());
					item.put("industryCode", Integer.valueOf(it.getUdf1() == null ? "0" : it.getUdf1()));
					res.add(item);
				});
			}
		}
		Paged<Map<String, Object>> paged = new Paged<>();
		paged.setCount(count);
		paged.setList(res);
		return paged;
	}

	@Override
	public void setIndustryCode(int industryCode, int xnindCode) {
		Example example = new Example(InsCode.class);
		example.createCriteria().andEqualTo("name", MeixConstants.REPORT_INDUSTRY).andEqualTo("code", xnindCode);
		InsCode record = new InsCode();
		record.setUdf1(String.valueOf(industryCode));
		insCodeMapper.updateByExampleSelective(record, example);
	}
}
