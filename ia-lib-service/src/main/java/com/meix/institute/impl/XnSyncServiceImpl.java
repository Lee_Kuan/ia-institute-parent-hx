package com.meix.institute.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.meix.institute.api.ICodeService;
import com.meix.institute.api.ICompanyService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.api.IReportService;
import com.meix.institute.api.ISecumainService;
import com.meix.institute.api.IUserService;
import com.meix.institute.api.IXnSyncService;
import com.meix.institute.api.XnSyncDao;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.BaseResp;
import com.meix.institute.core.Key;
import com.meix.institute.entity.InsCode;
import com.meix.institute.entity.InsSyncTableJsid;
import com.meix.institute.entity.InsUser;
import com.meix.institute.entity.InsUserDirection;
import com.meix.institute.entity.ReportInfo;
import com.meix.institute.entity.ReportRelation;
import com.meix.institute.entity.SecuMain;
import com.meix.institute.mapper.InsCodeMapper;
import com.meix.institute.mapper.InsSyncTableJsidMapper;
import com.meix.institute.mapper.InsUserDirectionMapper;
import com.meix.institute.mapper.InsUserMapper;
import com.meix.institute.mapper.ReportInfoMapper;
import com.meix.institute.pojo.InsUserInfo;
import com.meix.institute.pojo.xn.XnDirection;
import com.meix.institute.pojo.xn.XnReportVo;
import com.meix.institute.pojo.xn.XnUserVo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.vo.user.UserInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import tk.mybatis.mapper.entity.Example;

/**
 * 贝格同步服务
 *
 * @author xueyc
 * @author zenghao
 * @author likuan
 * @since 2020-4-3
 */
@Service
public class XnSyncServiceImpl implements IXnSyncService {

	@Autowired
	private XnSyncDao xnSyncDao;
	@Autowired
	private InsUserMapper insUserMapper;
	@Autowired
	private InsUserDirectionMapper userDirectionMapper;
	@Autowired
	private IInsUserService insUserService;
	@Autowired
	private IUserService userServiceImpl;
	@Autowired
	private ICodeService codeService;
	@Autowired
	private InsCodeMapper codeMapper;
	@Autowired
	private ICompanyService companySerciveImpl;
	@Autowired
	private ReportInfoMapper reportMapper;
	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private ISecumainService secumainServiceImpl;
	@Autowired
	private IReportService reportServiceImpl;
	@Autowired
	private InsSyncTableJsidMapper syncTableJsidMapper;
	@Autowired
	private ICustomerGroupService groupService;

	private static final Logger log = LoggerFactory.getLogger(XnSyncServiceImpl.class);

	// 初始化信息
	private String ins_companyAbbr = null; // 证券公司信息
	private String department_analyst = null;
	private String department_market = null;
	private String department_operate = null;
	private String department_system_manage = null;
	private String department_other = null;
	private String institute = null; // 所长默认值
	private String company = null; // 公司默认值
	private static Boolean syncXnReport = false;
	private static Boolean syncXnUser = false;
	private static Boolean syncXnDirection = false;
	private Set<String> directionAndPositionCodes = null;
	private Map<String, InsCode> reportType = null;
	private Map<String, InsCode> reportSubType = null;
//	private Map<String, InsCode> industryMap = null;
	private Map<String, InsCode> reportSearchType = null;

	@Override
	public BaseResp action(InsSyncTableJsid syncRecord, Integer syncVia, Boolean isSyncAll, String user) {
		if (syncRecord == null || syncVia == null || StringUtils.isBlank(syncRecord.getTableName())) {
			return new BaseResp(MeixConstantCode.M_1009, "参数不合法", false);
		}
		switch (syncVia) {
			case MeixConstants.CREATED_VIA_1: // 系统
				handleAutoVia(syncRecord);
				break;
			case MeixConstants.CREATED_VIA_2: // 人工
				Long id = syncRecord.getId();
				if (!isSyncAll && null == id) {
					return new BaseResp(MeixConstantCode.M_1009, "参数不合法", false);
				}
				handleManualVia(syncRecord, isSyncAll);
				break;
			default:
				break;
		}
		String tableName = syncRecord.getTableName();
		if (StringUtils.equals(MeixConstants.SYNC_JS_TABLE_NAME_USER, tableName)) {
			syncInnerUser(syncRecord, isSyncAll);
		} else if (StringUtils.equals(MeixConstants.SYNC_JS_TABLE_NAME_REPORT, tableName)) {
			syncBgReport(syncRecord, isSyncAll);
		} else if (StringUtils.equals(MeixConstants.SYNC_JS_TABLE_NAME_ORG, tableName)) {
			syncOrg();
		}
		saveSyncRecord(syncRecord, user);
		updateHistoryFailRecord(syncRecord);

		return BaseResp.ok();
	}

	private void handleAutoVia(InsSyncTableJsid syncRecord) {
		String tableName = syncRecord.getTableName();
		InsSyncTableJsid lastInsSyncTableJsid = getLastSyncTableRecord(tableName);
		// 封装传参
		Date updateTimeFm = null;
		if (lastInsSyncTableJsid == null || lastInsSyncTableJsid.getId() == null) {
			updateTimeFm = DateUtil.stringToDate("2000-01-01 00:00:00");
		} else {
			updateTimeFm = lastInsSyncTableJsid.getJsUpdateTimeTo();
		}
		syncRecord.setJsUpdateTimeFm(updateTimeFm);
		syncRecord.setJsUpdateTimeTo(new Date());
	}

	private void handleManualVia(InsSyncTableJsid syncRecord, Boolean isSyncAll) {
		if (isSyncAll) {
			syncRecord.setJsUpdateTimeFm(DateUtil.stringToDate("1990-01-01 00:00:00"));
			syncRecord.setJsUpdateTimeTo(new Date());
		} else {
			InsSyncTableJsid selectByPrimaryKey = syncTableJsidMapper.selectByPrimaryKey(syncRecord.getId());
			syncRecord.setJsUpdateTimeFm(selectByPrimaryKey.getJsUpdateTimeFm());
			syncRecord.setJsUpdateTimeTo(selectByPrimaryKey.getJsUpdateTimeTo());
			syncRecord.setRetryCnt(selectByPrimaryKey.getRetryCnt());
		}
	}

	private InsSyncTableJsid getLastSyncTableRecord(String tableName) {
		Example example = new Example(InsSyncTableJsid.class);
		example.createCriteria().andEqualTo("tableName", tableName).andEqualTo("state", MeixConstants.SYNC_STATE_SUCCESS);
		example.orderBy("jsUpdateTimeTo").desc().orderBy("id").desc();
		RowBounds rowBounds = new RowBounds(0, 1);
		List<InsSyncTableJsid> list = syncTableJsidMapper.selectByExampleAndRowBounds(example, rowBounds);
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * 保存同步记录
	 *
	 * @param syncRecord
	 * @param user
	 */
	private void saveSyncRecord(InsSyncTableJsid syncRecord, String user) {
		Date _now = new Date();
		if (syncRecord.getId() == null) { // 新增
			syncRecord.setCreatedAt(_now);
			syncRecord.setCreatedBy(user);
			syncRecord.setUpdatedAt(_now);
			syncRecord.setUpdatedBy(user);
			syncRecord.setRetryCnt(1);
			syncRecord.setRetryMax(3);
			syncTableJsidMapper.insert(syncRecord);
		} else { // 保存
			syncRecord.setUpdatedAt(_now);
			syncRecord.setUpdatedBy(user);
			syncRecord.setRetryCnt(syncRecord.getRetryCnt() + 1);
			syncTableJsidMapper.insert(syncRecord);
			syncTableJsidMapper.updateByPrimaryKeySelective(syncRecord);
		}
	}

	/**
	 * 更新历史失败记录状态
	 */
	private void updateHistoryFailRecord(InsSyncTableJsid syncRecord) {
		if (StringUtils.endsWith(MeixConstants.SYNC_STATE_FAIL, syncRecord.getState())) {
			return;
		}
		Example example = new Example(InsSyncTableJsid.class);
		List<String> states = new ArrayList<String>();
		states.add(MeixConstants.SYNC_STATE_NEW);
		states.add(MeixConstants.SYNC_STATE_FAIL);
		example.createCriteria()
			.andEqualTo("tableName", syncRecord.getTableName())
			.andIn("state", states)
			.andLessThan("jsUpdateTimeTo", syncRecord.getJsUpdateTimeTo())
			.andGreaterThanOrEqualTo("createdAt", DateUtil.getDate(-30)); // 近一个月

		InsSyncTableJsid params = new InsSyncTableJsid();
		params.setState(MeixConstants.SYNC_STATE_CLOSE);
		params.setUpdatedAt(new Date());
		syncTableJsidMapper.updateByExampleSelective(params, example);
	}

	public void syncInnerUser(InsSyncTableJsid syncRecord, Boolean isSyncAll) {
		if (syncXnUser) {
			syncRecord.setRemarks("用户正在同步中，请稍后重试！！！");
			syncRecord.setState(MeixConstants.SYNC_STATE_CLOSE);
			log.warn("用户正在同步中，请稍后重试！！！");
			return;
		}
		syncXnUser = true;
		syncRecord.setState(MeixConstants.SYNC_STATE_NEW);
		try {
			String updateTimeFm = DateUtil.dateToStr(syncRecord.getJsUpdateTimeFm(), DateUtil.dateFormat);
			String updateTimeTo = DateUtil.dateToStr(syncRecord.getJsUpdateTimeTo(), DateUtil.dateFormat);
			int count = xnSyncDao.countInnerUser(updateTimeFm, updateTimeTo, isSyncAll);
			if (count < 1) {
				syncXnUser = false;
				syncRecord.setRemarks("成功数：【0】");
				syncRecord.setState(MeixConstants.SYNC_STATE_SUCCESS);
				return;
			}
			String errorMsg = initCache();
			if (StringUtils.isNotBlank(errorMsg)) {
				syncRecord.setRemarks(errorMsg);
				syncRecord.setState(MeixConstants.SYNC_STATE_FAIL);
				syncXnUser = false;
				log.warn("同步内部用户异常：【{}】", errorMsg);
				return;
			}
			int page = 0;
			int pageSize = 200;
			List<XnUserVo> list = null;
			while ((page * pageSize) < count) {
				list = xnSyncDao.getInnerUser(updateTimeFm, updateTimeTo, isSyncAll, page, pageSize);
				saveInnerUser(list);
				page++;
			}
			// 同步成功
			syncRecord.setRemarks("成功数：【" + count + "】");
			syncRecord.setState(MeixConstants.SYNC_STATE_SUCCESS);
		} catch (Exception e) {
			// 同步失败
			syncRecord.setState(MeixConstants.SYNC_STATE_FAIL);
			syncRecord.setRemarks("同步内部用户异常");
			log.warn("同步内部用户异常:", e);
		} finally {
			syncXnUser = false;
			directionAndPositionCodes = null;
		}
	}

	public void syncDirection(InsSyncTableJsid syncRecord) {
		syncRecord.setState(MeixConstants.SYNC_STATE_NEW);
		if (syncXnDirection) {
			log.warn("研究方向正在同步中，请稍后重试！！！");
			return;
		}
		syncXnDirection = true;
		try {
			List<XnDirection> directions = xnSyncDao.getDirection();
			if (CollectionUtils.isEmpty(directions)) {
				return;
			}
			String errorMsg = initDepartment();
			if (StringUtils.isNotBlank(errorMsg)) {
				syncRecord.setRemarks(errorMsg);
				syncRecord.setState(MeixConstants.SYNC_STATE_FAIL);
				syncXnUser = false;
				log.warn("同步研究方向异常：【{}】", errorMsg);
				return;
			}
			// 先删除
			InsCode directionCode = new InsCode();
			directionCode.setName(MeixConstants.DIRECTION);
			codeMapper.delete(directionCode);
			// 再添加
			List<InsCode> codes = new ArrayList<InsCode>();
			directions.forEach((direction) -> {
				InsCode code = new InsCode();
				code.setName(MeixConstants.DIRECTION);
				code.setCode(direction.getOid());
				code.setText(direction.getObjname());
				code.setUdf1(convertDepartment(direction.getCol1()));
				code.setUdf2(direction.getCol1());
				codes.add(code);
			});
			if (codes != null && codes.size() > 0) {
				codeMapper.insertList(codes);
			}
			// 同步成功
			syncRecord.setRemarks("已经全量同步");
			syncRecord.setState(MeixConstants.SYNC_STATE_SUCCESS);
		} catch (Exception e) {
			// 同步失败
			syncRecord.setState(MeixConstants.SYNC_STATE_FAIL);
			syncRecord.setRemarks("同步研究方向异常");
			log.warn("同步研究方向异常:", e);
		} finally {
			syncXnDirection = false;
		}
	}

	public void syncOrg() {
		log.info("部门请直接在数据库中维护（注意数据库环境）！！！");
	}

	public void syncBgReport(InsSyncTableJsid syncRecord, Boolean isSyncAll) {
		syncRecord.setState(MeixConstants.SYNC_STATE_NEW);
		if (syncXnReport) {
			log.warn("研报正在同步中，请稍后重试！！！");
			return;
		}
		syncXnReport = true;
		String updateTimeFm = DateUtil.dateToStr(syncRecord.getJsUpdateTimeFm(), DateUtil.dateFormat);
		String updateTimeTo = DateUtil.dateToStr(syncRecord.getJsUpdateTimeTo(), DateUtil.dateFormat);
		try {
			
			String errorMsg = initReportTypeCache();
			if (StringUtils.isNotBlank(errorMsg)) {
				syncRecord.setRemarks(errorMsg);
				syncRecord.setState(MeixConstants.SYNC_STATE_FAIL);
				syncXnReport = false;
				log.warn("同步研报异常：【{}】", errorMsg);
				return;
			}
			
			int count = xnSyncDao.countBgReport(updateTimeFm, updateTimeTo, isSyncAll);
			if (count < 1) {
				syncXnReport = false;
				syncRecord.setRemarks("成功数：【0】");
				syncRecord.setState(MeixConstants.SYNC_STATE_SUCCESS);
				return;
			}
			int page = 0;
			int pageSize = 200;
			List<XnReportVo> reportVoList = null;
			while ((page * pageSize) < count) {
				reportVoList = xnSyncDao.getBgReportList(updateTimeFm, updateTimeTo, isSyncAll, page, pageSize);
				saveReport(reportVoList);
				page++;
			}
			// 同步成功
			syncRecord.setRemarks("成功数：【" + count + "】");
			freeCache();
			syncRecord.setState(MeixConstants.SYNC_STATE_SUCCESS);
		} catch (Exception e) {
			// 同步失败
			syncRecord.setState(MeixConstants.SYNC_STATE_FAIL);
			syncRecord.setRemarks("同步研报异常");
			log.warn("同步研报异常：", e);
		} finally {
			syncXnReport = false;
		}
	}

	private void saveReport(List<XnReportVo> list) {
		if (list == null || list.size() < 1) {
			return;
		}
		List<InsCode> codeList = new ArrayList<>();
		JSONObject share = new JSONObject();
		share.accumulate("share", -1);
		share.accumulate("dm", 0);
		for (XnReportVo xnReportVo : list) {
			Date _now = new Date();
			ReportInfo reportInfo = new ReportInfo();
			reportInfo.setResearchId(0L);
			reportInfo.setThirdId(xnReportVo.getObjid());
			reportInfo.setType(0);
			reportInfo.setOrgCode(commonServerProperties.getCompanyCode());
			reportInfo.setTitle(xnReportVo.getTitle());
			reportInfo.setPublishDate(DateUtil.strToDate(xnReportVo.getArchivetime(), DateUtil.dateFormat));
			
			boolean isExist = false;
			// 是否存在
			Example example = new Example(ReportInfo.class);
			example.createCriteria().andEqualTo("thirdId", xnReportVo.getObjid());
			ReportInfo rep = reportMapper.selectOneByExample(example);
			if (rep != null && rep.getResearchId() != null) {
				if (MeixConstants.REPORT_STATUS_3 == rep.getStatus()) { // 已撤销的不同步
					continue;
				}
				reportInfo.setResearchId(rep.getResearchId());
				isExist = true;
			}
			reportInfo.setDeepStatus(0);
			reportInfo.setPowerSubType(0);
			String pinyin = null;
			if (StringUtils.isNotBlank(xnReportVo.getName())) {
				String typeCode = PinyinHelper.getShortPinyin(xnReportVo.getName()).toUpperCase();//二级分类
				if (reportSubType.containsKey(typeCode) ) {
					InsCode code = reportSubType.get(typeCode);
					if (code.getUdf1() != null) {
						reportInfo.setInfoType(code.getUdf1()); //一级分类
					} else {
						reportInfo.setInfoType(xnReportVo.getColumnid());
						if (!reportType.containsKey(xnReportVo.getColumnid())) {
							//新增reportType
							InsCode insCode = new InsCode();
							insCode.setName(MeixConstants.REPORT_TYPE);
							insCode.setCode(xnReportVo.getColumnid());
							insCode.setText(xnReportVo.getColumnname());
							codeList.add(insCode);
							reportType.put(xnReportVo.getColumnid(), insCode);
						}
					}
					if (code.getRemarks() != null) { //搜索分类
						reportInfo.setInfoSearchType(code.getRemarks());
					} else {
						reportInfo.setInfoSearchType(xnReportVo.getColumnid());
						if (!reportSearchType.containsKey(xnReportVo.getColumnid())) {
							//新增reportSearchType
							InsCode insCode = new InsCode();
							insCode.setName(MeixConstants.REPORT_SEARCH_TYPE);
							insCode.setCode(xnReportVo.getColumnid());
							insCode.setText(xnReportVo.getColumnname());
							codeList.add(insCode);
							reportSearchType.put(xnReportVo.getColumnid(), insCode);
						}
					}
					//默认值0的将无权限展示
					reportInfo.setPowerSubType(code.getUdf2() == null ? 0 : Integer.valueOf(code.getUdf2()));
					reportInfo.setDeepStatus(code.getUdf3() == null ? 0 : Integer.valueOf(code.getUdf3()));
				} else {
					reportInfo.setInfoType(xnReportVo.getColumnid());
					//新增子类型
					InsCode code = new InsCode();
					code.setName(MeixConstants.REPORT_SUB_CATALOG);
					code.setCode(typeCode);
					code.setText(xnReportVo.getName());
					code.setUdf1(xnReportVo.getColumnid());
					code.setRemarks(xnReportVo.getColumnid());
					codeList.add(code);
					reportSubType.put(typeCode, code);
					if (!reportType.containsKey(xnReportVo.getColumnid())) {
						//新增reportType
						InsCode insCode = new InsCode();
						insCode.setName(MeixConstants.REPORT_TYPE);
						insCode.setCode(xnReportVo.getColumnid());
						insCode.setText(xnReportVo.getColumnname());
						codeList.add(insCode);
						reportType.put(xnReportVo.getColumnid(), insCode);
					}
					reportInfo.setInfoSearchType(xnReportVo.getColumnid());
					if (!reportSearchType.containsKey(xnReportVo.getColumnid())) {
						//新增reportSearchType
						InsCode insCode = new InsCode();
						insCode.setName(MeixConstants.REPORT_SEARCH_TYPE);
						insCode.setCode(xnReportVo.getColumnid());
						insCode.setText(xnReportVo.getColumnname());
						codeList.add(insCode);
						reportSearchType.put(xnReportVo.getColumnid(), insCode);
					}
				}
				pinyin = MeixConstants.reportTypeCollection.get(xnReportVo.getName());
			}
			// 处理图片
			if (StringUtils.isBlank(pinyin)) {
				pinyin = MeixConstants.reportTyepDefault;
			}
			reportInfo.setPicture(commonServerProperties.getReportServer() + pinyin + ".jpg");
			reportInfo.setInfoSubType(xnReportVo.getName());
			reportInfo.setInfoGrade(xnReportVo.getStkrank());
			reportInfo.setSummary(xnReportVo.getSummary());
			reportInfo.setShare(1);
			reportInfo.setStatus(1);

			// 个股、行业、作者相关
			List<ReportRelation> relations = new ArrayList<>();
			// 作者信息
			StringBuffer authNames = new StringBuffer();
			if (StringUtil.isNotEmpty(xnReportVo.getAuthorid())) {
				// 匹配作者
				String[] ids = xnReportVo.getAuthorid().split(",");
				for (int i=0; i< ids.length; i++) {
					if (StringUtils.isNotBlank(ids[i])) {
						InsUser authorRecord = insUserMapper.selectByThirdId(ids[i]);
						if (authorRecord != null && authorRecord.getId() != null) {
							ReportRelation author_relation = new ReportRelation();
							author_relation.setCode(authorRecord.getId());
							authNames.append(authorRecord.getUserName()).append(",");
							author_relation.setContent(authorRecord.getUserName());
							author_relation.setType(MeixConstants.REPORT_RELATION_TYPE_1);
							author_relation.setNo(i);
							author_relation.setCreatedAt(_now);
							author_relation.setCreatedBy(commonServerProperties.getDefaultUser());
							relations.add(author_relation);
						}
					}
				}
			}
			reportInfo.setAuthNames(authNames.length() > 1 ? authNames.substring(0, authNames.length() - 1) : null);

			// 个股
			String skt_code = xnReportVo.getScode();
			if (StringUtils.isNotBlank(skt_code)) {
				// 根据股票代码获取内码
				SecuMain secuMain = secumainServiceImpl.getSecuMainBySecuCode(skt_code);
				if (secuMain != null) {
					ReportRelation stock_relation = new ReportRelation();
					stock_relation.setCode(Long.valueOf(secuMain.getInnerCode()));
					stock_relation.setContent(secuMain.getSecuAbbr());
					stock_relation.setType(MeixConstants.REPORT_RELATION_TYPE_2);
					stock_relation.setNo(0);
					stock_relation.setCreatedAt(_now);
					stock_relation.setCreatedBy(commonServerProperties.getDefaultUser());
					relations.add(stock_relation);
				}
			}

			// 行业
			String indu_code = xnReportVo.getIndcode();
			if (StringUtils.isNotBlank(indu_code)) {
				long industryCode = 0;
				
				// 携宁行业代码转换为申万行业代码
				indu_code = indu_code.substring(2);
				industryCode = Long.valueOf(indu_code);
				while (industryCode / 100000 == 0) {
					industryCode = industryCode * 10;
				}
				ReportRelation indu_relation = new ReportRelation();
				String indName = xnReportVo.getIndname();
				if (indName != null && indName.endsWith("Ⅱ") && xnReportVo.getIndcode().length() > 4) {//二级行业名字
					indName = indName.substring(0, indName.length() - 1);
				}
				indu_relation.setContent(indName);
				indu_relation.setCode(industryCode);
				indu_relation.setXnindCode(xnReportVo.getIndcode());
				indu_relation.setXnindName(xnReportVo.getIndname());
				indu_relation.setType(MeixConstants.REPORT_RELATION_TYPE_3);
				indu_relation.setNo(0);
				indu_relation.setCreatedAt(_now);
				indu_relation.setCreatedBy(commonServerProperties.getDefaultUser());
				relations.add(indu_relation);
			}

			// 渠道——默认每市、公众号、小程序
			JSONArray issueType = new JSONArray();
			issueType.add(MeixConstants.ISSUETYPE_MEIX);
			issueType.add(MeixConstants.ISSUETYPE_WECHAT);
			issueType.add(MeixConstants.ISSUETYPE_APPLET);

			long resId = reportInfo.getResearchId() == 0 ? com.meix.institute.util.StringUtils.getId(reportInfo.getOrgCode()) : reportInfo.getResearchId();
			// 保存研报
			long researchId = reportServiceImpl.saveReport(resId, commonServerProperties.getDefaultUser(), reportInfo, relations, issueType);
			if (!isExist && researchId > 0 && !"10006".equals(xnReportVo.getColumnid())) { //已存在的研报不重新进行授权;10006-内部报告
				JSONArray groupList = new JSONArray();
				List<Map<String, Object>> shareList = groupService.getDataShareList(researchId, MeixConstants.USER_YB, reportInfo.getPowerSubType() == null ? 0 : reportInfo.getPowerSubType(), -1);
				if (shareList != null && !shareList.isEmpty()) {
					for(Map<String, Object> tmpMap : shareList) {
						JSONObject shareObj = new JSONObject();
						shareObj.accumulate("location", MapUtils.getIntValue(tmpMap, "powerLocation"));
						shareObj.accumulate("share", MeixConstants.SHARE_DEFAULT);
						shareObj.accumulate("dm", 0);
						groupList.add(shareObj);
					}
				}
				groupService.saveDataGroupShare(researchId, MeixConstants.USER_YB, reportInfo.getPowerSubType() == null ? 0 : reportInfo.getPowerSubType(), groupList);
			}
			// 保存研报pdf地址
			if (StringUtils.isNotBlank(xnReportVo.getObjid())) {
				reportServiceImpl.saveReportPdf(researchId, xnReportVo.getObjid());
			}
		}
		codeService.insertList(codeList);
	}
	
	/**
	 * 创建员工
	 */
	private void createStaff(XnUserVo vo, List<InsCode> tempCodes) {
		// 根据第三方id获取用户信息
		InsUser result = insUserMapper.selectByThirdId(vo.getOrgid());
		Integer userState = convertUserState(vo.getInservice());
		if ((result == null || result.getId() == null) && userState == 1) {
			return;
		}
		// 员工参数
		InsUserInfo insUserInfo = new InsUserInfo();
		insUserInfo.setCompanyCode(commonServerProperties.getCompanyCode());
		insUserInfo.setId(result == null ? null : result.getId());
		insUserInfo.setOaName(vo.getUsername());
		insUserInfo.setUserName(vo.getOrgname());
		insUserInfo.setChiSpelling(PinyinHelper.getShortPinyin(insUserInfo.getUserName()).toUpperCase());
		insUserInfo.setPassword(commonServerProperties.getDefaultPassword()); // 默认密码
		insUserInfo.setMobile(vo.getMobilephone());
		insUserInfo.setEmail(vo.getEmail());
		insUserInfo.setPosition(vo.getJob());
		insUserInfo.setDepartment(converDepartment(vo.getUnitid()));
		insUserInfo.setThirdId(vo.getOrgid());
		insUserInfo.setUserState(userState); // 员工入职状态
		insUserInfo.setUserType(convertUserType(vo.getJob(), vo.getPositionname(), vo.getUnname()));
		insUserInfo.setRole(convertUserRole(insUserInfo.getUserType(), vo.getUnitid()));
		insUserInfo.setQualifyno(vo.getQualifyno());//分析师编号
		insUserInfo.setDescr(vo.getIntroduction());
		if (MeixConstants.USER_TYPE_1 == insUserInfo.getUserType() && StringUtils.isNotBlank(vo.getIndustryname())) {
			insUserInfo.setDirection(vo.getIndustryname());
		}
		if (null != result && null != result.getId()) { //已存在的员工，不更新角色与用户类型
			insUserInfo.setRole(null);
			insUserInfo.setHeadImageUrl(null);
			insUserInfo.setDescr(null);
		}
		Key key = insUserService.save(insUserInfo, commonServerProperties.getDefaultUser()); // 保存用户
		// 保存分析师方向
		String chiefAnalysts = isChiefAnalysts(insUserInfo.getUserType(), vo.getJob(), vo.getPositionname());
		if (MeixConstants.USER_TYPE_1 == insUserInfo.getUserType() && StringUtils.isNotBlank(vo.getIndustrycode())
				&& StringUtils.isNotBlank(vo.getIndustryname())) {
			String[] inds = vo.getIndustrycode().split(",");
			String[] indn = vo.getIndustryname().split(",");
			if (inds.length == indn.length) {
				for (int i=0; i<inds.length; i++) {
					if (StringUtils.isNotBlank(inds[i]) && StringUtils.isNotBlank(indn[i]) && !directionAndPositionCodes.contains(inds[i])) {
						InsCode directionCode = new InsCode();
						directionCode.setName(MeixConstants.DIRECTION);
						directionCode.setCode(inds[i]);
						directionCode.setText(indn[i]);
						directionCode.setUdf1(vo.getJob());
						directionCode.setUdf2(chiefAnalysts);
						tempCodes.add(directionCode);
						directionAndPositionCodes.add(inds[i]);
					}
				}
			}
		}
				
		// 关联分析师方向
		if (MeixConstants.USER_TYPE_1 == insUserInfo.getUserType() && StringUtils.isNotBlank(vo.getIndustrycode())) {
			saveUserDirection(key.getId(), vo.getIndustrycode(), chiefAnalysts);
		}

		// 职位
		if (StringUtils.isNotBlank(vo.getJob()) && !directionAndPositionCodes.contains(vo.getJob())) {
			InsCode positionCode = new InsCode();
			positionCode.setName(MeixConstants.POSITION);
			positionCode.setCode(vo.getJob());
			positionCode.setText(vo.getJob());
			positionCode.setUdf1(vo.getJob());
			positionCode.setUdf2(chiefAnalysts);
			tempCodes.add(positionCode);
			directionAndPositionCodes.add(vo.getJob());
		}
				
		// 自动注册到用户表
		UserInfo userInfo = new UserInfo();
		userInfo.setUserName(insUserInfo.getUserName());
		userInfo.setCompanyCode(commonServerProperties.getCompanyCode());
		userInfo.setCompanyAbbr(ins_companyAbbr);
		userInfo.setMobile(insUserInfo.getMobile());
		userInfo.setPosition(insUserInfo.getPosition());
		userInfo.setEmail(insUserInfo.getEmail());
		userInfo.setIsDelete(insUserInfo.getUserState());
		userInfo.setChiSpelling(insUserInfo.getChiSpelling());
		userInfo.setAccountType(1);
		userInfo.setAuthStatus(3);
		userInfo.setThirdId(insUserInfo.getThirdId());
		userInfo.setInsUuid(insUserInfo.getUser());
		userInfo.setComment(insUserInfo.getDescr());
		userServiceImpl.saveThirdUser(userInfo);
	}

	/**
	 * 判断是否首席分析师： 0 不是分析师  1 非首席分析师  2 首席分析师
	 *
	 * @param 
	 * @return
	 */
	private String isChiefAnalysts(int userType, String job, String positionname) {
		if (userType != MeixConstants.USER_TYPE_1) {
			return "0";
		}
		if (job != null && job.contains(MeixConstants.CHIEF_ANALYST_KEY)) {
			return "2";
		}
		if (positionname != null && positionname.contains(MeixConstants.CHIEF_ANALYST_KEY)) {
			return "2";
		}
		return "1";
	}

	/**
	 * 部门判断
	 *
	 * @param department
	 * @return
	 */
	private String converDepartment(String department) {
		if (StringUtils.equals(company, department)) { // 如果值为公司，赋值为研究所
			return institute;
		}
		return department;
	}

	private void saveUserDirection(Long id, String industryCode, String chiefAnalysts) {
		InsUserDirection userDirection = new InsUserDirection();
		userDirection.setUid(id);
		// 1.删除
		userDirectionMapper.delete(userDirection);
		// 2.重新生成
		String[] inds = industryCode.split(",");
		for (String ind : inds) {
			if (StringUtils.isNotBlank(ind)) {
				userDirection.setId(null);
				userDirection.setDirection(ind);
				userDirectionMapper.insert(userDirection);
			}
		}
		// 3.拆分首席分析师
		
//		if (StringUtils.equals("2", chiefAnalysts)) { 
//			List<String> subOrgids = xnSyncDao.getSubOrgid(industryCode); 
//			if (subOrgids != null && subOrgids.size() > 1) { 
//				for (String item : subOrgids) { 
//					userDirection.setId(null);
//					userDirection.setDirection(item); 
//					userDirectionMapper.insert(userDirection);
//				} 
//			} 
//		}
		 
	}

	/**
	 * 转换用户角色
	 *
	 * @param userType
	 * @return
	 */
	private Integer convertUserRole(int userType, String department) {
		Integer role = 3;
		switch (userType) {
			case 1:
				role = 2;
				break;
			case 2:
				role = 4;
				break;
			case 3:
				role = 1;
				break;
			case 4:
				role = 1;
				break;
			case 5:
				role = 3;
				break;
			default:
				break;
		}
		if (StringUtils.equals(institute, department) || StringUtils.equals(company, department)) {
			role = 1;
		}
		return role;
	}

	/**
	 * 转换部门信息
	 *
	 * @param col1
	 * @return
	 */
	private String convertDepartment(String col1) {
		if (StringUtils.contains(col1, department_analyst)) {
			return department_analyst;
		} else if (StringUtils.contains(col1, department_market)) {
			return department_market;
		} else if (StringUtils.contains(col1, department_operate)) {
			return department_operate;
		} else if (StringUtils.contains(col1, department_system_manage)) {
			return department_system_manage;
		} else if (StringUtils.contains(col1, department_other)) {
			return department_other;
		}
		return null;
	}

	/**
	 * 转换用户状态
	 * 	管理员账号我们给他们，销售、分析师自动匹配。 剩下的都放到其他。
	 * @param department
	 * @return
	 */
	private int convertUserType(String job, String positionname, String unname) {
		if (job != null && job.contains(MeixConstants.ANALYST_KEY)) {
			return MeixConstants.USER_TYPE_1;
		}
		if (positionname != null && positionname.contains(MeixConstants.ANALYST_KEY)) {
			return MeixConstants.USER_TYPE_1;
		}
		if (unname != null && unname.contains(MeixConstants.SALER_KEY)) {
			return MeixConstants.USER_TYPE_2;
		}
		return MeixConstants.USER_TYPE_5;
	}

	/**
	 * 员工状态
	 *
	 * @param inservice
	 * @return 在职 0，否则 1
	 */
	private Integer convertUserState(String inservice) {
		if (!StringUtils.equals(inservice, MeixConstants.INSERVICE)) {
			return 1;
		}
		return 0;
	}

	/**
	 * 初始化缓存
	 *
	 * @return
	 */
	private String initCache() {
		String msg1 = null;
		String msg2 = null;
		String msg3 = null;
		String msg4 = null;
		String msg5 = null;
		try {
			msg1 = initCompanyAbbr(); // 公司名称
			msg2 = initDepartment(); // 部门信息
			msg3 = initInstitute(); // 研究所
			msg4 = initCompany(); // 公司
			msg5 = initDirectionAndPositionCode(); // 初始化研究方向和职位code
		} catch (Exception e) {
			log.warn("同步内部用户时加载初始化信息异常：", e);
		}
		StringBuilder sb = new StringBuilder();
		sb.append(msg1).append(msg2).append(msg3).append(msg4).append(msg5);
		return sb.toString();
	}
	
	private void freeCache() {
		reportType.clear();
		reportSubType.clear();
		reportSearchType.clear();
	}
	/**
	 * 初始化缓存
	 *
	 * @return
	 */
	private String initReportTypeCache() {
		String msg1 = null;
		String msg2 = null;
		String msg4 = null;
		try {
			msg1 = initReportType(); //一级分类
			msg2 = initReportSubType(); //二级分类
			msg4 = initReportSearchType(); //搜索分类
		} catch (Exception e) {
			log.warn("同步研报时加载初始化信息异常：", e);
		}
		StringBuilder sb = new StringBuilder();
		sb.append(msg1).append(msg2).append(msg4);
		return sb.toString();
	}

	private String initReportType() {
		InsCode record = new InsCode();
		record.setName(MeixConstants.REPORT_TYPE);
		List<InsCode> list = codeMapper.select(record);
		if (list != null && !list.isEmpty()) {
			reportType = new HashMap<>();
			for (InsCode tmp : list) {
				reportType.put(tmp.getCode(), tmp);
			}
			return "";
		} else {
			return "初始化研报大类失败";
		}
	}
	private String initReportSubType() {
		InsCode record = new InsCode();
		record.setName(MeixConstants.REPORT_SUB_CATALOG);
		List<InsCode> list = codeMapper.select(record);
		if (list != null && !list.isEmpty()) {
			reportSubType = new HashMap<>();
			for (InsCode tmp : list) {
				reportSubType.put(tmp.getCode(), tmp);
			}
			return "";
		} else {
			return "初始化研报子类失败";
		}
	}
	
	private String initReportSearchType() {
		reportSearchType = new HashMap<>();
		InsCode record = new InsCode();
		record.setName(MeixConstants.REPORT_SEARCH_TYPE);
		List<InsCode> list = codeMapper.select(record);
		if (list != null && !list.isEmpty()) {
			for (InsCode tmp : list) {
				reportSearchType.put(tmp.getCode(), tmp);
			}
		}
		return "";
	}
	
	private String initDirectionAndPositionCode() {
		Example example = new Example(InsCode.class);
		List<String> names = new ArrayList<String>();
		names.add(MeixConstants.DIRECTION);
		names.add(MeixConstants.POSITION);
		example.createCriteria().andIn("name", names);
		List<InsCode> records = codeMapper.selectByExample(example);
		if (records != null && records.size() > 0) {
			directionAndPositionCodes = records.stream().map(InsCode::getCode).collect(Collectors.toSet());
		} else {
			directionAndPositionCodes = new HashSet<String>();
		}
		return "";
	}

	private String initInstitute() {
		InsCode record = new InsCode();
		record.setName(MeixConstants.INSTITUTE);
		record = codeMapper.selectOne(record);
		institute = record.getCode();
		if (StringUtils.isBlank(institute)) {
			return "初始化研究所code失败！";
		}
		return "";
	}

	private String initCompany() {
		InsCode record = new InsCode();
		record.setName(MeixConstants.COMPANY);
		record = codeMapper.selectOne(record);
		company = record.getCode();
		if (StringUtils.isBlank(company)) {
			return "初始化公司code失败！";
		}
		return "";
	}

	/**
	 * 初始化部门信息
	 */
	private String initDepartment() {
		InsCode record = new InsCode();
		record.setName(MeixConstants.DEPARTMENT);
		List<InsCode> departments = codeMapper.select(record);
		departments.forEach((department) -> {
			if (StringUtils.equals(MeixConstants.DEPARTMENT_ANALYST, department.getUdf1())) {
				department_analyst = department.getCode();
			} else if (StringUtils.equals(MeixConstants.DEPARTMENT_MARKET, department.getUdf1())) {
				department_market = department.getCode();
			} else if (StringUtils.equals(MeixConstants.DEPARTMENT_OPERATE, department.getUdf1())) {
				department_operate = department.getCode();
			} else if (StringUtils.equals(MeixConstants.DEPARTMENT_SYSTEM_MANAGE, department.getUdf1())) {
				department_system_manage = department.getCode();
			}
		});
		if (StringUtils.isBlank(department_analyst) || StringUtils.isBlank(department_market)
			|| StringUtils.isBlank(department_operate) || StringUtils.isBlank(department_system_manage)) {
			return "初始化部门信息失败！";
		}
		return "";
	}

	/**
	 * 初始化公司名称
	 */
	private String initCompanyAbbr() {
		ins_companyAbbr = companySerciveImpl.getCompanyInfoById(commonServerProperties.getCompanyCode()).getCompanyAbbr();
		if (StringUtils.isBlank(ins_companyAbbr)) {
			return "初始化公司信息失败！";
		}
		return "";
	}

	/**
	 * 保存内部用户
	 *
	 * @param list
	 */
	private void saveInnerUser(List<XnUserVo> list) {
		List<InsCode> tempCodes = new ArrayList<InsCode>();
		if (list == null || list.size() < 1) {
			return;
		}
		list.forEach((bgUserVo) -> {
			createStaff(bgUserVo, tempCodes);
		});
		if (tempCodes != null && tempCodes.size() > 0) {
			codeMapper.insertList(tempCodes);
			tempCodes.clear();
		}
	}

}
