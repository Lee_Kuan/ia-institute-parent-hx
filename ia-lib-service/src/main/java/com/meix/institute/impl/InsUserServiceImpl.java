package com.meix.institute.impl;

import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.meix.institute.api.*;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.Key;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.*;
import com.meix.institute.mapper.InsUserDirectionMapper;
import com.meix.institute.mapper.InsUserLabelMapper;
import com.meix.institute.mapper.InsUserMapper;
import com.meix.institute.mapper.SecuMapper;
import com.meix.institute.pojo.InsUserInfo;
import com.meix.institute.pojo.InsUserS;
import com.meix.institute.pojo.InsUserSearch;
import com.meix.institute.util.*;
import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.user.InsUserLabelVo;
import com.meix.institute.vo.user.UserInfo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * Created by zenghao on 2019/8/1.
 */
@Service
public class InsUserServiceImpl implements IInsUserService {

	@Autowired
	private InsUserMapper insUserMapper;
	@Autowired
	private InsUserLabelMapper insUserLabelMapper;
	@Autowired
	private ISecumainService secumainService;
	@Autowired
	private SecuMapper secuMapper;
	@Autowired
	private IUserDao userDao;
	@Autowired
	private IDictTagService dictTagService;
	@Autowired
	private ICodeService codeService;
	@Autowired
	private IUserService userServiceImpl;
	@Autowired
	private ICompanyService companySerciveImpl;
	@Autowired
	private InsUserDirectionMapper userDirectionMapper;
	@Autowired
	private ICustomerGroupService groupService;

	@Value("${ins.defaultPassword}")
	private String defaultPassword;
	@Value("${ins.defaultUser}")
	private String defaultUser;

	@Override
	public InsUser getUserInfo(long uid) {
		InsUser userInfo = insUserMapper.selectByPrimaryKey(uid);
		return userInfo;
	}

	@Override
	public InsUser getUserInfo(String uuid) {
		Example example = new Example(InsUser.class);
		example.createCriteria().andEqualTo("user", uuid);
		return insUserMapper.selectOneByExample(example);
	}

	@Override
	public void updateUserStatus(long uid, int userStatus, String optUser) {
		InsUser userInfo = new InsUser();
		userInfo.setId(uid);
		userInfo.setUserState(userStatus);
		userInfo.setUpdatedBy(optUser);
		userInfo.setUpdatedAt(new Date());
		insUserMapper.updateByPrimaryKeySelective(userInfo);
	}

	@Override
	public int countByDuplicate(InsUserSearch search) {
		InsUserS recordS = new InsUserS();
		recordS.setMobileOrEmail(search.getMobileOrEmail());
		InsUser resp = insUserMapper.getUserByMobileOrEmail(recordS);
		if (resp == null) {
			return 0;
		}
		return 1;
	}

	@Override
	public Key save(InsUserInfo insUserInfo, String user) {
		Long id = insUserInfo.getId();
		Date current = new Date();
		InsUser record = new InsUser();
		BeanUtils.copyProperties(insUserInfo, record);

		Key key = new Key();

		if (id != null && id != 0l) { // 更新
			// 设置更新时间
			record.setPassword(null);//防止密码被更新
			record.setUpdatedAt(current);
			record.setUpdatedBy(user);

			insUserMapper.updateByPrimaryKeySelective(record);

			key.setId(id);
			key.setCode("");

		} else {  // 插入
			String _new = UuidUtil.generate32bitUUID();
			record.setUser(_new);
			insUserInfo.setUser(_new);

			// 生成密码
			String salt = RandomUtil.generateRandom(5);
			record.setSalt(salt);
			record.setPassword(MD5Util.md5(salt + insUserInfo.getPassword()));

			// 操作状态
			record.setCreatedAt(current);
			record.setCreatedBy(user);
			record.setUpdatedAt(current);
			record.setUpdatedBy(user);
			insUserMapper.insert(record);

			key.setId(record.getId());
			key.setCode(user);
			
			// 自动注册到用户表
			UserInfo userInfo = new UserInfo();
			userInfo.setUserName(record.getUserName());
			userInfo.setCompanyCode(record.getCompanyCode());
			userInfo.setCompanyAbbr(companySerciveImpl.getCompanyInfoById(record.getCompanyCode()).getCompanyAbbr());
			userInfo.setMobile(record.getMobile());
			userInfo.setPosition(record.getPosition());
			userInfo.setEmail(record.getEmail());
			userInfo.setIsDelete(record.getUserState());
			userInfo.setChiSpelling(record.getChiSpelling());
			userInfo.setAccountType(1);
			userInfo.setAuthStatus(3);
			userInfo.setThirdId(record.getThirdId());
			userInfo.setInsUuid(record.getUser());
			userInfo.setComment(record.getDescr());
			userServiceImpl.saveThirdUser(userInfo);
		}

		return key;
	}

	@Override
	public void resetPwd(long uid, String password, String user) {
		if (StringUtils.isBlank(password)) {
			password = defaultPassword;
		}
		InsUser record = new InsUser();
		// 生成密码
		String salt = RandomUtil.generateRandom(5);
		record.setSalt(salt);
		record.setPassword(MD5Util.md5(salt + password));
		record.setId(uid);
		record.setUpdatedAt(new Date());
		record.setUpdatedBy(user);
		insUserMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public InsUserInfo selectByPrimaryKey(long uid) {
		InsUser record = insUserMapper.selectByPrimaryKey(uid);
		if (record == null) {
			return null;
		}
		InsUserInfo info = new InsUserInfo();
		BeanUtils.copyProperties(record, info, "password", "salt");

		if (StringUtils.isNotBlank(info.getPosition())) {
			info.setPositionText(dictTagService.getText(MeixConstants.POSITION, info.getPosition()));
		}

		if (info.getRole() != null) {
			info.setRoleText(dictTagService.getText(MeixConstants.ROLE, info.getRole().toString()));
		}
		//by zenghao 2020-05-07 研究方向直接使用ia_user表数据，根据研究方向设置分析师
		InsUserDirection drecord = new InsUserDirection();
		drecord.setUid(uid);
		List<InsUserDirection> dlist = userDirectionMapper.select(drecord);
		if (CollectionUtils.isNotEmpty(dlist)) {
			int size = dlist.size();
			for (int i = 0; i < size; i++) {
				InsUserDirection insUserDirection = dlist.get(i);
				InsCode insCode = codeService.selectByBizKey(MeixConstants.DIRECTION, insUserDirection.getDirection());
				if (insCode != null) {
					if ("1".equals(insCode.getUdf2())) {
						info.setPositionText("分析师");
						break;
					} else if ("2".equals(insCode.getUdf2())) {
						info.setPositionText("首席分析师");
						break;
					}
				}
			}
		}
		return info;
	}

	@Override
	public Paged<Map<String, Object>> getContactList(Map<String, Object> params) {
		String condition = MapUtils.getString(params, "condition"); // 查询条件
		String position = MapUtils.getString(params, "position");
		int showNum = MapUtils.getIntValue(params, "showNum", 10);
		int currentPage = MapUtils.getIntValue(params, "currentPage", 0);
		int userType = MapUtils.getIntValue(params, "userType", 0);
		long groupId = MapUtils.getLongValue(params, "groupId", 0);
		int isExcept = MapUtils.getIntValue(params, "isExcept", 0);

		Example example = new Example(InsUser.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("userState", 0); // 未删除账户
		if (userType > 0) criteria.andEqualTo("userType", userType); // 用户类型
		criteria.andNotEqualTo("user", defaultUser);
		if (StringUtils.isNotBlank(position)) {
			criteria.andEqualTo("position", position); // 职位
		}
		if (groupId != 0) {
			List<Long> ids = groupService.getGroupUserList(groupId);
			if (ids != null && !ids.isEmpty()) {
				if (isExcept == 0) {
					criteria.andIn("id", ids);
				} else {
					criteria.andNotIn("id", ids);
				}
			} else if (isExcept == 0) {
				Paged<Map<String, Object>> paged = new Paged<Map<String, Object>>();
				paged.setCount(0);
				paged.setList(Collections.emptyList());
				return paged;
			}
		}
		if (StringUtil.isNotEmpty(condition)) {
			condition = "%" + condition + "%";
			example.and().andLike("userName", condition).orLike("mobile", condition.toUpperCase()).orLike("chiSpelling", condition.toUpperCase());
		}
		
		RowBounds rowBounds = new RowBounds(currentPage, showNum);
		List<InsUser> list = insUserMapper.selectByExampleAndRowBounds(example, rowBounds);
		Paged<Map<String, Object>> paged = new Paged<Map<String, Object>>();
		if (list == null || list.size() == 0) {
			paged.setCount(0);
			paged.setList(Collections.emptyList());
			return paged;
		}
		int count = insUserMapper.selectCountByExample(example);
		List<Map<String, Object>> result = new ArrayList<>();
		for (InsUser item : list) {
			Map<String, Object> map = new HashMap<>();
			map.put("user", item.getUser());
			map.put("contactId", item.getId());
			map.put("contactName", item.getUserName());
			map.put("email", item.getEmail());
			map.put("position", item.getPosition());
			if (StringUtils.isNotBlank(item.getPosition())) {
				map.put("positionText", dictTagService.getText(MeixConstants.POSITION, item.getPosition()));
			}
			map.put("mobile", item.getMobile());
			map.put("role", item.getRole());
			map.put("userType", item.getUserType());
			if (item.getRole() != null) {
				map.put("roleText", dictTagService.getText(MeixConstants.ROLE, item.getRole().toString()));
			}
			map.put("direction", item.getDirection());
			map.put("salerId", item.getId());
			UserInfo userInfo = userDao.getUserInfoByUuid(item.getUser());
			if (userInfo != null) {
				map.put("customerId", userInfo.getId()); //customerId
				map.put("groupName", groupService.getGroupNameListByUser(userInfo.getId()));
			}
			result.add(map);
		}
		paged.setCount(count);
		paged.setList(result);
		return paged;
	}

	@Override
	public InsUserInfo getUserByMobileOrEmail(String mobileOrEmail, Integer userState) {
		InsUserS search = new InsUserS();
		search.setMobileOrEmail(mobileOrEmail);
		if (null != userState) {
			search.setUserState(userState);
		}
		InsUser record = insUserMapper.getUserByMobileOrEmail(search);
		if (record != null) {
			InsUserInfo info = new InsUserInfo();
			BeanUtils.copyProperties(record, info, new String[]{"password", "salt"});
			return info;
		}
		return null;
	}

	@Override
	public Paged<Map<String, Object>> getNotAuditCustomerList(InsUserSearch search, String user) {
		InsUserS daoS = new InsUserS();
		BeanUtils.copyProperties(search, daoS);
		List<Map<String, Object>> list = insUserMapper.getNotAuditCustomerList(daoS);
		Paged<Map<String, Object>> paged = new Paged<Map<String, Object>>();
		if (list == null || list.size() == 0) {
			paged.setCount(0);
			paged.setList(Collections.emptyList());
			return paged;
		}
		int count = insUserMapper.countNotAuditCustomerList(daoS);

		list.forEach((item) -> {
			//item.put("url", "");
		});
		paged.setCount(count);
		return paged;
	}

	@Override
	public void auditCustomer(UserInfo userInfo, String user) {
		Integer isUsed = userInfo.getAuthStatus();

		if (!(1 == isUsed || 0 == isUsed)) {
			return;
		}

		if (1 == isUsed) { // 通过
			// 更新用户信息
//			UserInfo data = new UserInfo();
/*            data.setUserName(userInfo.getUserName());
            data.setCompanyCode(companyCode);
            data.setCompanyAbbr(companyAbbr);
            data.setPosition(position);
            data.setCardID(cardID);*/
			userInfo.setAuthStatus(3);
			userDao.updateUser(userInfo);
		}

		// 更新名片状态
		insUserMapper.updateCustomerCard(isUsed, userInfo.getCardID());
	}

	@Override
	public Paged<Map<String, Object>> getUserLableList(Map<String, Object> search, String user) {
		List<Map<String, Object>> list = insUserMapper.getUserLableList(search);
		Paged<Map<String, Object>> paged = new Paged<Map<String, Object>>();
		if (list == null || list.size() == 0) {
			paged.setCount(0);
			paged.setList(Collections.emptyList());
			return paged;
		}
		int count = insUserMapper.countUserLable(search);

		list.forEach((item) -> {
			String userName = MapUtils.getString(item, "userName");
			String nickName = MapUtils.getString(item, "nickName");
			if (StringUtils.isBlank(userName) || MeixConstants.DEFAULT_NAME.equals(userName)) {
				if (StringUtils.isNotBlank(nickName)) {
					item.put("userName", nickName);
				}
			}
			//item.put("url", "");
		});
		paged.setList(list);
		paged.setCount(count);
		return paged;
	}


	@Override
	public List<UserInfo> getCustomerList(int currentPage, int showNum, String condition) {
		Map<String, Object> params = new HashMap<>();
		params.put("currentPage", currentPage * showNum);
		params.put("showNum", showNum);
		params.put("condition", condition);
		return userDao.getUser(params);
	}


	@Override
	public void addUserLable(Map<String, Object> data, String user) {
		// 参数
		Long contactId = MapUtils.getLong(data, "contactId");
		String labelCode = MapUtils.getString(data, "labelCode");
		Integer labelType = MapUtils.getInteger(data, "labelType");
		Integer createVia = MapUtils.getInteger(data, "createVia");

		String[] split = StringUtils.split(labelCode, ",");

		for (int i = 0, length = split.length; i < length; i++) {
			InsUserLabel record = new InsUserLabel();
			record.setUid(contactId);
			record.setLabelCode(split[i]);
			record.setLabelType(labelType);

			// 校验
			int count = insUserLabelMapper.selectCount(record);
			if (count > 0) {
				continue;
			}

			record.setScore(50f);
			if (createVia == null || createVia == 0) {
				record.setCreateVia(MeixConstants.CREATED_VIA_2); //手工创建
			}
			record.setCreatedBy(user);
			record.setCreatedAt(new Date());
			record.setUpdatedBy(user);
			record.setUpdatedAt(new Date());
			insUserLabelMapper.insert(record);
		}
	}

	@Override
	public void deleteUserLable(Long labelId, String user) {
		// 删除
		insUserLabelMapper.deleteByPrimaryKey(labelId);
	}

	@Override
	public List<InsUserLabelVo> getUserLabelByContactId(long contactId, String user) {
		Example example = new Example(InsUserLabel.class);
		example.createCriteria().andEqualTo("uid", contactId);
		example.orderBy("score").desc();
		example.excludeProperties(new String[]{"createdAt", "createdBy", "updatedAt", "updatedBy"});
		List<InsUserLabel> list = insUserLabelMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(list)) {
			return Collections.emptyList();
		}
		List<InsUserLabelVo> data = new ArrayList<InsUserLabelVo>();
		list.forEach((item) -> {
			InsUserLabelVo vo = new InsUserLabelVo();
			BeanUtils.copyProperties(item, vo);
			convertUserLabelVo(data, vo);
		});
		return data;
	}

	private void convertUserLabelVo(List<InsUserLabelVo> data, InsUserLabelVo vo) {
		int i = 0, j = 0, k = 0;
		SecuMain secuMain = null;
		InsCompsIndustry compsIndustry = null;
		UserInfo userInfo = null;
		if (i < 5 && 2 == vo.getLabelType()) { // 股票
			secuMain = secumainService.getSecuMain(Integer.valueOf(vo.getLabelCode()));
			if (secuMain != null) {
				vo.setLabelName(secuMain.getSecuAbbr());
				data.add(vo);
			}
			i++;
		} else if (j < 5 && 3 == vo.getLabelType()) { // 行业
			compsIndustry = secuMapper.getCompsIndustryByLabelId(vo.getLabelCode());
			if (compsIndustry != null) {
				vo.setLabelName(compsIndustry.getLabelName());
				data.add(vo);
			}
			j++;
		} else if (k < 5 && 0 == vo.getLabelType()) { //作者
			userInfo = userDao.getUserInfo(Integer.valueOf(vo.getLabelCode()), null, null);
			if (userInfo != null) {
				vo.setLabelName(userInfo.getUserName() + "，" + userInfo.getPosition());
				data.add(vo);
			}
		}
	}

	@Override
	public UserInfo selectCustomerInfoById(long id, String user) {
		UserInfo userInfo = insUserMapper.selectCustomerInfoById(id);
		if (StringUtils.isBlank(userInfo.getUserName()) || MeixConstants.DEFAULT_NAME.equals(userInfo.getUserName())) {
			if (StringUtils.isNotBlank(userInfo.getNickName())) {
				userInfo.setUserName(userInfo.getNickName());
			}
		}
		String headImageUrl = OssResourceUtil.getHeadImage(userInfo.getHeadImageUrl());
		if (StringUtils.isBlank(headImageUrl)) {
			headImageUrl = userInfo.getWechatHeadUrl();
		}
		userInfo.setHeadImageUrl(headImageUrl);
		return userInfo;
	}

	@Override
	public List<Map<String, Object>> getUserFocusLabel(String user) {
		return insUserLabelMapper.getUserFocusLabel();
	}

	@Override
	public float getUserLabelTotalScore(long uid, int innerCode, int industryCode) {
		Example example = new Example(InsUserLabel.class);
		example.createCriteria().andEqualTo("uid", uid).andEqualTo("labelType", 2).andEqualTo("labelCode", innerCode);
		example.or().andEqualTo("uid", uid).andEqualTo("labelType", 3).andEqualTo("labelCode", industryCode);
		List<InsUserLabel> labelList = insUserLabelMapper.selectByExample(example);
		if (labelList == null || labelList.size() == 0) {
			return 0;
		}
		float score = 0;
		for (InsUserLabel item : labelList) {
			score += item.getScore();
		}
		return score;
	}

	@Override
	public List<String> saveInsUser(String uuid, long companyCode, JSONArray datas) {
		List<String> errInfo = new ArrayList<>();
		List<InsCode> positions = new ArrayList<InsCode>();
		Map<String, String> position_ids = new HashMap<>();
		for (int i = 0; i < datas.size(); i++) {

			JSONObject obj = (JSONObject) datas.get(i);

			// 员工参数
			InsUserInfo insUserInfo = new InsUserInfo();
			insUserInfo.setUserName(MapUtils.getString(obj, "userName"));
			insUserInfo.setChiSpelling(PinyinHelper.getShortPinyin(insUserInfo.getUserName()).toUpperCase());
			insUserInfo.setPassword(defaultPassword); // 默认密码
			insUserInfo.setCompanyCode(companyCode);
			insUserInfo.setMobile(MapUtils.getString(obj, "mobile"));
			insUserInfo.setEmail(MapUtils.getString(obj, "email"));
			insUserInfo.setDirection(MapUtils.getString(obj, "direction"));
			insUserInfo.setCheckStatus(3); //分析师审核状态(0: 初始化, 1: 审核中, 2: 审核失败, 3: 审核成功
			insUserInfo.setUserState(0); // 用户在职状态 0-在职，1-离职

			String position = MapUtils.getString(obj, "position");
			String positionId = null;
			if (StringUtils.isNotBlank(position)) {
				if (!position_ids.containsKey(position)) {
					// 职位
					InsCode code = new InsCode();
					code.setName(MeixConstants.POSITION);
					positionId = codeService.getCode(MeixConstants.POSITION, position);
					if (StringUtils.isBlank(positionId)) {
						positionId = UUID.randomUUID().toString().replaceAll("-", "");
						code.setCode(positionId);
						code.setText(position);
						positions.add(code);
					}
					position_ids.put(position, positionId);
				} else {
					positionId = position_ids.get(position);
				}
			}
			insUserInfo.setPosition(positionId);

			// 根据第三方id获取用户信息
			InsUser result = insUserMapper.selectByThirdId(insUserInfo.getThirdId());
			if (result != null && result.getId() != null) {
				insUserInfo.setId(result.getId());
				insUserInfo.setPassword(defaultPassword); // 默认密码
				insUserInfo.setCompanyCode(Long.valueOf(companyCode));
			}

			// 设置用户角色、类型
			if (StringUtils.contains(MapUtils.getString(obj, "role"), "销售")) {
				insUserInfo.setUserType(MeixConstants.USER_TYPE_2);
				insUserInfo.setRole(MeixConstants.USER_ROLE_4);
			} else {
				insUserInfo.setUserType(MeixConstants.USER_TYPE_3);
				insUserInfo.setRole(MeixConstants.USER_ROLE_1);
			}
			// 处理分析师
			if (StringUtils.contains(MapUtils.getString(obj, "role"), "分析师")) {
				insUserInfo.setUserType(MeixConstants.USER_TYPE_1);
				insUserInfo.setRole(MeixConstants.USER_ROLE_1);
			}

			this.save(insUserInfo, uuid); // 保存用户

			// 自动注册到用户表
			UserInfo userInfo = new UserInfo();
			userInfo.setUserName(insUserInfo.getUserName());
			userInfo.setCompanyCode(insUserInfo.getCompanyCode());
			CompanyInfoVo companyInfo = companySerciveImpl.getCompanyInfoById(insUserInfo.getCompanyCode());
			if (companyInfo != null) {
				userInfo.setCompanyAbbr(companyInfo.getCompanyAbbr());
			}
			userInfo.setMobile(insUserInfo.getMobile());
			userInfo.setPosition(position);
			userInfo.setEmail(insUserInfo.getEmail());
			userInfo.setIsDelete(insUserInfo.getUserState());
			userInfo.setChiSpelling(insUserInfo.getChiSpelling());
			userInfo.setHeadImageUrl(OssResourceUtil.getHeadImage(insUserInfo.getHeadImageUrl()));
			userInfo.setAccountType(1);
			userInfo.setAuthStatus(3);
			userInfo.setThirdId(insUserInfo.getThirdId());
			userInfo.setInsUuid(insUserInfo.getUser());
			userInfo.setComment(insUserInfo.getDescr());
			userServiceImpl.saveThirdUser(userInfo);
		}

		if (positions != null && positions.size() > 0) {
			codeService.insertList(positions);
		}
		return errInfo;
	}

	@Override
	public void updateUserRole(long uid, int role, String user) {
		InsUser userInfo = new InsUser();
		userInfo.setId(uid);
		userInfo.setRole(role);
		userInfo.setUpdatedBy(user);
		userInfo.setUpdatedAt(new Date());
		insUserMapper.updateByPrimaryKeySelective(userInfo);
	}

	@Override
	public void deleteUser(String user, String uuid) {
		InsUser userInfo = new InsUser();
		userInfo.setUserState(1);
		userInfo.setUpdatedBy(uuid);
		userInfo.setUpdatedAt(new Date());
		Example example = new Example(InsUser.class);
		example.createCriteria().andEqualTo("user", user);
		insUserMapper.updateByExampleSelective(userInfo, example);
	}

}
