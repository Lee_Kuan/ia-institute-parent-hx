package com.meix.institute.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.BaseService;
import com.meix.institute.api.ICustomerGroupService;
import com.meix.institute.api.IReportService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.mapper.CustomerGroupMapper;
import com.meix.institute.vo.CustomerGroupPowerVo;
import com.meix.institute.vo.CustomerGroupVo;
import com.meix.institute.vo.DataShareVo;
import com.meix.institute.vo.GroupPowerVo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * @Description:用户组服务
 */
@Service
public class CustomerGroupServiceImpl extends BaseService implements ICustomerGroupService {

	@Autowired
	private CustomerGroupMapper groupMapper;
	@Autowired
	private IReportService reportService;
	
	@Override
	public void saveUserGroup(long uid, long groupId, long fromGroup, String user) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("groupId", groupId);
		params.put("user", user);
		params.put("fromGroup", fromGroup);
		if (!groupMapper.checkUserCanInGroup(uid, groupId)) {//用户与组类别校验
			return;
		}
		if (groupMapper.checkUserIsInGroup(uid, groupId)) {//用户已经存在目标组中
			if (fromGroup > 0) {
				groupMapper.deleteCustomerFromGroup(uid, fromGroup);//从原组中删除
			}
		} else if (fromGroup == 0 || groupMapper.updateUserGroup(params) == 0) {//将用户从fromGroup中修改为groupId中
			groupMapper.addUserGroup(params);
		}
	}

	@Override
	public List<Map<String, Object>> getCustomerGroupList(int groupType, int isSelect, int showNum, int currentPage) {
		Map<String, Object> params = new HashMap<>();
		params.put("groupType", groupType);
		params.put("isSelect", isSelect);
		params.put("showNum", showNum);
		params.put("currentPage", showNum * currentPage);
		return groupMapper.getCustomerGroupList(params);
	}

	@Override
	public int getCustomerGroupCount(int groupType) {
		Map<String, Object> params = new HashMap<>();
		params.put("groupType", groupType);
		return groupMapper.getCustomerGroupCount(params);
	}

	@Override
	public int deleteCustomerGroup(long groupId, String uuid) {
		int count = groupMapper.deleteCustomerGroup(groupId);
		if (count == 0) {
			return 0;
		}
//		userDaoImpl.deleteUserList(groupId, uuid, null);
		groupMapper.deleteGroupCustomerRela(groupId, null);
		groupMapper.deleteCustomerGroupPower(groupId);
		return count;
	}

	@Override
	public long saveCustomerGroup(long groupId, String groupName, int groupType, String uuid, JSONArray powers) {
		Map<String, Object> params = new HashMap<>();
		params.put("groupName", groupName);
		params.put("groupType", groupType);
		params.put("user", uuid);
		if (StringUtils.isNotBlank(groupName)) {
			
			int exist = groupMapper.checkGroupName(groupName, groupId);
			if (exist > 0) {
				return -1;
			}
			if (groupId == 0) {
				//新增用户组
				groupId = com.meix.institute.util.StringUtils.getId(commonServerProperties.getCompanyCode());
				params.put("groupId", groupId);
				groupMapper.addGroupInfo(params);
			} else {
				//修改用户组
				params.put("groupId", groupId);
				groupMapper.updateGroupInfo(params);
			}
		}
		
		//删除组的原有权限信息
		Map<String, Object> params1 = new HashMap<>();
		params1.put("powerId", 0);
		params1.put("groupId", groupId);
		params1.put("isSelect", 0);
		params1.put("user", uuid);
		groupMapper.updateGroupPower(params1);
		
		if (powers != null && !powers.isEmpty()) {
			for (Object tmp : powers) {
				JSONObject obj = JSONObject.fromObject(tmp);
				long powerId = MapUtils.getLongValue(obj, "powerId", 0);
				int isSelect = MapUtils.getIntValue(obj, "isSelect", 0);
				saveGroupPower(groupId, powerId, isSelect, uuid);
			}
		}
		return groupId;
	}

	@Override
	public void saveCustomerGroupUser(long groupId, JSONArray ids, String user, long fromGroup) {
		if (ids != null && !ids.isEmpty()) {
			//将用户转移到用户组
			for (Object id : ids) {
				if (String.valueOf(id) != null && !"".equals(String.valueOf(id)) && !"null".equals(String.valueOf(id))) {
					saveUserGroup(Long.valueOf(String.valueOf(id)), groupId, fromGroup, user);
				}
			}
		}
	}

	private void saveGroupPower(long groupId, long powerId, int isSelect, String user) {
		Map<String, Object> params = new HashMap<>();
		params.put("powerId", powerId);
		params.put("groupId", groupId);
		params.put("isSelect", isSelect);
		params.put("user", user);
		if (groupMapper.updateGroupPower(params) == 0) {
			groupMapper.addGroupPower(params);
		}
	}

	@Override
	public int saveDataGroupShare(long dataId, int dataType, int dataSubType, JSONArray shareList) {
		int resShare = 1;
		List<GroupPowerVo> list = groupMapper.getDataPowerLeaf(dataType, dataSubType);
		list = list == null ? new ArrayList<>() : list;
		if (dataId > 0) {
			groupMapper.deleteDataGroupShare(dataId, dataType);
		}
		if (shareList != null && !shareList.isEmpty()) {
			List<Map<String, Object>> ids = new ArrayList<>();
			for (Object obj : shareList) {
				try {
					JSONObject jsonObj = JSONObject.fromObject(obj);
					if (jsonObj.containsKey("share") && jsonObj.containsKey("dm")) {
						int share = jsonObj.getInt("share");
						long dm = jsonObj.getLong("dm");
						//-1 默认授权，1 组授权
						if (share == -1 || (share == 1 && dm > 0) || (share == 4 && dm > 0) || share == 0) {
							Map<String, Object> map = new HashMap<>();
							map.put("share", share);
							map.put("dm", share == -1 || share == 0 ? 0 : dm);
							int location = 0;
							if (jsonObj.containsKey("location")) {
								location = jsonObj.getInt("location");
							}
							map.put("location", location);
							if (share == 0) {
								if (dataType == MeixConstants.USER_HD) { //活动不拆分，有公开授权则为公开
									list.clear();
								} else {
									dealPowerLeaf(list, location);
									//研报的原文（pdf）权限只有一个生效，另一个直接移除
									if (dataType == MeixConstants.USER_YB && (location == MeixConstants.SHARE_LIVE || location == MeixConstants.SHARE_REPLAY)) {
										dealPowerLeaf(list, MeixConstants.SHARE_LIVE + MeixConstants.SHARE_REPLAY - location);
									}
								}
							}
							ids.add(map);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (!ids.isEmpty()) {
				groupMapper.addDataGroupShare(dataId, dataType, dataSubType, ids);
			}
		}
		if (list.isEmpty()) {
			resShare = 0; //公开
		}
		return resShare;
	}
	
	/**
	 * 将公开部分的叶子节点移除
	 * @param list
	 * @param share
	 */
	private void dealPowerLeaf(List<GroupPowerVo> list, int share) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getShare() == share) {
				list.remove(i);
				i --;
			}
		}
	}

	@Override
	public List<GroupPowerVo> getResourceGroupPower(long uid, long dataId, int dataType, int dataSubType, int share) {
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("dataId", dataId);
		params.put("dataType", dataType);
		params.put("dataSubType", dataSubType);
		params.put("share", share);
		List<GroupPowerVo> list = groupMapper.getResourceGroupPower(params);
		List<GroupPowerVo> tmpList = new ArrayList<>();
		if (list != null && !list.isEmpty()) {
			for (GroupPowerVo vo : list) {
				if (vo.getLevel() > 0) {
					GroupPowerVo parentPower = getParent(list, vo);
					if (parentPower != null && vo.getPower() == 1 && parentPower.getPower() == 0) {
						tmpList.add(vo);
					}
				}
			}
			while(tmpList.size() > 0) {
				GroupPowerVo tmp = tmpList.get(0);
				tmp.setPower(1);//父节点授权
				if (tmp.getLevel() > 0) {
					GroupPowerVo parentPower = getParent(list, tmp);
					if (parentPower != null && tmp.getPower() == 1 && parentPower.getPower() == 0) {
						tmpList.add(tmp);
					}
				}
				tmpList.remove(0);
			}
		}
		return list;
	}
	
	private GroupPowerVo getParent(List<GroupPowerVo> list, GroupPowerVo tmp) {
		for (GroupPowerVo vo : list) {
			if (vo.getId() == tmp.getLevel()) {
				return vo;
			}
		}
		return null;
	}

	@Override
	public List<CustomerGroupVo> getDefaultPowerGroup(int dataType, int dataSubType, int share) {
		Map<String, Object> params = new HashMap<>();
		params.put("dataType", dataType);
		params.put("dataSubType", dataSubType);
		params.put("share", share);
		return groupMapper.getDefaultPowerGroup(params);
	}

	@Override
	public List<Long> getGroupUserList(long groupId) {
		return groupMapper.getGroupUserList(groupId);
	}

	@Override
	public Map<String, Object> getCustomerGroupInfo(long groupId) {
		return groupMapper.getCustomerGroupName(groupId);
	}

	@Override
	public List<Map<String, Object>> getDataShareList(long dataId, int dataType, int dataSubType, int location) {
		int activityLocation = -1;
		if (dataType == MeixConstants.USER_HD) {
			activityLocation = location == -1 ? 0 : location;
			location = location > -1 ? location : MeixConstants.SHARE_DETAIL; //活动默认获取详情权限
		}
		List<DataShareVo> list = groupMapper.getDataShareList(dataId, dataType, dataSubType, location);
		int deepStatus = -1;
		if (dataId > 0 && dataType == MeixConstants.USER_YB) {
			deepStatus = reportService.getReportDeepStatus(dataId);//0-未知，1-普通报告，2-深度报告
			if (deepStatus == 1 || deepStatus == 2) {
				deepStatus = deepStatus + 2; //转为power的share
				deepStatus = MeixConstants.SHARE_LIVE + MeixConstants.SHARE_REPLAY - deepStatus; //share取反
			} else {
				deepStatus = -1;
			}
		}
		List<Map<String, Object>> res = new ArrayList<>();
		if (list!= null && !list.isEmpty()) {
			int ltmp = -1;
			Map<String, Object> map = null;
			List<DataShareVo> defaultList = null;
			List<DataShareVo> groupList = null;
			List<DataShareVo> powerList = null;
			for (DataShareVo tmp : list) {
				if (tmp.getLocation() == deepStatus) {
					continue;
				}
				if (dataType == MeixConstants.USER_HD) {
					if (ltmp == -1) {
						ltmp = tmp.getLocation();
						defaultList = new ArrayList<>();
						groupList = new ArrayList<>();
						powerList = getShareNameCollection();
						if (dataId == 0) { //新建内容默认勾选默认授权
							setPowerListSelect(powerList, -1, 0);
						}
						map = new HashMap<>();
						map.put("powerName", tmp.getPowerName());
						map.put("powerLocation", dataType == MeixConstants.USER_HD ? activityLocation : tmp.getLocation());
						map.put("defaultList", defaultList);
						map.put("groupList", groupList);
						map.put("powerList", powerList);
						map.put("publicFlag", 0);
						res.add(map);
					}
				} else if (tmp.getLocation() != ltmp) {
					ltmp = tmp.getLocation();
					defaultList = new ArrayList<>();
					groupList = new ArrayList<>();
					powerList = getShareNameCollection();
					if (dataId == 0) { //新建内容默认勾选默认授权
						setPowerListSelect(powerList, -1, 0);
					}
					map = new HashMap<>();
					map.put("powerName", tmp.getPowerName());
					map.put("powerLocation", dataType == MeixConstants.USER_HD ? activityLocation : tmp.getLocation());
					map.put("defaultList", defaultList);
					map.put("groupList", groupList);
					map.put("powerList", powerList);
					map.put("publicFlag", 0);
					res.add(map);
				}
				if (tmp.getShare() == MeixConstants.SHARE_PUBLIC) {
					map.put("publicFlag", 1);
				} else if (tmp.getShare() == MeixConstants.SHARE_DEFAULT) {
					if (tmp.getDm() == 0) { //默认授权
						setPowerListSelect(powerList, tmp.getShare(), tmp.getDm());
					} else { //有默认权限的组
						defaultList.add(tmp);
					}
				} else if (tmp.getShare() == MeixConstants.SHARE_ACCOUNTTYPE) {
					setPowerListSelect(powerList, tmp.getShare(), tmp.getDm());
				} else if (tmp.getShare() == MeixConstants.SHARE_GROUP) {
					setPowerListSelect(powerList, tmp.getShare(), 0);
					groupList.add(tmp);
				}
			}
		}
		return res;
	}
	private void setPowerListSelect(List<DataShareVo> powerList, int share, long dm) {
		for (DataShareVo vo : powerList) {
			if (vo.getShare() == share && vo.getDm() == dm) {
				vo.setIsSelect(1);
				return;
			}
		}
	}
	@Override
	public List<Map<String, Object>> getCustomerGroupPower(long groupId, int groupType) {
		List<CustomerGroupPowerVo> list = groupMapper.getCustomerGroupPower(groupId, groupType);
		List<Map<String, Object>> res = new ArrayList<>();//大类列表
		if (list!= null && !list.isEmpty()) {
			Map<String, Object> map = null;//大类map
			int dataType = -1;
			int dataSubType = -1;
			List<Map<String, Object>> tmpList = null;//子类列表
			Map<String, Object> tmpMap = null;//子类map
			String[] titles = null;
			List<Map<String, Object>> powerList = null;//权限列表
			Map<String, Object> powerMap = null;//权限map
			for (CustomerGroupPowerVo vo : list) {
				if (vo.getDataType() != dataType) {
					dataType = vo.getDataType();
					dataSubType = -1;
					map = new HashMap<>();
					tmpList = new ArrayList<>();
					res.add(map);
				}
				if (vo.getDataType() == MeixConstants.USER_CH) {
					if (dataSubType == -1) {
						dataSubType = vo.getDataSubType();
						tmpMap = new HashMap<>();
						map.put("listTmpList", tmpList);
						tmpList.add(tmpMap);
						powerList = new ArrayList<>();
					}
				} else if (vo.getDataSubType() != dataSubType) {
					dataSubType = vo.getDataSubType();
					tmpMap = new HashMap<>();
					map.put("listTmpList", tmpList);
					
					tmpList.add(tmpMap);
					powerList = new ArrayList<>();
				}
				if (vo.getLevel() == 0) { //根节点
					powerMap = getPowerMap(list, vo);
					powerList.add(powerMap);
					tmpMap.put("listPowerList", powerList);
					if (vo.getShareName() != null) {
						titles = vo.getShareName().split("-");
					}
					map.put("title", titles == null || titles.length <= 0 ?  "" : titles[0]); //大类名称
					tmpMap.put("title", titles == null || titles.length <= 1 ? "" : titles[1]); //子类名称
					
				}
				
			}
		}
		return res;
	}
	
	private Map<String, Object> getPowerMap(List<CustomerGroupPowerVo> list, CustomerGroupPowerVo tmp) {
		Map<String, Object> powerMap = null;
		List<Map<String, Object>> powerList = new ArrayList<>();
		String[] titles = null;
		for (CustomerGroupPowerVo vo : list) {
			if (vo.getLevel() == tmp.getPowerId()) { //子节点
				powerMap = new HashMap<>();
				powerMap.put("isSelect", tmp.getIsSelect());
				powerMap.put("flag", tmp.getFlag());
				powerMap.put("dataType", tmp.getDataType());
				powerMap.put("dataSubType", tmp.getDataSubType());
				powerMap.put("powerId", tmp.getPowerId());
				powerMap.put("share", tmp.getShare());
				if (vo.getShareName() != null) {
					titles = tmp.getShareName().split("-");
				}
				powerMap.put("title", titles.length > 2 ? titles[2] : ""); //权限名称
				powerList.add(getPowerMap(list, vo));
				powerMap.put("list", powerList);
			}
		}
		if (powerMap == null) {
			powerMap = new HashMap<>();
			powerMap.put("isSelect", tmp.getIsSelect());
			powerMap.put("flag", tmp.getFlag());
			powerMap.put("dataType", tmp.getDataType());
			powerMap.put("dataSubType", tmp.getDataSubType());
			powerMap.put("powerId", tmp.getPowerId());
			powerMap.put("share", tmp.getShare());
			if (tmp.getShareName() != null) {
				titles = tmp.getShareName().split("-");
			}
			powerMap.put("title", titles.length > 2 ? titles[2] : ""); //权限名称
		}
		return powerMap;
	}

	@Override
	public String getGroupNameListByUser(long uid) {
		return groupMapper.getGroupNameListByUser(uid);
	}
}
