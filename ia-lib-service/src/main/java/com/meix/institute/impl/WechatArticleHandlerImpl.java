package com.meix.institute.impl;

import com.meix.institute.api.IArticlePushService;
import com.meix.institute.api.IInstituteService;
import com.meix.institute.api.WechatArticleAction;
import com.meix.institute.api.WechatArticleHandler;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.DataResp;
import com.meix.institute.pojo.InsArticleGroupInfo;
import com.meix.institute.pojo.InsArticleInfo;
import com.meix.institute.pojo.wechat.Material;
import com.meix.institute.util.HttpUtil;
import com.meix.institute.util.WechatUtil;
import com.meix.institute.vo.company.CompanyWechatConfig;

import net.sf.json.JSONObject;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

import static com.meix.institute.util.WechatUtil.PARAM_WX_ACCESS_TOKEN;

@Service
public class WechatArticleHandlerImpl implements WechatArticleHandler {

	@Value("${ins.companyCode}")
	private String appType;

	@Autowired
	private IInstituteService instituteService;
	@Autowired
	private IArticlePushService articlePushService;
	@Autowired
	private CommonServerProperties commonServerProperties;

	private static final Logger log = LoggerFactory.getLogger(WechatArticleHandlerImpl.class);

	@Override
	public DataResp action(WechatArticleAction wechatArticleAction, String user) {
		String action = wechatArticleAction.getAction();
		String type = wechatArticleAction.getType();
		CompanyWechatConfig config = instituteService.getWechatConfigByAppTypeAndClientType(appType, 4);
		String accessToken = WechatUtil.updateAccessToken(config.getCompanyCode(), config.getAppId(), config.getAppSecret(), true);
		DataResp resp = null;
		if (StringUtils.equals(action, MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_UPLOADIMG)) {
			String url = commonServerProperties.getApiServer() + WechatUtil.MEDIA_UPLOADIMG;
			url = url.replaceFirst(PARAM_WX_ACCESS_TOKEN, accessToken);
			resp = uploading((File) wechatArticleAction.getData(), url, user);
		} else if (StringUtils.equals(action, MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_UPLOAD)) {
		    String url = commonServerProperties.getApiServer() + WechatUtil.MATERIAL_UPLOAD;
            url = url.replaceFirst(PARAM_WX_ACCESS_TOKEN, accessToken).replace(WechatUtil.PARAM_WX_TYPE, type);
            resp = upload((File) wechatArticleAction.getData(), url, user);
		} else if (StringUtils.equals(action, MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_ADD_NEW)) {
			String url = commonServerProperties.getApiServer() + WechatUtil.MATERIAL_ADD_NEWS;
			url = url.replaceFirst(PARAM_WX_ACCESS_TOKEN, accessToken);
			resp = addNew((String) wechatArticleAction.getData(), url, user);
		} else if (StringUtils.equals(action, MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_UPLOADNEWS)) {
			String url = commonServerProperties.getApiServer() + WechatUtil.MATERIAL_UPLOADNEWS;
			url = url.replaceFirst(PARAM_WX_ACCESS_TOKEN, accessToken);
			resp = uploadnews((String) wechatArticleAction.getData(), url, user);
		} else if (StringUtils.equals(action, MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_ADD_MATERIAL)) {
			String url = commonServerProperties.getApiServer() + WechatUtil.MATERIAL_ADD_MATERIAL;
			url = url.replaceFirst(PARAM_WX_ACCESS_TOKEN, accessToken);
			resp = addOther((File) wechatArticleAction.getData(), url, user);
		} else if (StringUtils.equals(action, MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_UPDATE_NEW)) {
			String url = commonServerProperties.getApiServer() + WechatUtil.MATERIAL_UPDATE_NEWS;
			url = url.replaceFirst(PARAM_WX_ACCESS_TOKEN, accessToken);
			resp = updateNew((Long) wechatArticleAction.getData(), url, user);
		} else if (StringUtils.equals(action, MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_DELETE_NEW)) {
			String url = commonServerProperties.getApiServer() + WechatUtil.MATERIAL_UPDATE_NEWS;
			url = url.replaceFirst(PARAM_WX_ACCESS_TOKEN, accessToken);
			resp = delete((String) wechatArticleAction.getData(), url, user);
		} else if (StringUtils.equals(action, MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_MASS_SEND_ALL)) {
			String url = commonServerProperties.getApiServer() + WechatUtil.MASS_SENDALL;
			url = url.replaceFirst(PARAM_WX_ACCESS_TOKEN, accessToken);
			resp = sendAll((String) wechatArticleAction.getData(), url, user);
		} else if (StringUtils.equals(action, MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_MASS_DELETE)) {

		} else if (StringUtils.equals(action, MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_MASS_PREVIEW)) {
			String url = commonServerProperties.getApiServer() + WechatUtil.MASS_PREVIEW;
			url = url.replaceFirst(PARAM_WX_ACCESS_TOKEN, accessToken);
			resp = preview((JSONObject) wechatArticleAction.getData(), url, user);
		} else {
			resp = new DataResp(1009, "未找到公众号文章请求行为", false);
		}
		return resp;
	}

	private DataResp upload(File file, String url, String user) {
	    DataResp resp = new DataResp();
        JSONObject result = HttpUtil.httpPostFile_common(file, url, null);
        String media_id = MapUtils.getString(result, "media_id");
        if (StringUtils.isBlank(media_id)) {
            resp.setData(result);
            resp.setSuccess(false);
            return resp;
        }
        resp.setData(media_id);
        resp.setSuccess(true);
        return resp;
    }

    private DataResp uploadnews(String param, String url, String user) {
        return addNew(param, url, user);
	}

	private DataResp addOther(File file, String url, String user) {
		DataResp resp = new DataResp();
		JSONObject result = HttpUtil.httpPostFile_common(file, url, null);
		String media_id = MapUtils.getString(result, "media_id");
		if (StringUtils.isNotBlank(media_id)) {
			resp.setSuccess(true);
		} else {
			resp.setSuccess(false);
		}
		resp.setData(result);
		return resp;
	}

	private DataResp uploading(File file, String url, String user) {
		DataResp resp = new DataResp();
		JSONObject result = HttpUtil.httpPostFile_common(file, url, null);
		String wxurl = MapUtils.getString(result, "url");
		if (StringUtils.isBlank(wxurl)) {
			resp.setData(result);
			resp.setSuccess(false);
			return resp;
		}
		resp.setData(wxurl);
		resp.setSuccess(true);
		return resp;
	}

	private DataResp addNew(String param, String url, String user) {
		// 将本地图片/网络图片上传到微信
		String httpResult = HttpUtil.httpPost(url, param, null);
		DataResp dataResp = new DataResp();
		if (StringUtils.isBlank(httpResult)) {
			dataResp.setErr_code(MeixConstantCode.M_1009);
			dataResp.setErr_desc("httpPost请求异常:");
			dataResp.setSuccess(false);
			return dataResp;
		}
		JSONObject result = JSONObject.fromObject(httpResult);
		String media_id = MapUtils.getString(result, "media_id");
		if (StringUtils.isBlank(media_id)) {
			dataResp.setSuccess(false);
			dataResp.setData(result);
			return dataResp;
		}
		dataResp.setSuccess(true);
		dataResp.setData(media_id);
		return dataResp;
	}

	private DataResp sendAll(String mediaId, String url, String user) {
		JSONObject params = new JSONObject();
		params.put("msgtype", "mpnews");
		params.put("send_ignore_reprint", 0);
		JSONObject filter = new JSONObject();
		filter.put("is_to_all", true);
		params.put("filter", filter);
		JSONObject mpnews = new JSONObject();
		mpnews.put("media_id", mediaId);
		params.put("mpnews", mpnews);

		String httpPost = HttpUtil.httpPost(url, params.toString(), null);
		DataResp dataResp = new DataResp();
        dataResp.setSuccess(false);
        dataResp.setData(httpPost);
		if (StringUtils.isNotBlank(httpPost)) {
		    JSONObject jsonObj = JSONObject.fromObject(httpPost);
		    dataResp.setData(jsonObj);
	        if (MapUtils.getIntValue(jsonObj, "errcode", -1) == 0) {
	            dataResp.setSuccess(true);
	        }
		}
		return dataResp;
	}

	private DataResp preview(JSONObject obj, String url, String user) {
		@SuppressWarnings("unchecked")
		List<String> tousers = (List<String>) MapUtils.getObject(obj, "touser");
		obj.remove("touser");
		boolean success = false;
		for (String touser : tousers) {
			obj.put("towxname", touser);
			String httpPost = HttpUtil.httpPost(url, obj.toString(), null);
			log.info("微信号：【{}】，文章群发预览推送结果：{}", touser, httpPost);
			if (StringUtils.contains(httpPost, "\"errcode\":0")) {
				success = true;
			}
		}
		DataResp resp = new DataResp();
		resp.setSuccess(success);
		return resp;
	}

	private DataResp delete(String param, String url, String user) {
		String result = HttpUtil.httpPost(url, param.toString(), null);
		log.info("删除永久素材{}-结果：{}", param, result);
		return DataResp.ok();
	}

	private DataResp updateNew(Long id, String url, String user) {
		InsArticleGroupInfo articleGroupDetails = articlePushService.getArticleGroupDetails(id, false, user);
		List<InsArticleInfo> articles = articleGroupDetails.getArticles();
		if (articles == null || articles.size() == 0) {
			return DataResp.fail();
		}
		JSONObject params = new JSONObject();
		int i = 0;
		for (InsArticleInfo insArticleInfo : articles) {
			params.put("media_id", insArticleInfo.getMediaId());
			params.put("index", i);
			Material material = new Material();
			material.setTitle(insArticleInfo.getTitle());
			material.setThumb_media_id(insArticleInfo.getMediaId());
			material.setAuthor(insArticleInfo.getAuthor());
			material.setDigest(insArticleInfo.getShareContent());
			material.setShow_cover_pic(insArticleInfo.getIsSharePicture());
			material.setContent(insArticleInfo.getContent());
			material.setContent_source_url(insArticleInfo.getContentSourceUrl());
			params.put("articles", material);
			//String param = JSONObject.fromObject(params).toString();
			//String httpPost = HttpUtil.httpPost(url, param , null);
			i++;
		}

		return null;
	}

}
