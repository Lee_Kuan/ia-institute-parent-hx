package com.meix.institute.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.api.IInsSyncRecReqService;
import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.Key;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsSyncRecReq;
import com.meix.institute.mapper.InsSyncRecReqMapper;
import com.meix.institute.pojo.InsSyncRecReqInfo;
import com.meix.institute.pojo.InsSyncRecReqS;
import com.meix.institute.pojo.InsSyncRecReqSearch;

@Service
public class InsSyncRecReqServiceImpl implements IInsSyncRecReqService {

	@Autowired
	private InsSyncRecReqMapper insSyncRecReqMapper;
	@Autowired
	private IMeixSyncService meixSyncService;

	@Override
	public Key save(InsSyncRecReqInfo info, String user) {
		Long id = info.getId();
		InsSyncRecReq record = new InsSyncRecReq();
		BeanUtils.copyProperties(info, record);
		Key key = new Key();

		if (null == id || id == 0l) { // 新增
			insSyncRecReqMapper.insert(record);
			key.setId(record.getId());
		} else {  // 更新
			record.setUpdatedAt(new Date());
			record.setUpdatedBy(user);
			insSyncRecReqMapper.updateByPrimaryKeySelective(record);
			key.setId(id);
		}
		return key;
	}

	@Override
	public InsSyncRecReqInfo selectByPrimaryKey(Long id) {
		InsSyncRecReq record = insSyncRecReqMapper.selectByPrimaryKey(id);
		if (record == null) {
			return null;
		}
		InsSyncRecReqInfo info = new InsSyncRecReqInfo();
		BeanUtils.copyProperties(record, info);
		return info;
	}

	@Override
	public List<InsSyncRecReqInfo> findExceptionReq() {
		List<InsSyncRecReq> daolist = insSyncRecReqMapper.findExceptionReq();
		return getServiceList(daolist);
	}

	@Override
	public Paged<InsSyncRecReqInfo> paged(InsSyncRecReqSearch search, String user) {
		InsSyncRecReqS daoSearch = new InsSyncRecReqS();
		BeanUtils.copyProperties(search, daoSearch);
		int count = insSyncRecReqMapper.count(daoSearch);
		Paged<InsSyncRecReqInfo> paged = new Paged<InsSyncRecReqInfo>();
		paged.setCount(count);
		if (count > 0) {
			List<InsSyncRecReq> daolist = insSyncRecReqMapper.find(daoSearch);
			paged.setList(getServiceList(daolist));
		} else {
			paged.setList(Collections.emptyList());
		}
		return paged;
	}

	@Override
	public void close(Long id, String remarks, String user) {
		InsSyncRecReq record = new InsSyncRecReq();
		record.setState(MeixConstants.SYNC_STATE_CLOSE);
		record.setId(id);
		record.setUpdatedAt(new Date());
		record.setUpdatedBy(user);
		record.setRemarks(remarks);
		insSyncRecReqMapper.updateByPrimaryKeySelective(record);
	}

	private List<InsSyncRecReqInfo> getServiceList(List<InsSyncRecReq> daolist) {
		if (daolist != null && daolist.size() > 0) {
			List<InsSyncRecReqInfo> list = new ArrayList<InsSyncRecReqInfo>();
			daolist.forEach((info) -> {
				InsSyncRecReqInfo data = new InsSyncRecReqInfo();
				BeanUtils.copyProperties(info, data);
				list.add(data);
			});
			return list;
		}
		return Collections.emptyList();
	}

	@Override
	public void retry(Long id, String user) {
		InsSyncRecReqInfo info = this.selectByPrimaryKey(id);
		if (info == null || StringUtils.equals(MeixConstants.SYNC_STATE_SUCCESS, info.getState())) {
			return;
		}
		info.setRetryCnt(info.getRetryCnt() + 1);
		info.setRetryMax(info.getRetryMax());

		info = meixSyncService.handleSndSyncAction(info);
		// 设置成功标志
		info.setRemarks("成功");
		info.setState(MeixConstants.SYNC_STATE_SUCCESS);
		//优化更新
		info.setReqBody(null);
		this.save(info, user);
		
	}
}
