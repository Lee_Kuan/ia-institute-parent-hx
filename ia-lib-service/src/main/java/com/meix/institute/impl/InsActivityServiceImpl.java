package com.meix.institute.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.BaseService;
import com.meix.institute.api.IInsActivityService;
import com.meix.institute.api.IInstituteMessagePushService;
import com.meix.institute.api.IUserService;
import com.meix.institute.entity.InsActivity;
import com.meix.institute.entity.InsActivityAnalyst;
import com.meix.institute.entity.InsActivityIssueType;
import com.meix.institute.entity.InsActivityPerson;
import com.meix.institute.entity.InsUser;
import com.meix.institute.mapper.InsActivityAnalystMapper;
import com.meix.institute.mapper.InsActivityIssueTypeMapper;
import com.meix.institute.mapper.InsActivityMapper;
import com.meix.institute.mapper.InsActivityPersonMapper;
import com.meix.institute.mapper.InsUserMapper;
import com.meix.institute.search.ComplianceListSearchVo;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.activity.InsActivityVo;
import com.meix.institute.vo.user.UserInfo;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by zenghao on 2019/7/31.
 */
@Service
public class InsActivityServiceImpl extends BaseService implements IInsActivityService {

	@Autowired
	private InsActivityMapper insActivityMapper;
	@Autowired
	private InsActivityIssueTypeMapper insActivityIssueTypeMapper;
	@Autowired
	private InsUserMapper insUserMapper;
	@Autowired
	private InsActivityPersonMapper personMapper;
	@Autowired
	private IInstituteMessagePushService messagePush;
	@Autowired
	private IUserService userService;
	@Autowired
	private InsActivityAnalystMapper activityAnalystMapper;

	@Override
	public InsActivity getActivity(long activityId, long companyCode) {
		Example example = new Example(InsActivity.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", activityId);
		if (companyCode > 0) {
			criteria.andEqualTo("companyCode", companyCode);
		}
		InsActivity activity = insActivityMapper.selectOneByExample(example);
		return activity;
	}

	private Example buildComplianceListExample(ComplianceListSearchVo searchVo) {
		Example example = new Example(InsActivity.class);
		Example.Criteria criteria = example.createCriteria();
//		criteria.andEqualTo("activityStatus", 1); // 活动状态：默认1（已审核）
		if (searchVo.getType() > 0) {
			criteria.andEqualTo("activityType", searchVo.getType());
		}
		if (searchVo.getCompanyCode() > 0) {
			criteria.andEqualTo("companyCode", searchVo.getCompanyCode());
		}
		if (searchVo.getStatusList() != null && searchVo.getStatusList().size() > 0) {
			criteria.andIn("activityStatus", searchVo.getStatusList());
		}
		if (searchVo.getShare() > -1) {
			criteria.andEqualTo("share", searchVo.getShare());
		}
		if (!StringUtils.isNullorWhiteSpace(searchVo.getCondition())) {
			Example userExample = new Example(InsUser.class);
			userExample.createCriteria().andLike("userName", "%" + searchVo.getCondition() + "%");
			List<InsUser> userList = insUserMapper.selectByExample(userExample);
			Example.Criteria condition = example.and();
			condition.andLike("title", "%" + searchVo.getCondition() + "%");
			if (userList != null && userList.size() > 0) {
				List<String> uuidList = new ArrayList<>();
				for (InsUser user : userList) {
					uuidList.add(user.getUser());
				}
				condition.orIn("createdBy", uuidList);
			}
			Example analystExample = new Example(InsActivityAnalyst.class);
			analystExample.createCriteria().andLike("analystName", "%" + searchVo.getCondition() + "%");
			List<InsActivityAnalyst> analystList = activityAnalystMapper.selectByExample(analystExample);
			if (analystList != null && analystList.size() > 0) {
				List<Long> activityList = new ArrayList<>();
				for (InsActivityAnalyst analyst : analystList) {
					activityList.add(analyst.getActivityId());
				}
				condition.orIn("id", activityList);
			}
		}
		if (searchVo.getSortType() == 0) {
			if (searchVo.getSortRule() == -1) {
				example.orderBy("startTime").desc();
			} else {
				example.orderBy("startTime").asc();
			}
		} else if (searchVo.getSortType() == 1) {
			if (searchVo.getSortRule() == -1) {
				example.orderBy("updatedAt").desc();
			} else {
				example.orderBy("updatedAt").asc();
			}
		} else {
			example.orderBy("startTime").desc();
		}
		return example;
	}

	@Override
	public List<InsActivityVo> getComplianceList(ComplianceListSearchVo searchVo) {
		Example example = buildComplianceListExample(searchVo);
		RowBounds rowBounds = new RowBounds(searchVo.getCurrentPage(), searchVo.getShowNum());
		List<InsActivity> list = insActivityMapper.selectByExampleAndRowBounds(example, rowBounds);
		if (list == null || list.size() == 0) {
			return Collections.emptyList();
		}
		List<InsActivityVo> result = new ArrayList<>(list.size());
		for (InsActivity item : list) {
			InsActivityVo insActivityVo = new InsActivityVo();
			BeanUtils.copyProperties(item, insActivityVo);

			insActivityVo.setPublishDate(item.getStartTime());
			insActivityVo.setUndoTime(item.getUpdatedAt());
			//设置创建人和修改人信息
			Example userExample = new Example(InsUser.class);
			userExample.createCriteria().andEqualTo("user", item.getCreatedBy());
			InsUser createUser = insUserMapper.selectOneByExample(userExample);
			if (createUser != null) {
				insActivityVo.setAuthorName(createUser.getUserName());
				insActivityVo.setUserState(createUser.getUserState());
			}
			userExample.clear();
			userExample.createCriteria().andEqualTo("user", item.getUpdatedBy());
			InsUser updateUser = insUserMapper.selectOneByExample(userExample);
			if (updateUser != null) {
				insActivityVo.setEditUserName(updateUser.getUserName());
			}

			//设置发布渠道信息
			InsActivityIssueType issueTypeSearch = new InsActivityIssueType();
			issueTypeSearch.setActivityId(item.getId());
			List<InsActivityIssueType> typeList = insActivityIssueTypeMapper.select(issueTypeSearch);
			if (typeList != null && typeList.size() > 0) {
				List<Integer> issueTypeList = new ArrayList<>(typeList.size());
				for (InsActivityIssueType type : typeList) {
					issueTypeList.add(type.getIssueType());
				}
				insActivityVo.setIssueTypeList(issueTypeList);
			} else {
				insActivityVo.setIssueTypeList(new ArrayList<>());
			}
			insActivityVo.setShareName(getShareDesc(item.getShare()));
			result.add(insActivityVo);
		}
		list.clear();
		return result;
	}

	@Override
	public long getComplianceCount(ComplianceListSearchVo searchVo) {
		Example example = buildComplianceListExample(searchVo);
		return insActivityMapper.selectCountByExample(example);
	}

	@Override
	public void examineActivity(InsActivity activity) {
		insActivityMapper.updateByPrimaryKeySelective(activity);
	}

	@Override
	public List<InsActivityVo> getManagementList(ComplianceListSearchVo searchVo) {

		Example example = buildComplianceListExample(searchVo);
		RowBounds rowBounds = new RowBounds(searchVo.getCurrentPage(), searchVo.getShowNum());
		List<InsActivity> list = insActivityMapper.selectByExampleAndRowBounds(example, rowBounds);
		if (list == null || list.size() == 0) {
			return Collections.emptyList();
		}
		List<InsActivityVo> result = new ArrayList<>(list.size());
		for (InsActivity item : list) {
			InsActivityVo insActivityVo = new InsActivityVo();
			BeanUtils.copyProperties(item, insActivityVo);

			//设置创建人和修改人信息
/*			Example userExample = new Example(InsUser.class);
			userExample.createCriteria().andEqualTo("user", item.getCreatedBy());
			InsUser createUser = insUserMapper.selectOneByExample(userExample);
			if (createUser != null) {
				insActivityVo.setAuthorName(createUser.getUserName());
				insActivityVo.setUserState(createUser.getUserState());
			}
			userExample.clear();
			userExample.createCriteria().andEqualTo("user", item.getUpdatedBy());
			InsUser updateUser = insUserMapper.selectOneByExample(userExample);
			if (updateUser != null) {
				insActivityVo.setEditUserName(updateUser.getUserName());
			}
			insActivityVo.setEditTime(item.getUpdatedAt());*/

			// 设置作者
			InsActivityAnalyst record = new InsActivityAnalyst();
			record.setActivityId(item.getId());
			List<InsActivityAnalyst> analysts = activityAnalystMapper.select(record);
			if (CollectionUtils.isNotEmpty(analysts)) {
				StringBuffer analystName = new StringBuffer();
				analysts.forEach((analyst) -> {
					analystName.append("，").append(analyst.getAnalystName());
				});
				insActivityVo.setAuthorName(analystName.toString().substring(1)); 
			}
			Example userEx = new Example(InsUser.class);
			userEx.createCriteria().andEqualTo("user", item.getUpdatedBy());
			InsUser insUser = insUserMapper.selectOneByExample(userEx);
			insActivityVo.setEditUserName(insUser == null ? "" : insUser.getUserName());
			insActivityVo.setEditTime(item.getUpdatedAt());
			//设置发布渠道信息
			InsActivityIssueType issueTypeSearch = new InsActivityIssueType();
			issueTypeSearch.setActivityId(item.getId());
			List<InsActivityIssueType> typeList = insActivityIssueTypeMapper.select(issueTypeSearch);
			if (typeList != null && typeList.size() > 0) {
				List<Integer> issueTypeList = new ArrayList<>(typeList.size());
				for (InsActivityIssueType type : typeList) {
					issueTypeList.add(type.getIssueType());
				}
				insActivityVo.setIssueTypeList(issueTypeList);
			} else {
				insActivityVo.setIssueTypeList(new ArrayList<>());
			}

			//设置报名名单审批状态信息
			Example joinPersonExample = new Example(InsActivityPerson.class);
			joinPersonExample.createCriteria().andEqualTo("activityId", insActivityVo.getId()).andEqualTo("status", 0);
			int count = personMapper.selectCountByExample(joinPersonExample);
			insActivityVo.setUnExamineJoinPersonCount(count);
			insActivityVo.setShareName(getShareDesc(item.getShare()));
			result.add(insActivityVo);
		}
		list.clear();
		return result;
	}

	@Override
	public int getManagementCount(ComplianceListSearchVo searchVo) {
		Example example = buildComplianceListExample(searchVo);
		return insActivityMapper.selectCountByExample(example);
	}

	@Override
	public void examineJoinPersonList(long id, int optType, String uuid) {
		InsActivityPerson person = new InsActivityPerson();
		person.setId(id);
		person.setStatus(optType);
		person.setUpdatedAt(new Date());
		person.setUpdatedBy(uuid);
		personMapper.updateByPrimaryKeySelective(person);

		if (optType == 1 || optType == 2) {
			person = personMapper.selectByPrimaryKey(id);
			InsActivity activity = insActivityMapper.selectByPrimaryKey(person.getActivityId());
			UserInfo user = userService.getUserInfo(0, null, person.getMobile());
			InsUser comInfo = insUserService.getUserInfo(uuid);
			if (activity != null && user != null && comInfo != null) {
//				long companyCode, long uid, int messageType, String title, int dataType, int clueType, String mobile
				messagePush.messagePush(comInfo.getCompanyCode(), user.getId(), InstituteMessagePushServiceImpl.JOIN_ACTIVITY_SUCCESS, activity.getTitle(), activity.getActivityType(), optType, null);
			}
		}
	}

	@Override
	public void activityStartRemind(long companyCode) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Example example = new Example(InsActivity.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("companyCode", companyCode).andEqualTo("activityType", 7).andEqualTo("activityStatus", 1);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.SECOND, 0);
		cal.add(Calendar.MINUTE, 10); //活动开始前10分钟
		String startTime = df.format(cal.getTime());
		cal.add(Calendar.MINUTE, 1);
		String endTime = df.format(cal.getTime());
		criteria.andGreaterThanOrEqualTo("startTime", startTime).andLessThan("startTime", endTime);
		List<InsActivity> list = insActivityMapper.selectByExample(example);
		for (InsActivity activity : list) {
			messagePush.messagePush(companyCode, activity.getId(), InstituteMessagePushServiceImpl.ACTIVITY_START_REMIND, activity.getTitle(), activity.getActivityType(), 0, null);
		}
	}
}
