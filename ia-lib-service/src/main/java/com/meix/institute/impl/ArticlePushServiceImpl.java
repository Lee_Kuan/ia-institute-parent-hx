package com.meix.institute.impl;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meix.institute.api.FsService;
import com.meix.institute.api.IArticlePushService;
import com.meix.institute.api.IDictTagService;
import com.meix.institute.api.WechatArticleAction;
import com.meix.institute.api.WechatArticleHandler;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.BaseResp;
import com.meix.institute.core.DataResp;
import com.meix.institute.core.Key;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsArticle;
import com.meix.institute.entity.InsArticleGroup;
import com.meix.institute.entity.InsArticleOther;
import com.meix.institute.mapper.InsArticleGroupMapper;
import com.meix.institute.mapper.InsArticleMapper;
import com.meix.institute.mapper.InsArticleOtherMapper;
import com.meix.institute.pojo.InsArticleGroupInfo;
import com.meix.institute.pojo.InsArticleGroupPreviewInfo;
import com.meix.institute.pojo.InsArticleGroupSearch;
import com.meix.institute.pojo.InsArticleInfo;
import com.meix.institute.pojo.InsArticleOtherInfo;
import com.meix.institute.pojo.InsArticleSearch;
import com.meix.institute.pojo.wechat.Material;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.HttpUtil;
import com.meix.institute.util.ModelUtil;
import com.meix.institute.util.UuidUtil;

import net.sf.json.JSONObject;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class ArticlePushServiceImpl implements IArticlePushService{
    
    @Autowired
    private InsArticleMapper articleMapper;
    @Autowired
    private InsArticleGroupMapper articleGroupMapper;
    @Autowired
    private InsArticleOtherMapper articleOtherMapper;
    @Autowired
    private WechatArticleHandler wechatArticleHandler;
    @Autowired
    private IDictTagService dictTagService;
    @Autowired
    private FsService fsService;
    
    private static final Logger log = LoggerFactory.getLogger(ArticlePushServiceImpl.class);
    
    @Override
    public Paged<InsArticleInfo> paged(InsArticleSearch search, String user) {
        Paged<InsArticleInfo> paged = new Paged<InsArticleInfo>();
        Example example = new Example(InsArticle.class);
        Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(search.getTitle())) {
        	criteria.andLike("title", "%" + search.getTitle() + "%");
        }
        if (search.getExcludeArticleIds() != null && !search.getExcludeArticleIds().isEmpty()) {
        	criteria.andNotIn("id", search.getExcludeArticleIds());
        }
        if (search.getState() == null || 4 == search.getState()) {
            criteria.andGreaterThan("state", MeixConstants.ARTICLE_STATE_DELETE); // 排除已删除
        } else {
            criteria.andEqualTo("state", search.getState());
        }
        int count = articleMapper.selectCountByExample(example);
        paged.setCount(count);
        if (count == 0) {
            paged.setList(new ArrayList<InsArticleInfo>());
            return paged;
        }
        example.excludeProperties(new String[]{"content"});
        RowBounds rowBounds = new RowBounds(search.getCurrentPage() * search.getShowNum(), search.getShowNum());
        List<InsArticle> data = articleMapper.selectByExampleAndRowBounds(example, rowBounds);
        List<InsArticleInfo> info = ModelUtil.model2InfoList(data, InsArticleInfo.class);
        InsArticleOther other = new InsArticleOther();
        for (InsArticleInfo item : info) {
            other.setMediaId(item.getSharePictureMediaId());
            if (StringUtils.isNotBlank(other.getMediaId())) {
	            List<InsArticleOther> select = articleOtherMapper.select(other);
	            if (select != null && select.size() > 0) {
	                item.setSharePicture(select.get(0).getRemoteUrl());
	            }
            }
        }
        paged.setList(info);
        return paged;
    }

    @Override
    public InsArticleInfo getArticleDetails(Long id, String user) {
        InsArticle record = articleMapper.selectByPrimaryKey(id);
        InsArticleInfo info = ModelUtil.model2Info(record, InsArticleInfo.class);
        InsArticleOther oRecord = new InsArticleOther();
        oRecord.setMediaId(info.getSharePictureMediaId());
        if (StringUtils.isNotBlank(oRecord.getMediaId())) {
	        List<InsArticleOther> select = articleOtherMapper.select(oRecord);
	        if (select != null && select.size() > 0) {
	            info.setSharePicture(select.get(0).getRemoteUrl());
	        }
        }
        return info;
    }

    @Override
    public Key save(InsArticleInfo info, String user) {
        InsArticle record = ModelUtil.info2Model(info, InsArticle.class);
        Long id = info.getId();
        Date _now = new Date();
        record.setUpdatedAt(_now);
        record.setUpdatedBy(user);
        if (id == null || id == 0l) {
            record.setState(MeixConstants.ARTICLE_STATE_NEW);
            record.setIsSharePicture(0);
            record.setOpenComment(1);
            record.setOnlyFansCanComment(0);
            record.setCreatedAt(_now);
            record.setCreatedBy(user);
            articleMapper.insert(record);
            id = record.getId();
        } else {
            articleMapper.updateByPrimaryKeySelective(record);
        }
        return new Key(id, "");
    }

    @Override
    public void delete(Long id, String user) {
        InsArticle record = new InsArticle();
        record.setId(id);
        record.setState(MeixConstants.ARTICLE_STATE_DELETE);
        record.setUpdatedAt(new Date());
        record.setUpdatedBy(user);
        articleMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Paged<InsArticleGroupInfo> groupPage(InsArticleSearch search, String user) {
        Paged<InsArticleGroupInfo> paged = new Paged<InsArticleGroupInfo>();
        Example example = new Example(InsArticleGroup.class);
        Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(search.getTitle())) {
        	criteria.andLike("title", "%" + search.getTitle() + "%");
        }
        if (search.getState() != 4) {
            criteria.andEqualTo("state", search.getState());
        }
        int count = articleGroupMapper.selectCountByExample(example);
        paged.setCount(count);
        if (count == 0) {
            paged.setList(new ArrayList<InsArticleGroupInfo>());
            return paged;
        }
       
        RowBounds rowBounds = new RowBounds(search.getCurrentPage() * search.getShowNum(), search.getShowNum());
        List<InsArticleGroup> data = articleGroupMapper.selectByExampleAndRowBounds(example, rowBounds);
        List<InsArticleGroupInfo> infoList = ModelUtil.model2InfoList(data, InsArticleGroupInfo.class);
        paged.setList(infoList);
        return paged;
    }

    @Override
    public InsArticleGroupInfo getArticleGroupDetails(Long id, Boolean loadContent, String user) {
        InsArticleGroup record = articleGroupMapper.selectByPrimaryKey(id);
        InsArticleGroupInfo info = ModelUtil.model2Info(record, InsArticleGroupInfo.class);
        List<InsArticleInfo> articles = new ArrayList<InsArticleInfo>();
        BeanWrapper wrapper = new BeanWrapperImpl(record);
        DecimalFormat articleDf = new DecimalFormat("article00");
        List<Long> articleIds = new ArrayList<Long>();
        for (int i = 1; i <= 8; i++) {
            String articleField = articleDf.format(i);
            Long article = (Long) wrapper.getPropertyValue(articleField);
            if (article == null || article == 0 ) {
                break;
            }
            articleIds.add(article);
        }
        if (articleIds.size() > 0) {
            Example example = new Example(InsArticle.class);
            if (!loadContent) {
                example.excludeProperties(new String[]{"content"});
            }
            example.createCriteria().andIn("id", articleIds);
            List<InsArticle> list = articleMapper.selectByExample(example);
            List<InsArticleInfo> infoList = ModelUtil.model2InfoList(list, InsArticleInfo.class);
            Map<Long, InsArticleInfo> infoMap = infoList.stream().collect(Collectors.toMap(InsArticleInfo::getId, p->p));
            List<InsArticleInfo> result = new ArrayList<InsArticleInfo>();
            for (Long articleId : articleIds) {
                InsArticleInfo insArticleInfo = infoMap.get(articleId);
                InsArticleOther other = new InsArticleOther();
                other.setMediaId(insArticleInfo.getSharePictureMediaId());
                List<InsArticleOther> others = articleOtherMapper.select(other);
                if (others != null && others.size() > 0) {
                    insArticleInfo.setSharePicture(others.get(0).getRemoteUrl());
                }
                if (insArticleInfo.getState() != null) {
                    insArticleInfo.setStateText(dictTagService.getText(MeixConstants.ARTICLE_STATE, insArticleInfo.getState().toString()));
                }
                result.add(infoMap.get(articleId));
            }
            info.setArticles(infoList);
        } else {
            info.setArticles(articles);
        }
        return info;
    }

    @Override
    public void deleteGroup(Long id, String user) {
        InsArticleGroup record = new InsArticleGroup();
        record.setId(id);
        record.setState(MeixConstants.ARTICLE_STATE_DELETE);
        record.setUpdatedAt(new Date());
        record.setUpdatedBy(user);
        articleGroupMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public DataResp groupPublish(Long id, String mediaId, Date publishTime, String user) {
        DataResp actionResp = null;
        boolean isHasChance = checkTodayIsHasPushChance();
        if (!isHasChance) {
            actionResp = new DataResp();
            actionResp.setSuccess(false);
            actionResp.setData("微信订阅号文章推送，每天只能推送一条哦！！！");
            return actionResp;
        }
        
        Date _now = new Date();
        Integer state = MeixConstants.ARTICLE_STATE_NEW;
        // 判断是否立即发送
        if (publishTime == null || publishTime.compareTo(_now) <= 0) { // 发布时间为空 获取 小于当前时间： 立即发布
            // 群发
            WechatArticleAction action = new WechatArticleAction();
            action.setAction(MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_MASS_SEND_ALL);
            action.setData(mediaId);
            actionResp = wechatArticleHandler.action(action, user);
            if (actionResp.isSuccess()) {
                state = MeixConstants.ARTICLE_STATE_PUBLISH;
                publishTime = publishTime == null ? _now : publishTime;
            } else {
            	return actionResp;
            }
        } else if (publishTime.after(_now)) { // 发布时间大于当前时间： 预约发布
            state = MeixConstants.ARTICLE_STATE_PRE;
        }
        // 更新group
        InsArticleGroup articleGroup = new InsArticleGroup();
        articleGroup.setId(id);
        articleGroup.setState(state);
        articleGroup.setPublishTime(publishTime);
        articleGroupMapper.updateByPrimaryKeySelective(articleGroup);
        
        // 更新group下的article
        articleGroup = articleGroupMapper.selectByPrimaryKey(id);
        BeanWrapper wrapper = new BeanWrapperImpl(articleGroup);
        DecimalFormat articleDf = new DecimalFormat("article00");
        InsArticle insArticle = new InsArticle();
        for (int i = 1; i <= 8; i++) {
            String articleField = articleDf.format(i);
            Long articleId = (Long) wrapper.getPropertyValue(articleField);
            if (articleId == null || articleId == 0l ) {
                break;
            }
            insArticle.setId(articleId);
            insArticle.setState(state);
            insArticle.setPublishTime(publishTime);
            articleMapper.updateByPrimaryKeySelective(insArticle);
        }
        return DataResp.ok();
    }
    
    /**
     * 检查今天是否有推送机会
     * @return
     */
    private boolean checkTodayIsHasPushChance() {
        Date _now = new Date();
        String publishTimeFm = DateUtil.dateToStr(_now, DateUtil.dateShortFormat);
        String publishTimeTo = DateUtil.getDayDate(publishTimeFm, 1);
        InsArticleGroupSearch search = new InsArticleGroupSearch();
        search.setState(MeixConstants.ARTICLE_STATE_PUBLISH);
        search.setPublishTimeFm(DateUtil.stringToDate(publishTimeFm));
        search.setPublishTimeTo(DateUtil.stringToDate(publishTimeTo));
        List<InsArticleGroupInfo> list = groupFind(search);
        if (list == null || list.size() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public Key groupSave(InsArticleGroupInfo info, String user){
        List<Long> articleIds = info.getArticleIds();
        if (articleIds == null || articleIds.size() == 0) {
            return null;
        }
        Long id = info.getId();
        InsArticleGroup record = new InsArticleGroup();
        BeanWrapper wrapper = new BeanWrapperImpl(record);
        DecimalFormat articleDf = new DecimalFormat("article00");
        for(int i=0, size=articleIds.size(); i<size; i++) {
            wrapper.setPropertyValue(articleDf.format(i+1), articleIds.get(i));
        }
        // 检查是是否存在相同文章
        if (id == null || id == 0l) {
            for(int i=articleIds.size(); i<8; i++) {
                wrapper.setPropertyValue(articleDf.format(i+1), 0);
            }
            List<InsArticleGroup> select = articleGroupMapper.select(record);
            if (select != null && select.size() > 0) {
                return new Key(select.get(0).getId(), select.get(0).getMediaId());
            }
        }
        Date _now = new Date();
        record.setUpdatedAt(_now);
        record.setUpdatedBy(user);
        
        // 复制第一个文章title
        InsArticle insArticle = articleMapper.selectByPrimaryKey(articleIds.get(0));
        record.setTitle(insArticle.getTitle());
        
        if (id != null && id > 0) { // 更新
            record.setId(id);
            InsArticleGroup group = articleGroupMapper.selectByPrimaryKey(id);
            //String articleUpdateRecord = recordUpdateArticle(group, record);
            //record.setArticlesChageRecord(articleUpdateRecord);
            articleGroupMapper.updateByPrimaryKeySelective(record);
            return new Key(record.getId(), group.getMediaId());
        } else { // 添加
            record.setState(MeixConstants.ARTICLE_STATE_NEW);
            record.setCreatedAt(_now);
            record.setCreatedBy(user);
            articleGroupMapper.insertSelective(record);
            return new Key(record.getId(), "");
        }
    }
    
    /**
     * 记录更新文章{field:_new}
     * @param group
     * @param record
     * @return
     */
    /*private String recordUpdateArticle(InsArticleGroup _old, InsArticleGroup _new) {
        BeanWrapper oldWrapper = new BeanWrapperImpl(_old);
        BeanWrapper newWrapper = new BeanWrapperImpl(_new);
        DecimalFormat articleDf = new DecimalFormat("article00");
        JSONObject updateRecord = new JSONObject(); 
        for (int i = 1; i <= 8; i++) {
            String articleField = articleDf.format(i);
            long oldArticleId = (long) oldWrapper.getPropertyValue(articleField);
            long newArticleId = (long) newWrapper.getPropertyValue(articleField);
            if (oldArticleId != newArticleId) {
                updateRecord.put(articleField, String.valueOf(newArticleId));
            }
        }
        return updateRecord.toString();
    }*/

    @Override
    public BaseResp groupUndo(Long id, String user) {
        return null;
    }

    @Override
    public Key saveOther(InsArticleOtherInfo info, String user) {
        InsArticleOther record = ModelUtil.info2Model(info, InsArticleOther.class);
        Long id = info.getId();
        Date _now = new Date();
        record.setUpdatedAt(_now);
        record.setUpdatedBy(user);
        if (id == null) {
            record.setState(MeixConstants.ARTICLE_STATE_NEW);
            record.setCreatedAt(_now);
            record.setCreatedBy(user);
            articleOtherMapper.insert(record);
            id = record.getId();
        } else {
            articleOtherMapper.updateByPrimaryKeySelective(record);
        }
        return new Key(id, "");
    }

    @Override
    public DataResp articlePreview(String data, String type, List<String> accounts, String user) {
        String msgtype = type;
        JSONObject coreValue = new JSONObject();
        if (StringUtils.equals(type, MeixConstants.WECHAT_ARTICLE_TYPE_TEXT)) {
            coreValue.put("content", data);
            msgtype = "text";
        } else if (StringUtils.equals(type, MeixConstants.WECHAT_ARTICLE_TYPE_GRAPHIC)) {
            coreValue.put("media_id", data);
            msgtype = "mpnews";
        } else if (StringUtils.equals(type, MeixConstants.WECHAT_ARTICLE_TYPE_IMAGE)) {
            InsArticleOther other = articleOtherMapper.selectByPrimaryKey(data);
            coreValue.put("media_id", other.getMediaId());
            if (StringUtils.equals(type, MeixConstants.WECHAT_ARTICLE_TYPE_VIDEO)) {
                msgtype = "mpvideo";
            }
        } else {
            return new DataResp(1009, "不支持的推送类型", false);
        }
        if (coreValue.isEmpty()) {
            return new DataResp(1009, "未找到推送内容", false);
        }
        WechatArticleAction action = new WechatArticleAction();
        action.setAction(MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_MASS_PREVIEW);
        JSONObject params = new JSONObject();
        params.put("touser", accounts);
        params.put(msgtype, coreValue);
        params.put("msgtype", msgtype);
        action.setData(params);
        return wechatArticleHandler.action(action, user);
    }

    @Override
    public DataResp addGroupPreviewMaterial(List<Long> articleIds, String user) {
        if (articleIds == null || articleIds.size() == 0) {
            return DataResp.fail();
        }
        Example example = new Example(InsArticle.class);
        example.createCriteria().andIn("id", articleIds);
        List<InsArticle> list = articleMapper.selectByExample(example);
        List<Material> materials = new ArrayList<Material>();
        for (InsArticle insArticle : list) {
            Material material = new Material();
            material.setTitle(insArticle.getTitle());
            material.setThumb_media_id(convertPreviceMediaId(insArticle.getSharePictureMediaId()));
            material.setAuthor(insArticle.getAuthor());
            material.setDigest(insArticle.getShareContent());
            material.setShow_cover_pic(insArticle.getIsSharePicture());
            material.setContent(handleLocalPicture(insArticle.getContent()));
            material.setContent_source_url(insArticle.getContentSourceUrl());
            material.setNeed_open_comment(insArticle.getOpenComment());
            material.setOnly_fans_can_comment(insArticle.getOnlyFansCanComment());
            materials.add(material);
        }
        JSONObject aj = new JSONObject();
        aj.put("articles", materials);
        String param = aj.toString();
        
        WechatArticleAction action = new WechatArticleAction();
        action.setAction(MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_UPLOADNEWS);
        action.setData(param);
        DataResp actionResp = wechatArticleHandler.action(action, user);
        return actionResp;
    }
    
    private String convertPreviceMediaId(String mediaId) {
        File tempFile = null;
        String remoteUrl = null;
        try {
            InsArticleOther record = new InsArticleOther();
            record.setMediaId(mediaId);
            List<InsArticleOther> select = articleOtherMapper.select(record);
            if (select != null && select.size() > 0) {
                remoteUrl = select.get(0).getRemoteUrl();
                byte[] http = HttpUtil.getHttp(remoteUrl, 3000);
                String uuid = UuidUtil.generate32bitUUID();
                int indexOf = remoteUrl.lastIndexOf("?wx_fmt=");
                if (indexOf > 0) {
                    uuid += "." + remoteUrl.substring(indexOf + 8);
                }
                tempFile = fsService.createTempFile(uuid, http);
                WechatArticleAction action = new WechatArticleAction();
                action.setAction(MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_UPLOAD);
                action.setData(tempFile);
                action.setType(MeixConstants.WECHAT_ARTICLE_TYPE_IMAGE);
                DataResp actionResp = wechatArticleHandler.action(action, "");
                if (actionResp.isSuccess()) {
                    return String.valueOf(actionResp.getData());
                }
            }
        } catch (Exception e) {
            log.warn("convertPreviceMediaId异常, media:【{}】, url:【{}】", mediaId, remoteUrl, e);
        } finally {
            if (tempFile != null) {
                tempFile.delete();
            }
        }
        return null;
    }

    /** 上传图文消息内的图片获取URL **/
    private String handleLocalPicture(String content) {
        Document parse = Jsoup.parseBodyFragment(content);
        Elements imgs = parse.getElementsByTag("img");
        Map<String, String> replaceMap = new HashMap<String, String>();
        for (Element img : imgs) {
            String linkSrc = img.attr("src");
            if (!StringUtils.contains(linkSrc, "mmbiz.qpic.cn")) {
                String weixinLink = replaceToWeixinPicture(linkSrc);
                if (StringUtils.isNotBlank(weixinLink)) {
                    replaceMap.put(linkSrc, weixinLink);
                } 
            }
        }
        if (replaceMap.isEmpty()) {
            return content;
        }
        String htmlStr = parse.body().toString();
        htmlStr = htmlStr.substring(7, htmlStr.length() - 7);
        htmlStr = StringUtils.replace(htmlStr, "\r", "");
        htmlStr = StringUtils.replace(htmlStr, "\n", "");
        for(Map.Entry<String, String> entry : replaceMap.entrySet()){
            String oldChar = entry.getKey();
            String newChar = entry.getValue();
            htmlStr = htmlStr.replace(oldChar, newChar);
        }
        return htmlStr;
    }

    private String replaceToWeixinPicture(String linkSrc) {
        String replaceLinkSrc = null;
        File tempFile = null;
        byte[] result = null;
        try {
            if (linkSrc.contains("http://")) {
                result = HttpUtil.getHttp(linkSrc, 3000);
            } else if (linkSrc.contains("https://")) {
                result = HttpUtil.getHttps(linkSrc, 3000);
            }
            String uuid = UuidUtil.generate32bitUUID();
            int indexOf = linkSrc.lastIndexOf(".");
            if (indexOf > 0) {
                uuid += linkSrc.substring(indexOf);
            }
            tempFile = fsService.createTempFile(uuid, result);
            if (tempFile != null && tempFile.exists()) {
                WechatArticleAction action = new WechatArticleAction();
                action.setAction(MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_UPLOADIMG);
                action.setData(tempFile);
                DataResp actionResp = wechatArticleHandler.action(action, "");
                if (actionResp.isSuccess()) {
                    replaceLinkSrc = String.valueOf(actionResp.getData());
                } 
            }
        } catch (Exception e) {
            log.warn("替换微信图片异常：", e);
        } finally {
            if (tempFile != null && tempFile.exists()) {
                tempFile.delete();
            }
        }
        return replaceLinkSrc;
    }

    @Override
    public DataResp groupSaveAndPublish(InsArticleGroupInfo info, String user) {
        Key key = groupSave(info, user);
        DataResp dataResp = addGroupMaterial(key.getId(), user);
        if (dataResp.isSuccess()) {
            dataResp = groupPublish(key.getId(), String.valueOf(dataResp.getData()), info.getPublishTime(), user);
        }
        return dataResp;
    }

    @Override
    public DataResp addGroupMaterial(Long id, String user) {
        DataResp actionResp = null;
        InsArticleGroup articleGroup = articleGroupMapper.selectByPrimaryKey(id);
        String mediaId = articleGroup.getMediaId();
        if (articleGroup != null && StringUtils.isNotBlank(mediaId)) {
            actionResp = new DataResp();
            actionResp.setData(articleGroup.getMediaId());
            actionResp.setSuccess(true);
            return actionResp;
        }
        InsArticleGroupInfo articleGroupDetails = this.getArticleGroupDetails(id, true, user);
        List<InsArticleInfo> articles = articleGroupDetails.getArticles();
        if (articles == null || articles.size() == 0) {
            return DataResp.fail();
        }
        List<Material> materials = new ArrayList<Material>();
        for (InsArticleInfo insArticleInfo : articles) {
            Material material = new Material();
            material.setTitle(insArticleInfo.getTitle());
            material.setThumb_media_id(insArticleInfo.getSharePictureMediaId());
            material.setAuthor(insArticleInfo.getAuthor());
            material.setDigest(insArticleInfo.getShareContent());
            material.setShow_cover_pic(insArticleInfo.getIsSharePicture());
            material.setContent(handleLocalPicture(insArticleInfo.getContent()));
            material.setContent_source_url(insArticleInfo.getContentSourceUrl());
            material.setNeed_open_comment(insArticleInfo.getOpenComment());
            material.setOnly_fans_can_comment(insArticleInfo.getOnlyFansCanComment());
            materials.add(material);
        }
        JSONObject aj = new JSONObject();
        aj.put("articles", materials);
        String param = aj.toString();
        WechatArticleAction action = new WechatArticleAction();
        action.setAction(MeixConstants.WECHAT_ARTICLE_PUBLISH_ACTION_ADD_NEW);
        action.setData(param);
        actionResp = wechatArticleHandler.action(action, user);
        if (actionResp.isSuccess()) {
            mediaId = String.valueOf(actionResp.getData());
        } else {
            return actionResp;
        }
        // 更新 media_id
        InsArticleGroup groupRecord = new InsArticleGroup();
        groupRecord.setId(id);
        groupRecord.setMediaId(mediaId);
        articleGroupMapper.updateByPrimaryKeySelective(groupRecord);
        
        actionResp.setSuccess(true);
        actionResp.setData(mediaId);
        return actionResp;
    }

    @Override
    public DataResp groupPreview(InsArticleGroupPreviewInfo info, String user) {
        List<Long> articleIds = info.getArticleIds();
        DataResp dataResp = addGroupPreviewMaterial(articleIds, user);
        if (dataResp.isSuccess()) {
            List<String> accounts = Arrays.asList(StringUtils.split(info.getOpenIds()));
            dataResp = articlePreview(String.valueOf(dataResp.getData()), MeixConstants.WECHAT_ARTICLE_TYPE_GRAPHIC, accounts, user);
        }
        return dataResp;
    }

    @Override
    public List<InsArticleGroupInfo> groupFind(InsArticleGroupSearch search) {
        Example example = new Example(InsArticleGroup.class);
        Criteria createCriteria = example.createCriteria();
        createCriteria.andEqualTo("state", search.getState());
        createCriteria.andGreaterThanOrEqualTo("publishTime", search.getPublishTimeFm());
        createCriteria.andLessThan("publishTime", search.getPublishTimeTo());
        List<InsArticleGroup> selectByExample = articleGroupMapper.selectByExample(example);
        return ModelUtil.model2InfoList(selectByExample, InsArticleGroupInfo.class);
    }

}
