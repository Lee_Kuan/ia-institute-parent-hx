package com.meix.institute.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.meix.institute.api.ITaskService;
import com.meix.institute.api.InstituteDataCenterDao;
import com.meix.institute.util.DateUtil;

@Service
public class TaskServiceImpl implements ITaskService{
    
    @Value("${ins.companyCode}")
    private Integer companyCode;
    
    @Autowired
    private InstituteDataCenterDao instituteDataCenterDaoImpl;
    
    @Override
    public void executeHeatData() {
        
        // 清空
        instituteDataCenterDaoImpl.clearOrgHeatData();
        
        // 近一月
        generateHeatDataByIndustry(1); // 按行业
        generateHeatDataByInnerCode(1);// 按个股
        // 近三月
        generateHeatDataByIndustry(2); // 按行业
        generateHeatDataByInnerCode(2);// 按个股
        // 近半年
        generateHeatDataByIndustry(3); // 按行业
        generateHeatDataByInnerCode(3);// 按个股
        // 近一年
        generateHeatDataByIndustry(4); // 按行业
        generateHeatDataByInnerCode(4);// 按个股

    }
    
    private void generateHeatDataByIndustry(Integer dateType) {
        // 1、统计关注度
        statisticsAttentionByIndustry(dateType);
        // 2、统计内容数
        statisticContentNumByIndustry(dateType);
    }
    
    private void statisticContentNumByIndustry(Integer dateType) {
        // 全市场
        statisticMarketContentNumByIndustry(dateType);
        // 本所
        statisticSelfContentNumByIndustry(dateType);
    }

    private void statisticSelfContentNumByIndustry(Integer dateType) {
        String field = "OrgContentNum";
        String startDate = convertDate(dateType);
        // 统计活动数——行业
        List<Map<String, Object>> activity = instituteDataCenterDaoImpl.statisticActivityNumByIndustry(startDate);
        instituteDataCenterDaoImpl.saveDuplicate(activity, dateType, companyCode, field, 2);
        // 统计研报——行业
        List<Map<String, Object>> report = instituteDataCenterDaoImpl.statisticReportNumByIndustry(startDate);
        instituteDataCenterDaoImpl.saveDuplicate(report, dateType, companyCode, field, 2);
        
    }

    private void statisticMarketContentNumByIndustry(Integer dateType) {
        // 请求每市数据
    }

    private void statisticsAttentionByIndustry(Integer dateType) {
        // 1. 全市场关注度
        statisticsMarketAttentionByIndustry(dateType);
        // 2. 本所关注度
        statisticsSelfAttentionByIndustry(dateType, 0, "OrgAttention");
        // 3. 本所白名单关注度
        statisticsSelfAttentionByIndustry(dateType, 1, "OrgWhiteAttention");
    }
    
    private void statisticsSelfAttentionByIndustry(Integer dateType, Integer type, String field) {
        String startDate = convertDate(dateType);
        // 1.统计研报阅读分享数
        List<Map<String, Object>> report = instituteDataCenterDaoImpl.statisticsReportReadAndShareByIndustry(startDate, type);
        instituteDataCenterDaoImpl.saveDuplicate(report, dateType, companyCode, field, 2);
        // 2.活动阅读分享 (包括电话会议)
        List<Map<String, Object>> activity = instituteDataCenterDaoImpl.statisticsActivityReadAndShareByIndustry(startDate, type);
        instituteDataCenterDaoImpl.saveDuplicate(activity, dateType, companyCode, field, 2);
        // 3.活动参加人数
        List<Map<String, Object>> join = instituteDataCenterDaoImpl.statisticsActivityJoinNumByIndustry(startDate, type);
        instituteDataCenterDaoImpl.saveDuplicate(join, dateType, companyCode, field, 2);
    }

    private void statisticsMarketAttentionByIndustry(Integer dateType) {
        // http请求每市数据
    }

    private void generateHeatDataByInnerCode(Integer dateType) {
        // 1、统计关注度
        statisticsAttentionByInnerCode(dateType);
        // 2、统计内容数
        statisticContentNumByInnerCode(dateType);
    }

    private void statisticContentNumByInnerCode(Integer dateType) {
        // 全市场
        statisticMarketContentNumByInnerCode(dateType);
        // 本所
        statisticSelfContentNumByInnerCode(dateType);
    }

    private void statisticSelfContentNumByInnerCode(Integer dateType) {
        String startDate = convertDate(dateType);
        String field = "OrgContentNum";
        // 统计活动数——股票
        List<Map<String, Object>> activity = instituteDataCenterDaoImpl.statisticActivityNumByInnerCode(startDate);
        instituteDataCenterDaoImpl.saveDuplicate(activity, dateType, companyCode, field, 1);
        // 统计研报——股票
        List<Map<String, Object>> report = instituteDataCenterDaoImpl.statisticReportNumByInnerCode(startDate);
        instituteDataCenterDaoImpl.saveDuplicate(report, dateType, companyCode, field, 1);
    }

    private void statisticMarketContentNumByInnerCode(Integer dateType) {
     // http请求每市数据
    }

    private void statisticsAttentionByInnerCode(int dateType) {
        // 1. 全市场关注度
        statisticsMarketAttentionByInnerCode(dateType);
        // 2. 本所关注度
        statisticsSelfAttentionByInnerCode(dateType, 0, "OrgAttention");
        // 3. 本所白名单关注度
        statisticsSelfAttentionByInnerCode(dateType, 1, "OrgWhiteAttention");
    }

    private void statisticsSelfAttentionByInnerCode(Integer dateType, Integer type, String field) {
        String startDate = convertDate(dateType);
        // 1.统计研报阅读分享数-按股票
        List<Map<String, Object>> report = instituteDataCenterDaoImpl.statisticsReportReadAndShareByInnerCode(startDate, type);
        instituteDataCenterDaoImpl.saveDuplicate(report, dateType, companyCode, field, 1);
        // 2.活动阅读 分享数(包括电话会议)
        List<Map<String, Object>> activity = instituteDataCenterDaoImpl.statisticsActivityReadAndShareByInnerCode(startDate, type);
        instituteDataCenterDaoImpl.saveDuplicate(activity, dateType, companyCode, field, 1);
        // 3.活动参加人数
        List<Map<String, Object>> join = instituteDataCenterDaoImpl.statisticsActivityJoinNumByInnerCode(startDate, type);
        instituteDataCenterDaoImpl.saveDuplicate(join, dateType, companyCode, field, 1);
    }

    private void statisticsMarketAttentionByInnerCode(Integer dateType) {
        // 请求每市数
    }

    private String convertDate(int dateType) {
        String date = null;
        String dateStr = DateUtil.dateToStr(new Date(), DateUtil.dateShortFormat);
        switch (dateType) {
            case 1 :
                date = DateUtil.getMonthDate(dateStr, -1);
                break;
            case 2 :
                date = DateUtil.getMonthDate(dateStr, -3);
                break;

            case 3 :
                date = DateUtil.getMonthDate(dateStr, -6);
                break;

            case 4 :
                date = DateUtil.getMonthDate(dateStr, -12);
                break;
            default :
                break;
        }
        return date;
    }

    @Override
    public void clearHeadData() {
        instituteDataCenterDaoImpl.clearHeadData();
    }

    @Override
    public void copyInstituteHeatData() {
        instituteDataCenterDaoImpl.copyInstituteHeatData();
    }

}
