package com.meix.institute.pojo;

import com.meix.institute.search.BaseSearchVo;

public class InsUserSearch extends BaseSearchVo{
    
    private static final long serialVersionUID = 1L;
    
    private long id;
    private String userName;
    private String password;
    private String mobileOrEmail;
    private String position;
    private Integer userState;
    private Integer role;
    private Integer accountType;
    
    public Integer getAccountType() {
        return accountType;
    }
    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }
    public Integer getRole() {
        return role;
    }
    public void setRole(Integer role) {
        this.role = role;
    }
    public void setUserState(Integer userState) {
        this.userState = userState;
    }
    public Integer getUserState() {
        return userState;
    }
    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }
    public String getMobileOrEmail() {
        return mobileOrEmail;
    }
    public void setMobileOrEmail(String mobileOrEmail) {
        this.mobileOrEmail = mobileOrEmail;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
}
