package com.meix.institute.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class InsArticleGroupInfo implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private Long id;
    private String title;
    private Date publishTime;
    private Integer state;
    private Date updatedAt;
    private String updatedBy;
    private String stateText;
    private List<Long> articleIds;
    private String mediaId;
    private String openIds;

    private List<InsArticleInfo> articles;

}
