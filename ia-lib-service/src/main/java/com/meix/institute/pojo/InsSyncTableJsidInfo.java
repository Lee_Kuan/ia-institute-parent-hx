package com.meix.institute.pojo;

import java.io.Serializable;
import java.util.Date;

public class InsSyncTableJsidInfo implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private Long id;
    private String tableName;  
    private Date jsUpdateTimeFm;      
    private Date jsUpdateTimeTo;
    private String state;
    private Integer retryCnt;   
    private Date createdAt;
    private Date updatedAt;
    private String updatedBy;
    private String remarks;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public Date getJsUpdateTimeFm() {
        return jsUpdateTimeFm;
    }
    public void setJsUpdateTimeFm(Date jsUpdateTimeFm) {
        this.jsUpdateTimeFm = jsUpdateTimeFm;
    }
    public Date getJsUpdateTimeTo() {
        return jsUpdateTimeTo;
    }
    public void setJsUpdateTimeTo(Date jsUpdateTimeTo) {
        this.jsUpdateTimeTo = jsUpdateTimeTo;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public Integer getRetryCnt() {
        return retryCnt;
    }
    public void setRetryCnt(Integer retryCnt) {
        this.retryCnt = retryCnt;
    }
    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    public Date getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    public String getRemarks() {
        return remarks;
    }
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
