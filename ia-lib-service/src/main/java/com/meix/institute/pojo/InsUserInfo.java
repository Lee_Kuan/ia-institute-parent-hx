package com.meix.institute.pojo;

import java.io.Serializable;
import java.util.Date;

public class InsUserInfo implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private Long id;
    private String user;
    private Integer userType;
    private Long companyCode;
    private String userName;
    private String oaName;
    private String password;
    private String mobile;
    private String email;
    private String position;
    private String qualifyno;
    private String department;
    private String headImageUrl;
    private String chiSpelling;
    private Integer userState;
    private String salt;
    private String descr;
    private String direction;
    private Integer role;
    private String thirdId;
    private Integer checkStatus;
    private Date createdAt;
    private String createdBy;
    private Date updatedAt;
    private String updatedBy;
    
    private String token;
    private Integer status;
    private String roleText;
    private String positionText;
    
    public String getQualifyno() {
		return qualifyno;
	}
	public void setQualifyno(String qualifyno) {
		this.qualifyno = qualifyno;
	}
	public String getOaName() {
		return oaName;
	}
	public void setOaName(String oaName) {
		this.oaName = oaName;
	}
	public Integer getCheckStatus() {
        return checkStatus;
    }
    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }
    public String getDirection() {
        return direction;
    }
    public void setDirection(String direction) {
        this.direction = direction;
    }
    public String getRoleText() {
        return roleText;
    }
    public void setRoleText(String roleText) {
        this.roleText = roleText;
    }
    public String getPositionText() {
        return positionText;
    }
    public void setPositionText(String positionText) {
        this.positionText = positionText;
    }
    public Integer getUserType() {
        return userType;
    }
    public void setUserType(Integer userType) {
        this.userType = userType;
    }
    public String getThirdId() {
        return thirdId;
    }
    public void setThirdId(String thirdId) {
        this.thirdId = thirdId;
    }
    public String getDepartment() {
        return department;
    }
    public void setDepartment(String department) {
        this.department = department;
    }
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public Integer getRole() {
        return role;
    }
    public void setRole(Integer role) {
        this.role = role;
    }
    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }
    public String getSalt() {
        return salt;
    }
    public void setSalt(String salt) {
        this.salt = salt;
    }
    public String getMobile() {
        return mobile;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getHeadImageUrl() {
        return headImageUrl;
    }
    public void setHeadImageUrl(String headImageUrl) {
        this.headImageUrl = headImageUrl;
    }
    public String getChiSpelling() {
        return chiSpelling;
    }
    public void setChiSpelling(String chiSpelling) {
        this.chiSpelling = chiSpelling;
    }
    public Integer getUserState() {
        return userState;
    }
    public void setUserState(Integer userState) {
        this.userState = userState;
    }
    public String getDescr() {
        return descr;
    }
    public void setDescr(String descr) {
        this.descr = descr;
    }
    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    public Date getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public Long getCompanyCode() {
        return companyCode;
    }
    public void setCompanyCode(Long companyCode) {
        this.companyCode = companyCode;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
}
