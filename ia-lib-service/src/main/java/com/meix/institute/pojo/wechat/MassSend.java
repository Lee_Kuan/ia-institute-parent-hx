package com.meix.institute.pojo.wechat;

import lombok.Data;
import net.sf.json.JSONObject;

@Data
public class MassSend {
    
    private String is_to_all;
    private String tag_id;
    private String media_id;
    private String recommend;
    private String msgtype;
    private String title;
    private String description;
    private String thumb_media_id;
    private String send_ignore_reprint;
    
    public JSONObject convertMpnews() {
        JSONObject result = new JSONObject();
        JSONObject filter = new JSONObject();
        filter.put("is_to_all", is_to_all);
        filter.put("tag_id", tag_id);
        result.put("filter", filter);
        
        JSONObject mpnews = new JSONObject();
        mpnews.put("media_id", media_id);
        result.put("mpnews", mpnews);
        
        result.put("msgtype", msgtype);
        result.put("send_ignore_reprint", send_ignore_reprint);
        return result;
    }
}
