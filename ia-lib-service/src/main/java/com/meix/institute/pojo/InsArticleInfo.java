package com.meix.institute.pojo;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class InsArticleInfo implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private Long id;
    private String title;
    private String author;
    private Date publishTime;
    private String content;
    private Integer state;
    private String stateText;
    private String mediaId;
    private String sharePictureMediaId;
    private String sharePicture;
    private String shareContent;
    private Date updatedAt;
    private String updatedBy;
    private Integer isSharePicture;
    private String contentSourceUrl;
    private Integer openComment;
    private Integer onlyFansCanComment;
   
}
