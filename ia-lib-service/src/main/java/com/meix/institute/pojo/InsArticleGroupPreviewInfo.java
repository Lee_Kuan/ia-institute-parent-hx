package com.meix.institute.pojo;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class InsArticleGroupPreviewInfo implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private Long id;
    private List<Long> articleIds;
    private String openIds;
}
