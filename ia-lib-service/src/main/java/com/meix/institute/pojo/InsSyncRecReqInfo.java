package com.meix.institute.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 接受请求
 *
 * @author Xueyc
 */
@Data
public class InsSyncRecReqInfo implements Serializable {
	private static final long serialVersionUID = 8199790856310122629L;
	private Long id;

	private String sndApp;
	private String recApp;
	private String method;
	private String state;
	private Integer retryMax;
	private Integer retryCnt;
	private Date retryLast;
	private String reqHeader;
	private String reqBody;

	private String remarks;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;

	private int dataType;
	private String keyword;

}
