package com.meix.institute.pojo.wechat;

import java.io.Serializable;

import lombok.Data;

@Data
public class Material implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private String title;
    private String thumb_media_id;
    private String author;
    private String digest;
    private int show_cover_pic;
    private String content;
    private String content_source_url;
    private int need_open_comment;
    private int only_fans_can_comment;
    
}
