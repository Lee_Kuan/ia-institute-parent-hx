package com.meix.institute.pojo;

import com.meix.institute.search.BaseSearchVo;

public class InsSyncTableJsidSearch extends BaseSearchVo{
    
    private static final long serialVersionUID = 1L;
    
    private Long id;
    private String tableName;  
    private String state;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    
}
