package com.meix.institute.pojo;

import java.io.Serializable;

/**
 * 同步对象
 * @author Xueyc
 *
 */
public class SyncAction implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private Long id;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
}
