package com.meix.institute.pojo;

import java.util.List;

import com.meix.institute.search.BaseSearchVo;

public class InsArticleSearch extends BaseSearchVo{

    private static final long serialVersionUID = 1L;
    
    /**
     * 标题
     */
    private String title;
    /**
     * 状态
     */
    private Integer state;
    
    private List<Long> excludeArticleIds;
    
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Integer getState() {
        return state;
    }
    public void setState(Integer state) {
        this.state = state;
    }
    public List<Long> getExcludeArticleIds() {
        return excludeArticleIds;
    }
    public void setExcludeArticleIds(List<Long> excludeArticleIds) {
        this.excludeArticleIds = excludeArticleIds;
    }
    
}
