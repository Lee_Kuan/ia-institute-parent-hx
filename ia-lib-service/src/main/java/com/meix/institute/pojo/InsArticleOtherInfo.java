package com.meix.institute.pojo;

import lombok.Data;

@Data
public class InsArticleOtherInfo {
    
    private Long id;
    private String type;
    private String mediaId;
    private String localUrl;
    private String remoteUrl;
}
