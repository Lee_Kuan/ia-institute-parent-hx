package com.meix.institute.pojo;

import java.io.Serializable;
import java.util.Date;

public class InsSyncSndReqInfo implements Serializable {
	private static final long serialVersionUID = 7587551268209936509L;
	private Long id;

	private String sndApp;
	private String recApp;
	private String method;
	private String state;
	private Integer retryMax;
	private Integer retryCnt;
	private Date retryLast;
	private String reqHeader;
	private String reqBody;
	private String result;

	private String remarks;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;

	private int dataType;
	private String ketword;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSndApp() {
		return sndApp;
	}

	public void setSndApp(String sndApp) {
		this.sndApp = sndApp;
	}

	public String getRecApp() {
		return recApp;
	}

	public void setRecApp(String recApp) {
		this.recApp = recApp;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getRetryMax() {
		return retryMax;
	}

	public void setRetryMax(Integer retryMax) {
		this.retryMax = retryMax;
	}

	public Integer getRetryCnt() {
		return retryCnt;
	}

	public void setRetryCnt(Integer retryCnt) {
		this.retryCnt = retryCnt;
	}

	public Date getRetryLast() {
		return retryLast;
	}

	public void setRetryLast(Date retryLast) {
		this.retryLast = retryLast;
	}

	public String getReqHeader() {
		return reqHeader;
	}

	public void setReqHeader(String reqHeader) {
		this.reqHeader = reqHeader;
	}

	public String getReqBody() {
		return reqBody;
	}

	public void setReqBody(String reqBody) {
		this.reqBody = reqBody;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public String getKetword() {
		return ketword;
	}

	public void setKetword(String ketword) {
		this.ketword = ketword;
	}
}
