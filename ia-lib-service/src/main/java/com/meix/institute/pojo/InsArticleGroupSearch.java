package com.meix.institute.pojo;

import java.util.Date;

import lombok.Data;

@Data
public class InsArticleGroupSearch{

    private Date publishTimeFm;
    private Date publishTimeTo;
    private Integer state;

}
