package com.meix.institute.util;

import com.meix.institute.config.CommonServerProperties;

public class OssResourceUtil {

	private static CommonServerProperties commonServerProperties;

	static {
		commonServerProperties = ApplicationUtil.getBean(CommonServerProperties.class);
	}

	/**
	 * @param key
	 * @return
	 * @Title: getHeadImage、
	 * @Description:获取完整头像
	 * @return: String
	 */
	public static String getHeadImage(String key) {
		if (key == null || key.equals("")) {
			return "";
		}
		if (key.startsWith("http")) {
			return key;
		}
		String imageServer = commonServerProperties.getFileServer();
		if (imageServer.endsWith("/") && key.startsWith("/")) {
			imageServer = imageServer.substring(0, imageServer.length() - 1);
		}
		return imageServer + key;
	}
}
