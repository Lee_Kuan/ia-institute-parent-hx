package com.meix.institute.util;

import com.meix.institute.api.IWechatAccessTokenService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.entity.WechatAccessToken;
import com.meix.institute.vo.wechat.AccessTokenInfo;
import com.meix.institute.vo.wechat.SmallProgResult;
import com.meix.institute.vo.wechat.templateMsg.BaseTemplate;
import com.meix.institute.vo.wechat.templateMsg.Data;
import com.meix.institute.vo.wechat.templateMsg.Model;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.*;

/**
 * Created by zenghao on 2019/7/11.
 */
public class WechatUtil {

	protected final static Logger log = LoggerFactory.getLogger(WechatUtil.class);

	/**
	 * wechat web page authorize url
	 */
	private static final String authorize = "/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect";
	/**
	 * get access_token|jsapi
	 */
	private static final String access_token = "/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";

	/**
	 * 获取accessToken
	 */
	private static final String GET_WX_ACCESS_TOKEN_URL = "/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";

	/**
	 * wechat's ticket
	 */
	private static final String jsapi_ticket = "/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";

	/**
	 * get wechat userinfo
	 */
	private static final String snsapi_userinfo = "/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";

	/**
	 * 发送微信模板消息
	 */
	private static final String SEND_WX_TEMPLATE_MSG_POST_URL = "/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";

	/**
	 * 小程序获取openId
	 */
	private static final String GET_WX_OPENID = "/sns/jscode2session?appid=APPID&secret=APPSECRET&js_code=JSCODE&grant_type=authorization_code";

	/**
	 * 小程序码获取
	 */
	private static final String GET_WX_SP_ACODE = "/wxa/getwxacodeunlimit?access_token=ACCESS_TOKEN";

	/**
	 * 公众号新增其他类型永久素材
	 */
	public static String MATERIAL_ADD_MATERIAL = "/cgi-bin/material/add_material?access_token=ACCESS_TOKEN&type=TYPE";

	/**
	 * 新增永久图文素材
	 */
	public static String MATERIAL_ADD_NEWS = "/cgi-bin/material/add_news?access_token=ACCESS_TOKEN";

	/**
	 * 新增永久图文素材
	 */
	public static String MATERIAL_UPDATE_NEWS = "/cgi-bin/material/update_news?access_token=ACCESS_TOKEN";

	/**
	 * 上传图文消息素材(临时)
	 */
	public static String MATERIAL_UPLOADNEWS = "/cgi-bin/media/uploadnews?access_token=ACCESS_TOKEN";

	/**
     * 新增素材(临时)
     */
    public static String MATERIAL_UPLOAD = "/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE";

	/**
	 * 上传图文消息内的图片获取URL
	 */
	public static String MEDIA_UPLOADIMG = "/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN";

	/**
	 * 根据标签进行群发
	 */
	public static String MASS_SENDALL = "/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN";

	/**
	 * 根据OpenID列表群发
	 */
	public static String MASS_SEND = "/cgi-bin/message/mass/send?access_token=ACCESS_TOKEN";

	/**
	 * 预览接口
	 */
	public static String MASS_PREVIEW = "/cgi-bin/message/mass/preview?access_token=ACCESS_TOKEN";

	public static final String PARAM_WX_ACCESS_TOKEN = "ACCESS_TOKEN";
	public static final String PARAM_WX_TYPE = "TYPE";
	public static final String PARAM_WX_APPID = "APPID";
	public static final String PARAM_WX_APPSECRET = "APPSECRET";
	public static final String PARAM_WX_JSCODE = "JSCODE";

	public static final String WX_MSG_SEND_SUCCESS = "\"errcode\":0";

	private static IWechatAccessTokenService wechatAccessTokenService;
	private static CommonServerProperties commonServerProperties;

	static {
		wechatAccessTokenService = ApplicationUtil.getBean(IWechatAccessTokenService.class);
		commonServerProperties = ApplicationUtil.getBean(CommonServerProperties.class);
	}

	/**
	 * 微信基础access_token
	 */
	public static String baseAccessToken(String appid, String appsecret) {
		String baseUrl = commonServerProperties.getApiServer() + GET_WX_ACCESS_TOKEN_URL;
		return baseUrl.replaceFirst(PARAM_WX_APPID, appid).replaceFirst(PARAM_WX_APPSECRET, appsecret);
	}

	/**
	 * 获取微信ticket
	 */
	public static String jsapiTicket(String access_token) {
		String baseUrl = commonServerProperties.getApiServer() + jsapi_ticket;
		return baseUrl.replaceFirst(PARAM_WX_ACCESS_TOKEN, access_token);
	}

	public static String getAcodeUrl(String access_token) {
		String baseUrl = commonServerProperties.getApiServer() + GET_WX_SP_ACODE;
		return baseUrl.replaceFirst(PARAM_WX_ACCESS_TOKEN, access_token);
	}

	/**
	 * 以snsapi_userinfo|snsapi_base方式授权请求
	 */
	public static String authorize(String appId, String redirectUri, String state, String scope) throws IOException {
		String tempurl = StringUtils.encodeUrl(redirectUri, "UTF-8");
		String baseUrl = commonServerProperties.getOpenServer() + authorize;
		return baseUrl.replaceFirst(PARAM_WX_APPID, appId).replaceFirst("REDIRECT_URI", tempurl).replaceFirst("SCOPE", scope).replaceFirst("STATE", state);
	}

	/**
	 * 获取access_token
	 */
	public static String accessToken(String appid, String secret, String code) {
		String baseUrl = commonServerProperties.getApiServer() + access_token;
		return baseUrl.replaceFirst(PARAM_WX_APPID, appid).replaceFirst("SECRET", secret).replaceFirst("CODE", code);
	}

	/**
	 * 生成获取微信用户信息url
	 *
	 * @author jiawj 2016年9月6日 下午4:18:08
	 * @since 1.7.5
	 */
	public static String getUserinfoUrl(String access_token, String openid, String lang) {
		if (StringUtils.isEmpty(lang)) {
			lang = "zh_CN";
		}
		if (StringUtils.isEmpty(access_token) || StringUtils.isEmpty(openid)) {
			return null;
		}
		String baseUrl = commonServerProperties.getApiServer() + snsapi_userinfo;
		return replaceKey(replaceKey(replaceKey(baseUrl, "ACCESS_TOKEN", access_token), "OPENID", openid), "zh_CN", lang);
	}

	private static String replaceKey(String content, String target, String replace) {
		if (StringUtils.isEmpty(content) || StringUtils.isEmpty(target)) {
			return content;
		}
		return content.replaceFirst(target, replace);
	}

	/**
	 * 检测是否超过2小时
	 *
	 * @return 超过返回 true / 未超过返回  false
	 * @since 1.9.0
	 */
	private static boolean checkOver2hour(Date datetime) {
		try {
			if (datetime == null) {
				return true;
			}
			if ((System.currentTimeMillis() - datetime.getTime()) >= (1000 * 60 * 55) * 2) { //接近2小时
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return true;
	}

	/**
	 * 检测access_token并返回
	 */
	public static String updateAccessToken(long companyCode, String appId, String secret, boolean isCheckHour) {
		if (isCheckHour) {
			WechatAccessToken accessTokenInfo = wechatAccessTokenService.getWechatAccessToken(companyCode, appId);
			if (accessTokenInfo != null && !checkOver2hour(accessTokenInfo.getUpdateTime())) {
				return accessTokenInfo.getAccessToken();
			}
		}

		String baseAccessTokenUrl = baseAccessToken(appId, secret);
		JSONObject accessTokenObj = HttpUtil.getWechat(baseAccessTokenUrl);
		if (accessTokenObj == null) {
			return null;
		}
		AccessTokenInfo accessTokenInfo = GsonUtil.json2Obj(accessTokenObj.toString(), AccessTokenInfo.class);
		if (accessTokenInfo == null) {
			return null;
		}
		accessTokenInfo.setUpdate_time(DateUtil.getCurrentDateTime());
		WechatAccessToken accessToken = wechatAccessTokenService.getWechatAccessToken(companyCode, appId);
		if (accessToken == null) {
			wechatAccessTokenService.saveWechatAccessToken(companyCode, appId, accessTokenInfo.getAccess_token(), accessTokenInfo.getExpires_in(), accessTokenInfo.getUpdate_time());
		} else {
			wechatAccessTokenService.updateWechatAccessToken(companyCode, appId, accessTokenInfo.getAccess_token(), accessTokenInfo.getExpires_in(), accessTokenInfo.getUpdate_time());
		}
		return accessTokenInfo.getAccess_token();
	}

	public static String updateJsapiTicket(long companyCode, String appId, String accessToken, boolean isCheckHour) {
		if (isCheckHour) {
			WechatAccessToken ticket = wechatAccessTokenService.getWechatTicket(companyCode, appId);
			if (ticket != null) {
				if (!checkOver2hour(ticket.getUpdateTime())) {
					return ticket.getAccessToken();
				}
			}
		}

		String jsapiTicketUrl = jsapiTicket(accessToken);
		JSONObject jsapiticketObj = HttpUtil.getWechat(jsapiTicketUrl);
		if (jsapiticketObj == null) {
			return null;
		}
		AccessTokenInfo ticketInfo = GsonUtil.json2Obj(jsapiticketObj.toString(), AccessTokenInfo.class);
		if (ticketInfo == null) {
			return null;
		}
		if ("ok".equals(ticketInfo.getErrmsg())) {
			ticketInfo.setUpdate_time(DateUtil.getCurrentDateTime());
			WechatAccessToken ticket = wechatAccessTokenService.getWechatTicket(companyCode, appId);
			if (ticket == null) {
				wechatAccessTokenService.saveWechatTicket(companyCode, appId, ticketInfo.getTicket(), ticketInfo.getExpires_in(), ticketInfo.getUpdate_time());
			} else {
				wechatAccessTokenService.updateWechatTicket(companyCode, appId, ticketInfo.getTicket(), ticketInfo.getExpires_in(), ticketInfo.getUpdate_time());
			}
			return ticketInfo.getTicket();
		}
		log.info("【checkJsapiTicket】 -- 重新调用jsapi_ticket接口:{}", jsapiticketObj.toString());
		return null;
	}

	/**
	 * 获取小程序的openid等信息
	 *
	 * @param appId
	 * @param secret
	 * @param jscode
	 * @return
	 */
	public static SmallProgResult getSmallProgramOpenId(String appId, String secret, String jscode) {
		String baseUrl = commonServerProperties.getApiServer() + GET_WX_OPENID;
		String url = baseUrl.replace(PARAM_WX_APPID, appId).replace(PARAM_WX_APPSECRET, secret).replace(PARAM_WX_JSCODE, jscode);
		JSONObject wxdata = HttpUtil.getWechat(url);
		if (wxdata == null) {
			return null;
		}
		SmallProgResult result = GsonUtil.json2Obj(wxdata.toString(), SmallProgResult.class);
		return result;
	}

	public static String getServiceNoticeTemplate(String template_id, String openid, String title,
	                                              String content, String sender, String end, String url) {
		if (StringUtil.isBlank(template_id)) {
			return null;
		}
		try {
			Model firstModel = new Model(title, "#3F52B6");
			Model contentModel = new Model(content, "#666666");
			Model senderModel = new Model(sender, "#E94222");
			Model remarkModel = new Model(end, "#666666");

			Data.ServiceNoticeData data = new Data.ServiceNoticeData(firstModel, contentModel, senderModel, remarkModel);

			String touser = openid;
			String topcolor = "#FF0000";
			BaseTemplate.TemplateReserve params = new BaseTemplate.TemplateReserve();
			params.setTouser(touser);
			params.setTemplate_id(template_id);
			params.setTopcolor(topcolor);
			params.setData(data);
			params.setUrl(url);
			return GsonUtil.obj2Json(params);
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	public static String getExamineNoticeTemplate(String template_id, String openid, String title,
	                                              String theme, String time, String result, String reason, String end, String url) {
		if (StringUtil.isBlank(template_id)) {
			return null;
		}
		try {
			Model firstModel = new Model(title, "#3F52B6");
			Model themeModel = new Model(theme, "#666666");
			Model timeModel = new Model(time, "#666666");
			Model resultModel = new Model(result, "#E94222");
			Model reasonModel = new Model(reason, "#E94222");
			Model remarkModel = new Model(end, "#666666");

			Data.ExamineNoticeData data = new Data.ExamineNoticeData(firstModel, themeModel, timeModel, resultModel, reasonModel, remarkModel);

			String touser = openid;
			String topcolor = "#FF0000";
			BaseTemplate.TemplateReserve params = new BaseTemplate.TemplateReserve();
			params.setTouser(touser);
			params.setTemplate_id(template_id);
			params.setTopcolor(topcolor);
			params.setData(data);
			params.setUrl(url);
			return GsonUtil.obj2Json(params);
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	/**
	 * 发送微信公众号推送
	 *
	 * @param appId
	 * @param secret
	 * @param template
	 * @return 是否发送成功
	 */
	public static boolean sendTemplateMsg(long companyCode, String appId, String secret, String template) {
		String access_token = updateAccessToken(companyCode, appId, secret, true);
		if (StringUtil.isBlank(access_token)) {
			log.info("未获取到公众号推送access_token");
			return false;
		}
		String baseUrl = commonServerProperties.getApiServer() + SEND_WX_TEMPLATE_MSG_POST_URL;
		String url = baseUrl.replace(PARAM_WX_ACCESS_TOKEN, access_token);
		String result = HttpUtil.httpPost(url, template, "");
		log.info("发送微信模板消息！appId：{}，secret：{}，template：{}，result：{}", appId, secret, template, result);
		if (StringUtil.isBlank(result)) {
			return false;
		}
		if (result.contains(WX_MSG_SEND_SUCCESS)) {
			return true;
		}
		return false;
	}

	/**
	 * 签名算法
	 *
	 * @param jsapi_ticket
	 * @param url
	 * @return
	 */
	public static Map<String, String> sign(String jsapi_ticket, String url) {
		Map<String, String> ret = new HashMap<String, String>();
		String nonce_str = create_nonce_str();
		String timestamp = create_timestamp();
		String string1;
		String signature = "";

		//注意这里参数名必须全部小写，且必须有序
		string1 = "jsapi_ticket=" + jsapi_ticket +
			"&noncestr=" + nonce_str +
			"&timestamp=" + timestamp +
			"&url=" + url;
		//System.out.println(string1);

		try {
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.reset();
			crypt.update(string1.getBytes("UTF-8"));
			signature = byteToHex(crypt.digest());
		} catch (Exception e) {
			e.printStackTrace();
		}

		ret.put("url", url);
		ret.put("jsapi_ticket", jsapi_ticket);
		ret.put("nonceStr", nonce_str);
		ret.put("timestamp", timestamp);
		ret.put("signature", signature);

		return ret;
	}

	/**
	 * 根据标签进行群发
	 *
	 * @return
	 */
	public static JSONObject sendall() {
		return null;
	}

	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}

	private static String create_nonce_str() {
		return UUID.randomUUID().toString();
	}

	private static String create_timestamp() {
		return Long.toString(System.currentTimeMillis() / 1000);
	}

	public static void main(String[] args) {
		//String appId = "wxd324aa1a87b99540";
		//String appSecret = "653777522e8c80d4f205079ecb4e5507";
		String title = "尊敬的张震，我们为您推荐了您可能感兴趣的内容";
		String content = "电话会议";
		String sender = "《房地产（801180.SW）国信证券 国家安全政策及网络投资机会交流》";
		String end = "请注意参加";
		String tempId = "3Cmqijuea-1aHpZtUiSWOBE0xyNhaaqcFWr3fLkWx-8";
		String openId = "oKhlos0vIaWq2z2p7nRg-IiSEfjI";
		String temp = "{\"data\":{\"first\":{\"value\":\"%s\",\"color\":\"#3F52B6\"},\"keyword1\":{\"value\":\"%s\",\"color\":\"#666666\"},\"keyword2\":{\"value\":\"%s\",\"color\":\"#E94222\"},\"remark\":{\"value\":\"%s\",\"color\":\"#666666\"}},\"touser\":\"%s\",\"template_id\":\"%s\", \"url\":\"%s\", \"topcolor\":\"#FF0000\"}";
		String url = "https://dev.meix.com/institute/detail/report/618338086001/256";
		String tempContent = String.format(temp, title, content, sender, end, openId, tempId, url);
		System.out.println(tempContent);
		//WechatUtil.sendTemplateMsg(appId, appSecret, tempContent);


		String serviceNoticeTemplate = getServiceNoticeTemplate("template_id", "openid", "title", "content", "sender", "end", "url");
		System.out.println(serviceNoticeTemplate);
	}

}
