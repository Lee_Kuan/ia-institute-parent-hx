package com.meix.institute.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by zenghao on 2019/9/17.
 */
public class FileUtil {

	/**
	 * 保存到临时文件夹中
	 */
	public static File createTempFile(InputStream in, String path, String fileFullName) {

		StringBuffer tempPath = new StringBuffer(FileUtils.getTempDirectoryPath());
		if (!org.apache.commons.lang.StringUtils.isEmpty(path)) {
			tempPath.append(File.separator);
			tempPath.append(path);
		}
		tempPath.append(File.separator);
		tempPath.append(fileFullName);

		File file = new File(tempPath.toString());
		if (file.exists()) {
			file.delete();
		}
		try {
			FileUtils.copyInputStreamToFile(in, file);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return file;
	}
	/**
	 * 保存到临时文件夹中
	 */
	public static File createTempFile(byte[] data, String path, String fileFullName) {

		StringBuffer tempPath = new StringBuffer(FileUtils.getTempDirectoryPath());
		if (!org.apache.commons.lang.StringUtils.isEmpty(path)) {
			tempPath.append(File.separator);
			tempPath.append(path);
		}
		tempPath.append(File.separator);
		tempPath.append(fileFullName);

		File file = new File(tempPath.toString());
		if (file.exists()) {
			file.delete();
		}
		try {
			FileUtils.writeByteArrayToFile(file, data);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return file;
	}
}
