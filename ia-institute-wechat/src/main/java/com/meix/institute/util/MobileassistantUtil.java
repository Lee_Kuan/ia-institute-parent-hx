package com.meix.institute.util;

import com.meix.institute.constant.MeixConstants;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * @类说明 : 移动投资研究助手工具类
 */
public final class MobileassistantUtil {

	public static Logger log = LoggerFactory.getLogger(MobileassistantUtil.class);

	/**
	 * @param token
	 * @return
	 * @Title: getUserId、
	 * @Description:获取Token中的用户ID
	 * @return: String
	 */
	public static long getUserId(String token) {
		if (StringUtils.isEmpty(token)) {
			return 0;
		}
		String str = Des.decrypt(token);
		if (StringUtils.isBlank(str)) {
			return 0;
		}
		return Long.parseLong(str.split(MeixConstants.tokenSplite)[0]);
	}

	/**
	 * @param request
	 * @return
	 * @Title: getRequestParams、
	 * @Description:获取请求参数
	 * @return: JSONObject
	 */
	public static JSONObject getRequestParams(HttpServletRequest request) {
		String clientstr = (String) request.getAttribute("clientstr");
		if (StringUtils.isEmpty(clientstr)) {
			clientstr = request.getParameter("clientstr");
		}
		JSONObject obj = null;
		try {
			obj = JSONObject.fromObject(clientstr);
			if (obj != null) {
				String token = MapUtils.getString(obj, "token");
				if (!StringUtils.isEmpty(token)) {
					try {
						String str = Des.decrypt(token);
						if (StringUtils.isNotBlank(str)) {
							long uid = Long.parseLong(str.split(MeixConstants.tokenSplite)[0]);
							String user = str.split(MeixConstants.tokenSplite)[1];
							obj.put("uid", uid);
							obj.put("uuid", user);
						}
					} catch (Exception e) {
						log.warn("token【{}】解密失败", token);
					}
				}
				//设置最大查询数量showNum--50
				if (null != obj.get("showNum")) {
					int showNum = MapUtils.getIntValue(obj, "showNum");
					if (showNum > 50) {
						obj.put("showNum", 50);
					}
				}
				//设置clientType
				String clientType = request.getHeader(MeixConstants.CLIENT_TYPE_HEAD);
				obj.put("clientType", clientType == null ? 0 : clientType);
			}
			return obj;
		} catch (Exception e) {
			log.warn("获取请求参数异常", e);
		}
		return obj;
	}

	public static String getMobile(String mobile) {
		try {
			Long.parseLong(mobile);
		} catch (Exception e) {
			mobile = "";
		}
		return mobile;
	}
}
