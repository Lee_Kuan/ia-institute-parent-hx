package com.meix.institute.config;

import com.meix.institute.config.bo.SecretProperty;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class EmailConfig {
	@Autowired
	private SecretProperty secretProperty;

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	public JavaMailSenderImpl mailSender() throws Exception {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(secretProperty.getStringValue("email.host"));
		mailSender.setPort(secretProperty.getIntValue("email.port"));
		mailSender.setUsername(secretProperty.getStringValue("email.username"));
		mailSender.setPassword(secretProperty.getStringValue("email.password"));
		Properties javaMailProperties = new Properties();
		javaMailProperties.setProperty("mail.smtp.auth", "true");
		javaMailProperties.setProperty("mail.smtp.timeout", secretProperty.getStringValue("email.timeout"));
		javaMailProperties.setProperty("mail.debug", secretProperty.getStringValue("email.isDebug"));
		String socketFactory = secretProperty.getStringValue("email.socketFactory");
		if (StringUtils.isNotBlank(socketFactory)) {
			javaMailProperties.setProperty("mail.smtp.socketFactory.class", socketFactory);
		}
		mailSender.setJavaMailProperties(javaMailProperties);
		return mailSender;
	}
}
