package com.meix.institute.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meix.institute.api.IUserService;
import com.meix.institute.config.vo.MVC;
import com.meix.institute.config.vo.ServerProperties;
import com.meix.institute.interceptor.ExecuteHandlerInterceptor;
import com.meix.institute.interceptor.ThreadLocalInterceptor;
import com.meix.institute.interceptor.VerifyHandlerInterceptor;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.AesUtil;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.SecurityUtil;
import com.meix.institute.util.VersionUtil;
import com.meix.institute.vo.user.UserInfo;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.util.UrlPathHelper;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @Description:配置mvc
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.meix"})
public class MVCConfig implements WebMvcConfigurer, SchedulingConfigurer {
	@Autowired
	private VerifyHandlerInterceptor verifyHandlerInterceptor;
	@Autowired
	private ThreadLocalInterceptor threadLocalInterceptor;
	@Autowired
	private MVC mvc;
	@Resource
	private UrlPathHelper versionUrlPathHelper;
	@Autowired
	private IUserService userService;
	@Autowired
	private ServerProperties serverProperties;

	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {

		return new MappingJackson2HttpMessageConverter() {
			//重写writeInternal方法，在返回内容前首先进行加密
			@Override
			protected void writeInternal(Object object, Type type, HttpOutputMessage outputMessage) throws IOException,
				HttpMessageNotWritableException {

				RequestAttributes ra = RequestContextHolder.getRequestAttributes();
				ServletRequestAttributes sra = (ServletRequestAttributes) ra;
				HttpServletRequest request = sra.getRequest();
				if (request.getServletPath().startsWith("/actuator") || request.getServletPath().startsWith("/error")) {
					super.writeInternal(object, type, outputMessage);
					return;
				}

				int authStatus = 3;
				MessageInfo response = null;
				if (object instanceof MessageInfo) {
					response = (MessageInfo) object;
					JSONObject requestInfo = new JSONObject();
					requestInfo.put("requestTime", request.getAttribute("startTime"));
					requestInfo.put("responseTime", System.currentTimeMillis());
					requestInfo.put("interfaceMd5", request.getHeader("interfaceMd5"));
					long uid = SecurityUtil.getUidByToken(response.getToken());
					UserInfo user = userService.getUserInfo(uid, null, null);
					if (user != null) {
						authStatus = user.getAuthStatus();
					}
					response.setAuthStatus(authStatus);
					response.setRequestInfo(requestInfo);

				}

				String json = GsonUtil.obj2Json(object);

				//printLog(request, object);

				if (checkEncrypt(json)) {
					String data = AesUtil.encrypt(json.trim());
					if (StringUtils.isNotBlank(data)) {
						data = data.replaceAll("[\r\n]", "");
					}
//        	System.out.println("[加密]"+data);
					super.writeInternal(data, type, outputMessage);
				} else {
					super.writeInternal(object, type, outputMessage);
				}
			}
		};
	}

	private boolean checkEncrypt(String json) {
		try {
			JSONObject data = JSONObject.fromObject(json);
			if (data.containsKey("enabled") && data.getInt("enabled") == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	protected int getUpdateCacheFlag(String pageConfigVersion, String currentVersion) {
		return VersionUtil.getVersionValue(pageConfigVersion) < VersionUtil.getVersionValue(currentVersion) ? 1 : 0;
	}

	public static byte[] doCompress(byte[] in) throws IOException {
		GZIPOutputStream gzip = null;
		ByteArrayOutputStream out = null;
		try {
			out = new ByteArrayOutputStream(in.length);
			gzip = new GZIPOutputStream(out);
			gzip.write(in, 0, in.length);
//        	gzip.flush();
			gzip.finish();
			return out.toByteArray();
		} catch (Exception e) {
			throw new RuntimeException("压缩失败", e);
		} finally {
			if (out != null) {
				out.close();
			}
			if (gzip != null) {
				gzip.close();
			}
		}
	}

	public static byte[] uncompress(byte[] bytes) throws IOException {
		if (bytes == null || bytes.length == 0) {
			return null;
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		GZIPInputStream ungzip = null;
		try {
			ungzip = new GZIPInputStream(in);
			byte[] buffer = new byte[1024];
			int n;
			while ((n = ungzip.read(buffer)) >= 0) {
				out.write(buffer, 0, n);
			}
			return out.toByteArray();
		} catch (IOException e) {
			throw new RuntimeException("解压失败", e);
		} finally {
			if (out != null) {
				out.close();
			}
			if (in != null) {
				in.close();
			}
			if (ungzip != null) {
				ungzip.close();
			}
		}

	}

	/*******************添加拦截器***************/
	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		String[] executeInclude = new String[mvc.getExecuteInclude().size()];
		executeInclude = mvc.getExecuteInclude().toArray(executeInclude);
		String[] executeExclude = new String[mvc.getExecuteExclude().size()];
		executeExclude = mvc.getExecuteExclude().toArray(executeExclude);
		registry.addInterceptor(new ExecuteHandlerInterceptor()).addPathPatterns(executeInclude).excludePathPatterns(executeExclude);

		String[] verifyInclude = new String[mvc.getVerifyInclude().size()];
		verifyInclude = mvc.getVerifyInclude().toArray(verifyInclude);
		String[] verifyExclude = new String[mvc.getVerifyExclude().size()];
		verifyExclude = mvc.getVerifyExclude().toArray(verifyExclude);
		registry.addInterceptor(verifyHandlerInterceptor).addPathPatterns(verifyInclude).excludePathPatterns(verifyExclude);

		registry.addInterceptor(threadLocalInterceptor).addPathPatterns(verifyInclude);
	}

	/**
	 * @return
	 * @throws IOException
	 * @Title: multipartResolver、
	 * @Description:文件上传限定
	 * @return: CommonsMultipartResolver
	 */
	@Bean
	public CommonsMultipartResolver multipartResolver() throws IOException {
		CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
		commonsMultipartResolver.setMaxUploadSize(mvc.getMaxUploadSize());
		return commonsMultipartResolver;
	}

	@Override
	public void configureMessageConverters(
		List<HttpMessageConverter<?>> converters) {
		List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();

		supportedMediaTypes.add(MediaType.TEXT_HTML);
//	    supportedMediaTypes.add(new MediaType("text", "plain",Charset.forName("UTF-8")));
		supportedMediaTypes.add(new MediaType("application", "json", Charset.forName("UTF-8")));
		supportedMediaTypes.add(new MediaType("application", "vnd.spring-boot.actuator.v2+json", Charset.forName("UTF-8")));
		ObjectMapper customObjectMapper = new ObjectMapper();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		customObjectMapper.setDateFormat(dateFormat);

		MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = mappingJackson2HttpMessageConverter();
		mappingJackson2HttpMessageConverter.setSupportedMediaTypes(supportedMediaTypes);
		mappingJackson2HttpMessageConverter.setObjectMapper(customObjectMapper);
		converters.add(mappingJackson2HttpMessageConverter);

		StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
		List<MediaType> supportedMediaTypes2 = new ArrayList<MediaType>();

		supportedMediaTypes2.add(MediaType.TEXT_HTML);
		supportedMediaTypes2.add(new MediaType("text", "plain", Charset.forName("UTF-8")));
		stringHttpMessageConverter.setSupportedMediaTypes(supportedMediaTypes2);
		converters.add(stringHttpMessageConverter);
//		mappingJackson2HttpMessageConverter

//		super.configureMessageConverters(converters);
	}

	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		configurer.setUrlPathHelper(versionUrlPathHelper);
//		super.configurePathMatch(configurer);
	}

	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setScheduler(setTaskExecutors());
	}

	@Bean(destroyMethod = "shutdown")
	public Executor setTaskExecutors() {
		return Executors.newScheduledThreadPool(3); // 3个线程来处理。
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
			//是否发送Cookie
			.allowCredentials(true)
			//放行哪些原始域
			.allowedOrigins("*")
			.allowedMethods(new String[]{"GET", "POST", "PUT", "DELETE"});
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		//配置静态资源处理
		registry.addResourceHandler(serverProperties.getStaticPathPattern())
			.addResourceLocations(serverProperties.getStaticLocations());
	}
}
