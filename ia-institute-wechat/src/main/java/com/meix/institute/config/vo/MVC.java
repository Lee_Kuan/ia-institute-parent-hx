package com.meix.institute.config.vo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:mvc配置文件
 */
@Component
@ConfigurationProperties(prefix = "mvc")
public class MVC {

	private List<String> verifyInclude;
	private List<String> verifyExclude;
	private List<String> executeInclude;
	private List<String> executeExclude;
	
	/**
	 * 上传文件大小
	 */
	private long maxUploadSize;
	/**
	 * 上传文件目录
	 */
	private String uploadTempDir;

	public List<String> getExecuteInclude() {
        return executeInclude;
    }

    public void setExecuteInclude(List<String> executeInclude) {
        this.executeInclude = executeInclude;
    }

    public List<String> getExecuteExclude() {
        return executeExclude;
    }

    public void setExecuteExclude(List<String> executeExclude) {
        this.executeExclude = executeExclude;
    }

    public long getMaxUploadSize() {
		return maxUploadSize;
	}

	public void setMaxUploadSize(long maxUploadSize) {
		this.maxUploadSize = maxUploadSize;
	}

	public String getUploadTempDir() {
		return uploadTempDir;
	}

	public void setUploadTempDir(String uploadTempDir) {
		this.uploadTempDir = uploadTempDir;
	}

	public List<String> getVerifyInclude() {
		return verifyInclude;
	}

	public void setVerifyInclude(List<String> verifyInclude) {
		this.verifyInclude = verifyInclude;
	}

	public List<String> getVerifyExclude() {
		return verifyExclude;
	}

	public void setVerifyExclude(List<String> verifyExclude) {
		this.verifyExclude = verifyExclude;
	}

}
