package com.meix.institute.config.vo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class ServerProperties {

	@Value("${static-path-pattern}")
	private String staticPathPattern;

	@Value("${static-locations}")
	private String staticLocations;

}
