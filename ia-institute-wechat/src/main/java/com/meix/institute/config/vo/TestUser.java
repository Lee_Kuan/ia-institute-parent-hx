package com.meix.institute.config.vo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:mvc配置文件
 */
@Component
@ConfigurationProperties(prefix = "meix")
public class TestUser {

	private List<String> innerTestUsers;

    public List<String> getInnerTestUsers() {
        return innerTestUsers;
    }
    public void setInnerTestUsers(List<String> innerTestUsers) {
        this.innerTestUsers = innerTestUsers;
    }
}
