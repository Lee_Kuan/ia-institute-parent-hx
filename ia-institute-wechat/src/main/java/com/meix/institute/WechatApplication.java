package com.meix.institute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by zenghao on 2019/7/24.
 */
@SpringBootApplication
//@MapperScan("com.meix.institute.mapper")
public class WechatApplication {
	public static void main(String[] args) {
		SpringApplication.run(WechatApplication.class, args);
	}
}
