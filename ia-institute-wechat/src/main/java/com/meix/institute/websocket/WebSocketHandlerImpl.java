package com.meix.institute.websocket;

import com.meix.institute.util.Des;
import com.meix.institute.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

@Service
public class WebSocketHandlerImpl implements WebSocketHandler {
	private static final Logger log = LoggerFactory.getLogger(WebSocketHandlerImpl.class);

	private static final Map<String, WebSocketSession> userMap = new HashMap<>();
	private static final Map<String, String> encryptKeyMap = new HashMap<>();

	@Override
	public void afterConnectionEstablished(WebSocketSession session)
		throws Exception {
		Long uid = (Long) session.getAttributes().get("uid");
		Long activityId = (Long) session.getAttributes().get("activityId");
		Integer clientType = (Integer) session.getAttributes().get("clientType");
		if (uid != null && activityId != null && clientType != null) {
			String key = uid + "_" + clientType + "_" + activityId;
			userMap.put(key, session);
			encryptKeyMap.put(uid + "_" + clientType, (String) session.getAttributes().get("encryptKey"));
			log.info("链接成功......uid:{}，{}", uid, key);
		}
	}


	@Override
	public void handleMessage(WebSocketSession session,
	                          WebSocketMessage<?> message) throws Exception {
		String strPayload = message.getPayload().toString();
		log.debug(session.getAttributes().get("uid") + strPayload + "|" + session.isOpen() + "|" + session.getId());
		log.info("receive:{},{}", getSessionKey(session), strPayload);
		if (strPayload.equalsIgnoreCase("ping")) {
			session.sendMessage(new TextMessage("pong"));

			//session在服务端异常并关闭时，isOpen=false，但是当客户端继续通过此session ping的时候，此session则又变为了正常连接状态，isOpen=true，
			//由于在服务端异常时会清除掉session信息，所以在此时再记录下来
			if (session.isOpen()) {
				Long uid = (Long) session.getAttributes().get("uid");
				Long activityId = (Long) session.getAttributes().get("activityId");
				Integer clientType = (Integer) session.getAttributes().get("clientType");
				if (uid != null && activityId != null && clientType != null) {
					String key = uid + "_" + clientType + "_" + activityId;
					if (!userMap.containsKey(key)) {
						userMap.put(key, session);
						log.info("重新记录session成功......uid:{}", uid);
					}
					if (!encryptKeyMap.containsKey(uid + "_" + clientType)) {
						encryptKeyMap.put(uid + "_" + clientType, (String) session.getAttributes().get("encryptKey"));
					}
				}
			}
		}
	}

	@Override
	public void handleTransportError(WebSocketSession session,
	                                 Throwable exception) throws Exception {
		log.error("{}", exception);
		if (session.isOpen()) {
			session.close();
		}
		log.info("断开：{}", getSessionKey(session));
		log.info("链接出错，关闭链接......");
		userMap.remove(getSessionKey(session));
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session,
	                                  CloseStatus closeStatus) throws Exception {
		log.info("链接关闭......" + closeStatus.toString());
		userMap.remove(getSessionKey(session));
	}

	@Override
	public boolean supportsPartialMessages() {
		return false;
	}

	/**
	 * 获取用户标识
	 *
	 * @param session
	 * @return
	 */
	private String getSessionKey(WebSocketSession session) {
		try {
			Long uid = (Long) session.getAttributes().get("uid");
			Long activityId = (Long) session.getAttributes().get("activityId");
			Integer clientType = (Integer) session.getAttributes().get("clientType");
			return uid + "_" + clientType + "_" + activityId;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 发送文本信息给指定用户
	 *
	 * @param receiverId
	 * @param clientType
	 * @param activityId
	 * @param message
	 * @return
	 */
	public boolean sendMessageToUser(long receiverId, int clientType, long activityId, String message) {
		try {
			WebSocketSession session = userMap.get(receiverId + "_" + clientType + "_" + activityId);
			if (session == null) {
				return false;
			}

			if (!session.isOpen()) {
				return false;
			}

			log.info("sendMessageToUser: activityId = {}, receiverId = {}, clientType = {}", activityId, receiverId, clientType);
//			if (clientType == MeixConstants.CLIENT_TYPE_MOBLE) {
//				byte[] out = MVCConfig.doCompress(message.getBytes());
//				out = Des.encrypt(out, encryptKeyMap.get(receiverId + "_" + clientType), 2);
//				session.sendMessage(new BinaryMessage(out));
//			} else {
			session.sendMessage(new TextMessage(Des.encryptBASE64(StringUtils.encodeUrl(message, "utf-8"))));
//			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 发送信息给指定活动
	 *
	 * @param uid
	 * @param activityId
	 * @param message
	 * @return
	 */
	public void sendMessageToActivity(long uid, long activityId, String message) {
		for (Entry<String, WebSocketSession> entry : userMap.entrySet()) {
			String key = entry.getKey();
			int activityBeginIndex = key.lastIndexOf("_");
			int clientTypeBeginIndex = key.indexOf("_");
			long curActivityId = Long.parseLong(key.substring(activityBeginIndex + 1));
			long receiverId = Long.parseLong(key.substring(0, clientTypeBeginIndex));
			int clientType = Integer.parseInt(key.substring(clientTypeBeginIndex + 1, activityBeginIndex));
			log.info("sendMessageToActivity: activityId = {}, receiverId = {}, clientType = {}", curActivityId, receiverId, clientType);
			if (uid == receiverId) {
				continue;
			}
			if (curActivityId == activityId) {
				sendMessageToUser(receiverId, clientType, activityId, message);
			}
		}
	}
}
