package com.meix.institute.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * Created by zenghao on 2020/4/16.
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

	@Autowired
	private WebSocketIntercepter webSocketInterceptor;

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(new WebSocketHandlerImpl(), "/sock/webSocketServer").addInterceptors(webSocketInterceptor).setAllowedOrigins("*");
		registry.addHandler(new WebSocketHandlerImpl(), "/sockjs/webSocketServer").addInterceptors(webSocketInterceptor).withSockJS();
	}
}
