package com.meix.institute.websocket;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import com.meix.institute.api.IInsLoginService;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.SecurityUtil;
import com.meix.institute.vo.SecToken;

import net.sf.json.JSONObject;

@Component
public class WebSocketIntercepter implements HandshakeInterceptor {
	private Logger log = LoggerFactory.getLogger(WebSocketIntercepter.class);
	@Autowired
	private IInsLoginService insLoginService;

	@Override
	public boolean beforeHandshake(ServerHttpRequest request,
	                               ServerHttpResponse response, WebSocketHandler wsHandler,
	                               Map<String, Object> attributes) throws Exception {
		if (request instanceof ServletServerHttpRequest) {
			ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
			ServletServerHttpResponse servletResponse = (ServletServerHttpResponse) response;
			String clientstr = servletRequest.getServletRequest().getParameter("clientstr");
			if (!StringUtils.isEmpty(clientstr)) {
				attributes.put("clientstr", clientstr);
				return doVerification(servletRequest, servletResponse.getServletResponse(), attributes);
			} else {
				return false;
			}
		}
		return true;
	}

	@Override
	public void afterHandshake(ServerHttpRequest request,
	                           ServerHttpResponse response, WebSocketHandler wsHandler,
	                           Exception exception) {
	}

	private boolean doVerification(ServletServerHttpRequest httpRequest,
	                               ServletResponse response,
	                               Map<String, Object> attributes) throws ServletException, IOException {
		HttpServletRequest request = httpRequest.getServletRequest();
		String clientstr = request.getParameter("clientstr");
		String requestURI = request.getRequestURI();
//		String requestIP = RequestUtil.getIpAddr(request);
		if (clientstr == null) {
			//被拦截，重定向到login界面
			log.info("参数异常,参数为Null！---请求接口：" + requestURI + "---clientstr=" + clientstr);
			request.getRequestDispatcher("/login/tokenParmsError.do").forward(request, response);
			return false;
		}
		JSONObject jsonobject = null;
		try {
			jsonobject = JSONObject.fromObject(clientstr);
		} catch (Exception e) {
			throw new RuntimeException("clientstr异常:" + clientstr);
		}
		int clientType = 0;
		if (jsonobject.containsKey("clientType")) {	
			clientType = jsonobject.getInt("clientType");
		}
		attributes.put("clientType", clientType);
		//验证TOKEN是否有效
		String token ="";
		if (jsonobject.containsKey("token")) {
			token = jsonobject.getString("token");
		}
		request.setAttribute("clientstr", jsonobject);
		String interfaceUrl = request.getServletPath();
		long uid = MobileassistantUtil.getUserId(token);
		attributes.put("uid", uid);
		if (jsonobject.containsKey("activityId")) {
			attributes.put("activityId", jsonobject.getLong("activityId"));
		}
		try {
			String user = SecurityUtil.getUserByToken(token);
			SecToken secToken = insLoginService.getTokenByUser(user, clientType);
			String dbtoken = secToken == null ? null : secToken.getToken();
			if (!org.apache.commons.lang.StringUtils.equals(token, dbtoken)) {
				log.info("token验证失败！uid[" + uid + "],interfaceUrl:" + interfaceUrl);
				request.getRequestDispatcher("/login/tokenInvalid.do?token=" + token).forward(request, response);
				return false;
			}
		} catch (Exception e) {
			log.error("Token字符不合法！" + e.getLocalizedMessage());
			request.getRequestDispatcher("/login/tokenInvalid.do").forward(request, response);
			return false;
		}
		String deviceId = "weblogin";
		attributes.put("encryptKey", deviceId.substring(0, 8));
		return true;
	}

}
