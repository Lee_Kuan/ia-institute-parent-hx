package com.meix.institute.message;

import com.meix.institute.util.VersionUtil;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class MessageInfo implements Serializable {
	private static final long serialVersionUID = -8603049414455165095L;
	private String loginstate;// 登录状态
	private List<?> data = Collections.emptyList();// 服务端返回数据
	private Object object;// 存储单个对象
	private String token;// token字符串
	private String code;// 验证码
	private String resultStatus = "success";// 返回状态码
	private int messageCode;// 错误代码;
	private String devMessage = "";// 开发人员消息
	private String message = "";// 客户端显示消息
	private int dataCount = 0;// list集合总记录数
	private String key = null;
	private boolean zip = false;
	private JSONObject requestInfo;//页面请求信息
	private int authStatus;
	/**
	 * 配置数据是否加密处理：1是，0否
	 */
	private int enabled = 1;//默认启动数据加密

	public MessageInfo(HttpServletRequest request) {

		//如果请求头里面包含zip字段就不对返回结果进行压缩
		String zipFlag = request.getHeader("zip");
		if (zipFlag != null) {
			zip = false;
		}
		try {
			enabled = Integer.parseInt(request.getAttribute("enabled").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		String clientstr = (String) request.getAttribute("clientstr");
		if (StringUtils.isEmpty(clientstr)) {
			clientstr = request.getParameter("clientstr");
		}
		if (clientstr != null) {
			JSONObject jsonobject = null;
			try {
				jsonobject = JSONObject.fromObject(clientstr);
				//验证token是否有效
				token = MapUtils.getString(jsonobject, "token");
			} catch (Exception e) {
			}
		}
	}

	public MessageInfo() {

	}

	public MessageInfo(String appVersion) {
		long version = VersionUtil.getVersionValue(appVersion);
		if (version >= VersionUtil.v2_1_0) {
			zip = true;
		}
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLoginstate() {
		return loginstate;
	}

	public void setLoginstate(String loginstate) {
		this.loginstate = loginstate;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(String resultStatus) {
		this.resultStatus = resultStatus;
	}

	public String getDevMessage() {
		return devMessage;
	}

	public void setDevMessage(String devMessage) {
		this.devMessage = devMessage;
	}

	public int getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(int messageCode) {
		this.messageCode = messageCode;
	}

	public int getDataCount() {
		return dataCount;
	}

	public void setDataCount(int dataCount) {
		this.dataCount = dataCount;
	}

	public boolean isZip() {
		return zip;
	}

	public void setZip(boolean zip) {
		this.zip = zip;
	}

	public JSONObject getRequestInfo() {
		return requestInfo;
	}

	public void setRequestInfo(JSONObject requestInfo) {
		this.requestInfo = requestInfo;
	}

	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public int getAuthStatus() {
		return authStatus;
	}

	public void setAuthStatus(int authStatus) {
		this.authStatus = authStatus;
	}

}
