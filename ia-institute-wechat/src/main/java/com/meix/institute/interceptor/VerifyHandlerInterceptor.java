package com.meix.institute.interceptor;

import com.meix.institute.api.IInsLoginService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.GsonUtil;
import com.meix.institute.util.SecurityUtil;
import com.meix.institute.vo.SecToken;

import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @Description:权限拦截器
 */
@Component
public class VerifyHandlerInterceptor implements HandlerInterceptor {

	@Autowired
	private IInsLoginService insLoginService;

	private Logger log = LoggerFactory.getLogger(VerifyHandlerInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request,
	                         HttpServletResponse response, Object handler) throws Exception {

		String clientstr = (String) request.getAttribute("clientstr");
		String clientType = request.getHeader(MeixConstants.CLIENT_TYPE_HEAD);
		
		if (StringUtils.isEmpty(clientstr)) {
			clientstr = request.getParameter("clientstr");
		}
		if (clientstr == null) {
			return false;
		}
		JSONObject jsonobject = null;
		try {
			jsonobject = JSONObject.fromObject(clientstr);
		} catch (Exception e) {
			log.warn("clientstr： json格式化异常【{}】", e);
			return false;
		}
		if (StringUtils.isEmpty(clientType)) { //从clientstr中读取clientType
			clientType = MapUtils.getString(jsonobject, "clientType", "");
		}
		if (StringUtils.isEmpty(clientType)) {
			sendResponse(response, MeixConstantCode.M_1009, "缺少clientType验证信息");
			return false;
		}
		//验证token是否有效
		String token = MapUtils.getString(jsonobject, "token");
		log.info("{},{}", clientstr, token);
		String tempToken = MapUtils.getString(jsonobject, "tempToken");
		if (StringUtils.isNotEmpty(tempToken) && request.getServletPath().contains("uploadFile")) {
			return true;
		}
		if (StringUtils.isBlank(token)) {
			sendResponse(response, MeixConstantCode.M_1012, "token令牌为空");
			return false;
		}
		String user = SecurityUtil.getUserByToken(token);
		SecToken secToken = insLoginService.getTokenByUser(user, Integer.valueOf(clientType));
		String tokenDb = secToken == null ? null : secToken.getToken();
		if (!StringUtils.equals(token, tokenDb)) {
			sendResponse(response, MeixConstantCode.M_1010, "token认证失败");
			return false;
		}
		return true;
	}

	private void sendResponse(HttpServletResponse response, int messageCode, String message) {
		response.setContentType("application/json; charset=utf-8");
		MessageInfo info = new MessageInfo();
		info.setMessage(message);
		info.setMessageCode(messageCode);
		try {
			PrintWriter writer = response.getWriter();
			writer.write(GsonUtil.obj2Json(info));
			writer.flush();
			writer.close();
		} catch (IOException e) {
			log.warn("认证返回异常：{}", e);
		}
	}

}
