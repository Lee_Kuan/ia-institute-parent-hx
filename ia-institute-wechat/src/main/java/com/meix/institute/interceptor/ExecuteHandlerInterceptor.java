package com.meix.institute.interceptor;

import com.meix.institute.util.AesUtil;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Enumeration;

/**
 * 统计接口执行时长，请求数据解密
 * 解密请求参数：clientstr|oldUrlFix
 */
public class ExecuteHandlerInterceptor implements HandlerInterceptor {

	private static Logger log = LoggerFactory.getLogger(ExecuteHandlerInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		request.setAttribute("requestStartTime", System.currentTimeMillis());
		int enabled = 0;
		if (StringUtils.isEmpty(request.getAttribute("enabled"))) {
			if (!StringUtils.isEmpty(request.getParameter("enabled"))) {
				enabled = Integer.parseInt(request.getParameter("enabled"));
			}
		} else {
			enabled = Integer.parseInt(request.getAttribute("enabled").toString());
		}
		request.setAttribute("enabled", enabled);
		if (enabled > 0) {
			try {
				// pc 请求参数 clientstr
				String clientstr = StringUtils.isEmpty(request.getAttribute("clientstr")) ? null : (String) request.getAttribute("clientstr");
				if (StringUtils.isEmpty(clientstr)) {
					clientstr = request.getParameter("clientstr");
				}
				if (!StringUtils.isEmpty(clientstr)) {
					clientstr = clientstr.replaceAll("\"", "");
					request.setAttribute("clientstr", AesUtil.decrypt(clientstr));
				}
			} catch (Exception e) {
			}
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		String temp = request.getAttribute("requestStartTime").toString();
		long startTime = 0L;
		long executeTime = 0L;
		if (!StringUtils.isEmpty(temp)) {
			try {
				startTime = Long.parseLong(temp);
				executeTime = System.currentTimeMillis() - startTime;
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		String uri = (String) request.getAttribute("oldUrlFix");
		if (StringUtils.isEmpty(uri)) {
			uri = request.getParameter("oldUrlFix");
		}
		if (StringUtils.isEmpty(uri)) {
			uri = request.getRequestURI();
		}

		String clientstr = (String) request.getAttribute("clientstr");
		if (StringUtils.isEmpty(clientstr)) clientstr = request.getParameter("clientstr");
		if ("/iaweb/forword/all".equals(uri) && StringUtils.isEmpty(clientstr)) {

		} else {
			log.info("[" + uri + "] [executeTime:" + executeTime + "ms]" + " [clientstr:" + clientstr + "]");
		}

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		Enumeration<String> headers = request.getHeaderNames();
		JSONObject headerJonsObj = new JSONObject();
		while (headers.hasMoreElements()) {
			String tempHeaderName = headers.nextElement();
			headerJonsObj.put(tempHeaderName, request.getHeader(tempHeaderName));
		}
		log.debug("【" + request.getServletPath() + "】的header:{}", headerJonsObj.toString());
	}

}
