package com.meix.institute.interceptor;

import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.message.MessageInfo;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @Description:全局异常处理
 */
@Component
public class MeishiHandlerExceptionResolver implements HandlerExceptionResolver {
	private Logger log = LoggerFactory.getLogger(MeishiHandlerExceptionResolver.class);

	@Override
	public ModelAndView resolveException(HttpServletRequest request,
	                                     HttpServletResponse response, Object obj, Exception exception) {
		if (exception == null || exception.getMessage() == null) {
			log.warn("Exception message is null!");
			return null;
		}
		//clientAbortException
		if (exception.getMessage().contains("java.io.IOException: Broken pipe")) {
			return null;
		}
		log.error("", exception);
		try {
			PrintWriter out = response.getWriter();
			MessageInfo info = new MessageInfo(request);
			info.setMessageCode(MeixConstantCode.M_1009);
			out.print(JSONObject.fromObject(info).toString());
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
