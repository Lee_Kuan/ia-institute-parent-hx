package com.meix.institute.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meix.institute.api.IDictTagService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.core.Item;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.MobileassistantUtil;

/**
 * 枚举接口
 * @author Xueyc
 *
 */
@Controller
@RequestMapping("/sys")
public class CodeController {
    
    @Autowired
    private IDictTagService dictTagService;
    
    private static Logger log = LoggerFactory.getLogger(CodeController.class);

    @RequestMapping(value = "/getCodes")
    @ResponseBody
    public MessageInfo getCodes(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject object = null;
        try {
            object = MobileassistantUtil.getRequestParams(request);
            String name = MapUtils.getString(object, "name");
            
            List<Item> findItem = dictTagService.findItem(name);
            
            info.setData(findItem);
            info.setMessage("操作成功");
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            log.warn("getCode异常：", e);
            info.setMessage("操作失败");
            info.setMessageCode(MeixConstantCode.M_1009);
        }
        return info;
    }
    
    /**
     * 枚举通用接口（批量）
     * @param request
     * @return
     */
    @RequestMapping(value = "/getBatchCodes")
    @ResponseBody
    public MessageInfo getBatchCodes(HttpServletRequest request) {
        MessageInfo info = new MessageInfo(request);
        JSONObject object = null;
        try {
            object = MobileassistantUtil.getRequestParams(request);
            String names = MapUtils.getString(object, "names");
            String[] split = StringUtils.split(names, ",");
            Map<String, Object> resp = null;
            if (split != null && split.length > 0) {
                resp = new HashMap<String, Object>();
                for (String name : split) {
                    List<Item> list = dictTagService.findItem(name);
                    resp.put(name, list);
                }
            }
            info.setObject(resp);
            info.setMessage("操作成功");
            info.setMessageCode(MeixConstantCode.M_1008);
        } catch (Exception e) {
            log.warn("getBatchCodes异常：", e);
            info.setMessage("操作失败");
            info.setMessageCode(MeixConstantCode.M_1009);
        }
        return info;
    }
}
