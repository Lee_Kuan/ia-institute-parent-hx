package com.meix.institute.controller;

import com.meix.institute.BaseService;
import com.meix.institute.api.IMeetingService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.vo.meeting.MorningMeetingVo;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @describe 晨会接口
 */
@Controller
@RequestMapping("meeting")
public class MeetingController extends BaseService {
	private static final Logger log = LoggerFactory.getLogger(MeetingController.class);

	@Autowired
	private IMeetingService meetingServiceImpl;

	/**
	 * @Description: 晨会列表
	 */
	@RequestMapping(value = "getMeetingList", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getMeetingList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {

			obj = MobileassistantUtil.getRequestParams(request);

			String clientTypeStr = request.getHeader("clientType");
			int clientType = org.apache.commons.lang.StringUtils.isBlank(clientTypeStr) ? 1 : Integer.valueOf(clientTypeStr);
			int issueType = MeixConstants.getIssuetypeByClientType(clientType);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			long companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int meetingType = MapUtils.getIntValue(obj, "meetingType", -1);

			List<MorningMeetingVo> list = meetingServiceImpl.getMeetingList(uid, companyCode, showNum, currentPage, issueType, meetingType);
			for (MorningMeetingVo vo : list) {
				vo.setPublishDateText(DateUtil.formatReprotPublishDate(vo.getPublishDate()));
			}
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "获取晨会列表失败";
			log.error(devMessage, e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage(devMessage);
		}

		return info;
	}

	/**
	 * @Description: 晨会详情
	 */
	@RequestMapping(value = "getMeetingDetail", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getMeetingDetail(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long id = obj.getLong("id");
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String openId = MapUtils.getString(obj, "openId", "");

			MorningMeetingVo meeting = meetingServiceImpl.getMeetingDetail(id, uid);
			if (meeting != null) {
				saveUserReadRecordState(uid, id, MeixConstants.USER_CH, openId);
			}

			info.setObject(meeting);
			info.setMessageCode(MeixConstantCode.M_1008);

		} catch (Exception e) {
			String devMessage = "获取晨会详情失败";
			log.error(devMessage, e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage(devMessage);
		}

		return info;
	}
}
