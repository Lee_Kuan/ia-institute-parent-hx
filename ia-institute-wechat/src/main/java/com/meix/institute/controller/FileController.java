package com.meix.institute.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.meix.institute.api.MimeService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.Filepath;
import com.meix.institute.core.Mime;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.XssShiedUtil;

import net.sf.json.JSONObject;

/**
 * Created by zenghao on 2019/9/17.
 */
@RequestMapping(value = "/file/")
@Controller
public class FileController {
	
    @Autowired
    private MimeService mimeService;//上传下载相关
    @Autowired
	private CommonServerProperties serverProperties;
    
	private static final Logger log = LoggerFactory.getLogger(FileController.class);
	
	/**
	 * 上传文件，文件路径uploadFileDir/{date}/{filename}
	 *
	 * @param file
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "uploadFile", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo uploadFile(@RequestParam(value = "file", required = true) MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		 
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String user = MapUtils.getString(obj, "uuid");
			String fileName = file.getOriginalFilename();
			String contentType = file.getContentType();
			InputStream is = file.getInputStream();
            Mime mime = new Mime();
            mime.setFileName(fileName);
            mime.setContentType(contentType);
            
            Filepath filepath = mimeService.write(mime, is, user);
            
			Map<String, Object> result = new HashMap<>();
			result.put("fileName", filepath.getUuid());
            result.put("url", filepath.getPath()); // 外部访问路径
            info.setObject(result);
			info.setMessageCode(MeixConstantCode.M_1008);
			info.setMessage("文件上传成功");
		} catch (Exception e) {
			log.warn("uploadFile异常：", e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage("文件上传失败");
			e.printStackTrace();
		}
		return info;
	}
	
	/**
	 * 获取远程文件
	 * @param request
	 * @param response
	 */
	@RequestMapping("show/remoteFile")
    public void getRemoteFileStream(HttpServletRequest request, HttpServletResponse response) {
        InputStream is = null;
        try {
            String fileUrl = request.getParameter("fileUrl");
            if (StringUtils.isBlank(fileUrl)) {
                return;
            }
            byte[] decode = Base64.getDecoder().decode(fileUrl.getBytes());
            String filename = new String(decode);
            log.info("远程服务器文件地址：【{}】", filename);
            URL url = new URL(filename);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // 设置连接超时时间
            conn.setConnectTimeout(3000);
            if (conn.getResponseCode() == 200) {
                is = conn.getInputStream();
                ServletOutputStream os = response.getOutputStream();
                IOUtils.copy(is, os);
                os.flush();
                os.close();
            }
        } catch (Exception e) {
            log.warn("获取远程文件失败", e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	
	@GetMapping("show/{date}/{uid}/{filename:.+}")
    public void show(HttpServletResponse response, @PathVariable("date") String date, @PathVariable("uid") String uid, @PathVariable("filename") String filename) {
        try {
            date = XssShiedUtil.stripXss(date);
            uid = XssShiedUtil.stripXss(uid);
            filename = XssShiedUtil.stripXss(filename);

            String uploadFileDir = serverProperties.getFileDir();
            if (uploadFileDir != null) {
            	uploadFileDir = uploadFileDir.replaceAll("upload", "");
            }
            uploadFileDir = uploadFileDir + MeixConstants.fileSeparate + date
                + MeixConstants.fileSeparate + uid;
            String path = uploadFileDir + MeixConstants.fileSeparate + filename;
            File file = new File(path);
            if (!file.exists()) {
                log.warn("不存在的文件：{}", path);
                return;
            }
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(Files.readAllBytes(Paths.get(uploadFileDir).resolve(filename)));
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            log.error("", e);
        }
    }
}
