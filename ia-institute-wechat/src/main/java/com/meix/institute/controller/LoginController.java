package com.meix.institute.controller;

import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.meix.institute.BaseService;
import com.meix.institute.api.*;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.config.SmsMsgConfig;
import com.meix.institute.config.vo.TestUser;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.pojo.InsUserInfo;
import com.meix.institute.service.SmsService;
import com.meix.institute.util.*;
import com.meix.institute.vo.IA_PWDMapping;
import com.meix.institute.vo.SecToken;
import com.meix.institute.vo.commen.InsVerificationCodeVo;
import com.meix.institute.vo.user.UserInfo;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class LoginController extends BaseService {
	private static Logger log = LoggerFactory.getLogger(LoginController.class);
	@Autowired
	private IUserService userServiceImpl;
	@Autowired
	private IInsLoginService insLoginService;
	@Autowired
	private IInsUserService insUserService;
	@Autowired
	private SmsMsgConfig smsMsgConfig;
	@Autowired
	private CommonServerProperties commonServerProperties;
	@Autowired
	private IVerificationCodeService verificationCodeService;
	@Autowired
	private IUserLoginRecordService userLoginRecordService;
	@Autowired
	private SmsService smsService;
	@Autowired
	private TestUser testUser;

	@Value("${ins.defaultPassword}")
	private String defaultPassword;

	/**
	 * 获取验证码图片
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = {"/user/valifyCodeImage"}, method = RequestMethod.GET)
	public MessageInfo valifyCodeImage(HttpServletRequest request, HttpServletResponse response,
	                                   @RequestParam(required = false) String sessionId) {
		MessageInfo info = new MessageInfo(request);
		try {
			if (StringUtil.isBlank(sessionId)) {
				info.setMessage("缺少sessionId参数！");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			response.setDateHeader("Expires", 0);
			response.setContentType("image/jpeg");
			//生成随机字串
			String verifyCode = VerifyCodeUtil.generateVerifyCode(4);
			log.info("create sessionId:{},varifyCode:{}", sessionId, verifyCode.toLowerCase());
			InsVerificationCodeVo vo = new InsVerificationCodeVo();
			vo.setCode(verifyCode);
			vo.setJid(sessionId);
			vo.setType(MeixConstants.VERCODE_KEY_WX);
			vo.setDate(DateUtil.stringToDate(DateUtil.dateToStr(new Date(), DateUtil.dateShortFormat)));
			verificationCodeService.save(vo);
			//生成图片
			int w = 200, h = 80;
			VerifyCodeUtil.outputImage(w, h, response.getOutputStream(), verifyCode);
			return null;
		} catch (Exception e) {
			log.warn("获取验证码异常：", e);
			info.setMessage("获取图片验证码失败！");
			info.setMessageCode(MeixConstantCode.M_1009);
			return info;
		}
	}

	/**
	 * 验证输入的验证码
	 *
	 * @param request
	 */
	@RequestMapping(value = {"/user/checkValifyCode"}, method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo checkValifyCode(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject jsonobject = null;
		try {
			jsonobject = MobileassistantUtil.getRequestParams(request);

			String verCode = MapUtils.getString(jsonobject, MeixConstants.VER_CODE);
			if (StringUtils.isBlank(verCode)) {
				info.setMessageCode(MeixConstantCode.M_1098);
				info.setMessage("验证码不能为空");
				return info;
			}
			String sessionId = MapUtils.getString(jsonobject, MeixConstants.JSESSIONID);
			if (StringUtils.isBlank(sessionId)) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("请求参数格式不正确");
				return info;
			}
			// 验证验证码
			InsVerificationCodeVo vo = new InsVerificationCodeVo();
			vo.setJid(sessionId);
			vo.setType(MeixConstants.VERCODE_KEY_WX);
			vo.setDate(DateUtil.stringToDate(DateUtil.dateToStr(new Date(), DateUtil.dateShortFormat)));
			vo = verificationCodeService.selectOne(vo);
			if (vo == null || !StringUtils.equalsIgnoreCase(verCode, vo.getCode())) {
				info.setMessageCode(MeixConstantCode.M_1031);
				info.setMessage("验证码不正确");
				return info;
			}
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("验证码发送失败！");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("getVerCode异常：", e);
		}
		return info;
	}

	/**
	 * @param request
	 * @return
	 * @Title: getVerCode、
	 * @Description:获取验证码
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/user/getVerCode", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getVerCode(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject jsonobject = null;
		try {
			jsonobject = MobileassistantUtil.getRequestParams(request);
			String mobile = MapUtils.getString(jsonobject, "mobile");

			if (StringUtil.isBlank(mobile)) {
				info.setMessage("手机号不能为空");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}

			String verCode = MapUtils.getString(jsonobject, MeixConstants.VER_CODE);
			if (StringUtils.isBlank(verCode)) {
				info.setMessageCode(MeixConstantCode.M_1098);
				info.setMessage("验证码不能为空");
				return info;
			}

			// 验证验证码
			String sessionId = MapUtils.getString(jsonobject, MeixConstants.JSESSIONID);
			if (StringUtils.isBlank(sessionId)) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("请求参数格式不正确");
				return info;
			}
			String clientTypeStr = request.getHeader("clientType");
			int clientType = StringUtils.isBlank(clientTypeStr) ? MeixConstants.CLIENT_TYPE_3 : Integer.valueOf(clientTypeStr);
			String clientTypeHint = "";
			if (clientType == MeixConstants.CLIENT_TYPE_2) {
				clientTypeHint = "公众号";
			} else if (clientType == MeixConstants.CLIENT_TYPE_3) {
				clientTypeHint = "小程序";
			}

			// 验证验证码
			InsVerificationCodeVo vo = new InsVerificationCodeVo();
			vo.setJid(sessionId);
			vo.setType(MeixConstants.VERCODE_KEY_WX);
			vo.setDate(DateUtil.stringToDate(DateUtil.dateToStr(new Date(), DateUtil.dateShortFormat)));
			vo = verificationCodeService.selectOne(vo);
			if (vo == null || !StringUtils.equalsIgnoreCase(verCode, vo.getCode())) {
				info.setMessageCode(MeixConstantCode.M_1031);
				info.setMessage("验证码不正确");
				return info;
			}

			IA_PWDMapping bean = new IA_PWDMapping();
			bean.setMobileOrEmail(mobile);
			bean.setLoginType(MeixConstants.LOGIN_TYPE_1);//1 手机登录
			String code = userServiceImpl.saveVerCode(bean);

			log.info(bean.getMobileOrEmail() + ":" + code);

			String content = getSmsContent(smsMsgConfig.getLogin(), "{\"code\":\"" + code + "\",\"clientType\":\"" + clientTypeHint + "\"}");
			String result = smsService.sendSMS(bean.getMobileOrEmail(), content);
			if (result == null) {
				info.setMessage("验证码发送成功！");
				info.setMessageCode(MeixConstantCode.M_1008);
			} else {
				info.setMessage(result);
				info.setMessageCode(MeixConstantCode.M_1009);
			}
		} catch (Exception e) {
			info.setMessage("验证码发送失败！");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("getVerCode异常：", e);
		}
		return info;
	}

	/**
	 * @param request
	 * @Title: login、
	 * @Description:验证码登录
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/user/login", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo login(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject jsonobject = MobileassistantUtil.getRequestParams(request);
		// 参数准备
		String mobile = MapUtils.getString(jsonobject, "mobile");
		String verCode = MapUtils.getString(jsonobject, "pwdOrUrl");
		String appId = MapUtils.getString(jsonobject, "appId", String.valueOf(commonServerProperties.getCompanyCode()));
		Integer clientType = MapUtils.getInteger(jsonobject, MeixConstants.CLIENT_TYPE_HEAD);
		try {
			if (StringUtils.isBlank(mobile)) {
				info.setMessage("手机号必填！");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			if (StringUtils.isBlank(verCode)) {
				info.setMessage("验证码必填！");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			long companyCode = Long.parseLong(StringUtils.isBlank(appId) ? "0" : appId);
			// 验证验证码
			boolean checkVerCode = userServiceImpl.checkVerCode(mobile, MeixConstants.LOGIN_TYPE_1, verCode);

			// 研究所测试账号  2019年7月31日20:21:13  by Xueyc
			if (StringUtils.equals(mobile, "17621502522")
				|| StringUtils.equals(mobile, "13764701594")) { // 白名单用户
				checkVerCode = true;
			}
			if ("123456".equals(verCode) && 
					(commonServerProperties.getEnv().contains(MeixConstants.ACTIVE_DEV) || commonServerProperties.getEnv().contains(MeixConstants.ACTIVE_DEMO))) {
				checkVerCode = true;
			}
			// 测试账号
			List<String> innerTestUsers = testUser.getInnerTestUsers();
			if (innerTestUsers.contains(mobile)) {
				checkVerCode = true;
			}

			if (!checkVerCode) {
				log.warn("验证码登录:【" + mobile + "】:验证码无效");
				info.setMessageCode(MeixConstantCode.M_1031);
				info.setMessage("验证码无效");
				return info;
			}

			UserInfo uinfo = userServiceImpl.getUserInfo(0, null, mobile);
			if (uinfo == null) {
				uinfo = new UserInfo();
				register(uinfo, mobile, companyCode);
			}

			if (uinfo.getIsDelete() == MeixConstants.ACCOUNT_DJ) {
				info.setMessage("账户已冻结！");
				info.setMessageCode(MeixConstantCode.M_1039);
				return info;
			}
			// 验证码参数
			IA_PWDMapping bean = new IA_PWDMapping();
			bean.setMobileOrEmail(mobile);
			bean.setPwdOrUrl(verCode);
			bean.setLoginType(MeixConstants.LOGIN_TYPE_1);

			boolean resetToken = false;
			/******************* 绑定微信 ***************************/
			String bindOpenId = MapUtils.getString(jsonobject, "bindOpenId", null);
			if (StringUtils.isNotBlank(bindOpenId)) {
				try {
					userServiceImpl.bindYJSAccount(bindOpenId, appId, uinfo.getId(), clientType);
					String nickName = MapUtils.getString(jsonobject, "userName", null);
					String headImageUrl = MapUtils.getString(jsonobject, "headImageUrl", null);
					if (StringUtils.isNotBlank(nickName) && !StringUtils.equals("null", nickName) && !StringUtils.equals("\"null\"", nickName)) {
						uinfo.setNickName(nickName);
					}
					if (StringUtils.isNotBlank(headImageUrl) && !StringUtils.equals("null", nickName) && !StringUtils.equals("\"null\"", headImageUrl)) {
						uinfo.setWechatHeadUrl(headImageUrl);
					}
					userServiceImpl.updateUser(uinfo);
					resetToken = true;
				} catch (Exception e) {
					log.warn("login 绑定微信失败：", e);
					resetToken = false;
				}
			}

			// 生成token
			UserInfo returnbean = userServiceImpl.login(bean, uinfo, clientType, resetToken);

			//整理返回值
			Map<String, Object> data = new HashMap<>();
			data.put("mobile", mobile);
			data.put("userName", (StringUtils.isBlank(uinfo.getUserName()) || MeixConstants.DEFAULT_NAME.equals(uinfo.getUserName())) ?
				(StringUtils.isBlank(uinfo.getNickName()) ? uinfo.getUserName() : uinfo.getNickName()) : uinfo.getUserName());
			data.put("id", uinfo.getId());
			String headImageUrl = OssResourceUtil.getHeadImage(uinfo.getHeadImageUrl());
			if (StringUtils.isBlank(headImageUrl)) {
				headImageUrl = uinfo.getWechatHeadUrl();
			}
			data.put("headImageUrl", headImageUrl);
			data.put("companyCode", uinfo.getCompanyCode());
			data.put("companyAbbr", uinfo.getCompanyAbbr());
			data.put("position", uinfo.getPosition());
			data.put("accountType", uinfo.getAccountType());
			data.put("openId", bindOpenId);
			data.put("appId", appId);
			data.put("token", returnbean.getToken());
			data.put("privacyStatus", uinfo.getPrivacyStatus());
			data.put("authStatus", uinfo.getAuthStatus());
			data.put("identify", uinfo.getIdentify());
			info.setMessage("验证成功,可以操作了!");
			info.setToken(returnbean.getToken());
			info.setObject(data);
			info.setMessageCode(MeixConstantCode.M_1011);

			//记录登录日志
			userServiceImpl.saveLoginInfo(uinfo.getId());
			userLoginRecordService.saveUserLoginRecord(uinfo.getId(), clientType);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage("账号或密码错误，请重新填写");
			log.warn(mobile + "登录失败", e);
		}
		return info;
	}

	/**
	 * @Title: initLoginParams、
	 * @Description:初始化登录参数
	 */
	private UserInfo initLoginParams(UserInfo returnbean) throws Exception {
		returnbean.setMobile(MobileassistantUtil.getMobile(returnbean.getMobile()));
		returnbean.setMobileOrEmail(MobileassistantUtil.getMobile(returnbean.getMobileOrEmail()));
		returnbean.setPassword("");
		//用户头像
		UserInfo userInfoCache = getUserInfo(returnbean.getId());
		if (userInfoCache != null) {
			returnbean.setHeadImageUrl(OssResourceUtil.getHeadImage(userInfoCache.getHeadImageUrl()));
		}
		return returnbean;
	}

	/**
	 * @param request
	 * @Title: thirdLoginByOpenId、
	 * @Description: 公众号微信第三方微信登录
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/login/thirdLoginByOpenId", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo thirdLoginByOpenId(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		String openId = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			openId = MapUtils.getString(object, "openId");
			Integer clientType = MapUtils.getInteger(object, "clientType");

			@SuppressWarnings("rawtypes")
			Map map = userServiceImpl.getUidByOpenId(openId, clientType == null ? MeixConstants.CLIENT_TYPE_3 : clientType);

			long uid = MapUtils.getLongValue(map, "uid", 0l);
			UserInfo userInfo = getUserInfo(uid);
			if (null == userInfo) {
				//未注册用户
				info.setMessage("账户未注册！");
				info.setMessageCode(MeixConstantCode.M_1001);
				return info;
			}
			
			if (userInfo.getIsDelete() == MeixConstants.ACCOUNT_DJ) {
				info.setMessage("账户已冻结！");
				info.setMessageCode(MeixConstantCode.M_1039);
				return info;
			}
			/*************登录流程***************/
			// 登录时查询token，如果存在，则直接返回，否则登录生成新token
			SecToken secToken = insLoginService.getTokenByUser(userInfo.getMobile(), clientType);
			String dbtoken = secToken == null ? null : secToken.getToken();
			if (StringUtils.isNotBlank(dbtoken)) {
				info.setToken(dbtoken);
				info.setMessage("登录成功！");
				info.setMessageCode(1011);
			} else {
				// 生成token
				String token = SecurityUtil.generateLoginToken(userInfo.getId(), userInfo.getMobile(), userInfo.getCompanyCode(), 0, clientType);
				// 保存token
				insLoginService.saveToken(token, userInfo.getMobile(), clientType);
				info.setToken(token);
				dbtoken = token;
				log.info("thirdLoginByOpenId 返回token：【{}】", info.getToken());
			}

			//整理返回值
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("mobile", userInfo.getMobile());
			data.put("userName", userInfo.getUserName());
			data.put("id", userInfo.getId());
			data.put("headImageUrl", OssResourceUtil.getHeadImage(userInfo.getHeadImageUrl()));
			data.put("companyCode", userInfo.getCompanyCode());
			data.put("companyAbbr", userInfo.getCompanyAbbr());
			data.put("position", userInfo.getPosition());
			data.put("accountType", userInfo.getAccountType());
			data.put("openId", openId);
			data.put("privacyStatus", userInfo.getPrivacyStatus());
			data.put("token", dbtoken);
			data.put("authStatus", userInfo.getAuthStatus());
			data.put("identify", userInfo.getIdentify());
			info.setMessage("验证成功,可以操作了!");
			info.setObject(data);
			info.setMessageCode(MeixConstantCode.M_1011);


			//记录登录日志
			userServiceImpl.saveLoginInfo(userInfo.getId());
			userLoginRecordService.saveUserLoginRecord(userInfo.getId(), clientType);
		} catch (Exception e) {
			log.warn("thirdLoginByOpenId:【" + openId + "】第三方登录失败", e);
			info.setMessage("获取登录信息失败，请稍后重试");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 根据手机号匹配用户信息，设置到用户
	 *
	 * @param mobile
	 * @param userInfo
	 */
	private void setUserInfo(String mobile, UserInfo userInfo, long insCompanyCode) {
		userInfo.setMobile(mobile);
		// 内部用户
		InsUserInfo insUser = insUserService.getUserByMobileOrEmail(mobile, 0);
		if (insUser != null) {
			//研究所内部账号
			userInfo.setAccountType(MeixConstants.ACCOUNT_INNER);
			userInfo.setAuthStatus(MeixConstants.CUSTOMER_AUTH_SUCCESS);
			userInfo.setEmail(insUser.getEmail());
			userInfo.setUserName(insUser.getUserName());
			userInfo.setCompanyCode(insUser.getCompanyCode());
			userInfo.setPosition(insUser.getPosition());
			userInfo.setHeadImageUrl(OssResourceUtil.getHeadImage(insUser.getHeadImageUrl()));
			userInfo.setChiSpelling(PinyinHelper.getShortPinyin(userInfo.getUserName()).toUpperCase());
			userInfo.setInsUuid(insUser.getUser());
			return;
		}
		// 游客
		userInfo.setAccountType(MeixConstants.ACCOUNT_TOURIST); // 游客
		userInfo.setUserName(MeixConstants.DEFAULT_NAME);
		userInfo.setCompanyCode(0);
		userInfo.setCompanyAbbr("");
		userInfo.setIsDelete(MeixConstants.ACCOUNT_JH);
	}

	/**
	 * @param mobile
	 * @return long
	 * @Title: addThirdUser、
	 * @Description: 注册新用户
	 */
	private long register(UserInfo user, String mobile, long companyCode) {
		setUserInfo(mobile, user, companyCode);
		long uid = userServiceImpl.addUser(user);
		return uid;
	}


	// 退出
	@RequestMapping(value = "/user/loginOut", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo loginOut(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		String token = null;
		JSONObject jsonobject = null;
		try {
			jsonobject = MobileassistantUtil.getRequestParams(request);
			token = jsonobject.getString("token");
			String clientTypeStr = request.getHeader("clientType");
			int clientType = StringUtils.isBlank(clientTypeStr) ? MeixConstants.CLIENT_TYPE_3 : Integer.valueOf(clientTypeStr);
			long uid = MapUtils.getLongValue(jsonobject, "uid", 0);
			UserInfo userInfo = getUserInfo(uid);
			if (userInfo != null) {
				// 注销
				insLoginService.logout(userInfo.getMobile(), clientType);
			}
			info.setMessage("注销成功!");
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("注销失败");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("注销失败[" + token + "]" + e.getMessage(), e);
		}
		return info;
	}


	//验证Token
	@RequestMapping(value = "/user/validationToken", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo validationToken(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		String token = null;
		JSONObject jsonobject = null;
		try {
			jsonobject = MobileassistantUtil.getRequestParams(request);
			token = jsonobject.get("token").toString();
			String[] tokenArray = SecurityUtil.getTokenArray(token);
			if (tokenArray == null) {
				info.setToken("");
				info.setLoginstate("0");
				info.setMessageCode(MeixConstantCode.M_1010);
				info.setMessage("登录信息过期！请重新登录");
				return info;
			}
			int clientType = 8;
			String uid = tokenArray[0];
			UserInfo user = userServiceImpl.getUserInfo(Long.parseLong(uid), null, null);
			//账户信息发生变更，需要重新登录
			if (user == null) {
				String devMessage = tokenInvalidCause(token);
				info.setToken("");
				info.setLoginstate("0");
				info.setMessageCode(MeixConstantCode.M_1010);
				info.setMessage(devMessage);
				log.info("uid[" + uid + "]账户信息发生变更,需要重新登录");
				return info;
			}
			SecToken secToken = insLoginService.getTokenByUser(user.getMobile(), clientType);
			String dbtoken = secToken == null ? null : secToken.getToken();
			String deviceId = MapUtils.getString(jsonobject, "x", "");
			if (!StringUtils.isEmpty(token)) {
				user.setPassword("");
				info.setObject(user);
				//正式账户，验证token
				if (StringUtils.isEmpty(dbtoken) || !token.equals(dbtoken)) {
					info.setMessageCode(MeixConstantCode.M_1010);
					info.setMessage("Token令牌验证失败");
					info.setToken("");
					info.setLoginstate("0");
					return info;
				}

				user.setToken(token);
				initLoginParams(user);

				info.setKey(deviceId);
				info.setMessage("Token令牌验证成功!");
				info.setMessageCode(MeixConstantCode.M_1011);
				info.setToken(token);
				info.setObject(user);
				//判断登录的是移动端还是PC端
				if (clientType == MeixConstants.CLIENT_TYPE_PC) {
					return info;
				}
			} else {
				info.setMessageCode(MeixConstantCode.M_1010);
				info.setMessage("Token令牌验证失败");
				info.setToken("");
			}
		} catch (Exception e) {
			info.setMessage("登录状态过期，请重新登录");
			info.setMessageCode(MeixConstantCode.M_1010);
			info.setToken("");
			log.warn("token[" + token + "]失效" + e.getMessage(), e);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Title: tokenInvalid、
	 * @Description:token失效
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/login/tokenInvalid", method = {RequestMethod.POST, RequestMethod.GET})
	public @ResponseBody
	MessageInfo tokenInvalid(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		String token = null;
		try {
			token = request.getParameter("token");
			String devMessage = tokenInvalidCause(token);
			info.setToken("");
			info.setLoginstate("0");
			info.setMessageCode(MeixConstantCode.M_1010);
			info.setMessage(devMessage);
			log.error(devMessage);
		} catch (Exception e) {
			String devMessage = "Token令牌验证失败";
			info.setToken("");
			info.setLoginstate("0");
			log.error(devMessage + e.getMessage(), e);
		}
		return info;
	}

	/**
	 * @param token
	 * @return
	 * @Title: tokenInvalidCause、
	 * @Description:token不可用的原因
	 * @return: String
	 */
	private String tokenInvalidCause(String token) {
		String devMessage = "登录状态已过期，请重新登录!";
		return devMessage;
	}

	@RequestMapping(value = "/login/accessRefuse", method = {RequestMethod.POST, RequestMethod.GET})
	public @ResponseBody
	MessageInfo accessRefuse(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		info.setToken("");
		info.setLoginstate("0");
		return info;
	}

	@RequestMapping(value = "/login/tokenParmsError", method = {RequestMethod.POST, RequestMethod.GET})
	public @ResponseBody
	MessageInfo tokenParmsError(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo();
		info.setMessageCode(MeixConstantCode.M_1009);
		info.setMessage("请求参数格式不正确");
		info.setToken("");
		info.setLoginstate("0");
		return info;
	}

	@RequestMapping(value = "/login/echoRequests", method = {RequestMethod.POST, RequestMethod.GET})
	public @ResponseBody
	MessageInfo echoRequests(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo();
		String message = request.getParameter("message");
		if (message == null) {
			info.setMessage("重复请求！");
		} else {
			info.setMessage(message);
		}
		String strMessageCode = request.getParameter("messageCode");
		if (strMessageCode == null) {
			info.setMessageCode(MeixConstantCode.M_1053);
		} else {
			int messageCode = Integer.parseInt(strMessageCode);
			info.setMessageCode(messageCode);
		}

		return info;
	}

	@RequestMapping(value = "/login/getEncryptUid", method = RequestMethod.GET)
	public @ResponseBody
	String getEncryptUid(HttpServletRequest request, HttpServletResponse response) {
		String uid = request.getParameter("uid");
		return Des.encrypt(uid);
	}

	/**
	 * 微信账号解绑
	 **/
	@RequestMapping(value = "/user/unbindweixin", method = RequestMethod.GET)
	@ResponseBody
	public MessageInfo unbindweixin(HttpServletRequest request) {
		MessageInfo resp = new MessageInfo();
		try {
			String mobile = request.getParameter("mobile");
			String clientTypeStr = request.getHeader("clientType");
			int clientType = StringUtils.isBlank(clientTypeStr) ? MeixConstants.CLIENT_TYPE_3 : Integer.valueOf(clientTypeStr);
			if (StringUtils.isNotBlank(mobile)) {
				userServiceImpl.unbindweixin(mobile, clientType);
				// 注销
				insLoginService.logout(mobile, clientType);
				UserInfo user = userServiceImpl.getUserInfo(0, null, mobile);
				if (user == null) {
					resp.setMessage("未找到手机号信息");
					resp.setMessageCode(MeixConstantCode.M_1008);
					return resp;
				}
			}
			resp.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "操作失败";
			resp.setMessage(devMessage);
			resp.setMessageCode(MeixConstantCode.M_1009);
			log.error("微信账号解绑异常：【{}】", e);
		}
		//resp.setEnabled(0); 测试使用
		return resp;
	}

}
