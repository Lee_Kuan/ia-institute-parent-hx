package com.meix.institute.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meix.institute.BaseService;
import com.meix.institute.api.IReportService;
import com.meix.institute.config.CommonServerProperties;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.thread.SendPersonaInfoToMeixThread;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.vo.report.ReportDetailVo;
import com.meix.institute.vo.user.UserRecordStateVo;

import net.sf.json.JSONObject;
import oracle.sql.BLOB;

/**
 * @describe 研报/会议纪要 接口
 */
@Controller
@RequestMapping("/report/")
public class ReportController extends BaseService {
	private static final Logger log = LoggerFactory.getLogger(ReportController.class);

	@Autowired
	private IReportService reportServiceImpl;
	@Autowired
	private CommonServerProperties commonServerProperties;
//	@Autowired
//	private PdfConfig pdfConfig;

	/**
	 * @Description: 研报列表
	 */
	@RequestMapping(value = "getReportAllList", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getReportAllList(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			String clientTypeStr = request.getHeader("clientType");
			int clientType = org.apache.commons.lang.StringUtils.isBlank(clientTypeStr) ? 1 : Integer.valueOf(clientTypeStr);
			int issueType = MeixConstants.getIssuetypeByClientType(clientType);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			long companyCode = MapUtils.getLongValue(obj, "companyCode", commonServerProperties.getCompanyCode());
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			long authorCode = MapUtils.getLongValue(obj, "authorCode", 0);
			String infoType = MapUtils.getString(obj, "infoType");
			String publishDateFm = MapUtils.getString(obj, "publishDateFm");
			String publishDateTo = MapUtils.getString(obj, "publishDateTo");
			String condition = MapUtils.getString(obj, "condition"); // 作者名称、标题、摘要查询条件
			int type = MapUtils.getIntValue(obj, "type", 0);
//			if (StringUtils.isBlank(publishDateFm)) {
//				publishDateFm = DateUtil.getMonthDate(DateUtil.dateToStr(new Date(), DateUtil.dateShortFormat), -6); // 默认近半年
//			}
			if (StringUtils.isNotBlank(publishDateTo)) {
				publishDateTo = DateUtil.getDayDate(publishDateTo, 1);
			}
			List<String> infoTypeLists = null;
			if (StringUtils.isNotBlank(infoType)) {
				String[] infoTypes = StringUtils.split(infoType, ",");
				infoTypeLists = Arrays.asList(infoTypes);
			}
			List<ReportDetailVo> list = reportServiceImpl.getReportAllList(uid, type, companyCode, showNum, currentPage, issueType, authorCode, infoTypeLists, publishDateFm, publishDateTo, condition);
			info.setData(list);
			try {
				if (StringUtil.isNotEmpty(condition) && list != null && list.size() > 0) {
					List<UserRecordStateVo> searchList = new ArrayList<>(list.size());
					for (ReportDetailVo item : list) {
						// 推送给每市
						UserRecordStateVo vo = new UserRecordStateVo();
						vo.setDataState(MeixConstants.RECORD_SEARCH);
						vo.setUid(uid);
						vo.setDataId(item.getId());
						vo.setDataType(MeixConstants.USER_YB);
						searchList.add(vo);
					}
					MeixConstants.fixedThreadPool.execute(new SendPersonaInfoToMeixThread(searchList));
				}
			} catch (Exception e) {
				log.error("", e);
			}
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "获取研报列表失败";
			log.error(devMessage, e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage(devMessage);
		}

		return info;
	}

	/**
	 * @Description: 研报详情
	 */
	@RequestMapping(value = "getReportById", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getReportById(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long id = obj.getLong("id");
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String openId = MapUtils.getString(obj, "openId", "");

			ReportDetailVo vo = reportServiceImpl.getReportDetail(id, uid);
			if (vo == null) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("没有查到此研报的详细信息！");
				return info;
			}
			if (uid > 0 && vo.getPermission() == 1) {
				int dataType = MeixConstants.USER_YB;
				if (vo.getType() == 1) {
					dataType = MeixConstants.USER_HYJY;
				}
				saveUserReadRecordState(uid, id, dataType, openId);
			}
			info.setObject(vo);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "获取研报详情失败";
			log.error(devMessage, e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage(devMessage);
		}

		return info;
	}

	/**
	 * 获取研报原文
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "getPdfStream", method = RequestMethod.GET)
	public void getRemoteFileStream(HttpServletRequest request, HttpServletResponse response,
	                                @RequestParam(required = false) String fileUrl) {
		InputStream is = null;
		try {
			if (StringUtils.isBlank(fileUrl)) {
				return;
			}
			fileUrl = URLDecoder.decode(fileUrl, "utf-8");
//			String pdfServer = pdfConfig.getPdfServer();
//			fileUrl = pdfServer + fileUrl;
//			log.info("获取研报原文地址：【{}】", fileUrl);
//			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(
//				pdfConfig.getPdfHost(), pdfConfig.getPdfUserName(), pdfConfig.getPdfPassword());
//			byte[] bytes = HttpUtil.smbGet(fileUrl, auth);
//			ServletOutputStream os = response.getOutputStream();
//			os.write(bytes);
//			os.flush();
//			os.close();
			ServletOutputStream os = response.getOutputStream();
			BLOB res = reportServiceImpl.getReportFile(fileUrl);
			if(res == null) {
				os.close();
				return;
			}
			InputStream input = res.getBinaryStream();
            int len = (int) res.length();
            byte buffer[] = new byte[len];
			while ((len = input.read(buffer)) != -1) {
				os.write(buffer, 0, len);
			}
			os.flush();
			os.close();
			input.close();
//			URL url = new URL(fileUrl);
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//			// 设置连接超时时间
//			conn.setConnectTimeout(3000);
//			if (conn.getResponseCode() == 200) {
//				is = conn.getInputStream();
//				ServletOutputStream os = response.getOutputStream();
//				IOUtils.copy(is, os);
//				os.flush();
//				os.close();
//			}
		} catch (Exception e) {
			log.warn("获取获取研报原文失败", e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @Description: 研报合辑列表
	 */
	@RequestMapping(value = "getReportAlbumList", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getReportAlbumList(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			String clientTypeStr = request.getHeader("clientType");
			int clientType = org.apache.commons.lang.StringUtils.isBlank(clientTypeStr) ? 1 : Integer.valueOf(clientTypeStr);
			int issueType = MeixConstants.getIssuetypeByClientType(clientType);

			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int type = MapUtils.getIntValue(obj, "type", 0);
			List<Map<String, Object>> list = reportServiceImpl.getReportAlbumList(type, 1, null, null, null, showNum, currentPage, 1, issueType, 0, 0, -1);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "获取研报合辑列表失败";
			log.error(devMessage, e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage(devMessage);
		}

		return info;
	}


	/**
	 * @Description: 研报合辑详情列表
	 */
	@RequestMapping(value = "getReportAlbumHead", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getReportAlbumHead(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long uid = MapUtils.getLongValue(obj, "uid");
			long bid = MapUtils.getLongValue(obj, "id"); //合辑ID
			Map<String, Object> object = reportServiceImpl.getReportAlbumDetail(uid, bid);
			info.setObject(object);
			info.setMessageCode(MeixConstantCode.M_1008);
			if (uid > 0 && MapUtils.getIntValue(object, "permission", 0) == 1) {
				int dataType = MeixConstants.REPORT_ALBUM_DETAIL;
				int type = MapUtils.getIntValue(object, "type", 0);
				if (type == 1) {
					dataType = MeixConstants.MEETING_ALBUM_DETAIL;
				}
				saveUserReadRecordState(uid, bid, dataType, null);
			}
		} catch (Exception e) {
			String devMessage = "获取研报合辑列表失败";
			log.error(devMessage, e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage(devMessage);
		}

		return info;
	}

	/**
	 * @Description: 研报合辑详情列表
	 */
	@RequestMapping(value = "getReportAlbumDetail", method = RequestMethod.POST)
	public @ResponseBody
	MessageInfo getReportAlbumDetail(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);

			String clientTypeStr = request.getHeader("clientType");
			int clientType = org.apache.commons.lang.StringUtils.isBlank(clientTypeStr) ? 1 : Integer.valueOf(clientTypeStr);
			int issueType = MeixConstants.getIssuetypeByClientType(clientType);
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			long submenuId = MapUtils.getLongValue(obj, "submenuId", 0); //子菜单ID，不存在则为0
			long bid = MapUtils.getLongValue(obj, "id"); //合辑ID
			int type = MapUtils.getIntValue(obj, "type", 0);
			List<ReportDetailVo> list = reportServiceImpl.getReportListByAlbum(bid, submenuId, currentPage, showNum, type, issueType);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "获取研报合辑列表失败";
			log.error(devMessage, e);
			info.setMessageCode(MeixConstantCode.M_1009);
			info.setMessage(devMessage);
		}

		return info;
	}
}
