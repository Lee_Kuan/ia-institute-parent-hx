package com.meix.institute.controller;

import com.meix.institute.BaseService;
import com.meix.institute.api.IActivityService;
import com.meix.institute.api.IUserService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.core.Paged;
import com.meix.institute.entity.InsActivityPerson;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.response.PersonalInfoPo;
import com.meix.institute.thread.SendPersonaInfoToMeixThread;
import com.meix.institute.util.*;
import com.meix.institute.vo.CommentVo;
import com.meix.institute.vo.user.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.DayOfWeek;
import java.util.*;

@Controller
public class UserController extends BaseService {
	private static Logger log = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private IUserService userServiceImpl;
	private MessageInfo info = null;
	@Autowired
	private IActivityService activityServiceImpl;

	/**
	 * 获取我关注的人列表
	 *
	 * @author jiawj
	 * @since 2015年4月13日 上午10:04:05
	 */
	@RequestMapping(value = "/user/getMyConcrenedPersons", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getMyConcrenedPersons(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo result = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			Map<String, Object> params = new HashMap<>();
			params.put("uid", obj.getLong("uid"));
			params.put("searchType", obj.getInt("searchType"));
			if (obj.containsKey("keyWord")) {
				params.put("keyWord", obj.getString("keyWord"));
			}
			int currentPage = obj.getInt("currentPage");
			int showNum = obj.getInt("showNum");
			int userType = MapUtils.getIntValue(obj, "userType", 0);
			params.put("userType", userType);
			long companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
			params.put("companyCode", companyCode);
			params.put("currentPage", currentPage * showNum);
			params.put("showNum", showNum);
			List<Map<String, Object>> data = userServiceImpl.getMyConcrenedPersons(params);
			result.setData(data);
			result.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "获取我关注的人失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return result;
	}

	/**
	 * @param request
	 * @return
	 * @Title: getPersonalInfo、
	 * @Description:获取个人信息
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/user/getPersonalInfo", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getPersonalInfo(HttpServletRequest request) {
		JSONObject clienjson = null;
		info = new MessageInfo(request);
		try {
			clienjson = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(clienjson, "uid");
			long authorId = MapUtils.getLongValue(clienjson, "authorId", 0l);

			if (authorId == 0l) {
				authorId = uid;
			}

			PersonalInfoPo po = userServiceImpl.getPersonalInfo(uid, authorId);
			info.setMessageCode(MeixConstantCode.M_1008);
			info.setObject(po);
		} catch (Exception e) {
			String devMessage = "获取个人信息失败";
			log.warn(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Title: saveUserRecordState、
	 * @Description:保存用户点击状态
	 * @
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/user/saveUserRecordState")
	@ResponseBody
	public MessageInfo saveUserRecordState(HttpServletRequest request, HttpServletResponse response) {
		JSONObject clienjson = null;
		info = new MessageInfo(request);
		try {
			clienjson = MobileassistantUtil.getRequestParams(request);
			UserRecordStateVo vo = new UserRecordStateVo();
			vo.setDataId(MapUtils.getLongValue(clienjson, "dataId", 0));
			vo.setUid(MapUtils.getLongValue(clienjson, "uid", 0));
			vo.setDataState(MapUtils.getIntValue(clienjson, "dataState", 0));
			vo.setDataType(MapUtils.getIntValue(clienjson, "dataType", 0));
			vo.setDuration(MapUtils.getLong(clienjson, "duration", 0l));

			// 检验数据有效性
			if (vo.getUid() == 0 || vo.getDataState() == 0 || vo.getDataType() == 0) {
				info.setMessage("无效的用户行为");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}

			long createTime = MapUtils.getLongValue(clienjson, "createTime", 0);
			if (createTime > 0) {
				vo.setCreateTime(DateUtil.dateToStr(new Date(createTime), DateUtil.dateFormat));
			}
			if (vo.getDataId() != 0 ||
				(vo.getDataId() == 0 && (vo.getDataType() == MeixConstants.USER_HOME_PAGE ||
					vo.getDataType() == MeixConstants.USER_APPLET ||
					vo.getDataType() == MeixConstants.PAGE_RESEARCH ||
					vo.getDataType() == MeixConstants.PAGE_ACTIVITY ||
					vo.getDataType() == MeixConstants.PAGE_MINE
				))
				) {
				int ret = userServiceImpl.saveUserRecordState(vo);
				if (vo.getDataState() == MeixConstants.RECORD_FOLLOW
					&& (vo.getDataType() == MeixConstants.USER_YH)) {
					info.setObject(ret);
				}
			}
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "保存用户点击状态失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @Description: 通过微信的openId获取用户id
	 */
	@RequestMapping(value = "/user/getUidByOpenId", method = {RequestMethod.POST})
	@ResponseBody
	public MessageInfo getUidByOpenId(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String clientTypeStr = request.getHeader("clientType");
			int clientType = StringUtils.isBlank(clientTypeStr) ? MeixConstants.CLIENT_TYPE_3 : Integer.valueOf(clientTypeStr);
			String openId = null;
			String activityIdStr = null;
			long activityId = 0;
			String correspond = "无";
			String companyName = "";
			String position = "";
			int companyType = 0;
			if (object != null) {
				if (object.containsKey("openId")) {
					openId = object.getString("openId");
				}
				if (object.containsKey("activityId")) {
					activityId = object.getLong("activityId");
				}
			} else {
				openId = request.getParameter("openId");
				activityIdStr = request.getParameter("activityId");
				if (!StringUtils.isBlank(activityIdStr)) {
					try {
						activityId = Long.valueOf(activityIdStr);
					} catch (Exception e) {
						activityId = 0;
					}
				}
			}
			@SuppressWarnings("rawtypes")
			Map uidMap = userServiceImpl.getUidByOpenId(openId, clientType);
			long uid = MapUtils.getLongValue(uidMap, "uid", 0);
			String userName = "";
			String mobile = "";
			String joinTime = "";
			int type = 2;
			Map<String, Object> result = new HashMap<>();
			if (activityId == 0) {
				UserInfo user = getUserInfo(uid);
				if (user != null) {
					userName = user.getUserName();
					mobile = MobileassistantUtil.getMobile(user.getMobile());
					companyName = user.getCompanyAbbr();
					position = user.getPosition();
					if (StringUtils.isEmpty(mobile)) {
						userName = "";
						companyType = 0;
						companyName = "";
						position = "";
					}
				}
				result.put("uid", uid);
				result.put("isJoinIn", 0);
				result.put("userName", userName);
				result.put("mobile", mobile);
				result.put("joinTime", joinTime);
				result.put("type", 2);
				result.put("companyType", companyType);
				result.put("position", position);
				result.put("companyName", companyName);
				result.put("correspond", correspond);
				info.setObject(result);
				info.setMessageCode(MeixConstantCode.M_1008);
				return info;
			}
			InsActivityPerson map = activityServiceImpl.getJoinPerson(activityId, uid);
			if (map == null) {
				UserInfo cache = getUserInfo(uid);
				if (cache != null) {
					userName = cache.getUserName();
					mobile = MobileassistantUtil.getMobile(cache.getMobile());
					companyName = cache.getCompanyAbbr();
					UserInfo user = userServiceImpl.getUserInfo(uid, null, null);
					if (user != null) {
						position = user.getPosition();
					}
					if (StringUtils.isEmpty(mobile)) {
						userName = "";
						companyType = 0;
						companyName = "";
						position = "";
					}
				}
				result.put("uid", uid);
				result.put("isJoinIn", 0);
				if (StringUtils.isEmpty(userName)) {
					userName = "";
					mobile = "";
				}
				result.put("userName", userName);
				result.put("mobile", mobile);
				result.put("joinTime", joinTime);
				result.put("type", type == 0 ? 2 : type);
				result.put("companyType", companyType);
				result.put("position", position);
				result.put("companyName", companyName);
				result.put("correspond", correspond);
			} else {
				userName = map.getUserName();
				mobile = map.getMobile();
				type = map.getType();
				companyName = map.getCompanyName();
				position = map.getPosition();
				correspond = map.getSalerName();
				if (StringUtils.isEmpty(userName)) {
					UserInfo user = getUserInfo(uid);
					if (user != null) {
						userName = user.getUserName();
						mobile = MobileassistantUtil.getMobile(user.getMobile());
						companyName = user.getCompanyAbbr();
						position = user.getPosition();
						if (StringUtils.isEmpty(mobile)) {
							userName = "";
							companyType = 0;
							companyName = "";
							position = "";
						}
					} else {
						userName = "";
						mobile = "";
					}
				}
				result.put("userName", userName);
				result.put("mobile", mobile);
				result.put("type", type == 0 ? 2 : type);
				result.put("uid", uid);
				result.put("isJoinIn", 1);
				result.put("joinTime", DateUtil.dateToStr(map.getCreatedAt(), "yyyy-MM-dd HH:mm:ss"));
				result.put("companyType", companyType);
				result.put("position", position);
				result.put("companyName", companyName);
				result.put("correspond", correspond);
			}
			info.setObject(result);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "通过微信的openId获取用户失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @Description: 保存留言
	 */
	@RequestMapping(value = "/user/saveComment", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo saveComment(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			long dataId = MapUtils.getLongValue(obj, "dataId");
			int dataType = MapUtils.getIntValue(obj, "dataType");
			String commentStr = MapUtils.getString(obj, "comment", "");
			Float score = MapUtils.getFloat(obj, "score", 5.0f);
			int hideNameFlag = MapUtils.getIntValue(obj, "hideName", 1);

			/*if (getAccountType(uid) != MeixConstants.ACCOUNT_INNER &&
				getAccountType(uid) != MeixConstants.ACCOUNT_WL &&
				getAccountType(uid) != MeixConstants.ACCOUNT_NORMAL) {
				info.setMessage("无评论权限");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}*/

			info.setObject(userServiceImpl.saveServerComment(uid, 0, 0, score, commentStr, dataId, dataType, hideNameFlag));

			info.setMessageCode(MeixConstantCode.M_1008);

		} catch (Exception e) {
			String devMessage = "保存留言失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 查询评价列表
	 */
	@RequestMapping(value = "/service/getCommentList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getCommentList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {

			obj = MobileassistantUtil.getRequestParams(request);
			long id = MapUtils.getLongValue(obj, "id");
			int type = MapUtils.getIntValue(obj, "type", 0);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 20);
			long version = RequestUtil.getVersionValue(request);
			long uid = MapUtils.getLongValue(obj, "uid");
			long companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
			String condition = MapUtils.getString(obj, "condition");
			JSONArray jsonArray = null;
			if (null != obj.get("ids")) {
				jsonArray = obj.getJSONArray("ids");
			}
			List<CommentVo> list = userServiceImpl.getCommentList(uid, id, type, version, currentPage, showNum, jsonArray, companyCode, condition);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);

		} catch (Exception e) {
			String devMessage = "获取留言列表失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @Description: 关注列表
	 */
	@RequestMapping(value = "/user/getFocusedPersons", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getFocusedPersons(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			int searchType = MapUtils.getIntValue(obj, "searchType", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);

			List<Map<String, Object>> list = userServiceImpl.getFocusedPersons(uid, searchType, showNum, currentPage);

			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "获取关注列表失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}

		return info;
	}

	/**
	 * @date 2018年1月8日
	 * @Description: 通过微信的openId获取用户信息
	 */
	@RequestMapping(value = "/user/getUserInfoByOpenId", method = {RequestMethod.POST})
	@ResponseBody
	public MessageInfo getUserInfoByOpenId(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			String clientTypeStr = request.getHeader("clientType");
			int clientType = StringUtils.isBlank(clientTypeStr) ? MeixConstants.CLIENT_TYPE_3 : Integer.valueOf(clientTypeStr);
			String openId = MapUtils.getString(object, "openId", "");
			@SuppressWarnings("rawtypes")
			Map uidMap = userServiceImpl.getUidByOpenId(openId, clientType);
			long uid = MapUtils.getLongValue(uidMap, "uid", 0);
			if (uid <= 0) {
				info.setMessage("用户信息不存在");
				info.setMessageCode(MeixConstantCode.M_1016);
				info.setObject(new JSONObject());
				return info;
			}
			UserInfo cache = getUserInfo(uid);
			if (cache == null) {
				info.setMessage("用户信息不存在");
				info.setMessageCode(MeixConstantCode.M_1016);
				info.setObject(new JSONObject());
				return info;
			}
			Map<String, Object> map = new HashMap<>();
			map.put("uid", cache.getId());
			map.put("companyCode", cache.getCompanyCode());
			map.put("userName", cache.getUserName());
			map.put("mobile", cache.getMobile());
			map.put("headImageUrl", OssResourceUtil.getHeadImage(cache.getHeadImageUrl()));
			map.put("companyAbbr", cache.getCompanyAbbr());
			map.put("createTime", cache.getCreateTime());
			map.put("chiSpelling", cache.getChiSpelling());
			map.put("comment", cache.getComment());
			map.put("accountType", cache.getAccountType());

			info.setObject(map);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "通过微信的openId获取用户信息失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 保存最后一次用户点击记录
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/user/saveLastUserRecord", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo saveLastUserRecord(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject object = null;
		try {
			object = MobileassistantUtil.getRequestParams(request);
			UserRecordStateVo record = (UserRecordStateVo) JSONObject.toBean(object, UserRecordStateVo.class);
			userServiceImpl.saveLastUserRecord(record);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "保存最后一次用户点击记录失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 获取公众号消息推送所需要的openId
	 *
	 * @param request
	 * @param response
	 * @return
	 * @author likuan    2019年6月28日14:55:23
	 */
	@RequestMapping(value = "/user/getOpenIdByUid", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getOpenIdByUid(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			int clientType = MapUtils.getIntValue(obj, MeixConstants.CLIENT_TYPE_HEAD);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String companyCode = MapUtils.getString(obj, "appId", "");
			String openId = userServiceImpl.getOpenIdByUid(uid, companyCode, clientType);
			info.setObject(openId);
		} catch (Exception e) {
			String devMessage = "获取openId失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 上传名片/头像
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/user/uploadCardImg", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo uploadCardImg(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			int dealType = MapUtils.getIntValue(obj, "dealType", 0); // 处理类型： 6上传用户名片 7 更新名片
			int authStatus = MapUtils.getIntValue(obj, "authStatus", 1); // 当前认证状态
			int issueType = MapUtils.getIntValue(obj, MeixConstants.CLIENT_TYPE_HEAD, 0);
			String mobile = MapUtils.getString(obj, "mobile");
			String userName = MapUtils.getString(obj, "userName");
			String fileName = MapUtils.getString(obj, "fileName");
			String companyAbbr = MapUtils.getString(obj, "companyAbbr");
			String email = MapUtils.getString(obj, "email");
			String position = MapUtils.getString(obj, "position");
			int identify = MapUtils.getIntValue(obj, "identify", 0); // 用户身份，0-个人投资者，1-普通机构投资者，2-专业机构投资者

			if (uid == 0) {
				info.setMessageCode(MeixConstantCode.M_1016);
				info.setMessage("用户信息不存在!");
				return info;
			}
			if (dealType != 6 && dealType != 7) {
				info.setMessageCode(MeixConstantCode.M_1009);
				info.setMessage("暂不支持该功能!");
				return info;
			}

			UserInfo userInfo = getUserInfo(uid);
			if (userInfo == null) {
				info.setMessageCode(MeixConstantCode.M_1016);
				info.setMessage("用户信息不存在!");
				return info;
			}

			if (StringUtil.isBlank(fileName)) {
				Map<String, Object> lastCard = userServiceImpl.selectCardByUid(uid, mobile, dealType);
				if (lastCard != null) {
					fileName = MapUtils.getString(lastCard, "url", "");
				}
			}

			Map<String, Object> params = new HashMap<>();
			params.put("imageName", fileName);
			params.put("isUsed", 0); // 默认未使用（需要审核）
			params.put("url", fileName);
			params.put("uid", uid);
			params.put("mobile", mobile);
			params.put("userName", userName);
			params.put("issueType", issueType);
			params.put("type", dealType);
			params.put("position", position);
			params.put("companyName", companyAbbr);
			params.put("email", email);

			long cardId = userServiceImpl.addCustomerCard(params);
			userServiceImpl.updateUserIdentify(uid, identify);
			log.info("上传成功，uid:{}，cardId：{}", uid, cardId);

			// 更新名片和认证状态
//			if (authStatus < 1 || authStatus == 2) {
			authStatus = 1;
//			}
			userServiceImpl.updateUserCard(uid, 0, authStatus, null, null, null, null);

			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "上传信息失败";
			log.error(devMessage, e);
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 获取分析师列表
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/user/getUserList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getUserList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		Map<String, Object> params = new HashMap<>();

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);

			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			String condition = MapUtils.getString(obj, "condition"); // 查询条件
			String position = MapUtils.getString(obj, "position");
			int userType = MapUtils.getIntValue(obj, "userType", 0);
			int analystsType = MapUtils.getIntValue(obj, "analystsType", 0); // 分析师类型:  0 全部  1 所长 2 首席分析师 3 我关注的
			String directions = MapUtils.getString(obj, "directions");

			params.put("showNum", showNum);
			params.put("currentPage", currentPage * showNum);
			params.put("condition", StringUtil.replaceMysqlWildcard(condition));
			params.put("uid", uid);
			params.put("position", position);
			if (StringUtils.isNotBlank(directions)) {
				String[] split = StringUtils.split(directions, ",");
				params.put("directions", Arrays.asList(split));
			}
			if (userType > 0) params.put("userType", userType);
			if (analystsType > 0) params.put("analystsType", analystsType);
			params.put("userState", 0); // 默认有效（在职）用户

			Paged<Map<String, Object>> data = userServiceImpl.getAnalystList(params);

			info.setMessageCode(MeixConstantCode.M_1008);
			info.setData(data.getList());
			info.setDataCount(data.getCount());
			try {
				if (StringUtil.isNotEmpty(condition) && data.getList() != null && data.getList().size() > 0) {
					List<UserRecordStateVo> searchList = new ArrayList<>(data.getList().size());
					for (Map<String, Object> item : data.getList()) {
						// 推送给每市
						UserRecordStateVo vo = new UserRecordStateVo();
						vo.setDataState(MeixConstants.RECORD_SEARCH);
						vo.setUid(uid);
						vo.setDataId(MapUtils.getLongValue(item, "contactId", 0L));
						vo.setDataType(MeixConstants.USER_YH);
						searchList.add(vo);
					}
					MeixConstants.fixedThreadPool.execute(new SendPersonaInfoToMeixThread(searchList));
				}
			} catch (Exception e) {
				log.error("", e);
			}
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("getUserList:异常", e);
		}
		return info;
	}


	/**
	 * @Description: 获取相关推荐列表
	 */
	@RequestMapping(value = "/user/getRelatedRecommendations", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getRelatedRecommendations(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			long authorId = MapUtils.getLongValue(obj, "authorId", 0);
			long dataId = MapUtils.getLongValue(obj, "dataId", 0);
			int dataType = MapUtils.getIntValue(obj, "dataType", 0);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int type = MapUtils.getIntValue(obj, "type", 1);//1非作者相关推荐 2作者相关推荐
			String clientTypeStr = request.getHeader("clientType");
			int clientType = org.apache.commons.lang.StringUtils.isBlank(clientTypeStr) ? 1 : Integer.valueOf(clientTypeStr);
			int issueType = MeixConstants.getIssuetypeByClientType(clientType);
			List<UserRecommendationInfo> list = userServiceImpl.getRelatedRecommendations(uid, authorId, dataId, dataType, currentPage, showNum, issueType, type);

			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);

		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("getRelatedRecommendations:异常", e);
		}

		return info;
	}

	/**
	 * @Description: 记录用户隐私策略状态
	 */
	@RequestMapping(value = "/user/updateUserPrivacyPolicyStatus", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo recordUserPrivacyPolicyStatus(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String user = MapUtils.getString(obj, "uuid");
			int status = MapUtils.getIntValue(obj, "status", 0); // 隐私策略状态  (1 仅浏览  2 同意并继续)

			userServiceImpl.updateUserPrivacyPolicyStatus(uid, status, user);

			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("recordUserPrivacyPolicyStatus:异常", e);
		}

		return info;
	}
	
	/**
	 * @Description: 获取用户行为记录列表
	 */
	@RequestMapping(value = "/user/getUserRecordStateList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getUserRecordStateList(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);

			Map<String, Object> params = new HashMap<>();
			params.put("uid", uid);
			params.put("currentPage", currentPage * showNum);
			params.put("showNum", showNum);
			params.put("companyCode", commonServerProperties.getCompanyCode());

			List<UserRecordStateResp> list = userServiceImpl.getUserRecordStateList(params);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("recordUserPrivacyPolicyStatus:异常", e);
		}
		return info;
	}

	/**
	 * @Description: 获取用户内容列表
	 */
	@RequestMapping(value = "/user/getUserContentReadStat", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getUserContentReadStat(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);

			Map<String, Object> params = new HashMap<>();
			params.put("uid", uid);
			params.put("currentPage", currentPage * showNum);
			params.put("showNum", showNum);

			String startTime = DateUtil.getWeekBegin(DayOfWeek.MONDAY);
			String endTime = DateUtil.getCurrentDateTime();
			params.put("startTime", startTime);
			params.put("endTime", endTime);

			//替换成内部用户表id
			long insUid = userServiceImpl.getInsUidByCustomerId(uid);
			params.put("uid", insUid);

			List<UserContentReadStat> list = userServiceImpl.getUserContentReadStat(params);
			info.setData(list);
			long weekDataCount = userServiceImpl.getUserContentCount(params);
			long totalReadNum = userServiceImpl.getDataReadNum(insUid, null, null);
			long weekIncreaseReadNum = userServiceImpl.getDataReadNum(insUid, startTime, endTime);
			Map<String, Object> dataObj = new HashMap<>();
			dataObj.put("weekDataCount", weekDataCount);
			dataObj.put("totalReadNum", totalReadNum);
			dataObj.put("weekIncreaseReadNum", weekIncreaseReadNum);
			info.setObject(dataObj);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("recordUserPrivacyPolicyStatus:异常", e);
		}
		return info;
	}
}
