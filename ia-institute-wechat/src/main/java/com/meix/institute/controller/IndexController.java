package com.meix.institute.controller;

import com.meix.institute.BaseService;
import com.meix.institute.api.IHomeService;
import com.meix.institute.api.IUserService;
import com.meix.institute.config.SmsMsgConfig;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.impl.IaServiceImpl;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.response.HoneybeeMsgInfo;
import com.meix.institute.service.SmsService;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.util.StringUtils;
import com.meix.institute.vo.IA_PWDMapping;
import com.meix.institute.vo.banner.InsBannerVo;
import com.meix.institute.vo.user.UserInfo;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * Created by zenghao on 2019/7/24.
 */
@Controller
public class IndexController extends BaseService {
	private static Logger log = LoggerFactory.getLogger(IndexController.class);

	/**
	 * ia-server 通信接口
	 * http|https
	 */
	@Autowired
	protected IaServiceImpl iaServiceImpl;
	@Autowired
	private IUserService userService;
	@Autowired
	private SmsMsgConfig smsMsgConfig;
	@Autowired
	private IHomeService homeService;
	@Autowired
	private SmsService smsService;

	@RequestMapping(value = "/", method = {RequestMethod.GET})
	@ResponseBody
	public Object index() {
		return "index";
	}

	@RequestMapping(value = "/test/forwardout", method = {RequestMethod.GET})
	@ResponseBody
	public JSONObject forwardout() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tempToken", "nozuonodie");
		JSONObject resp = iaServiceImpl.httpsGetByTempToken("test/test.do", params, false, false, 10000);
		return resp;
	}

	@RequestMapping(value = "/test/sendSms", method = {RequestMethod.GET})
	@ResponseBody
	public MessageInfo sendSms(@RequestParam(value = "mobile", required = false) String mobile) {
		MessageInfo info = new MessageInfo();
		info.setEnabled(0);
		try {
			if (StringUtil.isBlank(mobile)) {
				info.setMessage("手机号不能为空");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}

			IA_PWDMapping bean = new IA_PWDMapping();
			bean.setMobileOrEmail(mobile);
			bean.setLoginType(MeixConstants.LOGIN_TYPE_1);//1 手机登录
			//String code = userServiceImpl.saveVerCode(bean);
			String code = "123456";
			log.info(bean.getMobileOrEmail() + ":" + code);

			String content = getSmsContent(smsMsgConfig.getLogin(), "{\"code\":\"" + code + "\",\"clientType\":\"" + "公众号" + "\"}");
			String result = smsService.sendSMS(bean.getMobileOrEmail(), content);
			if (result == null) {
				info.setMessage("验证码发送成功！");
				info.setMessageCode(MeixConstantCode.M_1008);
			} else {
				info.setMessage(result);
				info.setMessageCode(MeixConstantCode.M_1009);
			}
		} catch (Exception e) {
			info.setMessage("验证码发送失败！");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("getVerCode异常：", e);
		}
		return info;
	}

	/**
	 * 获取推荐系统提供的首页瀑布流数据列表
	 *
	 * @param request
	 * @param info
	 * @return
	 * @throws Exception
	 */
	private Object getUserHoneybee(HttpServletRequest request, MessageInfo info) {
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			String clientTypeStr = request.getHeader("clientType");
			int clientType = StringUtils.isBlank(clientTypeStr) ? 1 : Integer.valueOf(clientTypeStr);
			int issueType = MeixConstants.getIssuetypeByClientType(clientType);
			obj.remove("token");
//			调用推荐系统
			obj.put("companyCode", commonServerProperties.getCompanyCode());
			obj.put("multiple", 2);
			long uid = MapUtils.getLong(obj, "uid", 0L);
			if (uid > 0) {
				UserInfo userInfo = userService.getUserInfo(uid, null, null);
				if (userInfo != null) {
					obj.put("mobile", userInfo.getMobile());
				}
			}
			obj.put("tempToken", "nozuonodie");

			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 20);
			String lastId = MapUtils.getString(obj, "startId");
			HoneybeeMsgInfo data = homeService.getMainPageWaterFlowList(uid, currentPage, showNum, lastId, issueType);
			if (data != null) {
				info.setObject(data.getObject());
				info.setData(data.getData());
				info.setMessageCode(MeixConstantCode.M_1008);
			} else {
				info.setData(Collections.EMPTY_LIST);
				info.setMessageCode(MeixConstantCode.M_1008);
			}
		} catch (Exception e) {
			log.error("获取推荐系统数据失败", e);
			info.setMessage("获取推荐系统数据失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 获取首页瀑布流数据列表
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/index/getMainPageComponentList", method = RequestMethod.POST)
	@ResponseBody
	public Object getMainPageComponentList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		MessageInfo info = new MessageInfo(request);
//		使用推荐系统 获取瀑布流数据 ，组合详情下的相关资讯还是走以前的方式 searchType=1
		return getUserHoneybee(request, info);
	}

	/**
	 * 未登录用户瀑布流
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/index/getNoLoginPageComponentList", method = RequestMethod.POST)
	@ResponseBody
	public Object getNoLoginPageComponentList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		MessageInfo info = new MessageInfo(request);
		return getUserHoneybee(request, info);

//		//测试接口是否导致卡顿
//		info.setData(Collections.EMPTY_LIST);
//		info.setMessageCode(MeixConstantCode.M_1008);
//		return info;
	}

	/**
	 * 获取热点推荐数据列表
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/index/getHotRecommendList", method = RequestMethod.POST)
	@ResponseBody
	public Object getHotRecommendList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLong(obj, "uid", 0L);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 5);
			String clientTypeStr = request.getHeader("clientType");
			int clientType = StringUtils.isBlank(clientTypeStr) ? 1 : Integer.valueOf(clientTypeStr);
			int issueType = MeixConstants.getIssuetypeByClientType(clientType);
			HoneybeeMsgInfo data = homeService.getHotRecommendList(uid, currentPage * showNum, showNum, issueType);
			if (data != null) {
				info.setObject(data.getObject());
				info.setData(data.getData());
				info.setMessageCode(MeixConstantCode.M_1008);
			} else {
				info.setData(Collections.EMPTY_LIST);
				info.setMessageCode(MeixConstantCode.M_1008);
			}
		} catch (Exception e) {
			log.info("获取热点推荐数据失败", e);
			info.setMessage("获取热点推荐数据失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * 获取banner列表
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/index/getBannerList", method = RequestMethod.POST)
	@ResponseBody
	public Object getBannerList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			int applyModule = MapUtils.getIntValue(obj, "applyModule", 0);
			int bannerType = MapUtils.getIntValue(obj, "bannerType", -1);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", -1);
			if (applyModule == 0) {
				info.setMessage("应用模块必填！");
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			List<InsBannerVo> list = homeService.getBannerList(applyModule, bannerType, currentPage, showNum);
			info.setData(list == null ? new ArrayList<>() : list);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			log.info("获取banner数据失败", e);
			info.setMessage("获取banner数据失败");
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
}
