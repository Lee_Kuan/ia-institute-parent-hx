package com.meix.institute.controller;

import com.meix.institute.BaseService;
import com.meix.institute.api.ICompanyService;
import com.meix.institute.api.IMeixSyncService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.util.DateUtil;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.vo.stock.StockVo;
import com.meix.institute.vo.user.InfluenceAnalyseVo;
import com.meix.institute.vo.user.InfluenceTrend;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 公司设置
 */
@Controller
public class CompanyController extends BaseService {
	private static Logger log = LoggerFactory.getLogger(CompanyController.class);

	@Autowired
	private ICompanyService companySerciveImpl;
	@Autowired
	private IMeixSyncService meixServiceImpl;

	@RequestMapping(value = "/company/getCompanyMonthlyStock", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getCompanyMonthlyStock(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			long companyCode = obj.getLong("companyCode");
			String month = MapUtils.getString(obj, "month", null);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 20);
			Map<String, Object> params = new HashMap<>();
			params.put("companyCode", companyCode);
			params.put("uid", uid);
			params.put("month", month);
			params.put("currentPage", currentPage * showNum);
			params.put("showNum", showNum);
			List<StockVo> result = companySerciveImpl.getCompanyMonthlyStockVo(params);

			info.setData(result);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String errorMsg = "获取月度金股失败";
			log.error(errorMsg, e);
			info.setMessage(errorMsg);
			info.setMessageCode(MeixConstantCode.M_1009);
		}

		return info;
	}

	@RequestMapping(value = "/company/getGoldStockCombYield", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getGoldStockCombYield(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
//		JSONObject obj = null;
		try {
//			obj = MobileassistantUtil.getRequestParams(request);
			JSONObject res = meixServiceImpl.getGoldStockCombYield();
			if (res != null && res.containsKey("object")) {
				info.setObject(res.get("object"));
				info.setMessageCode(MeixConstantCode.M_1008);
			} else {
				info.setMessage("未知错误！");
				info.setMessageCode(MeixConstantCode.M_1009);
			}
		} catch (Exception e) {
			String errorMsg = "获取金股组合收益失败";
			log.error(errorMsg, e);
			info.setMessage(errorMsg);
			info.setMessageCode(MeixConstantCode.M_1009);
		}

		return info;
	}
	
	@RequestMapping(value = "/company/getCompanyInfluenceAnalysis", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getCompanyInfluenceAnalysis(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long companyCode = obj.getLong("companyCode");
			int dataType = MapUtils.getIntValue(obj, "dataType", 0);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 2);
			Map<String, Object> params = new HashMap<>();
			params.put("companyCode", companyCode);
			params.put("dataType", dataType);
			params.put("currentPage", currentPage * showNum);
			params.put("showNum", showNum);
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			params.put("startTime", DateUtil.dateToStr(calendar.getTimeInMillis(), DateUtil.dateFormat));
			params.put("endTime", DateUtil.getCurrentDateTime());
			List<InfluenceAnalyseVo> result = companySerciveImpl.getCompanyInfluenceAnalysis(params);
			info.setData(result);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String errorMsg = "获取影响力分析数据失败";
			log.error(errorMsg, e);
			info.setMessage(errorMsg);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	@RequestMapping(value = "/company/getCompanyInfluenceTrend", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getCompanyInfluenceTrend(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long companyCode = obj.getLong("companyCode");
			Map<String, Object> params = new HashMap<>();
			params.put("companyCode", companyCode);
			InfluenceTrend result = companySerciveImpl.getCompanyInfluenceTrend(params);
			info.setObject(result);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String errorMsg = "获取影响力分析数据失败";
			log.error(errorMsg, e);
			info.setMessage(errorMsg);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
}
