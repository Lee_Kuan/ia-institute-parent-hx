package com.meix.institute.controller;

import com.meix.institute.api.ISelfStockService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.thread.SendPersonaInfoToMeixThread;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.vo.stock.StockVo;
import com.meix.institute.vo.user.UserRecordStateVo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/9/23.
 */
@RestController
public class SelfStockController {
	private static Logger log = LoggerFactory.getLogger(SelfStockController.class);
	@Autowired
	private ISelfStockService selfStockServiceImpl;

	/**
	 * 获取所有自选股
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/selfStock/getUserSelfStock", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getUserSelfStock(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			int showNum = MapUtils.getIntValue(obj, "showNum", 20);
			Map<String, Object> params = new HashMap<>();
			params.put("uid", uid);
			params.put("currentPage", currentPage * showNum);
			params.put("showNum", showNum);
			List<StockVo> result = selfStockServiceImpl.getUserSelfStockList(params);
			info.setData(result);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "获取自选股失败";
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}

		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Title: editSelfStock、
	 * @Description:编辑自选股
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/selfStock/editSelfStock", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo editSelfStock(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONArray jsonArray = null;
		JSONObject clinetObj = null;
		long uid = 0;
		clinetObj = MobileassistantUtil.getRequestParams(request);
		try {
			jsonArray = clinetObj.getJSONArray("data");
			uid = clinetObj.getLong("uid");

			JSONObject obj = null;
			for (int i = 0; i < jsonArray.size(); i++) {
				obj = jsonArray.getJSONObject(i);
				int innerCode = obj.getInt("innerCode");
				int flag = obj.getInt("flag");
				if (flag == 3) {
					//删除自选
					selfStockServiceImpl.updateSelfStock(innerCode, uid, 1);

					// 推送给每市
					UserRecordStateVo vo = new UserRecordStateVo();
					vo.setDataState(MeixConstants.RECORD_DEL_SELF_STOCK);
					vo.setUid(uid);
					vo.setDataId(innerCode);
					vo.setDataType(MeixConstants.USER_GP);
					MeixConstants.fixedThreadPool.execute(new SendPersonaInfoToMeixThread(Collections.singletonList(vo)));
				}
			}
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "编辑自选股列表出错";
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
			return info;
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @Title: addSelfStock、
	 * @Description:添加自选股
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/selfStock/addSelfStock", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo addSelfStock(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject clinetObj = null;
		try {
			clinetObj = MobileassistantUtil.getRequestParams(request);

			long uid = MapUtils.getLongValue(clinetObj, "uid");
			int innerCode = clinetObj.getInt("innerCode");

			int exist = selfStockServiceImpl.checkStockIsExist(uid, innerCode);
			if (exist > 0) {
				// 已存在
			} else {
				if (selfStockServiceImpl.updateSelfStock(innerCode, uid, 0) == 0) {
					selfStockServiceImpl.addSelfStock(innerCode, uid);
				}
			}
			// 推送给每市
			UserRecordStateVo vo = new UserRecordStateVo();
			vo.setDataState(MeixConstants.RECORD_ADD_SELF_STOCK);
			vo.setUid(uid);
			vo.setDataId(innerCode);
			vo.setDataType(MeixConstants.USER_GP);
			MeixConstants.fixedThreadPool.execute(new SendPersonaInfoToMeixThread(Collections.singletonList(vo)));

			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String devMessage = "保存自选股列表出错";
			info.setMessage(devMessage);
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}
}
