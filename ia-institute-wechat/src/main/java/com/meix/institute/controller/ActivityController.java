package com.meix.institute.controller;

import com.meix.institute.BaseService;
import com.meix.institute.api.IActivityService;
import com.meix.institute.api.IInsUserService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.entity.InsUser;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.thread.SendPersonaInfoToMeixThread;
import com.meix.institute.util.MobileassistantUtil;
import com.meix.institute.util.RequestUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.vo.activity.ActivityListSearchVo;
import com.meix.institute.vo.activity.ActivityVo;
import com.meix.institute.vo.activity.InsActivityVo;
import com.meix.institute.vo.user.UserInfo;
import com.meix.institute.vo.user.UserRecordStateVo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by zenghao on 2019/9/25.
 */
@Controller
@RequestMapping(value = "/activity")
public class ActivityController extends BaseService {
	private static final Logger log = LoggerFactory.getLogger(ActivityController.class);
	@Autowired
	private IActivityService activityServiceImpl;
	@Autowired
	private IInsUserService insUserService;

	/**
	 * @Description: 查询活动列表
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getActivityList", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getActivityList(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;

		try {
			obj = MobileassistantUtil.getRequestParams(request);
			ActivityListSearchVo searchVo = new ActivityListSearchVo();

			String clientTypeStr = request.getHeader("clientType");
			int clientType = org.apache.commons.lang.StringUtils.isBlank(clientTypeStr) ? 1 : Integer.valueOf(clientTypeStr);
			int issueType = MeixConstants.getIssuetypeByClientType(clientType);
			searchVo.setClientType(issueType);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			int activityType = MapUtils.getIntValue(obj, "activityType", 0);
			int stockRange = MapUtils.getIntValue(obj, "stockRange", 0);//0 全部 1自选股 2 单个股票 3 组合相关个股
			int activityRange = MapUtils.getIntValue(obj, "activityRange", 0);// 0 全部  1我发起的活动 2我参加的活动 3我录入的活动 4 我公司预约的活动 5 我发起的和我录入的
			int showNum = MapUtils.getIntValue(obj, "showNum", 10);
			int currentPage = MapUtils.getIntValue(obj, "currentPage", 0);
			String startDateTime = MapUtils.getString(obj, "startDateTime", ""); // 查询的起始时间
			/*if (StringUtils.isBlank(startDateTime)) {
			    startDateTime = DateUtil.getMonthDate(DateUtil.dateToStr(new Date(), DateUtil.dateShortFormat), -6); // 默认近半年
			}*/
			String endDateTime = MapUtils.getString(obj, "endDateTime", ""); // 查询的起始时间
			int orderType = MapUtils.getIntValue(obj, "orderType", 1); // 1、a.StartTime DESC；2、a.StartTime ASC
			String orderStr = " a.Start_Time DESC, a.ID DESC ";
			int shareQueryFlag = MapUtils.getIntValue(obj, "shareQueryFlag", 0);
			int isEnd = MapUtils.getIntValue(obj, "isEnd", -2);
			int additionalFlag = MapUtils.getIntValue(obj, "additionalFlag", 0);
			if (isEnd == 11) {
				orderType = 2;
			}
			long categoryId = MapUtils.getLongValue(obj, "categoryId", 0);
			int searchType = MapUtils.getIntValue(obj, "searchType", 0);

			long activityId = MapUtils.getLongValue(obj, "activityId", 0);
			int queryFlag = MapUtils.getIntValue(obj, "queryFlag", 0);
			int innerCode = MapUtils.getIntValue(obj, "innerCode", 0);
			long combId = MapUtils.getLongValue(obj, "combId", 0);
			String condition = MapUtils.getString(obj, "condition");

			switch (searchType) {
				case 22:
					activityType = MeixConstants.CHAT_MESSAGE_DHHY;
					break;
				case 14:
					activityType = MeixConstants.CHAT_MESSAGE_DY;
					break;
				case 15:
					activityType = 0;
					activityRange = 4;
					break;
			}

			switch (orderType) {
				case 2:
					orderStr = " a.Start_Time ASC, a.ID ASC ";
					break;
			}
			long companyCode = MapUtils.getLongValue(obj, "companyCode", 0);
			//行业代码(1.3.4变成内码)
			JSONArray industry = new JSONArray();
			JSONArray industry2 = new JSONArray();
			if (obj.containsKey("industry")) {
				industry2 = obj.getJSONArray("industry");
			}
			searchVo.setUid(uid);
			searchVo.setStockRange(stockRange);
			searchVo.setActivityType(activityType);
			searchVo.setActivityRange(activityRange);
			searchVo.setShowNum(showNum);
			searchVo.setOrderStr(orderStr);

			searchVo.setCurrentPage(currentPage * showNum);
			searchVo.setShareType(getShareType(uid));
			searchVo.setCompanyCode(companyCode);

			searchVo.setAccountType(getAccountType(uid));
			searchVo.setSearchType(searchType);
			searchVo.setShareQueryFlag(shareQueryFlag);
			searchVo.setIsEnd(isEnd);
			searchVo.setCategoryId(categoryId);
			searchVo.setAdditionalFlag(additionalFlag);
			searchVo.setCondition(StringUtil.replaceMysqlWildcard(condition));
			//日期的处理
			if (startDateTime.length() == 10) {
				startDateTime += " 00:00:00";
			}
			if (endDateTime.length() == 10) {
				endDateTime += " 23:59:00";
			}
			if (startDateTime.length() == 16) {
				startDateTime += ":00";
			}
			if (endDateTime.length() == 16) {
				endDateTime += ":00";
			}
			searchVo.setStartDateTime(startDateTime);
			searchVo.setEndDateTime(endDateTime);
			//城市
			if (obj.containsKey("city")) {
				if (!org.apache.commons.lang.StringUtils.isEmpty(obj.getString("city"))) {
					searchVo.setCity(StringUtil.replaceMysqlWildcard(obj.getString("city")));
				}
			}
			JSONArray citys = new JSONArray();
			if (obj.containsKey("cityList")) {
				citys = obj.getJSONArray("cityList");
			}
			searchVo.setActivityId(activityId);
			searchVo.setQueryFlag(queryFlag);

			//自定义查询，如果不是第一次请求，就返回空列表，防止显示重复数据
			String customQuery = searchVo.getCustomQuery();
			if (!TextUtils.isEmpty(customQuery) && activityId > 0) {
				info.setData(new ArrayList<ActivityVo>());
				info.setMessageCode(MeixConstantCode.M_1008);
				return info;
			}
			long version = RequestUtil.getVersionValue(request);
			JSONArray analystList = null;
			if (obj.containsKey("analystList")) {
				analystList = obj.getJSONArray("analystList");
				if (analystList.size() > 0) {
					searchVo.setAnalystList(analystList);
				}
			}
			UserInfo userInfo = getUserInfo(uid);
			if (userInfo != null) {
				searchVo.setMobile(userInfo.getMobile());
				if (StringUtil.isNotEmpty(userInfo.getInsUuid())) {
					InsUser insUser = insUserService.getUserInfo(userInfo.getInsUuid());
					if (insUser != null) {
						searchVo.setInsUid(insUser.getId());
					}
				}
				// 研究所项目：本公司用户没检验权限   by Xueyc  at 2019年8月23日13:56:57
				if (StringUtils.equals(clientTypeStr, String.valueOf(MeixConstants.CLIENT_TYPE_2)) ||
					StringUtils.equals(clientTypeStr, String.valueOf(MeixConstants.CLIENT_TYPE_3))) {
					if (userInfo.getAccountType() == 1 && userInfo.getCompanyCode() == companyCode) {
						searchVo.setCheckPermission(0);
					}
				}
			}
			List<ActivityVo> list = activityServiceImpl.getActivityList(searchVo, stockRange, industry, industry2, version, citys, combId, innerCode);
			info.setData(list);
			info.setMessageCode(MeixConstantCode.M_1008);
			try {
				if (StringUtil.isNotEmpty(condition) && list != null && list.size() > 0) {
					List<UserRecordStateVo> searchList = new ArrayList<>(list.size());
					for (ActivityVo item : list) {
						// 推送给每市
						UserRecordStateVo vo = new UserRecordStateVo();
						vo.setDataState(MeixConstants.RECORD_SEARCH);
						vo.setUid(uid);
						vo.setDataId(item.getId());
						vo.setDataType(MeixConstants.USER_HD);
						searchList.add(vo);
					}
					MeixConstants.fixedThreadPool.execute(new SendPersonaInfoToMeixThread(searchList));
				}
			} catch (Exception e) {
				log.error("", e);
			}
		} catch (Exception e) {
			info.setMessage("获取活动列表失败");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}

		return info;
	}

	/**
	 * @Description: 活动详情
	 */
	@RequestMapping(value = "/getActivityDetail", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getActivityDetail(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String openId = MapUtils.getString(obj, "openId");
			long activityId = MapUtils.getLong(obj, "activityId");
			InsActivityVo vo = activityServiceImpl.getActivityDetail(uid, activityId, 1);
			info.setObject(vo);
			if (uid > 0 && vo != null && vo.getPermission() == 1) {
				//记录阅读数
				saveUserReadRecordState(uid, activityId, MeixConstants.USER_HD, openId);
			}
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}

	/**
	 * @Description: 活动播放(防止页面拦截)
	 */
	@RequestMapping(value = "/palyActivity", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo palyActivity(HttpServletRequest request) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long uid = MapUtils.getLongValue(obj, "uid", 0);
//			String openId = MapUtils.getString(obj, "openId");
			long activityId = MapUtils.getLong(obj, "activityId");
			InsActivityVo vo = activityServiceImpl.getActivityDetail(uid, activityId, 1);
			//权限认证
			if (uid > 0 && vo != null && vo.getPermission() == 1) {
				//记录阅读数
				saveUserReadRecordState(uid, activityId, MeixConstants.USER_HD, null);
			}
			info.setObject(vo);
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}
		return info;
	}

	/**
	 * @Description: 电话会议：预约， 非电话会议：报名
	 */
	@RequestMapping(value = "/joinActivity", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo joinActivity(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long activityId = MapUtils.getLongValue(obj, "activityId", 0);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			UserInfo user = getUserInfo(uid);
			if (user == null) {
				info.setMessage("您的账号信息错误，uid：" + uid);
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}
			String userName = user.getUserName();
			String mobile = user.getMobile();
			String companyName = user.getCompanyAbbr();
			String position = user.getPosition();
			activityServiceImpl.saveJoinActivity(uid, activityId, userName, mobile, companyName, position);

			// 推送给每市
			UserRecordStateVo vo = new UserRecordStateVo();
			vo.setDataState(MeixConstants.RECORD_JOIN_ACTIVITY);
			vo.setUid(uid);
			vo.setDataId(activityId);
			vo.setDataType(MeixConstants.USER_HD);
			MeixConstants.fixedThreadPool.execute(new SendPersonaInfoToMeixThread(Collections.singletonList(vo)));
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("报名失败");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.error("", e);
		}

		return info;
	}

	/**
	 * @Description: 电话会议收听人员
	 */
	@RequestMapping(value = "/attendActivity", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo attendActivity(HttpServletRequest request, HttpServletResponse response) {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long activityId = MapUtils.getLongValue(obj, "activityId", 0);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			String user = MapUtils.getString(obj, "uuid");

			activityServiceImpl.attendActivity(uid, activityId, user);

			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			info.setMessage("参加活动失败");
			info.setMessageCode(MeixConstantCode.M_1009);
			log.warn("", e);
		}
		return info;
	}
}
