package com.meix.institute.controller;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meix.institute.api.IInstituteService;
import com.meix.institute.api.IUserService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.util.HttpUtil;
import com.meix.institute.util.StringUtil;
import com.meix.institute.util.WechatUtil;
import com.meix.institute.vo.company.CompanyWechatConfig;
import com.meix.institute.vo.user.UserInfo;
import com.meix.institute.vo.wechat.SmallProgResult;

/**
 * Created by zenghao on 2019/9/27.
 */
@Controller
@RequestMapping("/wechat")
public class WechatController {
	private final static Logger log = LoggerFactory.getLogger(WechatController.class);

	@Autowired
	private IInstituteService instituteService;
	@Autowired
	private IUserService userServiceImpl;


	/**
	 * 获取小程序码
	 *
	 * @param requset
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getAcode", method = {RequestMethod.GET})
	@ResponseBody
	public JSONObject getAcode(HttpServletRequest requset, HttpServletResponse response,
	                           @RequestParam(required = true) String appId,
	                           @RequestParam(required = false) String page,
	                           @RequestParam(required = false) String scene,
	                           @RequestParam(required = true) Integer width
	) {
		JSONObject resp = new JSONObject();
		try {
			if (appId == null || width == null) {
				resp.put("message", "缺少参数");
				resp.put("messageCode", 1009);
				return resp;
			}
			long companyCode = Integer.parseInt(appId);
			CompanyWechatConfig config = instituteService.getSPConfigByCompanyCode(companyCode);
			if (config == null) {
				resp.put("message", "无效的appId");
				resp.put("messageCode", 1009);
				return resp;
			}
			String accessToken = WechatUtil.updateAccessToken(config.getCompanyCode(), config.getAppId(), config.getAppSecret(), true);
			if (StringUtil.isBlank(accessToken)) {
				resp.put("message", "获取accessToken失败");
				resp.put("messageCode", 1009);
				return resp;
			}
			log.info("【accessToken】:{}，page：{}，scene：{}，width：{}", accessToken, page, scene, width);
			String base64Acode = getWXAcode(accessToken, page, scene, width);
			resp.put("messageCode", 1008);
			resp.put("acode", base64Acode);
		} catch (Exception e) {
			log.error("【微信分享SDK接入接口调用失败】");
			resp.put("message", "获取小程序码失败");
			resp.put("messageCode", 1009);
		}
		return resp;
	}

	/**
	 * 获取小程序码
	 *
	 * @param access_token
	 * @param page         扫码后需要跳转的页面
	 * @param scene        携带的参数
	 * @param width        二维码的宽度
	 * @return
	 */
	public String getWXAcode(String access_token, String page, String scene, int width) {
		String acodeUrl = WechatUtil.getAcodeUrl(access_token);  // 获取小程序码的接口头部
		JSONObject requestParam = new JSONObject();  // 小程序的参数可查看官方文档
		requestParam.put("page", page);
		requestParam.put("scene", scene);
		requestParam.put("width", width);
		String param = requestParam.toString();

		HttpURLConnection conn = null;
		BufferedReader bufferedReader = null;
		PrintWriter out = null;
		InputStream in = null;
		ByteArrayOutputStream bos = null;
		String base64String = "";
		try {
			URL realUrl = new URL(acodeUrl);
			// 打开和URL之间的连接
			conn = (HttpURLConnection) realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 超时设置,防止 网络异常的情况下,可能会导致程序僵死而不继续往下执行
			conn.setConnectTimeout(3000);
			conn.setReadTimeout(3000);
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();

			in = conn.getInputStream();  // 得到图片的二进制内容
			int leng = in.available();  // 获取二进制流的长度，该方法不准确
			if (leng < 1000) {  // 出现错误时，获取字符长度就一百不到，图片的话有几万的长度
				// 定义BufferedReader输入流来读取URL的响应
				bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
				String line;
				String result = "";
				while ((line = bufferedReader.readLine()) != null) {
					result += line;
				}
				log.warn("获取小程序码出错：" + result);
				return result;
			}

			// 修改图片的分辨率,分辨率太大打印纸不够大
			//BufferedInputStream in2 = new BufferedInputStream(conn.getInputStream());
			// 将文件二进制流修改为图片流
			Image srcImg = ImageIO.read(in);
			// 构建图片流
			BufferedImage buffImg = new BufferedImage(width, width, BufferedImage.TYPE_INT_RGB);
			//绘制改变尺寸后的图
			buffImg.getGraphics().drawImage(srcImg.getScaledInstance(width, width, Image.SCALE_SMOOTH), 0, 0, null);
			// 将图片流修改为文件二进制流
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(buffImg, "png", os);
			in = new ByteArrayInputStream(os.toByteArray());
			// 刷新，将重置为类似于首次创建时的状态
			buffImg.flush();
			srcImg.flush();
			// 设null是告诉jvm此资源可以回收
			buffImg = null;  // 该io流不存在关闭函数
			srcImg = null;  // 该io流不存在关闭函数
			os.close();

			bos = new ByteArrayOutputStream();
			byte[] b1 = new byte[1024];
			int len = -1;
			while ((len = in.read(b1)) != -1) {
				bos.write(b1, 0, len);
			}
			byte[] fileByte = bos.toByteArray();  // 转换为字节数组，方便转换成base64编码

			base64String = Base64.encodeBase64String(fileByte);  // import org.apache.commons.codec.binary.Base64;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		//使用finally块来关闭输出流、输入流
		finally {
			try {
				if (bufferedReader != null) {
					bufferedReader.close();
				}
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
				if (bos != null) {
					bos.close();
				}
				if (conn != null) {
					conn.disconnect();
					conn = null;
				}
				//让系统回收资源，但不一定是回收刚才设成null的资源，可能是回收其他没用的资源。
				System.gc();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return base64String;  // 将base64格式的图片发送到前端
	}


	/**
	 * 微信分享SDK接入
	 *
	 * @param requset
	 * @param response
	 * @return
	 * @author Xueyc
	 * @time 2018-12-25 09:19:34
	 */
	@RequestMapping(value = "/share", method = {RequestMethod.POST})
	@ResponseBody
	public JSONObject share(HttpServletRequest requset, HttpServletResponse response) {
		JSONObject resp = null;
		try {
			//参数准备
			String accessToken = null;
			String jsapiTicket = null;
			String url = requset.getParameter("url");
			String appType = requset.getParameter("appId");
			CompanyWechatConfig config = instituteService.getWechatConfigByAppType(appType);
			if (config == null) {
				resp = new JSONObject();
				resp.put("messageCode", 1009);
				return resp;
			}
			accessToken = WechatUtil.updateAccessToken(config.getCompanyCode(), config.getAppId(), config.getAppSecret(), true);
			jsapiTicket = WechatUtil.updateJsapiTicket(config.getCompanyCode(), config.getAppId(), accessToken, true);
			log.info("【accessToken】=================================" + accessToken +
				"【jsapiTicket】=================================" + jsapiTicket);
			Map<String, String> sign = WechatUtil.sign(jsapiTicket, url);
			sign.put("appId", config.getAppId());
			resp = JSONObject.fromObject(sign);
			resp.put("messageCode", 1008);
			log.info("微信分享SDK参数信息:{}", sign.toString());
		} catch (Exception e) {
			log.error("【微信分享SDK接入接口调用失败】");
			resp = new JSONObject();
			resp.put("messageCode", 1009);
		}
		return resp;
	}

	/**
	 * 获取openId
	 *
	 * @author likuan
	 * @time 2019年7月10日09:05:23
	 */
	@RequestMapping(value = "/getOpenIdByAppId")
	@ResponseBody
	private Object getOpenIdByAppId(HttpServletRequest request, HttpServletResponse response) {
		int clientType = MeixConstants.CLIENT_TYPE_2;//默认公众号
		String clientTypeStr = request.getHeader("clientType");
		if (!StringUtil.isBlank(clientTypeStr)) {
			try {
				clientType = Integer.parseInt(clientTypeStr);
			} catch (Exception e) {
				log.error("", e);
			}
		}
		if (clientType == MeixConstants.CLIENT_TYPE_3) {
			return getSmallProgramOpenId(request, response);
		} else {
			return getOpenId(request, response, clientType);
		}
	}

	/**
	 * 获取微信用户的openid
	 * 自动识别是否是微信浏览器内访问页面
	 *
	 * @throws IOException
	 * @author likuan
	 * @since 2019年7月11日14:48:01
	 */
	private JSONObject getOpenId(HttpServletRequest request, HttpServletResponse response, int clientType) {
		JSONObject resp = new JSONObject();
		String openId = null;
		String accessToken;
		// 优先从cookie获取
		Cookie[] cookies = request.getCookies();
		if (cookies != null && cookies.length > 0) {
			for (Cookie cookie : cookies) {
				if (StringUtils.equals(cookie.getName(), "openId")) {
					openId = cookie.getValue();
					break;
				}
			}
		}
		if (StringUtils.isNotBlank(openId)) {
			resp.put("openId", openId);
			resp.put("messageCode", 1008);
			return resp;
		}

		String code = request.getParameter("code");
		String appId = request.getParameter("appId");
		CompanyWechatConfig config = instituteService.getWechatConfigByAppType(appId);
		if (config == null) {
			resp.put("messageCode", 1009);
			resp.put("message", "appId无效");
			return resp;
		}
		String appSecret = config.getAppSecret();
		if (StringUtils.isBlank(code)) {
			String state = "STATE";// 可以使用公司code
			String scope = "snsapi_userinfo";// 授权提醒，弹框（snsapi_userinfo）/不弹框（snsapi_base）
			String redirectUri = request.getParameter("redirectUri");//重定向url
			if (StringUtils.isBlank(redirectUri)) {
				resp.put("messageCode", 1009);
				resp.put("message", "redirectUri为空");
				return resp;
//				redirectUri = String.format("%s?v=%s&%s", redirectUri, System.currentTimeMillis() ,request.getQueryString());
			}
			try {
				// redirectUri = "http://192.168.10.136:8080/iaweb/mobile/filterWechat?params=eyJyZXF1ZXN0VHlwZSI6IjEifQ==";
				String authUrl = WechatUtil.authorize(config.getAppId(), redirectUri, state, scope);
				log.info("微信重定向接口【authorUrl】====================>{}", authUrl);
//				response.sendRedirect(authUrl);
				resp.put("authUrl", authUrl);
				return resp;
			} catch (Exception e) {
				log.error("微信重定向异常:", e);
			}
		} else {
			try {
				String access_token_url = WechatUtil.accessToken(config.getAppId(), appSecret, code);
				JSONObject result = HttpUtil.getWechat(access_token_url);
				if (result == null || result.containsKey("errcode")) {
					log.warn(
						"【获取openid报错】==========================>access_token_url:{},result:{}",
						access_token_url, result);
					if (result != null && result.containsKey("errcode")) {
						int errcode = result.getInt("errcode");
						if (errcode == 40029 || errcode == 40163) {
							log.warn("【wechat authorize code invalid, try it again.】");
							resp.put("messageCode", 1009);
							resp.put("message", "authorize code invalid, 请重新授权");
							return resp;
						}
					}
				} else {
					log.info("【获取openId微信认证成功】=================================>{}", result);
					openId = MapUtils.getString(result, "openid", null);

					accessToken = MapUtils.getString(result, "access_token", null);

					//获取用户信息
					String userInfoUrl = WechatUtil.getUserinfoUrl(accessToken, openId, null);
					JSONObject obj = HttpUtil.getWechat(userInfoUrl);
					log.info("【获取微信信息==============================>{}】", obj.toString());
					//obj == null, 则value = null
					resp.put("userName", MapUtils.getString(obj, "nickname", null));
					resp.put("gender", MapUtils.getIntValue(obj, "sex", 0));
					resp.put("headImageUrl", MapUtils.getString(obj, "headimgurl", null));
					if (StringUtils.isNotBlank(openId)) {
						@SuppressWarnings("rawtypes")
						Map uidMap = userServiceImpl.getUidByOpenId(openId, clientType);
						long uid = MapUtils.getLongValue(uidMap, "uid", 0);
						if (uid > 0) {
							UserInfo userInfo = new UserInfo();
							userInfo.setId(uid);
							userInfo.setNickName(MapUtils.getString(obj, "nickname", null));
							userInfo.setWechatHeadUrl(MapUtils.getString(obj, "headimgurl", null));
							userServiceImpl.updateWechatInfo(userInfo);
						}
					}
					// 添加到cookie
					Cookie openIdCookie = new Cookie("openId", openId);
					openIdCookie.setMaxAge(30 * 24 * 60 * 60); // 默认一个月
					openIdCookie.setPath("/");
					response.addCookie(openIdCookie);
				}
				resp.put("openId", openId);
				resp.put("appId", config.getAppId());
				log.info("【getOpenId】成功=================================>{}", openId);
			} catch (Exception e) {
				resp.put("messageCode", 1009);
				log.error("请求接口【getOpenId】异常：", e);
			}
		}
		return resp;
	}

	private JSONObject getSmallProgramOpenId(HttpServletRequest request, HttpServletResponse response) {
		JSONObject resp = new JSONObject();
		String appId = request.getParameter("appId");
		String code = request.getParameter("code");
		long companyCode = 0;
		try {
			companyCode = Long.parseLong(appId);
		} catch (Exception e) {
			log.error("", e);
		}
		CompanyWechatConfig config = instituteService.getSPConfigByCompanyCode(companyCode);
		if (config == null) {
			resp.put("messageCode", 1009);
			resp.put("message", "appId无效");
			return resp;
		}
		SmallProgResult smallProgResult = WechatUtil.getSmallProgramOpenId(config.getAppId(), config.getAppSecret(), code);
		if (smallProgResult == null) {
			resp.put("messageCode", 1009);
			resp.put("message", "获取微信用户信息失败");
			return resp;
		}
		if (smallProgResult.getErrcode() != 0) {
			resp.put("messageCode", 1009);
			resp.put("message", smallProgResult.getErrmsg());
			return resp;
		}
		resp.put("messageCode", MeixConstantCode.M_1008);
		resp.put("openId", smallProgResult.getOpenid());
		resp.put("appId", config.getAppId());
		return resp;
	}
}
