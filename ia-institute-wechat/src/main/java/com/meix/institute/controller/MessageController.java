package com.meix.institute.controller;

import com.meix.institute.BaseService;
import com.meix.institute.api.IMessageService;
import com.meix.institute.constant.MeixConstantCode;
import com.meix.institute.constant.MeixConstants;
import com.meix.institute.message.MessageInfo;
import com.meix.institute.response.MeetingMessagePo;
import com.meix.institute.util.*;
import com.meix.institute.vo.message.MeetingMessageVo;
import com.meix.institute.vo.user.UserInfo;
import com.meix.institute.websocket.WebSocketHandlerImpl;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zenghao on 2019/9/26.
 */
@RestController
public class MessageController extends BaseService {
	private static final Logger log = LoggerFactory.getLogger(MessageController.class);

	@Autowired
	private IMessageService messageServiceImpl;
	@Autowired
	private WebSocketHandlerImpl webSocketHandlerImpl;

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * @Title: sendMeetingMessage、
	 * @Description:发送会议留言
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/messageView/sendMeetingMessage", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo sendMeetingMessage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);
			long version = RequestUtil.getVersionValue(request);

			MeetingMessageVo mmv = new MeetingMessageVo();
			int messageType = MapUtils.getIntValue(obj, "messageType", 0);
			int connectType = MapUtils.getIntValue(obj, "connectType", 0);
			long activityId = MapUtils.getLongValue(obj, "activityId", 0);
			long uid = MapUtils.getLongValue(obj, "uid", 0);
			int checkFlag = MapUtils.getIntValue(obj, "checkFlag", 1);
			String message = MapUtils.getString(obj, "message", "");
			String resourceUrl = MapUtils.getString(obj, "resourceUrl", "");
			long messageId = MapUtils.getLongValue(obj, "messageId", 0);
			long newMessageId = 0;
			if (connectType == 0) {
				if (messageType == 0) {
					info.setMessageCode(MeixConstantCode.M_1009);
					info.setMessage("不支持的数据类型！");
					return info;
				}

				int accountType = getAccountType(uid);
				if ((accountType != MeixConstants.ACCOUNT_INNER &&
					accountType != MeixConstants.ACCOUNT_WL &&
					accountType != MeixConstants.ACCOUNT_NORMAL) && checkFlag == 1) {
					info.setMessageCode(MeixConstantCode.M_1009);
					info.setMessage("游客不能发言！");
					return info;
				}

				mmv.setSender(uid);
				mmv.setActivityId(activityId);
				mmv.setMessage(message);
				//多媒体文件的服务器地址
				mmv.setResourceUrl(resourceUrl);
				mmv.setCreateTime(DateUtil.getCurrentDateTime());
				//消息类型
				mmv.setMessageType(messageType);
				//base64处理
				mmv.setMessage(Des.encryptBASE64(mmv.getMessage()));

				//保存会议消息
				Map<String, Object> messageMap = messageServiceImpl.saveMeetingMessage(mmv);
				newMessageId = (long) messageMap.get("id");

				//获取聊天信息
				if (messageId == 0) {
					messageId = newMessageId - 1;
				}
				List<MeetingMessagePo> data = messageServiceImpl.getMeetingMessages(uid, activityId, -1, messageId, 1, version, -1);

				info.setData(data);
				info.setDataCount(data.size());
				info.setMessageCode(MeixConstantCode.M_1008);
				Map<String, Object> result = new HashMap<>();
				result.put("messageId", newMessageId);
				result.put("createTime", mmv.getCreateTime());
				info.setObject(result);

			}

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("id", newMessageId);
			jsonObj.put("message", message);
			jsonObj.put("resourceUrl", resourceUrl);
			jsonObj.put("sender", uid);
			jsonObj.put("createTime", mmv.getCreateTime());
			jsonObj.put("isDeleted", 0);
			jsonObj.put("messageType", mmv.getMessageType());
			jsonObj.put("mediaTime", mmv.getMediaTime());
			jsonObj.put("connectType", connectType);
			UserInfo uic = getUserInfo(uid);
			if (uic != null) {
				jsonObj.put("senderName", uic.getUserName());
				String headImage = uic.getWechatHeadUrl();
				if (StringUtil.isBlank(headImage)) {
					headImage = OssResourceUtil.getHeadImage(uic.getHeadImageUrl());
				}
				jsonObj.put("senderImageUrl", headImage);
				jsonObj.put("senderCompany", uic.getCompanyAbbr());
			}
			webSocketHandlerImpl.sendMessageToActivity(uid, activityId, jsonObj.toString());
		} catch (Exception e) {
			String errorMsg = "发送会议留言失败";
			log.error(errorMsg, e);
			info.setMessage(errorMsg);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * @Title: getMeetingMessages、
	 * @Description:获取会议留言
	 * @return: MessageInfo
	 */
	@RequestMapping(value = "/messageView/getMeetingMessages", method = RequestMethod.POST)
	@ResponseBody
	public MessageInfo getMeetingMessages(HttpServletRequest request, HttpServletResponse response) throws Exception {
		MessageInfo info = new MessageInfo(request);
		JSONObject obj = null;
		try {
			obj = MobileassistantUtil.getRequestParams(request);

			long uid = MapUtils.getLongValue(obj, "uid", 0);
			long speakerId = MapUtils.getLongValue(obj, "speakerId", -1);
			long activityId = MapUtils.getLongValue(obj, "activityId", -1);
			long messageId = MapUtils.getLongValue(obj, "messageId", 0);
			int messageFlag = MapUtils.getIntValue(obj, "messageFlag", 1);
			int showNum = MapUtils.getIntValue(obj, "showNum", -1);
			long version = RequestUtil.getVersionValue(request);

			if (activityId == -1 && speakerId == -1) {
				info.setMessageCode(MeixConstantCode.M_1009);
				return info;
			}

			List<MeetingMessagePo> data = messageServiceImpl.getMeetingMessages(
				uid, activityId, speakerId, messageId, messageFlag, version, showNum);
			info.setData(data);
			info.setDataCount(data.size());
			info.setMessageCode(MeixConstantCode.M_1008);
		} catch (Exception e) {
			String errorMsg = "获取消息列表失败";
			log.error(errorMsg, e);
			info.setMessage(errorMsg);
			info.setMessageCode(MeixConstantCode.M_1009);
		}
		return info;
	}
}
