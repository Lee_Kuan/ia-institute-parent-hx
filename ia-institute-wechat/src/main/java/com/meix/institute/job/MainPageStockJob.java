package com.meix.institute.job;

import com.meix.institute.api.ICompanyMonthlyStockService;
import com.meix.institute.api.IStockHighYieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 首页股票数据定时任务
 * Created by zenghao on 2019/10/10.
 */
@Component
@EnableScheduling
public class MainPageStockJob {

	@Autowired
	private IStockHighYieldService stockHighYieldService;
	@Autowired
	private ICompanyMonthlyStockService companyMonthlyStockService;

	@PostConstruct
	public void loadRecsysObjLabels() {
		stockHighYieldService.refreshCache();
		companyMonthlyStockService.refreshCache();
	}

	@Scheduled(cron = "0 0/10 9-20 * * ?")
	public void refreshStockHighYield() {
		stockHighYieldService.refreshCache();
		companyMonthlyStockService.refreshCache();
	}
}
