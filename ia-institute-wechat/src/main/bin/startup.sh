###启动  
#!/bin/bash
moduleName="ia-institute-wechat"
pidPath="./ia-institute-wechat.pid"
jarfile="../$moduleName.jar"
nohup java -Xms512m -Xmx512m -Xss512k -server -jar $jarfile --spring.profiles.active=$1 > ./run.log 2>&1 &
echo "-----ia-institute-wechat-------"
echo $! > $pidPath
