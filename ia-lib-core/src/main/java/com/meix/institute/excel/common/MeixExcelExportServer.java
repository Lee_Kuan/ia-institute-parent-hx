package com.meix.institute.excel.common;

import cn.afterturn.easypoi.excel.annotation.ExcelEntity;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import cn.afterturn.easypoi.excel.export.base.BaseExportService;
import cn.afterturn.easypoi.excel.export.styler.IExcelExportStyler;
import cn.afterturn.easypoi.exception.excel.ExcelExportException;
import cn.afterturn.easypoi.exception.excel.enums.ExcelExportEnum;
import cn.afterturn.easypoi.util.PoiPublicUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by zenghao on 2019/4/23.
 */
public class MeixExcelExportServer extends BaseExportService {

	private int MAX_NUM = '\uea60';

	public MeixExcelExportServer() {
	}

	protected int createHeaderAndTitle(ExportParams entity, Sheet sheet, Workbook workbook, List<ExcelExportEntity> excelParams, int curIndex) {
		int rows = curIndex;
//		int feildWidth = this.getFieldLength(excelParams);
//		if (entity.getTitle() != null) {
//			rows += this.createHeaderRow(entity, sheet, workbook, feildWidth, rows);
//		}

		if (entity instanceof MeixExportParams) {
			if (((MeixExportParams) entity).isCreateTitleRows()) {
				rows += this.createTitleRow(entity, sheet, workbook, rows, excelParams);
			}
		} else {
			rows += this.createTitleRow(entity, sheet, workbook, rows, excelParams);
		}
		//取消窗口冻结
//		sheet.createFreezePane(0, rows, 0, rows);
		return rows;
	}

	public int createHeaderRow(ExportParams entity, Sheet sheet, Workbook workbook, int feildWidth, int curIndex) {
		Row row = sheet.createRow(curIndex);
		row.setHeight(entity.getTitleHeight());
		this.createStringCell(row, 0, entity.getTitle(), this.getExcelExportStyler().getHeaderStyle(entity.getHeaderColor()), (ExcelExportEntity) null);

		for (int i = 1; i <= feildWidth; ++i) {
			this.createStringCell(row, i, "", this.getExcelExportStyler().getHeaderStyle(entity.getHeaderColor()), (ExcelExportEntity) null);
		}

		sheet.addMergedRegion(new CellRangeAddress(curIndex, curIndex, 0, feildWidth));
		if (entity.getSecondTitle() == null) {
			return 1;
		} else {
			row = sheet.createRow(curIndex + 1);
			row.setHeight(entity.getSecondTitleHeight());
			CellStyle style = workbook.createCellStyle();
			style.setAlignment(HorizontalAlignment.RIGHT);
			this.createStringCell(row, 0, entity.getSecondTitle(), style, (ExcelExportEntity) null);

			for (int i = 1; i <= feildWidth; ++i) {
				this.createStringCell(row, i, "", this.getExcelExportStyler().getHeaderStyle(entity.getHeaderColor()), (ExcelExportEntity) null);
			}

			sheet.addMergedRegion(new CellRangeAddress(curIndex + 1, curIndex + 1, 0, feildWidth));
			return 2;
		}
	}

	public List<ExcelExportEntity> getExcelParams(Class<?> pojoClass) {
		if (pojoClass != null) {
			try {
				List<ExcelExportEntity> excelParams = new ArrayList<ExcelExportEntity>();
				Field[] fileds = PoiPublicUtil.getClassFields(pojoClass);
				ExcelTarget etarget = (ExcelTarget) pojoClass.getAnnotation(ExcelTarget.class);
				String targetId = etarget == null ? null : etarget.value();
				this.getAllExcelField(null, targetId, fileds, excelParams, pojoClass, null, (ExcelEntity) null);
				return excelParams;
			} catch (Exception var9) {
				LOGGER.error(var9.getMessage(), var9);
				LOGGER.error(var9.getMessage(), var9);
				throw new ExcelExportException(ExcelExportEnum.EXPORT_ERROR, var9.getCause());
			}
		} else {
			throw new ExcelExportException(ExcelExportEnum.PARAMETER_ERROR);
		}
	}

	public void createSheet(Workbook workbook, ExportParams entity, Class<?> pojoClass, Collection<?> dataSet) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Excel export start ,class is {}", pojoClass);
			LOGGER.debug("Excel version is {}", entity.getType().equals(ExcelType.HSSF) ? "03" : "07");
		}

		if (workbook != null && entity != null && pojoClass != null && dataSet != null) {
			try {
				List<ExcelExportEntity> excelParams = new ArrayList<ExcelExportEntity>();
				Field[] fileds = PoiPublicUtil.getClassFields(pojoClass);
				ExcelTarget etarget = (ExcelTarget) pojoClass.getAnnotation(ExcelTarget.class);
				String targetId = etarget == null ? null : etarget.value();
				this.getAllExcelField(entity.getExclusions(), targetId, fileds, excelParams, pojoClass, null, (ExcelEntity) null);
				this.createSheetForMap(workbook, entity, excelParams, dataSet);
			} catch (Exception var9) {
				LOGGER.error(var9.getMessage(), var9);
				LOGGER.error(var9.getMessage(), var9);
				throw new ExcelExportException(ExcelExportEnum.EXPORT_ERROR, var9.getCause());
			}
		} else {
			throw new ExcelExportException(ExcelExportEnum.PARAMETER_ERROR);
		}
	}

	public void createSheetForMap(Workbook workbook, ExportParams entity, List<ExcelExportEntity> entityList, Collection<?> dataSet) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Excel version is {}", entity.getType().equals(ExcelType.HSSF) ? "03" : "07");
		}

		if (workbook != null && entity != null && entityList != null && dataSet != null) {
			super.type = entity.getType();
			if (this.type.equals(ExcelType.XSSF)) {
				this.MAX_NUM = 1000000;
			}

			if (entity.getMaxNum() > 0) {
				this.MAX_NUM = entity.getMaxNum();
			}

			Sheet sheet = null;

			try {
				sheet = workbook.createSheet(entity.getSheetName());
			} catch (Exception var7) {
				sheet = workbook.createSheet();
			}

			this.insertDataToSheet(workbook, entity, entityList, dataSet, sheet, 0);
		} else {
			throw new ExcelExportException(ExcelExportEnum.PARAMETER_ERROR);
		}
	}

	public void insertDataToSheet(Workbook workbook, ExportParams entity, Class<?> pojoClass, Collection<?> dataSet) {
		insertDataToSheet(workbook, entity, pojoClass, dataSet, 0);
	}

	public void insertDataToSheet(Workbook workbook, ExportParams entity, Class<?> pojoClass, Collection<?> dataSet, int intervalLine) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Excel export start ,class is {}", pojoClass);
			LOGGER.debug("Excel version is {}", entity.getType().equals(ExcelType.HSSF) ? "03" : "07");
		}

		if (workbook != null && entity != null && pojoClass != null && dataSet != null) {
			try {
				List<ExcelExportEntity> excelParams = getExcelParams(pojoClass);
				Sheet sheet = workbook.getSheet(entity.getSheetName());
				if (sheet == null) {
					createSheetForMap(workbook, entity, excelParams, dataSet);
				} else {
					insertDataToSheet(workbook, entity, excelParams, dataSet, sheet, intervalLine);
				}
			} catch (Exception var9) {
				LOGGER.error(var9.getMessage(), var9);
				throw new ExcelExportException(ExcelExportEnum.EXPORT_ERROR, var9.getCause());
			}
		} else {
			throw new ExcelExportException(ExcelExportEnum.PARAMETER_ERROR);
		}
	}

	@SuppressWarnings("deprecation")
	public void insertDataToSheet(Workbook workbook, ExportParams entity, List<ExcelExportEntity> entityList, Collection<?> dataSet, Sheet sheet, int intervalLine) {
		try {
			this.dataHandler = entity.getDataHandler();
			if (this.dataHandler != null && this.dataHandler.getNeedHandlerFields() != null) {
				this.needHandlerList = Arrays.asList(this.dataHandler.getNeedHandlerFields());
			}

			this.setExcelExportStyler((IExcelExportStyler) entity.getStyle().getConstructor(new Class[]{Workbook.class}).newInstance(new Object[]{workbook}));
			Drawing<?> patriarch = sheet.createDrawingPatriarch();
			List<ExcelExportEntity> excelParams = new ArrayList<>();
			if (entity.isAddIndex()) {
				excelParams.add(this.indexExcelEntity(entity));
			}

			excelParams.addAll(entityList);
			this.sortAllParams(excelParams);
			int curIndex = sheet.getLastRowNum();
			if (curIndex > 0) {//等于0表示是sheet中第一个数据源
				curIndex += 1;
			}
			curIndex += intervalLine;
			int index = entity.isCreateHeadRows() ? this.createHeaderAndTitle(entity, sheet, workbook, excelParams, curIndex) : curIndex;
			this.setCellWith(excelParams, sheet);
			short rowHeight = this.getRowHeight(excelParams);
			this.setCurrentIndex(1);
			Iterator<?> its = dataSet.iterator();
			ArrayList<Object> tempList = new ArrayList<>();

			while (its.hasNext()) {
				Object t = its.next();
				index += this.createCells(patriarch, index, t, excelParams, sheet, workbook, rowHeight, 0)[0];
				tempList.add(t);
				if (index >= this.MAX_NUM) {
					break;
				}
			}

			if (entity.getFreezeCol() != 0) {
				sheet.createFreezePane(entity.getFreezeCol(), 0, entity.getFreezeCol(), 0);
			}

			this.mergeCells(sheet, excelParams, index);
			its = dataSet.iterator();
			int i = 0;

			for (int le = tempList.size(); i < le; ++i) {
				its.next();
				its.remove();
			}

			if (dataSet.size() > 0) {
				this.createSheetForMap(workbook, entity, entityList, dataSet);
			} else {
				this.addStatisticsRow(this.getExcelExportStyler().getStyles(true, (ExcelExportEntity) null), sheet);
			}

		} catch (Exception var15) {
			LOGGER.error(var15.getMessage(), var15);
			throw new ExcelExportException(ExcelExportEnum.EXPORT_ERROR, var15.getCause());
		}
	}

	private int createTitleRow(ExportParams title, Sheet sheet, Workbook workbook, int index, List<ExcelExportEntity> excelParams) {
		Row row = sheet.createRow(index);
		int rows = this.getRowNums(excelParams, true);
		row.setHeight((short) 450);
		Row listRow = null;
		if (rows == 2) {
			listRow = sheet.createRow(index + 1);
			listRow.setHeight((short) 450);
		}

		int cellIndex = 0;
		CellStyle titleStyle = this.getExcelExportStyler().getTitleStyle(title.getColor());
		int i = 0;

		for (int exportFieldTitleSize = excelParams.size(); i < exportFieldTitleSize; ++i) {
			ExcelExportEntity entity = (ExcelExportEntity) excelParams.get(i);
			if (StringUtils.isNotBlank(entity.getName())) {
				this.createStringCell(row, cellIndex, entity.getName(), titleStyle, entity);
			}

			if (entity.getList() == null) {
				if (rows == 2) {
					this.createStringCell(listRow, cellIndex, "", titleStyle, entity);
					sheet.addMergedRegion(new CellRangeAddress(index, index + 1, cellIndex, cellIndex));
				}
			} else {
				List<ExcelExportEntity> sTitel = entity.getList();
				if (StringUtils.isNotBlank(entity.getName())) {
					sheet.addMergedRegion(new CellRangeAddress(index, index, cellIndex, cellIndex + sTitel.size() - 1));
				}

				int j = 0;

				for (int size = sTitel.size(); j < size; ++j) {
					this.createStringCell(rows == 2 ? listRow : row, cellIndex, ((ExcelExportEntity) sTitel.get(j)).getName(), titleStyle, entity);
					++cellIndex;
				}

				--cellIndex;
			}

			++cellIndex;
		}

		return rows;
	}
}
