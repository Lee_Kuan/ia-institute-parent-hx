package com.meix.institute.excel.common;

import cn.afterturn.easypoi.excel.annotation.ExcelEntity;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import cn.afterturn.easypoi.excel.export.styler.IExcelExportStyler;
import cn.afterturn.easypoi.exception.excel.ExcelExportException;
import cn.afterturn.easypoi.exception.excel.enums.ExcelExportEnum;
import cn.afterturn.easypoi.util.PoiExcelGraphDataUtil;
import cn.afterturn.easypoi.util.PoiPublicUtil;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 批量导出大量数据（超过1w），只支持单表格导出
 * Created by zenghao on 2019/11/14.
 */
public class ExcelBatchExportService extends MeixExcelExportServer {

	private static ThreadLocal<ExcelBatchExportService> THREAD_LOCAL = new ThreadLocal<>();
	private Workbook workbook;
	private Sheet sheet;
	private List<ExcelExportEntity> excelParams;
	private ExportParams entity;
	private int titleHeight;
	private Drawing<?> patriarch;
	private short rowHeight;
	private int index;

	public ExcelBatchExportService() {
	}

	public void init(ExportParams entity, Class<?> pojoClass) {
		List<ExcelExportEntity> excelParams = this.createExcelExportEntityList(entity, pojoClass);
		this.init(entity, excelParams);
	}

	public void init(ExportParams entity, List<ExcelExportEntity> excelParams) {
		LOGGER.debug("ExcelBatchExportServer only support SXSSFWorkbook");
		entity.setType(ExcelType.XSSF);
		this.workbook = new SXSSFWorkbook();
		this.entity = entity;
		this.excelParams = excelParams;
		super.type = entity.getType();
		this.createSheet(this.workbook, entity, excelParams);
		if (entity.getMaxNum() == 0) {
			entity.setMaxNum(1000000);
		}

		this.insertDataToSheet(this.workbook, entity, excelParams, (Collection<?>) null, this.sheet);
	}

	public List<ExcelExportEntity> createExcelExportEntityList(ExportParams entity, Class<?> pojoClass) {
		try {
			List<ExcelExportEntity> excelParams = new ArrayList<>();
			if (entity.isAddIndex()) {
				excelParams.add(this.indexExcelEntity(entity));
			}

			Field[] fileds = PoiPublicUtil.getClassFields(pojoClass);
			ExcelTarget etarget = (ExcelTarget) pojoClass.getAnnotation(ExcelTarget.class);
			String targetId = etarget == null ? null : etarget.value();
			this.getAllExcelField(entity.getExclusions(), targetId, fileds, excelParams, pojoClass, null, (ExcelEntity) null);
			this.sortAllParams(excelParams);
			return excelParams;
		} catch (Exception var7) {
			throw new ExcelExportException(ExcelExportEnum.EXPORT_ERROR, var7);
		}
	}

	public void createSheet(Workbook workbook, ExportParams entity, List<ExcelExportEntity> excelParams) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Excel export start ,List<ExcelExportEntity> is {}", excelParams);
			LOGGER.debug("Excel version is {}", entity.getType().equals(ExcelType.HSSF) ? "03" : "07");
		}

		if (workbook != null && entity != null && excelParams != null) {
			try {
				try {
					this.sheet = workbook.createSheet(entity.getSheetName());
				} catch (Exception var5) {
					this.sheet = workbook.createSheet();
				}

			} catch (Exception var6) {
				throw new ExcelExportException(ExcelExportEnum.EXPORT_ERROR, var6);
			}
		} else {
			throw new ExcelExportException(ExcelExportEnum.PARAMETER_ERROR);
		}
	}

	public Workbook appendData(Collection<?> dataSet) {
		if (this.sheet.getLastRowNum() + dataSet.size() > this.entity.getMaxNum()) {
			this.sheet = this.workbook.createSheet();
			this.index = 0;
		}

		Iterator<?> its = dataSet.iterator();

		while (its.hasNext()) {
			Object t = its.next();

			try {
				this.index += this.createCells(this.patriarch, this.index, t, this.excelParams, this.sheet, this.workbook, this.rowHeight, 0)[0];
			} catch (Exception var5) {
				LOGGER.error(var5.getMessage(), var5);
				throw new ExcelExportException(ExcelExportEnum.EXPORT_ERROR, var5);
			}
		}

		return this.workbook;
	}

	protected void insertDataToSheet(Workbook workbook, ExportParams entity, List<ExcelExportEntity> entityList, Collection<?> dataSet, Sheet sheet) {
		try {
			this.dataHandler = entity.getDataHandler();
			if (this.dataHandler != null && this.dataHandler.getNeedHandlerFields() != null) {
				this.needHandlerList = Arrays.asList(this.dataHandler.getNeedHandlerFields());
			}

			this.setExcelExportStyler((IExcelExportStyler) entity.getStyle().getConstructor(new Class[]{Workbook.class}).newInstance(new Object[]{workbook}));
			this.patriarch = PoiExcelGraphDataUtil.getDrawingPatriarch(sheet);
			List<ExcelExportEntity> excelParams = new ArrayList<>();
			if (entity.isAddIndex()) {
				excelParams.add(this.indexExcelEntity(entity));
			}

			excelParams.addAll(entityList);
			this.sortAllParams(excelParams);
			this.index = entity.isCreateHeadRows() ? this.createHeaderAndTitle(entity, sheet, workbook, excelParams, 0) : 0;
			this.titleHeight = this.index;
			this.setCellWith(excelParams, sheet);
			this.rowHeight = this.getRowHeight(excelParams);
			this.setCurrentIndex(1);
		} catch (Exception var7) {
			LOGGER.error(var7.getMessage(), var7);
			throw new ExcelExportException(ExcelExportEnum.EXPORT_ERROR, var7.getCause());
		}
	}

	public static ExcelBatchExportService getExcelBatchExportService(ExportParams entity, Class<?> pojoClass) {
		if (THREAD_LOCAL.get() == null) {
			ExcelBatchExportService batchServer = new ExcelBatchExportService();
			batchServer.init(entity, pojoClass);
			THREAD_LOCAL.set(batchServer);
		}

		return THREAD_LOCAL.get();
	}

	public static ExcelBatchExportService getExcelBatchExportService(ExportParams entity, List<ExcelExportEntity> excelParams) {
		if (THREAD_LOCAL.get() == null) {
			ExcelBatchExportService batchServer = new ExcelBatchExportService();
			batchServer.init(entity, excelParams);
			THREAD_LOCAL.set(batchServer);
		}

		return THREAD_LOCAL.get();
	}

	public static ExcelBatchExportService getCurrentExcelBatchExportService() {
		return THREAD_LOCAL.get();
	}

	@SuppressWarnings("deprecation")
	public void closeExportBigExcel() {
		if (this.entity.getFreezeCol() != 0) {
			this.sheet.createFreezePane(this.entity.getFreezeCol(), 0, this.entity.getFreezeCol(), 0);
		}

		this.mergeCells(this.sheet, this.excelParams, this.titleHeight);
		this.addStatisticsRow(this.getExcelExportStyler().getStyles(true, (ExcelExportEntity) null), this.sheet);
		THREAD_LOCAL.remove();
	}

}
