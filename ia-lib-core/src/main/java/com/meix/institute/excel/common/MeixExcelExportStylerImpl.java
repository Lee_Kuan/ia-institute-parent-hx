package com.meix.institute.excel.common;

import cn.afterturn.easypoi.excel.export.styler.AbstractExcelExportStyler;
import org.apache.poi.ss.usermodel.*;

/**
 * Created by zenghao on 2019/4/25.
 */
public class MeixExcelExportStylerImpl extends AbstractExcelExportStyler {
	public MeixExcelExportStylerImpl(Workbook workbook) {
		super.createStyles(workbook);
	}

	public CellStyle getTitleStyle(short color) {
		CellStyle titleStyle = this.workbook.createCellStyle();
		setBorderStyle(titleStyle);
		titleStyle.setFillForegroundColor(color);
		titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		titleStyle.setAlignment(HorizontalAlignment.CENTER);
		titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		titleStyle.setWrapText(true);
		return titleStyle;
	}

	public CellStyle stringSeptailStyle(Workbook workbook, boolean isWarp) {
		CellStyle style = workbook.createCellStyle();
		setBorderStyle(style);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setDataFormat(STRING_FORMAT);
		if (isWarp) {
			style.setWrapText(true);
		}

		return style;
	}

	public CellStyle getHeaderStyle(short color) {
		CellStyle titleStyle = this.workbook.createCellStyle();
		setBorderStyle(titleStyle);
		Font font = this.workbook.createFont();
		font.setFontHeightInPoints((short) 12);
		titleStyle.setFont(font);
		titleStyle.setFillForegroundColor(color);
		titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		titleStyle.setAlignment(HorizontalAlignment.CENTER);
		titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		return titleStyle;
	}

	public CellStyle stringNoneStyle(Workbook workbook, boolean isWarp) {
		CellStyle style = workbook.createCellStyle();
		setBorderStyle(style);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setDataFormat(STRING_FORMAT);
		if (isWarp) {
			style.setWrapText(true);
		}

		return style;
	}

	private void setBorderStyle(CellStyle style) {
		//下边框
		style.setBorderBottom(BorderStyle.THIN);
		//左边框
		style.setBorderLeft(BorderStyle.THIN);
		//上边框
		style.setBorderTop(BorderStyle.THIN);
		//右边框
		style.setBorderRight(BorderStyle.THIN);
	}
}
