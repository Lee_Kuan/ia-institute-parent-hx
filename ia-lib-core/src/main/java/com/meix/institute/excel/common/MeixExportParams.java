package com.meix.institute.excel.common;

import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import org.apache.poi.ss.usermodel.IndexedColors;

/**
 * Created by yingw on 2019/4/25.
 */
public class MeixExportParams extends ExportParams {

	private boolean isCreateTitleRows = true;

	public MeixExportParams() {
		super();
		setMeixExcelStyle();
	}

	public MeixExportParams(String title, String sheetName) {
		super(title, sheetName);
		setMeixExcelStyle();
	}

	public MeixExportParams(String title, String sheetName, boolean isCreateTitleRows) {
		super(title, sheetName);
		this.isCreateTitleRows = isCreateTitleRows;
		setMeixExcelStyle();
	}

	public MeixExportParams(String title, String sheetName, boolean isCreateTitleRows, ExcelType excelType) {
		super(title, sheetName);
		this.isCreateTitleRows = isCreateTitleRows;
		this.setType(excelType);
		setMeixExcelStyle();
	}

	private void setMeixExcelStyle() {
//		setStyle(ExcelExportStylerDefaultImpl.class);
		setStyle(MeixExcelExportStylerImpl.class);
//		setStyle(ExcelStyleUtil.class);
		setHeaderColor(IndexedColors.LIGHT_TURQUOISE.index);
//		setColor(IndexedColors.LIGHT_TURQUOISE.index);
	}

	public boolean isCreateTitleRows() {
		return isCreateTitleRows;
	}

	public void setCreateTitleRows(boolean createTitleRows) {
		isCreateTitleRows = createTitleRows;
	}
}
