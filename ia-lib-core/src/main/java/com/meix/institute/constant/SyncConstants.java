package com.meix.institute.constant;

/**
 * Created by zenghao on 2020/4/8.
 */
public class SyncConstants {

	public static final String TEMP_TOKEN = "EN3gr0ZTOXrHq#wg7VOtr#jsDlhoxH^e^75vLYfDjgimksWgEoTzCkT1M8Yn2!6*";
	
	public static final String ACTION_SAVE_ACTIVITY = "saveActivity";
	public static final String ACTION_DELETE_ACTIVITY = "deleteActivity";
	public static final String ACTION_UPDATE_ACTIVITY_STATUS = "updateActivityStatus";
	public static final String ACTION_SAVE_GENSEE_VOD_LIST = "saveGenseeVodList";
	public static final String ACTION_DELETE_GENSEE_VOD = "deleteGenseeVod";

	public static final String ACTION_GET_GENSEE_RECORD_LIST = "getGenseeRecordList";
	public static final String ACTION_GET_TELE_JOIN_RECORD_LIST = "getTeleJoinRecordList";
	public static final String ACTION_SAVE_USER_STATE_LIST = "saveUserStateList";

	public static final String ACTION_SAVE_PERSONA_INFO = "savePersonaInfo";
	public static final String ACTION_GET_PERSONA_INFO_LIST = "getPersonaInfoList";
	public static final String ACTION_USER_WL = "saveWhiteList";
	public static final String ACTION_SAVE_REPORT = "saveReport";
	public static final String ACTION_UNDO_RESOURCE = "undoResource";
	public static final String ACTION_UPLOAD_ACTIVITY_FILE = "uploadActivityFile"; //上传录音
}
