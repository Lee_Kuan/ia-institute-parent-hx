package com.meix.institute.constant;
/**
 * @类说明  :客户端消息代码
 */
public class MeixConstantCode {
	//系统代码
	public static final int M_1001 = 1001;//用户未注册
	public static final int M_1002 = 1002;//验证码过期
	public static final int M_1003 = 1003;//登录超时
	public static final int M_1004 = 1004;//网络异常
	public static final int M_1005 = 1005;//手机号与验证码不匹配
	public static final int M_1006 = 1006;//成功登录 （弃用）
	public static final int M_1007 = 1007;//重复的请求
	public static final int M_1008 = 1008;//操作成功
	public static final int M_1009 = 1009;//操作失败
	public static final int M_1010 = 1010;//Token令牌验证失败
	public static final int M_1011 = 1011;//登录成功
	public static final int M_1012 = 1012;//Token令牌为空
	public static final int M_1013 = 1013;//验证码发送成功
	public static final int M_1014 = 1014;//验证码验证成功
	public static final int M_1015 = 1015;//邮箱登陆
	public static final int M_1016 = 1016;//用户信息不存在
	public static final int M_1017 = 1017;//页面验证码发送成功
	public static final int M_1018 = 1018;//客户端验证码发送成功
	public static final int M_1019 = 1019;//短信验证码发送失败
	public static final int M_1020 = 1020;//邮箱验证码发送失败
	public static final int M_1021 = 1021;//扫描二维码失败
	public static final int M_1022 = 1022;//根据二维码获取登陆用户信息
	public static final int M_1023 = 1023;//使用邮箱登陆
	public static final int M_1024 = 1024;//重新获取验证码
	public static final int M_1025 = 1025;//页面验证码错误
	public static final int M_1026 = 1026;//用户已存在
	public static final int M_1027 = 1027;//重置密码失败
	public static final int M_1028 = 1028;//密码错误
	public static final int M_1029 = 1029;//版本更新失败
	public static final int M_1030 = 1030;//修改手机号失败
	public static final int M_1031 = 1031;//验证码不正确
	public static final int M_1032 = 1032;//无权限
	public static final int M_1033 = 1033;//您已被移除群组
	public static final int M_1034 = 1034;//版本过低，请升级新版本!
	public static final int M_1035 = 1035;//组合隐藏，不能授权
	public static final int M_1036 = 1036;//组合属于独家
	public static final int M_1037 = 1037;//账户未激活
	public static final int M_1038 = 1038;//授权给中欧的独家被打破
	public static final int M_1039 = 1039;//账户被冻结
	public static final int M_1040 = 1040;//账户被注销
	public static final int M_1041 = 1041;//该群已解散
	public static final int M_1042 = 1042;//切换设备需要短信登录
	public static final int M_1043 = 1043;//试用账户已到期,需要续费
	public static final int M_1044 = 1044;//账户已到期,需要续费
	public static final int M_1045 = 1045;//绑定每市账户失败
	public static final int M_1046 = 1046;//账户异常
	public static final int M_1050 = 1050;//表示接口可以进行二次调用，配合confirm参数，false表示校验 true表示确定提交
	public static final int M_1051 = 1051;//表示调用外部链接
	public static final int M_1052 = 1052;//重发消息 ，配合confirm参数，false表示校验 true表示确定提交
	public static final int M_1053 = 1053;//重复请求 
	public static final int M_1054 = 1054;//无接口访问权限
	public static final int M_1060 = 1060;//管理员不能登录移动版
	public static final int M_1090 = 1090;//获取音频文件出错
	public static final int M_1091 = 1091;//用户名片识别失败
	public static final int M_1092 = 1092;//活动报名时间已截止
	public static final int M_1093 = 1093;//手机号已注册
	public static final int M_1094 = 1094;//国际短信验证码日条数限制
	public static final int M_1095 = 1095;//用户名不能为空
	public static final int M_1096 = 1096;//密码不能为空
	public static final int M_1097 = 1097;//手机号不能为空
	public static final int M_1098 = 1098;//验证码不能为空
	
}
