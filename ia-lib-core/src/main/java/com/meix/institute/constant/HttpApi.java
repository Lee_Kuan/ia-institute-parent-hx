package com.meix.institute.constant;

/**
 * Created by zenghao on 2019/2/28.
 */
public class HttpApi {

	/**
	 * 证券信息列表同步
	 */
	public static final String SYNC_SECUMAIN_LIST = "sync/getSecumainList.do";
	/**
	 * 热度列表同步
	 */
	public static final String SYNC_HEAT_DATA_LIST = "sync/getHeatDataList.do";
	/**
	 * 常量列表同步
	 */
	public static final String SYNC_CONSTANT_LIST = "sync/getConstantList.do";

	/**
	 * 证券信息列表保存
	 */
	public static final String SAVE_SECUMAIN_LIST = "sync/saveSecumainList.do";
	/**
	 * 热度列表保存
	 */
	public static final String SAVE_HEAT_DATA_LIST = "sync/saveHeatDataList.do";
	/**
	 * 证券信息列表保存
	 */
	public static final String SAVE_CONSTANT_LIST = "sync/saveConstantList.do";

	/**
	 * 首页推荐列表
	 */
	public static final String INDEX_COMPONENT_LIST = "index/getInsMainPageCompList.do";

	/**
	 * 股票区间收益率
	 */
	public static final String SYNC_STOCK_INTERVAL_YIELD = "sync/getStockIntervalYield.do";

	/**
	 * 用户画像发送美市
	 */
	public static final String SYNC_SAVE_PERSONA_UUID = "sync/savePersonaUuidInfo.do";

}
