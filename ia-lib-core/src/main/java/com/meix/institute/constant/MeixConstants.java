package com.meix.institute.constant;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.meix.institute.vo.commen.ConstantVo;


/**
 * @版权信息: 上海睿涉取有限公司
 * @部门 ：开发部
 * @工程名 : 投资终端
 * @作者 ：zhouwei
 * @E-mail : zhouwei@51research.com
 * @创建日期: 2014-6-18下午6:39:55
 */
public class MeixConstants {
	//-------------交易所-------
	public static final String MARKEY_A = "A";//股市场，沪深按照一个市场
	public static final String MARKEY_HK = "HK";//港交所
	public static final String MARKEY_SH = "SH";//上交所市场
	public static final String MARKEY_SZ = "SZ";//深交所市场

	//-------------元件类型------
	public static final int CELL_FIXED = 1;//客户端固定放置的元件
	public static final int CELL_CONFIG = 2;//服务端配置的元件
	public static final int CELL_DATA = 3;//数据块
	public static final int SUB_CELL_HEAD_IMG = 1;//头像
	public static final int SUB_CELL_USER_NAME = 2;//用户名称链接
	public static final int SUB_CELL_COMPANY = 3;//公司链接
	public static final int SUB_CELL_STOCK = 4;//股票
	public static final int SUB_CELL_COMPANY_HEAD_IMG = 5;//机构
	public static final int SUB_CELL_STOCK_NAME = 6;//股票名称

	//-------------股票状态-------
	public static final int STOCK_STATE_SH = 1;//上市
	public static final int STOCK_STATE_ZT = 3;//暂停
	public static final int STOCK_STATE_ZZ = 5;//终止
	public static final int STOCK_STATE_QT = 9;//其他
	public static final int STOCK_STATE_JY = 10;//交易
	public static final int STOCK_STATE_TP = 11;//停牌
	public static final int STOCK_STATE_ZP = 12;//摘牌
	//------------ 交易状态 -----------
	public static final int STOCK_TRADING_STATUS_QT = -1;//其它
	public static final int STOCK_TRADING_STATUS_XS = 0;//休市
	public static final int STOCK_TRADING_STATUS_JJ = 1;//集合竞价
	public static final int STOCK_TRADING_STATUS_JY = 2;//交易
	public static final int STOCK_TRADING_STATUS_SP = 3;//收盘
	public static final int STOCK_TRADING_STATUS_TP = 4;//停牌

	/***************证券种类****************/
	public static final int ZQ_GG = 51;//港股
	public static final int ZQ_ZS = 4;//指数
	public static final int ZQ_JJ = 13;//基金
	public static final String ZQ_JJ_NAME = "基金行业";//基金
	/***************证券市场种类****************/
	public static final int MARKET_GG = 72;//港股
	/***************删除标记***************/
	public static final int DELETE_YES = 1;//删除
	public static final int DELETE_NO = 0;//保留
	//----------记录类型-------
	public static final int USER_GG = -1;//广告
	public static final int USER_TC = 1;//调仓
	public static final int USER_GD = 2;//观点
	public static final int USER_YB = 3;//研报
	public static final int USER_GP = 5;//股票
	public static final int USER_YH = 4;//用户
	public static final int USER_JG = 10;//机构
	public static final int USER_HYJY = 14;//会议纪要
	public static final int USER_HOME_PAGE = 30;//首页
	public static final int USER_APPLET = 31;//小程序总时长
	public static final int PAGE_RESEARCH = 32;//研究页面总时长
	public static final int PAGE_ACTIVITY = 33;//活动页面总时长
	public static final int PAGE_MINE = 34;//我的页面总时长
	/**
	 * 活动
	 */
	public static final int USER_HD = 8;
	public static final int USER_GPC = 10;//被分享的股票池
	public static final int USER_CH = 13;//晨会
	public static final int USER_YJS_WL = 18;//研究所白名单
	public static final int HOME_COMB_LIST = 30;//组合榜单
	public static final int HOME_PERSON_LIST = 31;//人员榜单
	public static final int HOME_TOP_ROTATION = 32;//顶部轮播图
	public static final int USER_MP = 14;//名片
	public static final int USER_CHLY = 17;//晨会留言
	public static final int USER_YBLY = 18;//研报留言
	public static final int USER_ZJ = 19;//专家
	public static final int USER_DHHYFW = 32;//电话会议服务
	public static final int USER_LYFW = 33;//路演服务
	public static final int USER_DYFW = 34;//调研服务
	public static final int USER_YBFW = 35;//研报服务
	public static final int USER_GDFW = 36;//观点服务
	public static final int USER_RTC = 40;//金组合电话会议关联阅读推荐广告
	public static final int USER_JZHDHHY = 41;//金组合电话会议
	public static final int USER_TJ = 42;//阿尔法推荐首页广告
	public static final int USER_TC_TAB = 101;//调仓
	public static final int USER_GD_TAB = 102;//观点
	public static final int USER_YB_TAB = 103;//研报
	public static final int USER_STOCK_GD_TAB = 202;//观点
	public static final int USER_STOCK_YB_TAB = 203;//研报
	public static final int USER_BFX_GPC_TAB = 207;//被分享的股票池
	public static final int USER_COMB_GD_TAB = 302;//观点
	public static final int USER_ACTIVITY_TAB = 401;//活动标签
	public static final int USER_TREND_TAB = 501;//动态标签
	public static final int USER_COMMENT_TAB = 601;//评价标签

	public static final int USER_COMPANY = 701;//公司

	//聊天消息类型
	public static final int CHAT_MESSAGE_TEXT = 1;//文本
	public static final int CHAT_MESSAGE_AUDIO = 2;//语音
	public static final int CHAT_MESSAGE_ZH = 5;//组合
	public static final int CHAT_MESSAGE_GD = 4;//观点
	public static final int CHAT_MESSAGE_TC = 3;//调仓
	public static final int CHAT_MESSAGE_DY = 6;//调研
	public static final int CHAT_MESSAGE_DHHY = 7;//电话会议
	public static final int CHAT_MESSAGE_LY = 8;//路演
	public static final int CHAT_MESSAGE_IMAGE = 9;//图片
	public static final int CHAT_MESSAGE_FILE = 10;//文件
	public static final int CHAT_MESSAGE_CL = 11;//策略
	public static final int CHAT_MESSAGE_HY = 12;//会议
	public static final int CHAT_MESSAGE_HTFW = 20;//合同服务
	public static final int CHAT_MESSAGE_DH = 21;//合同服务
	public static final int CHAT_MESSAGE_PUSH = 22;//推送消息
	public static final int RECORD_READ = 1;//阅读
	public static final int RECORD_PRAISE = 2;//点赞
	public static final int RECORD_FOLLOW = 3;//关注
	public static final int RECORD_REMIND = 4;//提醒
	public static final int RECORD_CLICK = 5;//点击
	public static final int RECORD_PLAY = 6;//播放
	public static final int RECORD_SEARCH = 7;//搜索
	public static final int RECORD_SHARE = 8;//分享
	public static final int RECORD_DURATION = 9;//时长
	public static final int RECORD_ADD_SELF_STOCK = 12;//加自选
	public static final int RECORD_DEL_SELF_STOCK = 13;//取消自选
	public static final int RECORD_JOIN_ACTIVITY = 14;//参加活动
	/* ******推送消息的类型  **********/
	/**
	 * 其他
	 */
	public static final int MESSAGE_TYPE_OTHER = -1;
	/**
	 * 全部
	 */
	public static final int MESSAGE_TYPE_ALL = 0;
	/**
	 * 消息
	 */
	public static final int MESSAGE_TYPE_XX = 1;
	/**
	 * 授权
	 */
	public static final int MESSAGE_TYPE_SQ = 2;
	/**
	 * 调仓
	 */
	public static final int MESSAGE_TYPE_TC = 3;
	/** 关注组合 */
//	public static final int MESSAGE_TYPE_GZZH=4;
	/**
	 * 移出群聊
	 */
	public static final int MESSAGE_TYPE_YCQL = 5;
//	/** 取消独家 */
//	public static final int MESSAGE_TYPE_QXDJ=6;
//	/** 组合转托管 */
//	public static final int MESSAGE_TYPE_ZHZTG=7;
//	/** 仓位报警 */
//	public static final int MESSAGE_TYPE_ZHCWBJ=8;
	/**
	 * 活动变更
	 */
	public static final int MESSAGE_TYPE_HDBG = 9;
	/**
	 * 活动取消
	 */
	public static final int MESSAGE_TYPE_HDQX = 10;
//	/** 鼓励组合 **/
//	public static final int MESSAGE_TYPE_GLZH=11;
//	/** 付费组合 **/
//	public static final int MESSAGE_TYPE_FFZH=12;
//	/** 关注的作者写研报通知 **/
//	public static final int MESSAGE_TYPE_GZYBTZ=22;
//	/** 研报被赞 **/
//	public static final int MESSAGE_TYPE_YBBZ=23;
//	/** 研报被评论 **/
//	public static final int MESSAGE_TYPE_YBPL=24;
//	/** 活动推广 今日热门研报 **/
//	public static final int MESSAGE_TYPE_JRRMYB=25;
//	/** 研报周热榜 **/
//	public static final int MESSAGE_TYPE_YBZRB=26;
//	/** 评论被赞通知 **/
//	public static final int MESSAGE_TYPE_PLBZ=27;
//	/** 评论回复通知 **/
//	public static final int MESSAGE_TYPE_PLHF=28;
//	/** 炫耀通知 **/
//	public static final int MESSAGE_TYPE_XYTZ=29;
//	/** 后悔通知 **/
//	public static final int MESSAGE_TYPE_HHTZ=30;
//	/** 关注通知 **/
//	public static final int MESSAGE_TYPE_GZTZ=31;
//	/** 点评鼓励 **/
//	public static final int MESSAGE_TYPE_DPGL=32;
//	/** 建仓通知 **/
//	public static final int MESSAGE_TYPE_JCTZ=33;
//	/** 仓位限制通知 **/
//	public static final int MESSAGE_TYPE_CWXZ=34;
//	/** 附近的活动 **/
//	public static final int MESSAGE_TYPE_FJHD=35;
//	/** 组合排名变动 **/
//	public static final int MESSAGE_TYPE_ZHPMBD = 36;
//	/** 组合 **/
//	public static final int MESSAGE_TYPE_ZH = 37;
//	/** 组合排名变动(短信通知) **/
//	public static final int MESSAGE_TYPE_ZHPMBD_DX = 38;
	/**
	 * 活动推荐
	 **/
	public static final int MESSAGE_TYPE_HDTJ = 39;
	/**
	 * 电话会议信息通知
	 **/
	public static final int MESSAGE_TYPE_DHHY = 40;
	/**
	 * 撤单成功
	 **/
	public static final int MESSAGE_TYPE_CDCG = 41;
	/**
	 * 共享自选股调入
	 **/
	public static final int MESSAGE_TYPE_GXZXGTR = 42;
	/**
	 * 共享自选股调出
	 **/
	public static final int MESSAGE_TYPE_GXZXGTC = 43;
	/**
	 * 股票池分享
	 **/
	public static final int MESSAGE_TYPE_GPCFX = 44;
	/**
	 * 共享自选股 ：44、 43 和 42 合集
	 **/
	public static final int MESSAGE_TYPE_GXZXG = 45;
	/**
	 * 广告推送
	 **/
	public static final int MESSAGE_TYPE_GGTS = 46;
	/**
	 * 活动消息：9、 10 和 39 集合
	 **/
	public static final int MESSAGE_TYPE_ACTIVITY = 47;
	/**
	 * 用户被关注消息
	 **/
	public static final int MESSAGE_TYPE_BE_SUBSCRIBE = 48;
	/**
	 * 用户创建组合推送关注自己的用户
	 **/
	public static final int MESSAGE_TYPE_PUSH_COMB_2_FOLLOWERS = 49;
	/** 组合被赞/评论 **/
//    public static final int MESSAGE_TYPE_COMB_BE_PRAISE = 50;
	/**
	 * 组合周报
	 **/
	public static final int MESSAGE_TYPE_SIMU_WEEKLY = 51;
	/**
	 * 组合排名上升
	 **/
	public static final int MESSAGE_TYPE_SIMU_RANK_RISE = 52;
	/**
	 * 组合排名下降
	 **/
	public static final int MESSAGE_TYPE_SIMU_RANK_FALL = 53;
	/**
	 * 调仓股票未成交
	 **/
	public static final int MESSAGE_TYPE_SIMU_ORDER = 54;

	/* ****************IOS推送类型*******************/
	/**
	 * 企业版
	 */
	public static final int MESSAGE_IOS_PUSH_QY = 1;
	/**
	 * APPSTORE
	 */
	public static final int MESSAGE_IOS_PUSH_APPSTORE = 2;
	/* ****************ALF_IOS推送类型*******************/
	/**
	 * 企业版
	 */
	public static final int MESSAGE_ALF_IOS_PUSH_QY = 3;
	/**
	 * APPSTORE
	 */
	public static final int MESSAGE_ALF_IOS_PUSH_APPSTORE = 4;
	/* ****************ANDROID推送类型*******************/
	/**
	 * 旧包名
	 */
	public static final int MESSAGE_ANDROID_PUSH_OLD = 1;
	/**
	 * 新包名
	 */
	public static final int MESSAGE_ANDROID_PUSH_NEW = 2;
	/**
	 * 阿尔法包名
	 */
	public static final int MESSAGE_ALF_ANDROID_PUSH_NEW = 3;
	/****************消息通知类型***************/
	public static final int NOTIFICATION_GR = 1;//个人
	public static final int NOTIFICATION_GSLX = 2;//公司类型
	//	public static final int NOTIFICATION_DP_GZ = 3;//点评 关注
//	public static final int NOTIFICATION_DP_XY = 4;//点评 炫耀
//	public static final int NOTIFICATION_DP_HH = 5;//点评后悔
	public static final int NOTIFICATION_DHHY = 6;//电话会议通知
	public static final int NOTIFICATION_ZBJS = 7;//直播结束通知
	//----------------授权状态----------
	/**
	 * 设置普通授权
	 */
	public static final int AUTHORIZE_TYPE_COMMON = 1;
	/**
	 * 设置打赏授权
	 */
	public static final int AUTHORIZE_TYPE_REWARD = 2;
	/**
	 * 保存打赏内容 2、打赏
	 */
	public static final int REWARD_TYPE_DS = 2;
	/**
	 * 保存打赏内容 1、打赏授权
	 */
	public static final int REWARD_TYPE_DSSQ = 1;
	/**
	 * 保存打赏内容 3、付费看组合
	 */
	public static final int REWARD_TYPE_FFKZH = 3;
	/**
	 * 保存打赏内容 4、积分看组合
	 */
	public static final int REWARD_TYPE_JFKZH = 4;
	public static final int AUTHORIZE_REQUEST_YES = 2;//授权通过
	public static final int AUTHORIZE_REQUEST_WAITING = 1;//授权等待
	public static final int AUTHORIZE_REQUEST_ALL = 0;//授权全部状态
	/***********************授权请求类型*********************/
	public static final int AUTHORIZE_REQUEST_TYPE_ZH = 1;//组合
	public static final int AUTHORIZE_REQUEST_TYPE_GP = 2;//股票
	public static final int AUTHORIZE_REQUEST_TYPE_ZHZTG = 3;//组合转托管
	/***********************授权请求身份标志*********************/
	public static final int AUTHORIZE_REQUEST_IDENTITY_SELF = 1;//自己
	public static final int AUTHORIZE_REQUEST_IDENTITY_OTHER = -1;//对方
	public static final int AUTHORIZE_REQUEST_IDENTITY_ALL = 0;//所有
	//------------清算标识-------
	public static final int SETTLE_NO_DO = 0;//未清算
	public static final int SETTLE_YES = 1;//全部成交
	public static final int SETTLE_PART = 2;//部分成交
	public static final int SETTLE_NO = 3;//没有成交
	//-------------指令成交状态--------
	public static final int TC_CASH_LIMIT = 1;//现金限制
	public static final int TC_MARKET_LIMIT = 2;//市值限制
	public static final int TC_AMOUNT_LIMIT = 3;//成交额限制
	public static final int TC_STOP_LIMIT = 4;//停牌
	public static final int TC_MARKET_HOLIDAY = 5;//交易所停牌
	/**
	 * 涨跌停限制
	 */
	public static final int TC_UP_DOWN_LIMIT = 5;
	/**
	 * 不能成交
	 */
	public static final int TC_NO_DEAL = -1;

	public static final int TC_ALL_DEAL = 0;//全部成交
	public static final int TC_BUY = 1;//买
	public static final int TC_SELL = -1;//卖
	public static final int TC_ZD = 1;//自动调仓
	/***********涨跌标志***********/
	public final static int FLAG_UP = 1;
	public final static int FLAG_DOWN = -1;
	public final static int FLAG_NOCHANGE = 0;
	/**
	 * 涨跌停换手率限制
	 */
	public static final BigDecimal turnoverRateLimit = new BigDecimal(0.05);
	//------------客户端对话类型---------
	public static final int MS_TOAST = 1;//消息类型
	public static final int MS_DIALOG = 2;//
	public static final int MS_CONFIRM = 3;//
	/*************用户身份*********************/
	public static final int IDENTITY_TYPE_BUY = 2;//买方
	public static final int IDENTITY_TYPE_SELL = 1;//卖方
	public static final int COMPANY_MANAGE = 99;//管理
	public static final int COMPANY_SM = 0;//私募
	public static final int COMPANY_QSYJS = 8;//券商研究所
	public static final String IDENTITY_BUY = "买方";
	public static final String IDENTITY_SELL = "卖方";
	public static final int CONTRACT_TYPE_BUY = 2;//合同买方
	public static final int CONTRACT_TYPE_SELL = 1;//合同卖方
	public static final int CONTRACT_TYPE_NO = 0;//非合同
	public static final int CONTRACT_TYPE_NO_SELL = 3;//非合同卖方
	public static final int CONTRACT_TYPE_NO_BUY = 4;//非合同买方
	/*************模拟组合状态*********************/
	public static final int COMB_HIDEFLAG_XS = 0;//显示
	public static final int COMB_HIDEFLAG_YC = 1;//隐藏
	public static final int COMB_HIDEFLAG_BKYC = -1;//不可隐藏
	public static final int COMB_SOLEFLAG_DJ = 1;//独家
	public static final int COMB_SoleFlag_FDJ = 0;//非独家
	public static final int COMB_DEALSTATE_FZ = -1;//复制
	public static final int COMB_DISABLE = 0;//关闭
	public static final int COMB_ENABLE = 1;//
	/*************模拟组合权限*********************/
	public static final int COMB_PRIVILEGE_YES = 1;//有权限
	public static final int COMB_PRIVILEGE_NO = 0;//无权限
	/*************权限分享*********************/
	public static final int PRIVILEGE_COMB_SM = 0;//模拟组合私密
	public static final int PRIVILEGE__COMB_BGK = 1;//模拟组合半公开
	public static final int PRIVILEGE_COMB_QGK = 2;//模拟组合全公开
	/*************模拟组合行业*********************/
	public static final int COMB_INDUSTRY_QHY = 0;//全行业
	public static final int COMB_INDUSTRY_ZT = -1;//主题
	public static final int COMB_INDUSTRY_HS300 = 0;//沪深300
	public static final int COMB_INDUSTRY_HSZS = 12;//香港恒生指数
	public static final int COMB_INDUSTRY_MIN = 12;//组合单行业最小值
	public static final int COMB_INDUSTRY_CT = 20;//财通自定义组合收益基准
	/************指标排序***********/
	public final static int COMB_DAY = 1;//日收益率
	public final static int COMB_WEEK = 2;//周收益率
	public final static int COMB_MONTH = 3;//月收益
	public final static int COMB_QUARTER = 4;//季收益
	public final static int COMB_YEAR = 5;//年
	public final static int COMB_ACCUMULATED = 6;//累计
	public final static int COMB_EXCESS = 7;//超额
	public final static int COMB_THISYEAR_ACCU = 8;//今年以来累计
	public final static int COMB_THISYEAR_EXCESS = 9;//今年以来超额
	public final static int COMB_THISMONTH = 10;//本月
	/************* 观点分享类型 ***************/
	public static final int POINT_SHARE_JG = 1;//机构
	public static final int POINT_SHARE_YJY = 2;//研究员
	public static final int POINT_SHARE_QZ = 3;//群组
	public static final int POINT_SHARE_MF = 4;//买方
	public static final int POINT_SHARE_SELL = 5;//卖方
	public static final int POINT_SHARE_COMPANY_TYPE = 7;//机构类别
	public static final int POINT_SHARE_RHY_INFORMAL = 10;//睿行研非正式用户
	public static final int POINT_SHARE_RHY = 11;//睿行研正式用户
	public static final int POINT_SHARE_VOTE_RIGHT = 12;//新财富投票权用户
	public static final int POINT_SHARE_WHITELIST = 13;//机构白名单
	public static final int POINT_SHARE_YJS_WHITELIST = 18;//研究所白名单
	public static final int SHARE_CODE_MF = 0;//买方代码为0
	/************* 观点分享类型 ***************/
	public static final int POINT_RELATION_GP = 1;//股票
	public static final int POINT_RELATION_HY = 2;//行业
	public static final int POINT_RELATION_HG = 3;//宏观
	public static final int RELATION_CODE_HG = 1;//宏观代码为1
	public static final int MEMBER_ORG = 1;//成员类型 机构
	public static final int MEMBER_PERSON = 2;//个人
	public static final int MEMBER_GROUP = 3;//群组
	public static final int MEMBER_MF = 4;//买方
	public static final int MEMBER_VOTE_RIGHT = 12;//新财富投票权用户
	public static final int MEMBER_VOTE_ZXZH = 13;//尊享组合
	public static final int POINT_MR = 1;//买入
	public static final int POINT_MC = -1;//卖出
	public static final int POINT_KD = 2;//看多
	public static final int POINT_KK = -2;//看空
	public static final int POINT_ZX = 0;//中性
	/******************登录类型**********************/
	public static final int CLIENT_TYPE_MOBLE = 1;//每市移动端
	public static final int CLIENT_TYPE_PC = 2;//PC端
	public static final int CLIENT_TYPE_WECHAT = 3;//微信端
	public static final int CLIENT_TYPE_GZH = 4;//微信公众号
	public static final int CLIENT_TYPE_ALF = 7;//阿尔法平台
	public static final int CHECK_DEVICE_CHANGE = 1;//校验设备更换
	/***************** 设备型号 *******************/
	public static final int APP_IOS = 1;// ios手机
	public static final int APP_ANDROID = 2;// Android手机
	public static final int APP_WEB = 3;// web端
	public static final int APP_ALF_IOS = 4;// 阿尔法ios手机
	public static final int APP_ALF_ANDROID = 5;// 阿尔法Android手机
	/***************** 第三方类型 *******************/
	public static final int THIRD_WX = 1;// 微信
	/************* 分享类型 ***************/
	public static final int SHARE_DATA_TYPE_ZH = 1;//分享组合
	public static final int SHARE_DATA_TYPE_GD = 2;//分享观点
	public static final int SHARE_DATA_TYPE_TC = 4;//分享调仓
	public static final int SHARE_DATA_TYPE_GDJ = 5;//分享观点集

	public static final int WX_TEMPLETE_WHITELIST_REQ = 1;//微信白名单权限消息

	/***********confirm***********/
	public static final int COMMIT_YES = 1;
	public static final int COMMIT_NO = 0;
	/***********禁投标志***********/
	public static final int STOCK_BAN = 1;
	public static final int STOCK_PERMIT = 0;
	/**************身份角色******************/
	public static final int ROLE_ADMIN = 1;//管理员
	public static final int ROLE_NORMAL = 0;//普通用户
	/*****************************/
	public static final int MOBILE_HIDE = 1;//手机号隐藏
	public static final int MOBILE_SHOW = 0;//手机号显示
	/**************用户状态******************/
	public static final int ACCOUNT_JH = 0;//激活
	public static final int ACCOUNT_WJH = 1;//未激活
	public static final int ACCOUNT_DJ = -1;//冻结
	public static final int ACCOUNT_ZX = -3;//注销
	public static final int ACCOUNT_YC = 2;//账户异常
	/**
	 * 内部账户
	 */
	public static final int ACCOUNT_INNER = 1;
	/**
	 * 白名单账户
	 */
	public static final int ACCOUNT_WL = 2;
	/**
	 * 普通（名片审核通过）账户
	 */
	public static final int ACCOUNT_NORMAL = 3;
	/**
	 * 游客账户
	 */
	public static final int ACCOUNT_TOURIST = 4;
	/**************请求头部******************/
	public static final String[] REQUEST_HEADER = {"appVersion", "systemVersion"};//请求头部
	/**************消息群组状态******************/
	public static final int MESSAGE_GROUP_JS = -1;//解散
	public static final int MESSAGE_GROUP_YC = -2;//被移出
	/**************权限记录类型***********************/
	public static final int PRIVILEGE_RECORD_YHGS = 1;//用户公司改变
	public static final int PRIVILEGE_RECORD_YHSF = 2;//用户身份改变
	public static final int PRIVILEGE_RECORD_ZH_GSFZ = 3;//修改模拟组合授权的分组
	public static final int PRIVILEGE_RECORD_YH_GSFZ = 4;//修改公司的人员分组
	public static final int PRIVILEGE_RECORD_QZ = 5;//群组成员修改
	public static final int PRIVILEGE_RECORD_ZH = 6;//修改模拟组合权限
	public static final int PRIVILEGE_RECORD_GD = 7;//观点权限修改
	public static final int PRIVILEGE_RECORD_HD = 8;//活动权限修改
	public static final int PRIVILEGE_RECORD_ZXG = 9;//自选股权限修改
	public static final int PRIVILEGE_RECORD_HT = 10;//合同权限修改
	/**************文件类型************************************/
	public static final int FILE_TEXT = 1;//文本
	public static final int FILE_AUDIO = 2;//音频
	/**************处理状态************************************/
	public static final int PROGRESS_RUNNING = 1;//进行中
	public static final int PROGRESS_STOP = -1;//终止
	public static final int PROGRESS_FINISH = 2;//完成
	/**************登录信息************************************/

	public static final String DEVICE_TOKEN_INIT = "0";//初始值
	/*******************文本类型***********************/
	public static final int CONTENT_TEXT = 1;//文本
	public static final int CONTENT_RICH_TEXT = 2;//富文本
	public static final int CONTENT_RICH_URL = 3;//外部链接
	public static final int LATITUDE_INT = 100000;//经纬度整型
	/*******************积分***********************/
	/**
	 * 注册
	 **/
	public static final int INTEGRAL_ZC = 1;
	/**
	 * 邀请并注册
	 **/
	public static final int INTEGRAL_YQ = 2;
	/**
	 * 每日签到
	 **/
	public static final int INTEGRAL_QD = 3;
	/**
	 * 连续七天签到
	 **/
	public static final int INTEGRAL_QDJL = 4;
	/**
	 * 偷看
	 **/
	public static final int INTEGRAL_TKZH = 5;
	/**
	 * 系统奖励
	 **/
	public static final int INTEGRAL_XTJL = 9;
	/**
	 * 创建组合
	 **/
	public static final int INTEGRAL_CJZH = 10;
	/**
	 * 发表观点
	 **/
	public static final int INTEGRAL_FBGD = 11;
	/**
	 * 加自选股
	 **/
	public static final int INTEGRAL_JZXG = 12;
	/**
	 * 参加活动
	 **/
	public static final int INTEGRAL_CJHD = 13;
	/**
	 * 查看研报
	 **/
	public static final int INTEGRAL_CKYB = 14;
	/**
	 * 组合调仓
	 **/
	public static final int INTEGRAL_ZHTC = 15;
	/**
	 * 写评论
	 **/
	public static final int INTEGRAL_XPL = 16;
	/**
	 * 花积分看活动
	 **/
	public static final int INTEGRAL_KHD = 17;
	/*******************签到***********************/
	public static final int SIGNIN_DAYS = 7; //连续签到的天数


	//	public static final int ZO_ORG_CODE = 48;//中欧机构代码
	public static int comb_companyType_third = 80;//第三方
	/**
	 * 投资总监
	 */
	public static final int USER_POSITION_TZZJ = 3;
	/**
	 * 基金经理
	 */
	public static final int USER_POSITION_JJJL = 2;
	/**
	 * 研究员
	 */
	public static final int USER_POSITION_YJY = 1;

	/***************精选研报来源******************/
	public static final int REPORT_LY_XCF = 1;//新财富
	public static final int REPORT_LY_TC = 2;//调仓
	public static final int REPORT_LY_ZDY = 3;//自定义
	public static final int REPORT_LY_JG = 4;//机构
	/**************自选股类型*******************/
	public static final int SELF_POOL_USER = 0;//用户目录
	public static final int SELF_POOL_SYS = 1;//系统目录
	/**
	 * 睿行研
	 */
	public static final int SELF_POOL_RYX = 2;

	/**************投研服务******************/
	public static final int SERVICE_STOCK_LIMIT_NUM = 20;//分享股票数据限制
	/**************股票与用户相关的标签******************/
	public static final int STOCK_YJZX = 1;
	public static final int STOCK_YLZX = 2;
	public static final int STOCK_DYZX = 3;
	public static final int STOCK_STZX = 4;
	/**************主页配置类型******************/
	public static final int BLOCK_TYPE_LXHD = 2;//连续滑动区
	public static final int BLOCK_TYPE_YB = 3;//研报
	public static final int BLOCK_TYPE_ZH = 6;//组合
	public static final int BLOCK_TYPE_RL = 8;//投资日历
	public static final int BLOCK_TYPE_BK = 9;//板块区
	public static final int BLOCK_TYPE_QBZS = 10;//全部展示

	/************** 用户标签类型 **********************/
	/**
	 * 研究之星
	 **/
	public static final int USER_LABEL_TYPE_YJZX = 1;
	/**
	 * 盈利之星
	 **/
	public static final int USER_LABEL_TYPE_YLZX = 2;
	/**
	 * 调研之星
	 **/
	public static final int USER_LABEL_TYPE_DYZX = 3;
	/**
	 * 首推之星
	 **/
	public static final int USER_LABEL_TYPE_STZX = 4;

	/************** 规则编码 ************************/
	public static final String RULE_BUY = "U1";
	public static final String RULE_SELLER = "U2";
	public static final String RULE_ORG_USER = "U3";
	public static final String RULE_ALL_MARKET = "U4";
	public static final String RULE_HAS_RIGHT = "R1";
	public static final String RULE_NO_RIGHT = "R2";
	public static final String RULE_SUBSCRIBED_COMB = "R3";
	public static final String RULE_FOCUSED_PERSON = "R4";
	public static final String RULE_SELF_STOCK = "R5";
	public static final String RULE_SAME_INDUSTRY = "R6";
	public static final String RULE_EXCESS_YIELD_OVER_5 = "R7";
	public static final String RULE_XCF = "R8";
	public static final String RULE_ORDER_SETTLE = "R9";
	public static final String RULE_CREATE_TIME = "R10";
	public static final String RULE_START_TIME = "R11";
	public static final String RULE_AUDIO_UPLOAD = "R12";
	public static final String RULE_STRATEGY = "R13";
	public static final String RULE_DLC_3 = "R14";
	public static final String RULE_DLC_2 = "R15";
	public static final String RULE_DLC_1 = "R16";
	public static final String RULE_DLC_0 = "R17";
	public static final String RULE_SAME_COMPANY = "R18";
	public static final String RULE_APPLY_TIME = "R19";
	//合并调仓	
	public static final String RULE_MERGED_ORDER = "R20";
	//清算的组合	
	public static final String RULE_COMB_SETTLE = "R21";
	//组合有调仓
	public static final String RULE_COMB_SETTLE_HAS_ORDER = "R22";
	//大幅调仓	
	public static final String RULE_LARGE_COMB_ORDER = "R23";
	//新建组合触发首页瀑布流显示
	public static final String RULE_NEW_COMB = "R24";

	/****************客户端类型************************/
	public static List<String> resourceUrl = new ArrayList<String>();//资源路径
	public static int requestTimeOutLimit = 1000;//请求超时上限（ms）
	public static final String tokenSplite = "_,";//token分隔符
	public static final String TOP_INDUSTRY_PREFIX = "topIndustry_";
	/**
	 * 判断当天的盘后清算是否完成
	 */
	public static final String KEY_IS_SETTLE_COMB_COMPLETED = "isSettleCombCompleted";

	// 创建一个可重用的固定线程数的线程池
	public static ExecutorService fixedThreadPool = Executors.newFixedThreadPool(5);

	/**
	 * 获取创建线程数(CPU 核数 * 2)
	 */
	public static int corePoolSize = Runtime.getRuntime().availableProcessors() * 2;
	/**
	 * 创建一个服务器专用线程池(避免线程池出现OOM)
	 */
	public static ExecutorService serverThreadPool = new ThreadPoolExecutor(corePoolSize, corePoolSize, 0, TimeUnit.SECONDS, new ArrayBlockingQueue<>(512), new ThreadPoolExecutor.DiscardPolicy());
	//评价描述
	public static Map<Integer, String> evaluateMap = new HashMap<Integer, String>();
	//新增活动列表，用于活动推荐
	public static ConcurrentLinkedQueue<Map<String, Object>> newCreatedActivityList = new ConcurrentLinkedQueue<Map<String, Object>>();
	/**
	 * 公司类别列表
	 * key:type,
	 * value:desc
	 */
	public static Map<Integer, Object> companyTypeMap = new HashMap<Integer, Object>();
	/**
	 * key:type
	 * value:config
	 */
	public static Map<String, Object> wechatConfigMap = new HashMap<String, Object>();

	/**
	 * 下划线分隔符
	 */
	public static final String split = "_";

	/**
	 * 研报机构-每市机构对应关系
	 */
	public static Map<Long, Long> companyOrgRela = new HashMap<Long, Long>();


	public static String[] comb_rank_array = {"dayRank", "weekRank", "sinceThisMonthRank", "monthRank", "quarterRank",
		"sinceThisYearRank", "yearRank", "excessRank", "followRank", "readRank"};
	public static int comb_companyType_other = -6;//其他
	public static int comb_companyType_all = -1;//全部
	//	public static int comb_companyType_exclude = 80;//排除非机构
	private static Map<Integer, Integer> comb_companyType_map = null;

	/**
	 * 组合所属公司类型映射字典
	 */
	public static Map<Integer, Integer> getCombCompanyTypeMap() {
		if (comb_companyType_map != null) {
			return comb_companyType_map;
		}
		comb_companyType_map = new HashMap<Integer, Integer>();
		//-2 公募 -3 券商 -4 私募 -5 保险 -6其他
		comb_companyType_map.put(0, -4);
		comb_companyType_map.put(16, -4);
		comb_companyType_map.put(17, -4);
		comb_companyType_map.put(8, -3);
		comb_companyType_map.put(1, -2);
		comb_companyType_map.put(2, -2);
		comb_companyType_map.put(13, -5);
		return comb_companyType_map;
	}

	/**
	 * @param companyType 用户所属公司类型
	 * @return
	 * @Title: convertCombCompanyType、
	 * @Description: 转换组合公司类型
	 * @return: int
	 */
	public static int convertCombCompanyType(int companyType) {
		Map<Integer, Integer> combCompanyTypeMap = getCombCompanyTypeMap();
		if (combCompanyTypeMap.containsKey(companyType)) {
			return combCompanyTypeMap.get(companyType);
		}
		return comb_companyType_other;
	}

	/**
	 * 组合排行的指标名称映射字典
	 */
	public static Map<String, String> comb_rank_map = null;

	public static Map<String, String> getCombRankMap() {
		if (comb_rank_map != null) {
			return comb_rank_map;
		}
		comb_rank_map = new HashMap<String, String>();
		comb_rank_map.put("weekRank", "近一周收益");
		comb_rank_map.put("monthRank", "近一月收益");
		comb_rank_map.put("quarterRank", "近一季收益");
		comb_rank_map.put("yearRank", "近一年收益");
		comb_rank_map.put("sinceThisMonthRank", "本月收益");
		comb_rank_map.put("dayRank", "日收益");
		comb_rank_map.put("sinceThisYearRank", "今年以来收益");
		comb_rank_map.put("excessRank", "超额收益");
		comb_rank_map.put("followRank", "被关注数");
		comb_rank_map.put("readRank", "阅读数");
		comb_rank_map.put("turnoverRateRank", "换手率");
		comb_rank_map.put("positionRank", "仓位");
		comb_rank_map.put("accumulatedRank", "盈利率");
		comb_rank_map.put("concentrationRank", "集中度");
		return comb_rank_map;
	}

	//港湾数据密钥
	public static final String encryptKey = "vdvidvc#Tjsfdsf";
	/*******环境********/
	public final static String ACTIVE_TRIAL = "trial";//试用
	public final static String ACTIVE_PROD = "prod";//生产版本
	public final static String ACTIVE_DEV = "dev";//dev版本
	public final static String ACTIVE_LOCAL = "local";//local版本
	public final static String ACTIVE_UAT = "uat";//uat版本
	public final static String ACTIVE_DEMO = "demo";//demo版本
	/*********外部用户*************/
	public static final Object USER_WB = "外部用户";
	public static final Object USER_WB_IMG = "";//头像
	/*******环境配置********/
	public static String active;
	public static String crmMarketServer;
	public static String iawebServer;
	public static String iawebServerLocalAddress;
	public static String iaServer;
	public static String iawebUri;
	public static String downloadServer;
	public static String mutiFilePath;//多文件上传路径
	public static String customerDir = "customerImage";
	public static String headDir = "headImage";
	public static String messageDir = "message";//消息目录
	public static String meetingMessageDir = "meetingMessage";//会议消息目录
	public static String meetingDir = "meeting";//会议目录
	public static String pointDir = "point";//观点目录
	public static String tempDir = "temp";//观点目录
	public static String audioDir = "audio";//音频目录
	public static String imageDir = "image";//图片目录
	public static String homeDir = "home";//首页目录
	public static String productDir = "product";//产品目录
	public static String userDir = "user";//用户目录
	public static String companyDir = "company";//公司目录
	public static String liveAudioDir = "live";//直播音频目录
	public static String awardDir = "award";//个人奖项目录
	public static String fileSeparate = "/";//文件分隔
	public static String uploadDir = null;//上传文件的临时目录
	public static String serverConfigPath = null;//配置文件路径
	public static String meixKeyId = null;//阿里云api访问access key id
	public static String meixKeySecret = null;////阿里云api访问access key secret

	static {
		evaluateMap.put(POINT_MR, "买入");
		evaluateMap.put(POINT_MC, "卖出");
		evaluateMap.put(POINT_KD, "看多");
		evaluateMap.put(POINT_KK, "看空");
		evaluateMap.put(POINT_ZX, "中性");
	}

	public static Map<String, String> reportTypeCollection = new HashMap<String, String>() {
		private static final long serialVersionUID = 1662852028567187L;
		{
			put("策略专题报告", "celuezhuanti");
			put("港股公司点评报告", "ganggugongsidianping");
			put("港股公司动态报告", "ganggugongsidongtai");
			put("港股公司深度报告", "ganggugongsishendu");
			put("港股行业点评报告", "gangguhangyedianping");
			put("港股行业动态报告", "gangguhangyedongtai");
			put("港股行业深度报告", "gangguhangyeshendu");
			put("公司点评报告", "gongsidianping");
			put("公司动态报告", "gongsidongtai");
			put("公司深度研究报告", "gongsishenduyanjiu");
			put("行业点评报告", "hangyedianping");
			put("行业动态报告", "hangyedongtai");
			put("行业深度研究报告", "hangyeshenduyanjiu");
			put("行业投资策略报告", "hangyetouzicelue");
			put("行业月报", "hangyeyuebao");
			put("行业周报", "hangyezhoubao");
			put("宏观点评报告", "hongguandianping");
			put("宏观季度报告", "hongguanjidu");
			put("宏观年度报告", "hongguanniandu");
			put("宏观深度报告", "hongguanshendu");
			put("宏观研究报告", "hongguanyanjiu");
			put("宏观专题报告", "hongguanzhuanti");
			put("投资策略月报", "touzicelueyuebao");
			put("投资策略周报", "touziceluezhoubao");
			put("新股研究报告", "xinguyanjiubaogao");
			put("债券点评", "zhaiquandianping");
			put("债券深度报告", "zhaiquanshendubaogao");
			put("债券周报", "zhaiquanzhoubao");
			put("债券专题报告", "zhaiquanzhuantibaogao");
		}
	};
	public static final String reportTyepDefault = "default";
	
	public static final int READ_RESEARCH_REPORT_5 = 1;
	public static final int LISTEN_ACTIVITY_3 = 2;
	public static final int SUBSCRIBE_ANALYST_3 = 3;
	public static final int JOIN_ACTIVITY_3 = 4;
	public static final int WHITE_LIST_REQ = 5;
	public static final int REAL_NAME_AUTH = 6;

	public static final String READ_RESEARCH_REPORT_5_NAME = "15天内查看研报超过5次，总时长不少于10分钟";
	public static final String LISTEN_ACTIVITY_3_NAME = "15天内收听电话会议超过3次，总时长不少于10分钟";
	public static final String SUBSCRIBE_ANALYST_3_NAME = "15天内关注分析师超过3个";
	//	public static final String JOIN_ACTIVITY_3_NAME = "30天内申请报名参加活动（调研、策略会、其他会议等）超过3次";
	public static final String JOIN_ACTIVITY_3_NAME = "30天内报名参加活动超过3次";
	//	public static final String WHITE_LIST_REQ_NAME = "申请内容权限";
	public static final String REAL_NAME_AUTH_NAME = "注册认证成为实名客户";

	/**
	 * 线索池线索类型
	 */
	private static final List<ConstantVo> clueTypeList = new ArrayList<ConstantVo>() {
        private static final long serialVersionUID = 1L;

        {
			add(new ConstantVo("全部", -1));
			add(new ConstantVo(READ_RESEARCH_REPORT_5_NAME, READ_RESEARCH_REPORT_5));
			add(new ConstantVo(LISTEN_ACTIVITY_3_NAME, LISTEN_ACTIVITY_3));
			add(new ConstantVo(SUBSCRIBE_ANALYST_3_NAME, SUBSCRIBE_ANALYST_3));
			add(new ConstantVo(JOIN_ACTIVITY_3_NAME, JOIN_ACTIVITY_3));
//			add(new ConstantVo(WHITE_LIST_REQ_NAME, WHITE_LIST_REQ));
			add(new ConstantVo(REAL_NAME_AUTH_NAME, REAL_NAME_AUTH));
		}
	};

	public static List<ConstantVo> getClueTypeList() {
		return clueTypeList;
	}

	private static final Map<Integer, String> clueTypeMap = new HashMap<Integer, String>() {
        private static final long serialVersionUID = 1L;
        {
			put(READ_RESEARCH_REPORT_5, READ_RESEARCH_REPORT_5_NAME);
			put(LISTEN_ACTIVITY_3, LISTEN_ACTIVITY_3_NAME);
			put(SUBSCRIBE_ANALYST_3, SUBSCRIBE_ANALYST_3_NAME);
			put(JOIN_ACTIVITY_3, JOIN_ACTIVITY_3_NAME);
//			put(WHITE_LIST_REQ, WHITE_LIST_REQ_NAME);
			put(REAL_NAME_AUTH, REAL_NAME_AUTH_NAME);
		}
	};

	public static String getClueName(int clueType) {
		return clueTypeMap.get(clueType);
	}

	public static String getClueAction(int clueType) {
		String clueAction = "";
		switch (clueType) {
			case READ_RESEARCH_REPORT_5:
				clueAction = "查看研报";
				break;
			case LISTEN_ACTIVITY_3:
				clueAction = "收听电话会议";
				break;
			case SUBSCRIBE_ANALYST_3:
				clueAction = "关注分析师";
				break;
			case JOIN_ACTIVITY_3:
				clueAction = "参加活动";
				break;
			case WHITE_LIST_REQ:
				clueAction = "申请内容权限";
				break;
			case REAL_NAME_AUTH:
				clueAction = "注册实名用户";
				break;
		}
		return clueAction;
	}

	public static String getCNActivityType(int activityType) {
		String type = "";
		switch (activityType) {
			case MeixConstants.CHAT_MESSAGE_DY: {
				type = "调研";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_LY: {
				type = "路演";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_CL: {
				type = "策略会";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_DHHY: {
				type = "电话会议";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_HY: {
				type = "会议";
				break;
			}
			default:
				return null;
		}
		return type;
	}

	public static final String TEMP_TOKEN = "nozuonodie";

	/**
	 * Boolean
	 **/
	public static final String BOOLEAN = "BOOLEAN";
	public static final String BOOLEAN_Y = "Y";
	public static final String BOOLEAN_N = "N";

	/**
	 * Boolean_Int
	 **/
	public static final String BOOLEAN_INT = "BOOLEAN_INT";
	public static final int BOOLEAN_INT_0 = 0;
	public static final int BOOLEAN_INT_1 = 1;

	/**
	 * 同步状态
	 **/
	public static final String SYNC_STATE = "SYNC_STATE";
	public static final String SYNC_STATE_NEW = "NEW"; // 新建
	public static final String SYNC_STATE_SUCCESS = "SUCCESS"; // 成功
	public static final String SYNC_STATE_FAIL = "FAIL"; // 失败
	public static final String SYNC_STATE_CLOSE = "CLOSE"; // 关闭

	/**
	 * 用户名
	 **/
	public static final String LOGIN_NO = "loginNo";
	/**
	 * 用户名
	 **/
	public static final String USERNAME = "userName";
	/**
	 * 密码
	 **/
	public static final String PASSWORD = "password";
	/**
	 * 验证码
	 **/
	public static final String VER_CODE = "valifyCode";

	/**
	 * 请求头
	 **/
	public static final Map<String, String> HEAD = new HashMap<>();

	static {
		HEAD.put("clientType", "2");
		HEAD.put("appVersion", "3.3.0");
	}

	/**
	 * 创建方式
	 **/
	public static final String CREATED_VIA = "CREATED_VIA";
	public static final int CREATED_VIA_1 = 1; // 系统（自动）生成
	public static final int CREATED_VIA_2 = 2; // 人工添加

	/**
	 * 创建方式——文本
	 **/
	public static final String CREATED_VIA_TEXT = "CREATED_VIA_TEXT";
	public static final String CREATED_VIA_TEXT_1 = "系统生成"; // 系统生成
	public static final String CREATED_VIA_TEXT_2 = "批量导入"; // 系统生成
	public static final String CREATED_VIA_TEXT_3 = "手动录入"; // 人工添加

	/**
	 * APP类型
	 */
	public static final String CLIENT_TYPE_HEAD = "clientType";
	public static final String CLIENT_TYPE = "CLIENT_TYPE";
	public static final int CLIENT_TYPE_1 = 1; // 后台管理
	public static final int CLIENT_TYPE_2 = 2; // 公众号
	public static final int CLIENT_TYPE_3 = 3; // 小程序
	public static final int CLIENT_TYPE_4 = 4; // 同步程序

	public static final int WX_CLIENT_TYPE_2 = 2; // 服务号
	public static final int WX_CLIENT_TYPE_3 = 3; // 小程序
	public static final int WX_CLIENT_TYPE_4 = 4; // 订阅号

	public static int getIssuetypeByClientType(int clientType) {
		int issueType = ISSUETYPE_MEIX;
		if (clientType == MeixConstants.CLIENT_TYPE_2) {    //公众号发布渠道为1
			issueType = ISSUETYPE_WECHAT;
		} else if (clientType == MeixConstants.CLIENT_TYPE_3) {    //公众号发布渠道为4
			issueType = ISSUETYPE_APPLET;
		}
		return issueType;
	}

	/**
	 * 登录类型
	 */
	public static final String LOGIN_TYPE = "LOGIN_TYPE";
	public static final int LOGIN_TYPE_1 = 1; // 手机
	public static final int LOGIN_TYPE_2 = 2; // 邮箱
	public static final int LOGIN_TYPE_3 = 3; // 二维码
	public static final int LOGIN_TYPE_4 = 4; // 微信

	/**
	 * 默认登录名
	 */
	public static final String DEFAULT_NAME = "匿名用户";

	/**
	 * 默认原因
	 */
	public static final String DEFAULT_REASON = "您的名片信息与输入的信息不匹配，无法验证您的身份！";
	/**
	 * 发布渠道
	 */
	public static final int ISSUETYPE_MEIX = 0; //每市
	public static final int ISSUETYPE_WECHAT = 1; //公众号
	public static final int ISSUETYPE_MSG = 2; //短信
	public static final int ISSUETYPE_EMAIL = 3; //邮件
	public static final int ISSUETYPE_APPLET = 4; //小程序

	/**
	 * 职位
	 */
	public static final String POSITION = "POSITION"; //职位

	/**
	 * 研究所
	 */
	public static final String INSTITUTE = "INSTITUTE"; //研究所

	/**
	 * 公司
	 */
	public static final String COMPANY = "COMPANY"; //公司

	/**
	 * 部门
	 */
	public static final String DEPARTMENT = "DEPARTMENT"; //部门 
	public static final String DEPARTMENT_ANALYST = "analyst"; //研究部
	public static final String DEPARTMENT_MARKET = "market"; //销售部
	public static final String DEPARTMENT_OPERATE = "operate"; //运营部 
	public static final String DEPARTMENT_SYSTEM_MANAGE = "systemManage"; //系统管理部
	public static final String DEPARTMENT_OTHER = "other"; //其他部门

	/**
	 * 分析师研究方向
	 */
	public static final String DIRECTION = "DIRECTION";

	/**
	 * 角色
	 */
	public static final String ROLE = "ROLE";

	/**
	 * 权限
	 */
	public static final String SCOPE = "SCOPE";
	/**
	 * 研报关联类型
	 */
	public static final String REPORT_RELATION_TYPE = "REPORT_RELATION_TYPE";
	public static final int REPORT_RELATION_TYPE_0 = 0; // rate
	public static final int REPORT_RELATION_TYPE_1 = 1; // 作者
	public static final int REPORT_RELATION_TYPE_2 = 2; // 个股
	public static final int REPORT_RELATION_TYPE_3 = 3; // 行业

	/**
	 * 研报大类
	 */
	public static final String REPORT_TYPE = "REPORT_TYPE";

	/**
	 * 研报小类
	 */
	public static final String REPORT_SUB_CATALOG = "REPORT_SUB_CATALOG";
	
	/**
	 * 研报搜索分类
	 */
	public static final String REPORT_SEARCH_TYPE = "REPORT_SEARCH_TYPE";
	
	/**
	 * 行业转换
	 */
	public static final String REPORT_INDUSTRY = "REPORT_INDUSTRY";
	/**
	 * 员工类型
	 */
	public static final String USER_TYPE = "USER_TYPE";
	public static final int USER_TYPE_1 = 1; // 分析师
	public static final int USER_TYPE_2 = 2; // 销售
	public static final int USER_TYPE_3 = 3; // 运营
	public static final int USER_TYPE_4 = 4; // 系统管理
	public static final int USER_TYPE_5 = 5; // 其他
	
	/**
	 * 员工角色
	 */
	public static final String USER_ROLE = "USER_ROLE";
	public static final int USER_ROLE_1 = 1; // 管理员
	public static final int USER_ROLE_2 = 2; // 合规
	public static final int USER_ROLE_3 = 3; // 分析师
	public static final int USER_ROLE_4 = 4; // 销售

	/**
	 * 活动状态
	 */
	public static final String ACTIVITY_STATUS = "ACTIVITY_STATUS";
	public static final int ACTIVITY_STATUS_0 = 0; // 未审核
	public static final int ACTIVITY_STATUS_1 = 1; // 已通过
	public static final int ACTIVITY_STATUS_2 = 2; // 已拒绝
	public static final int ACTIVITY_STATUS_3 = 3; // 已撤销

	/**
	 * 研报状态
	 */
	public static final String REPORT_STATUS = "REPORT_STATUS";
	public static final int REPORT_STATUS_0 = 0; // 未审核
	public static final int REPORT_STATUS_1 = 1; // 已通过
	public static final int REPORT_STATUS_2 = 2; // 已拒绝
	public static final int REPORT_STATUS_3 = 3; // 已撤销

	/**
	 * 评论状态
	 */
	public static final String COMMENT_STATUS = "COMMENT_STATUS";
	public static final int COMMENT_STATUS_0 = 0; // 未审核
	public static final int COMMENT_STATUS_1 = 1; // 已通过
	public static final int COMMENT_STATUS_2 = 2; // 已拒绝
	public static final int COMMENT_STATUS_3 = 3; // 已撤销

	public static final String POWER_SHARE_ALL = "全市场";

	/**
	 * 验证码key
	 */
	public static final String VALIFY_CODE_KEY = "VERCODE_KEY";
	public static final String VERCODE_KEY_MNG = "VERCODE_KEY_MNG"; // 后台管理
	public static final String VERCODE_KEY_WX = "VERCODE_KEY_WX"; // 微信

	/**
	 * redis 分隔符
	 */
	public static final String REDIS_SPLIT = ":";

	/**
	 * JSESSIONID
	 */
	public static final String JSESSIONID = "JSESSIONID";

	/**
	 * APP_ID
	 */
	public static final String APP_ID = "appId";

	public static final int CUSTOMER_AUTH_FAILED = 2;//认证失败
	public static final int CUSTOMER_AUTH_SUCCESS = 3;//认证成功

	/**
	 * 部门类型
	 **/
	public static final String DEPARTMENT_TYPE = "DEPARTMENT_TYPE";
	public static final String DEPARTMENT_TYPE_A = "analyst";
	public static final String DEPARTMENT_TYPE_M = "market";

	/**
	 * 用户角色
	 **/
	public static final String USER_SCOPE = "DEPARTMENT_TYPE";
	public static final String USER_SCOPE_WHITELIST = "whitelist";
	public static final String USER_SCOPE_ORG = "org_user";
	public static final String USER_SCOPE_INDIVIDUAL = "individual";
	public static final String USER_SCOPE_WHITELIST_INDIVIDUAL = "whitelist_individual";
	public static final String USER_SCOPE_TRIAL = "trial";

	/**
	 * 授权
	 **/
	public static final int SHARE_PUBLIC = 0; //公开
	public static final int SHARE_DEFAULT = -1; //默认授权
	public static final int SHARE_GROUP = 1; //指定组授权
	public static final int SHARE_ACCOUNTTYPE = 4;

	/**
	 * 授权位置
	 **/
	public static final int SHARE_LIST = 1; //列表权限
	public static final int SHARE_DETAIL = 2; //详情权限
	public static final int SHARE_LIVE = 3; //直播权限
	public static final int SHARE_REPLAY = 4; //回放权限
	
	/**
	 * 同步类型枚举
	 **/
	public static final String SYNC_JS_TABLE_NAME = "SYNC_JS_TABLE_NAME";
	public static final String SYNC_JS_TABLE_NAME_USER = "bgUser";
	public static final String SYNC_JS_TABLE_NAME_REPORT = "bgReport";
	public static final String SYNC_JS_TABLE_NAME_DIRECTION = "bgDirection";
	public static final String SYNC_JS_TABLE_NAME_STATION = "bgStation";
	public static final String SYNC_JS_TABLE_NAME_ORG = "bgOrg";
	
	/** 用于记录log **/
	public static final int REPORT_ALBUM = 3000; //研报合辑
	public static final int MEETING_ALBUM = 3001; //会议纪要合辑
    public static final int REPORT_ALBUM_DETAIL = 3002; //研报合辑详情
    public static final int MEETING_ALBUM_DETAIL = 3003; //会议纪要合辑详情
    
    public static final int USER_YH_CLUE = 43; //线索池用户
    public static final int USER_YH_INNER_PW = 411; //内部用户密码
    public static final int USER_YH_INNER_ROLE = 412; //内部用户角色
    public static final int USER_YH_GROUP = 403; //用户组
    public static final int USER_YH_WL = 42; //白名单用户
    public static final int USER_YH_LABEL = 401; //画像管理
    
    public static final int BANNER = 1000; //banner
    public static final int MESSAGE_PUSH = 2000;//消息推送
    /** 文章状态  **/
    public static final String ARTICLE_STATE = "ARTICLE_STATE";
    public static final int ARTICLE_STATE_DELETE = -1; // 删除
    public static final int ARTICLE_STATE_NEW = 0; // 未发布
    public static final int ARTICLE_STATE_PRE = 1; // 预约发布
    public static final int ARTICLE_STATE_PUBLISH = 2; // 已发布
    public static final int ARTICLE_STATE_UNDO = 3; // 已下架
    
    /** 微信文章推送行为 **/
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION = "WECHAT_ARTICLE_PUBLISH_ACTION";
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION_UPLOADIMG = "UPLOADIMG"; // 上传图文消息内的图片获取URL
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION_ADD_NEW = "ADD_NEW"; // 添加图文素材
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION_UPLOAD= "UPLOAD"; // 添加素材（临时）
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION_UPLOADNEWS = "UPLOADNEWS"; // 添加图文素材（临时）
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION_ADD_MATERIAL = "ADD_MATERIAL"; // 添加其他类型永久素材
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION_UPDATE_NEW = "UPDATE_NEW"; // 更新图文素材
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION_DELETE_NEW = "DEL_NEW"; // 删除图文素材
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION_MASS_SEND_ALL = "MASS_SEND_ALL"; // 群发
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION_MASS_DELETE = "MASS_DELETE"; // 删除群发
    public static final String WECHAT_ARTICLE_PUBLISH_ACTION_MASS_PREVIEW = "MASS_PREVIEW"; // 预览

	/**
	 * 微信上传素材类型
	 **/
	public static final String WECHAT_ARTICLE_TYPE = "WECHAT_ARTICLE_TYPE";
	public static final String WECHAT_ARTICLE_TYPE_TEXT = "text";
	public static final String WECHAT_ARTICLE_TYPE_GRAPHIC = "graphic";
	public static final String WECHAT_ARTICLE_TYPE_IMAGE = "image";
	public static final String WECHAT_ARTICLE_TYPE_VOICE = "voice";
	public static final String WECHAT_ARTICLE_TYPE_VIDEO = "video";
	public static final String WECHAT_ARTICLE_TYPE_THUMB = "thumb";
	
	/**
	 * 接口管理枚举
	 */
	public static final String SND_REQ_METHOD = "SND_REQ_METHOD";
	public static final String REC_REQ_METHOD = "REC_REQ_METHOD";
	public static final String SYNC_TABLE_NAME = "SYNC_TABLE_NAME";
	public static final String INSERVICE = "在职";
	public static final String ANALYST_KEY = "分析师";
	public static final String CHIEF_ANALYST_KEY = "首席";
	public static final String SALER_KEY = "销售";
	
	/**
	 * 操作枚举
	 */
	public static final int OPERATOR_ADD = 1;
	public static final int OPERATOR_UPDATE = 2;
	public static final int OPERATOR_UNDO = 3;
	public static final int OPERATOR_DELETE = 4;
	public static final int OPERATOR_ASSIGN_SALER = 5;
	public static final int OPERATOR_EXPORT = 6;
	public static final int OPERATOR_IMPORT = 7;
	public static final int OPERATOR_AUTH_WHITELIST = 8;
	public static final int OPERATOR_CHANGE_GROUP = 9;
}
