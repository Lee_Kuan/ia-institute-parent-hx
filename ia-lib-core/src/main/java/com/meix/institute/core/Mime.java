package com.meix.institute.core;

import lombok.Data;

/**
 * 媒体类型
 *
 */
@Data
public class Mime {

    private String fileName;
    private String contentType;
}
