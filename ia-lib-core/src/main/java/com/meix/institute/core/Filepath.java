package com.meix.institute.core;

import lombok.Data;

@Data
public class Filepath {
    
    private String uuid;
    private String path;
    private String pubPath;
}
