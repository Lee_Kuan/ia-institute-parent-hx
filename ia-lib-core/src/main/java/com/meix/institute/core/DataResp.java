package com.meix.institute.core;

import java.io.Serializable;

public class DataResp implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private boolean success;
    private int err_code;
    private String err_desc;
    private Object data;
    
    public DataResp () {
        
    }
    
    public DataResp(int err_code, String err_desc, boolean success) {
        this.err_code = err_code;
        this.err_desc = err_desc;
        this.success = success;
    }
    
    public static DataResp ok() {
        return new DataResp(1008, "成功", true);
    }
    
    public static DataResp fail() {
        return new DataResp(1009, "失败", false);
    }
    
    public boolean isSuccess() {
        return success;
    }
    public void setSuccess(boolean success) {
        this.success = success;
    }
    public int getErr_code() {
        return err_code;
    }
    public void setErr_code(int err_code) {
        this.err_code = err_code;
    }
    public String getErr_desc() {
        return err_desc;
    }
    public void setErr_desc(String err_desc) {
        this.err_desc = err_desc;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
