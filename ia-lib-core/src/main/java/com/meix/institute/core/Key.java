package com.meix.institute.core;

public class Key {
    
    private Long id;
    private String code;
    
    public Key(Long id, String code) {
        this.id = id;
        this.code = code;
    }
    
    public Key() {
    }
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
}
