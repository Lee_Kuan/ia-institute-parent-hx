package com.meix.institute.core;

public class SecurityInfo {
    
    private Long id;
    private String user;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    
}
