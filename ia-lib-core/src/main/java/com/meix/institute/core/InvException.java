package com.meix.institute.core;

/**
 * 自定义异常
 * @author admin
 *
 */
public class InvException extends Exception{

    private static final long serialVersionUID = 1L;
    
    protected String err; // 错误代码
    protected String message; // 错误描述
    protected Object[] args; // 参数
    
    public InvException(String message) {
        super(message);
        this.message = message;
    }
    
    public InvException(String err, String message) {
        super(message);
        this.err = err;
        this.message = message;
    }
    
    public InvException(String err, String message, Object... args) {
        super(message);
        this.err = err;
        this.message = message;
        setArgs(args);
    }
    
    public Object[] getArgs() {
        return this.args;
    }
    public void setArgs(Object[] args) {
        this.args = args;
    }
   
}
