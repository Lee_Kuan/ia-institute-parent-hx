package com.meix.institute.util;

import org.apache.commons.lang.StringUtils;

import com.meix.institute.constant.MeixConstants;

/**
 * @版权信息	:上海睿涉取有限公司
 * @部门		:开发部
 * @作者		:zhouwei
 * @E-mail  : zhouwei@51research.com
 * @创建日期	: 2015年7月28日
 * @Description:版本控制
 */
public class VersionUtil {
	public static long v1_2_0=1*100000000+2*1000000+0*10000;
	public static long v1_3_0=1*100000000+3*1000000+0*10000;
	public static long v1_3_1=1*100000000+3*1000000+1*10000;
	public static long v1_3_2=1*100000000+3*1000000+2*10000;
	public static long v1_3_4=1*100000000+3*1000000+4*10000;
	public static long v1_4_0=1*100000000+4*1000000+0*10000;
	public static long v1_5_0=1*100000000+5*1000000+0*10000;
	public static long v1_6_0=1*100000000+6*1000000+0*10000;
	public static long v1_7_0=1*100000000+7*1000000+0*10000;
	public static long v1_9_0=1*100000000+9*1000000+0*10000;
	public static long v1_9_5=1*100000000+9*1000000+5*10000;
	public static long v1_9_6=1*100000000+9*1000000+6*10000;
	public static long v2_0_0=2*100000000+0*1000000+0*10000;
	public static long v2_1_0=2*100000000+1*1000000+0*10000;
	public static long v2_2_0=2*100000000+2*1000000+0*10000;
	public static long v2_3_0=2*100000000+3*1000000+0*10000;
	public static long v2_4_0=2*100000000+4*1000000+0*10000;
	public static long v2_5_0=2*100000000+5*1000000+0*10000;
	public static long v2_6_0=2*100000000+6*1000000+0*10000;
	public static long v2_7_0=2*100000000+7*1000000+0*10000;
	public static long v2_7_3=2*100000000+7*1000000+3*10000;
	public static long v2_7_4=2*100000000+7*1000000+4*10000;
	public static long v2_8_0=2*100000000+8*1000000+0*10000;
	public static long v2_9_0=2*100000000+9*1000000+0*10000;
	public static long v2_9_5=2*100000000+9*1000000+5*10000;
	public static long V3_0_0=3*100000000+0*1000000+0*10000;
	public static long V3_0_3=3*100000000+0*1000000+3*10000;
	public static long V3_0_4=3*100000000+0*1000000+4*10000;

	/**
	 * @Title: getVersionValue、
	 * @Description:获取版本号的值
	 * @param version
	 * @return
	 * @return: long
	 */
	public static long getVersionValue(String version){
		if(StringUtils.isEmpty(version)){
			return 0;
		}
		//去除前缀字母
		if(version.toUpperCase().indexOf("V")>-1){
			version=version.substring(1, version.length());
		}
		//拆分版本数字
		String[] versionArray=StringUtils.split(version,".");
		int firstVersion=Integer.parseInt(versionArray[0]);
		int secondVersion=Integer.parseInt(versionArray[1]);
		int thirdVersion=Integer.parseInt(versionArray[2]);
		int fourthVersion=0;
		if(versionArray.length==4){
			fourthVersion=Integer.parseInt(versionArray[3]);
		}
		long versionValue=firstVersion*100000000+secondVersion*1000000+thirdVersion*10000+fourthVersion;
		return versionValue;
	}
	/**
	 * @Title: isRiskRemind、
	 * @Description:是否风险提示
	 * @param deviceType
	 * @param appVersion
	 * @return
	 * @return: boolean
	 */
	public static boolean isRiskRemind(int deviceType,long interfaceVersion){
		boolean isRisk=false;//是否提示风险
		if((deviceType==MeixConstants.APP_ANDROID && interfaceVersion>VersionUtil.v1_3_0)
				|| 
				(deviceType==MeixConstants.APP_IOS && interfaceVersion>VersionUtil.v1_3_1)
				|| deviceType==MeixConstants.APP_WEB){
			isRisk=true;
		}
		return isRisk;
	}
}
