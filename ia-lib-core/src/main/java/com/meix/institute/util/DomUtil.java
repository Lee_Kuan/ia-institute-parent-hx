package com.meix.institute.util;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

/**
 * @版权信息	:上海睿涉取有限公司
 * @部门		:开发部
 * @作者		:zhouwei
 * @E-mail  : zhouwei@51research.com
 * @创建日期	: 2014年11月6日
 * @Description:dom文档的工具类
 */
public class DomUtil {
	public static int imgType=0;
	public static int audioType=1;//音频
	public static int videoType=2;//视频
	public static int attachType=3;//附件
	public static int reportType=4;
	/**
	 * @Title: cleanContent、
	 * @Description:格式化文档
	 * @param content
	 * @return
	 * @return: String
	 */
	public static String cleanContent(String content){
		content=content.replaceAll("&", "&amp;");
		return content;
	}
	/**
	 * @Title: recoverContent、
	 * @Description:复原
	 * @param content
	 * @return
	 * @return: String
	 */
	public static String recoverContent(String content){
		content=content.replaceAll("&amp;","&");
		return content;
	}
	private static Whitelist list=new Whitelist();
	private static Whitelist solrList=new Whitelist();
	static{
		list.addTags("a","img","p","br","&nbsp;","font");
		list.addAttributes(":all", "onclick","name","src","original","type","size","href",
				"title","alt","height","width","id","class","value","color");
		solrList.addTags("font");
		solrList.addAttributes(":all","color");
	}
	public static String cleanHtml(String content){
		String _content=Jsoup.clean(content,list);
		return recoverContent(_content); 
	}
	/**
	 * @Title: cleanHtml、
	 * @Description:有替换功能的clean方法
	 * @param content
	 * @param replaceMap
	 * @return
	 * @return: String
	 */
	public static String cleanHtml(String content,Map<String,String> replaceMap){
		String _content=Jsoup.clean(content,list);
		String key=null;
		Iterator<String> it=replaceMap.keySet().iterator();
		while(it.hasNext()){
			key=it.next();
			_content=_content.replaceAll(key, replaceMap.get(key));
		}
		return recoverContent(_content); 
	}
	/**
	 * @Title: replaceHtml、
	 * @Description:替换内容
	 * @param content
	 * @param replaceMap
	 * @return
	 * @return: String
	 */
	public static String replaceHtml(String _content,Map<String,String> replaceMap){
		String key=null;
		Iterator<String> it=replaceMap.keySet().iterator();
		while(it.hasNext()){
			key=it.next();
			_content=_content.replaceAll(key, replaceMap.get(key));
		}
		return recoverContent(_content); 
	}
	public static String solrHtml(String content){
		String _content=Jsoup.clean(content,Whitelist.none());
		_content=_content.replaceAll("&nbsp;", "");
		return recoverContent(_content); 
	}
	/**
	 * @Title: checkHtml、
	 * @Description:校验文本是否为html
	 * @param content
	 * @return
	 * @return: boolean
	 */
	public static boolean checkHtml(String content){
		String src=StringUtils.replace(content,"\r","");
		src=StringUtils.replace(src,"\n","");
		src=StringUtils.replace(src," ","");
		int srcLen=src.length();
		String dest=Jsoup.clean(content,Whitelist.none());
		dest=StringUtils.replace(dest,"\r","");
		dest=StringUtils.replace(dest,"\n","");
		dest=StringUtils.replace(dest," ","");
		int destLen=dest.length();
		if(srcLen==destLen){
			return false;
		}
		return true;
	}
	public static String cleanHtml(String html, String className) {
		if(className != null && !className.equals("")) {
			try {
				Document doc = Jsoup.parse(html);
				Elements nodes = doc.getElementsByClass(className);
				nodes.remove();
				html = doc.html();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		html = Jsoup.clean(html, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
		html = StringUtils.replace(html, "&nbsp;", "");
		html = StringUtils.replace(html, "</br>", "");
		html =  StringUtils.replace(html, " ", "");
		return html;
	}
	
    public static String replaceTag(String htmlStr, String tag) {
	  //解析传递的字符串 parse 包含 <body>标签
	  Document parse = Jsoup.parseBodyFragment(htmlStr);
	  Elements imgs = parse.getElementsByTag("img");
	  for (Element img : imgs) {
	      String linkSrc = img.attr(tag);
	      System.out.println(linkSrc);
	      String newLinkSrc = "http:www.baidu.com";
	      img.attr(tag, newLinkSrc);
	  }
	  htmlStr = parse.body().toString();
	  htmlStr = htmlStr.substring(7, htmlStr.length() - 7);
	  htmlStr = StringUtils.replace(htmlStr, "\r", "");
	  htmlStr = StringUtils.replace(htmlStr, "\n", "");
	  return htmlStr;
	}
	
	
	public static void main(String[] args) throws IOException {
	    String html = "<section style=\"height: 100%; overflow: hidden; line-height: 0; vertical-align: middle; max-width: 100%; box-sizing: border-box;\"><img style=\"width: 100%; height: 100%; opacity: 0; box-sizing: border-box;\" data-ratio=\"0.9047619\" data-w=\"294\" _width=\"100%\" src=\"https://image.meix.com/ueditor/jsp/upload/image/20200422/1587541122713083390.jpg\" _src=\"https://image.meix.com/ueditor/jsp/upload/image/20200422/1587541122713083390.jpg\"/></section>";
	    System.out.println(html);
	    String className = "src";
	    String replaceTag = replaceTag(html, className);
	    System.out.println(replaceTag);
	}
	@SuppressWarnings("unused")
	private static void parseHtml(String str_url) throws IOException {
		String youdao_interface = "http://note.youdao.com/yws/public/note/";
		URL url = new URL(str_url);
		Document doc = Jsoup.parse(url, 5000);
		Elements nodes = doc.getElementsByClass("file-name");
		System.out.println(nodes.text());
	}
}
