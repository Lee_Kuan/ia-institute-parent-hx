package com.meix.institute.util;

import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.mp3.MP3AudioHeader;
import org.jaudiotagger.audio.mp3.MP3File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @版权信息    :上海睿涉取有限公司
 * @部门        :开发部
 * @作者        :zhouwei
 * @E-mail : zhouwei@51research.com
 * @创建日期    : 2014年11月7日
 * @Description:多媒体工具类
 */
public class MediaUtil {
	private static Logger log = LoggerFactory.getLogger(MediaUtil.class);

	/**
	 * @Title: convertToMp3、
	 * @Description:文件转换成mp3
	 * @param filePath
	 * @return
	 * @return: String
	 */
	public static String convertToMp3(String path,String fileName){
		int index=fileName.lastIndexOf(".");
		 String newFileName=fileName.substring(0,index)+".mp3";
		 String destPath=path+File.separator+newFileName;
		 String filePath=path+File.separator+fileName;
		 Runtime runtime = Runtime.getRuntime();  
         Process proce = null;  
         //VBR -qscale 6 CBR -b 360
         String cmd = "ffmpeg"+" -i "+filePath+" -b:a 24k -y "+destPath;  
         log.info("语音转换指令:" + cmd);
         try{
        	 proce = runtime.exec(cmd);           
        	 new PrintStream(proce.getInputStream()).start();
        	 new PrintStream(proce.getErrorStream()).start();
			 proce.waitFor();
         }catch(Exception e){
        	 throw new RuntimeException("MP3转换出错！",e);
         }
         return newFileName;
	}
	
	/**
	 * @param destPath
	 * @return
	 * @Title: getMp3Time、
	 * @Description:获取MP3时长
	 * @return: int
	 */
	public static int getMp3Time(String destPath) {
		try {
			MP3File f = (MP3File) AudioFileIO.read(new File(destPath));
			MP3AudioHeader audioHeader = (MP3AudioHeader) f.getAudioHeader();
			int len = audioHeader.getTrackLength();
			return len == 0 ? 1 : len;
		} catch (Exception e) {
			log.error("获取Mp3时长出错", e);
			return 1;
			//throw new RuntimeException("获取Mp3时长出错",e);
		}
	}

	/**
	 * @return
	 * @Title: getMp3Time、
	 * @Description:获取MP3时长
	 * @return: int
	 */
	public static int getMp3Time(File file) {
		try {
			MP3File f = (MP3File) AudioFileIO.read(file);
			MP3AudioHeader audioHeader = (MP3AudioHeader) f.getAudioHeader();
			int len = audioHeader.getTrackLength();
			return len == 0 ? 1 : len;
		} catch (Exception e) {
			log.error("获取Mp3时长出错", e);
			return 1;
			//throw new RuntimeException("获取Mp3时长出错",e);
		}
	}

	public static class PrintStream extends Thread {
		java.io.InputStream __is = null;

		public PrintStream(java.io.InputStream is) {
			__is = is;
		}

		public void run() {
			try {
				while (this != null) {
					int _ch = __is.read();
					if (_ch != -1)
						System.out.print((char) _ch);
					else break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
