package com.meix.institute.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;

public class DesIAServer {
	@SuppressWarnings("unused")
	private static final String PASSWORD_CRYPT_KEY = "__jDlog_";
	@SuppressWarnings("unused")
	private static final String DES = "DES";
	private static byte[] iv = new byte[]{1, 2, 3, 4, 5, 6, 7, 8};

	public DesIAServer() {
	}

	public static byte[] encrypt(byte[] src, byte[] key) throws Exception {
		SecureRandom sr = new SecureRandom();
		DESKeySpec dks = new DESKeySpec(key);
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		SecretKey securekey = keyFactory.generateSecret(dks);
		Cipher cipher = Cipher.getInstance("DES");
		cipher.init(1, securekey, sr);
		return cipher.doFinal(src);
	}

	public static byte[] encrypt2(byte[] src, byte[] keys) throws Exception {
		IvParameterSpec zeroIv = new IvParameterSpec(iv);
		SecretKeySpec key = new SecretKeySpec(keys, "DES");
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		cipher.init(1, key, zeroIv);
		byte[] encryptedData = cipher.doFinal(src);
		return encryptedData;
	}

	public static byte[] decrypt(byte[] src, byte[] key) throws Exception {
		SecureRandom sr = new SecureRandom();
		DESKeySpec dks = new DESKeySpec(key);
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		SecretKey securekey = keyFactory.generateSecret(dks);
		Cipher cipher = Cipher.getInstance("DES");
		cipher.init(2, securekey, sr);
		return cipher.doFinal(src);
	}

	public static byte[] decrypt2(byte[] src, byte[] keys) throws Exception {
		IvParameterSpec zeroIv = new IvParameterSpec(iv);
		SecretKeySpec key = new SecretKeySpec(keys, "DES");
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		cipher.init(2, key, zeroIv);
		byte[] decryptedData = cipher.doFinal(src);
		return decryptedData;
	}

	public static final String decrypt(String data) {
		try {
			return new String(decrypt(hex2byte(data.getBytes()), "__jDlog_".getBytes()));
		} catch (Exception var2) {
			return null;
		}
	}

	public static final String decrypt(String data, String key) {
		try {
			return decrypt(data, key, 1);
		} catch (Exception var3) {
			return null;
		}
	}

	public static final String decrypt(String data, String key, int byteMethod) {
		try {
			return byteMethod == 1 ? new String(decrypt(hex2byte(data.getBytes()), key.getBytes())) : new String(decrypt2(decryptBASE64(data), key.getBytes()));
		} catch (Exception var4) {
			return null;
		}
	}

	public static final byte[] decryptToByte(byte[] data, String key, int byteMethod) {
		try {
			return byteMethod == 1 ? decrypt(hex2byte(data), key.getBytes()) : decrypt2(decryptBASE64(new String(data)), key.getBytes());
		} catch (Exception var4) {
			return null;
		}
	}

	public static final byte[] decryptNoBase64(byte[] data, String key, int byteMethod) {
		try {
			return byteMethod == 1 ? decrypt(data, key.getBytes()) : decrypt2(data, key.getBytes());
		} catch (Exception var4) {
			return null;
		}
	}

	public static final String decrypt(byte[] data) {
		try {
			return new String(decrypt(hex2byte(data), "__jDlog_".getBytes()));
		} catch (Exception var2) {
			return null;
		}
	}

	public static final String encrypt(String content) {
		try {
			return byte2hex(encrypt(content.getBytes(), "__jDlog_".getBytes()));
		} catch (Exception var2) {
			return null;
		}
	}

	public static final String encrypt(String content, String key) {
		try {
			return byteArr2HexStr(encrypt(content.getBytes(), key.getBytes()));
		} catch (Exception var3) {
			return null;
		}
	}

	public static final String encrypt(String content, String key, int byteMethod) {
		try {
			return byteMethod == 1 ? byteArr2HexStr(encrypt(content.getBytes("UTF-8"), key.getBytes())) : encryptBASE64(encrypt2(content.getBytes("UTF-8"), key.getBytes()));
		} catch (Exception var4) {
			return null;
		}
	}

	public static final String encrypt(byte[] content) {
		try {
			return byte2hex(encrypt(content, "__jDlog_".getBytes()));
		} catch (Exception var2) {
			return null;
		}
	}

	public static String byte2hex(byte[] b) {
		String hs = "";
		String stmp = "";

		for (int n = 0; n < b.length; ++n) {
			stmp = Integer.toHexString(b[n] & 255);
			if (stmp.length() == 1) {
				hs = hs + "0" + stmp;
			} else {
				hs = hs + stmp;
			}
		}

		return hs.toUpperCase();
	}

	public static String byteArr2HexStr(byte[] arrB) throws Exception {
		int iLen = arrB.length;
		StringBuffer sb = new StringBuffer(iLen * 2);

		for (int i = 0; i < iLen; ++i) {
			int intTmp;
			for (intTmp = arrB[i]; intTmp < 0; intTmp += 256) {
				;
			}

			if (intTmp < 16) {
				sb.append("0");
			}

			sb.append(Integer.toString(intTmp, 16));
		}

		return sb.toString();
	}

	public static byte[] hex2byte(byte[] b) {
		if (b.length % 2 != 0) {
			throw new IllegalArgumentException("长度不是偶数");
		} else {
			byte[] b2 = new byte[b.length / 2];

			for (int n = 0; n < b.length; n += 2) {
				String item = new String(b, n, 2);
				b2[n / 2] = (byte) Integer.parseInt(item, 16);
			}

			return b2;
		}
	}

	public static byte[] hexStr2ByteArr(String strIn) throws Exception {
		byte[] arrB = strIn.getBytes();
		int iLen = arrB.length;
		byte[] arrOut = new byte[iLen / 2];

		for (int i = 0; i < iLen; i += 2) {
			String strTmp = new String(arrB, i, 2);
			arrOut[i / 2] = (byte) Integer.parseInt(strTmp, 16);
		}

		return arrOut;
	}

	public static byte[] decryptBASE64(String key) throws Exception {
		Base64 base64 = new Base64();
		return base64.decode(key);
	}

	public static String decryptBASE64AsString(String key) {
		if (StringUtils.isEmpty(key)) {
			return "";
		} else {
			Base64 base64 = new Base64();

			try {
				return new String(base64.decode(key), "UTF-8");
			} catch (Exception var3) {
				throw new RuntimeException("base64解析失败！" + var3.getMessage(), var3);
			}
		}
	}

	public static String encryptBASE64(byte[] key) throws Exception {
		Base64 base64 = new Base64();
		return base64.encodeAsString(key);
	}

	public static String encryptBASE64(String key) throws Exception {
		Base64 base64 = new Base64();
		return base64.encodeAsString(key.getBytes("UTF-8"));
	}

	public static String getDeviceId(String token) {
		if (StringUtils.isEmpty(token)) {
			return null;
		} else {
			String str = Des.decrypt(token);
			String[] array = str.split("_,");
			return array.length <= 9 ? null : array[9];
		}
	}

	public static void test() {
		String temp = decrypt("KXr4PpI50+ZCrTEGK0CCAvUarM98FBA2fTeA+JiTHqMUC75sa0HgkPwzx8wtNaVrCFSIY1VYbeErhaK0Ly9HsH6wNYMCt8k5LQYbxQ2gGy0gquInULN/EqJKlmDEn9x0Oa/e12FV1eRcRQZCJs0dFrbJRCucBeY2gkEzZTSlgG5PgFb4G7wcfVF1gbqV74CtN9xxzgf6XHgFWBmFgm6fEiczGL56xW82IeTqdBgddwTUXqpmCqep1Lfdl5V/Kg44l+JVzRUNBFxOl7Z52ZLRpoHzkxEwtzvY", "nozuonod", 2);
		System.out.println(temp);
	}

	public static void test1() {
		String tmp = "0YeSXVqHwjal5pnnIi/CHfiI5iK/cTVYEXLVMr6ngDP41KpVTLWdyp+aQyTjOFNaukicog2nSKPYrirC7JJ6X9NwKLhCG2K5TGKx/fvTsqLkBjq1S3FWVlJSn7T96xs950gOSkTlWn2KkE64lJ2ohs8iEfubTxZMaulswNTQ543SsBmPhXLFnU0WZBSeyVtVIX7ckfxSxGy5Z2wAOIjrgfyKkriQexRoCmQWWi4Dfc3NBp0jMzWrIm4N2SQvN5M5XQEutExSzRXwi7oZpzBJqGkH6NGb+upyllFIK7xfKFB1q44DP4Q6e7DM06Prckdv7AOD+13noogQjiWrIdAuEjSasqS3XWxOl7OGL0vuvu22GaTsE1A+EeWCdAWYnzue";
		String temp = decrypt(tmp);
		System.out.println(temp);
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		String key = "9NWZsKsDWYExPDBgi/aJIzkt6JVZTbUaIei/OzBwNjwPT8rnMF+FZ2B9vTF0oWPmrVk0AX5dM6gQL1T6KC28ctlUaPsF3Czj7UaqJLdkA5WGd9w7p/qZu2qG/2COs9Bw/QtLskYELSHudN7jyEpWyjlIM2PaSdDiTbgCmGPEB03/H8bdMSHlvuf3g7F7RZIfWB3n3qOm3rke7DJ9Frv+J6Byzi8/rTi8brnP1fqvNayMdI8eFi9w2456C9VFRJXBczA8X8RPFipS8TEO+RdUYoMiNgY6gQnZK1x3n129NrNxzK2U4bJsLUaFzENMFyNANkqoYIjtZ54c9v51yF6JaZNaAOgJMXR4QGDPZOKgXz5240BIj0QgvOWu16mUn9I4m6ykXcDjNuYDubeqxUyt4JLVCiaoTzRcf/na98TLYwNUwqdc6J2/zjmaD0lWKU8bNI5VPFB6PM238WnMrWFfda+9xtc73g3SFtW/zsUoMc/e+mCZowJyktd78SCI/ISYUeSwaFrcWJrNQ4UvOBT+2NZ/ytttI99KrQ+qNaG3mLG6xzuL24dyoDMV777qngqW7LTFPAZUw0I+qW0tDlE2vFVPbEh7yD0mj7GiOUPApJTG0xFhtEhvAsukQ7oWkMK0ov0XNB2grdBlGgF1znNm+cfYKw2M+WryGXVegFr7ar+Wn0nW3OZqsg95A3+QRUGF/kepjg5aO5jR/5VWcGk6mpY8vait905k+bwTZ6VgS5qWqHe17GlM0bYShvhm/B5SSnRcsMS86nIoMSmtT3yojNDY2MxAmHphV4PGCkxVrZrxnAoZKT/PJfeaoRRbgz9Ar6hWouR8AxfrW4I40+qXYQB4PpdI6qn+CixiTFFi7Lf9FNRgWPGD31FBBiFgERbAkpTGxw0J3Og/0GsmpPPDKO4ldDshiFIfkIbd1D4/u2I0sJm7sY693l0lGcQBzWpe8goC8hCo90bgzR2dFG8P7txJqmslz1/krkOi/nr5ePa9Unt/E6TSpSOn8aNa4XADACUXWBn0drhCvURg2IUlsnt2pcJHwSBt0HygVssZWkud6BIZWVXA85qh8ev+YHPl22jR03O5Z4gJSJIklU5V79vaQiPYPj+ujQFCflPeo3kgfoxtvUPrlpX809/EZEtTKoYB4nF8ziDzmq/hryiseGFX14gsy9lGNPkWGLab02Lknv5Q16pV+4U0Ipwr9+mci6tv7RbUKZEqAsCQvyJDdPfoQCtUE7AVmIQZyr54fj1CMfssYwzeD6tmWqyxv2vaoMNr+N+qe5Xu7Y18zKukbYxY3FFr/AHqXPvvwTQfBf/cz8zbJ3emwbGOKgq27SpgTh1LjQBW7XXkrL0BoWb4pvMha1p1/FGYuarESQft4mJg9mk0l8f8azK0cPEwKNez2ghMsh/SVhkRWHmt64IAfx9kG58lbto0JXKZopiqufN7jiGDTWwXRIFioyaL8TZti+V2v6BAliE=";
		System.out.println(new String(decryptToByte(key.getBytes(), "9693B9E4", 2)));
	}
}

