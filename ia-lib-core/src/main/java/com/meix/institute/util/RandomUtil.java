package com.meix.institute.util;

import java.util.Random;

public class RandomUtil {
    
    public static final String VALUE = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@!#$%^&*()_+<>?";
    
    public static String generateRandom(int n) {
        StringBuffer sb = new StringBuffer();
        char[] c = VALUE.toCharArray();  
        Random random = new Random();  
        for( int i = 0; i < n; i ++) {
            sb.append(c[random.nextInt(c.length)]);
        }  
        return sb.toString();
    }
    
    public static void main(String[] args) {
        String generateRandom = generateRandom(5);
        System.out.println(generateRandom);
    }
}
