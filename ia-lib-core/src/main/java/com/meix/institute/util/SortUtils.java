package com.meix.institute.util;

import com.meix.institute.vo.HoneyBeeBaseVo;
import com.meix.institute.vo.stock.StockVo;

import java.io.Serializable;
import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * Created by zenghao on 2018/9/4.
 */
public class SortUtils {

	// ranking DESC,applyTime desc
	public static class HoneyBeeDescComparator implements Serializable, Comparator<HoneyBeeBaseVo> {

		private static final long serialVersionUID = -4390859249137127276L;

		@Override
		public int compare(HoneyBeeBaseVo one, HoneyBeeBaseVo two) {
			Collator ca = Collator.getInstance(Locale.CHINA);
			int flag = 0;
			if (one.getRanking() == two.getRanking()) {
				flag = ca.compare(one.getApplyTime(), two.getApplyTime());
			} else {
				flag = ((Double) one.getRanking()).compareTo(two.getRanking());
			}
			return -flag;
		}
	}

	public static class MonthlyStockDescComparator implements Serializable, Comparator<StockVo> {


		private static final long serialVersionUID = -239688382554099811L;

		@Override
		public int compare(StockVo one, StockVo two) {
			int flag = 0;
			float ret = one.getMonthYieldRate() - two.getMonthYieldRate();
			if (ret == 0) {
				return flag;
			}
			flag = ret > 0 ? 1 : -1;
			return -flag;
		}
	}
}
