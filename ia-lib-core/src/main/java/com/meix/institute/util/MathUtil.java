package com.meix.institute.util;

import java.math.BigDecimal;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;

import com.meix.institute.constant.MeixConstants;

import net.sf.json.JSONObject;

/**
 * @版权信息    :上海睿涉取有限公司
 * @部门        :开发部
 * @作者        :zhouwei
 * @E-mail : zhouwei@51research.com
 * @创建日期    : 2015年7月28日
 * @Description:数学工具
 */
public class MathUtil {
	public static BigDecimal w = new BigDecimal(10000);//万
	public static final int LATITUDE_INT = 100000;//经纬度整型

	/**
	 * @param value
	 * @return
	 * @Title: getValueIfNull、
	 * @Description:空值的默认处理
	 * @return: BigDecimal
	 */
	public static BigDecimal getValueIfNull(BigDecimal value) {
		if (value == null) {
			return BigDecimal.ZERO;
		}
		return value;
	}

	/**
	 * @param value
	 * @return
	 * @Title: precisionW、
	 * @Description:精确到万
	 * @return: BigDecimal
	 */
	public static BigDecimal precisionW(BigDecimal value) {
		if (value == null) {
			return BigDecimal.ZERO;
		}
		return value.divide(w, 0, BigDecimal.ROUND_HALF_UP).multiply(w);
	}

	/**
	 * @param value
	 * @return
	 * @Title: precisionDecimal、
	 * @Description:精确小数位
	 * @return: BigDecimal
	 */
	public static BigDecimal precisionDecimal(BigDecimal value) {
		if (value == null) {
			return BigDecimal.ZERO;
		}
		return value.setScale(4, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * @param value
	 * @param scale
	 * @return
	 * @Title: precisionDecimal、
	 * @Description:
	 * @return: String
	 */
	public static String precisionDecimal(BigDecimal value, int scale) {
		if (value == null) {
			return "";
		}
		return value.setScale(scale, BigDecimal.ROUND_HALF_UP).toString();
	}

	public static String getStringValueIfNull(BigDecimal value) {
		if (value == null) {
			return "--";
		}
		return value.toString();
	}

	/**
	 * @param value
	 * @return
	 * @Title: percentFormat、
	 * @Description:百分号处理
	 * @return: String
	 */
	public static String percentFormat(BigDecimal value) {
		value = value.multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP);
		return value.toString() + "%";
	}

	/**
	 * @return
	 * @Title: defeatPercent、
	 * @Description:打败百分比计算
	 * @return: String
	 */
	public static String defeatPercent(int rank, int total) {
		if (rank == 0 || total == 0) {
			return "0%";
		}

		double percent = ((double) (total - rank) / total) * 100;

		return String.format("%.0f", percent) + "%";
	}

	/**
	 * @return
	 * @Title: defeatPercent、
	 * @Description:打败百分比计算 针对新财富提供的方法
	 * @return: String
	 */
	public static String defeatPercentForXCF(int rank, int total) {
		double percent = 0;
		if (rank == 0 || total == 0) {
			return "0%";
		}
		if (rank == 1) {
			return "100%";
		} else {
			percent = ((double) (total - rank) / total) * 100;
			if (percent >= 99) {
				return "99%";
			}
		}

		return String.format("%.0f", percent) + "%";
	}

	/**
	 * @param value
	 * @return
	 * @Title: formateWValue、
	 * @Description:处理成万
	 * @return: String
	 */
	public static String formateWValue(BigDecimal value) {
		return value.divide(new BigDecimal(10000), 0, BigDecimal.ROUND_HALF_UP) + "万";
	}

	/**
	 * @param count
	 * @return
	 * @Title: getSimuCombCount、
	 * @Description:数量处理
	 * @return: String
	 */
	public static String getSimuCombCount(int count) {
		if (count < 25) {
			return count + "";
		} else if (count < 100) {
			return count / 10 + "0+";
		} else if (count < 1000) {
			return count / 100 + "00+";
		} else if (count < 10000) {
			return count / 100 + "00+";
		} else {
			return count / 10000 + "万+";
		}
	}

	/**
	 * @param value1
	 * @param value2
	 * @return
	 * @Title: calcScaleRate、
	 * @Description:计算缩放比例
	 * @return: BigDecimal
	 */
	public static BigDecimal calcScaleRate(int value1, int value2) {
		BigDecimal bv1 = new BigDecimal(value1);
		BigDecimal bv2 = new BigDecimal(value2);
		return bv1.divide(bv2, 6, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * @param scaleRate
	 * @param value
	 * @return
	 * @Title: calcScaleValue、
	 * @Description:计算缩放值
	 * @return: int
	 */
	public static int calcScaleValue(BigDecimal scaleRate, int value) {
		return scaleRate.multiply(BigDecimal.valueOf(value)).intValue();
	}

	/**
	 * @param mapPosition
	 * @return
	 * @Title: getLatitudeIntValue、
	 * @Description:获取纬度的整数值(放大)
	 * @return: int
	 */
	public static int getLatitudeIntValue(String mapPosition) {
		if (StringUtils.isEmpty(mapPosition)) {
			return -1;
		}
		try {
			JSONObject json = JSONObject.fromObject(mapPosition);
			//经纬度做整数化处理
			Float f_latitude = MapUtils.getFloatValue(json, "latitude") * LATITUDE_INT;
			int latitude = f_latitude.intValue();
			return latitude;
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * @param mapPosition
	 * @return
	 * @Title: getLongitudeIntValue、
	 * @Description:获取精度的整数值(放大)
	 * @return: int
	 */
	public static int getLongitudeIntValue(String mapPosition) {
		if (StringUtils.isEmpty(mapPosition)) {
			return -1;
		}
		try {
			JSONObject json = JSONObject.fromObject(mapPosition);
			//经纬度做整数化处理
			Float f_longitude = MapUtils.getFloatValue(json, "longitude") * LATITUDE_INT;
			int longitude = f_longitude.intValue();
			return longitude;
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * @param f_latitude
	 * @param distance   距离 米
	 * @return
	 * @Title: getLatitudeRange、
	 * @Description:根据距离计算纬度的范围
	 * @return: Integer[]
	 */
	public static Integer[] getLatitudeRange(float f_latitude, int distance) {
		//纬度 一度约等于 110.94公里
		int latitude = getIntValueByTimes(f_latitude);
		Integer[] limitArray = new Integer[2];
		int degress = getIntValueByTimes(
			new BigDecimal(1).divide(new BigDecimal(latitudeMeter), 10, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(distance)).floatValue());
		limitArray[0] = latitude - degress;
		limitArray[1] = latitude + degress;
		return limitArray;
	}

	/**
	 * @param f_latitude
	 * @param f_longitude
	 * @param distance
	 * @return
	 * @Title: getLongitudeRange、
	 * @Description:根据距离计算经度的范围
	 * @return: Integer[]
	 */
	public static Integer[] getLongitudeRange(float f_latitude, float f_longitude, int distance) {
		//计算纬线的周长
		long latitudeLength = Double.valueOf(
			//赤道纬线长度
			40075360 * Math.sin(Math.toRadians(90 - Float.valueOf(f_latitude)))
		).intValue();
		int longitude = getIntValueByTimes(f_longitude);
		Integer[] limitArray = new Integer[2];
		int degress = getIntValueByTimes(
			new BigDecimal(360).divide(new BigDecimal(latitudeLength), 10, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(distance)).floatValue());
		limitArray[0] = longitude - degress;
		limitArray[1] = longitude + degress;
		return limitArray;
	}

	/**
	 * @param f
	 * @return
	 * @Title: getIntValueByTimes、
	 * @Description:获取放大倍数的整型值
	 * @return: int
	 */
	public static int getIntValueByTimes(float f) {
		Float F = f * LATITUDE_INT;
		int i = F.intValue();
		return i;
	}

	private static final int latitudeMeter = 110940;//纬度 一度多少米

	/**
	 * @param m_latitude
	 * @param m_longitude
	 * @param latitude
	 * @param longitude
	 * @return
	 * @Title: getDistance、
	 * @Description:根据经纬度计算距离
	 * @return: int
	 */
	public static int getDistance(float m_latitude, float m_longitude, long latitude, long longitude) {
		//计算纬度相差的距离: （纬度（放大）-纬度）/放大倍数×单位 去绝对值
		int latitudeDistance = new BigDecimal(getIntValueByTimes(m_latitude) - latitude).divide(new BigDecimal(LATITUDE_INT), 10, BigDecimal.ROUND_HALF_UP)
			.multiply(new BigDecimal(latitudeMeter)).abs().intValue();
		//计算纬线长度 :赤道长×sin（90-纬度）
		long latitudeLength = Double.valueOf(
			//赤道纬线长度
			40075360 * Math.sin(Math.toRadians(90 - Float.valueOf(m_latitude)))
		).intValue();
		//计算纬度单位 1度？米 
		BigDecimal longitudeMeter = new BigDecimal(latitudeLength).divide(new BigDecimal(360), 6, BigDecimal.ROUND_HALF_UP);
		//计算经度相差的距离: （经度（放大）-经度）/放大倍数×单位 去绝对值
		int longitudeDistance = new BigDecimal(getIntValueByTimes(m_longitude) - longitude).divide(new BigDecimal(LATITUDE_INT), 10, BigDecimal.ROUND_HALF_UP)
			.multiply(longitudeMeter).abs().intValue();
		//计算两点距离
		double distance = Math.sqrt(Math.pow(latitudeDistance, 2) + Math.pow(longitudeDistance, 2));
		return Double.valueOf(distance).intValue();
	}

	/**
	 * getStockPriceXs
	 *
	 * @param secuCategory
	 * @param secuMarket
	 * @return
	 * @Description 根据证券类型或证券市场类型获取价格小数位数
	 */
	public static int getStockPriceXs(int secuCategory, int secuMarket) {
		return secuCategory == MeixConstants.ZQ_JJ || secuMarket == MeixConstants.MARKET_GG ? 3 : 2;
	}

	public static void main(String[] args) {
		Integer[] limitArray = getLatitudeRange(31.201012f, 3000);
		Integer[] limitArray2 = getLongitudeRange(31.201012f, 121.43263f, 2000);
		System.out.println(limitArray[0].intValue());
		System.out.println(limitArray[1].intValue());
		System.out.println(limitArray2[0].intValue());
		System.out.println(limitArray2[1].intValue());
		System.out.println(getDistance(31.19466f, 121.42617f, 3120151, 12143308));
		System.out.println(defeatPercent(2, 100));
	}
}
