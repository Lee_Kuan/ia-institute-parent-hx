package com.meix.institute.util;


import com.google.gson.*;
import com.google.gson.internal.bind.ObjectTypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.meix.institute.gson.GsonExclusionStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GsonUtil {
	private static Logger logger = LoggerFactory.getLogger(GsonUtil.class.getName());
	//	private static Gson serializeGson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
//		.serializeNulls().create();
	private static Gson gson = new GsonBuilder().registerTypeAdapter(new TypeToken<Map<String, Object>>() {
	}.getType(), new GsonTypeAdapter()).setDateFormat("yyyy-MM-dd HH:mm:ss")
		//.excludeFieldsWithoutExposeAnnotation()
		// 开启 排除 不进行序列化的 属性
		.addDeserializationExclusionStrategy(new GsonExclusionStrategy())
		.serializeNulls().create();

	public static String obj2Json(Object obj) {
		if (obj == null) {
			return null;
		}
		try {
			return gson.toJson(obj);
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;
	}

	public static <T> T json2Obj(String json, Class<T> c) {
		try {
			if (json == null || json.isEmpty()) {
				return null;
			}
			return gson.fromJson(json, c);
		} catch (Exception e) {
			logger.error("解析报错：{}", json);
			logger.error("", e);
			return null;
		}
	}

	public static Map<String, Object> json2Map(String json) {
		try {
			if (json == null || json.isEmpty()) {
				return null;
			}
			TypeToken<Map<String, Object>> typeToken = new TypeToken<Map<String, Object>>() {
			};
			return gson.fromJson(json, typeToken.getType());
		} catch (Exception e) {
			logger.info("json2Obj解析报错，json:{}", json);
			logger.error("", e);
			return null;
		}
	}

	static public List<Map<String, Object>> json2MapList(String str) {
		if (str == null || str.isEmpty()) {
			return null;
		}
		try {
			JsonArray array = new JsonParser().parse(str).getAsJsonArray();
			List<Map<String, Object>> list = new ArrayList<>();
			TypeToken<Map<String, Object>> typeToken = new TypeToken<Map<String, Object>>() {
			};
			for (final JsonElement elem : array) {
				list.add(gson.fromJson(elem, typeToken.getType()));
			}
			return list;
		} catch (Exception e) {
			logger.error("解析报错：{}", str);
			logger.error("", e);
			return null;
		}
	}

	public static <T> T json2Obj(String json, TypeToken<T> c) {
		try {
			if (json == null || json.isEmpty()) {
				return null;
			}
			return gson.fromJson(json, c.getType());
		} catch (Exception e) {
			logger.info("json2Obj解析报错，json:{}", json);
			logger.error("", e);
			return null;
		}
	}


	static public <T> List<T> json2List(String str, final Class<T> c) {
		if (str == null || str.isEmpty()) {
			return null;
		}
		try {
			JsonArray array = new JsonParser().parse(str).getAsJsonArray();
			List<T> list = new ArrayList<T>();
			for (final JsonElement elem : array) {
				list.add(gson.fromJson(elem, c));
			}
			return list;
		} catch (Exception e) {
			logger.error("解析报错：{}", str);
			logger.error("", e);
			return null;
		}
	}
	
	static public <E, T> List<E> list2OtherList(List<T> list, final Class<E> e) {
        if (list == null) {
            return null;
        }
        List<E> otherlist = new ArrayList<>();
        for (Object info : list) {
            E obj = json2Obj(gson.toJson(info), e);
            otherlist.add(obj);
        }
        return otherlist;
    }

	static public class GsonTypeAdapter extends TypeAdapter<Object> {
		@Override
		public Object read(JsonReader in) throws IOException {
			// 反序列化
			JsonToken token = in.peek();
			switch (token) {
				case BEGIN_ARRAY:

					List<Object> list = new ArrayList<Object>();
					in.beginArray();
					while (in.hasNext()) {
						list.add(read(in));
					}
					in.endArray();
					return list;

				case BEGIN_OBJECT:

					Map<String, Object> map = new HashMap<String, Object>();
					in.beginObject();
					while (in.hasNext()) {
						map.put(in.nextName(), read(in));
					}
					in.endObject();
					return map;

				case STRING:

					return in.nextString();

				case NUMBER:

					/**
					 * 改写数字的处理逻辑，将数字值分为整型与浮点型。
					 */
					double dbNum = in.nextDouble();

					// 数字超过long的最大值，返回浮点类型
					if (dbNum > Long.MAX_VALUE) {
						return dbNum;
					}

					// 判断数字是否为整数值
					long lngNum = (long) dbNum;
					if (dbNum == lngNum) {
						return lngNum;
					} else {
						return dbNum;
					}

				case BOOLEAN:
					return in.nextBoolean();

				case NULL:
					in.nextNull();
					return null;

				default:
					throw new IllegalStateException();
			}
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public void write(JsonWriter out, Object value) throws IOException {
			// 序列化(参考ObjectTypeAdapter处理，不处理则会报错)
			if (value == null) {
				out.nullValue();
			} else {
				TypeAdapter typeAdapter = gson.getAdapter(value.getClass());
				if (typeAdapter instanceof ObjectTypeAdapter) {
					out.beginObject();
					out.endObject();
				} else {
					typeAdapter.write(out, value);
				}
			}
		}
	}
}
