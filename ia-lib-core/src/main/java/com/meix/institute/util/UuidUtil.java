package com.meix.institute.util;

import java.util.UUID;

/**
 * UUID 生成器
 * @author Xueyc
 * @date 2019-8-1 16:10:51
 */
public class UuidUtil {
    
    /**
     * 生成32位UUID
     *
     * @return uuid(32位)
     */
    public static String generate32bitUUID() {
        
        synchronized (UuidUtil.class) {
            String randomUUID = UUID.randomUUID().toString();
            return randomUUID.toUpperCase().replace("-", "");
        }
    }
    
    public static void main(String[] args) {
        System.out.println(generate32bitUUID());
    }
}

