package com.meix.institute.util;

import com.meix.institute.constant.MeixConstants;
import com.meix.institute.vo.activity.ActivityAnalystVo;
import com.meix.institute.vo.activity.InsActivityVo;
import com.meix.institute.vo.company.CompanyInfoVo;
import com.meix.institute.vo.report.ReportDetailVo;
import com.meix.institute.vo.user.UserInfo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @版权信息 :上海睿涉取有限公司
 * @部门 :开发部
 * @作者 :zhouwei
 * @E-mail : zhouwei@51research.com
 * @创建日期 : 2015年11月13日
 * @Description:消息工具类
 */
public class MessageUtil {
	/**
	 * 单人聊天提醒
	 **/
	public static final String singleChatRemind = "您聊天的对象是<b>#{user}</b>,请注意聊天内容的合规;点击该用户的头像可以了解对方公司的详情。";
	/**
	 * 群聊天提醒
	 **/
	public static final String groupChatRemind = "本群有<b>#{user}</b>,请注意聊天内容的合规;点击该用户的头像可以了解对方公司的详情。";
	/**
	 * 授权个人提醒
	 **/
	public static final String authorizePersonRemind = "您正在授权给<b>#{user}</b>;点击该用户的头像可以了解对方公司的详情。";
	/**
	 * 授权机构提醒
	 **/
	public static final String authorizeOrgRemind = "您正在授权给<b>#{user}</b>,请注意合规 ;点击该用户的头像可以了解对方公司的详情。";
	/**
	 * 加人提醒
	 **/
	public static final String addBuddy = "您要加的人有<b>#{user}</b>;点击该用户的头像可以了解对方公司的详情。";
	/**
	 * 加群提醒
	 **/
	public static final String addGroupRemind = "您要加的人里有<b>#{user}</b>,请注意聊天内容的合规。";

	/**
	 * @param activityType
	 * @param title
	 * @return
	 * @Title: getActivityTitle、
	 * @Description:获取活动标题
	 * @return: String
	 */
	public static String getActivityTitle(int activityType, String title) {
		switch (activityType) {
			case MeixConstants.CHAT_MESSAGE_DY:
				title = "[调研]" + title;
				break;
			case MeixConstants.CHAT_MESSAGE_DHHY:
				title = "[电话会议]" + title;
				break;
			case MeixConstants.CHAT_MESSAGE_LY:
				title = "[路演]" + title;
				break;
			case MeixConstants.CHAT_MESSAGE_CL:
				title = "[策略会]" + title;
				break;
			case MeixConstants.CHAT_MESSAGE_HY:
				title = "[会议]" + title;
				break;
			default:
				break;
		}
		return title;
	}

	/**
	 * @param companyAbbr
	 * @param userName
	 * @return
	 * @Title: getSingleChatRemind、
	 * @Description:单人聊天提醒
	 * @return: String
	 */
	public static String getSingleChatRemind(String companyAbbr, String userName, String companyTypeDesc) {
		return singleChatRemind.replace("#{user}", companyAbbr + "的" + userName + ",属于" + companyTypeDesc);
	}

	/**
	 * @param activityTitle
	 * @param flag
	 * @return
	 * @Title: getActivityRemind、
	 * @Description:活动提醒
	 * @return: String
	 */
	public static String getActivityRemind(String activityTitle, int flag) {
		String message = "";
		if (flag == MeixConstants.MESSAGE_TYPE_HDBG) {//修改
			message = "请注意，您报名的《%s》，活动信息发生变更，请及时查看。";
		} else {
			message = "很抱歉，您报名的《%s》，因故取消。";
		}
//    	String activityDesc="";
//    	switch (activityType) {
//		case MeixConstants.CHAT_MESSAGE_DY:
//			activityDesc="调研";
//			break;
//		case MeixConstants.CHAT_MESSAGE_LY:
//			activityDesc="路演";
//			break;
//		case MeixConstants.CHAT_MESSAGE_DHHY:
//			activityDesc="电话会议";
//			break;
//		case MeixConstants.CHAT_MESSAGE_CL:
//			activityDesc="策略会";
//			break;
//		case MeixConstants.CHAT_MESSAGE_HY:
//			activityDesc="会议";
//			break;
//		default:
//			break;
//		}
		return String.format(message, activityTitle);
	}

	/**
	 * @param ybfl 研报分类
	 * @param ybsx 研报属性
	 * @param yjdx 研究对象
	 * @return
	 * @Title: getReportClass、
	 * @Description:
	 * @return: String[]
	 */
	@SuppressWarnings("rawtypes")
	public static Set[] getReportClass(int ybfl, List<Integer> sjdx, List<Integer> ybsx, List<Integer> yjdx) {
		Set[] results = new Set[2];
		Set<String> yjfl = new HashSet<String>();
		Set<String> ejfl = new HashSet<String>();
		int v_sjdx = 0;
		if (sjdx != null && sjdx.size() > 0) {
			v_sjdx = sjdx.get(0);
		}
		String s_ejfl = null;
		switch (ybfl) {
			case 2:
				yjfl.add("公司");
				switch (v_sjdx) {
					case 7:
						ejfl.add("季报点评");
						break;
					case 8:
						ejfl.add("半年报点评");
						break;
					case 9:
						ejfl.add("年报点评");
						break;
					default:
						if (ybsx != null && ybsx.size() > 0) {
							for (Integer v_ybsx : ybsx) {
								if (v_ybsx == 2 && v_sjdx == 0) {
									ejfl.add("深度报告");
								} else if (v_ybsx == 3 && v_sjdx == 0) {
									ejfl.add("调研报告");
								} else if ((v_ybsx == 9 || v_ybsx == 10) && v_sjdx == 0) {
									ejfl.add("新股点评");
								} else {
									ejfl.add("简评");
								}
							}
						} else {
							ejfl.add("简评");
						}
						break;
				}
				break;
			case 3:
				yjfl.add("行业");
				s_ejfl = getEJFL(v_sjdx);
				if (s_ejfl == null || s_ejfl.equals("")) {
					if (ybsx != null && ybsx.size() > 0) {
						for (Integer v_ybsx : ybsx) {
							if (v_ybsx == 2) {
								ejfl.add("深度报告");
							} else if (v_ybsx == 1) {
								ejfl.add("策略报告");
							} else {
								ejfl.add("简评");
							}
						}
					} else {
						ejfl.add("简评");
					}
				} else {
					ejfl.add(s_ejfl);
				}
				break;
			case 4:
				yjfl.add("宏观");
				s_ejfl = getEJFL(v_sjdx);
				if (s_ejfl == null || s_ejfl.equals("")) {
					if (ybsx != null && ybsx.size() > 0) {
						for (Integer v_ybsx : ybsx) {
							if (v_ybsx == 2) {
								ejfl.add("深度报告");
							} else {
								ejfl.add("简评");
							}
						}
					} else {
						ejfl.add("简评");
					}
				} else {
					ejfl.add(s_ejfl);
				}
				break;
			case 5:
				if (yjdx != null && yjdx.size() > 0) {
					for (int v_yjdx : yjdx) {
						if (v_yjdx >= 11001 && v_yjdx <= 11007) {
							yjfl.add("股票市场");
							s_ejfl = getEJFL(v_sjdx);
							if (s_ejfl == null || s_ejfl.equals("")) {
								if (ybsx != null && ybsx.size() > 0) {
									for (Integer v_ybsx : ybsx) {
										if (v_ybsx == 2) {
											ejfl.add("深度报告");
										} else if (v_ybsx == 6) {
											ejfl.add("金融工程");
										} else {
											ejfl.add("简评");
										}
									}
								} else {
									ejfl.add("简评");
								}
							} else {
								ejfl.add(s_ejfl);
							}
						} else if (v_yjdx >= 14001 && v_yjdx <= 14017) {
							yjfl.add("固收市场");
							if (v_sjdx > 0) {
								ejfl.add("定期报告");
							} else {
								if (ybsx != null && ybsx.size() > 0) {
									for (Integer v_ybsx : ybsx) {
										if (v_ybsx == 2) {
											ejfl.add("深度报告");
										} else {
											ejfl.add("简评");
										}
									}
								} else {
									ejfl.add("简评");
								}
							}
						} else if (v_yjdx >= 12001 && v_yjdx <= 13006) {
							yjfl.add("基金市场");
							ejfl.add("基金报告");
						} else if (v_yjdx == 19003) {
							yjfl.add("股票市场");
							ejfl.add("新三板");
						} else if (v_yjdx == 19006) {
							yjfl.add("股票市场");
							ejfl.add("科创板");
						}
					}
				}
				break;
			case 1:
				yjfl.add("晨会");
				ejfl.add("晨会");
				break;
			case 6:
				yjfl.add("其他");
				ejfl.add("山证动态");
				break;
			default:
				break;
		}
		results[0] = yjfl;
		results[1] = ejfl;
		return results;
	}

	/**
	 * @param v_sjdx
	 * @return
	 * @Title: getEJFL、
	 * @Description:获取二级分类
	 * @return: String
	 */
	public static String getEJFL(int v_sjdx) {
		switch (v_sjdx) {
			case 1:
				return "日评";
			case 2:
				return "周评";
			case 3:
				return "周评";
			case 4:
				return "周评";
			case 5:
				return "月度报告";
			case 6:
				return "月度报告";
			case 7:
				return "季度报告";
			case 8:
				return "半年度报告";
			case 9:
				return "年度报告";
			default:
				return "";
		}
	}

	/**
	 * 根据服务类型ID获取服务类型名称
	 *
	 * @param type
	 * @return
	 */
	public static String getServiceTypeName(int type) {
		switch (type) {
			case 1:
				return "重大事项通知";
			case 2:
				return "买卖点及逻辑咨询";
			case 3:
				return "股价波动探讨";
			case 4:
				return "临时咨询";
			default:
				return "";
		}
	}

	//附近的活动通知
	public static String getNearbyActivityMessage(String activityName) {
		return "[活动]" + activityName;
	}

	/**
	 * 电话会议推送内容解析
	 *
	 * @param activityTitle
	 * @return
	 * @author likuan
	 */
	public static String getSubscribeActivityRemind(String activityTitle) {
		String message = "您预约的电话会议《%s》，将在30分钟后开始，请注意参加。";
		return String.format(message, activityTitle);
	}

	/**
	 * 研究所权限申请通知
	 *
	 * @return
	 */
	public static String getInstitutePermSalerNotice(UserInfo reqUser, InsActivityVo vo, boolean hasHeaderAndFooter) {
		String content = "";
		if (hasHeaderAndFooter) {
			content += "您有新的客户申请，";
		}
		if (StringUtil.isNotEmpty(reqUser.getCompanyAbbr())) {
			content += reqUser.getCompanyAbbr() + "的";
			if (StringUtil.isNotEmpty(reqUser.getPosition())) {
				content += reqUser.getPosition();
			}
		}
		content += reqUser.getUserName() + "，手机号为" + reqUser.getMobile() + "，申请";
		switch (vo.getActivityType()) {
			case MeixConstants.CHAT_MESSAGE_DY: {
				String time = DateUtil.dateToStr(vo.getStartTime(), "yyyy年M月d号");
				content += "参加";
				content += time + "名为《" + vo.getTitle() + "》的调研活动。";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_LY: {
				String time = DateUtil.dateToStr(vo.getStartTime(), "yyyy年M月d号");
				content += "参加";
				content += time + "名为《" + vo.getTitle() + "》的路演活动。";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_CL: {
				String time = DateUtil.dateToStr(vo.getStartTime(), "yyyy年M月d号");
				content += "参加";
				content += time + "名为《" + vo.getTitle() + "》的策略会活动。";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_DHHY: {
				content += "收听";
				List<ActivityAnalystVo> analyst = vo.getAnalyst();
				if (analyst != null && analyst.size() > 0) {
					String name = analyst.get(0).getName();
					content += "分析师" + name + "的";
				}
				content += "白名单电话会议《" + vo.getTitle() + "》。";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_HY: {
				String time = DateUtil.dateToStr(vo.getStartTime(), "yyyy年M月d号");
				content += "参加";
				content += time + "名为《" + vo.getTitle() + "》的活动。";
				break;
			}
			default:
				return null;
		}
		if (hasHeaderAndFooter) {
			content += "请尽快联系客户确认！";
		}
		return content;
	}

	public static String getInstituteWechatSalerTitle(InsActivityVo vo) {
		String title = "您有新的客户申请";
		switch (vo.getActivityType()) {
			case MeixConstants.CHAT_MESSAGE_DY: {
				title += "参加调研活动!";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_LY: {
				title += "参加路演活动!";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_CL: {
				title += "参加策略会活动!";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_DHHY: {
				title += "收听白名单电话会议!";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_HY: {
				title += "参加活动!";
				break;
			}
			default:
				return null;
		}
		return title;
	}

	/**
	 * 研报权限申请
	 *
	 * @param reqUser
	 * @param bean
	 * @param hasHeaderAndFooter
	 * @return
	 */
	public static String getInstitutePermSalerNotice(UserInfo reqUser, ReportDetailVo bean, boolean hasHeaderAndFooter) {
		String content = "";
		if (hasHeaderAndFooter) {
			content += "您有新的客户申请，";
		}
		if (StringUtil.isNotEmpty(reqUser.getCompanyAbbr())) {
			content += reqUser.getCompanyAbbr() + "的";
			if (StringUtil.isNotEmpty(reqUser.getPosition())) {
				content += reqUser.getPosition();
			}
		}
		content += reqUser.getUserName() + "，手机号为" + reqUser.getMobile() + "，申请查看名为《" + bean.getTitle() + "》的研报原文。";
		if (hasHeaderAndFooter) {
			content += "请尽快联系客户确认！";
		}
		return content;
	}

	public static String getInstitutePermUserNotice(CompanyInfoVo companyInfoCache, InsActivityVo vo, boolean hasHeaderAndFooter) {
		String content = "您申请";
		switch (vo.getActivityType()) {
			case MeixConstants.CHAT_MESSAGE_DY: {
				String time = DateUtil.dateToStr(vo.getStartTime(), "yyyy年M月d号");
				content += "参加了";
				if (hasHeaderAndFooter) {
					content += companyInfoCache.getCompanyAbbr();
				}
				content += time + "名为《" + vo.getTitle() + "》的调研活动。";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_LY: {
				String time = DateUtil.dateToStr(vo.getStartTime(), "yyyy年M月d号");
				content += "参加了";
				if (hasHeaderAndFooter) {
					content += companyInfoCache.getCompanyAbbr();
				}
				content += time + "名为《" + vo.getTitle() + "》的路演活动。";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_CL: {
				String time = DateUtil.dateToStr(vo.getStartTime(), "yyyy年M月d号");
				content += "参加了";
				if (hasHeaderAndFooter) {
					content += companyInfoCache.getCompanyAbbr();
				}
				content += time + "名为《" + vo.getTitle() + "》的策略会活动。";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_DHHY: {
				content += "收听了";
				List<ActivityAnalystVo> analyst = vo.getAnalyst();
				if (analyst != null && analyst.size() > 0) {
					String name = analyst.get(0).getName();
					content += "分析师" + name + "的";
				}
				content += "白名单电话会议《" + vo.getTitle() + "》。";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_HY: {
				String time = DateUtil.dateToStr(vo.getStartTime(), "yyyy年M月d号");
				content += "参加了";
				if (hasHeaderAndFooter) {
					content += companyInfoCache.getCompanyAbbr();
				}
				content += time + "名为《" + vo.getTitle() + "》的活动。";
				break;
			}
			default:
				return null;
		}
		if (hasHeaderAndFooter) {
			content += "请您注意查收对口销售参会成功与否的提醒！";
		}
		return content;
	}

	public static String getInstituteWechatUserTitle(CompanyInfoVo companyInfoCache, InsActivityVo vo) {
		String title = "您申请";
		switch (vo.getActivityType()) {
			case MeixConstants.CHAT_MESSAGE_DY: {
				title += "参加了" + companyInfoCache.getCompanyAbbr() + "调研活动!";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_LY: {
				title += "参加了" + companyInfoCache.getCompanyAbbr() + "路演活动!";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_CL: {
				title += "参加了" + companyInfoCache.getCompanyAbbr() + "策略会活动!";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_DHHY: {
				title += "收听了" + companyInfoCache.getCompanyAbbr() + "白名单电话会议!";
				break;
			}
			case MeixConstants.CHAT_MESSAGE_HY: {
				title += "参加了" + companyInfoCache.getCompanyAbbr() + "活动!";
				break;
			}
			default:
				return null;
		}
		return title;
	}

	public static String getInstitutePermUserNotice(ReportDetailVo bean, boolean hasHeaderAndFooter) {
		String content = "您申请查看名为《" + bean.getTitle() + "》的研报原文。";
		if (hasHeaderAndFooter) {
			content += "请您注意查收对口销售审核结果的提醒！";
		}
		return content;
	}

	public static String getAssignSalerNotice(UserInfo userInfo, int clueType, boolean isNewClue) {
		String note = "指派";
//		if (isNewClue) {
//			note = "推送";
//		}
		String content = "您有新的商机" + note;
		content += "【";
		content += userInfo.getUserName();
		if (StringUtil.isNotEmpty(userInfo.getCompanyName())) {
			content += "，" + userInfo.getCompanyName();
		}
		if (StringUtil.isNotEmpty(userInfo.getPosition())) {
			content += "，" + userInfo.getPosition();
		}
		if (StringUtil.isNotEmpty(userInfo.getMobile()) && !userInfo.getMobile().startsWith(UserInfo.MOBILE_PREFIX)) {
			content += "，手机号：" + userInfo.getMobile();
		}
		content += "】。";
//		String clueAction = MeixConstants.getClueAction(clueType);
//		if (StringUtil.isNotEmpty(clueAction)) {
//			content += "，商机行为：" + clueAction + "。";
//		}
		content += "请注意覆盖！";
		return content;
	}

	public static String getActivityExamineNotice(String activityTitle, int activityType, int optType, String reason) {
		String type = MeixConstants.getCNActivityType(activityType);
		if (StringUtil.isBlank(type)) {
			return null;
		}
		String result = "";
		switch (optType) {
			case 2:
				result = "审核不通过";
				break;
			case 3:
				result = "已被撤销";
				break;
			default:
				return null;
		}
		String content = "您发布的";
		content += type;
		content += "《" + activityTitle + "》";
		content += result + "。";
		content += "原因：" + reason + "。";
		content += "请尽快确认修改！";
		return content;
	}

	/**
	 * 新增活动，发送给管理员的审批通知信息内容
	 *
	 * @param activityTitle
	 * @param activityType
	 * @return
	 */
	public static String getActivityAddToAdminNotice(String activityTitle, int activityType) {
		String type = MeixConstants.getCNActivityType(activityType);
		if (StringUtil.isBlank(type)) {
			return null;
		}
		String content = "您有新的研究资源正等待审核，请尽快处理";
		return content;
	}

	public static String getInsActivityWxNotice(int activityType, String title, String startTime) {
		String cnType = MeixConstants.getCNActivityType(activityType);
		String time = DateUtil.dateToStr(DateUtil.stringToDate(startTime), "yyyy年M月d日 HH:mm");
		String content = "%s《%s》，将在%s开始。请准时参加！";
		return String.format(content, cnType, title, time);
	}
}
