package com.meix.institute.util;

import com.meix.institute.constant.MeixConstants;

public class SecurityUtil {

	public static String[] getTokenArray(String token) {
		String str = Des.decrypt(token);
		if (StringUtil.isBlank(str)) {
			return null;
		}
		return str.split(MeixConstants.tokenSplite);
	}

	/**
	 * 生成token
	 *
	 * @param uid
	 * @param user
	 * @param companyCode
	 * @param state
	 * @param clientType
	 * @return
	 */
	public static String generateLoginToken(long uid, String user, long companyCode, int state, int clientType) {
		String tokenstr = uid + MeixConstants.tokenSplite + user + MeixConstants.tokenSplite + companyCode
			+ MeixConstants.tokenSplite + state + MeixConstants.tokenSplite + clientType + MeixConstants.tokenSplite + System.currentTimeMillis();
		return Des.encrypt(tokenstr);
	}

	/**
	 * 根据token 获取用户id
	 *
	 * @param token
	 * @return
	 */
	public static long getUidByToken(String token) {
		if (StringUtils.isBlank(token)) {
			return 0l;
		}
		String decrypt = Des.decrypt(token);
		if (StringUtils.isBlank(decrypt)) {
			return 0l;
		}
		String[] split = decrypt.split(MeixConstants.tokenSplite);

		return Long.valueOf(split[0]);
	}

	/**
	 * 根据token 获取用户user
	 *
	 * @param token
	 * @return
	 */
	public static String getUserByToken(String token) {
		if (StringUtils.isBlank(token)) {
			return null;
		}
		String decrypt = Des.decrypt(token);
		if (StringUtils.isBlank(decrypt)) {
			return null;
		}
		String[] split = decrypt.split(MeixConstants.tokenSplite);

		return split[1];
	}
}
