package com.meix.institute.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
    
    /**
     * MD5加密
     *
     * @param str 被加密字符串
     * @return MD5加密后的32位密文
     */
    public static String md5(String str) {
        if (str == null) {
            return "";
        }

        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            return "";
        }
        return StringUtils.byteArrayToHexString(md.digest(str.getBytes()));
    }
    
    public static void main(String[] args) {
        String md5 = md5("m#&tl" + "xyc17621502522"); 
        String md52 = md5("vv)(l" + "zz1391761785"); 
        String md53 = md5("*&65$" + "qht13061689182"); 
        System.out.println(md5); //f47c7ad28d6b9dfb9d8ee07028a43dce
        System.out.println(md52);
        System.out.println(md53);
    }
}
