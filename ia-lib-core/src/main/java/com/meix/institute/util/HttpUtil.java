package com.meix.institute.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.config.RequestConfig.Builder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.meix.institute.constant.MeixConstants;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import net.sf.json.JSONObject;
/**
 * @版权信息: 上海睿涉取有限公司
 * @部门 ：开发部
 * @作者 ：zhouwei
 * @E-mail : zhouwei@51research.com
 * @创建日期: 2014-5-12下午2:12:29
 */
@SuppressWarnings("deprecation")
public class HttpUtil {
	private static Logger log = LoggerFactory.getLogger(HttpUtil.class);
	public static String emailServer = null;

	public static byte[] getHttp(String url) {
		if (StringUtils.indexOf(url.toUpperCase(), "HTTPS") == 0) {
			return getHttps(url, 5000);
		} else {
			return getHttp(url, 5000);
		}
	}

	/**
	 * @param url
	 * @return
	 * @Title: getWechat、
	 * @Description:调用微信接口
	 * @return: JSONObject
	 */
	public static JSONObject getWechat(String url) {
		byte[] bytes = HttpUtil.getHttp(url);
		if (bytes == null) {
			return null;
		}
		JSONObject result = JSONObject.fromObject(new String(bytes));
		log.info("微信 url:[" + url + "],result:[" + result.toString() + "]");
		int errorCode = MapUtils.getIntValue(result, "errcode", 0);
		if (errorCode > 0) {
			return null;
		}
		return result;
	}

	public static String getHttp(String url, Map<String, Object> params, int timeout) {
		StringBuilder builder = new StringBuilder();
		String json = null;
		try {
			if (!MapUtils.isEmpty(params)) {
				Iterator<String> it = params.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					builder.append(key).append("=").append(params.get(key).toString()).append("&");
				}
			}
			String paramstr = builder.toString();
			if (paramstr.length() > 0) {
				url = url + "?" + paramstr.substring(0, paramstr.length() - 1);
			}
			byte[] resp = getHttp(url, timeout);
			if (resp == null) {
				return null;
			}
			json = new String(resp);
		} catch (Exception e) {
			log.error(url + "请求失败:", e);
		}
		return json;
	}

	/**
	 * @方法信息：2014-5-12 zhouwei
	 * @功能介绍：获取get请求数据
	 */
	public static byte[] getHttp(String url, int _marketTimeOut) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		byte[] bytes = null;
		try {
			RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(_marketTimeOut)
				.setConnectTimeout(_marketTimeOut).build();// 设置请求和传输超时时间
			// 创建GET方法的实例
			HttpGet httpGet = new HttpGet(url);
			httpGet.setConfig(requestConfig);
			// 发起请求 并返回请求的响应
			response = httpClient.execute(httpGet);
			// 获取响应对象
			HttpEntity resEntity = response.getEntity();
			if (resEntity != null) {
				// 读取内容
				bytes = EntityUtils.toByteArray(resEntity);
			}
			// 销毁
			EntityUtils.consume(resEntity);
			return bytes;
		} catch (Exception e) {
			// 发生致命的异常，可能是协议不对或者返回的内容有问题
			log.error(url + "请求失败:", e);
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * @方法信息：2014-5-12 zhouwei
	 * @功能介绍：获取get请求数据
	 */
	public static byte[] getHttps(String url, int _marketTimeOut) {
		HttpClient client = null;
		HttpGet get = new HttpGet(url);
		byte[] bytes = null;
		try {
			// 设置参数
			Builder customReqConf = RequestConfig.custom();
			customReqConf.setConnectTimeout(_marketTimeOut);
			customReqConf.setSocketTimeout(_marketTimeOut);
			get.setConfig(customReqConf.build());

			HttpResponse res = null;

			// 执行 Https 请求.
			client = createSSLInsecureClient();
			res = client.execute(get);

			bytes = EntityUtils.toByteArray(res.getEntity());
			// 销毁
			EntityUtils.consume(res.getEntity());
		} catch (Exception e) {
			// 发生致命的异常，可能是协议不对或者返回的内容有问题
			log.error(url + "请求失败:", e);
		} finally {
			get.releaseConnection();
			if (client != null && client instanceof CloseableHttpClient) {
				try {
					((CloseableHttpClient) client).close();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					client = null;
				}
			}
		}
		return bytes;
	}

	/**
	 * @param url
	 * @return
	 * @Title: postHttp、
	 * @Description:post
	 * @return: JSONObject
	 */
	public static JSONObject postHttp(String url, String token, String paramStr) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		JSONObject json = null;
		try {
			// 创建POST方法的实例
			RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(30000)
				.setConnectTimeout(30000).build();// 设置请求和传输超时时间60s
			HttpPost httpPost = new HttpPost(url);
			httpPost.setConfig(requestConfig);
			httpPost.addHeader("Content-Type", "application/json");
			if (!StringUtils.isEmpty(token)) {
				httpPost.addHeader("Authorization", "Bearer " + token);
			}
			httpPost.setEntity(new StringEntity(paramStr, "UTF-8"));
			// 发起请求 并返回请求的响应
			response = httpClient.execute(httpPost);
			// 获取响应对象
			HttpEntity resEntity = response.getEntity();
			if (resEntity != null) {
				// 读取内容
				String reponseText = EntityUtils.toString(resEntity);
				json = JSONObject.fromObject(reponseText);
			}
			// 销毁
			EntityUtils.consume(resEntity);
			return json;
		} catch (Exception e) {
			// 发生致命的异常，可能是协议不对或者返回的内容有问题
			log.error(url + "请求失败:", e);
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}


	/**
	 * @param url
	 * @param headerMap 请求头
	 * @param params    参数
	 * @return byte[]
	 * @Title: postHttpsBasic、
	 * @Description: https 的 post请求
	 */
	public static JSONObject postHttpsBasic(String url, Map<String, String> headerMap, Map<String, Object> params, int timeout) {
		CloseableHttpResponse response = null;
		CloseableHttpClient httpClient = null;
		JSONObject result = null;
		try {
			httpClient = createSSLInsecureClient();
			HttpPost httpPost = new HttpPost(url);
			if (!MapUtils.isEmpty(headerMap)) {
				Iterator<String> it = headerMap.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					httpPost.addHeader(key, headerMap.get(key));
				}
			}
			List<NameValuePair> nvps = new ArrayList<>();
			if (!MapUtils.isEmpty(params)) {
				Iterator<String> it = params.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					nvps.add(new BasicNameValuePair(key, params.get(key).toString()));
				}
			}
			httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
			// 设置参数
			Builder customReqConf = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(timeout);
			httpPost.setConfig(customReqConf.build());
			// 发起请求 并返回请求的响应
			response = httpClient.execute(httpPost);
			// 获取响应对象
			HttpEntity resEntity = response.getEntity();
			if (resEntity != null) {
				// 读取内容
				String data = EntityUtils.toString(resEntity);
				result = JSONObject.fromObject(data);
			}
			// 销毁
			EntityUtils.consume(resEntity);
			return result;
		} catch (Exception e) {
			// 发生致命的异常，可能是协议不对或者返回的内容有问题
			log.error(url + "请求失败:", e);
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * @param url
	 * @param headerMap 请求头
	 * @param params    参数
	 * @return byte[]
	 * @Title: postHttpBasic、
	 * @Description: http 的 post请求
	 */
	public static byte[] postHttpBasic(String url, Map<String, String> headerMap, Map<String, String> params) {
		CloseableHttpResponse response = null;
		CloseableHttpClient httpClient = null;
		byte[] bytes = null;
		try {
			httpClient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
			if (!MapUtils.isEmpty(headerMap)) {
				Iterator<String> it = headerMap.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					httpPost.addHeader(key, headerMap.get(key));
				}
			}
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if (!MapUtils.isEmpty(params)) {
				Iterator<String> it = params.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					nvps.add(new BasicNameValuePair(key,
						params.get(key)));
				}
			}
			httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
//			httpPost.setEntity(new StringEntity(paramStr));
			// 设置参数
			Builder customReqConf = RequestConfig.custom();
			httpPost.setConfig(customReqConf.build());
			// 发起请求 并返回请求的响应
			response = httpClient.execute(httpPost);
			// 获取响应对象
			HttpEntity resEntity = response.getEntity();
			if (resEntity != null) {
				// 读取内容
				bytes = EntityUtils.toByteArray(resEntity);
			}
			// 销毁
			EntityUtils.consume(resEntity);
			return bytes;
		} catch (Exception e) {
			// 发生致命的异常，可能是协议不对或者返回的内容有问题
			log.error(url + "请求失败:", e);
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * @param url
	 * @param headerMap 请求头
	 * @param paramStr  参数
	 * @return byte[]
	 * @Title: postHttpBasic、
	 * @Description: http 的 post请求
	 */
	public static byte[] postHttpBasic(String url, Map<String, String> headerMap, String paramStr) {
		CloseableHttpResponse response = null;
		CloseableHttpClient httpClient = null;
		byte[] bytes = null;
		try {
			httpClient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
			if (!MapUtils.isEmpty(headerMap)) {
				Iterator<String> it = headerMap.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					httpPost.addHeader(key, headerMap.get(key));
				}
			}
			httpPost.setEntity(new StringEntity(paramStr));
			// 设置参数
			Builder customReqConf = RequestConfig.custom();
			httpPost.setConfig(customReqConf.build());
			// 发起请求 并返回请求的响应
			response = httpClient.execute(httpPost);
			// 获取响应对象
			HttpEntity resEntity = response.getEntity();
			if (resEntity != null) {
				// 读取内容
				bytes = EntityUtils.toByteArray(resEntity);
			}
			// 销毁
			EntityUtils.consume(resEntity);
			return bytes;
		} catch (Exception e) {
			// 发生致命的异常，可能是协议不对或者返回的内容有问题
			log.error(url + "请求失败:", e);
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static String httpPost(String connectUrl, String param, String authorization) {
		try {
			URL url = new URL(connectUrl);
			// 将url 以 open方法返回的urlConnection  连接强转为HttpURLConnection连接  (标识一个url所引用的远程对象连接)
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();// 此时cnnection只是为一个连接对象,待连接中
			// 设置连接输出流为true,默认false (post 请求是以流的方式隐式的传递参数)
			connection.setDoOutput(true);
			// 设置连接输入流为true
			connection.setDoInput(true);
			// 设置请求方式为post
			connection.setRequestMethod("POST");
			// post请求缓存设为false
			connection.setUseCaches(false);
			// 设置该HttpURLConnection实例是否自动执行重定向
			connection.setInstanceFollowRedirects(true);
			// 设置请求头里面的各个属性 (以下为设置内容的类型,设置为经过urlEncoded编码过的from参数)
			// application/x-JavaScript text/xml->xml数据 application/x-javascript->json对象 application/x-www-form-urlencoded->表单数据
			connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Accept-Charset", "UTF-8");
			connection.setRequestProperty("contentType", "UTF-8");
			connection.setRequestProperty("Authorization", authorization);
			// 建立连接 (请求未开始,直到connection.getInputStream()方法调用时才发起,以上各个参数设置需在此方法之前进行)
			connection.connect();
			// 创建输入输出流,用于往连接里面输出携带的参数,(输出内容为?后面的内容)
			DataOutputStream dataout = new DataOutputStream(connection.getOutputStream());
			// 将参数输出到连接
			if (StringUtils.isNotEmpty(param)) {
				dataout.write(param.getBytes("UTF-8"));
			}
			//dataout.writeBytes(URLEncoder.encode(param, "utf-8"));
			// 输出完成后刷新并关闭流
			dataout.flush();
			dataout.close(); // 重要且易忽略步骤 (关闭流,切记!)
			int code = connection.getResponseCode();
			if (200 == code) {
				// 连接发起请求,处理服务器响应  (从连接获取到输入流并包装为bufferedReader)
				BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				String line;
				StringBuilder sb = new StringBuilder(); // 用来存储响应数据
				// 循环读取流,若不到结尾处
				while ((line = bf.readLine()) != null) {
					sb.append(line);
				}
				bf.close();    // 重要且易忽略步骤 (关闭流,切记!)
				connection.disconnect(); // 销毁连接
//				logger.info(sb.toString(), "");
				return sb.toString();
			} else {
				log.info("返回码：{}", code);
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}


	/**
	 * @param paramStr
	 * @return
	 * @Title: getUrlParams、
	 * @Description:获取URL传参
	 * @return: Map<String,String>
	 */
	public static Map<String, String> getUrlParams(String paramStr) {
		paramStr = Des.decrypt(paramStr);
		Map<String, String> params = new HashMap<String, String>();
		List<org.apache.http.NameValuePair> list = URLEncodedUtils.parse(
			paramStr, Charset.forName("UTF-8"));
		for (int i = 0; i < list.size(); i++) {
			params.put(list.get(i).getName(), list.get(i).getValue());
		}
		return params;
	}

	/**
	 * 创建 SSL连接
	 *
	 * @return
	 * @throws GeneralSecurityException
	 */
	private static CloseableHttpClient createSSLInsecureClient() throws GeneralSecurityException {
		try {
			SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
				@Override
				public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					return true;
				}
			}).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}

			});
			return HttpClients.custom().setSSLSocketFactory(sslsf).build();
		} catch (GeneralSecurityException e) {
			throw e;
		}
	}


	public static String dealIP(String ip) {
		// 多次反向代理后会有多个IP值，第一个为真实IP。
		int index = ip.indexOf(',');
		if (index != -1) {
			return ip.substring(0, index);
		} else {
			return ip;
		}
	}

	public static String getHostIP() {
		try {
			InetAddress localHost = Inet4Address.getLocalHost();
			String ip = localHost.getHostAddress();
			return ip;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		return "";
	}

	public static byte[] post(String url, Map<String, Object> params, Map<String, String> headParam) {
		return url.startsWith("https") ? httpsPost_bytes(url, params, headParam) : httpPost_bytes(url, params, headParam);
	}

	@SuppressWarnings("rawtypes")
	private static byte[] httpsPost_bytes(String url, Map<String, Object> params, Map<String, String> headParam) {
		HttpPost httpPost = new HttpPost(url);
		Iterator entrys;
		if (!MapUtils.isEmpty(headParam)) {
			entrys = headParam.keySet().iterator();

			while (entrys.hasNext()) {
				String name = (String) entrys.next();
				httpPost.setHeader(name, (String) headParam.get(name));
			}
		}

		List<NameValuePair> formparams = new ArrayList<>();
		entrys = params.entrySet().iterator();

		while (entrys.hasNext()) {
			Map.Entry entry = (Map.Entry) entrys.next();
			formparams.add(new BasicNameValuePair(entry.getKey().toString(), entry.getValue().toString()));
		}

		byte[] response = null;

		try {
			CloseableHttpClient closeableHttpClient = createSSLInsecureClient();
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, "UTF-8");
			httpPost.setEntity(entity);
			HttpResponse httpResponse = closeableHttpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			if (httpEntity != null) {
				response = EntityUtils.toByteArray(httpEntity);
			}

			closeableHttpClient.close();
		} catch (Exception var11) {
			var11.printStackTrace();
			log.error("httpPost请求异常！e:{},url:{}", var11.getMessage(), url);
		}

		return response;
	}

	@SuppressWarnings("rawtypes")
	public static byte[] httpPost_bytes(String url, Map<String, Object> menus, Map<String, String> headParam) {
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		CloseableHttpClient closeableHttpClient = httpClientBuilder.build();
		HttpPost httpPost = new HttpPost(url);
		Iterator entrys;
		if (!MapUtils.isEmpty(headParam)) {
			entrys = headParam.keySet().iterator();

			while (entrys.hasNext()) {
				String name = (String) entrys.next();
				httpPost.setHeader(name, (String) headParam.get(name));
			}
		}

		List<NameValuePair> formparams = new ArrayList<>();
		entrys = menus.entrySet().iterator();

		while (entrys.hasNext()) {
			Map.Entry entry = (Map.Entry) entrys.next();
			formparams.add(new BasicNameValuePair(entry.getKey().toString(), entry.getValue().toString()));
		}

		byte[] response = null;

		try {
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, "UTF-8");
			httpPost.setEntity(entity);
			HttpResponse httpResponse = closeableHttpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			if (httpEntity != null) {
				response = EntityUtils.toByteArray(httpEntity);
			}
		} catch (Exception var20) {
			var20.printStackTrace();
			log.error("httpPost请求异常！e:{},url:{}", var20.getMessage(), url);
		} finally {
			try {
				closeableHttpClient.close();
				if (closeableHttpClient != null) {
					closeableHttpClient = null;
				}
			} catch (IOException var19) {
				var19.printStackTrace();
				log.error("httpPost关闭字节流异常！", var19);
			}

		}

		return response;
	}

	@SuppressWarnings("rawtypes")
	public static byte[] getHttps_bytes(String url, Map<String, String> headParam, int rTimeout) {
		HttpClient client = null;
		HttpGet get = new HttpGet(url);
		if (!MapUtils.isEmpty(headParam)) {
			Iterator var6 = headParam.keySet().iterator();

			while (var6.hasNext()) {
				String name = (String) var6.next();
				get.setHeader(name, (String) headParam.get(name));
			}
		}

		byte[] result = null;

		try {
			Builder customReqConf = RequestConfig.custom();
			customReqConf.setConnectTimeout(10000);
			customReqConf.setSocketTimeout(rTimeout == 0 ? 10000 : rTimeout);
			get.setConfig(customReqConf.build());
			HttpResponse res = null;
			client = createSSLInsecureClient();
			res = client.execute(get);
			result = EntityUtils.toByteArray(res.getEntity());
		} catch (Exception var45) {
			var45.printStackTrace();
			log.error("httpPost请求异常！e:{},url:{}", var45.getMessage(), url);
		} finally {
			get.releaseConnection();
			if (client != null && client instanceof CloseableHttpClient) {
				try {
					((CloseableHttpClient) client).close();
				} catch (IOException var43) {
					var43.printStackTrace();
				} finally {
					client = null;
				}
			}

		}

		return result;
	}
	
	public static JSONObject httpPostFile(File file, String url, JSONObject params) {
		try {
			CloseableHttpClient client = HttpClientBuilder.create().build();
			log.info("mng转发文件到{}", url);
			HttpPost postMethod = new HttpPost(url);
			postMethod.setHeader("clientType", String.valueOf(MeixConstants.CLIENT_TYPE_4));
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setSocketTimeout(30000).build();
			postMethod.setConfig(requestConfig);

			MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create()
				.setMode(HttpMultipartMode.RFC6532);    //文件中文名乱码
			multipartEntityBuilder.addTextBody("clientstr", params.toString());
			multipartEntityBuilder.addBinaryBody("file", file);
			HttpEntity httpEntity = multipartEntityBuilder.build();
			postMethod.setEntity(httpEntity);

			CloseableHttpResponse httpResponse = client.execute(postMethod);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
				StringBuffer buffer = new StringBuffer();
				String str = "";
				while (StringUtil.isNotEmpty(str = reader.readLine())) {
					buffer.append(str);
				}
				byte[] response = buffer.toString().getBytes();
				String result = new String(response);
				log.info("res:{}", result);
				return JSONObject.fromObject(result);
			} else {
				return null;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 读取结束后关闭返回流
	 * @param fileUrl
	 * @return
	 */
	public static InputStream getFileByHttp(String fileUrl) {
		InputStream is = null;
        try {
            URL url = new URL(fileUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // 设置连接超时时间
            conn.setConnectTimeout(30000);
            if (conn.getResponseCode() == 200) {
                is = conn.getInputStream();
            }
        } catch (Exception e) {
            log.warn("获取文件失败", e);
        }
        return is;
	}
	
	public static JSONObject httpPostFile_common(File file, String url, Map<String, Object> params) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpPost postMethod = new HttpPost(url);
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setSocketTimeout(30000).build();
            postMethod.setConfig(requestConfig);

            MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create()
                .setMode(HttpMultipartMode.RFC6532);    //文件中文名乱码
            multipartEntityBuilder.addBinaryBody("media", file);
            if (!MapUtils.isEmpty(params)) {
                Iterator<String> it = params.keySet().iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    multipartEntityBuilder.addTextBody(key, params.toString());
                }
            }
            HttpEntity httpEntity = multipartEntityBuilder.build();
            postMethod.setEntity(httpEntity);

            CloseableHttpResponse httpResponse = client.execute(postMethod);
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
                StringBuffer buffer = new StringBuffer();
                String str = "";
                while (StringUtil.isNotEmpty(str = reader.readLine())) {
                    buffer.append(str);
                }
                byte[] response = buffer.toString().getBytes();
                String result = new String(response);
                log.info("res:{}", result);
                return JSONObject.fromObject(result);
            } else {
                return null;
            }
        } catch (Exception e) {
            log.warn("httpPostFile_common 方法异常：", e);
        }
        return null;
    }
	
	
	public static InputStream smbGetIs(String remoteUrl, NtlmPasswordAuthentication auth) {
		InputStream is = null;
		try {
			SmbFile remoteFile = new SmbFile(remoteUrl, auth);
			if (remoteFile.exists()) {
				return new BufferedInputStream(new SmbFileInputStream(remoteFile));
			}
		} catch (Exception e) {
			log.info("获取smb:{}{}", "共享文件不存在", remoteUrl);
			e.printStackTrace();
		} finally {
			if (is != null ) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}

		return null;
	}
	
	public static byte[] smbGet(String remoteUrl, NtlmPasswordAuthentication auth) {
		byte[] bytes = {};
		InputStream is = null;
		try {
//			System.setProperty("jcifs.smb.client.dfs.disabled", "true");
			SmbFile remoteFile = new SmbFile(remoteUrl, auth);
			if (remoteFile.exists()) {
				is = remoteFile.getInputStream();
//				is = new BufferedInputStream(new SmbFileInputStream(remoteFile));
				bytes = IOUtils.toByteArray(is);
			} else {
				log.info("获取smb:{}{}", "共享文件不存在", remoteUrl);
			}
		} catch (Exception e) {
			log.info("获取smb:{}{}", "共享文件不存在", remoteUrl);
			e.printStackTrace();
		} finally {
			if (is != null ) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}
		return bytes;
	}

	/**
	 * @param remoteUrl
	 * @param fileName 文件名
	 * @param auth
	 * @param file
	 * @param deleteOnExist true-如果存在时删除，false-存在则重命名
	 * @return new fileName
	 */
	public static String smbPut(String remoteUrl, String fileName, NtlmPasswordAuthentication auth, File file, boolean deleteOnExist) {
		InputStream is = null;
		try {
			int tmp = 1;
			SmbFile remoteFile = new SmbFile(remoteUrl + fileName, auth);
			if (remoteFile.exists() && deleteOnExist) {
				remoteFile.delete();
			} else {
				while (remoteFile.exists()) {
					fileName = getNewFileUrl(fileName, tmp);
					tmp ++;
					remoteFile = new SmbFile(remoteUrl + fileName, auth);
				}
			}
			remoteFile.createNewFile();
			OutputStream os = remoteFile.getOutputStream();
			is = new FileInputStream(file);
			IOUtils.copy(is, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			log.info("上传文件异常:{}", remoteUrl);
			e.printStackTrace();
		} finally {
			if (is != null ) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}

		return fileName;
	}
	private static String getNewFileUrl(String fileName, int tmp) {
		String[] args = fileName.split("\\.");
		if (args.length == 1 || args.length == 2) {
			fileName = args[0] + "(" + tmp +")";
			if (args.length == 2) {
				fileName += "." + args[1];
			}
		} else {
			fileName += "(" + tmp +")";
		}
		return fileName;
	}
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException, IOException {
//    	String url = "https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=xjepCIi4WKKZ9trrUBoRUhmX&client_secret=hEohWQ07wPL5NLGTLqEp4GyrHYDsccCA";
//    	byte[] bytes = getHttp(url, 5000);
//    	System.out.println(new String(bytes));
		/*String access_token = "24.f43382485a3ac5de2372bd770fa9096d.2592000.1499393889.282335-9736114";
		String url = "https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic?access_token=" + access_token;
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/x-www-form-urlencoded");
		Map<String, String> params = new HashMap<String, String>();
		params.put("image", "");
		byte[] bytes = postHttpsBasic(url, headerMap, params);
		System.out.println(new String(bytes));*/

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("action", "getStaffs");
		String paramStr = jsonObj.toString();
		JSONObject postHttp = postHttp("https://ri-ci.geekthings.com.cn/open/user", null, paramStr);
		System.out.println(postHttp);
	}
}
