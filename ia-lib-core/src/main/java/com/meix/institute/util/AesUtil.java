package com.meix.institute.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Created by zenghao on 2019/9/24.
 */
@SuppressWarnings("restriction")
public class AesUtil {

	public static final String format = "AES/CBC/NoPadding";
	public static String key = "12345645678iaweb";
	public static String iv = "1234567812345678";

	public AesUtil() {
	}

	public static void main(String[] args) throws Exception {
//		String test1 = "/iaweb/passwordLogin.do";
//		String test1 = "{\"token\":\"DA11F553AEB9A334F7034C87C890B8CE85CC62522C40908D7E622F5D76193863665E04D8EDAC6E588DDD6C8FBF49139A\"}";
		//System.out.println(encrypt(test1));
//		String test2 = "vsnTiElriOMI6ehZetpGOWAEhJTThVuzsMg8THZdu0UrvmP/xtDk8VSc8Wa0BNRyWsh8NW9MwBNEropEaP4LuN/Ihlt2kB/saAdbgb7JgeujU8nIF5xu2nohNIUx1+4l7NDE+2d270g1FxoYG4A5v1mubFqq/62WFnC6kv7Nl9Gg/5JvgFlbntPEHDlBIty3";
		String test3 = "vsnTiElriOMI6ehZetpGOWAEhJTThVuzsMg8THZdu0UrvmP/xtDk8VSc8Wa0BNRyWsh8NW9MwBNEropEaP4LuN/Ihlt2kB/saAdbgb7JgeujU8nIF5xu2nohNIUx1+4l7NDE+2d270g1FxoYG4A5v1mubFqq/62WFnC6kv7Nl9Gg/5JvgFlbntPEHDlBIty3";
		System.out.println(decrypt(test3));
	}

	public static String encrypt(String data) {
		if (org.apache.commons.lang3.StringUtils.isEmpty(data)) {
			return null;
		} else {
			try {
				Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
				int blockSize = cipher.getBlockSize();
				byte[] dataBytes = data.getBytes();
				int plaintextLength = dataBytes.length;
				if (plaintextLength % blockSize != 0) {
					plaintextLength += blockSize - plaintextLength % blockSize;
				}

				byte[] plaintext = new byte[plaintextLength];
				System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
				SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
				IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
				cipher.init(1, keyspec, ivspec);
				byte[] encrypted = cipher.doFinal(plaintext);
				return (new BASE64Encoder()).encode(encrypted);
			} catch (Exception var9) {
				var9.printStackTrace();
				return null;
			}
		}
	}

	public static String decrypt(String data) {
		if (org.apache.commons.lang3.StringUtils.isEmpty(data)) {
			return null;
		} else {
			try {
				byte[] encrypted1 = (new BASE64Decoder()).decodeBuffer(data);
				Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
				SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
				IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
				cipher.init(2, keyspec, ivspec);
				byte[] original = cipher.doFinal(encrypted1);
				String originalString = new String(original);
				return originalString;
			} catch (Exception var7) {
				var7.printStackTrace();
				return data;
			}
		}
	}
	
}
