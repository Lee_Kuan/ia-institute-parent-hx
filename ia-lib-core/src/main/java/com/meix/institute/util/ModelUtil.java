package com.meix.institute.util;

import java.util.List;

import net.sf.json.JSONObject;

public class ModelUtil {
    
    /**
     * 请求参数JSONObject 转换为search
     * @param JSONObject obj
     * @param searchType
     * @return
     */
    public static <T> T obj2Search(JSONObject obj, final Class<T> searchType) {
        if (obj == null) {
            return null;
        }
        if (!obj.containsKey("currentPage")) {
            obj.put("currentPage", 0);
        }
        if (!obj.containsKey("showNum")) {
            obj.put("showNum", 10);
        } else if (obj.getInt("showNum") > 50) {
            obj.put("showNum", 50);
        }
        return GsonUtil.json2Obj(obj.toString(), searchType);
    }
    
    /**
     * 请求参数JSONObject 转换为info
     * @param JSONObject obj
     * @param infoType
     * @return
     */
    public static <T> T obj2Info(JSONObject obj, final Class<T> infoType) {
        return GsonUtil.json2Obj(obj.toString(), infoType);
    }
    
    /**
     * model转换为info
     * 
     * @param model
     * @param infoType
     * @return
     */
    public static <T> T model2Info(Object model, final Class<T> infoType) {
        if (model == null) {
            return null;
        }
        return GsonUtil.json2Obj(GsonUtil.obj2Json(model), infoType);
    }
    
    /**
     * search转换为model
     * 
     * @param model
     * @param modelType
     * @return
     */
    public static <T> T search2Model(Object search, final Class<T> modelType) {
        if (search == null) {
            return null;
        }
        return GsonUtil.json2Obj(GsonUtil.obj2Json(search), modelType);
    }
    
    /**
     * info转换为model
     * 
     * @param model
     * @param modelType
     * @return
     */
    public static <T> T info2Model(Object info, final Class<T> modelType) {
        if (info == null) {
            return null;
        }
        return GsonUtil.json2Obj(GsonUtil.obj2Json(info), modelType);
    }
    
    /**
     * model转换为info (list 方法)
     * @param modelList
     * @param infoType
     * @return
     */
    static public <E, T> List<E> model2InfoList(List<T> modelList, final Class<E> infoType) {
        return GsonUtil.list2OtherList(modelList, infoType);
    }
    
}
