package com.meix.institute.util;

import com.meix.institute.constant.MeixConstants;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Date;

/**
 * 文件说明: 常用日期转换函数的封装
 */

public class DateUtil {

	public static String dateFormat = "yyyy-MM-dd HH:mm:ss";
	public static String dateShortFormat = "yyyy-MM-dd";

	public static String formatDate(Date date) {
		SimpleDateFormat fmtDate = new SimpleDateFormat(dateFormat);
		return fmtDate.format(date);
	}

	public static String formatDate(Date date, String format) {
		SimpleDateFormat fmtDate = new SimpleDateFormat(format);
		return fmtDate.format(date);
	}

	/*
	 * 名称：strToDate 功能：将指定的字符串转换成日期 输入：aStrValue: 要转换的字符串; aFmtDate: 转换日期的格式,
	 * 默认为:"yyyy/MM/dd" aDteRtn: 转换后的日期
	 */
	public static Date strToDate(String aStrValue, String aFmtDate) {
		if (StringUtils.isBlank(aStrValue)) {
			return null;
		}
		Date aDteRtn = Calendar.getInstance().getTime();
		// System.out.println(Calendar.getInstance().getTime());
		if (aFmtDate.length() == 0) {
			aFmtDate = "yyyyMMddhhmmss";
		}
		SimpleDateFormat fmtDate = new SimpleDateFormat(aFmtDate);

		try {
			aDteRtn.setTime(fmtDate.parse(aStrValue).getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return aDteRtn;
	}

	public static Date stringToDate(String str) {
		if (str == null) {
			return null;
		}
		Date return_date = null;
		String format = "";
		if (str.length() > 16) {
			format = "yyyy-MM-dd HH:mm:ss";
		} else if (str.length() > 10) {
			format = "yyyy-MM-dd HH:mm";
		} else {
			format = "yyyy-MM-dd";
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return_date = sdf.parse(str);
		} catch (ParseException e) {

			e.getMessage();
			return null;
		}
		return return_date;
	}

	// ***************************************************
	// 名称：dateToStr
	// 功能：将指定的日期转换成字符串
	// 输入：aDteValue: 要转换的日期;
	// aFmtDate: 转换日期的格式, 默认为:"yyyy-MM-dd"
	// 输出：
	// 返回：转换之后的字符串
	// ***************************************************
	public static String dateToStr(Date aDteValue, String aFmtDate) {
		if (aDteValue == null) {
			return null;
		}
		String strRtn = null;

		if (aFmtDate.length() == 0) {
			aFmtDate = "yyyy-MM-dd";
		}
		SimpleDateFormat fmtDate = new SimpleDateFormat(aFmtDate);
		// Format fmtDate = new SimpleDateFormat(aFmtDate);
		// try {
		strRtn = fmtDate.format(aDteValue);
		// } catch (Exception e) {
		// return "无录入时间";
		// }
		return strRtn;
	}

	/**
	 * @param dateTime
	 * @param aFmtDate
	 * @return
	 * @Title: dateToStr、
	 * @Description:日期转换为字符串
	 * @return: String
	 */
	public static String dateToStr(long dateTime, String aFmtDate) {
		String strRtn = null;
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(dateTime);
		if (aFmtDate.length() == 0) {
			aFmtDate = "yyyy-MM-dd";
		}
		SimpleDateFormat fmtDate = new SimpleDateFormat(aFmtDate);
		// Format fmtDate = new SimpleDateFormat(aFmtDate);
		// try {
		strRtn = fmtDate.format(calendar.getTime());
		// } catch (Exception e) {
		// return "无录入时间";
		// }
		return strRtn;
	}

	/**
	 * 获取本周的开始时间
	 *
	 * @param weekBeginDay 本周从周几开始
	 * @return
	 */
	public static String getWeekBegin(DayOfWeek weekBeginDay) {
		WeekFields weekFields = WeekFields.of(weekBeginDay, 1);
		LocalDateTime curTime = LocalDateTime.now();
		LocalDateTime startDate = LocalDateTime.of(curTime.toLocalDate(), LocalTime.MIN).minusDays(curTime.get(weekFields.dayOfWeek()) - 1);
		return startDate.format(DateTimeFormatter.ofPattern(dateFormat));
	}

	/**
	 * @return
	 * @Title: getFirstDayOfMonth、
	 * @Description:获取本月的第一天
	 * @return: String
	 */
	public static String getFirstDayOfMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		return dateToStr(calendar.getTime(), "");
	}

	public static Date getThisWeekBegin() {
		Calendar beginTimecal = Calendar.getInstance();
		beginTimecal.set(Calendar.DAY_OF_WEEK, 1);
		beginTimecal.set(Calendar.HOUR_OF_DAY, 0);
		beginTimecal.set(Calendar.MINUTE, 0);
		beginTimecal.set(Calendar.SECOND, 0);
		return beginTimecal.getTime();
	}

	public static Date getLastWeekBegin() {
		Calendar beginTimecal = Calendar.getInstance();
		beginTimecal.set(Calendar.WEEK_OF_MONTH,
			Calendar.getInstance().get(Calendar.WEEK_OF_MONTH) - 1);
		beginTimecal.set(Calendar.DAY_OF_WEEK, 1);
		beginTimecal.set(Calendar.HOUR_OF_DAY, 0);
		beginTimecal.set(Calendar.MINUTE, 0);
		beginTimecal.set(Calendar.SECOND, 0);
		return beginTimecal.getTime();
	}

	public static Date getLastWeekEnd() {
		Calendar endTimecal = Calendar.getInstance();
		endTimecal.set(Calendar.WEEK_OF_MONTH,
			Calendar.getInstance().get(Calendar.WEEK_OF_MONTH) - 1);
		endTimecal.set(Calendar.DAY_OF_WEEK, 7);
		endTimecal.set(Calendar.HOUR_OF_DAY, 23);
		endTimecal.set(Calendar.MINUTE, 59);
		endTimecal.set(Calendar.SECOND, 59);
		return endTimecal.getTime();

	}

	/**
	 * @param dateStr
	 * @return
	 * @Title: isWeekend、
	 * @Description:判断日期是否属于周末
	 * @return: boolean
	 */
	public static boolean isWeekend(String dateStr) {
		Date date = stringToDate(dateStr);
		return isWeekend(date);
	}

	/**
	 * @param date
	 * @return
	 * @Title: isWeekend、
	 * @Description:判断日期是否是周末
	 * @return: boolean
	 */
	public static boolean isWeekend(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ||
			calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			return true;
		}
		return false;
	}

	/**
	 * 取得今天的最后一个时刻
	 *
	 * @return 今天的最后一个时刻
	 */
	public static Date currentEndDate() {
		return getEndDate(new Date());
	}

	/**
	 * 取得今天的第一个时刻
	 *
	 * @return 今天的第一个时刻
	 */
	public static Date currentStartDate() {
		return getStartDate(new Date());
	}

	/**
	 * 获取某天的起始时间, e.g. 2005-10-01 00:00:00.000
	 *
	 * @param date 日期对象
	 * @return 该天的起始时间
	 */
	public static Date getStartDate(Date date) {
		if (date == null) {
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	/**
	 * 获取某天的结束时间, e.g. 2005-10-01 23:59:59.999
	 *
	 * @param date 日期对象
	 * @return 该天的结束时间
	 */
	public static Date getEndDate(Date date) {

		if (date == null) {
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);

		return cal.getTime();
	}

	public static String getEarlyOneMonthFromNow(Date date) {
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		return dateToStr(cal.getTime(), "");
	}

	public static String getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		return dateToStr(cal.getTime(), "");
	}

	/**
	 * @return
	 * @Title: getCurrentTime、
	 * @Description:获取当前时间
	 * @return: String
	 */
	public static String getCurrentTime() {
		Calendar cal = Calendar.getInstance();
		return dateToStr(cal.getTime(), "HH:mm:ss");
	}

	public static String getCurrentDateTime() {
		Calendar cal = Calendar.getInstance();
		return dateToStr(cal.getTime(), dateFormat);
	}

	public static String dateToWeek(String date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		Calendar c = Calendar.getInstance();
		String week = "";
		try {
			c.setTime(format.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
			return "日期错误";
		}
		switch (c.get(Calendar.DAY_OF_WEEK)) {
			case 1:
				week = "星期日";
				break;
			case 2:
				week = "星期一";
				break;
			case 3:
				week = "星期二";
				break;
			case 4:
				week = "星期三";
				break;
			case 5:
				week = "星期四";
				break;
			case 6:
				week = "星期五";
				break;
			default:
				week = "星期六";
				break;
		}
		return week;
	}

	public static String getToWeek() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String newDate = "";
		Date de = null;
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(new Date());
		} catch (Exception e) {
			e.printStackTrace();
			return "日期错误";
		}
		switch (c.get(Calendar.DAY_OF_WEEK)) {
			case 1:
				c.add(Calendar.DATE, -2);//周日
				de = c.getTime();
				newDate = format.format(de);
				break;
			case 7:
				c.add(Calendar.DATE, -1);//周六
				de = c.getTime();
				newDate = format.format(de);
				break;
			case 2:
				c.add(Calendar.DATE, -3);//周一
				de = c.getTime();
				newDate = format.format(de);
				break;
			default:
				c.add(Calendar.DATE, -1);
				de = c.getTime();
				newDate = format.format(de);
				break;
		}
		return newDate;
	}

	/**
	 * @param dateStr
	 * @param n
	 * @return
	 * @Title: getDayDate、
	 * @Description:根据传入的日期计算前几天，后几天
	 * @return: String
	 */
	public static String getDayDate(String dateStr, int n) {
		Date date = stringToDate(dateStr);
		if (date == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, n);
		return dateToStr(calendar.getTime(), "yyyy-MM-dd");
	}

	/**
	 * @param dateStr
	 * @param n
	 * @return
	 * @Title: getHourDate、
	 * @Description:小时计算
	 * @return: String
	 */
	public static String getHourDate(String dateStr, int n) {
		Date date = stringToDate(dateStr);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, n);
		return dateToStr(calendar.getTime(), "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * @param dateStr
	 * @param n
	 * @return
	 * @Title: getMonthDate、
	 * @Description:月计算
	 * @return: String
	 */
	public static String getMonthDate(String dateStr, int n) {
		Date date = stringToDate(dateStr);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, n);
		return dateToStr(calendar.getTime(), "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * @param n
	 * @return
	 * @Title: getDayDate、
	 * @Description:根据传入的日期计算前几天，后几天
	 * @return: String
	 */
	public static String getDayDate(int n) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, n);
		return dateToStr(calendar.getTime(), "yyyy-MM-dd");
	}

	/**
	 * 获取时间间隔日期
	 *
	 * @param date
	 * @param n
	 * @return
	 */
	public static String getIntervalDate(String date, int n) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(stringToDate(date));
		calendar.add(Calendar.DAY_OF_MONTH, n);
		return dateToStr(calendar.getTime(), "yyyy-MM-dd");
	}

	public static Date getDate(int n) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, n);
		return calendar.getTime();
	}

	/**
	 * @param dateStr
	 * @param n
	 * @return
	 * @Title: getMinuteDate、
	 * @Description:
	 * @return: String
	 */
	public static String getMinuteDate(String dateStr, int n) {
		return getMinuteDate(dateStr, n, "yyyy-MM-dd HH:mm");
	}

	public static String getMinuteDate(String dateStr, int n, String format) {
		Date date = stringToDate(dateStr);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, n);
		return dateToStr(calendar.getTime(), format);
	}

	/**
	 * @param dateStr
	 * @param n
	 * @return
	 * @Title: getSecondDateTime、
	 * @Description:计算前几秒后几秒
	 * @return: String
	 */
	public static String getSecondDateTime(String dateStr, int n) {
		Date date = stringToDate(dateStr);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.SECOND, n);
		return dateToStr(calendar.getTime(), "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * @param dateTime1
	 * @param dateTime2
	 * @return -1 小于  1大于  0等于
	 * @Title: compare、
	 * @Description:比较两个日期大小
	 * @return: int
	 */
	public static int compare(String dateTime1, String dateTime2) {
		if (dateTime1.compareTo(dateTime2) > 0) {
			return 1;
		} else if (dateTime1.compareTo(dateTime2) < 0) {
			return -1;
		} else {
			return 0;
		}
	}

	/**
	 * @author zhangzhen(J)
	 * @date 2016年8月24日上午10:00:51
	 * @Description: 秒数 转为  时：分：秒
	 */
	public static String formatSencond(int time) {
		StringBuffer timeStr = new StringBuffer();
		int hour = 0;
		int minute = 0;
		int second = 0;
		if (time <= 0)
			return "";
		else {
			minute = time / 60;
			if (minute < 60) {
				second = time % 60;
				timeStr.append(unitFormat(minute)).append(":").append(unitFormat(second));
			} else {
				hour = minute / 60;
				if (hour > 99)
					return "99:59:59";
				minute = minute % 60;
				second = time - hour * 3600 - minute * 60;
				timeStr.append(unitFormat(hour)).append(":").append(unitFormat(minute)).append(":").append(unitFormat(second));
			}
		}
		return timeStr.toString();
	}

	private static StringBuffer unitFormat(int i) {
		StringBuffer retStr = new StringBuffer();
		if (i >= 0 && i < 10)
			retStr.append("0").append(Integer.toString(i));
		else
			retStr.append(i);
		return retStr;
	}

	public static boolean isTradingTime(String market, String time) {
		switch (market) {
			case "A":
				if ((time.compareTo("0930") > 0 && time.compareTo("1130") <= 0)
					||
					(time.compareTo("1300") >= 0 && time.compareTo("1459") < 0)
					) {
					return true;
				}
				break;
			case "HK":
				if ((time.compareTo("0930") > 0 && time.compareTo("1200") <= 0)
					||
					(time.compareTo("1300") >= 0 && time.compareTo("1609") < 0)
					) {
					return true;
				}
				break;
		}
		return false;
	}

	public static int getRepealStatus(String market, String orderTime, int orderDelayTime) {
		//调仓创建5分钟后的时间(调仓生效时间)
		Date orderRealTime = DateUtil.stringToDate(DateUtil.getMinuteDate(orderTime, orderDelayTime, "yyyy-MM-dd HH:mm:ss"));
		Date now = new Date();
		String settleDate = DateUtil.dateToStr(now, "");
		String startTime = settleDate + " 09:25";// 开盘时间
		String noonEnd = settleDate + " 11:30";// 中午停市
		String noonStart = settleDate + " 13:00";// 中午开市
		String endTime = settleDate + " 14:59";// 收盘时间

		switch (market) {
			case "HK":
				startTime = settleDate + " 09:00";// 开盘时间
				noonEnd = settleDate + " 12:00";// 中午停市
				noonStart = settleDate + " 13:00";// 中午开市
				endTime = settleDate + " 16:09";//收盘时间
				break;
		}

		int allowRepeal = 1;
		if (now.compareTo(DateUtil.stringToDate(startTime)) < 0) {// 撤销指令是否在开盘时间之前
			allowRepeal = 1;
		} else {
			if (now.compareTo(DateUtil.stringToDate(endTime)) >= 0) {// 撤销指令是否在收盘时间之后
				if (orderTime.compareTo(endTime) >= 0) {// 调仓指令是否在收盘时间之后
					allowRepeal = 1;
				} else {
					if (now.compareTo(orderRealTime) > 0) {
						allowRepeal = 3;
					} else {
						allowRepeal = 1;
					}
				}
			} else {
				// 调仓时间、撤销指令均在中午停市-中午开市之间，可以撤销
				if ((now.compareTo(DateUtil.stringToDate(noonEnd)) > 0 && now.compareTo(DateUtil.stringToDate(noonStart)) < 0)
					&& (orderTime.compareTo(noonEnd) > 0 && orderTime.compareTo(noonStart) < 0)) {
					allowRepeal = 1;
				} else {
					if (now.compareTo(orderRealTime) > 0) {
						allowRepeal = 2;
					} else {
						allowRepeal = 1;
					}
				}
			}
		}

		return allowRepeal;
	}

	public static long dateDiff(Date dateFrom, Date dateTo) throws Exception {
		Calendar calFrom = Calendar.getInstance();
		calFrom.setTime(dateFrom);
		calFrom.set(Calendar.HOUR_OF_DAY, 0);
		calFrom.set(Calendar.MINUTE, 0);
		calFrom.set(Calendar.SECOND, 0);
		calFrom.set(Calendar.MILLISECOND, 0);

		Calendar calTo = Calendar.getInstance();
		calTo.setTime(dateTo);
		calTo.set(Calendar.HOUR_OF_DAY, 0);
		calTo.set(Calendar.MINUTE, 0);
		calTo.set(Calendar.SECOND, 0);
		calTo.set(Calendar.MILLISECOND, 0);

		long dayDiff = (calTo.getTimeInMillis() - calFrom.getTimeInMillis()) / (1000 * 60 * 60 * 24);
		return dayDiff;

	}

	public static String diff(Date dateFrom, Date dateTo) throws Exception {
		Calendar calFrom = Calendar.getInstance();
		calFrom.setTime(dateFrom);
		Calendar calTo = Calendar.getInstance();
		calTo.setTime(dateTo);
		long dayDiff = (calTo.getTimeInMillis() - calFrom.getTimeInMillis()) / 1000;
		String diff = null;
		if (dayDiff > 60 * 60 * 24 * 30 * 12) { //1年前
			diff = "1年前";
		} else if (dayDiff > 60 * 60 * 24 * 30 * 6) { //半年前
			diff = "半年前";
		} else if (dayDiff > 60 * 60 * 24 * 30 * 2) { // 2个月前
			diff = "2个月前";
		} else if (dayDiff > 60 * 60 * 24 * 30) { //30天前
			diff = "30天前";
		} else if (dayDiff > 60 * 60 * 24) { // 1天前
			diff = "1天前";
		} else if (dayDiff > 60 * 60) { //1小时前
			diff = "1小时前";
		} else if (dayDiff > 60 * 10) { //10分钟前
			diff = "10分钟前";
		} else if (dayDiff > 60) { //
			long n = dayDiff / 60;
			diff = n + "分钟前";
		} else {
			diff = "刚刚";
		}
		return diff;
	}

	/**
	 * 获取当年第一天日期
	 *
	 * @return Date
	 */
	public static String getYearFirst() {
		Calendar currCal = Calendar.getInstance();
		int currentYear = currCal.get(Calendar.YEAR);
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, currentYear);
		Date currYearFirst = calendar.getTime();
		return dateToStr(currYearFirst, dateFormat);
	}

	/**
	 * @return
	 * @Title: getFirstDayOfMonth、
	 * @Description:获取本月的第一天
	 * @return: String
	 */
	public static String getFDayOfMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return dateToStr(calendar.getTime(), dateFormat);
	}

	/**
	 * @return
	 * @Title: getFirstDayOfWeek、
	 * @Description:获取本周的第一天
	 * @return: String
	 */
	public static String getFirstDayOfWeek() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return dateToStr(cal.getTime(), dateFormat);
	}

	public static void main(String[] args) throws Exception {
		// System.out.println("this weekbegin--" + DateUtil.getThisWeekBegin());
		// System.out.println("last weekbegin--" + DateUtil.getLastWeekBegin());
		// System.out.println("last weekend--" + DateUtil.getLastWeekEnd());
		// System.out.println("getCurrentDate" + DateUtil.getCurrentDate());
		// System.out.println("EarlyOneMonthFromNow--"
		// + DateUtil.getEarlyOneMonthFromNow(new Date()));
		// System.out.println(DateUtil.dateToWeek("2008-09-25"));
		String intervalDate = getIntervalDate("2018-09-01", -1);
		System.out.println(intervalDate);
	}

	/**
	 * 获取交易状态
	 */
	public static int getTradingStatus(LocalTime time) {
		LocalTime time1 = LocalTime.of(9, 0, 0);
		LocalTime time2 = LocalTime.of(9, 30, 0);
		LocalTime time3 = LocalTime.of(11, 30, 0);
		LocalTime time4 = LocalTime.of(13, 0, 0);
		LocalTime time5 = LocalTime.of(15, 30, 0);
		if (time.isBefore(time1) || time.isAfter(time5)) {
			return MeixConstants.STOCK_TRADING_STATUS_SP;
		} else if (time.isBefore(time2) && time.isAfter(time1)) {
			return MeixConstants.STOCK_TRADING_STATUS_JJ;
		} else if ((time.isBefore(time3) && time.isAfter(time2)) || ((time.isBefore(time5) && time.isAfter(time4)))) {
			return MeixConstants.STOCK_TRADING_STATUS_JY;
		} else if (time.isBefore(time4) && time.isAfter(time3)) {
			return MeixConstants.STOCK_TRADING_STATUS_XS;
		}
		return MeixConstants.STOCK_TRADING_STATUS_QT;
	}

	/**
	 * 统一时间展示
	 */
	public static String formatReprotPublishDate(Date publishDate) {
		if (null == publishDate) {
			return "";
		}
		// 发布日期
		String publishDateStr = DateUtil.dateToStr(publishDate, DateUtil.dateFormat);
		// 今天
		Date _now = new Date();
		String _jt = DateUtil.dateToStr(_now, DateUtil.dateShortFormat);
		int compare = DateUtil.compare(publishDateStr, _jt);
		if (compare > 0) {
			Calendar calFrom = Calendar.getInstance();
			calFrom.setTime(publishDate);
			Calendar calTo = Calendar.getInstance();
			calTo.setTime(_now);
			double dayDiff = (calTo.getTimeInMillis() - calFrom.getTimeInMillis()) / 1000 / 3600;
			if (dayDiff < 1) {
				return "1小时内";
			}
			int ceil = (int) Math.floor(dayDiff);
			return ceil + "小时前";
		}
		// 昨天
		String _zt = DateUtil.getDayDate(_jt, -1);
		compare = DateUtil.compare(publishDateStr, _zt);
		if (compare > 0) {
			return "昨天";
		}
		// 前天
		String _qt = DateUtil.getDayDate(_jt, -2);
		compare = DateUtil.compare(publishDateStr, _qt);
		if (compare > 0) {
			return "前天";
		}
		// 今天以内
		String _jnyn = DateUtil.getYearFirst();
		compare = DateUtil.compare(publishDateStr, _jnyn);
		if (compare > 0) {
			return DateUtil.dateToStr(publishDate, "MM-dd");
		}
		// 今年以前
		return publishDateStr;
	}
}
