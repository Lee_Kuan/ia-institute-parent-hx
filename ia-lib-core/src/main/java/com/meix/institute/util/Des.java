package com.meix.institute.util;


import org.apache.commons.lang.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

public class Des {
	private static final String PASSWORD_CRYPT_KEY = "__jDlog_";
	private final static String DES = "DES";

	/**
	 * 加密
	 *
	 * @param src 数据源
	 * @param key 密钥，长度必须是8的倍数
	 * @return 返回加密后的数据
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] src, byte[] key) throws Exception {
		//DES算法要求有一个可信任的随机数源
		SecureRandom sr = new SecureRandom();
		// 从原始密匙数据创建DESKeySpec对象
		DESKeySpec dks = new DESKeySpec(key);
		// 创建一个密匙工厂，然后用它把DESKeySpec转换成
		// 一个SecretKey对象
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
		SecretKey securekey = keyFactory.generateSecret(dks);
		// Cipher对象实际完成加密操作
		Cipher cipher = Cipher.getInstance(DES);
		// 用密匙初始化Cipher对象
		cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);
		// 现在，获取数据并加密
		// 正式执行加密操作
		return cipher.doFinal(src);
	}

	private static byte[] iv = {1, 2, 3, 4, 5, 6, 7, 8};

	public static byte[] encrypt2(byte[] src, byte[] keys) throws Exception {
		IvParameterSpec zeroIv = new IvParameterSpec(iv);
		SecretKeySpec key = new SecretKeySpec(keys, "DES");
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
		byte[] encryptedData = cipher.doFinal(src);
		return encryptedData;
	}

	/**
	 * 解密
	 *
	 * @param src 数据源
	 * @param key 密钥，长度必须是8的倍数
	 * @return 返回解密后的原始数据
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] src, byte[] key) throws Exception {
		// DES算法要求有一个可信任的随机数源
		SecureRandom sr = new SecureRandom();
		// 从原始密匙数据创建一个DESKeySpec对象
		DESKeySpec dks = new DESKeySpec(key);
		// 创建一个密匙工厂，然后用它把DESKeySpec对象转换成
		// 一个SecretKey对象
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
		SecretKey securekey = keyFactory.generateSecret(dks);
		// Cipher对象实际完成解密操作
		Cipher cipher = Cipher.getInstance(DES);
		// 用密匙初始化Cipher对象
		cipher.init(Cipher.DECRYPT_MODE, securekey, sr);
		// 现在，获取数据并解密
		// 正式执行解密操作
		return cipher.doFinal(src);
	}

	/**
	 * @param src
	 * @param keys
	 * @return
	 * @throws Exception
	 * @Title: decrypt2、
	 * @Description:CBC
	 * @return: byte[]
	 */
	public static byte[] decrypt2(byte[] src, byte[] keys) throws Exception {
		IvParameterSpec zeroIv = new IvParameterSpec(iv);
		SecretKeySpec key = new SecretKeySpec(keys, "DES");
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key, zeroIv);
		byte decryptedData[] = cipher.doFinal(src);
		return decryptedData;
	}

	/**
	 * 密码解密
	 *
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public final static String decrypt(String data) {
		try {
			return new String(decrypt(hex2byte(data.getBytes()),
				PASSWORD_CRYPT_KEY.getBytes()));
		} catch (Exception e) {
		}
		return null;
	}

	public final static String decrypt(String data, String key) {
		try {
			return decrypt(data, key, 1);
		} catch (Exception e) {
		}
		return null;
	}

	public final static String decrypt(String data, String key, int byteMethod) {
		try {
			if (byteMethod == 1) {
				return new String(decrypt(hex2byte(data.getBytes()),
					key.getBytes()));

			} else {
				return new String(decrypt2(decryptBASE64(data),
					key.getBytes()));
			}
		} catch (Exception e) {
		}
		return null;
	}

	public final static String decryptNoBase64(String data, String key, int byteMethod) {
		try {
			if (byteMethod == 1) {
				return new String(decrypt(data.getBytes(),
					key.getBytes()));

			} else {
				System.out.println(Arrays.toString(data.getBytes("iso-8859-1")));
				return new String(decrypt2(data.getBytes("iso-8859-1"),
					key.getBytes()), "iso-8859-1");
			}
		} catch (Exception e) {
		}
		return null;
	}

	public final static String decrypt(byte[] data) {
		try {
			return new String(decrypt(hex2byte(data),
				PASSWORD_CRYPT_KEY.getBytes()));
		} catch (Exception e) {

		}
		return null;
	}

	/**
	 * 加密
	 *
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public final static String encrypt(String content) {
		try {
			return byte2hex(encrypt(content.getBytes(), PASSWORD_CRYPT_KEY.getBytes()));
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * @param content
	 * @param key
	 * @return
	 * @Title: encrypt、
	 * @Description:自定义密钥
	 * @return: String
	 */
	public final static String encrypt(String content, String key) {
		try {
			return byteArr2HexStr(encrypt(content.getBytes(), key.getBytes()));
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * @param content
	 * @param key
	 * @param byteMethod 字节编码方式
	 * @return
	 * @Title: encrypt、加密
	 * @Description:
	 * @return: String
	 */
	public final static String encrypt(String content, String key, int byteMethod) {
		try {
			if (byteMethod == 1) {
				return byteArr2HexStr(encrypt(content.getBytes("UTF-8"), key.getBytes()));
			} else {
				//base64编码
				return encryptBASE64(encrypt2(content.getBytes("UTF-8"), key.getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public final static byte[] encrypt(byte[] content, String key, int byteMethod) {
		try {
			if (byteMethod == 1) {
				return encrypt(content, key.getBytes());
			} else {
				return encrypt2(content, key.getBytes());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public final static String encrypt(byte[] content) {
		try {
			return byte2hex(encrypt(content, PASSWORD_CRYPT_KEY.getBytes()));
		} catch (Exception e) {
		}
		return null;

	}

	/**
	 * 二行制转字符串
	 *
	 * @param b
	 * @return
	 */
	public static String byte2hex(byte[] b) {
		String hs = "";
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = (Integer.toHexString(b[n] & 0XFF));
			if (stmp.length() == 1)
				hs = hs + "0" + stmp;
			else
				hs = hs + stmp;
		}
		return hs.toUpperCase();
	}

	/**
	 * 将byte数组转换为表示16进制值的字符串， 如：byte[]{8,18}转换为：0813， 和public static byte[]
	 * hexStr2ByteArr(String strIn) 互为可逆的转换过程
	 *
	 * @param arrB 需要转换的byte数组
	 * @return 转换后的字符串
	 * @throws Exception
	 */
	public static String byteArr2HexStr(byte[] arrB) throws Exception {
		int iLen = arrB.length;
		// 每个byte用两个字符才能表示，所以字符串的长度是数组长度的两倍
		StringBuffer sb = new StringBuffer(iLen * 2);
		for (int i = 0; i < iLen; i++) {
			int intTmp = arrB[i];
			// 把负数转换为正数
			while (intTmp < 0) {
				intTmp = intTmp + 256;
			}
			// 小于0F的数需要在前面补0
			if (intTmp < 16) {
				sb.append("0");
			}
			sb.append(Integer.toString(intTmp, 16));
		}
		return sb.toString();
	}

	public static byte[] hex2byte(byte[] b) {
		if ((b.length % 2) != 0)
			throw new IllegalArgumentException("长度不是偶数");
		byte[] b2 = new byte[b.length / 2];
		for (int n = 0; n < b.length; n += 2) {
			String item = new String(b, n, 2);
			b2[n / 2] = (byte) Integer.parseInt(item, 16);
		}
		return b2;
	}

	/**
	 * 将表示16进制值的字符串转换为byte数组， 和public static String byteArr2HexStr(byte[] arrB)
	 * 互为可逆的转换过程
	 *
	 * @param strIn 需要转换的字符串
	 * @return 转换后的byte数组
	 * @throws Exception
	 */
	public static byte[] hexStr2ByteArr(String strIn) throws Exception {
		byte[] arrB = strIn.getBytes();
		int iLen = arrB.length;

		// 两个字符表示一个字节，所以字节数组长度是字符串长度除以2
		byte[] arrOut = new byte[iLen / 2];
		for (int i = 0; i < iLen; i = i + 2) {
			String strTmp = new String(arrB, i, 2);
			arrOut[i / 2] = (byte) Integer.parseInt(strTmp, 16);
		}
		return arrOut;
	}

	/**
	 * BASE64解密
	 *
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptBASE64(String key) throws Exception {
		return Base64.getDecoder().decode(key);
	}

	public static String decryptBASE64AsString(String key) {
		if (StringUtils.isEmpty(key)) {
			return "";
		}
		try {
			return new String(Base64.getDecoder().decode(key), "UTF-8");
		} catch (Exception e) {
			throw new RuntimeException("base64解析失败！" + e.getMessage(), e);
		}
	}

	/**
	 * BASE64加密
	 *
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String encryptBASE64(byte[] key) throws Exception {
		return Base64.getEncoder().encodeToString(key);
	}

	public static String encryptBASE64(String key) throws Exception {
		return Base64.getEncoder().encodeToString(key.getBytes("UTF-8"));
	}


	/**
	 * @param reponseText
	 * @param deviceId
	 * @return
	 * @Title: decryptByKeyLength、
	 * @Description:根据密钥长度解密
	 * @return: String
	 */
	public static String decryptByKeyLength(String reponseText, String deviceId) {
		if (!StringUtils.isEmpty(deviceId)) {
			//解密
			if (deviceId.length() < 8) {
				reponseText = Des.decrypt(reponseText);
			} else {
				reponseText = Des.decrypt(reponseText, deviceId.substring(0, 8), 2);
			}
		}
		return reponseText;
	}

	//测试数据
	public static void main(String[] args) throws Exception {
//    	String tokens="";
//		String[] tokenArray=tokens.split(",");
//		for (String string : tokenArray) {
//			
////		}
//    		System.out.println(Des.encrypt("hxm1919"));
		// 接口数据解密
//		System.out.println(Des.decrypt("", "&iaweb29", 2));
		// 用户密码解密
//		System.out.println(Des.decrypt("23521ba935b6af4e85a7c689e06767a1022b6d0b2076926350c40a34b475468a76fc899bfe954d1200c8a379798bf46e", "research_dowload_ireport"));
//		System.out.println(Des.decryptBASE64AsString("eyJjdCI6MTQ4NTE0ODA2OTc0MCwiaCI6Mjc3OSwibXQiOjAsIm9yaWdpbiI6InBpY18xNDU4N18xNDg1MTQ4MDQ4NjMzIiwic2l6ZSI6ODA1NjIyLCJ0eXBlIjoicGljXzE0NTg3XzE0ODUxNDgwNDg2MzMiLCJ1cmwiOiJwcm9kL21lc3NhZ2UvMTQ1ODcvMTQ1ODdfMTQ4NTE0ODA2OTIxNi5waWNfMTQ1ODdfMTQ4NTE0ODA0ODYzMyIsInciOjE1NjJ9"));
//		System.out.println(Des.encryptBASE64("619034920555A7F3"));
	}
}  
