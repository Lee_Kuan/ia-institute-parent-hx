package com.meix.institute.util;

import com.meix.institute.core.InvException;

public class ExceptionUtil {
    
    /**格式化错误消息*/
    public static String getFormatedMessage(Exception e) {
        String msg = null;
        if (e instanceof InvException) {
            InvException ie = (InvException) e;
            msg = getFormatedMessage(ie.getMessage(), ie.getArgs());
        } else {
            msg = "系统异常";
        }
        return msg;
    }

    public static String getFormatedMessage(String msg, Object... args) {
        String ret = msg;
        if (ret != null && args != null) {
            for (Object obj : args) {
                String str = obj != null ? obj.toString() : "";
                ret = ret.replaceFirst("\\{\\}", str);
            }
        }
        return ret;
    }
}
